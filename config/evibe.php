<?php

/**
 * Evibe admin config file
 *
 * @author Anji <anji@evibe.in>
 */
return [

	/*
	|--------------------------------------------------------------------------
	| Site
	|--------------------------------------------------------------------------
	*/
	'host'            => env('DASH_HOST', 'http://dash.evibe.in/'),
	'pro_host'        => env('PRO_HOST', 'http://pro.evibe.in'),

	/*
	|--------------------------------------------------------------------------
	| Customer head email address
	|--------------------------------------------------------------------------
	|
	| Email address of the executive taking care of customers bookings
	|
	*/
	'cust_head_email' => env('CUSTOMER_HEAD_EMAIL', 'swathi@evibe.in'),

	/*
	|--------------------------------------------------------------------------
	| default handler (regarding no response emails)
	|--------------------------------------------------------------------------
	*/
	'default_handler' => [
		'id'                   => env('DEFAULT_HANDLER_ID', '1008'),
		'name'                 => env('DEFAULT_HANDLER_NAME', 'Jyothi Mallavarapu'),
		'phone'                => env('DEFAULT_HANDLER_PHONE', '9640204000'),
		'email'                => env('DEFAULT_HANDLER_EMAIL', 'jyothi.m@evibe.in'),
		'metrics_data_user_id' => env('METRICS_DATA_USER_ID')
	],

	'operations'                => [
		'name'  => env('OPERATION_HEAD_NAME', 'Ashok Mettu'),
		'phone' => env('OPERATION_HEAD_PHONE', '9640807000'),
		'email' => env('OPERATION_HEAD_EMAIL', 'ashok.mettu@evibe.in'),
	],

	/*
	|--------------------------------------------------------------------------
	| Group emails
	|--------------------------------------------------------------------------
	*/
	'enquiry_group_email'       => env('ENQUIRY_GROUP_EMAIL', 'enquiry@evibe.in'),
	//'business_group_email'    => env('BUSINESS_GROUP_EMAIL', 'business@evibe.in'),
	'business_group_email'      => env('BUSINESS_GROUP_EMAIL', 'ops@evibe.in'),
	'operation_group_email'     => env('OPERATION_GROUP_EMAIL', 'operation@evibe.in'),
	'operation_group_email_1'   => env('OPERATION_GROUP_EMAIL1', 'ops@evibe.in'),
	'tech_group_email'          => env('TECH_GROUP_EMAIL', 'tech@evibe.in'),
	'tech_admin_email'          => env('TECH_ADMIN_EMAIL', 'sysad@evibe.in'),
	'accounts_group_email'      => env('ACCOUNTS_GROUP_EMAIL', 'accounts@evibe.in'),
	'invitations_group_email'   => env('INVITE_GROUP_EMAIL', 'invites@evibe.in'),
	'system_alert_email'        => env('SYSTEM_ALERT_EMAIL', 'system.alert@evibe.in'),
	'ops_alert_no_action_email' => env('OPS_ALERT_NO_ACTION_EMAIL', 'ops.alert.noaction@evibe.in'),

	/*
	|--------------------------------------------------------------------------
	| Web admin
	|--------------------------------------------------------------------------
	*/
	'webadmin'                  => env('WEB_ADMIN', 'anji@evibe.in'),
	'email_error'               => env('EMAIL_ERROR', false),

	/*
	|--------------------------------------------------------------------------
	| Phone number
	|--------------------------------------------------------------------------
	*/
	'phone'                     => '+91 9640204000',
	'phone_plain'               => '9640204000',

	/*
	|--------------------------------------------------------------------------
	| Email address
	|--------------------------------------------------------------------------
	*/
	'email'                     => 'ping@evibe.in',

	/*
	|--------------------------------------------------------------------------
	| Work Timing
	|--------------------------------------------------------------------------
	*/
	'work_timing'               => 'All Days, 10:00 AM - 10:00 PM',

	/*
	|--------------------------------------------------------------------------
	| Ticket type
	|--------------------------------------------------------------------------
	|
	| Always should be in sync with the values of the table `type_ticket`
	|
	*/

	'ticket_type'           => [
		'packages'           => env('TYPE_PAGE_PACKAGE', 1),
		'planners'           => env('TYPE_PAGE_PLANNER', 2),
		'venues'             => env('TYPE_PAGE_VENUE', 3),
		'services'           => env('TYPE_PAGE_SERVICE', 4),
		'trends'             => env('TYPE_PAGE_TREND', 5),
		'cakes'              => env('TYPE_PAGE_CAKE', 6),
		'home_page'          => 7,
		'no_results'         => 8,
		'dash'               => 9,
		'header'             => 10,
		'error'              => 11,
		'experience'         => 12,
		'decors'             => env('TYPE_PAGE_DECOR', 13),
		'entertainments'     => env('TYPE_PAGE_ENT', 14),
		'venue_halls'        => 15,
		'artists'            => 16,
		'resort'             => 17,
		'villa'              => env('TYPE_PAGE_VILLA', 18),
		'lounge'             => env('TYPE_PAGE_LOUNGE', 19),
		'food'               => env('TYPE_PAGE_FOOD', 20),
		'couple-experiences' => env('TYPE_PAGE_CE', 21),
		'venue-deals'        => env('TYPE_PAGE_VENUE_DEAL', 22),
		'priests'            => env('TYPE_PAGE_PRIEST', 23),
		'tents'              => env('TYPE_PAGE_TENT', 24),
		'party_bag'          => 25,
		'campaign'           => 26,
		'generic-package'    => env('TYPE_PAGE_GENERIC_PACKAGE', 27),
		'add-on'             => env('TYPE_PAGE_ADD_ON', 28),
	],

	/*
	|--------------------------------------------------------------------------
	| Ticket status
	|--------------------------------------------------------------------------
	*/
	'status'                => [
		'initiated'        => 1,
		'progress'         => 2,
		'confirmed'        => 3,
		'booked'           => 4,
		'cancelled'        => 5,
		'enquiry'          => 6,
		'followup'         => 7,
		'auto_pay'         => 8,
		'auto_cancel'      => 9,
		'service_auto_pay' => env('TYPE_STATUS_SERVICE_AP', 12),
		'not_interested'   => env('TYPE_STATUS_NOT_INTERESTED', 13),
		'duplicate'        => env('TYPE_STATUS_DUPLICATE', 14),
		'related'          => env('TYPE_STATUS_RELATED', 15),
		'irrelevant'       => env('TYPE_STATUS_IRRELEVANT', 16),
		'no_response'      => env('TYPE_STATUS_NO_RESPONSE', 17),
		'invalid_email'    => env('TYPE_STATUS_INVALID_STATUS', 18),
		'drop_off'         => env('TYPE_STATUS_DROP_OFF', 19)

	],

	/*
	 * Ticket creation source
	 */
	'ticket_enquiry_source' => [
		'phone'             => 1,
		'header_enquiry'    => 2,
		'chat'              => 3,
		'feasibility_check' => 4,
		'autobook'          => 5,
		'cd_decor'          => 6,
		'cd_food'           => 7,
		'cd_cake'           => 8,
		'product_enquiry'   => 9,
		'direct'            => 10,
		'direct_copy'       => 11,
		'pb_enquiry'        => 12,
		'home_enquiry'      => 13,
		'google_adwords'    => 14,
		'inspiration_page'  => 21,
		'vday_landing_page' => 22,
		'navigation_modal'  => 25
	],

	/*
	|--------------------------------------------------------------------------
	| Ticket Update type
	|--------------------------------------------------------------------------
	*/
	'ticket_type_update'    => [
		'phone'  => 'Phone',
		'email'  => 'Email',
		'auto'   => 'Auto',
		'manual' => 'Manual',
		'other'  => 'Other'
	],

	/*
	|--------------------------------------------------------------------------
	| Event type
	|--------------------------------------------------------------------------
	*/
	'event'                 => [
		'kids_birthday'      => env('EVENT_KIDS_BIRTHDAYS', 1),
		'bachelor_party'     => env('EVENT_BACHELOR_PARTY', 11),
		'pre_post'           => env('EVENT_PRE_POST', 13),
		'special_experience' => env('EVENT_COUPLE_EXPERIENCES', 16),
		'house_warming'      => env('EVENT_HOUSE_WARMING', 14),
		'corporate'          => env('EVENT_CORPORATE', 5),
		'others'             => env('EVENT_OTHERS', 23),
		'baby_shower'        => env('EVENT_BABY_SHOWER', 19),
		'saree_ceremony'     => env('EVENT_SAREE_CEREMONY', 34),
		'dhoti_ceremony'     => env('EVENT_DHOTI_CEREMONY', 35)
	],

	/*
	|--------------------------------------------------------------------------
	| Availability Check status
	|--------------------------------------------------------------------------
	*/
	'availability_status'   => [
		'confirm' => 1,
		'waiting' => 0
	],

	/*
	|--------------------------------------------------------------------------
	| Ticket status message
	|--------------------------------------------------------------------------
	*/
	'ticket_status_message' => [
		'booking_edit'             => 'Ticket booking edited',
		'order_process'            => 'Order process email sent',
		'more_info_edit'           => 'Booking more info edited',
		'payment_received'         => 'Payment received',
		'cancelled'                => 'Ticket got cancelled',
		'booking_deleted'          => 'Ticket booking deleted',
		'custom_quote'             => 'Custom quote email sent',
		'recommendation'           => 'Recommendation email sent',
		'exchange_contact'         => 'Contacts exchanged',
		'booking_finalize'         => 'Ticket booking finalized',
		'custom_email'             => 'Custom email sent',
		'no_response'              => 'No response notification sent to customer',
		'cancel_email'             => 'Cancel email sent to customer & vendor manually',
		'cancel_toggle_on'         => 'Auto cancel email toggle switched on',
		'cancel_toggle_off'        => 'Auto cancel toggle switched off',
		'reopen'                   => 'Ticket reopened',
		'edit_ticket'              => 'Ticket information has been edited',
		'receipt_automatic'        => 'Booked receipt sent automatically after payment',
		'receipt_manual'           => 'Booked receipt sent manually via Dash',
		'availability_asked'       => 'Venue availability asked',
		'partner_change'           => 'Partner changed',
		'old_partner_cancellation' => 'Cancellation receipt sent to old partner',
		//'partner_change'     => 'Partner changed with booking edit and updated receipts sent',
		'quick_book'               => 'Ticket mapping and booking have been created through quick book',
		'mark_star_order'          => 'Ticket booking has been marked as star order'
	],

	/*
	|--------------------------------------------------------------------------
	| Vendor status
	|--------------------------------------------------------------------------
	*/
	'vendor'                => [
		'status' => [
			'initiated'     => 1,
			'in_talk'       => 2,
			'signup_agreed' => 3,
			'data_received' => 4,
			'irrelevant'    => 5,
			'not_agree'     => 6,
			'live'          => 7,
			'non_live'      => 8
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Success email type
	|--------------------------------------------------------------------------
	|
	| 1 => Confirmed (advance amount left is above 0 => for ANY booking)
	| 2 => Booked (complete advance paid - for ALL bookings)
	|
	*/
	'success_type'          => [
		'confirmed' => 1,
		'booked'    => 2
	],

	/*
	|--------------------------------------------------------------------------
	| Itinerary for packages
	|--------------------------------------------------------------------------
	|
	*/
	'itinerary'             => [
		'lunch'  => '1',
		'dinner' => '2'
	],

	/*
	|--------------------------------------------------------------------------
	| Venues slot & Venue Type
	|--------------------------------------------------------------------------
	|
	*/
	'venue'                 => [
		'slot' => [
			'breakfast' => 7,
			'lunch'     => 8,
			'hi-tea'    => 9,
			'dinner'    => 10
		],

		'type' => [
			'veg'     => 1,
			'non_veg' => 2,
			'both'    => 3,
			'hall'    => 4
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Cancellation emails
	|--------------------------------------------------------------------------
	|
	| Cancellation emails are sent to customers automatically if advance is not
	| paid within 2 days of confirmation. This feature can be stopped manually.
	|
	*/
	'cancellation_mail'     => [
		'manual_action'  => 'manualCancellationMail',
		'is_auto_on'     => '1',
		'is_auto_off'    => '0',
		'is_success_on'  => '1',
		'is_success_off' => '0',
		'toggle_off_on'  => 'switch_on',
		'toggle_on_off'  => 'switch_off'
	],

	/**
	 * Server direct path
	 */
	'direct_root'           => env('SERVER_PATH_DIRECT_ROOT', '/var/www/evibe.in/web/'),

	/*
	|--------------------------------------------------------------------------
	| Gallery
	|--------------------------------------------------------------------------
	*/
	'gallery'               => [
		'root'                => env('GALLERY_ROOT', '../../gallery'),
		'host'                => env('GALLERY_HOST', 'http://gallery.evibe.in'),
		'youtube_host'        => 'https://www.youtube.com/watch?v=',
		'type'                => [
			'image' => 0,
			'video' => 1,
			'link'  => 2
		],
		'profile_pic'         => [
			'set'   => 1,
			'unset' => 0,
		],
		'default_profile_pic' => [
			'root'    => 'img/app',
			'package' => 'default_package.jpg',
			'venue'   => 'default_venue.png',
			'planner' => 'default_planner.png',
			'trend'   => 'default_planner.png',  // @todo: change this to trends
			'decor'   => 'default_planner.png', // @todo: change this to decors
			'service' => 'default_planner.png' // @todo: change this to service
		],
	],
	'root'                  => [
		'gallery_queue' => env('GALLERY_QUEUE_ROOT', '/var/www/html/gallery')
	],

	/*
	|--------------------------------------------------------------------------
	| For extensions
	|--------------------------------------------------------------------------
	*/
	'ext'                   => [
		'default'     => 10,
		'start'       => 10,
		'end'         => 99,
		'mapper_type' => [
			'planner'     => 1,
			'venue'       => 2,
			'customer'    => 3,
			'team_member' => 4
		],
	],

	/*
	|--------------------------------------------------------------------------
	| SMS Templates
	|--------------------------------------------------------------------------
	*/
	'sms_tpl'               => [
		'connect' => [
			'customer' => 'Hi #field1#, your party order with ID #field2# is under process. Pls contact #field3# on #field4# to finalise. Thanks, Team Evibe',
			'vendor'   => 'Hi #field1#, for booking on #field2#, pls contact #filed3# on #field4# to re-confirm and finalise from your side. Logon to EvibePro for order details.',
		],

		'reco' => [
			'customer' => "Hi #field1#, here are the best recommendations for your party: #field2#. Please check & submit your shortlist. Team Evibe.in"
		],

		'quote' => [
			'customer' => "Hi #field1#, we have emailed you a custom quote for your party to #field2#. Please check and revert at the earliest. Thanks, Team Evibe"
		],

		'no_response' => [
			'customer' => 'Hi #field1#, we tried reaching #field2# for your party enquiry but there was no response. Pls call us on #field3# to take it further. Evibe.in'
		],

		'pre_book' => [
			'customer' => 'Hi #field1#, requested party arrangements are available for #field2#. Pls pay advance of #field3# by #field4# using the link: #field5# to complete your booking. Evibe.in',
			'vendor'   => 'Hi #field1#, order for #field2# by #field3# (#field4#) is under process. On receiving advance of Rs. #field5#, we will confirm the order. Evibe.in',
		],

		'book' => [
			'customer' => 'Hi #field1#, your party order for #field2# is successfully booked. We have received an advance of Rs. #field3#. Your vendor is #field4# (#field5#). Pls check your email for booking receipt. Evibe.in',
			'vendor'   => 'Hi #field1#, order booked for #field2#. Received Rs. #field3# from #field4#. Pls check your email for full details & call #field5# within 8 business hours to assure from your side. Team Evibe.',
		],

		'book_type2'  => [
			'customer' => 'Hi #field1#, your party order for #field2# is booked. We received an advance of Rs. #field3#. Pls check your email for booking receipt & call your vendor #field4# (#field5#) to discuss any last minute details. Also, pls carry remaining balance of Rs. #field6# in cash which needs to be paid to #field7# at your event.',
			'vendor'   => 'Hi #field1#, order booked for #field2#. Received Rs. #field3# from #field4#. Pls check your email for full details & call #field5# immediately to assure from your side. Also remind them to pay remaining balance of Rs. #field6# at the event.',
		],
		'leads'       => [
			'non_venue' => [
				'vendor'   => 'Hi #field1#, pls. call #field2# (#field3#) on #field4# to help them with customisation details. Once you talk / meet, pls. update the status on #field5#. Evibe.in',
				'customer' => "Hi #field1#, you can call #field2#'s team on #field3# to discuss your party customisations. Pls. call #field4# to receive official booking confirmation when you are ready to finalize. Team Evibe."
			],
			'venue'     => [
				'vendor'   => 'Hi #field1#, a customer is interested in booking your services for #field2#. Pls contact #field3#, #field4# Evibe.in',
				'customer' => 'Hi #field1#, contact details of vendor(s) you liked:#field2#. We have already informed them. Team Evibe'
			]
		],
		'cancel'      => [
			'customer' => 'Hi #field1#, we regret to inform that your party order for #field2# is cancelled as #field3#. Call #field4# for any queries. Evibe.in',
			'vendor'   => 'Hi #field1#, we regret to inform that booking order for #field2# by #field3# got cancelled since #field4#. Team Evibe.',
		],
		'auto_cancel' => [
			'normal'      => 'Hi #field1#, sorry! your order for #field2# for #field3# is not available. Full refund of Rs. #field4# will be initiated in 2 business days. For any queries, pls reply to the email sent to #field5#. Team Evibe',
			'venue_deals' => 'Hi #field1#, we regret to inform that deal at #field2# is NOT available for your party on #field3#, #field4# slot. We will initiate full token amount refund of Rs. #field5# within 2 Business days. For any queries, pls reply to the email sent to #field6#. Team Evibe.in',
			'cake'        => 'Hi #field1#, we regret to inform that your order for #field2# is NOT available for #field3# (#field4# slot). We will initiate full refund of Rs. #field5# within 2 Business days. For any queries, please reply to the email sent to #field6#. Team Evibe.in'
		],
		'pro_app'     => [
			'otp'          => 'Hi, your One Time Password (OTP) to reset your password is #field1#. Pls do not share with anyone. Evibe.in',
			'notification' => 'Hi #field1#, you have a new #field2# notification. Please open EvibePro App to view details and reply . Thanks, Team Evibe',
		],

		'invibe_app' => [
			'otp' => 'Hi, Welcome to Invibe - simplest way to invite & manage your guests. Your OTP to signup is #field1#. Please do not share with anyone. Happy Invibing.'
		],

		'campaigns' => [
			'xmas2015'       => 'Hi #field1#, your X-Mas poster with Santa has been sent to #field2#. Share on Facebook & get a chance to win FREE candid photo shoot session. Incorrect email? Pls. email your phone number to ping@evibe.in',
			'invalid_email'  => 'Hi #field1#, your X-Mas poster with Santa is ready but we are unable to send as given email id is invalid. Pls write to ping@evibe.in.',
			'fb_contest'     => 'Hi #field1#, share your X-Mas poster on Facebook & get a chance to win FREE candid photo shoot session. Click "View & Download" button in your X-Mas poster email for more details.',
			'notify_tickets' => 'Hi #field1#, we are excited to invite you to meet our Live SANTA & get FREE X-Mas special poster. Pls check your email (#field2#) for details.'
		],

		'invite' => [
			'notify-card' => 'Hi #customerName#, your invite card is ready. Click #inviteLink# to download and share with your guests on WhatsApp/Messenger. Also share this link with you guests for RSVP. Evibe.in'
		]
	],

	/*
	|--------------------------------------------------------------------------
	| For Bookings
	|--------------------------------------------------------------------------
	*/
	'advance'               => [
		'unpaid'     => 0,
		'paid'       => 1,
		'percentage' => 50
	],

	/*
	|--------------------------------------------------------------------------
	| For Cakes
	|--------------------------------------------------------------------------
	*/
	'cake'                  => [
		'egg'     => 1,
		'eggless' => 2,
		'base_id' => 1506,
		'code'    => 'BLRCK'
	],

	/*
	|--------------------------------------------------------------------------
	| Cake price type
	|--------------------------------------------------------------------------
	*/
	'cake_price_type'       => [
		'fixed_price'  => 1,
		'per_kg_price' => 2,
	],

	'cake_cat_value' => [
		'weight'  => env('CAKE_CAT_VALUE_WEIGHT_ID', 1),
		'flavour' => env('CAKE_CAT_VALUE_FLAVOUR_ID', 2)
	],

	/*
	|--------------------------------------------------------------------------
	| Live Website Profile Urls
	|--------------------------------------------------------------------------
	|
	| Should be in sync with `evibe` config file in `EvibeMain` project
	|
	*/
	'live'           => [
		'host'        => env('LIVE_HOST', 'https://evibe.in'),
		'checkout'    => env('LIVE_CHECKOUT', 'https://evibe.in/pay/checkout'),
		'profile_url' => [
			'planner'           => 'birthday-party-planners',
			'venues'            => 'birthday-party-planners/venues',
			'packages'          => 'birthday-party-planners/packages',
			'experiences'       => 'special-couple-experiences',
			'trends'            => 'birthday-party-planners/trends',
			'cakes'             => 'birthday-party-planners/cakes',
			'decors'            => 'birthday-party-planners/birthday-party-decorations',
			'entertainment'     => 'birthday-party-planners/entertainment-activities-stalls',
			'bachelor'          => [
				'resorts'       => 'bachelor-party/resorts',
				'villas'        => 'bachelor-party/villas',
				'lounges'       => 'bachelor-party/lounges',
				'entertainment' => 'bachelor-party/entertainment',
				'food'          => 'bachelor-party/food',
				'cakes'         => 'bachelor-party/cakes',
			],
			'venue-deals'       => 'birthday-party-planners/venue-deals',
			'pre-post'          => [
				'venues'        => 'engagement-wedding-reception/venues',
				'decors'        => 'engagement-wedding-reception/decorations/all',
				'entertainment' => 'engagement-wedding-reception/entertainment',
				'cakes'         => 'engagement-wedding-reception/cakes'
			],
			'couple_experience' => [
				'packages' => 'special-couple-experiences/packages'
			],
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Codes
	|--------------------------------------------------------------------------
	*/
	'code'           => [
		'default' => [
			'venue'         => 'VDEF0001',
			'decor'         => 'DEC2350',
			'decor_gallery' => 'DS1970',
			'hall'          => 'HDEF0001',
		],
		'prefix'  => [
			'venue'         => 'V',
			'decor'         => 'DEC',
			'decor_gallery' => 'DS',
			'trend'         => 'TR',
			'services'      => 'SRV',
			'collection'    => 'COL',
			'package'       => 'PK',
			'hall'          => 'H',
		]
	],

	/*
	|--------------------------------------------------------------------------
	| URLs
	|--------------------------------------------------------------------------
	*/
	'urls'           => [
		'venue'  => '#field1#-#field2#-party-space-at-#field3#',
		'vendor' => '#field1#-#field2#-at-#field3#-#field4#',
		'hall'   => '#field1#-#field2#-for-#field3#-to-#field4#-at-#field5#',
	],

	/*
	|--------------------------------------------------------------------------
	| User Roles
	|--------------------------------------------------------------------------
	*/
	'roles'          => [
		'super_admin'      => env('ROLE_SUPER_ADMIN', 1),
		'admin'            => env('ROLE_ADMIN', 2),
		'planner'          => env('ROLE_PLANNER', 3),
		'venue'            => env('ROLE_VENUE', 4),
		'bd'               => env('ROLE_BD', 5),
		'customer_delight' => env('ROLE_CUSTOMER_DELIGHT', 6),
		'artist'           => env('ROLE_ARTIST', 7),
		'operations'       => env('ROLE_OPERATIONS', 8),
		'sr_crm'           => env('ROLE_SENIOR_CRM', 9),
		'invite_usr'       => env('ROLE_INVITE_USER', 10),
		'marketing'        => env('ROLE_MARKETING', 11),
		'tech'             => env('ROLE_TECH', 12),
		'customer'         => env('ROLE_CUSTOMER', 13),
		'sr_bd'            => env('ROLE_SENIOR_BD', 14),
		'syrow'            => env('ROLE_SYROW', 15),
		'ops_agent'        => env('ROLE_OPS_AGENT', 16),
		'ops_tl'           => env('ROLE_OPS_TL', 17),
		'bd_agent'         => env('ROLE_BD_AGENT', 18),
		'bd_tl'            => env('ROLE_BD_TL', 19),
		'accounts'         => env('ROLE_ACCOUNTS', 20),
	],

	/*
	|--------------------------------------------------------------------------
	| Validation Rules
	|--------------------------------------------------------------------------
	*/
	'validation'     => [

		'required' => [
			'id'    => 1,
			'rules' => 'required'
		],

		'numeric' => [
			'id'    => 2,
			'rules' => 'numeric'
		],

		'integer' => [
			'id'    => 3,
			'rules' => 'integer'
		],

		'min' => [
			'id'    => 4,
			'rules' => 'min:'
		],

		'max' => [
			'id'    => 5,
			'rules' => 'max:'
		],

		'phone' => [
			'id'    => 6,
			'rules' => 'integer:min:10'
		],
		'email' => [
			'id'    => 7,
			'rules' => 'email'
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Type input
	|--------------------------------------------------------------------------
	*/
	'input'          => [
		'text'     => 1,
		'number'   => 2,
		'textarea' => 3,
		'option'   => 4,
		'radio'    => 5,
		'checkbox' => 6
	],

	/*
	|--------------------------------------------------------------------------
	| Ticket Booking Type
	|--------------------------------------------------------------------------
	*/
	'type_booking'   => [
		'cakes' => env('TYPE_BOOKING_CAKE', 9)
	],

	'enquiry' => [
		'avl_check'    => 2,
		'custom_quote' => 4
	],

	'default-category-tag' => [
		env('TYPE_PAGE_DECOR', 13)  => env('ALL_DECORS_CATEGORY_ID', 323),
		env('TYPE_PAGE_CAKE', 6)    => env('ALL_CAKES_CATEGORY_ID', 324),
		env('TYPE_PAGE_SERVICE', 4) => env('ALL_SERVICES_CATEGORY_ID', 325),
		env('TYPE_PAGE_PACKAGE', 1) => env('ALL_KIDS_PACKAGES_CATEGORY_ID', 326),
		env('TYPE_PAGE_FOOD', 20)   => env('ALL_FOOD_PACKAGES_CATEGORY_ID', 327),
		env('TYPE_PAGE_TENT', 24)   => env('ALL_TENT_PACKAGES_CATEGORY_ID', 328),
		env('TYPE_PAGE_PRIEST', 23) => env('ALL_PRIEST_PACKAGES_CATEGORY_ID', 329),
		env('TYPE_PAGE_VILLA', 18)  => env('ALL_VILLA_PACKAGES_CATEGORY_ID', 330),
		env('TYPE_PAGE_CE', 21)     => env('ALL_SURPRISE_PACKAGES_CATEGORY_ID', 331),
		env('TYPE_PAGE_TREND', 5)   => env('ALL_TRENDS_CATEGORY_ID', 332)
	],

	/*
	|--------------------------------------------------------------------------
	| APIs
	|--------------------------------------------------------------------------
	*/
	'api'                  => [
		'base_url'        => env('API_BASE_URL', 'http://apis.evibe.in/'),
		'client_id'       => env('API_CLIENT_ID'),
		'partner'         => [
			'prefix' => 'partner/v1/',
		],
		'dashBoard'       => [
			'prefix' => 'common/tickets/v1/'
		],
		'tickets'         => [
			'update' => 'tickets/update'
		],
		'auto-followups'  => [
			'prefix' => 'common/auto-followups/v1/'
		],
		'finance'         => [
			'prefix'              => 'common/finance/v1',
			'settlements'         => [
				'prefix' => 'common/finance/v1/settlements'
			],
			'quick-settlements'   => [
				'prefix' => 'common/finance/v1/quick-settlements'
			],
			'adjustments'         => [
				'prefix' => 'common/finance/v1/adjustments'
			],
			'booking-settlements' => [
				'prefix' => 'common/finance/v1/bookings'
			],
			'refunds'             => [
				'prefix' => 'common/finance/v1/refunds/bookings' // @see: bookings is included
			],
			'cancellations'       => [
				'prefix' => 'common/finance/v1/cancellations/bookings' // @see: bookings is included
			]
		],
		'avail-check'     => [
			'prefix' => 'common/partner/avail-check'
		],
		'ops'             => [
			'dashboard' => [
				'tabs-count'          => 'ops/dashboard/tabs-count',
				'order-confirmations' => [
					'list'            => 'ops/dashboard/order-confirmations',
					'confirm-partner' => 'ops/dashboard/order-confirmations/confirm-partner',
					'search'          => 'ops/dashboard/order-confirmations/search'
				],
				'customer-updates'    => 'ops/dashboard/customer-updates',
				'customer-reviews'    => [
					'list'    => 'ops/dashboard/customer-reviews',
					'resolve' => 'ops/dashboard/customer-reviews-resolve'
				],
				'customer-proofs'     => [
					'list'     => 'ops/dashboard/customer-proofs',
					'search'   => 'ops/dashboard/customer-proofs/search',
					'validate' => 'ops/dashboard/customer-proofs/validate'
				]
			],
		],
		'piab'            => [
			'dashboard' => [
				'product-bookings' => [
					'list'          => 'piab/dashboard/product-bookings',
					'info'          => 'piab/dashboard/product-bookings/info',
					'update-status' => 'piab/dashboard/product-bookings/update-status',
				],
			],
		],
		'reviews-mapping' => [
			'mapped-reviews'  => 'reviews-mapping/mapped-reviews',
			'pending-reviews' => 'reviews-mapping/pending-reviews',
			'save'            => 'reviews-mapping/save',
			'get-options'     => 'reviews-mapping/get-options',
		],

		'pay_notification' => 'payment/notification/'
	],

	'google' => [
		'shortener_key' => env('GOOGLE_SHORTENER_KEY'),
		'places_key'    => env('GOOGLE_PLACES_KEY')
	],

	'bitly' => [
		'access_token'     => env('BITLY_ACCESS_TOKEN'),
		'alt_access_token' => env('BITLY_ALT_ACCESS_TOKEN'),
	],

	'evibes'          => [
		'access_token' => env('EVIBES_SHORTEN_ACCESS_TOKEN', "11feae8ebff7ca417beab3586e0f8a")
	],

	/*
	|--------------------------------------------------------------------------
	| OneSignal - Web Push Notifications
	|--------------------------------------------------------------------------
	*/
	'onesignal'       => [
		'appId'         => env('ONESIGNAL_APP_ID', '2fddf801-5723-4d68-bec4-8db284bd7c1c'),
		'subdomainName' => env('ONESIGNAL_SUBDOMAIN_NAME', 'evibe-dash'),
		'apiKey'        => env('ONESIGNAL_API_KEY', 'MzAyYTc2NWUtNGY4My00YWQ3LTkzOWYtMjk1Y2FlNWI5YzJi'),
	],

	/*
	|----------------------------------------------------------------------------
	| Partner app key, being used for app notification for partner application
	|----------------------------------------------------------------------------
	*/
	'partner_app_key' => [
		'enquiry'  => [
			'list'    => 'enquiry_list',
			'details' => 'enquiry_details'
		],
		'order'    => [
			'list'    => 'order_list',
			'details' => 'order_details'
		],
		'delivery' => [
			'list'    => 'delivery_list',
			'details' => 'delivery_details'
		],
		'apiKey'   => env('ONESIGNAL_API_KEY', 'MzAyYTc2NWUtNGY4My00YWQ3LTkzOWYtMjk1Y2FlNWI5YzJi')
	],

	'checkout_field' => [
		'cake' => [
			'message'         => env('CHECKOUT_CAKE_MESSAGE', 1),
			'delivery_slot'   => env('CHECKOUT_CAKE_DELIVERY_SLOT', 155),
			'delivery_charge' => env('CHECKOUT_CAKE_DELIVERY_CHARGE', 156),
			'type'            => env('CHECKOUT_CAKE_TYPE', 21),
			'weight'          => env('CHECKOUT_CAKE_WEIGHT', 22),
			'flavour'         => env('CHECKOUT_CAKE_FLAVOUR', 23)
		],
	],

	'project_id' => env('PROJECT_ID_DASH', 2),

	'email_salutation' => 'Thanks in advance,',

	'why_choose_evibe' => env('WHY_CHOOSE_EVIBE', 1),

	/*
	|--------------------------------------------------------------------------
	| Cities (Must be in sync with 'city' table)
	|--------------------------------------------------------------------------
	*/
	'city'             => [
		'Bengaluru'    => env('CITY_BANGALORE_ID', 1),
		'Hyderabad'    => env('CITY_HYDERABAD_ID', 2),
		'Secunderabad' => env('CITY_HYDERABAD_ID', 2),
	],

	/*
	 * Order status ids for order tracking
	 */
	'order_status'     => [
		'not_started' => 1,
		'in_progress' => 2,
		'tracked'     => 3
	],

	'lead_status' => [
		'unmapped' => 0,
		'hot'      => 1,
		'medium'   => 2,
		'cold'     => 3
	],

	'short_name'       => [
		'1'  => "IN",
		'2'  => "IP",
		'3'  => "OC",
		'4'  => "BK",
		'5'  => "CA",
		'6'  => "JE",
		'7'  => "FU",
		'8'  => "AP",
		'9'  => "AC",
		'10' => "RJ",
		'11' => "RD",
		'12' => "SA"
	],

	/*
	|--------------------------------------------------------------------------
	| EventId - name (should match 'type_event')
	|--------------------------------------------------------------------------
	*/
	'type-event-title' => [
		env('TYPE_EVENT_BIRTHDAY', 1)       => 'Birthday Party',
		env('TYPE_EVENT_BACHELOR', 11)      => 'Bachelor Party',
		env('TYPE_EVENT_PRE_POST', 13)      => 'Engagement + Reception',
		env('TYPE_EVENT_SURPRISES', 16)     => 'Surprises',
		env('TYPE_EVENT_HOUSE_WARMING', 14) => 'House Warming',
	],

	'type_file' => [
		'image' => 1,
		'pdf'   => 2
	],

	'partner'       => [
		'package_status' => [
			'submitted' => 2,
			'rejected'  => 3
		],

		'discussion' => [
			1 => "Gallery images quality is not good",
			2 => "Gallery images are not matching with description provided",
			3 => "More Gallery images needed to understand the package",
			4 => "Price is too high",
			5 => "Similar package already exists",
			6 => "Inclusions are not upto the mark"
		]
	],

	/*
	 * tag ids for surprise tags
	 */
	'tags'          => [
		'surprise_relation'   => env('SURPRISE_RELATIONSHIP_PARENT_ID'),
		'surprise_occasion'   => env('SURPRISE_OCCASION_PARENT_ID'),
		'surprise_age_group'  => env('SURPRISE_AGE_GROUP_PARENT_ID'),
		'valentines-day-2020' => env('TYPE_TAG_VALENTINES_DAY_2020', 262)
	],

	/*
	 * Type Reminder
	 */
	'type_reminder' => [
		'followup' => env('TYPE_REMINDER_FOLLOWUP', 1),
	],

	'test'    => [
		'reminders' => [
			'followup' => [
				'rr1'  => 'Hi #customerName#, here are the best recommendations for your party: #recoLink#. Please check and submit your shortlist. Team Evibe.in',
				'rr2'  => 'Hi #customerName#, have you checked handpicked recommendations for your party on #partyDate#? Click #recoLink# to check and submit your shortlist. Need help? Pls. call #evibePhone#. Team Evibe.',
				'rr3'  => 'Hi #customerName#, we did not get your shortlist yet. Pls. check top recommendations for your party here: #recoLink# and submit your shortlist for best deals. Team Evibe.in',
				'rr4'  => 'Hi #customerName#, your party is just in #partyDaysCount# days. Pls. check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#. Team Evibe.',
				'rr5'  => 'Hi #customerName#, we recommended top options for your party. Pls. select options you liked and submit your shortlist here: #recoLink#. Need help? Call #evibePhone#. Did not like anything? Click #notLikeLink#.',
				'rr6'  => 'Hi #customerName#, your party is just in #partyDaysCount# days, let Evibe.in: No. 1 party planners, help you host a memorable party. Submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.',
				'rr7'  => 'Hi #customerName#, last minute offer for your #eventName# party #partyDateString#. Get #discountPrice#* OFF valid only #couponValidString#. Use coupon code #couponCode#. Need help? Call #evibePhone#. Team Evibe.',
				'rr8'  => 'Hi #customerName#, at Evibe.in, you get best price and service for your party. Pls. check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.',
				'rr9'  => 'Hi #customerName#, at Evibe.in, you get 100% service delivery guarantee for your party. Pls check top recommendations and submit your shortlist here: #recoLink#  to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.',
				'rr10' => 'Hi #customerName#, at Evibe.in, you get best prices and 100% service delivery guarantee for your party. Pls check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.',
				'rr11' => 'Hi #customerName#, book your party hasslefree at Evibe.in: No. 1 party planners. Pls check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.'
			]
		]
	],

	/*
	 * selling status default state is medium
	 */
	'selling' => [
		'status' => [
			'default' => env('SELLING_STATUS_DEFAULT_ID', 2)
		]
	],

	/*
	 * Category id for the parent entertainment tag
	 */
	'service' => [
		'parentCategoryId' => env('SERVICE_PARENT_CATEGORY_ID')
	],

	'reference' => [
		'finance' => [
			'settlements'         => 'SETTLE',
			'partner-adjustments' => 'PART-ADJ',
			'customer-refunds'    => 'CUST-REFU',
			'quick-settlements'   => 'QUICK-SETTLE'
		],
		'add-on'  => [
			'details'         => 'ADD-ON-DETAILS',
			'option-mappings' => 'ADD-ON-OPTION_MAPPINGS'
		],
		'audit'   => [
			'edit-new'      => 'AUDIT-EDIT-NEW',
			'edit-pending'  => 'AUDIT-EDIT-PENDING',
			'edit-rejected' => 'AUDIT-EDIT-REJECTED',
			'review'        => 'AUDIT-REVIEW'
		]
	],

	/*
	 * Default values
	 */
	'default'   => [
		'access_token_handler' => env('DEFAULT_ACCESS_TOKEN_HANDLER', 2),
		'evibe_service_fee'    => 0.15,
		'gst'                  => 0.18,
		'partial_gst'          => 0.09,
	],

	'bookingLikeliness' => [
		"1" => "Immediately",
		"2" => "In a day",
		"6" => "In 3 days",
		"3" => "In a week",
		"4" => "In a month",
		"5" => "Just started planning"
	],

	/*
	 * Error codes to report errors to tech, admin
	 */
	'error_code'        => [
		'partner_update'            => 11,
		'update_settlement_booking' => 12,
		'delete_settlement_booking' => 13,
		'create_function'           => 21,
		'fetch_function'            => 22,
		'update_function'           => 23,
		'delete_function'           => 24,
	],

	/*
	 * Type fields to determine input format
	 */
	'type_field'        => [
		'text'     => env('TYPE_FIELD_TEXT', 1),
		'number'   => env('TYPE_FIELD_NUMBER', 2),
		'textarea' => env('TYPE_FIELD_TEXTAREA', 3),
		'option'   => env('TYPE_FIELD_OPTION', 4),
		'radio'    => env('TYPE_FIELD_RADIO', 5),
		'checkbox' => env('TYPE_FIELD_CHECKBOX', 6),
	],

	'type_customer_gender' => [
		'1' => 'Mr',
		'2' => 'Ms'
	],

	'type_event_slot' => [
		'1' => 'Breakfast - 7Am',
		'2' => 'Lunch - 11Am',
		'3' => 'HiTea - 3Pm',
		'4' => 'Dinner - 7Pm',
	],

	'type_customer_budget' => [
		'0',
		'1000',
		'3000',
		'5000',
		'10000',
		'15000',
		'20000',
		'25000',
		'50000',
		'75000',
		'100000'
	],

	'type_customer_relation' => [
		'1' => 'Self',
		'2' => 'Spouse',
		'3' => 'Family',
		'4' => 'Client',
		'5' => 'Friend'
	],

	'customer_preferred_slot' => [
		'Within 1 Hour',
		'10 AM - 1 PM',
		'1 PM - 3 PM',
		'3 PM - 6 PM',
		'6 PM - 8 PM'
	],

	'downTimeInMin' => env('SITE_DOWNTIME_MIN', 5),

	'type_followup' => [
		'1' => 'Callback',
		'2' => 'Reco Check',
		'3' => 'Avail Check',
		'4' => 'Decision Making',
		'5' => 'Conversion',
		'6' => 'Payment',
		'7' => 'For Addressing'
	],

	'whatsapp_messages' => [
		'followup'    => [
			1 => [
				'nonPhone' => 'Hi #CUSTOMER#, I’m #HANDLER# from Evibe.in. We received your enquiry regarding #category_name# for #party_date#. As discussed, I called you back but could not connect. Please call back, would be more than happy to help.',
				'phone'    => 'Hello, I’m #HANDLER# from Evibe.in. We received your enquiry on our website [www.evibe.in]. As discussed, I called you back but could not connect. Please call back, would be more than happy to help.'
			],
			2 => 'Hi #CUSTOMER#, I’m #HANDLER# from Evibe.in. Based on your requirements, We’ve sent the best matching recommendations. Please click here: #SHORTLIST_LINK# to check them. Click on “Shortlist” button for all the options you like and then click on “Submit” after you are done to let me know your choice to take this ahead. Look forward to receive your shortlist soon.',
			3 => 'Hi #CUSTOMER#, I’m #HANDLER# from Evibe.in. I’m happy to share the services are available and here’s the best quote:',
			4 => 'Hi #CUSTOMER#, I’m #HANDLER# from Evibe.in. We hope you liked our quote and discussed with your #decision_maker_value#. Please share the best time slot to call you and finalise the party plan.',
			5 => 'Hi #CUSTOMER#, I’m #HANDLER# from Evibe.in. We hope you liked our quote and services extended from our team. Please share the best time slot to call you and finalise the party plan.',
			6 => 'Hi #CUSTOMER#, I’m #HANDLER# from Evibe.in. I’m happy to share that all the requested party services are available for your party on #party_date#. Please use this link #SHORT_PAYMENT_LINK# to pay the advance by #payment_deadline# to complete the booking.'
		],
		'noResponse'  => [
			'nonPhone' => 'Hi #CUSTOMER#, We are from Evibe.in. We tried reaching you for your party enquiry but could not connect. I hope your enquiry is still open. Please call me back, would be more than happy to help you.',
			'phone'    => 'Hi #CUSTOMER#, We are from Evibe.in. We tried reaching you for your party enquiry but could not connect. I hope your enquiry is still open. Please call me back, would be more than happy to help you.'
		],
		'other'       => "Hi #CUSTOMER#, I’m #HANDLER# from Evibe.in.",
		'paymentLink' => "Hi #CUSTOMER#, I’m #HANDLER# from Evibe.in. I’m happy to share that all the requested party services are available for your party on #party_date#. Please use this link #SHORT_PAYMENT_LINK# to pay the advance by #payment_deadline# to complete the booking."
	],

	'ticket_type_id' => [
		env('TYPE_PAGE_PACKAGE', 1) => "Package",
		env('TYPE_PAGE_TREND', 5)   => "Trend",
		env('TYPE_PAGE_CAKE', 6)    => "Cake",
		env('TYPE_PAGE_DECOR', 13)  => "Decoration",
		env('TYPE_PAGE_ENT', 14)    => "Service",
		env('TYPE_PAGE_FOOD', 20)   => "Food",
		env('TYPE_PAGE_PRIEST', 23) => "Priest",
		env('TYPE_PAGE_TENT', 24)   => "Tent"
	],

	'default_partner_id' => env("DEFAULT_PARTNER_ID", 809),

	'price_filter' => [
		'1' => [
			'min' => 0,
			'max' => 100
		],
		'2' => [
			'min' => 100,
			'max' => 200
		],
		'3' => [
			'min' => 200,
			'max' => 500
		],
		'4' => [
			'min' => 500,
			'max' => 1000
		],
		'5' => [
			'min' => 1000,
			'max' => 2000
		],
		'6' => [
			'min' => 2000,
			'max' => 5000
		],
		'7' => [
			'min' => 5000,
			'max' => 8000
		],
		'8' => [
			'min' => 8000,
			'max' => 10000
		],
		'9' => [
			'min' => 10000,
			'max' => 0
		],
	],

	'avail_check'            => [
		'slots' => [
			1 => "All Day",
			2 => "BreakFast",
			3 => "Lunch",
			4 => "Hi Tea",
			5 => "Dinner"
		],

		"ids" => [
			"all"       => 1,
			"breakfast" => 2,
			"lunch"     => 3,
			"hi_tea"    => 4,
			"dinner"    => 5
		]
	],

	/*
	 * PIAB booking status
	 */
	'product-booking-status' => [
		'initiated'        => env('PRODUCT_BOOKING_STATUS_INITIATED', 1),
		'pain'             => env('PRODUCT_BOOKING_STATUS_PAID', 2),
		'accepted'         => env('PRODUCT_BOOKING_STATUS_ACCEPTED', 3),
		'shipped'          => env('PRODUCT_BOOKING_STATUS_SHIPPED', 4),
		'in-transit'       => env('PRODUCT_BOOKING_STATUS_IN_TRANSIT', 5),
		'out-for-delivery' => env('PRODUCT_BOOKING_STATUS_OUT_FOR_DELIVERY', 6),
		'delivered'        => env('PRODUCT_BOOKING_STATUS_DELIVERED', 7),
		'cancelled'        => env('PRODUCT_BOOKING_STATUS_CANCELLED', 8)
	]
];
