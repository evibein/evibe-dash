<?php
/**
 * PayUMoney options
 *
 * @author Anji <anji@evibe.in>
 * @since  28 Apr 2015
 */

return [

	'end_point'   => 'https://secure.payu.in/_payment',

	'salt'        => 'kdiruBMd',

	'key'         => '2LEHxq',

	'merchant_id' => '5121550',

	/**
	 * @author: Vikash
	 * @since 28 Dec 2015
	 *
	 * Commenting surl, furl and using direct routes in PayUController
	 * Issue with using 'route' method in Config file since Laravel 5.2
	 */
	// 'surl' => returnRoute('payu.success'),

	//'furl' => route('payu.fail')

];