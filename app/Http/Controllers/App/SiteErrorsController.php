<?php namespace App\Http\Controllers\App;

use App\Http\Controllers\Base\BaseController;
use App\Models\Util\TypeProject;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use SiteErrors;

class SiteErrorsController extends BaseController
{

	/**
	 * Display a listing of the resource.
	 * GET /errors
	 */
	public function showErrors()
	{
		$siteErrors = SiteErrors::orderBy('id', 'DESC');

		$projects = TypeProject::all();
		$validProjectIds = $projects->pluck('id')->toArray();

		$startDate = Input::get('startDate');
		$endDate = Input::get('endDate');
		$oneDay = 24 * 60 * 60 - 1;
		$formattedStartDate = $startDate ? date("Y-m-d H:i:s", strtotime($startDate)) : null;
		$formattedEndDate = $endDate ? date("Y-m-d H:i:s", strtotime($endDate) + $oneDay) : date("Y-m-d H:i:s", Carbon::now()->timestamp + $oneDay);

		if ($formattedStartDate && $formattedEndDate)
		{
			$rules = [
				'startDate' => 'date|required_with:endDate',
				'endDate'   => 'date'
			];

			$messages = [
				'startDate.required'      => 'From date is required',
				'startDate.required_with' => '<b>From date</b> is required with <b>To date</b> ',
				'startDate.date'          => 'From date is invalid',
				'endDate.date'            => 'To date is invalid'
			];

			if ($formattedStartDate == $formattedEndDate)
			{
				$formattedStartDate = date("Y-m-d H:i:s", strtotime($startDate));
				$formattedEndDate = date("Y-m-d H:i:s", strtotime($endDate) + ($oneDay));
			}

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails())
			{
				return redirect()->back()
				                 ->withErrors($validator->messages())
				                 ->withInput();
			}

			$siteErrors->whereBetween('created_at', [$formattedStartDate, $formattedEndDate]);

		}

		$projectId = Input::has('project_id') ? Input::get('project_id') : false;
		if ($projectId && in_array($projectId, $validProjectIds))
		{
			$siteErrors->where('project_id', $projectId);
		}

		$data = [
			'siteErrors' => $siteErrors->paginate(20),
			'projects'   => $projects
		];

		return view('site-errors.view', ['data' => $data]);
	}

	public function deleteErrors()
	{
		$response = ['success' => false];
		$oneDay = 24 * 60 * 60 - 1;
		$startDate = Input::get('startDate');
		$endDate = Input::get('endDate');
		$projectId = Input::get('projectId') && Input::get('projectId') > 0 ? Input::get('projectId') : false;

		$formattedStartDate = $startDate ? date("Y-m-d H:i:s", strtotime($startDate)) : null;
		$formattedEndDate = $endDate ? date("Y-m-d H:i:s", strtotime($endDate) + $oneDay) : date("Y-m-d H:i:s", Carbon::now()->timestamp + $oneDay);

		if ($formattedStartDate && $formattedEndDate)
		{
			$siteError = SiteErrors::whereBetween('created_at', [$formattedStartDate, $formattedEndDate]);
			if ($projectId)
			{
				$siteError = $siteError->where('project_id', $projectId);
			}
			$siteError->delete();

			$response = ['success' => true];
		}
		if (!$startDate && !$endDate)
		{
			if ($projectId)
			{
				SiteErrors::where('project_id', $projectId)->delete();
			}
			else
			{
				SiteErrors::truncate();
			}
			$response = ['success' => true];

		}

		return response()->json($response);
	}

	public function individualDelete($id)
	{
		$error = SiteErrors::find($id);

		if ($error)
		{
			$error->forceDelete();

			return redirect()->back();
		}
	}

}