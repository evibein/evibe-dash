<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Base\BaseController;
use App\Models\InternalQuestion;
use App\Models\PostalPinCode;
use Area;
use Carbon\Carbon;
use Evibe\Facades\AppUtilFacade as AppUtil;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use Illuminate\Support\Facades\Log;
class AppController extends BaseController
{
	public function watermarkImage()
	{
		$option = ['isRotate' => Input::get('is_rotate')];
		AppUtil::watermarkImage(Input::get('path'), $option);
	}

	public function authenticate()
	{
		$roleId = Auth::user()->role_id;

		// Role ids are as per database
		// ensure it is in sync with 'role' table in database
		$ticketsPageLandingRoleIds = [config('evibe.roles.super_admin'),
			config('evibe.roles.admin'),
			config('evibe.roles.customer_delight'),
			config('evibe.roles.sr_crm'),
			config('evibe.roles.ops_agent'),
			config('evibe.roles.ops_tl'),
			config('evibe.roles.bd_tl')
		];

		if (in_array($roleId, $ticketsPageLandingRoleIds))
		{
			return redirect('/tickets');
		}
		//Marketing
		elseif (in_array($roleId, [config('evibe.roles.marketing')]))
		{
			return redirect()->route('stories.customer.list');
		}
		// 5: business developer, 14: Sr.BD
		else
		{
			if (in_array($roleId, [config('evibe.roles.bd'), config('evibe.roles.sr_bd')]))
			{
				return redirect('/venues');
			}
			elseif (in_array($roleId, [config('evibe.roles.bd_agent')]))
			{
				return redirect('/decors');
			}

			// restricted access
			else
			{
				Log:info("reached");
				return redirect()->route('login')->with('auth_error', 'Not an authorised user')->withInput();
			}
		}
	}

	public function showLogin()
	{
		return view('auth.login');
	}

	public function doLogout()
	{
		Auth::logout();
		Session::flush();

		return redirect()->route('login');
	}

	public function addNewArea()
	{
		$rules = [
			'cityId'   => 'required',
			'pinCode'  => 'required|max:6',
			'areaName' => 'required',
		];

		$messages = [
			'cityId.required'   => 'Please choose a city',
			'pinCode.required'  => 'Please enter valid pincode',
			'areaName.required' => 'Please enter an area'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'msg'     => $validator->messages()->first()
			                        ]);
		}

		try
		{
			$inputCityId = Input::get('cityId');
			$locationDetails = Input::get('locationDetails');
			$pinCode = Input::get('pinCode');
			$locationArray = explode(', ', Input::get('areaName'));
			$areaName = $locationArray[0];
			$areaUrl = str_replace(" ", "-", strtolower($areaName));
			$cityId = null;
			$postalPinCode = PostalPinCode::where('pin_code', $pinCode)->first();
			if (!$postalPinCode)
			{
				return response()->json(['success' => false, 'msg' => 'Please enter a valid pincode']);
			}

			if ($locationDetails)
			{
				$locationArray = json_decode($locationDetails);
				foreach ($locationArray as $locationComponent)
				{
					if ($locationComponent->types[0] == 'locality')
					{
						$cityId = config("evibe.city.$locationComponent->long_name");
					}
					if ($locationComponent->types[0] == 'postal_code')
					{
						$pinCode = $locationComponent->long_name;
					}
				}

				if ($cityId && $inputCityId != $cityId)
				{
					return response()->json(['success' => false, 'msg' => 'Please select the correct city']);
				}
				elseif (!$cityId)
				{
					$existingArea = Area::where([
						                            'zip_code' => $pinCode,
						                            'city_id'  => $inputCityId,
						                            'name'     => $areaName
					                            ])->first();

					if ($existingArea)
					{
						return response()->json(['success' => false, 'msg' => 'Area already exists']);
					}

					$area = Area::create([
						                     'name'       => $areaName,
						                     'city_id'    => $inputCityId,
						                     'url'        => $areaUrl,
						                     'zip_code'   => $pinCode,
						                     'created_at' => Carbon::now(),
						                     'updated_at' => Carbon::now(),
					                     ]);
					if ($area)
					{
						return response()->json(['success' => true]);
					}

					return response()->json(['success' => false, 'msg' => 'Please enter a google suggested area']);
				}

				if ($cityId && $areaName)
				{
					$area = Area::where('city_id', $cityId)
					            ->where('name', $areaName)
					            ->orderBy('updated_at', 'DESC')
					            ->first();

					if ($area)
					{
						if (!$area->zip_code && $pinCode)
						{
							$area->zip_code = $pinCode;
							$area->save();
						}

						return response()->json(['success' => true]);
					}
					else
					{
						$newArea = Area::create([
							                        'name'       => $areaName,
							                        'url'        => $areaUrl,
							                        'zip_code'   => $pinCode,
							                        'city_id'    => $cityId,
							                        'created_at' => Carbon::now(),
							                        'updated_at' => Carbon::now(),
						                        ]);
						if ($newArea)
						{
							return response()->json(['success' => true]);
						}
					}
				}
			}

			return response()->json(['success' => false, 'msg' => 'Please enter a google suggested area']);
		} catch (Exception $e)
		{
			return response()->json(['error' => true]);
		}
	}

	/*
	* @author Vikash <vikash@evibe.in>
	* @Since 1 February 2016
	*
	* Save, edit and delete given internal questions (IQ)
	*/
	public function saveIq($mapId, $mapTypeId)
	{
		$question = Input::get('question');
		$answer = Input::get('answer');

		InternalQuestion::create([

			                         'question'    => $question,
			                         'answer'      => $answer,
			                         'user_id'     => Auth::user()->id,
			                         'map_id'      => $mapId,
			                         'map_type_id' => $mapTypeId
		                         ]);

		$res = ['success' => true];

		return response()->json($res);
	}

	public function deleteIq($id)
	{
		$iq = InternalQuestion::find($id);

		$iq->deleted_by = Auth::user()->id;
		$iq->save();

		if ($iq->delete())
		{
			return redirect()->back();
		}
	}

	public function editIq()
	{
		$question = Input::get('editQuestion');
		$answer = Input::get('editAnswer');

		InternalQuestion::where('id', Input::get('id'))
		                ->update([
			                         'question' => $question,
			                         'answer'   => $answer,
		                         ]);

		$res = ['success' => true];

		return response()->json($res);
	}
}
