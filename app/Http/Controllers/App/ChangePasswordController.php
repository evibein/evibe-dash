<?php namespace App\Http\Controllers\App;

use App\Http\Controllers\Base\BaseContentController;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ChangePasswordController extends BaseContentController
{

	// show change password screen
	public function showChangeScreen()
	{
		return view('app/planner/changepwd');
	}

	// update password
	public function updatePassword()
	{
		// validator rules
		$rules = [
			'old_pwd'              => 'required|min:6',
			'new_pwd'              => 'required|min:6|confirmed',
			'new_pwd_confirmation' => 'required|min:6'
		];

		$messages = [
			'old_pwd.required'              => 'Old password is required',
			'new_pwd.required'              => 'New password is required',
			'new_pwd_confirmation.required' => 'Repeat password is required',
			'new_pwd.confirmed'             => 'New password and repeat password do not match',
			'old_pwd.min'                   => 'Password should be atleast 6 characters long',
			'new_pwd.min'                   => 'Password should be atleast 6 characters long',
			'new_pwd_confirmation.min'      => 'Password should be atleast 6 characters long'
		];

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			return redirect('change-password')->withErrors($validation)->withInput();
		}

		$userId = Session::get('user_id');
		$user = User::find($userId);
		$existingPassword = $user->password;
		$oldPasswordInput = Input::get('old_pwd');
		$newPasswordInput = Input::get('new_pwd');

		// check with old password
		if (Hash::check($oldPasswordInput, $existingPassword))
		{
			$user->password = Hash::make($newPasswordInput);
			$user->save();

			return redirect('change-password')->with('update_success', 'Password updated succesfully');
		}
		else
		{
			return redirect('change-password')->with('custom_error', 'Old password is not valid');
		}
	}

}
