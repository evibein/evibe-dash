<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Base\BaseContentController;
use Evibe\Facades\AppUtilFacade as AppUtil;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class LoginController extends BaseContentController
{
	public function __construct()
	{
		// $this->middleware('guest.custom', ['except' => 'logout']);
	}

	public function postLogin(Request $request)
	{
		
		// validators
		$rules = [
			'username' => 'required|email',
			'password' => 'required|min:6'
		];

		$credentials = [
			'username' => $request->input('username'),
			'password' => $request->input('password')
		];

		$validation = Validator::make($credentials, $rules);

		// check for validation
		if ($validation->fails())
		{
			return redirect()->route('login')->withErrors($validation)->withInput();
		}
		$isRemember = request()->input('rememberme') == 'on' ? true : false;
		if (auth()->attempt($credentials, $isRemember))
		{
			$roleId = auth()->user()->role_id;
			AppUtil::setSessions();
			// Role ids are as per database
			// ensure it is in sync with 'role' table in database
			if (in_array($roleId, [config('evibe.roles.super_admin')]))
			{
				return redirect()->route('analytics.cumulative');
			}
			elseif (in_array($roleId, [config('evibe.roles.customer_delight'), config('evibe.roles.sr_crm'), config('evibe.roles.ops_agent'), config('evibe.roles.ops_tl'), config('evibe.roles.bd_tl')]))
			{
				return redirect()->route('ticket.list');
			}
			elseif (in_array($roleId, [config('evibe.roles.bd'), config('evibe.roles.sr_bd')]))
			{
				return redirect()->route('availability.venue.live'); // 5: business developer
			}
			elseif (in_array($roleId, [config('evibe.roles.marketing')]))
			{
				return redirect()->route('stories.customer.list'); // 10: Marketing team
			}
			elseif (in_array($roleId, [config('evibe.roles.tech')]))
			{
				return redirect(route("monitor.payments"));
			}
			elseif (in_array($roleId, [config('evibe.roles.operations'), config('evibe.roles.bd_agent')]))
			{
				return redirect('/decors'); // 8: operation team
			}
			elseif (in_array($roleId, [config("evibe.roles.syrow")]))
			{
				return redirect('/s/tickets/list');
			}
			else
			{
				// restricted access
				return redirect()->route('login')
				                 ->with('auth_error', 'Not an authorised user')
				                 ->withInput();
			}
		}
		// authentication failed
		return redirect('login')->with('auth_error', 'Authentication failed')->withInput();
	}
}
