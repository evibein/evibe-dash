<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Base\BaseController;
use App\Models\Payment\PaymentTransactionAttempts;
use Carbon\Carbon;
use Ticket;

class MonitoringController extends BaseController
{

	/**
	 * Showing the day analysis of the payments
	 */
	public function showPayments()
	{
		$tickets = "";
		$startDay = !is_null(request('startTime')) ? request('startTime') : Carbon::today()->startOfDay()->subHours(4);
		$endDay = !is_null(request('endTime')) ? request('endTime') : Carbon::today()->endOfDay()->subHours(4);

		$paymentTransactions = PaymentTransactionAttempts::orderBy("created_at", "desc")
		                                                 ->whereBetween("created_at", [$startDay, $endDay]);

		if (request("id") > 0)
		{
			$paymentTransactions = $paymentTransactions->where("ticket_id", request("id"));
		}

		$paymentTransactions = $paymentTransactions->paginate(100);
		if ($paymentTransactions->count() > 0)
		{
			$tickets = Ticket::whereIn("id", $paymentTransactions->pluck("ticket_id")->toArray());
		}

		$data = [
			'transactions' => $paymentTransactions,
			'tickets'      => $tickets,
			'startTime'    => $startDay,
			'endTime'      => $endDay
		];

		return view('monitor.payments', ['data' => $data]);
	}
}