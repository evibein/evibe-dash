<?php

namespace App\Http\Controllers\DashBoard;

use App\Http\Controllers\Base\BaseController;
use App\Models\TypeEvent;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CrmDashBoardController extends BaseController
{
	public function showNewTickets()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/new",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/new_tickets', ['data' => $data]);
	}

	public function showInProgressTickets()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/progress",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/new_tickets', ['data' => $data]);
	}

	public function showReturnTickets()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/return",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/return_tickets', ['data' => $data]);
	}

	public function showMissedTickets()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/missed",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/missed_tickets', ['data' => $data]);
	}

	public function showProspectTickets()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/prospect",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/prospect_tickets', ['data' => $data]);
	}

	public function showReturnTicketUpdates($ticketId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/updates/" . $ticketId,
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/update_modal', ["data" => $data]);
	}

	public function showHistory()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/history",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/history', ['data' => $data]);
	}

	public function showHistoryUpdates($ticketId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/history/" . $ticketId,
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/update_modal', ["data" => $data]);
	}

	public function showMetrics(Request $request)
	{
		$showFilterReset = false;
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));
		$dataFromBaseFunction = $this->baseFunctionForMetricsAndAnalytics();

		$tickets = $dataFromBaseFunction['tickets'];

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$tickets = $tickets->where('ticket.created_at', '<=', $dateRequirements['filterChosenEndTime'])
		                   ->where('ticket.created_at', '>=', $dateRequirements['filterChosenStartTime'])
		                   ->where('ticket.status_id', '!=', config('evibe.status.duplicate'))
		                   ->where('ticket.status_id', '!=', config('evibe.status.related'))
		                   ->where('ticket.status_id', '!=', config('evibe.status.irrelevant'))
		                   ->get();

		$totalOrderProcessed = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.status.confirmed') &&
				$item->success_email_type == 1 &&
				$item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$totalTicketBookings = \TicketBooking::select('id')
		                                     ->whereIn('ticket_id', $tickets->pluck("id"))
		                                     ->whereNull('deleted_at');

		$totalNoOfBookings = $totalTicketBookings->count();

		$recommendationsCount = \TicketMapping::where('recommended_at', '<=', $dateRequirements['filterChosenEndTime'])
		                                      ->where('recommended_at', '>=', $dateRequirements['filterChosenStartTime'])
		                                      ->whereIn('ticket_id', $tickets->pluck('id'))
		                                      ->whereNull('deleted_at')
		                                      ->count();
		$conversionRate = 0;

		// handling 0 tickets exception
		if ($tickets->count() > 0)
		{
			$conversionRate = ($totalTicketBookings->count() / $tickets->count()) * 100;
		}

		$countOfCustomersWhoGaveRatingForRecos = $tickets->filter(function ($item) {
			return $item->last_reco_rating != null;
		})->count();

		$cumulativeRecommendationsRating = $tickets->sum('last_reco_rating');

		if ($countOfCustomersWhoGaveRatingForRecos > 0)
		{
			$cumulativeRecommendationsRating = $cumulativeRecommendationsRating / $countOfCustomersWhoGaveRatingForRecos;
		}

		// total recommendation dislikes
		$totalNoOfDislikes = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->last_reco_dislike_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->last_reco_dislike_at <= $dateRequirements['filterChosenEndTime'];

		})->count();

		// planner rating considered based on ticket booking data
		$plannerRating = \Review::whereIn('ticket_booking_id', $totalTicketBookings->pluck('id'));

		$countOfPlannerRating = $plannerRating->count();

		$plannerRating = $plannerRating->avg('rating');

		$dataFromIndividualMetrics = null;

		$startDay = $dateRequirements['filterChosenStartTime'];
		$endDay = $dateRequirements['filterChosenEndTime'];
		$eventDetails = TypeEvent::select('name', 'id')->get();
		$statusDetails = \TypeTicketStatus::select('name', 'id')->get();
		$sourceDetails = \TypeTicketSource::select('name', 'id')->get();

		// handler filter
		$userSelectedHandlerId = intval($request->input('handler'));

		$userSelectedHandlerDetails = null;

		if ($userSelectedHandlerId != 'all' && $userSelectedHandlerId)
		{
			$allDataFromFunction = $this->baseFunctionForMetricsAndAnalytics();
			if ($allDataFromFunction['CRMAndAdminDetails'])
			{
				$userSelectedHandlerDetails = $allDataFromFunction['CRMAndAdminDetails'];
				$userSelectedHandlerDetails = $userSelectedHandlerDetails->where('id', $userSelectedHandlerId);
			}
		}

		// Getting CRM individual metrics
		$dataFromIndividualMetrics = [
			'crmRecoData'    => $this->crmDashBoardRecoData($startDay, $endDay, $userSelectedHandlerDetails),
			'crmBookingData' => $this->crmDashBoardBookingData($startDay, $endDay, $userSelectedHandlerDetails),
			'eventDetails'   => $eventDetails,
			'statusDetails'  => $statusDetails,
			'sourceDetails'  => $sourceDetails
		];

		$data =
			[
				'filterReset'                           => $showFilterReset,
				'countOfNewTickets'                     => $tickets->count(),
				'countOfTotalOrderProcessed'            => $totalOrderProcessed,
				'humanReadableStartDate'                => $dateRequirements['humanReadableStartDate'],
				'humanReadableEndDate'                  => $dateRequirements['humanReadableEndDate'],
				'startDate'                             => $dateRequirements['selectedStartTimeInSeconds'],
				'endDate'                               => $dateRequirements['selectedEndTimeInSeconds'],
				'handlerDetails'                        => $dataFromBaseFunction['CRMAndAdminDetails'],
				'totalBookings'                         => $totalNoOfBookings,
				'recommendationRating'                  => round($cumulativeRecommendationsRating, 2),
				'conversionRate'                        => round($conversionRate, 2),
				'recommendationCount'                   => $recommendationsCount,
				'recommendationDislikes'                => $totalNoOfDislikes,
				'plannerRating'                         => round($plannerRating, 2),
				'countOfCustomersWhoGaveRatingForRecos' => $countOfCustomersWhoGaveRatingForRecos,
				'countOfPlannerRating'                  => $countOfPlannerRating,
				'individualCRMMetrics'                  => $dataFromIndividualMetrics

			];

		return view('dashboard/crm-metrics', ['data' => $data]);
	}

	/*public function showHistory(Request $request)
	{
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'),
		                                            $request->input('filterChosenEndingTime'));

		$handlerDetails = User::select('id', 'name')
		                      ->whereIn('role_id', [config("evibe.roles.customer_delight"), config("evibe.roles.sr_crm"), config("evibe.roles.admin")])
		                      ->pluck('name', 'id')
		                      ->toArray();

		$typeTicketStatus = \TypeTicketStatus::select('id', 'name')
		                                     ->pluck('name', 'id')
		                                     ->toArray();

		$ticketUpdates = \TicketUpdate::select('ticket_update.handler_id AS ticket_updater', 'updated_at',
		                                       'type_update', 'status_id', 'ticket_update.comments', 'ticket_update.ticket_id')
		                              ->where([['updated_at', '<=', $dateRequirements['filterChosenEndTime']]
			                                      , ['updated_at', '>=', $dateRequirements['filterChosenStartTime']]])
		                              ->orderBy('updated_at', 'DESC')
		                              ->get()
		                              ->toArray();

		$sortedTicketUpdates = [];
		foreach ($ticketUpdates as $ticketUpdate)
		{
			$ticketId = $ticketUpdate["ticket_id"];
			if ($ticketId)
			{
				$sortedTicketUpdates[$ticketId][] = $ticketUpdate;
			}
		}

		$ticketIds = array_column($ticketUpdates, 'ticket_id');

		$tickets = \Ticket::select('ticket.id', 'ticket.name', 'ticket.status_id', 'ticket.phone', 'ticket.handler_id', 'ticket.event_date')
		                  ->whereIn('id', $ticketIds);

		$filterStatusId = intval(request('ticketStatusId'));
		$handlerId = intval(request('handlerId'));

		if ($filterStatusId && $filterStatusId != 'all')
		{
			$tickets = $tickets->where('status_id', $filterStatusId);
		}

		if ($handlerId && $handlerId != 'all')
		{
			$tickets = $tickets->where('handler_id', $handlerId);
		}

		$tickets = $tickets->get()->unique()->toArray();

		$showFilterReset = false;
		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$data =
			[
				'tickets'                => $sortedTicketUpdates,
				'ticketDetails'          => $tickets,
				'typeTicketStatus'       => $typeTicketStatus,
				'humanReadableStartDate' => $dateRequirements['humanReadableStartDate'],
				'filterReset'            => $showFilterReset,
				'humanReadableEndDate'   => $dateRequirements['humanReadableEndDate'],
				'startDate'              => $dateRequirements['selectedStartTimeInSeconds'],
				'endDate'                => $dateRequirements['selectedEndTimeInSeconds'],
				'crmAndAdminDetails'     => $handlerDetails
			];

		return view('dashboard/crm-history', ['data' => $data]);
	}*/
	private function baseFunctionForMetricsAndAnalytics()
	{
		//@see: fetching only required ticket fields
		$tickets = \Ticket::select('ticket.id', 'ticket.name', 'ticket.created_at', 'ticket.deleted_at', 'ticket.phone',
		                           'ticket.handler_id', 'ticket.lead_status_id', 'ticket.source_id',
		                           'ticket.enquiry_source_id', 'ticket.city_id', 'ticket.event_id', 'ticket.status_id',
		                           'ticket.paid_at', 'ticket.success_email_type', 'ticket.success_email_sent_at',
		                           'ticket.event_date', 'ticket.asked_order_details_at', 'ticket.created_handler_id',
		                           'last_reco_rating', 'last_reco_rated_at', 'last_reco_dislike_at')
		                  ->orderBy('ticket.created_at', 'DESC')
		                  ->orderBy('ticket.lead_status_id');

		$handlerDetails = User::select('id', 'name')
		                      ->whereIn('role_id', [config("evibe.roles.customer_delight"), config("evibe.roles.sr_crm"), config("evibe.roles.admin")])
		                      ->get();

		$data = [
			'tickets'            => $tickets,
			'CRMAndAdminDetails' => $handlerDetails
		];

		return $data;
	}

	private function dateRequirements($startTime = null, $endTime = null)
	{
		$defaultStartTime = Carbon::yesterday()->endOfDay()->subHours(4)->addMinute(1);
		$defaultEndTime = Carbon::today()->endOfDay()->subHours(4)->addMinute(1);

		$filterChosenStartTime = $startTime ? date('Y-m-d H:i:s', strtotime($startTime)) : $defaultStartTime;
		$filterChosenEndTime = $endTime ? date('Y-m-d H:i:s', strtotime($endTime)) : $defaultEndTime;

		$selectedStartTimeInSeconds = strtotime($filterChosenStartTime);
		$selectedEndTimeInSeconds = strtotime($filterChosenEndTime);

		$sevenDaysInSeconds = 604800;

		//Server side validation as to show only 7 days data

		if (($selectedEndTimeInSeconds - $selectedStartTimeInSeconds) >= $sevenDaysInSeconds)
		{
			$selectedStartTimeInSeconds = $selectedEndTimeInSeconds - $sevenDaysInSeconds;
			$filterChosenStartTime = date('Y-m-d H:i:s', $selectedStartTimeInSeconds);
		}

		$humanReadableStartDate = date("F j, Y, g:i A", $selectedStartTimeInSeconds);
		$humanReadableEndDate = date("F j, Y, g:i A", $selectedEndTimeInSeconds);

		$data = [
			'defaultStartTime'           => $defaultStartTime,
			'defaultEndTime'             => $defaultEndTime,
			'filterChosenStartTime'      => $filterChosenStartTime,
			'filterChosenEndTime'        => $filterChosenEndTime,
			'selectedStartTimeInSeconds' => $selectedStartTimeInSeconds,
			'selectedEndTimeInSeconds'   => $selectedEndTimeInSeconds,
			'humanReadableStartDate'     => $humanReadableStartDate,
			'humanReadableEndDate'       => $humanReadableEndDate
		];

		return $data;
	}

	private function crmDashBoardRecoData($startDay, $endDay, $userSelectedHandlerDetails = null)
	{
		// getting all the mappings who sent the recos, merge if there are multiple mappings for the same ticket id
		$ticketMappings = \TicketMapping::select('id', 'recommended_at', 'ticket_id', 'handler_id')
		                                ->whereBetween('recommended_at', [$startDay, $endDay])
		                                ->whereNull('deleted_at')
		                                ->get()
		                                ->sortBy('id')
		                                ->toArray();

		$ticketIds = array_unique(array_column($ticketMappings, 'ticket_id'));

		$ticketEventData = \Ticket::select('id', 'name', 'event_id', 'last_reco_rated_at', 'last_reco_rating')
		                          ->whereIn('id', $ticketIds)
		                          ->whereNull('deleted_at')
		                          ->get()
		                          ->toArray();

		$unique_mappings = [];
		$data = "";

		foreach ($ticketMappings as $mapping)
		{
			$hash = $mapping['ticket_id'];
			$unique_mappings[$hash] = $mapping;
		}

		// using filter

		$handlerNames = $userSelectedHandlerDetails ? $userSelectedHandlerDetails : User::select('id', 'name')
		                                                                                ->whereIn('id', array_column($unique_mappings, 'handler_id'))->get();

		foreach ($handlerNames as $handler)
		{
			$sent = 0;
			$rated = 0;
			$rating = 0;
			$skipped = 0;

			foreach ($unique_mappings as $mapping)
			{
				if ($handler->id == $mapping['handler_id'])
				{
					$key = array_search($mapping['ticket_id'], array_column($ticketEventData, 'id'));
					$thisEventData = $ticketEventData[$key];

					if ($thisEventData)
					{
						if (!is_null($mapping['recommended_at']))
						{
							$data[$handler->id][$thisEventData["event_id"]]['sent'] = !isset($data[$handler->id][$thisEventData["event_id"]]['sent']) ? 1 : $data[$handler->id][$thisEventData["event_id"]]['sent'] + 1;
							$sent = $sent == 0 ? 1 : $sent + 1;
						}
						if (!is_null($thisEventData["last_reco_rated_at"]))
						{
							$data[$handler->id][$thisEventData["event_id"]]['rated'] = !isset($data[$handler->id][$thisEventData["event_id"]]['rated']) ? 1 : $data[$handler->id][$thisEventData["event_id"]]['rated'] + 1;
							$rated = $rated == 0 ? 1 : $rated + 1;
							$data[$handler->id][$thisEventData["event_id"]]['rating'] = !isset($data[$handler->id][$thisEventData["event_id"]]['rating']) ? $thisEventData["last_reco_rating"] : $data[$handler->id][$thisEventData["event_id"]]['rating'] + $thisEventData["last_reco_rating"];
							$rating = $rating == 0 ? $thisEventData["last_reco_rating"] : $rating + $thisEventData["last_reco_rating"];
						}
						else
						{
							$data[$handler->id][$thisEventData["event_id"]]['skipped'] = !isset($data[$handler->id][$thisEventData["event_id"]]['skipped']) ? 1 : $data[$handler->id][$thisEventData["event_id"]]['skipped'] + 1;
							$skipped = $skipped == 0 ? 1 : $skipped + 1;
						}
					}
				}
			}

			$data[$handler->id]['Total']['sent'] = $sent;
			$data[$handler->id]['Total']['rated'] = $rated;
			$data[$handler->id]['Total']['rating'] = $rating;
			$data[$handler->id]['Total']['skipped'] = $skipped;
		}

		return $data;
	}

	private function crmDashBoardBookingData($startDay, $endDay, $userSelectedHandlerDetails = null)
	{
		// getting all the updates who handled the ticket first, merge if there are multiple updates for the same ticket id
		$ticketUpdates = \TicketUpdate::select('id', 'ticket_id', 'handler_id', 'status_at')
		                              ->whereBetween('status_at', [strtotime($startDay), strtotime($endDay)])
		                              ->whereNull('deleted_at')
		                              ->get()
		                              ->sortByDesc('id')
		                              ->toArray();

		$ticketIds = array_unique(array_column($ticketUpdates, 'ticket_id'));

		$ticketEventData = \Ticket::select('id', 'name', 'event_id', 'status_id', 'source_id')
		                          ->whereIn('id', $ticketIds)
		                          ->get()
		                          ->toArray();

		$unique_updates = [];
		$dataEvent = "";
		$dataSource = "";

		foreach ($ticketUpdates as $update)
		{
			$hash = $update['ticket_id'];
			$unique_updates[$hash] = $update;
		}

		$handlerNames = $userSelectedHandlerDetails ? $userSelectedHandlerDetails : User::select('id', 'name')
		                                                                                ->whereIn('id', array_unique(array_column($unique_updates, 'handler_id')))
		                                                                                ->get();

		foreach ($handlerNames as $handler)
		{
			$eventStarted = 0;
			$eventBooked = 0;
			$sourceStarted = 0;
			$sourceBooked = 0;
			$thisEventData = null;
			foreach ($unique_updates as $update)
			{
				if ($handler->id == $update['handler_id'])
				{
					$key = array_search($update['ticket_id'], array_column($ticketEventData, 'id'));
					$thisEventData = $ticketEventData[$key];

					if ($thisEventData)
					{
						$dataEvent[$handler->id][$thisEventData["event_id"]]['started'] = !isset($dataEvent[$handler->id][$thisEventData["event_id"]]['started']) ? 1 : $dataEvent[$handler->id][$thisEventData["event_id"]]['started'] + 1;
						$eventStarted = $eventStarted == 0 ? 1 : $eventStarted + 1;

						if ($thisEventData['status_id'] == config("evibe.status.booked"))
						{
							$dataEvent[$handler->id][$thisEventData["event_id"]]['booked'] = !isset($dataEvent[$handler->id][$thisEventData["event_id"]]['booked']) ? 1 : $dataEvent[$handler->id][$thisEventData["event_id"]]['booked'] + 1;
							$eventBooked = $eventBooked == 0 ? 1 : $eventBooked + 1;
						}

						$dataSource[$handler->id][$thisEventData["source_id"]]['started'] = !isset($dataSource[$handler->id][$thisEventData["source_id"]]['started']) ? 1 : $dataSource[$handler->id][$thisEventData["source_id"]]['started'] + 1;
						$sourceStarted = $sourceStarted == 0 ? 1 : $sourceStarted + 1;

						if ($thisEventData['status_id'] == config("evibe.status.booked"))
						{
							$dataSource[$handler->id][$thisEventData["source_id"]]['booked'] = !isset($dataSource[$handler->id][$thisEventData["source_id"]]['booked']) ? 1 : $dataSource[$handler->id][$thisEventData["source_id"]]['booked'] + 1;
							$sourceBooked = $sourceBooked == 0 ? 1 : $sourceBooked + 1;
						}
					}
				}
			}

			$dataEvent[$handler->id]['Total']['started'] = $eventStarted;
			$dataEvent[$handler->id]['Total']['booked'] = $eventBooked;
			$dataSource[$handler->id]['Total']['started'] = $sourceStarted;
			$dataSource[$handler->id]['Total']['booked'] = $sourceBooked;
		}

		return ['dataEvent' => $dataEvent, 'dataSource' => $dataSource];
	}

	public function showAvailChecks()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/avail-checks",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/crm/avail_checks', ['data' => $data]);
	}

}