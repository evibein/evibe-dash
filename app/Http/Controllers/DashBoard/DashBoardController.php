<?php

namespace App\Http\Controllers\DashBoard;

use App\Http\Controllers\Base\BaseController;
use App\Models\TypeEvent;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DashBoardController extends BaseController
{
	public function showList()
	{
		$roleId = Auth::user()->role_id;
		$handlers = User::select('id', 'name', 'role_id')
		                ->whereIn('role_id', [1, 2, 6, 9])
		                ->get();

		if ($roleId)
		{
			$lastBeforeMonday = Carbon::today()->startOfWeek()->subWeek(1); // last week monday
			$lastSunday = Carbon::today()->startOfWeek()->subWeek(1)->endOfWeek(); // last week sunday
			$startDay = !is_null(request('start_date')) ? request('start_date') : $lastBeforeMonday;
			$endDay = !is_null(request('end_date')) ? request('end_date') : $lastSunday;
			$eventDetails = TypeEvent::select('name', 'id')->get();
			$statusDetails = \TypeTicketStatus::select('name', 'id')->get();
			$sourceDetails = \TypeTicketSource::select('name', 'id')->get();

			if ($roleId == config("evibe.roles.super_admin") || $roleId == config("evibe.roles.admin"))
			{
				$data = [
					'admin'          => 1,
					'crmRecoData'    => $this->crmDashBoardRecoData($startDay, $endDay),
					'crmBookingData' => $this->crmDashBoardBookingData($startDay, $endDay),
					'handlers'       => $handlers,
					'eventDetails'   => $eventDetails,
					'statusDetails'  => $statusDetails,
					'sourceDetails'  => $sourceDetails
				];

			}
			elseif ($roleId == config("evibe.roles.sr_crm"))
			{
				$data = [
					'isSrCrm'        => 1,
					'crmRecoData'    => $this->crmDashBoardRecoData($startDay, $endDay),
					'crmBookingData' => $this->crmDashBoardBookingData($startDay, $endDay),
					'handlers'       => $handlers->whereIn('role_id', [config("evibe.roles.customer_delight"), config("evibe.roles.sr_crm")]),
					'eventDetails'   => $eventDetails,
					'statusDetails'  => $statusDetails,
					'sourceDetails'  => $sourceDetails
				];
			}
			elseif ($roleId == config("evibe.roles.customer_delight"))
			{
				$data = [
					'isCrm'          => 1,
					'crmRecoData'    => $this->crmDashBoardRecoData($startDay, $endDay),
					'crmBookingData' => $this->crmDashBoardBookingData($startDay, $endDay),
					'handlers'       => $handlers->where('role_id', config("evibe.roles.customer_delight")),
					'eventDetails'   => $eventDetails,
					'statusDetails'  => $statusDetails,
					'sourceDetails'  => $sourceDetails
				];
			}
			else
			{
				$data = null;
			}

			$data['isShowReset'] = false;
			if (request('start_date') || request('end_date'))
			{
				$data['isShowReset'] = true;
			}

			return view('dashboard.base', ['data' => $data]);
		}

		return redirect(route('ticket.list'));
	}

	private function crmDashBoardRecoData($startDay, $endDay)
	{
		// getting all the mappings who sent the recos, merge if there are multiple mappings for the same ticket id
		$ticketMappings = \TicketMapping::select('id', 'recommended_at', 'ticket_id', 'handler_id')
		                                ->whereBetween('recommended_at', [$startDay, $endDay])
		                                ->whereNull('deleted_at')
		                                ->get()
		                                ->sortBy('id')
		                                ->toArray();

		$ticketIds = array_unique(array_column($ticketMappings, 'ticket_id'));

		$ticketEventData = \Ticket::select('id', 'name', 'event_id', 'last_reco_rated_at', 'last_reco_rating')
		                          ->whereIn('id', $ticketIds)
		                          ->whereNull('deleted_at')
		                          ->get()
		                          ->toArray();

		$unique_mappings = [];
		$data = "";

		foreach ($ticketMappings as $mapping)
		{
			$hash = $mapping['ticket_id'];
			$unique_mappings[$hash] = $mapping;
		}

		$handlerNames = User::select('id', 'name')
		                    ->whereIn('id', array_column($unique_mappings, 'handler_id'))->get();

		foreach ($handlerNames as $handler)
		{
			$sent = 0;
			$rated = 0;
			$rating = 0;
			$skipped = 0;

			foreach ($unique_mappings as $mapping)
			{
				if ($handler->id == $mapping['handler_id'])
				{
					$key = array_search($mapping['ticket_id'], array_column($ticketEventData, 'id'));
					$thisEventData = $ticketEventData[$key];

					if ($thisEventData)
					{
						if (!is_null($mapping['recommended_at']))
						{
							$data[$handler->id][$thisEventData["event_id"]]['sent'] = !isset($data[$handler->id][$thisEventData["event_id"]]['sent']) ? 1 : $data[$handler->id][$thisEventData["event_id"]]['sent'] + 1;
							$sent = $sent == 0 ? 1 : $sent + 1;
						}
						if (!is_null($thisEventData["last_reco_rated_at"]))
						{
							$data[$handler->id][$thisEventData["event_id"]]['rated'] = !isset($data[$handler->id][$thisEventData["event_id"]]['rated']) ? 1 : $data[$handler->id][$thisEventData["event_id"]]['rated'] + 1;
							$rated = $rated == 0 ? 1 : $rated + 1;
							$data[$handler->id][$thisEventData["event_id"]]['rating'] = !isset($data[$handler->id][$thisEventData["event_id"]]['rating']) ? $thisEventData["last_reco_rating"] : $data[$handler->id][$thisEventData["event_id"]]['rating'] + $thisEventData["last_reco_rating"];
							$rating = $rating == 0 ? $thisEventData["last_reco_rating"] : $rating + $thisEventData["last_reco_rating"];
						}
						else
						{
							$data[$handler->id][$thisEventData["event_id"]]['skipped'] = !isset($data[$handler->id][$thisEventData["event_id"]]['skipped']) ? 1 : $data[$handler->id][$thisEventData["event_id"]]['skipped'] + 1;
							$skipped = $skipped == 0 ? 1 : $skipped + 1;
						}
					}
				}
			}

			$data[$handler->id]['Total']['sent'] = $sent;
			$data[$handler->id]['Total']['rated'] = $rated;
			$data[$handler->id]['Total']['rating'] = $rating;
			$data[$handler->id]['Total']['skipped'] = $skipped;
		}

		return $data;
	}

	private function crmDashBoardBookingData($startDay, $endDay)
	{
		// getting all the updates who handled the ticket first, merge if there are multiple updates for the same ticket id
		$ticketUpdates = \TicketUpdate::select('id', 'ticket_id', 'handler_id', 'status_at')
		                              ->whereBetween('status_at', [strtotime($startDay), strtotime($endDay)])
		                              ->whereNull('deleted_at')
		                              ->get()
		                              ->sortByDesc('id')
		                              ->toArray();

		$ticketIds = array_unique(array_column($ticketUpdates, 'ticket_id'));

		$ticketEventData = \Ticket::select('id', 'name', 'event_id', 'status_id', 'source_id')
		                          ->whereIn('id', $ticketIds)
		                          ->get()
		                          ->toArray();

		$unique_updates = [];
		$dataEvent = "";
		$dataSource = "";

		foreach ($ticketUpdates as $update)
		{
			$hash = $update['ticket_id'];
			$unique_updates[$hash] = $update;
		}

		$handlerNames = User::select('id', 'name')
		                    ->whereIn('id', array_unique(array_column($unique_updates, 'handler_id')))
		                    ->get();

		foreach ($handlerNames as $handler)
		{
			$eventStarted = 0;
			$eventBooked = 0;
			$sourceStarted = 0;
			$sourceBooked = 0;
			$thisEventData = null;
			foreach ($unique_updates as $update)
			{
				if ($handler->id == $update['handler_id'])
				{
					$key = array_search($update['ticket_id'], array_column($ticketEventData, 'id'));
					$thisEventData = $ticketEventData[$key];

					if ($thisEventData)
					{
						$dataEvent[$handler->id][$thisEventData["event_id"]]['started'] = !isset($dataEvent[$handler->id][$thisEventData["event_id"]]['started']) ? 1 : $dataEvent[$handler->id][$thisEventData["event_id"]]['started'] + 1;
						$eventStarted = $eventStarted == 0 ? 1 : $eventStarted + 1;

						if ($thisEventData['status_id'] == config("evibe.status.booked"))
						{
							$dataEvent[$handler->id][$thisEventData["event_id"]]['booked'] = !isset($dataEvent[$handler->id][$thisEventData["event_id"]]['booked']) ? 1 : $dataEvent[$handler->id][$thisEventData["event_id"]]['booked'] + 1;
							$eventBooked = $eventBooked == 0 ? 1 : $eventBooked + 1;
						}

						$dataSource[$handler->id][$thisEventData["source_id"]]['started'] = !isset($dataSource[$handler->id][$thisEventData["source_id"]]['started']) ? 1 : $dataSource[$handler->id][$thisEventData["source_id"]]['started'] + 1;
						$sourceStarted = $sourceStarted == 0 ? 1 : $sourceStarted + 1;

						if ($thisEventData['status_id'] == config("evibe.status.booked"))
						{
							$dataSource[$handler->id][$thisEventData["source_id"]]['booked'] = !isset($dataSource[$handler->id][$thisEventData["source_id"]]['booked']) ? 1 : $dataSource[$handler->id][$thisEventData["source_id"]]['booked'] + 1;
							$sourceBooked = $sourceBooked == 0 ? 1 : $sourceBooked + 1;
						}
					}
				}
			}

			$dataEvent[$handler->id]['Total']['started'] = $eventStarted;
			$dataEvent[$handler->id]['Total']['booked'] = $eventBooked;
			$dataSource[$handler->id]['Total']['started'] = $sourceStarted;
			$dataSource[$handler->id]['Total']['booked'] = $sourceBooked;
		}

		return ['dataEvent' => $dataEvent, 'dataSource' => $dataSource];
	}

}