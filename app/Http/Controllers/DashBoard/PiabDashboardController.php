<?php

namespace App\Http\Controllers\DashBoard;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PiabDashboardController extends BaseController
{

	public function showProductBookings()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.piab.dashboard.product-bookings.list"),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard.piab.product-bookings.list', ['data' => $data]);

	}
	public function showProductBookingInfo($productBookingId){
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.piab.dashboard.product-bookings.info"),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => [
				'productBookingId' => $productBookingId
			]
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard.piab.product-bookings.info', ['data' => $data]);
	}

	public function updateProductBookingStatus()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.piab.dashboard.product-bookings.update-status'),
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];
		$data = $this->makeGuzzleRequest($options);

		return response()->json($data);

	}

}
