<?php

namespace App\Http\Controllers\DashBoard;

use App\Http\Controllers\Base\BaseController;
use App\Models\AvailCheck\AvailabilityCheckGallery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Ticket\TicketDetailsController;

class OpsDashboardController extends BaseController
{
	public function fetchTabsCount()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.tabs-count'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return response()->json($data);
	}

	public function showOrderConfirmationTickets()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.order-confirmations.list'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/ops/order-confirmations/list', ['data' => $data]);
	}

	public function searchOrderConfirmationsData()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.order-confirmations.search'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/ops/order-confirmations/search-data', ['data' => $data]);
	}

	public function confirmPartner($ticketBookingId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.order-confirmations.confirm-partner'),
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => [
				'ticketBookingId' => $ticketBookingId
			]
		];

		$data = $this->makeGuzzleRequest($options);

		return response($data);
	}

	public function showCustomerUpdates()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.customer-updates'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/ops/customer-updates/list', ['data' => $data]);
	}

	public function showCustomerReviews()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.customer-reviews.list'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/ops/customer-reviews/list', ['data' => $data]);
	}

	public function resolveCustomerReview()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.customer-reviews.resolve'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];
		$data = $this->makeGuzzleRequest($options);

		return response()->json($data);

	}

	public function showOrderTracking()
	{

	}

	public function showCustomerProofs()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.customer-proofs.list'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		//dd($data);
		return view('dashboard/ops/customer-proofs/list', ['data' => $data]);
	}

	public function searchCustomerProofsData()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.customer-proofs.search'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/ops/customer-proofs/search-data', ['data' => $data]);
	}

	public function validateCustomerProofs()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.ops.dashboard.customer-proofs.validate'),
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return response()->json($data);
	}

	public function showAvailabilityChecks()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "ops/dashboard/avail-checks",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/ops/avail-checks/info', ['data' => $data]);

	}

	public function fetchAvailabilityData()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "ops/dashboard/avail-checks/data",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return $data;

	}

	public function confirmAvailCheck()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$data = Request()->all();

		$invalidImages = [];
		if (isset($data['photos']))
		{
			/*if (count($data['photos']) > 12)
			{
				Log::info("upload limit exceeded");
				$response = [
					'success'  => false,
					'errorMsg' => "uploaded images must be less than 13"
				];

				Log::info("upload limit exceeded");
				return $response;
			}*/

			$images = $data['photos'];

			if (count($images) > 0)
			{
				foreach ($images as $image)
				{
					$imageRules = [
						'file' => 'required|mimes:png,jpeg,jpg,JPG|max:4096'
					];

					$imageMessages = [
						'file.mimes' => 'One of the images/video is not valid. (only .png, .jpeg, .jpg, are accepted)',
						'file.max'   => 'Image size cannot be more than 4 MB'
					];

					$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

					if ($imageValidator->fails())
					{
						$failedRules = $imageValidator->failed();

						if (isset($failedRules['file']['mimes']))
						{
							$response = [
								'success'    => false,
								'imageError' => true,
								'imageName'  => $image->getClientOriginalName(),
								'errorMsg'   => $imageValidator->messages()->first()
							];

							return $response;
						}
						else
						{
							array_push($invalidImages, $image->getClientOriginalName());
						}
					}

				}

			}
		}

		if (count($invalidImages))
		{
			$response = [
				'success'    => false,
				'imageError' => true,
				'imageName'  => $invalidImages,
				'errorMsg'   => "Image size cannot be more than 4 MB"
			];

			return $response;
		}

		$options = [
			"url"         => config("evibe.api.base_url") . "ops/dashboard/avail-checks/confirm",
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => $data
		];

		$data = $this->makeGuzzleRequest($options);
		if ($data['success'])
		{
			$ticketId = $data['ticketId'];

			if (isset($images))
			{
				$uploadedImageLinks = $this->uploadImages($images, $data['availCheckId'], $ticketId);

				$imageUpload = AvailabilityCheckGallery::insert($uploadedImageLinks);

				if (!$imageUpload)
				{
					$response = [
						'success' => false,
						'msg'     => "failed to upload local images "
					];

					return $response;
				}
			}
		}

		return $data;

	}

	public function showOptionAvailability()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "ops/dashboard/option-availability",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard/ops/option-availability/info', ['data' => $data]);

	}

	public function fetchOptionSlots()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "ops/dashboard/option-availability/option-slots",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return response()->json($data);
	}

	public function addUnavailability()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "ops/dashboard/option-availability/add-unavailability",
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return response()->json($data);
	}

	protected function uploadImages($images, $id, $ticketId)
	{
		$directPath = "/ticket/$ticketId/images/avail-check/$id/";
		$uploadedImageLInks = [];
		foreach ($images as $image)
		{
			$option = [
				'isWatermark' => false,
				'isOptimize'  => false,
			];

			$imageName = $this->uploadImageToServer($image, $directPath, $option);

			$imgInsertData = [
				'url'                   => $imageName,
				'title'                 => $this->getImageTitle($image),
				'type_id'               => config('evibe.gallery.type.image'),
				'availability_check_id' => $id
			];
			array_push($uploadedImageLInks, $imgInsertData);
		}

		return $uploadedImageLInks;

	}

}