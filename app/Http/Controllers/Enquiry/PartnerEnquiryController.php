<?php
/**
 * @author: vikash <vikash@evibe.in>
 * non venue availability check,reminder will directly go to partner app.
 * @since :23 Sep 2016
 */

namespace App\Http\Controllers\Enquiry;

use App\Http\Controllers\Base\BaseTicketController;
use App\Models\Ticket\TicketMappingGallery;
use App\Models\User;
use App\Models\Util\AccessToken;
use App\Models\Util\AuthClient;
use App\Models\Util\CheckoutField;
use Evibe\Facades\AppUtilFacade as AppUtil;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class PartnerEnquiryController extends BaseTicketController
{

	// show enquiry initiated modal
	public function showAskDetails($mappingId)
	{
		$response = ['success' => false];

		$ticketMapping = \TicketMapping::findOrFail($mappingId);
		$data = $this->getMappingData($ticketMapping);

		if ($data)
		{
			$response = [
				'success'    => true,
				'info'       => $data['info'],
				'price'      => $data['price'],
				'formAction' => route('enquiry.partner.save.details', $mappingId)
			];
		}

		return response()->json($response);

	}

	// save the enquiry data to be asked
	public function saveEnquiryDetails($mappingId, Request $request)
	{
		return $this->savePartnerEnquiryData($mappingId, $request);
	}

	// show enquiry details for edit.
	public function showEnquiryDetails($mappingId)
	{
		$response = ['success' => false];

		$ticketMapping = \TicketMapping::findOrFail($mappingId);

		if ($ticketMapping)
		{
			// set the ticket mapping field old value in session

			$allFields = CheckoutField::where('type_ticket_booking_id', $ticketMapping->type_id)->get();

			$response = [
				'success'         => true,
				'id'              => $ticketMapping->id,
				'info'            => $ticketMapping->info,
				'price'           => $ticketMapping->price,
				'partyDate'       => date('Y/m/d H:i', strtotime($ticketMapping->party_date_time)),
				'enqType'         => $ticketMapping->enquiry_type_id,
				'category'        => $ticketMapping->type_id,
				'categoryDetails' => $ticketMapping->type_details,
				'formAction'      => route('enquiry.partner.update.details', $mappingId),
				'dynamic'         => [],
				'editor'          => [],
				'partnerType'     => $ticketMapping->target_partner_id ? 2 : 1
			];

			foreach ($allFields as $allField)
			{
				if ($allField->type_field_id == config('evibe.input.textarea'))
				{
					$response['editor'][$allField->name] = $allField->enquiryCheckoutValue($mappingId);
				}
				else
				{
					$response['dynamic'][$allField->name] = $allField->enquiryCheckoutValue($mappingId);
				}
			}
		}

		return response()->json($response);

	}

	// edit enquiry details
	public function updateEnquiryDetails($mappingId, Request $request)
	{
		return $this->savePartnerEnquiryData($mappingId, $request, true);
	}

	// ask enquiry from partner
	public function askEnquiry($mappingId)
	{

		$res = ['success' => false];

		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries/ask/' . $mappingId . '?isFromDash=true';

		try
		{
			$client = new Client();
			$client->request('POST', $url, ['json' => ['createdBy' => Auth::user()->id]]);

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			return redirect()->back()->with('errorMsg', $res['error']);
		}

		return redirect()->back()->with('successMsg', "Enquiry asked successfully");

	}

	// delete enquiry : only before asking from partner

	public function deleteEnquiry($mappingId)
	{
		$ticketMapping = \TicketMapping::findOrFail($mappingId);
		if ($ticketMapping)
		{
			$ticketMapping->update([
				                       'party_date_time'    => null,
				                       'price'              => null,
				                       'type_id'            => null,
				                       'type_details'       => null,
				                       'info'               => null,
				                       'enquiry_type_id'    => null,
				                       'enquiry_created_at' => null,
				                       'enquiry_sent_at'    => null
			                       ]);

			// delete gallery if any
			if ($ticketMapping->gallery->count())
			{
				foreach ($ticketMapping->gallery as $gallery)
				{
					$this->deleteImageWithFile($gallery);
				}

				// update ticket action
				$mappingValues = $this->getMappingData($ticketMapping);
				$message = !empty($mappingValues['code']) ? $mappingValues['code'] : "mapping id $mappingId";
				$ticketUpdate = [
					'comments'   => "Enquiry deleted for $message",
					'ticket'     => $ticketMapping->ticket,
					'typeUpdate' => config('evibe.ticket_type_update.manual')
				];
				$this->updateTicketAction($ticketUpdate);
			}
		}

		return redirect()->back()->with('successMsg', 'Enquiry deleted successfully');
	}

	// accept enquiry manually, InCase partner is not able to accept from application
	public function acceptEnquiry($mappingId)
	{
		$data = [
			'typeId'        => config('evibe.enquiry.avl_check'),
			'replyDecision' => 'Yes',
			'replyText'     => Input::get('comment'),
		];

		$res = $this->callEnquiryReplyApi($mappingId, $data);

		$ticket = \TicketMapping::find($mappingId)->ticket;
		$this->setCreatedHandlerId($ticket);

		return response()->json($res);
	}

	// reject enquiry
	public function rejectEnquiry($mappingId)
	{
		$data = [
			'typeId'        => config('evibe.enquiry.avl_check'),
			'replyDecision' => 'No',
			'replyText'     => Input::get('comment'),
		];

		$res = $this->callEnquiryReplyApi($mappingId, $data);

		$ticket = \TicketMapping::find($mappingId)->ticket;
		$this->setCreatedHandlerId($ticket);

		return response()->json($res);
	}

	// reply custom quote

	public function replyCustomQuote($mappingId)
	{
		$rules = ['quote' => 'required'];
		$messages = ['quote.required' => 'Please enter your best quote.'];

		$validator = validator(Input::all(), $rules, $messages);
		if ($validator->fails())
		{
			return [
				'success' => false,
				'error'   => $validator->messages()->first()
			];
		}

		$quote = Input::get('quote');
		$comment = Input::get('comment');

		$replyText = $comment ? $quote . '. ' . $comment : $quote;

		$data = [
			'typeId'    => config('evibe.enquiry.custom_quote'),
			'replyText' => $replyText,
		];

		$res = $this->callEnquiryReplyApi($mappingId, $data);

		return response()->json($res);

	}

	// delete enquiry gallery

	public function deleteEnquiryGallery($imgId)
	{
		$gallery = TicketMappingGallery::find($imgId);

		if ($gallery)
		{
			$ticketMapping = $gallery->ticketMapping;
			$redirectUrl = route('ticket.details.mappings', $ticketMapping->ticket->id) . '#' . $gallery->ticketMapping->id;

			$this->deleteImageWithFile($gallery);

			return redirect($redirectUrl)->with('successMessage' . $ticketMapping->id, 'Image Deleted successfully');
		}
	}

	// common method to save/edit enquiry data
	private function savePartnerEnquiryData($mappingId, $request, $isUpdate = false)
	{
		$res = ['success' => false];
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries/initiated/' . $mappingId . '?isFromDash=true';
		$data = $request->all();
		$data['isFromDash'] = true;
		if ($isUpdate)
		{
			$data['isUpdate'] = true;
		}

		try
		{
			$client = new Client();

			$response = $client->request('POST', $url, [
				'json' => $data
			]);

			$response = $response->getBody();
			$response = \GuzzleHttp\json_decode($response, true);

			if (array_key_exists('error', $response) || array_key_exists('hasImageError', $response))
			{
				$invalidImage = array_key_exists('invalidImage', $response) ? explode(",", $response['invalidImage']) : [];

				return response()->json(['success' => false, 'error' => $response['error'], 'imageError' => $invalidImage]);
			}

		} catch (ClientException $e)
		{
			$res['success'] = false;
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);

			if (isset($apiResponse['errorMessage']))
			{
				return response()->json(['success' => false, 'error' => $apiResponse['errorMessage']]);
			}

		}

		if ($request->hasFile('images') && $request->input('enq-image-type') == 1 && $response['success'])
		{
			$ticketId = $response['ticketId'];
			$redirectRoute = route('ticket.details.mappings', $ticketId) . '?';
			if ($isUpdate)
			{
				$redirectRoute .= 'isUpdate=1&';
			}
			$redirectRoute .= 'ref=avl-error#' . $mappingId;
			$images = $request->file('images');
			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'mimes:png,jpeg,jpg,JPG|max:1024'
				];

				$imageMessages = [
					'file.mimes' => 'One of the images/video is not valid. (only .png, .jpeg, .jpg, .mp4, .x-flv, .x-mpegURL, .MP2T, .3gpp, .quicktime, .x-msvideo, .x-ms-wmv are accepted)',
					'file.max'   => 'Image size cannot br more than 1 MB'
				];

				$imageValidator = validator(['file' => $image], $imageRules, $imageMessages);

				if ($imageValidator->fails())
				{
					return response()->json(['success' => false, 'error' => $imageValidator->messages()->first()]);
				}
			}

			$this->uploadEnquiryImages($ticketId, $mappingId, $images);
		}

		return response()->json(['success' => true]);

	}

	public function askMultipleCustomQuote(Request $request)
	{
		$mappingIds = $request->input('mappingTypes');
		$mappingIds = explode(',', $mappingIds);
		if (!count($mappingIds))
		{
			return response()->json(['success' => false, 'error' => 'No Mappings selected, please go back and select some mappings of same type']);
		}

		$res = ['success' => false];
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries/create/mcq?isFromDash=true';
		$data = $request->all();

		// first validate the image
		$images = $request->file('images');
		if ($request->hasFile('images') && $request->input('enq-image-type') == 1)
		{
			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'mimes:png,jpeg,jpg,JPG|max:1024'
				];

				$imageMessages = [
					'file.mimes' => 'One of the images/video is not valid. (only .png, .jpeg, .jpg are accepted)',
					'file.max'   => 'Image size cannot br more than 1 MB'
				];

				$imageValidator = validator(['file' => $image], $imageRules, $imageMessages);
				if ($imageValidator->fails())
				{
					return response()->json(['success' => false, 'error' => $imageValidator->messages()->first()]);
				}
			}
		}

		try
		{
			$client = new Client();

			$response = $client->request('POST', $url, [
				'json' => $data
			]);

			$response = $response->getBody();
			$response = \GuzzleHttp\json_decode($response, true);

			if (array_key_exists('error', $response) || array_key_exists('hasImageError', $response))
			{
				$invalidImage = array_key_exists('invalidImage', $response) ? explode(",", $response['invalidImage']) : [];

				return response()->json(['success' => false, 'error' => $response['error'], 'imageError' => $invalidImage]);
			}

		} catch (ClientException $e)
		{
			$res['success'] = false;
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);

			if (isset($apiResponse['errorMessage']))
			{
				return response()->json(['success' => false, 'error' => $apiResponse['errorMessage']]);
			}
		}

		if ($request->hasFile('images') && $request->input('enq-image-type') == 1 && $response['success'])
		{
			$iCount = 0;
			$imageLinks = [];

			// @see :: here for multiple quote, if file is there then uploading first images by file and
			// uploading images by the same link for other custom quote
			foreach ($mappingIds as $mappingId)
			{
				$mapping = \TicketMapping::find($mappingId);
				$ticketId = $mapping->ticket->id;

				if ($iCount == 0)
				{
					$imageLinks = $this->uploadEnquiryImages($ticketId, $mappingId, $images);
				}
				elseif ($iCount >= 1 && count($imageLinks))
				{
					$this->uploadEnquiryImages($ticketId, $mappingId, null, $imageLinks);
				}

				$iCount++;
			}
		}

		return response()->json(['success' => true]);

	}

	private function deleteImageWithFile($gallery)
	{
		// if type is not 1, then its just a link
		if ($gallery->type == 0)
		{
			$allImages = $gallery->getAllImageFilePath();

			//delete all image from server
			foreach ($allImages as $image)
			{
				File::delete($image);
			}
		}

		//delete image name from database
		$gallery->forceDelete();
	}

	private function getMappingData($ticketMapping, $isCollection = false)
	{
		return AppUtil::fillMappingValues([
			                                  'isCollection' => $isCollection,
			                                  'mapTypeId'    => $ticketMapping->map_type_id,
			                                  'mapId'        => $ticketMapping->map_id
		                                  ]);
	}

	private function getAccessToken($enquiry)
	{
		$userId = $enquiry->created_for;
		$clientId = config('evibe.api.client_id');
		$user = User::find($userId);

		if (!$userId || !$user)
		{
			Log::info("Enquiry created_for (user_id) is not valid");
			abort('403');
		}
		$token = AccessToken::where('user_id', $userId)->where('auth_client_id', $clientId)->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			$accessToken = $this->issueAccessToken($user, $client);
		}
		else
		{
			$accessToken = $token->access_token;
		}

		return $accessToken;
	}

	private function callEnquiryReplyApi($mappingId, $data)
	{
		$res = ['success' => false];
		$data['handlerId'] = Auth::user()->id;
		$enquiry = \Notification::where('ticket_mapping_id', $mappingId)->whereNull('is_replied')->orderBy('created_at', 'DESC')->first();
		if ($enquiry)
		{

			$accessToken = $this->getAccessToken($enquiry);
			$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries/' . $enquiry->id . '/reply?isFromDash=true';

			try
			{
				$client = new Client();
				$client->request('POST', $url, [
					'headers' => [
						'access-token' => $accessToken
					],
					'json'    => $data
				]);

				$res['success'] = true;
			} catch (ClientException $e)
			{
				$this->sendErrorReport($e);
				$apiResponse = $e->getResponse()->getBody(true);
				$apiResponse = \GuzzleHttp\json_decode($apiResponse);
				$res['error'] = $apiResponse->errorMessage;
			}
		}

		return $res;
	}

	protected function uploadEnquiryImages($ticketId, $mappingId, $images = null, $links = [])
	{
		$directPath = '/ticket/' . $ticketId . '/mapping/' . $mappingId . '/images/';
		$uploadedLinks = [];

		if (count($links) > 0)
		{
			$imgInsertData = [];
			foreach ($links as $link)
			{
				$imgInsertData[] = [
					'title'             => pathinfo($link, PATHINFO_FILENAME),
					'url'               => $link,
					'ticket_mapping_id' => $mappingId,
					'type_id'           => config('evibe.gallery.type.link'),
					'created_at'        => Carbon::now(),
					'updated_at'        => Carbon::now()
				];
			}

			if (count($imgInsertData) > 0)
			{
				TicketMappingGallery::insert($imgInsertData);
			}
		}
		else
		{
			foreach ($images as $image)
			{
				$option = ['isWatermark' => false];

				$imageName = $this->uploadImageToServer($image, $directPath, $option);

				$imgInsertData = [
					'url'               => $imageName,
					'title'             => $this->getImageTitle($image),
					'ticket_mapping_id' => $mappingId,
					'type_id'           => config('evibe.gallery.type.image'),
				];

				TicketMappingGallery::create($imgInsertData);
				$uploadedLinks[] = config('evibe.gallery.host') . '/ticket/' . $ticketId . '/mapping/' . $mappingId . '/images/' . $imageName;
			}
		}

		return $uploadedLinks;
	}
}
