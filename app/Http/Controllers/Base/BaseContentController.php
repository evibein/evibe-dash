<?php

namespace App\Http\Controllers\Base;

use App\Http\Requests;

class BaseContentController extends BaseController
{
	//common used function for all type of content will be here

	protected function getUniqueCode($name, $codeList)
	{
		$stringPos = 0;
		$allCode = $codeList->toArray();
		$splitNames = explode(" ", $name);
		$splitNames = array_slice($splitNames, 0, 3);
		$arrayLength = count($splitNames);
		$arrayPos = $arrayLength - 1;

		if ($arrayLength == 1)
		{
			$len = strlen($name);
			$count = 1;;
			do
			{
				$code = $name[0];
				$code .= substr($name, $count, 2);
				$code = strtoupper($code);
				$count++;
				if ($count == $len - 2)
				{
					break;
				}

			} while (in_array($code, $allCode));
		}
		else
		{
			do
			{
				$stringLength = strlen($splitNames[$arrayPos]);

				$code = $this->getCode($splitNames, $arrayPos, $stringPos);

				if (strlen($code) == 2)
				{

				}

				if ($stringPos + 1 == $stringLength)
				{
					$arrayPos--;
					$stringPos = 0;
				}

				$stringPos++;

				if ($arrayPos == 0)
				{
					break;
				}

			} while (in_array($code, $allCode));
		}

		return strtoupper($code);
	}

	//generating the unique code
	private function getCode($splitNames, $arrayPos, $stringPos)
	{
		$code = '';

		foreach ($splitNames as $splitName)
		{
			$code .= $splitName[0];
		}

		$newChar = $splitNames[$arrayPos][$stringPos];
		$code = substr_replace($code, $newChar, $arrayPos, 1);

		//return only first three character
		$code = substr($code, 0, 3);

		if (strlen($code) == 2)
		{
			$code = $code . substr($splitNames[1], -1);
		}

		return strtoupper($code);
	}

	protected function generateTypeUrl($name)
	{
		$lowerName = strtolower($name);
		$url = str_replace(" ", "-", $lowerName);

		return $url;
	}
}
