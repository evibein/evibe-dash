<?php

namespace App\Http\Controllers\Base;

use Ticket;
use App\Models\CheckAvailability;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;

class BaseAvailabilityController extends BaseTicketController
{
	public function getTicketsList($mapTypeId, $isLive = true, $searchQuery = null)
	{
		$liveTicketIds = CheckAvailability::where('map_type_id', $mapTypeId)
		                                  ->whereNull('respond_at')
		                                  ->whereExists(function ($query)
		                                  {
			                                  $query->select(DB::raw('id'))
			                                        ->from('ticket_mapping')
			                                        ->whereRaw('ticket_mapping.id = availability_check.ticket_mapping_id')
			                                        ->whereNull('deleted_at');
		                                  })
		                                  ->distinct()
		                                  ->pluck('ticket_id');

		$validTicketIds = $liveTicketIds;

		if (!$isLive)
		{
			$validTicketIds = CheckAvailability::where('map_type_id', $mapTypeId)
			                                   ->whereNotIn('ticket_id', $liveTicketIds)
			                                   ->distinct()
			                                   ->pluck('ticket_id');
		}

		$tickets = Ticket::with('checkAvailability')->whereIn('id', $validTicketIds)
		                 ->orderBy('updated_at', 'DESC');

		if ($searchQuery)
		{
			$padQuery = '%' . $searchQuery . '%';
			$tickets->where(function ($query) use ($padQuery)
			{
				$query->where('ticket.name', 'LIKE', $padQuery)
				      ->orWhere('ticket.email', 'LIKE', $padQuery)
				      ->orWhere('ticket.phone', 'LIKE', $padQuery)
				      ->orWhere('ticket.enquiry_id', 'LIKE', $padQuery);
			});
		}

		return $tickets->get();
	}
}
