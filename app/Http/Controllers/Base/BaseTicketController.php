<?php

namespace App\Http\Controllers\Base;

use App\Models\DecorEvent;
use App\Models\ServiceEvent;
use App\Models\Ticket\TicketFalloutFeedback;
use App\Models\TrendEvents;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;
use App\Models\VenueHallEvent;
use CakeEvent;
use Carbon\Carbon;

use Evibe\Facades\AppUtilFacade as AppUtil;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

use TicketBooking;
use TicketMapping;
use TicketUpdate;
use TypeTicket;
use VendorPackage;

class BaseTicketController extends BaseController
{
	/**
	 * @param      $data
	 * @param bool $handler
	 * @param null $isCancel
	 *
	 * @author Vikash <vikash@evibe.in>
	 * @Since  4th March 2016.
	 *
	 * generic function for all ticket action update
	 */
	protected function updateTicketAction($data, $handler = true)
	{
		$ticket = $data['ticket'];
		$statusId = $ticket->status_id;
		$typeUpdate = isset($data['typeUpdate']) ? $data['typeUpdate'] : config('evibe.ticket_type_update.auto');
		$statusAt = isset($data['statusAt']) ? $data['statusAt'] : time();
		$isStatusUpdated = false;
		$paymentStatuses = [
			config('evibe.status.booked'),
			config('evibe.status.confirmed'),
			config('evibe.status.auto_pay'),
			config('evibe.status.service_auto_pay')
		];
		$cancelStatuses = [
			config("evibe.status.cancelled"),
			config('evibe.status.auto_cancel')
		];

		if (array_key_exists('statusId', $data))
		{
			$statusId = $data['statusId'];
			$isStatusUpdated = true;
		}

		// ticket status will not be updated if the status is booked, confirmed or auto pay.
		if (in_array($ticket->status_id, $paymentStatuses))
		{
			// @see: update ticket to given status id and not ticket' status id.
			if (isset($data['isNoBooking']) && $data['isNoBooking'])
			{
				// use given status id
			}
			elseif ($isStatusUpdated && in_array($statusId, $cancelStatuses))
			{
				// use given status id
			}
			elseif (array_key_exists('serviceAutoCancel', $data) && $data['serviceAutoCancel'] == true)
			{
				$statusId = config('evibe.status.auto_cancel');
			}
			else
			{
				$isStatusUpdated = false;
			}
		}

		if ($isStatusUpdated)
		{
			$ticket->status_id = $statusId;
		}
		if ($handler)
		{
			$ticket->handler_id = Auth::user()->id;
		}
		$ticket->updated_at = Carbon::now();
		$ticket->save();

		$updateData = [
			'ticket_id'   => $ticket->id,
			'status_id'   => $statusId,
			'handler_id'  => Auth::user()->id,
			'comments'    => $data['comments'],
			'status_at'   => $statusAt,
			'type_update' => $typeUpdate
		];
		TicketUpdate::create($updateData);
	}

	protected function groupHallCode($venues)
	{
		$code = [];
		foreach ($venues as $venue)
		{
			array_push($code, $venue['code']);
		}
		$hallCode = implode(', ', $code);

		return $hallCode;
	}

	// exchange venue contact
	public function exchangeVenueContact($mappingId)
	{
		$res = ['success' => false];
		$ticketMapping = TicketMapping::with('ticket')->whereNull('contact_exchanged_at')->find($mappingId);

		if (!$ticketMapping)
		{
			$res = [
				'success' => false,
				'error'   => 'No vendor mappings found (or) all contacts already exchanged.'
			];
		}
		else
		{
			// get customer details
			$ticket = $ticketMapping->ticket;

			$mappedValues = $ticketMapping->getMappedValues();
			$vendorPhone = $mappedValues['provider']['phone'];
			$vendorName = $mappedValues['provider']['person'];

			if ($ticketMapping->map_type_id == config('evibe.ticket_type.venues'))
			{
				$customerTemplate = config('evibe.sms_tpl.leads.venue.customer');
				$customerReplaces = [
					'#field1#' => ucfirst($ticket->name),
					'#field2#' => $vendorName . '-' . $vendorPhone,
				];
				$customerSMS = str_replace(array_keys($customerReplaces), array_values($customerReplaces), $customerTemplate);

				$vendorTemplate = config('evibe.sms_tpl.leads.venue.vendor');
				$vendorReplaces = [
					'#field1#' => $vendorName,
					'#field2#' => date("d M, Y", $ticket->event_date),
					'#field3#' => ucfirst($ticket->name),
					'#field4#' => $ticket->phone
				];
				$vendorSMS = str_replace(array_keys($vendorReplaces), array_values($vendorReplaces), $vendorTemplate);

				if ($customerSMS)
				{
					$customerSMSData = [
						'to'   => $ticket->phone,
						'text' => $customerSMS
					];
					Queue::push('Evibe\Utilities\SendSMS@smsExchangeContacts', $customerSMSData);
				}

				//Send SMS to Vendor
				if ($vendorSMS)
				{
					$vendorSMSData = [
						'to'   => $vendorPhone,
						'text' => $vendorSMS
					];
					Queue::push('Evibe\Utilities\SendSMS@smsExchangeContacts', $vendorSMSData);
				}

				if ($customerSMS && $vendorSMS)
				{
					$ticketMapping->update(['contact_exchanged_at' => Carbon::now()]);

					$ticketUpdate = [
						'ticket'   => $ticket,
						'statusId' => config('evibe.status.followup'),
						'comments' => config('Contact exchanged for venue ' . $mappedValues['code'])
					];
					$this->updateTicketAction($ticketUpdate);

					$res = [
						'success' => true,
					];
				}
			}
		}

		return $res;
	}

	//function for filtering the mapping values based on event, which will shown
	// at the time of adding mapping for a ticket.
	protected function getMappingValuesBasedOnOccasion($eventId = null, $cityId)
	{
		$allMappings = [];
		$mapTypeIds = $this->getMappingTypesOnOccasion($eventId);

		foreach ($mapTypeIds as $mapTypeId)
		{
			$mappingValues = AppUtil::fillMappingValues([
				                                            'isCollection' => true,
				                                            'mapTypeId'    => $mapTypeId,
				                                            'cityId'       => $cityId
			                                            ]);

			$mapType = TypeTicket::find($mapTypeId);

			//filtering the mapping values based on occasion
			//@see: if event id is not there or for old tickets all mappings will be shown
			if ($eventId)
			{
				$filteredMappingValues = $this->filteredMappingValues($eventId, $mapTypeId, $mappingValues);
			}
			else
			{
				$filteredMappingValues = $mappingValues;
			}

			$allMappings[$mapTypeId] = [
				'name'   => $mapType->name,
				'values' => $filteredMappingValues
			];
		}

		return $allMappings;
	}

	// mapping types allowed at the time of adding mappings
	public function getMappingTypesOnOccasion($eventId = null)
	{
		$mapTypeIds = [config('evibe.ticket_type.planners')]; // make planners available for all occasions.

		if ($eventId == config('evibe.event.kids_birthday'))
		{
			$mapTypeIds = array_merge($mapTypeIds, [
				config('evibe.ticket_type.packages'),
				config('evibe.ticket_type.food'),
				config('evibe.ticket_type.tents'),
				config('evibe.ticket_type.venues'),
				config('evibe.ticket_type.trends'),
				config('evibe.ticket_type.cakes'),
				config('evibe.ticket_type.services'),
				config('evibe.ticket_type.decors')
			]);
		}
		elseif ($eventId == config('evibe.event.bachelor_party'))
		{
			$mapTypeIds = array_merge($mapTypeIds, [
				config('evibe.ticket_type.villa'),
				config('evibe.ticket_type.resort'),
				config('evibe.ticket_type.cakes'),
				config('evibe.ticket_type.services')
			]);
		}
		elseif ($eventId == config('evibe.event.pre_post'))
		{
			$mapTypeIds = array_merge($mapTypeIds, [
				config('evibe.ticket_type.venues'),
				config('evibe.ticket_type.cakes'),
				config('evibe.ticket_type.services'),
				config('evibe.ticket_type.decors')
			]);
		}
		elseif ($eventId == config('evibe.event.house_warming'))
		{
			$mapTypeIds = array_merge($mapTypeIds, [
				config('evibe.ticket_type.priests'),
				config('evibe.ticket_type.tents'),
				config('evibe.ticket_type.food'),
				config('evibe.ticket_type.cakes'),
				config('evibe.ticket_type.services'),
				config('evibe.ticket_type.decors')
			]);
		}
		elseif ($eventId == config('evibe.event.special_experience'))
		{
			$mapTypeIds = array_merge($mapTypeIds, [config('evibe.ticket_type.couple-experiences')]);
		}
		else
		{
			// this case is handled for occasion which is not live on
			// main website or for which ticket occasion is not there(old tickets)
			$mapTypeIds = array_merge($mapTypeIds, [
				config('evibe.ticket_type.packages'),
				config('evibe.ticket_type.venues'),
				config('evibe.ticket_type.trends'),
				config('evibe.ticket_type.cakes'),
				config('evibe.ticket_type.services'),
				config('evibe.ticket_type.decors')
			]);
		}

		return $mapTypeIds;
	}

	private function filteredMappingValues($eventId, $mapTypeId, $mappingValues)
	{
		$itemIds = [];
		$mappingData = [];

		if ($mapTypeId == config('evibe.ticket_type.packages'))
		{
			$packageIds = VendorPackage::where('event_id', $eventId)
			                            ->where('is_live', 1)
			                            ->pluck('id')
			                            ->toArray();
			$itemIds = $packageIds;
		}

		elseif ($mapTypeId == config('evibe.ticket_type.planners'))
		{
			$mappingData = $mappingValues;
		}

		elseif ($mapTypeId == config('evibe.ticket_type.venues'))
		{
			if ($eventId == config('evibe.event.pre_post'))
			{
				$venueHallIds = VenueHallEvent::where('event_id', $eventId)
				                              ->distinct()
				                              ->pluck('venue_hall_id')
				                              ->toArray();

				$itemIds = $venueHallIds;
			}
			else
			{
				$mappingData = $mappingValues;
			}
		}

		elseif ($mapTypeId == config('evibe.ticket_type.trends'))
		{
			$trendsIds = TrendEvents::where('type_event_id', $eventId)
			                        ->distinct()
			                        ->pluck('trend_id')
			                        ->toArray();

			$itemIds = $trendsIds;

		}

		elseif ($mapTypeId == config('evibe.ticket_type.cakes'))
		{
			$cakeIds = CakeEvent::where('event_id', $eventId)
			                     ->distinct()
			                     ->pluck('cake_id')
			                     ->toArray();

			$itemIds = $cakeIds;
		}

		elseif ($mapTypeId == config('evibe.ticket_type.services'))
		{
			$serviceIds = ServiceEvent::where('type_event_id', $eventId)
			                          ->distinct()
			                          ->pluck('type_service_id')
			                          ->toArray();

			$itemIds = $serviceIds;
		}

		elseif ($mapTypeId == config('evibe.ticket_type.decors'))
		{
			$decorIds = DecorEvent::where('event_id', $eventId)
			                      ->distinct()
			                      ->pluck('decor_id')
			                      ->toArray();

			$itemIds = $decorIds;
		}

		if (count($itemIds))
		{
			foreach ($mappingValues as $key => $value)
			{
				if (!in_array($key, $itemIds))
				{
					unset($mappingValues[$key]);
				}
			}

			if (count($mappingValues))
			{
				$mappingData = $mappingValues;
			}
		}

		return $mappingData;
	}

	protected function checkUpdateReceipt($ticket)
	{
		$data = [
			'isVisible' => false,
			'booking'   => []
		];

		if ($ticket->status_id == config('evibe.status.booked') && $ticket->paid_at)
		{
			$bookings = $ticket->bookings;

			foreach ($bookings as $booking)
			{
				if ($booking->is_advance_paid == 1)
				{
					$mapping = $booking->mapping->getMappedValues();

					$data['booking'][$booking->id] = [
						'name'      => $mapping['name'],
						'type'      => $booking->ticketBookingType ? $booking->ticketBookingType->name : "",
						'bookingId' => $booking->id,
						'isChecked' => false
					];

					if ($ticket->paid_at < strtotime($booking->updated_at))
					{
						$data['booking'][$booking->id]['isChecked'] = true;
						$data['isVisible'] = true;
					}
				}
			}
		}

		return $data;
	}

	protected function getCheckoutField($ticket, $typeTicketBookingId = null, $isCrm = null)
	{
		$eventId = $ticket->event_id ? $ticket->event_id : 1;
		$parentEventId = $this->getParentEventId($eventId);

		if (is_null($typeTicketBookingId))
		{
			$typeTicketBookingIds = TicketBooking::where('ticket_id', $ticket->id)
			                                      ->distinct()
			                                      ->pluck('type_ticket_booking_id');

			$checkoutFields = CheckoutField::where(function ($query) use ($parentEventId) {
				$query->where('event_id', $parentEventId)
				      ->orWhereNull('event_id');
			})->whereIn('type_ticket_booking_id', $typeTicketBookingIds);
		}
		else
		{
			$checkoutFields = CheckoutField::where(function ($query) use ($parentEventId) {
				$query->where('event_id', $parentEventId)
				      ->orWhereNull('event_id');
			})->where('type_ticket_booking_id', $typeTicketBookingId);
		}

		if (!is_null($isCrm))
		{
			$checkoutFields = $checkoutFields->where('is_crm', $isCrm);
		}

		if ($ticket->is_auto_booked == 1)
		{
			$checkoutFields = $checkoutFields->where('is_auto_booking', 1);
		}
		else
		{
			$checkoutFields = $checkoutFields->where('is_auto_booking', 0);
		}

		return $checkoutFields->get();
	}

	// saving checkout field value
	protected function saveCheckoutFieldValue($ticket, $typeTicketBookingId, $input)
	{
		$checkoutFields = $this->getCheckoutField($ticket, $typeTicketBookingId);

		foreach ($checkoutFields as $checkoutField)
		{
			// check if value is already there, if value is available then update otherwise create
			$checkoutValue = CheckoutFieldValue::where([
				                                           'ticket_id'         => $ticket->id,
				                                           'checkout_field_id' => $checkoutField->id
			                                           ])->first();

			if(isset($input['checkout-' . $checkoutField->name]))
			{
				$value = trim($input['checkout-' . $checkoutField->name]);
				if ($checkoutValue)
				{
					$checkoutValue->update(['value' => $value]);
				}
				else
				{
					CheckoutFieldValue::create([
						                           'ticket_id'         => $ticket->id,
						                           'checkout_field_id' => $checkoutField->id,
						                           'value'             => $value
					                           ]);
				}
			}
		}
	}

	protected function getFalloutFeedback($ticketId, $ticketStatusId)
	{
		$falloutFeedback = [];
		if ($ticketStatusId == config('evibe.status.not_interested'))
		{
			$falloutFeedbackReasons = TicketFalloutFeedback::join('type_fallout_feedback', 'type_fallout_feedback.id', '=', 'ticket_fallout_feedback.type_fallout_feedback_id')
			                                               ->where('ticket_fallout_feedback.ticket_id', $ticketId)
			                                               ->whereNull('ticket_fallout_feedback.deleted_at')
			                                               ->whereNull('type_fallout_feedback.deleted_at')
			                                               ->select('ticket_fallout_feedback.*', 'type_fallout_feedback.option_text')
			                                               ->get();

			$falloutFeedbackComments = TicketFalloutFeedback::where('ticket_id', $ticketId)
			                                                ->whereNotNull('comments')
			                                                ->whereNull('deleted_at')
			                                                ->first();

			if (count($falloutFeedbackReasons) || $falloutFeedbackComments)
			{
				$falloutFeedback = [
					'reasons'  => $falloutFeedbackReasons,
					'comments' => $falloutFeedbackComments
				];
			}
		}

		return $falloutFeedback;
	}

	public function getPaymentLink($id, $email)
	{
		$paymentLink = config('evibe.live.checkout');
		$paymentLink .= "?id=" . $id;
		$paymentLink .= "&token=" . Hash::make($id);
		$paymentLink .= "&uKm8=" . Hash::make($email);
		$data = ['getFullUrl'  => $paymentLink,
		         'getShortUrl' => $this->getShortUrl($paymentLink)];

		return $data;
	}

	public function sendWhatsAppMsg()
	{
		$id = request()->input('id');
		$ticket = \Ticket::select('name', 'phone', 'source_id', 'email', 'event_date', 'payment_deadline', 'reco_url', 'type_decision_maker')
		                 ->where('id', $id)
		                 ->first();

		if (!$ticket)
		{
			$whatsAppUrl = "https://web.whatsapp.com/send?phone=91&text=";
		}
		else
		{
			$name = $ticket->name;
			$number = $ticket->phone;
			$callingCode = $ticket->calling_code;
			$email = $ticket->email;
			$source_id = $ticket->source_id;
			$partyDate = $ticket->event_date;
			$partyDate = isset($partyDate) && $partyDate ? date('d M,h A', $partyDate) : "";
			$paymentDeadline = $ticket->payment_deadline;
			$paymentDeadline = isset($paymentDeadline) && $paymentDeadline ? date('d M,h A', $paymentDeadline) : "";
			$typeDecisionMaker = $ticket->type_decision_maker;
			$typeDecisionMaker = isset($typeDecisionMaker) && $typeDecisionMaker ? $typeDecisionMaker : null;
			$categoryName = TicketMapping::where('ticket_id', $id)
			                              ->select('map_type_id')
			                              ->first();
			$categoryName = isset($categoryName) && $categoryName->map_type_id ? config('evibe.ticket_type_id.' . $categoryName->map_type_id) : "";
			$recoUrl = $ticket->reco_url;
			$recoUrl = isset($recoUrl) && $recoUrl ? $recoUrl : null;
			$handlerName = isset(Auth::user()->name) ? Auth::user()->name : "kushi";

			if (request()->is('ticket/whatsapp/normal'))
			{
				$msgType = request()->input('msgType');
				$followupId = request()->input('followupId');
				if ($msgType == "followup")
				{
					if ($followupId == 1)
					{
						if ($source_id == 2)
						{
							$msg = config('evibe.whatsapp_messages.followup.1.phone');
						}
						else
						{
							$msg = config('evibe.whatsapp_messages.followup.1.nonPhone');
						}
					}
					elseif ($followupId == 6)
					{
						if ($paymentDeadline && $paymentDeadline > date('d M,h A', time()))
						{
							$msg = config('evibe.whatsapp_messages.followup.' . $followupId);
							$paymentLink = $this->getPaymentLink($id, $email)['getShortUrl'];
						}
						else
						{
							$msg = config('evibe.whatsapp_message.other');
						}
					}
					else
					{
						$msg = config('evibe.whatsapp_messages.followup.' . $followupId);
					}
				}
				elseif ($msgType == "noResponse")
				{
					if ($source_id == 2)
					{
						$msg = config('evibe.whatsapp_messages.' . $msgType . ".phone");
					}
					else
					{
						$msg = config('evibe.whatsapp_messages.' . $msgType . ".nonPhone");
					}

				}
				else
				{
					$msg = config('evibe.whatsapp_messages.' . $msgType);
				}
				$msgReplace = [
					'#CUSTOMER#'             => $name,
					'#HANDLER#'              => $handlerName,
					'#SHORT_PAYMENT_LINK#'   => isset($paymentLink) && $paymentLink ? $paymentLink : "",
					'#party_date#'           => $partyDate,
					'#decision_maker_value#' => $typeDecisionMaker == 1 ? config('evibe.type_customer_relation.3') : (config('evibe.type_customer_relation.' . $typeDecisionMaker) ? config('evibe.type_customer_relation.' . $typeDecisionMaker) : config('evibe.type_customer_relation.3')),
					'#category_name#'        => $categoryName,
					'#payment_deadline#'     => $paymentDeadline,
					'#SHORTLIST_LINK#'       => isset($recoUrl) && $recoUrl ? $this->getShortUrl($recoUrl) : null
				];
			}
			else
			{
				$paymentLink = $this->getPaymentLink($id, $email);
				$paymentLink = $paymentLink['getShortUrl'];
				$msg = config('evibe.whatsapp_messages.paymentLink');
				$msgReplace = [
					'#CUSTOMER#'           => $name,
					'#party_date#'         => $partyDate,
					'#payment_deadline#'   => $paymentDeadline,
					'#SHORT_PAYMENT_LINK#' => isset($paymentLink) && $paymentLink ? $paymentLink : "",
					'#HANDLER#'            => $handlerName,
				];
			}

			$msgText = str_replace(array_keys($msgReplace), array_values($msgReplace), $msg);
			$callingCode = $callingCode ?: '+91';
			$whatsAppUrl = "https://web.whatsapp.com/send?phone=". $callingCode . $number . "&text=" . $msgText;
		}

		return response()->json([
			                        'success' => true,
			                        'text'    => $whatsAppUrl
		                        ]);
	}

	//modified to send direct followups through whatsapp and update followups

	/*public function sendWhatsAppMsg()
	{
		try
		{

			$ticketId = request()->input('id');
			$ticket = \Ticket::find($ticketId);

			if (!$ticket)
			{
				$whatsAppUrl = "https://web.whatsapp.com/send?phone=91&text=";
			}
			else
			{
				$name = $ticket->name;
				$number = $ticket->phone;
				$callingCode = $ticket->calling_code;
				$email = $ticket->email;
				$source_id = $ticket->source_id;
				$partyDate = $ticket->event_date;
				$partyDate = isset($partyDate) && $partyDate ? date('d M,h A', $partyDate) : "";
				$paymentDeadline = $ticket->payment_deadline;
				$paymentDeadline = isset($paymentDeadline) && $paymentDeadline ? date('d M,h A', $paymentDeadline) : "";
				$typeDecisionMaker = $ticket->type_decision_maker;
				$typeDecisionMaker = isset($typeDecisionMaker) && $typeDecisionMaker ? $typeDecisionMaker : null;
				$categoryName = \TicketMapping::where('ticket_id', $ticketId)
				                              ->select('map_type_id')
				                              ->first();
				$categoryName = isset($categoryName) && $categoryName->map_type_id ? config('evibe.ticket_type_id.' . $categoryName->map_type_id) : "";
				$recoUrl = $ticket->reco_url;
				$recoUrl = isset($recoUrl) && $recoUrl ? $recoUrl : null;
				$handlerName = isset(Auth::user()->name) ? Auth::user()->name : "kushi";
				$handlerId = isset(Auth::user()->id) ? Auth::user()->id : "1000";
				$closureDate = $ticket->expected_closure_date;

				if (request()->is('ticket/whatsapp/normal'))
				{
					$msgType = request()->input('msgType');
					$followupId = request()->input('followupId');
					$followupDate = request()->input('followupDate');
					$followupComments = request()->input('followupComments');
					$noResponseDate = request()->input('noResponseDate');
					$noResponseComments = request()->input('noResponseComments');

					$followupType = config("evibe.type_followup." . $followupId);

					if ($msgType == "followup")
					{
						if (strtotime($followupDate) < strtotime($partyDate))
						{
							$followupData = [
								'followup_date' => strtotime($followupDate),
								'comments'      => $followupComments,
								'ticket_id'     => $ticketId,
								'handler_id'    => $handlerId,
								'followup_type' => $followupType
							];

							$isFollowup = TicketFollowup::where('ticket_id', $ticketId)
							                            ->whereNull('is_complete')
							                            ->first();

							if ($isFollowup)
							{
								$isFollowup->update($followupData);
							}
							else
							{
								TicketFollowup::create($followupData);
							}

							if ($ticket->expected_closure_date && ($ticket->expected_closure_date > strtotime($followupDate)))
							{
								// do nothing
							}
							else
							{
								$ticket->expected_closure_date = strtotime($followupDate);
							}
							$ticket->save();

							$this->updateTicketAction([
								                          'comments'   => "[" . $followupType . "]" . $followupComments,
								                          'typeUpdate' => "WhatsApp",
								                          'ticket'     => $ticket,
								                          'statusId'   => config('evibe.status.followup')
							                          ]);

						}
						else
						{

							$errorMsg = date('d-m-Y', strtotime($followupDate)) . " exceeds the party date " . date('d-m-Y', strtotime($partyDate));

							return response()->json([
								                        'success' => false,
								                        'text'    => $errorMsg
							                        ]);
						}

						if ($followupId == 1)
						{
							if ($source_id == 2)
							{
								$msg = config('evibe.whatsapp_messages.followup.1.phone');
							}
							else
							{
								$msg = config('evibe.whatsapp_messages.followup.1.nonPhone');
							}
						}
						elseif ($followupId == 6)
						{
							if ($paymentDeadline && $paymentDeadline > date('d M,h A', time()))
							{
								$msg = config('evibe.whatsapp_messages.followup.' . $followupId);
								$paymentLink = $this->getPaymentLink($ticketId, $email)['getShortUrl'];
							}
							else
							{
								$msg = config('evibe.whatsapp_messages.other');
							}
						}
						else
						{
							$msg = config('evibe.whatsapp_messages.followup.' . $followupId);
						}

						$msg = $msg . ' We will try to contact you by :' . $followupDate;
					}
					elseif ($msgType == "noResponse")
					{
						if (strtotime($noResponseDate) < strtotime($partyDate))
						{
							$followupData = [
								'followup_date' => strtotime($noResponseDate),
								'comments'      => "",
								'ticket_id'     => $ticketId,
								'handler_id'    => $handlerId,
								'followup_type' => ""
							];

							$existingFollowup = TicketFollowup::where('ticket_id', $ticketId)
							                                  ->whereNull('is_complete')
							                                  ->first();

							if ($existingFollowup)
							{
								$existingFollowup->update($followupData);
							}
							else
							{
								TicketFollowup::create($followupData);
							}

							$this->updateTicketAction([
								                          'comments'   => "[No Response] " . $noResponseComments,
								                          'typeUpdate' => "WhatsApp",
								                          'ticket'     => $ticket,
								                          'statusId'   => config('evibe.status.no_response')
							                          ]);
						}
						else
						{
							$errorMsg = date('Y-m-d', strtotime($noResponseDate)) . " exceeds the party date " . date('Y-m-d', strtotime($partyDate));

							return response()->json([
								                        'success' => false,
								                        'text'    => $errorMsg
							                        ]);
						}

						if ($source_id == 2)
						{
							$msg = config('evibe.whatsapp_messages.' . $msgType . ".phone");
						}
						else
						{
							$msg = config('evibe.whatsapp_messages.' . $msgType . ".nonPhone");
						}

					}
					else
					{
						$msg = config('evibe.whatsapp_messages.' . $msgType);
					}

					$msgReplace = [
						'#CUSTOMER#'             => $name,
						'#HANDLER#'              => $handlerName,
						'#SHORT_PAYMENT_LINK#'   => isset($paymentLink) && $paymentLink ? $paymentLink : "",
						'#party_date#'           => $partyDate,
						'#decision_maker_value#' => $typeDecisionMaker == 1 ? config('evibe.type_customer_relation.3') : (config('evibe.type_customer_relation.' . $typeDecisionMaker) ? config('evibe.type_customer_relation.' . $typeDecisionMaker) : config('evibe.type_customer_relation.3')),
						'#category_name#'        => $categoryName,
						'#payment_deadline#'     => $paymentDeadline,
						'#SHORTLIST_LINK#'       => isset($recoUrl) && $recoUrl ? $this->getShortUrl($recoUrl) : null
					];
				}
				else
				{
					$paymentLink = $this->getPaymentLink($ticketId, $email);
					$paymentLink = $paymentLink['getShortUrl'];
					$msg = config('evibe.whatsapp_messages.paymentLink');
					$msgReplace = [
						'#CUSTOMER#'           => $name,
						'#party_date#'         => $partyDate,
						'#payment_deadline#'   => $paymentDeadline,
						'#SHORT_PAYMENT_LINK#' => isset($paymentLink) && $paymentLink ? $paymentLink : "",
						'#HANDLER#'            => $handlerName,
					];
				}

				$msgText = str_replace(array_keys($msgReplace), array_values($msgReplace), $msg);
				$callingCode = $callingCode ?: '+91';
				$whatsAppUrl = "https://web.whatsapp.com/send?phone=" . $callingCode . $number . "&text=" . $msgText;
			}

			return response()->json([
				                        'success' => true,
				                        'text'    => $whatsAppUrl
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}*/

}

