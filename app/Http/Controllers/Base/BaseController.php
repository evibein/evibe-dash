<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use App\Jobs\Emails\sendErrorAlertToTechTeam;
use App\Jobs\partnerFirstActivationHandler;
use App\Models\DecorEvent;
use App\Models\ServiceEvent;
use App\Models\TrendEvents;
use App\Models\TypeEvent;
use App\Models\TypeServices;
use App\Models\Util\AccessToken;
use App\Models\Util\AuthClient;
use App\Models\Util\LogListingActivity;
use App\Models\VenueHallEvent;
use Cake;
use Carbon\Carbon;
use Decor;
use DecorGallery;
use Evibe\Facades\AppUtilFacade as AppUtil;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use PackageTag;
use SiteErrors;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tags;
use Ticket;
use Trend;
use Vendor;
use VendorPackage;
use Venue;
use VenueHall;

class BaseController extends Controller
{
	// generic function for image upload
	protected function uploadImageToServer($imageFile, $imageUploadTo, $args = null)
	{
		$isDeliveryImage = is_array($args) && array_key_exists("isDeliveryImage", $args) ? $args['isDeliveryImage'] : 0; // default 0
		$addTimeInName = is_array($args) && array_key_exists("addTimeInName", $args) ? $args['addTimeInName'] : true; // default true
		$rootPath = config('evibe.host');
		$parentDirs = config('evibe.gallery.root');
		$fullUploadTo = $parentDirs . $imageUploadTo; // append with parent dirs
		$imageName = $isDeliveryImage == 0 ? $imageFile->getClientOriginalName() : pathinfo($imageFile, PATHINFO_BASENAME);
		$originalName = str_replace(" ", "", $imageName);
		if($addTimeInName)
		{
			$newImageName = trim(time() . '-' . $originalName);
		}
		else
		{
			$newImageName = $originalName;
		}
		$tempServerPath = $isDeliveryImage == 0 ? $imageFile->getRealPath() : pathinfo($imageFile, PATHINFO_DIRNAME) . "/" . $imageName;
		$isWatermark = is_array($args) && array_key_exists("isWatermark", $args) ? $args['isWatermark'] : true; // default true
		$isOptimize = is_array($args) && array_key_exists("isOptimize", $args) ? $args['isOptimize'] : true; // default true
		$isCollection = is_array($args) && array_key_exists("isCollection", $args) ? $args['isCollection'] : false; // default false
		$isProfile = is_array($args) && array_key_exists("isProfile", $args) ? $args['isProfile'] : null; // default null
		$rotateAngle = is_array($args) && array_key_exists("imageAngle", $args) ? $args['imageAngle'] : null;

		if (!file_exists($fullUploadTo))
		{
			$oldumask = umask(0);
			mkdir($fullUploadTo, 0777, true);
			umask($oldumask);
		}

		try
		{
			// move to the correct place
			if ($isDeliveryImage == 1)
			{
				copy($tempServerPath, $fullUploadTo . $newImageName);
			}
			else
			{
				$pathInfo = pathinfo($fullUploadTo . $newImageName);
				$fileName = isset($pathInfo["filename"]) ? $pathInfo["filename"] : false;
				$extension = isset($pathInfo["extension"]) ? $pathInfo["extension"] : false;

				move_uploaded_file($tempServerPath, $fullUploadTo . $newImageName);

				// saving file in _original tag for restoring
				copy($fullUploadTo . $newImageName, $fullUploadTo . $fileName . "_original." . $extension);
			}

			// watermark image
			if ($isWatermark)
			{
				$curlPath = $rootPath . 'images/watermark?path=' . 'gallery' . urlencode($imageUploadTo . $newImageName) . '&is_rotate=' . $rotateAngle;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $curlPath);

				if (curl_exec($ch))
				{
					curl_close($ch);
				}
			}

			// optimize image => results + profile + thumbs
			if ($isOptimize && !($isCollection))
			{
				$this->optimizeImages($fullUploadTo, $newImageName);
			}
			elseif ($isOptimize && $isCollection)
			{
				$this->optimizeImages($fullUploadTo, $newImageName, $isProfile);
			}

			return $newImageName;

		} catch (Exception $e)
		{
			$this->sendErrorReport($e);
		}

		return false; // failed case
	}

	// redirect to partner login page on main
	public function redirectPartnerProfile($shortUrl)
	{
		$userId = auth()->user()->id;
		$token = Hash::make($userId);
		$redirectLink = config('evibe.live.host') . '/p/' . $shortUrl . '?token=' . $token;

		return redirect()->away($redirectLink);
	}

	// Private methods
	private function optimizeImages($basePath, $imageName, $collection = null)
	{
		$defaultWidth = 640;
		$defaultHeight = null;

		if ($collection == 1)
		{
			$types = [
				"profile" => ['width' => 1024, 'height' => 310],
				"home"    => ['width' => 470, 'height' => 190],
				"list"    => ['width' => 460, 'height' => 190]
			];
		}
		elseif ($collection == 2)
		{
			$types = [
				"profile" => ['width' => 210, 'height' => 210],
				"list"    => ['width' => 80, 'height' => 80]
			];
		}
		else
		{
			$types = [
				"profile" => ['width' => 640],
				"results" => ['width' => 394],
				"thumbs"  => ['width' => 80]
			];
		}

		try
		{
			$newImagePath = $basePath . $imageName;
			$img = Image::make($newImagePath);
		} catch (Exception $e)
		{
			Log::error("Could not create image");
		}

		foreach ($types as $type => $values)
		{
			$optImgPath = $basePath . $type . "/";
			try
			{
				if (!file_exists($optImgPath))
				{
					mkdir($optImgPath, 0777, true);
				}

				$width = array_key_exists("width", $values) ? $values["width"] : $defaultWidth;
				$height = array_key_exists("height", $values) ? $values["height"] : $defaultHeight;

				$img->resize($width, $height, function ($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				});

				$img->save($optImgPath . $imageName);
			} catch (Exception $e)
			{
				$this->sendErrorReport($e, false);
				Log::info("Exception with saving optimised image: " . $optImgPath . $imageName);
			}
		}
	}

	public function generateUrl($usp, $isTimestampAppendUrl = true)
	{
		$usp = trim($usp);
		$pattern = '/[^\w ]+/';
		$replacement = '';
		$usp_space = preg_replace($pattern, $replacement, $usp);
		if ($isTimestampAppendUrl)
		{
			$usp_space = time() . '-' . preg_replace('/\s\s+/', ' ', $usp_space);
		}
		$url = str_replace(' ', '-', $usp_space);
		$url_shorten = $this->limit_text($url, 70);

		return $url_shorten;
	}

	// protected methods
	protected function getTagsList($productTypeId = null, $event_ids = [])
	{
		if ($productTypeId && ($productTypeId > 0))
		{
			if ((($productTypeId == config('evibe.ticket_type.cakes')) || ($productTypeId == config('evibe.ticket_type.decors')))
				&& (count($event_ids) >= 0)
			)
			{
				$allTags = Tags::where('map_type_id', $productTypeId)
				               ->where(function ($query) use ($event_ids) {
					               $query->whereNull('type_event')
					                     ->orWhereIn('type_event', $event_ids);
				               })
				               ->get();
			}
			else
			{
				$allTags = Tags::where('map_type_id', $productTypeId)->get();

				// @see: they are negated in view file as they have child tags
				$surpriseSpecificTags = [
					config('evibe.tags.surprise_relation'),
					config('evibe.tags.surprise_occasion'),
					config('evibe.tags.surprise_age_group')
				];
			}
		}
		else
		{
			$allTags = Tags::all();
		}

		$tagsList = [];

		foreach ($allTags as $tag)
		{
			if ($tag->parent_id)
			{
				if (!array_key_exists($tag->parent_id, $tagsList))
				{
					$tagsList[$tag->parent_id] = [
						'id'     => '',
						'name'   => '',
						'childs' => []
					];
				}

				$tagsList[$tag->parent_id]['childs'][$tag->id] = [
					'id'   => $tag->id,
					'name' => $tag->name,
				];
			}
			else
			{
				if (array_key_exists($tag->id, $tagsList))
				{
					if (!$tagsList[$tag->id]['id'])
					{
						$tagsList[$tag->id]['id'] = $tag->id;
						$tagsList[$tag->id]['name'] = $tag->name;
					}
				}
				else
				{
					$tagsList[$tag->id] = [
						'id'     => $tag->id,
						'name'   => $tag->name,
						'childs' => []
					];
				}
			}
		}

		return $tagsList;
	}

	public function getShortUrl($longUrl)
	{
		$shortUrl = $longUrl;

		if ($longUrl && strpos($longUrl, "goo.gl") === false)
		{
			try
			{
				//$shortUrl = $this->getGooGlShortLink($longUrl);
				$shortUrl = $this->getEvibesShortLink($longUrl);
			} catch (Exception $e)
			{
				Log::info("ERROR:: While using the google short url to short the link " . $longUrl);
			}
		}

		return $shortUrl;
	}

	/**
	 * INTERNAL METHODS
	 */
	private function getEvibesShortLink($longUrl)
	{
		// longUrl definitely exists at this point
		$shortUrl = $longUrl;

		if ($longUrl && (strpos($longUrl, "bit.ly") === false) && (strpos($longUrl, "evb.app") === false))
		{
			Log::info("comes here ---- ");
			try
			{
				$options = [
					'url'         => "https://evb.app/api/v2/action/shorten",
					"method"      => "POST",
					"accessToken" => '',
					"jsonData"    => [
						'url'           => $longUrl,
						'key'           => config("evibe.evibes.access_token"),
						'custom_ending' => '',
						'is_secret'     => false,
						'response_type' => 'json'
					]
				];

				$res = $this->makeGuzzleRequest($options);
				Log::info("res: ".print_r($res, true));

				if (isset($res["action"]) && $res["action"] == "shorten" && isset($res["result"]) && $res["result"] != $longUrl)
				{
					$shortUrl = $res["result"];
				}

			} catch (Exception $exception)
			{
				Log::info("exception: ".$exception->getMessage());
				$this->sendErrorReport($exception);
			}
		}

		return $shortUrl;
	}

	/**
	 * INTERNAL METHODS
	 */
	private function getGooGlShortLink($longUrl)
	{
		// longUrl definitely exists at this point
		$shortUrl = $longUrl;

		if ($longUrl && (strpos($longUrl, "bit.ly") === false) && (strpos($longUrl, "evb.app") === false))
		{
			try
			{
				/* @see: changing goo.gl to bit.ly
				$baseUrl = "https://www.googleapis.com/urlshortener/v1/url?key=" . config('evibe.google.shortener_key');

				$postData = ['longUrl' => $longUrl];
				$jsonData = json_encode($postData);

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlObj, CURLOPT_HEADER, 0);
				curl_setopt($curlObj, CURLOPT_HTTPHEADER, ['Content-type:application/json']);
				curl_setopt($curlObj, CURLOPT_POST, 1);
				curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

				$response = curl_exec($curlObj);

				// Change the response json string to object
				curl_close($curlObj);

				$shortUrl = \GuzzleHttp\json_decode($response)->id;
				 */

				$accessToken = config('evibe.bitly.access_token');
				$switchTime = Carbon::createFromTimestamp(time())->startOfMonth()->addDays(15)->startOfDay()->toDateTimeString();
				if(time() > strtotime($switchTime))
				{
					$accessToken = config('evibe.bitly.alt_access_token');
				}
				$encodedUrl = urlencode($longUrl);

				// default format is json
				$baseUrl = "https://api-ssl.bitly.com/v3/shorten?access_token=" . $accessToken . "&longUrl=" . $encodedUrl;

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);

				$response = curl_exec($curlObj);

				curl_close($curlObj);

				// response is in json format
				// normal decode without 2nd var as 'true' will return in stdObjectClass
				$response = json_decode($response, true);
				if (isset($response['status_txt']) && $response['status_txt'] == "OK")
				{
					$shortUrl = isset($response['data']['url']) && $response['data']['url'] ? $response['data']['url'] : $longUrl;
				}
				else
				{
					$this->sendNonExceptionErrorReport([
						                                   'code'      => config('evibe.error_code.create_function'),
						                                   'url'       => 'Bitly Url Shortener',
						                                   'method'    => 'GET',
						                                   'message'   => '[BaseController.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                   'exception' => '[BaseController.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                   'trace'     => print_r($response, true),
						                                   'details'   => print_r($response, true)
					                                   ]);
				}

			} catch (Exception $exception)
			{
				$this->sendErrorReport($exception);
			}
		}

		return $shortUrl;
	}

	private function limit_text($text, $limit)
	{
		if (str_word_count($text, 0) > $limit)
		{
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$limit]) . '...';
		}

		return $text;
	}

	// Generic function for removing items from priority list
	protected function removeItemsFromAutoBookAndPriorityTable($mapId, $mapTypeId)
	{
		$this->removeItemsFromTable($mapId, $mapTypeId, 'priority');
		$this->removeItemsFromTable($mapId, $mapTypeId, 'auto_booking');
	}

	protected function getPackageIdsFilterByTag($occasionId, $typeTicketId)
	{
		$validTagIds = Tags::where('type_event', $occasionId)
		                   ->where('map_type_id', $typeTicketId)
		                   ->pluck('id');

		$packageIds = PackageTag::whereIn('tile_tag_id', $validTagIds)->distinct()->pluck('planner_package_id')->toArray();

		return $packageIds;
	}

	private function removeItemsFromTable($mapTypeId, $mapId, $tableName)
	{
		if ($mapTypeId == config('evibe.ticket_type.venues'))
		{
			//delete all the halls of that venue from priority list
			$halls = Venue::find($mapId)->venueHall;

			if ($halls->count())
			{
				foreach ($halls as $hall)
				{
					DB::table($tableName)->where(['map_id' => $hall->id, 'map_type_id' => config('evibe.ticket_type.venue_halls')])->delete();
				}
			}
		}
		else
		{
			DB::table($tableName)->where(['map_id' => $mapId, 'map_type_id' => $mapTypeId])->delete();
		}
	}

	// function for validating the emails
	protected function validateEmail($inputEmail, $ticket, $fieldName = null)
	{
		$email = is_array($inputEmail) ? $inputEmail[0] : $inputEmail;
		$fieldTitle = $fieldName ? $fieldName : "Sender";
		$res = ['success' => true];

		$rules = [
			//'name'  => 'required',
			'email' => 'required|email'
		];

		$messages = [
			'email.required' => $fieldTitle . ' email address not found',
			'email.email'    => $fieldTitle . ' email address is not valid',
			//'name.required'  => 'Please enter the customer (Ticket) name.'
		];

		$data = [
			'email' => $email,
			'name'  => $ticket->name ?: "Customer"
		];

		$validator = Validator::make($data, $rules, $messages);

		if ($validator->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validator->messages()->first()
			];
		}

		return $res;
	}

	/**
	 * @param $data {array:: should have ** "mapId", "mapTypeId", "message" ** key}
	 *
	 * @since  :: 01 December
	 *         function for adding the log of all activity
	 *
	 * @author :: Vikash <vikash@evibe.in>
	 */
	protected function logListingActivity($data)
	{
		$insertId = null;
		$message = isset($data['message']) ? $data['message'] : "";
		$mapTypeId = isset($data['mapTypeId']) ? $data['mapTypeId'] : "";
		$mapId = isset($data['mapId']) ? $data['mapId'] : "";
		$loggedBy = Auth::user()->id;

		$insertData = [
			'map_type_id' => $mapTypeId,
			'map_id'      => $mapId,
			'message'     => $message,
			'logged_by'   => $loggedBy
		];

		LogListingActivity::create($insertData);
	}

	/**
	 * @author :: Vikash <vikash@evibe.in>
	 * @since  :: 09 January 2017
	 */
	public function checkAndSendFirstActivationEmail($eventId, $itemType, $mapper, $mapType = null)
	{
		$mapType = $mapType ? $mapType : config('evibe.ticket_type.planners');
		$provider = $mapper->provider;

		if ($this->hasFirstBooking($mapType, $provider->id))
		{
			$cityUrl = $mapper->city_id ? $mapper->city->url : $provider->city->url;

			$data = [
				'partner'   => $provider,
				'mapTypeId' => $mapType,
				'itemType'  => $itemType,
				'liveLink'  => $provider->user ? config('evibe.live.host') . '/p/' . $provider->user->short_url : AppUtil::getWebsiteLiveLink($eventId, $mapper, $itemType, $cityUrl)
			];

			if ($itemType == config('evibe.ticket_type.packages'))
			{
				$data['itemType'] = $this->getItemTypeFromPackage($mapper);
			}

			$job = (new partnerFirstActivationHandler($data))->delay(Carbon::now()->addMinute(5)->timestamp);
			dispatch($job);
		}
	}

	/**
	 * generic function for reporting error, Use for try catch block
	 * this function needs parameter as Instance of \Exception class $e & array with data
	 */
	protected function reportErrorToTechTeam($e, $data)
	{
		$subSig = '[URGENT] ';
		$cc = array_key_exists('cc', $data) && $data['cc'] ? $data['cc'] : [];
		$sub = $subSig . array_key_exists('sub', $data) && $data['sub'] ? $data['sub'] : ':: Some error occurred, please check immediately';

		$data = [
			'sub'        => $sub,
			'cc'         => $cc,
			'errorTitle' => $e->getMessage(),
			'code'       => $e->getCode(),
			'details'    => $e->getTraceAsString()
		];

		dispatch(new sendErrorAlertToTechTeam($data));
	}

	protected function reportCustomErrorToTechTeam($data)
	{
		$subSig = '[URGENT] ';
		$cc = array_key_exists('cc', $data) && $data['cc'] ? $data['cc'] : [];
		$sub = $subSig . array_key_exists('sub', $data) && $data['sub'] ? $data['sub'] : ':: Some error occurred, please check immediately';

		$mailData = [
			'sub'        => $sub,
			'cc'         => $cc,
			'errorTitle' => isset($data['errorTitle']) && $data['errorTitle'] ? $data['errorTitle'] : 'DASH Error',
			'code'       => isset($data['code']) && $data['code'] ? $data['code'] : 0,
			'details'    => isset($data['details']) && $data['details'] ? $data['details'] : 'This is the default message shown when actual error details are missing.'
		];

		dispatch(new sendErrorAlertToTechTeam($mailData));
	}

	private function hasFirstBooking($mapTypeId, $providerId)
	{
		$bookingCount = 0;

		$count = VendorPackage::where([
			                              'map_type_id' => $mapTypeId,
			                              'map_id'      => $providerId,
			                              'is_live'     => 1,
		                              ])->count();
		if ($count > 1)
		{
			return false;
		}
		else
		{
			$bookingCount += $count;
		}

		if ($bookingCount < 2)
		{
			$count = Cake::where('provider_id', $providerId)->where('is_live', 1)->count();

			if ($count > 1)
			{
				return false;
			}
			else
			{
				$bookingCount += $count;
			}
		}

		if ($bookingCount < 2)
		{
			$count = Decor::where('provider_id', $providerId)->where('is_live', 1)->count();

			if ($count > 1)
			{
				return false;
			}
			else
			{
				$bookingCount += $count;
			}
		}

		if ($bookingCount < 2)
		{
			$count = Trend::where('planner_id', $providerId)->where('is_live', 1)->count();

			if ($count > 1)
			{
				return false;
			}
			else
			{
				$bookingCount += $count;
			}
		}

		if ($bookingCount < 2)
		{
			$count = Venue::where('id', $providerId)->where('is_live', 1)->count();

			if ($count > 1)
			{
				return false;
			}
			else
			{
				$bookingCount += $count;
			}
		}

		if ($bookingCount == 1)
		{
			return true;
		}

		return false;
	}

	protected function getItemTypeFromPackage($package)
	{
		$eventId = $package->event_id;
		$type = config('evibe.ticket_type.packages');

		if ($eventId == config('evibe.event.kids_birthday'))
		{
			if ($this->checkPackageType($package, config('evibe.ticket_type.venue-deals')))
			{
				$type = config('evibe.ticket_type.venue-deals');
			}
		}
		elseif ($eventId == config('evibe.event.bachelor_party'))
		{
			//resort
			if ($this->checkPackageType($package, config('evibe.ticket_type.resort')))
			{
				$type = config('evibe.ticket_type.resort');
			}
			//villa
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.villa')))
			{
				$type = config('evibe.ticket_type.villa');
			}
			//lounge
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.lounge')))
			{
				$type = config('evibe.ticket_type.lounge');
			}
			//food
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.food')))
			{
				$type = config('evibe.ticket_type.food');
			}
		}
		elseif ($eventId == config('evibe.event.special_experience'))
		{
			$type = config('evibe.ticket_type.couple-experiences');
		}

		return $type;
	}

	public function isVenueDeal($packageId)
	{
		$package = VendorPackage::find($packageId);
		$eventId = $package->event_id;

		if ($eventId == config('evibe.event.kids_birthday'))
		{
			if ($this->checkPackageType($package, config('evibe.ticket_type.venue-deals')))
			{
				return 1;
			}
		}

		return 0;
	}

	private function checkPackageType($package, $checkForType)
	{
		return AppUtil::checkPackageType($package, $checkForType);
	}

	// function for validating the phone number for sms
	protected function validatePhone($phone, $ticket, $fieldName = null)
	{
		$res = ['success' => true];
		$rules = [
			'phone' => 'required|phone'
		];

		$fieldName = $fieldName ? $fieldName : "Sender";

		$messages = [
			'phone.required' => $fieldName . ' phone number not found',
			'phone.phone'    => $fieldName . ' phone number is not valid'
		];

		$data['phone'] = $phone;
		$data['name'] = $ticket->name ? ucfirst($ticket->name) : $fieldName;

		$validator = Validator::make($data, $rules, $messages);

		if ($validator->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validator->messages()->first()
			];
		}

		return $res;

	}

	protected function formatPrice($price)
	{
		return AppUtil::formatPrice($price);
	}

	protected function getYouTubeVideoId($fullUrl)
	{
		return $videoId = $this->validateYouTubeUrl($fullUrl);
	}

	protected function getImageTitle($image)
	{
		return pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
	}

	protected function sendErrorReport($e, $sendEmail = true)
	{
		$code = 'Error';
		if ($e instanceof HttpException)
		{
			$code = $e->getStatusCode();
		}

		if (config('evibe.email_error'))
		{
			$data = [
				'fullUrl' => request()->fullUrl(),
				'code'    => $code,
				'message' => $e->getMessage(),
				'details' => $e->getTraceAsString()
			];

			if ($sendEmail)
			{
				$emailData = [
					'code'    => $code,
					'url'     => $data['fullUrl'],
					'method'  => request()->method(),
					'message' => $data['message'],
					'trace'   => $data['details']
				];

				Queue::push('\Evibe\Utilities\SendEmail@mailGenericErrorToAdmin', $emailData);
			}
			$this->saveError($data);
		}
	}

	protected function sendNonExceptionErrorReport($data, $sendEmail = true)
	{
		$errorData = [
			'code'       => isset($data['code']) && $data['code'] ? $data['code'] : 0,
			'url'        => isset($data['url']) && $data['url'] ? $data['url'] : config('evibe.host'),
			'method'     => isset($data['method']) && $data['method'] ? $data['method'] : request()->method(),
			'message'    => isset($data['message']) && $data['message'] ? $data['message'] : 'Some error occurred',
			'trace'      => isset($data['trace']) && $data['trace'] ? $data['trace'] : null,
			'project_id' => isset($data['projectId']) && $data['projectId'] ? $data['projectId'] : config('evibe.project_id'),
			'exception'  => isset($data['exception']) && $data['exception'] ? $data['exception'] : 'Unknown exception',
			'details'    => isset($data['details']) && $data['details'] ? $data['details'] : 'No clear details'
		];
		if ($sendEmail)
		{
			Queue::push('\Evibe\Utilities\SendEmail@mailGenericErrorToAdmin', $errorData);
		}
		$this->saveError($errorData);
	}

	protected function saveError($data)
	{
		SiteErrors::create([
			                   'project_id' => config('evibe.project_id'),
			                   'url'        => isset($data['fullUrl']) && $data['fullUrl'] ? $data['fullUrl'] : (isset($data['url']) && $data['url'] ? $data['url'] : null),
			                   'exception'  => $data['message'],
			                   'code'       => $data['code'],
			                   'details'    => $data['details']
		                   ]);
	}

	private function validateYouTubeUrl($url)
	{
		$pattern =
			'%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | y2u\.be/      # Either y2u.be
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch/     # watch
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x';

		$result = preg_match($pattern, $url, $matches);
		if ($result)
		{
			return $matches[1];
		}

		return false;
	}

	protected function getParams($paramName = 'page')
	{
		$existingQueryParams = [];

		foreach (request()->all() as $key => $value)
		{
			if ($key != $paramName)
			{
				$existingQueryParams[$key] = urldecode($value);
			}
		}

		return $existingQueryParams;
	}

	protected function getPartnerName($mapId, $mapTypeId)
	{
		$infoText = "";
		if ($mapTypeId == config('evibe.ticket_type.venues'))
		{
			$venue = Venue::find($mapId);
			$infoText = $venue ? $venue->person . ', ' . $venue->name : "";
		}
		elseif ($mapTypeId == config('evibe.ticket_type.planners'))
		{
			$planner = Vendor::find($mapId);
			$infoText = $planner ? $planner->person . ', ' . $planner->name : "";
		}

		return $infoText;
	}

	protected function getSinglePartner($mapId, $mapTypeId)
	{
		$planner = "";
		if ($mapTypeId == config('evibe.ticket_type.venues'))
		{
			$planner = Venue::find($mapId);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.planners'))
		{
			$planner = Vendor::find($mapId);
		}

		return $planner;
	}

	protected function getPaginatedSlNo($perPage, $page = 'page')
	{
		$pageCount = request($page, 1);
		$slStartCount = ($perPage * ($pageCount - 1)) + 1;

		return $slStartCount;
	}

	protected function changeImagePath($imagePath, $newImagePath, $imageUrl)
	{
		try
		{
			if (!file_exists($newImagePath))
			{
				mkdir($newImagePath, 0777, true);
			}

			copy($imagePath . $imageUrl, $newImagePath . $imageUrl);
		} catch (Exception $exception)
		{

			$this->sendErrorReport($exception, true);

			Log::info("Exception with changing image path: Image Path: $imagePath, newImagePath: $newImagePath");
		}

		$this->optimizeImages($newImagePath, $imageUrl);
	}

	protected function deleteImagesAfterProviderChange($dir)
	{
		try
		{
			foreach (scandir($dir) as $file)
			{
				if ('.' === $file || '..' === $file)
				{
					continue;
				}
				if (is_dir("$dir/$file"))
				{
					$this->deleteImagesAfterProviderChange("$dir/$file");
				}
				else
				{
					unlink("$dir/$file");
				}
			}

			rmdir($dir);
		} catch (Exception $e)
		{
			$this->sendErrorReport($e, true);
			Log::info("Could not delete files $dir");
		}
	}

	public function getAccessTokenFromUserId($user)
	{
		// default client id
		$clientId = config('evibe.api.client_id');

		$token = AccessToken::where('user_id', $user->id)
		                    ->where('auth_client_id', $clientId)
		                    ->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			if ($client)
			{
				$accessToken = $this->issueAccessToken($user, $client);

				return $accessToken;
			}
			else
			{
				return redirect(route("ticket.list"));
			}
		}
		else
		{
			return $token->access_token;
		}
	}

	public function issueAccessToken($user, $client)
	{
		$accessToken = new AccessToken();
		$accessToken->access_token = $this->getNewAccessToken();
		$accessToken->auth_client_id = $client->auth_client_id;
		$accessToken->user_id = $user->id;

		$accessToken->save();

		return $accessToken->access_token;
	}

	private function getNewAccessToken($len = 40)
	{
		$stripped = '';
		do
		{
			$bytes = openssl_random_pseudo_bytes($len, $strong);

			// We want to stop execution if the key fails because, well, that is bad.
			if ($bytes === false || $strong === false)
			{
				// @codeCoverageIgnoreStart
				throw new Exception('Error Generating Key');
				// @codeCoverageIgnoreEnd
			}
			$stripped .= str_replace(['/', '+', '='], '', base64_encode($bytes));
		} while (strlen($stripped) < $len);

		return substr($stripped, 0, $len);
	}

	public function setCreatedHandlerId($ticket, $userId = null)
	{
		if (!$userId)
		{
			$userId = Auth::user()->id;
		}

		if (is_int($ticket))
		{
			$ticket = Ticket::find($ticket); // id is given;
		}

		if (!$ticket->created_handler_id)
		{
			$ticket->created_handler_id = $userId;
			$ticket->save();
		}
	}

	// function that gives parent id from type event table
	public function getParentEventId($eventId)
	{
		$parentDetails = TypeEvent::select('parent_id')->where('id', $eventId)->first();

		if ($parentDetails && $parentDetails->parent_id)
		{
			return $parentDetails->parent_id;
		}

		return $eventId;
	}

	public function makeGuzzleRequest($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();

			$res = \GuzzleHttp\json_decode($res, true);

			return $res;

		} catch (ClientException $e)
		{
			$this->sendErrorReport($e);

			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			$this->saveError([
				                 'fullUrl' => request()->fullUrl(),
				                 'message' => 'Error occurred in Base controller while doing guzzle request',
				                 'code'    => 0,
				                 'details' => $res['error']
			                 ]);

			return false;
		}
	}

	// function replicated from AppUtil which gives the url of packages

	public function getPackageGalleryUrl($gallery, $isNew = false)
	{
		$url = config('evibe.gallery.root');

		$package = $gallery->package;

		if ($package)
		{
			if ($package->map_type_id == config('evibe.ticket_type.venues'))
			{
				$url .= '/venues/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			elseif ($package->map_type_id == config('evibe.ticket_type.planners'))
			{
				$url .= '/planner/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			elseif ($package->map_type_id == config('evibe.ticket_type.artists'))
			{
				$url .= '/artist/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			else
			{
				Log::info("Other than planner ,venue and artist");
			}

			$url .= '/' . $gallery->url;

			return $url;
		}
		else
		{
			//here to return null as we don't have package details
			return null;
		}
	}

	public function getOptionsByType($mapTypeId, $cityId, $eventId)
	{
		if ($eventId > 0)
		{
			$eventId = $this->getParentEventId($eventId);
		}

		$hasProvider = false;
		$mappingValues = $mappers = [];

		switch ($mapTypeId)
		{
			// packages
			case config('evibe.ticket_type.packages'):
			case config('evibe.ticket_type.food'):
			case config('evibe.ticket_type.priests'):
			case config('evibe.ticket_type.tents'):
			case config('evibe.ticket_type.venue-deals'):
			case config('evibe.ticket_type.resort'):
			case config('evibe.ticket_type.villa'):
			case config('evibe.ticket_type.lounge'):
			case config('evibe.ticket_type.couple-experiences'):
				$mappers = VendorPackage::with("provider")
				                        ->where('is_live', 1)
				                        ->where('type_ticket_id', $mapTypeId)
				                        ->where('event_id', $eventId)
				                        ->get();
				$hasProvider = true;
				break;

			// planners
			case config('evibe.ticket_type.planners'):
				$mappers = Vendor::where('is_live', 1)
				                 ->where('city_id', $cityId)
				                 ->get();
				break;

			// venues (aka halls)
			case config('evibe.ticket_type.venues'):
				$mappers = VenueHall::with('venue')->get();
				break;

			// services
			case config('evibe.ticket_type.services'):
				$mappers = TypeServices::select('type_service.*')
				                       ->join('service_event', 'service_event.type_service_id', '=', 'type_service.id')
				                       ->where('type_service.is_live', 1)
				                       ->where('type_service.city_id', $cityId)
				                       ->where('service_event.type_event_id', $eventId)
				                       ->get();
				break;

			// trends
			case config('evibe.ticket_type.trends'):
				$mappers = Trend::with("provider")
				                ->where('is_live', 1)
				                ->get();
				$hasProvider = true;
				break;

			// cakes
			case config('evibe.ticket_type.cakes'):
				$mappers = Cake::with("provider")
				               ->join('cake_event', 'cake_event.cake_id', '=', 'cake.id')
				               ->where('cake_event.event_id', $eventId)
				               ->where('cake.is_live', 1)
				               ->select('cake.*')
				               ->get();
				$hasProvider = true;
				break;

			// decors
			case config('evibe.ticket_type.decors'):
				$mappers = Decor::with("provider")
				                ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
				                ->where('decor_event.event_id', $eventId)
				                ->where('decor.is_live', 1)
				                ->select('decor.*')
				                ->get();
				$hasProvider = true;
				break;
		}

		if (count($mappers) > 0)
		{
			foreach ($mappers as $mapper)
			{
				$basicVal = [
					'code' => $mapper->code,
					'name' => $mapTypeId == config('evibe.ticket_type.cakes') ? $mapper->title : ($mapper->name ? $mapper->name : ""),
					'info' => $mapper->info
				];

				if ($mapTypeId == config('evibe.ticket_type.venues'))
				{
					if (($mapper->venue->is_live != 1) || ($mapper->venue->city_id != $cityId))
					{
						continue;
					}
					array_set($basicVal, "name", $mapper->venue->name . " - " . $mapper->name);
					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'         => $mapper->venue->price_min_veg,
						'partnerId'     => $mapper->venue_id,
						'partnerTypeId' => config('evibe.ticket_type.venues')
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket_type.decors'))
				{
					if ($mapper->provider->city_id != $cityId)
					{
						continue;
					}
					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'         => $mapper->min_price,
						'partnerId'     => $mapper->provider_id,
						'partnerTypeId' => config('evibe.ticket_type.planners')
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket_type.cakes'))
				{
					if ($mapper->provider->city_id != $cityId)
					{
						continue;
					}
					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'         => $mapper->price_per_kg,
						'partnerId'     => $mapper->provider_id,
						'partnerTypeId' => config('evibe.ticket_type.planners')
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket_type.services'))
				{
					$servicePrice = $this->formatPrice($mapper->min_price);
					$servicePrice .= $mapper->max_price ? ' - Rs. ' . $this->formatPrice($mapper->max_price) : '';

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'         => $servicePrice,
						'partnerTypeId' => config('evibe.ticket_type.planners')
					]);
				}
				else
				{
					// packages exists, but vendors is not active
					$isValid = $hasProvider ? $mapper->provider : $mapper->person;
					if (!$isValid)
					{
						continue;
					}

					if ($hasProvider && $mapper->provider->city_id != $cityId)
					{
						continue;
					}

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'         => $mapper->price,
						'partnerId'     => $mapper->map_id,
						'partnerTypeId' => $mapper->map_type_id
					]);
				}
			}
		}

		return $mappingValues;
	}

	public function generateDecorGalleryCode()
	{
		$decorPrefix = "" . config('evibe.code.prefix.decor_gallery'); // convert to string
		$digits = config('evibe.code.default.decor_gallery');
		$lastObj = DecorGallery::withTrashed()->orderBy('updated_at', 'DESC')->first();
		if ($lastObj)
		{
			if ($lastObj)
			{
				$lastCode = $lastObj->code;
				$digits = ((int)substr($lastCode, -4)) + 1;
			}
		}
		else
		{
			$digits = (int)substr($digits, -4);
		}

		return $decorPrefix . $digits;
	}

	public function getEventName($eventId)
	{
		$eventName = '';
		$event = TypeEvent::find($eventId);

		if ($event)
		{
			$eventName = $event->name;
		}

		return $eventName;
	}

	function getPartyDateTimeByBookingsPartyTime($minDateTime, $maxDateTime)
	{
		// if ticket has multiple ticket bookings
		// - on same date, use just the date
		// - on different dates, use range min - max
		// - only one booking, use date time format
		$partyDateTime = date("d M Y", $minDateTime);
		if ($minDateTime == $maxDateTime)
		{
			$partyDateTime = date("d M Y h:i A", $minDateTime);
		}
		elseif (date("d m y", $minDateTime) != date("d m y", $maxDateTime))
		{
			$partyDateTime = date("d/m/y", $minDateTime) . ' - ' . date("d/m/y", $maxDateTime);
		}

		return $partyDateTime;
	}

	public function makeApiCallWithUrl($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			return $res;

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;
			Log::error($res['error']);

			return false;
		}
	}

	protected function getMappingTypeData($typeId, $cityId, $eventId = null)
	{
		$data = [];

		switch ($typeId)
		{
			case config('evibe.ticket_type.packages'):
				$invalidTypes = [
					config('evibe.ticket_type.resort'),
					config('evibe.ticket_type.villa'),
					config('evibe.ticket_type.lounge'),
					config('evibe.ticket_type.food'),
					config('evibe.ticket_type.venue-deals'),
					config('evibe.ticket_type.couple-experiences'),
					config('evibe.ticket_type.priests'),
					config('evibe.ticket_type.tents')
				];

				$package = \VendorPackage::whereNotIn('type_ticket_id', $invalidTypes)
				                         ->where('city_id', $cityId)
				                         ->where('is_live', 1);

				if ($eventId)
				{
					$package = $package->where('event_id', $eventId);
				}
				$packages = $package->get();

				foreach ($packages as $package)
				{
					$data[] = [
						'id'   => $package->id,
						'name' => $package->name,
						'code' => $package->code,
						'link' => $package->getLink()
					];
				}

				break;

			case config('evibe.ticket_type.venue_halls'):
				$venues = \VenueHall::join('venue', 'venue.id', '=', 'venue_hall.venue_id')
				                    ->whereNull('venue.deleted_at')
				                    ->whereNull('venue_hall.deleted_at')
				                    ->where('venue.city_id', $cityId)
				                    ->where('venue.is_live', 1);

				if ($eventId)
				{
					$hallIds = VenueHallEvent::where('event_id', $eventId)->distinct()->pluck('venue_hall_id');
					$venues = $venues->whereIn('venue_hall.id', $hallIds);
				}

				$venues = $venues->select('venue_hall.id', 'venue_hall.venue_id', 'venue.name', 'venue_hall.code')->get();

				foreach ($venues as $venue)
				{
					$data[] = [
						'id'   => $venue->id,
						'name' => $venue->name,
						'code' => $venue->code,
						'link' => route('venues.hall', $venue->id),

					];
				}
				break;

			case config('evibe.ticket_type.trends'):
				$trend = \Trend::where('city_id', $cityId)->where('is_live', 1);

				if ($eventId)
				{
					$trendIds = TrendEvents::where('type_event_id', $eventId)->distinct()->pluck('trend_id');
					$trend = $trend->whereIn('id', $trendIds);
				}
				$trends = $trend->get();

				foreach ($trends as $trend)
				{
					$data[] = [
						'id'   => $trend->id,
						'name' => $trend->name,
						'code' => $trend->code,
						'link' => config('evibe.host') . 'trends/view/' . $trend->id,

					];
				}
				break;

			case config('evibe.ticket_type.cakes'):
				$cake = \Cake::join('planner', 'planner.id', '=', 'cake.provider_id')
				             ->where('planner.city_id', $cityId)
				             ->where('cake.is_live', 1)
				             ->whereNull('planner.deleted_at');

				if ($eventId)
				{
					$cakeIds = \CakeEvent::where('event_id', $eventId)->distinct()->pluck('cake_id');
					$cake = $cake->whereIn('cake.id', $cakeIds);
				}
				$cakes = $cake->select('cake.id', 'cake.title', 'cake.code')->get();

				foreach ($cakes as $cake)
				{
					$data[] = [
						'id'   => $cake->id,
						'name' => $cake->title,
						'code' => $cake->code,
						'link' => route('cakes.view.info', $cake->id),

					];
				}
				break;

			case config('evibe.ticket_type.decors'):
				$decor = \Decor::join('planner', 'planner.id', '=', 'decor.provider_id')
				               ->where('planner.city_id', $cityId)
				               ->where('decor.is_live', 1)
				               ->whereNull('planner.deleted_at');

				if ($eventId)
				{
					$decorIds = DecorEvent::where('event_id', $eventId)->distinct()->pluck('decor_id');

					$decor = $decor->whereIn('decor.id', $decorIds);
				}
				$decors = $decor->select('decor.id', 'decor.code', 'decor.name')->get();

				foreach ($decors as $decor)
				{
					$data[] = [
						'id'   => $decor->id,
						'name' => $decor->name,
						'code' => $decor->code,
						'link' => route('decor.info.view', $decor->id),

					];
				}
				break;

			case config('evibe.ticket_type.entertainments'):
				$ent = TypeServices::where('city_id', $cityId);

				if ($eventId)
				{
					$entIds = ServiceEvent::where('type_event_id', $eventId)->distinct()->pluck('type_service_id');
					$ent = $ent->whereIn('id', $entIds);
				}
				$services = $ent->get();

				foreach ($services as $service)
				{
					$data[] = [
						'id'   => $service->id,
						'name' => $service->name,
						'code' => $service->code,
						'link' => route('services.details', $service->id)
					];
				}
				break;

			case config('evibe.ticket_type.resort'):
				$data = $this->getOtherPackageType($cityId, config('evibe.ticket_type.resort'), $eventId);
				break;

			case config('evibe.ticket_type.villa'):
				$data = $this->getOtherPackageType($cityId, config('evibe.ticket_type.villa'), $eventId);
				break;

			case config('evibe.ticket_type.lounge'):
				$data = $this->getOtherPackageType($cityId, config('evibe.ticket_type.lounge'), $eventId);
				break;

			case config('evibe.ticket_type.food'):
				$data = $this->getOtherPackageType($cityId, config('evibe.ticket_type.food'), $eventId);
				break;

			case config('evibe.ticket_type.venue-deals'):
				$data = $this->getOtherPackageType($cityId, config('evibe.ticket_type.venue-deals'), $eventId);
				break;

			case config('evibe.ticket_type.couple-experiences'):
				$data = $this->getOtherPackageType($cityId, config('evibe.ticket_type.couple-experiences'), $eventId);
				break;

			case config('evibe.ticket_type.tents'):
				$data = $this->getOtherPackageType($cityId, config('evibe.ticket_type.tents'), $eventId);
				break;
		}

		return $data;
	}

	protected function getOtherPackageType($cityId, $pageType, $eventId)
	{
		$data = [];
		$packages = \VendorPackage::where('city_id', $cityId)
		                          ->where('is_live', 1)
		                          ->where('type_ticket_id', $pageType);

		if ($eventId)
		{
			$packages = $packages->where('event_id', $eventId);
		}

		$packages = $packages->get();

		foreach ($packages as $item)
		{
			$data[] = [
				'id'   => $item->id,
				'name' => $item->name,
				'code' => $item->code,
				'link' => $item->getLink(),
			];
		}

		return $data;
	}

	public function trackUserInfo()
	{
		if (env('APP_DATA_LOGGER', true))
		{
			$user = Auth::user();
			$userId = $user ? $user->id : "";

			$dataToLog = 'IP Address-' . \request()->ip() . " UserId-" . $userId . ' URL-(' . \request()->method() . ')' . url()->previous() . "\n";

			Log::info($dataToLog);
		}

		return \request();
	}
}
