<?php

namespace App\Http\Controllers\Base;

use App\Models\VendorFollowup;
use App\Models\VendorUpdate;
use App\Models\VendorSignup;

use App\Http\Requests;
use Area;
use Carbon\Carbon;
use City;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use TypeVendor;
use Vendor;

class BaseVendorController extends BaseController
{
	protected function generateVendorCode($cityId, $typeId, $lastCode = null)
	{
		$typeObj = TypeVendor::findOrFail($typeId);
		$cityObj = City::findOrFail($cityId);

		if (is_null($lastCode))
		{
			$lastObj = Vendor::withTrashed()
			                  ->where('city_id', $cityId)
			                  ->where('type_id', $typeId)
			                  ->orderBy('code', 'DESC')
			                  ->first();
			if (!$lastObj)
			{
				$lastCode = $typeObj->default_code;
			}
			else
			{
				$lastCode = $lastObj->code;
			}

			$digits = ((int)substr($lastCode, -4)) + 1;
		}
		else
		{
			$digits = (int)substr($lastCode, -4);
		}

		$digits = str_pad($digits, 4, '0', STR_PAD_LEFT);

		return $cityObj->code . $typeObj->code . $digits;
	}

	protected function generateVendorUrl($vendorType, $areaId)
	{
		$area = Area::findOrFail($areaId);
		$areaName = strtolower($area->name);
		$areaName = str_replace(' ', '-', $areaName);

		$vt = TypeVendor::findOrFail($vendorType);
		$vendorTypeName = strtolower($vt->name);
		$vendorTypeName = str_replace(' ', '-', $vendorTypeName);

		$urlTpl = config('evibe.urls.vendor');
		$replaces = [
			'#field1#' => time(),
			'#field2#' => $vendorTypeName,
			'#field3#' => $areaName,
			'#field4#' => $area->city->url
		];

		$url = str_replace(array_keys($replaces), array_values($replaces), $urlTpl);

		return $url;
	}

	// Add vendor update
	protected function saveVendorUpdate($vendor, $isExisting = true)
	{
		$userInput = Input::all();
		$rules = ['vUDate' => 'required|date'];

		$messages = [
			'vUDate.required' => 'Please select update date',
			'vUDate.date'     => 'Update date is invalid'
		];

		$validator = Validator::make($userInput, $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()
			                 ->withErrors($validator, 'updateError')
			                 ->withInput();

		}
		else
		{
			$vendorUpdateData = [
				'vendor'   => $vendor,
				'statusAt' => strtotime(Input::get('vUDate')),
				'source'   => Input::get('vUSource'),
				'comments' => Input::get('vUComments'),
			];

			!$isExisting ? $vendorUpdateData['statusId'] = Input::get('status') : null;

			$this->updateVendorAction($vendorUpdateData, $isExisting);

			return redirect()->back();
		}
	}

	//vendor followup section
	protected function saveFollowup($vendor, $isExisting = true)
	{
		$followupDate = Input::get('followupDate');
		$followupComments = Input::get('followupComment');

		$rules = [
			'followupDate' => 'required|date|after:now',
		];
		$messages = [
			'followupDate.required' => 'Followup Date is required',
			'followupDate.date'     => 'Followup Date is invalid',
			'followupDate.after'    => 'Followup Date should be after current date',
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator, 'followupError')
			                 ->withInput();
		}

		$followupData = [
			'followup_date' => strtotime($followupDate),
			'comments'      => $followupComments,
			'handler_id'    => Auth::user()->id,
			'map_type_id'   => config('evibe.ticket_type.planners'),
		];

		if ($isExisting)
		{
			$followupData['map_id'] = $vendor->id;
			$followupData['vendor_signup_id'] = $vendor->signup_id;
			$isFollowup = VendorFollowup::where('map_id', $vendor->id)
			                            ->whereNull('is_complete')
			                            ->first();
		}
		else
		{
			$isFollowup = VendorFollowup::where('vendor_signup_id', $vendor->id)
			                            ->whereNull('map_id')
			                            ->whereNull('is_complete')
			                            ->first();

			$followupData['vendor_signup_id'] = $vendor->id;
		}

		if ($isFollowup)
		{
			$saveVendorFollowup = $isFollowup->update([
				                                          'followup_date' => strtotime($followupDate),
				                                          'comments'      => $followupComments,
				                                          'handler_id'    => Auth::user()->id,
			                                          ]);
		}
		else
		{
			$saveVendorFollowup = VendorFollowup::create($followupData);
		}

		if ($saveVendorFollowup)
		{
			$vendorUpdateComment = $followupComments . ' at ' . date('d/m/Y', strtotime($followupDate));

			$vendorUpdate = [
				'vendor'   => $vendor,
				'comments' => $vendorUpdateComment
			];

			$this->updateVendorAction($vendorUpdate, $isExisting);

		}

		return redirect()->back();
	}

	protected function completeFollowup($followupId, $isExisting = true)
	{
		$res = ['success' => false];
		$followup = VendorFollowup::find($followupId);
		$comment = Input::get('doneComment');

		if ($followup)
		{
			$updateData = [
				'handler_id'   => Auth::user()->id,
				'is_complete'  => 1,
				'done_comment' => $comment
			];
			if ($followup->update($updateData))
			{
				$vendorUpdateComment = 'Followups marked as done';
				if ($isExisting)
				{
					$vendorId = $followup->map_id;
					$vendor = Vendor::find($vendorId);
				}

				else
				{
					$vendorId = $followup->vendor_signup_id;
					$vendor = VendorSignup::find($vendorId);
				}
				$vendorUpdate = [
					'vendor'   => $vendor,
					'comments' => $vendorUpdateComment
				];

				$this->updateVendorAction($vendorUpdate, $isExisting);

				$res = ['success' => true];
			}
		}
		else
		{
			$res = [
				'success' => false,
				'error'   => 'Followup data not found'
			];
		}

		return response()->json($res);
	}

	protected function updateFollowup($followupId)
	{
		$res['success'] = false;
		$followup = VendorFollowup::find($followupId);
		if ($followup)
		{
			$rules = [
				'date' => 'required|date|after:now',
			];
			$messages = [
				'date.required' => 'Followup Date is required',
				'date.date'     => 'Followup Date is invalid',
				'date.after'    => 'Followup Date should be after current date',
			];

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validator->messages()->first()
				                        ]);
			}

			$updateData = [
				'followup_date' => strtotime(Input::get('date')),
				'comments'      => Input::get('comment')
			];

			if ($followup->update($updateData))
			{
				$res['success'] = true;
			}
		}
		else
		{
			$res = [
				'success' => false,
				'error'   => "Followup data not found"
			];
		}

		return response()->json($res);
	}

	protected function deleteFollowup($followupId)
	{
		$followup = VendorFollowup::find($followupId);

		$res['success'] = false;
		if ($followup)
		{
			if ($followup->delete())
			{
				$res['success'] = true;
			}

			return response()->json($res);
		}
		else
		{
			$res = [
				'success' => false,
				'error'   => 'followup data not found'
			];
		}

		return response()->json($res);
	}

	protected function updateVendorAction($data, $isExisting = true)
	{
		$source = 'Other';
		$vendor = $data['vendor'];
		$statusId = $vendor->status_id;
		$statusAt = time();

		if (array_key_exists('statusAt', $data))
		{
			$statusAt = $data['statusAt'];
		}

		if (array_key_exists('source', $data))
		{
			$source = $data['source'];
		}
		if (array_key_exists('statusId', $data))
		{
			$statusId = $data['statusId'];
		}

		$updateData = [
			'handler_id'  => Auth::user()->id,
			'comments'    => $data['comments'],
			'status_id'   => $statusId,
			'status_at'   => $statusAt,
			'source'      => $source,
			'map_type_id' => config('evibe.ticket_type.planners'),
		];

		if ($isExisting)
		{
			$updateData['map_id'] = $vendor->id;
			$updateData['vendor_signup_id'] = $vendor->vendor_signup_id;
		}
		else
		{
			$updateData['vendor_signup_id'] = $vendor->id;
		}

		$vendorUpdate = VendorUpdate::create($updateData);

		if ($vendorUpdate)
		{
			$vendor->handler_id = $this->getPlannerHandler($vendor);
			$vendor->updated_at = Carbon::now();
			$vendor->status_id = $statusId;
			$vendor->save();
		}
	}

	protected function getPlannerHandler($vendor)
	{
		$handlerId = $vendor->handler_id;

		if (Auth::user()->role_id == config('evibe.roles.operations')
			|| Auth::user()->role_id == config('evibe.roles.bd')
			|| Auth::user()->role_id == config('evibe.roles.sr_bd')
		)
		{
			$handlerId = Auth::user()->id;
		}

		return $handlerId;
	}

	public function generateShortUrl($name, $id)
	{
		$name = str_replace(" ", "", $name);
		$shortUrl = strlen($name) >= 4 ? substr($name, 0, 4) : str_pad($name, 4, 'evibe');
		$id = $this->getReverseId($id);

		return strtolower($shortUrl . $id);
	}

	private function getReverseId($num)
	{
		$rev = 0;

		while ($num > 0)
		{
			$temp = $num % 10;
			$rev = $rev * 10 + $temp;
			$num = intval($num / 10);
		}

		return $rev;
	}

}
