<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Approval\BaseApprovalController;
use App\Http\Requests;
use App\Models\Util\PackageFieldValue;
use App\Models\Util\TypePackageField;
use App\Models\Util\TypePackageFieldCategory;
use Illuminate\Support\Facades\Input;
use Tags;
use Vendor;
use VendorPackage;
use Venue;

class BasePackageController extends BaseApprovalController
{
	protected function getExistingQueryParams()
	{
		$existingQueryParams = [];

		foreach (Input::all() as $key => $value)
		{
			if ($key != 'page')
			{
				$existingQueryParams[$key] = urldecode($value);
			}
		}

		return $existingQueryParams;
	}

	protected function getFilterableCategories($tagIds)
	{
		$categoryFilters = [
			'filters' => [],
			'all'     => []
		];
		$childIds = [];
		$validIds = [];

		// getting the value of parent tags
		foreach ($tagIds as $tagId)
		{
			array_push($validIds, $tagId);
		}

		$validTagsList = Tags::whereIn('id', $tagIds)->get();
		foreach ($validTagsList as $tag)
		{
			if ($tag->parent_id)
			{
				$parentId = $tag->parent_id;
				if (!in_array($parentId, $validIds))
				{
					array_push($validIds, $parentId);
				}
			}
		}
		$validTags = $validTagsList = Tags::whereIn('id', $validIds)->get();

		// iterate through each tag
		foreach ($validTags as $tag)
		{

			$categoryFilters['id'] = $tag->id;
			if (!in_array($categoryFilters['id'], $childIds))
			{
				$tagUrl = $tag->url ? $tag->url : $this->generateUrl($tag->name);
				$tagData = $this->fillTagData($tag);
				$categoryFilters['filters'][$tagUrl] = $tagData;
				$categoryFilters['filters'][$tagUrl] = $tagData;
				$categoryFilters['all'][$tagUrl] = $tagData;

				$subTags = Tags::where('parent_id', $tag->id)->get();
				foreach ($subTags as $subTag)
				{
					if (in_array($subTag->id, $validIds))
					{
						$subTagUrl = $subTag->url ? $subTag->url : $this->generateUrl($subTag->name);
						$subTagData = $this->fillTagData($subTag);
						$categoryFilters['filters'][$tagUrl]['subTags'][$subTagUrl] = $subTagData;
						$categoryFilters['all'][$subTagUrl] = $subTagData;
						array_push($childIds, $subTag->id);
					}
				}
			}
		}

		return $categoryFilters;
	}

	protected function fillTagData($tag)
	{
		$data = [
			'id'   => $tag->id,
			'name' => $tag->identifier ? $tag->identifier : ucfirst($tag->name),
			'url'  => $tag->url ? $tag->url : $this->generateUrl($tag->name),
		];

		return $data;
	}

	protected function getVenuePackageDynamicField($package)
	{
		$fields = [];
		$markupArray = [];

		$dynamicFieldId = PackageFieldValue::where('package_id', $package->id)->distinct()->pluck('type_package_field_id');
		$dynamicFields = TypePackageField::whereIn('id', $dynamicFieldId)->get();

		foreach ($dynamicFields as $dynamicField)
		{
			$fieldCat = TypePackageFieldCategory::find($dynamicField->type_cat_id);

			if (!$fieldCat)
			{
				continue;
			}

			$checkboxValue = [];

			if ($dynamicField->type_field_id == config('evibe.input.checkbox'))
			{
				$values = PackageFieldValue::where('package_id', $package->id)
				                           ->where('type_package_field_id', $dynamicField->id)
				                           ->get();

				foreach ($values as $value)
				{

					$checkboxValue[] = $value->value;
				}

				$name = $dynamicField->meta_name;
				$fields['name'] = ucfirst($name);
				$fields['value'] = implode(", ", $checkboxValue);

			}
			else
			{
				$value = PackageFieldValue::where('package_id', $package->id)
				                          ->where('type_package_field_id', $dynamicField->id)
				                          ->first()->value;

				$name = $dynamicField->meta_name;
				$fields['name'] = $name;
				$fields['value'] = $value;
			}

			$markupArray[$fieldCat->name][] = $fields;
		}

		return $markupArray;
	}

	protected function getPackageProviders($packageId)
	{
		$package = VendorPackage::select("id", "map_type_id")->find($packageId);

		if ($package)
		{
			$mapTypeId = $package->map_type_id;
			if ($mapTypeId == config('evibe.ticket_type.venues'))
			{
				$provider = Venue::select("id", "name")->orderBy('name')->get()->toArray();
				$type = "venues";
			}
			else
			{
				$provider = Vendor::select("id", "name")->orderBy('name')->get()->toArray();
				$type = "planner";
			}

			return ['provider' => $provider, 'type' => $type];
		}

		return null;
	}
}
