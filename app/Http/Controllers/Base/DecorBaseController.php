<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Approval\ApprovalController;
use App\Http\Requests;
use Decor;
use DecorGallery;

class DecorBaseController extends ApprovalController
{
	public function generateDecorGalleryCode()
	{
		$decorPrefix = "" . config('evibe.code.prefix.decor_gallery'); // convert to string
		$digits = config('evibe.code.default.decor_gallery');
		$lastObj = DecorGallery::withTrashed()->orderBy('updated_at', 'DESC')->first();
		if ($lastObj)
		{
			if ($lastObj)
			{
				$lastCode = $lastObj->code;
				$digits = ((int)substr($lastCode, -4)) + 1;
			}
		}
		else
		{
			$digits = (int)substr($digits, -4);
		}

		return $decorPrefix . $digits;
	}

	public function generateDecorCode()
	{
		$decorPrefix = "" . config('evibe.code.prefix.decor'); // convert to string
		$digits = config('evibe.code.default.decor');
		$lastObj = Decor::withTrashed()->orderBy('id', 'DESC')->first();
		if ($lastObj)
		{
			if ($lastObj)
			{
				$lastCode = $lastObj->code;
				$digits = ((int)substr($lastCode, -4)) + 1;
			}
		}
		else
		{
			$digits = (int)substr($digits, -4);
		}

		return $decorPrefix . $digits;
	}

	protected function generateDecorUrl($name)
	{
		return $this->generateUrl($name);
	}
}
