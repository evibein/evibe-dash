<?php

namespace App\Http\Controllers\Stories;

use App\Models\Story\VendorStory;
use App\Models\TypeEvent;
use App\Http\Controllers\Base\BaseController;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VendorStoriesController extends BaseController
{
	public function viewVendorStories(Request $request)
	{

		$events = TypeEvent::all();
		$stories = VendorStory::orderBy(DB::raw('ISNULL(priority)', 'ASC'))
		                      ->orderBy('priority', 'ASC');
		$reorderUrl = route('stories.vendor.reorder');

		$eventFilter = $request->input('event');

		if ($eventFilter)
		{
			$eventId = TypeEvent::where('name', $eventFilter)->select('id')->first()->id;
			$stories->where('event_id', $eventId);
		}

		if (request()->has('reorder') && request()->input('reorder') == 'true' && !request('event'))
		{
			$stories = $stories->get();
		}
		else
		{
			$stories = $stories->paginate(20);
		}

		$data = [
			'stories'    => $stories,
			'events'     => $events,
			'reorderUrl' => $reorderUrl
		];

		return view('stories.vendor.list')->with($data);
	}

	public function addVendorStory()
	{
		$events = TypeEvent::all();
		$cities = \City::all();
		$vendors = \Vendor::all();
		$venues = \Venue::all();

		$data = [
			'events'  => $events,
			'cities'  => $cities,
			'vendors' => $vendors,
			'venues'  => $venues
		];

		return view('stories.vendor.add')->with($data);
	}

	public function saveVendorStory(Request $request)
	{
		$vendorId = '';
		$fileType = $request->input('fileType');
		$image = $request->file('image');
		$allInput = $request->all();
		$type = $request->input('type');

		$rules = ['type' => 'required',];

		if ($type == config('evibe.ticket_type.venues'))
		{
			$rules['venue'] = 'required|integer|min:1';
			$messages['venue.min'] = "Please select a valid name";
			$vendorId = $request->input('venue');
		}
		elseif ($type == config('evibe.ticket_type.planners'))
		{
			$rules['vendor'] = 'required|integer|min:1';
			$messages['vendor.min'] = "Please select a valid name";
			$vendorId = $request->input('vendor');
		}

		$rules['event'] = 'required';
		$rules['story'] = 'required|min:10';
		$rules['fileType'] = 'required';

		$messages = [
			'vendor.required' => 'Name is required',
			'venue.required'  => 'Name is required',
			'story.required'  => 'Vendor story is required',
			'story.min'       => 'Vendor story should be minimum 10 character long'
		];

		if ($fileType == config('evibe.gallery.type.image'))
		{
			$rules['image'] = 'required|mimes:png,jpeg,jpg,JPG';
			$messages['image.required'] = 'Please select one image';
			$messages['image.mimes'] = 'Please select a valid image (only png, jpeg, JPG is allowed )';
			$allInput['image'] = $image;
		}

		elseif ($fileType == config('evibe.gallery.type.video'))
		{
			$rules['videoLink'] = 'unique:stories_customer,url';
			$rules['videoLink'] = 'required';
			$messages['videoLink.required'] = 'Please enter video link';
			$messages['videoLink.unique'] = 'Video link already exist';
		}

		$validator = Validator::make($allInput, $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()
			                 ->withErrors($validator)
			                 ->withInput();
		}

		$url = '';
		if ($fileType == config('evibe.gallery.type.image'))
		{
			//upload the image
			$uploadPath = '/story/vendor/';
			$imageName = $this->uploadImageToServer($image, $uploadPath, ['isWatermark' => true, 'isOptimize' => false]);

			$url = $imageName;
		}

		elseif ($fileType == config('evibe.gallery.type.video'))
		{
			//upload a video
			$url = $request->input('videoLink');
		}

		VendorStory::create([
			                    'story'       => $request->input('story'),
			                    'map_id'      => $vendorId,
			                    'map_type_id' => $type,
			                    'type_id'     => $fileType,
			                    'url'         => $url,
			                    'event_id'    => $request->input('event')
		                    ]);

		return redirect()->route('stories.vendor.list')
		                 ->with('successMsg', 'New stories created successfully');
	}

	public function showEditVendorStory($storyId)
	{
		$customerStory = VendorStory::findOrFail($storyId);
		$events = TypeEvent::all();
		$cities = \City::all();
		$vendors = \Vendor::all();
		$venues = \Venue::all();

		$data = [
			'events'  => $events,
			'cities'  => $cities,
			'vendors' => $vendors,
			'venues'  => $venues,
			'story'   => $customerStory
		];

		return view('stories.vendor.edit')->with($data);
	}

	public function updateVendorStory($storyId, Request $request)
	{
		$story = VendorStory::findOrFail($storyId);
		$vendorId = '';
		$fileType = $request->input('fileType');
		$image = $request->file('image');
		$allInput = $request->all();
		$type = $request->input('type');

		$rules = ['type' => 'required',];

		if ($type == config('evibe.ticket_type.venues'))
		{
			$rules['venue'] = 'required|integer|min:1';
			$messages['venue.min'] = "Please select a valid name";
			$vendorId = $request->input('venue');
		}
		elseif ($type == config('evibe.ticket_type.planners'))
		{
			$rules['vendor'] = 'required|integer|min:1';
			$messages['vendor.min'] = "Please select a valid name";
			$vendorId = $request->input('vendor');
		}

		$rules['event'] = 'required';
		$rules['story'] = 'required|min:10';
		$rules['fileType'] = 'required';

		$messages = [
			'vendor.required' => 'Name is required',
			'venue.required'  => 'Name is required',
			'story.required'  => 'Vendor story is required',
			'story.min'       => 'Vendor story should be minimum 10 character long'
		];

		if ($fileType == config('evibe.gallery.type.image') && (!$story->url || $story->type_id == config('evibe.gallery.type.video')))
		{
			$rules['image'] = 'required|mimes:png,jpeg,jpg,JPG';
			$messages['image.required'] = 'Please select one image';
			$messages['image.mimes'] = 'Please select a valid image (only png, jpeg, JPG is allowed )';
			$allInput['image'] = $image;
		}

		elseif ($fileType == config('evibe.gallery.type.video'))
		{
			$rules['videoLink'] = 'unique:stories_customer,url';
			$rules['videoLink'] = 'required';
			$messages['videoLink.required'] = 'Please enter video link';
			$messages['videoLink.unique'] = 'Video link already exist';
		}

		$validator = Validator::make($allInput, $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()
			                 ->withErrors($validator)
			                 ->withInput();
		}

		$url = null;
		$updateData = [
			'story'       => $request->input('story'),
			'map_id'      => $vendorId,
			'map_type_id' => $type,
			'type_id'     => $fileType,
			'event_id'    => $request->input('event')
		];

		if ($fileType == config('evibe.gallery.type.image') && (!$story->url || $story->type_id == config('evibe.gallery.type.video')))
		{
			//upload the image
			$uploadPath = '/story/vendor/';
			$imageName = $this->uploadImageToServer($image, $uploadPath, ['isWatermark' => true, 'isOptimize' => false]);

			$url = $imageName;
		}

		elseif ($fileType == config('evibe.gallery.type.video'))
		{
			//upload a video
			$url = $request->input('videoLink');
		}

		if ($url)
		{
			$updateData['url'] = $url;
		}

		$story->update($updateData);

		return redirect()->route('stories.vendor.list')
		                 ->with('successMsg', 'vendor story updated successfully');
	}

	public function removeVendorStoryImage($storyId)
	{
		$story = VendorStory::where('type_id', config('evibe.gallery.type.image'))->find($storyId);

		if ($story)
		{
			$story->update(['url' => null]);
			$message = 'Image removed successfully';
		}
		else
		{
			$message = 'Some error occurred, please try again.';
		}

		return redirect()->back()->with('message', $message);
	}

	public function deleteVendorStory($storyId)
	{
		$story = VendorStory::findOrFail($storyId);

		if ($story->delete())
		{
			return redirect()->back()
			                 ->with('successMsg', 'Story deleted successfully');
		}
	}

	public function saveStoryPriority(Request $request)
	{
		$ids = $request->input('order');
		$res = ['success' => false];

		if (count($ids) > 0)
		{
			$counter = 1;

			foreach ($ids as $id)
			{
				VendorStory::findOrFail($id)->update(['priority' => $counter]);
				$counter++;

			}

			if (count($ids) == $counter - 1)
			{
				$res = ['success' => true, 'redirect' => route('stories.vendor.list')];
			}
		}
		else
		{
			$res = ['success' => false];
		}

		return response()->json($res);
	}

}
