<?php

/**
 * @Since 17th May 2016 By Vikash <vikash@evibe.in>
 * Customer Story : Add, Edit, Delete,
 */

namespace App\Http\Controllers\Stories;

use App\Models\Story\CustomerStory;
use App\Models\TypeEvent;
use App\Http\Controllers\Base\BaseController;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CustomerStoriesController extends BaseController
{
	public function viewCustomerStories(Request $request)
	{
		$events = TypeEvent::all();
		$stories = CustomerStory::orderBy(DB::raw('ISNULL(priority)', 'ASC'))
		                        ->orderBy('priority', 'ASC');

		$eventFilter = $request->input('event');
		$reorderUrl = route('stories.customer.reorder');

		if ($eventFilter)
		{
			$eventId = TypeEvent::where('name', $eventFilter)->select('id')->first()->id;
			$stories->where('event_id', $eventId);
		}
		if (request()->has('reorder') && request()->input('reorder') == 'true' && !request('event'))
		{
			$stories = $stories->get();
		}
		else
		{
			$stories = $stories->paginate(20);
		}

		return view('stories.customer.list', compact('stories', 'events', 'reorderUrl'));
	}

	public function addCustomerStory()
	{
		$events = TypeEvent::all();
		$cities = \City::all();

		return view('stories.customer.add', compact('events', 'cities'));
	}

	public function saveCustomerStory(Request $request)
	{
		$fileType = $request->input('fileType');
		$image = $request->file('image');
		$allInput = $request->all();

		$rules = [
			'name'     => 'required',
			'event'    => 'required',
			'city'     => 'required',
			'story'    => 'required|min:10',
			'fileType' => 'required'
		];

		$messages = [
			'name.required'  => 'Customer name is required',
			'story.required' => 'Customer story is required',
			'story.min'      => 'Customer story should be minimum 10 character long'
		];

		if ($fileType == config('evibe.gallery.type.image'))
		{
			$rules['image'] = 'required|mimes:png,jpeg,jpg,JPG';
			$messages['image.required'] = 'Please select one image';
			$messages['image.mimes'] = 'Please select a valid image (only png, jpeg, JPG is allowed )';
			$allInput['image'] = $image;
		}

		elseif ($fileType == config('evibe.gallery.type.video'))
		{
			$rules['videoLink'] = 'unique:stories_customer,url';
			$rules['videoLink'] = 'required';
			$messages['videoLink.required'] = 'Please enter video link';
			$messages['videoLink.unique'] = 'Video link already exist';
		}

		$validator = Validator::make($allInput, $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()
			                 ->withErrors($validator)
			                 ->withInput();
		}

		$url = '';
		if ($fileType == config('evibe.gallery.type.image'))
		{
			//upload the image
			$uploadPath = '/story/customer/';
			$imageName = $this->uploadImageToServer($image, $uploadPath, ['isWatermark' => true, 'isOptimize' => false]);

			$url = $imageName;
		}

		elseif ($fileType == config('evibe.gallery.type.video'))
		{
			//upload a video
			$url = $request->input('videoLink');
		}

		CustomerStory::create([
			                      'name'     => $request->input('name'),
			                      'story'    => $request->input('story'),
			                      'url'      => $url,
			                      'city_id'  => $request->input('city'),
			                      'type_id'  => $fileType,
			                      'event_id' => $request->input('event')
		                      ]);

		return redirect()->route('stories.customer.list')
		                 ->with('successMsg', 'New stories created successfully');
	}

	public function showEditCustomerStory($storyId)
	{
		$customerStory = CustomerStory::findOrFail($storyId);
		$events = TypeEvent::all();
		$cities = \City::all();

		$data = [
			'events' => $events,
			'cities' => $cities,
			'story'  => $customerStory
		];

		return view('stories.customer.edit')->with($data);
	}

	public function updateCustomerStory($storyId, Request $request)
	{
		$story = CustomerStory::findOrFail($storyId);

		$fileType = $request->input('fileType');
		$image = $request->file('image');
		$allInput = $request->all();

		$rules = [
			'name'     => 'required',
			'event'    => 'required',
			'city'     => 'required',
			'story'    => 'required|min:10',
			'fileType' => 'required'
		];

		$messages = [
			'name.required'  => 'Customer name is required',
			'story.required' => 'Customer story is required',
			'story.min'      => 'Customer story should be minimum 10 character long'
		];

		if ($fileType == config('evibe.gallery.type.image') && (!$story->url || $story->type_id == config('evibe.gallery.type.video')))
		{
			$rules['image'] = 'required|mimes:png,jpeg,jpg,JPG';
			$messages['image.required'] = 'Please select one image';
			$messages['image.mimes'] = 'Please select a valid image (only png, jpeg, JPG is allowed )';
		}

		elseif ($fileType == config('evibe.gallery.type.video'))
		{
			$rules['videoLink'] = 'unique:stories_customer,url';
			$rules['videoLink'] = 'required';
			$messages['videoLink.required'] = 'Please enter video link';
			$messages['videoLink.unique'] = 'Video link already exist';
		}

		$validator = Validator::make($allInput, $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()
			                 ->withErrors($validator)
			                 ->withInput();
		}

		$updateData = [
			'name'     => $request->input('name'),
			'story'    => $request->input('story'),
			'city_id'  => $request->input('city'),
			'type_id'  => $fileType,
			'event_id' => $request->input('event')
		];

		$url = '';
		if ($fileType == config('evibe.gallery.type.image') && (!$story->url || $story->type_id == config('evibe.gallery.type.video')))
		{
			//upload the image
			$uploadPath = '/story/customer/';
			$imageName = $this->uploadImageToServer($image, $uploadPath, ['isWatermark' => true, 'isOptimize' => false]);

			$updateData['url'] = $imageName;
		}

		elseif ($fileType == config('evibe.gallery.type.video'))
		{
			//upload a video
			$updateData['url'] = $request->input('videoLink');
		}

		$story->update($updateData);

		return redirect()->route('stories.customer.list')
		                 ->with('successMsg', 'Story updated successfully');
	}

	public function removeCustomerStoryImage($storyId)
	{
		$story = CustomerStory::where('type_id', config('evibe.gallery.type.image'))->find($storyId);

		if ($story)
		{
			$story->update(['url' => null]);
			$message = 'Image removed successfully';
		}
		else
		{
			$message = 'Some error occurred, please try again.';
		}

		return redirect()->back()->with('message', $message);
	}

	public function deleteCustomerStory($storyId)
	{
		$story = CustomerStory::findOrFail($storyId);

		if ($story->delete())
		{
			return redirect()->back()
			                 ->with('successMsg', 'Story deleted successfully');
		}
	}

	public function saveStoryPriority(Request $request)
	{
		$ids = $request->input('order');
		$res = ['success' => false];

		if (count($ids) > 0)
		{
			$counter = 1;

			foreach ($ids as $id)
			{
				CustomerStory::findOrFail($id)->update(['priority' => $counter]);
				$counter++;

			}

			if (count($ids) == $counter - 1)
			{
				$res = ['success' => true, 'redirect' => route('stories.customer.list')];
			}
		}
		else
		{
			$res = ['success' => false];
		}

		return response()->json($res);
	}
}