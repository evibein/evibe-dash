<?php

namespace App\Http\Controllers\Stories;

use App\Http\Controllers\Base\BaseController;
use App\Models\ServiceGallery;
use App\Models\Story\SearchableProduct;

class ImagesDownloadController extends BaseController
{
	public function getAllImageCodes()
	{
		$searchableProducts = SearchableProduct::select('id', 'code')->whereNull('deleted_at')->get()->toArray();

		$data = [
			'searchableProducts' => $searchableProducts
		];

		return view('stories/get-product-codes', ['data' => $data]);
	}

	public function showAllImages($productCode)
	{
		$searchableProductInstance = SearchableProduct::find($productCode);

		if ($searchableProductInstance)
		{
			$data = [];

			if ($searchableProductInstance->option_type_id == config('evibe.ticket_type.decors'))
			{
				$decorImages = \DecorGallery::where('decor_id', $searchableProductInstance->option_id)->get();
				if ($decorImages)
				{
					$data = [
						'imageDetails' => $decorImages,
						'selectedCode' => $searchableProductInstance->code,
						'imageType'    => $searchableProductInstance->option_type_id
					];
				}
			}
			elseif ($searchableProductInstance->option_type_id == config('evibe.ticket_type.cakes'))
			{
				$cakeImages = \CakeGallery::where('cake_id', $searchableProductInstance->option_id)->get();
				$data =
					[
						'imageDetails' => $cakeImages,
						'selectedCode' => $searchableProductInstance->code,
						'imageType'    => $searchableProductInstance->option_type_id
					];
			}
			elseif ($searchableProductInstance->option_type_id == config('evibe.ticket_type.packages'))
			{
				$packageImages = \PackageGallery::where('planner_package_id', $searchableProductInstance->option_id)->get();

				$data =
					[
						'imageDetails' => $packageImages,
						'selectedCode' => $searchableProductInstance->code,
						'imageType'    => $searchableProductInstance->option_type_id
					];
			}
			elseif ($searchableProductInstance->option_type_id == config('evibe.ticket_type.trends'))
			{
				$trendsImages = \TrendGallery::where('trending_id', $searchableProductInstance->option_id)->get();

				$data =
					[
						'imageDetails' => $trendsImages,
						'selectedCode' => $searchableProductInstance->code,
						'imageType'    => $searchableProductInstance->option_type_id
					];
			}
			elseif ($searchableProductInstance->option_type_id == config('evibe.ticket_type.services'))
			{
				$servicesImages = ServiceGallery::where('type_service_id', $searchableProductInstance->option_id)->get();

				$data =
					[
						'imageDetails' => $servicesImages,
						'selectedCode' => $searchableProductInstance->code,
						'imageType'    => $searchableProductInstance->option_type_id
					];
			}

		}
		else
		{
			$data = null;
		}

		return view('stories/download-images', ['data' => $data]);
	}
}