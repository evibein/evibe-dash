<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Approval\BaseApprovalController;
use App\Http\Controllers\Base\BaseController;
use App\Models\Util\OptionAudit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class AuditController extends BaseApprovalController
{
	public function getAuditEditNewData()
	{
		$data = [];

		$data = array_merge($data, $this->columnSelectionData());

		//$data = array_merge($data, $this->getAuditData());
		$productType = Input::get('productType');
		$column = Input::get('column');
		$table = "";
		$optionData = [];
		if ($productType && $column)
		{
			switch ($productType)
			{
				case config('evibe.ticket_type.couple-experiences'):
					//$pendingReviewOptions =
					$table = 'planner_package';

					$pendingAuditOptionIds = OptionAudit::where('option_type_id', $productType)
					                                    ->where('table_name', $table)
					                                    ->where('column_name', $column)
					                                    ->whereNull('accepted_at')
					                                    ->whereNull('rejected_at')
					                                    ->pluck('option_id')
					                                    ->toArray();

					$rejectedAuditOptionIds = OptionAudit::where('option_type_id', $productType)
					                                     ->where('table_name', $table)
					                                     ->where('column_name', $column)
					                                     ->whereNull('accepted_at')
					                                     ->whereNotNull('rejected_at')
					                                     ->pluck('option_id')
					                                     ->toArray();

					$pendingAuditOptionIds = array_merge($pendingAuditOptionIds, $rejectedAuditOptionIds);
					//dd($pendingAuditOptionIds);

					// @see: hard coded for CLD
					$packageIds = \PackageTag::where('tile_tag_id', 186)->pluck('planner_package_id')->toArray();
					//dd($packageIds);

					$options = \VendorPackage::whereIn('id', $packageIds)
					                         ->where('planner_package.type_ticket_id', $productType)
					                         ->where('planner_package.is_live', 1)
					                         ->whereNull('planner_package.deleted_at');

					$options = $options->whereNotIn('planner_package.id', $pendingAuditOptionIds);

					$options = $options->get();

					foreach ($options as $option)
					{
						$partnerType = ($option->map_type_id == config('evibe.ticket_type.venues')) ? 'venues' : 'planner';

						array_push($optionData, [
							'auditId'       => '', // empty for new edits
							'id'            => $option->id,
							'name'          => $option->name,
							'code'          => $option->code,
							'url'           => config('evibe.host') . 'packages/view/' . $option->id,
							'price'         => $option->price,
							'priceWorth'    => $option->price_worth,
							'profilePicUrl' => $option->getProfilePic(),
							'auditColumn'   => $option->$column,
						]);
					}

				//dd($optionData);
			}
		}

		$perPage = 10;
		$optionData = $this->pagination($optionData, $perPage);

		$data['options'] = $optionData;
		$data['auditColumnName'] = $column;
		$data['auditTableName'] = $table;
		$data['optionTypeId'] = $productType;
		$data['filters'] = [
			'queryParams' => $this->getExistingQueryParams()
		];

		$data['referenceTab'] = config('evibe.reference.audit.edit-new');

		//dd($data);
		return view('audit.edit', ['data' => $data]);
	}

	public function saveAuditEditNewData(Request $request)
	{
		try
		{

			$data = $request->all();
			$optionId = $request->input('optionId');
			$optionTypeId = $request->input('optionTypeId');
			$tableName = $request->input('tableName');
			$columnName = $request->input('columnName');
			$columnData = $request->input('columnData');

			// check if any existing active audit exists
			$existingOptionAudit = OptionAudit::where([
				                                          'option_id'      => $optionId,
				                                          'option_type_id' => $optionTypeId,
				                                          'table_name'     => $tableName,
				                                          'column_name'    => $columnName,
			                                          ])
			                                  ->whereNull('accepted_at')
			                                  ->whereNull('rejected_at')
			                                  ->get();

			if (count($existingOptionAudit) > 0)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'An edit for the same column already exists for this option'
				                        ]);
			}

			OptionAudit::create([
				                    'option_id'      => $optionId,
				                    'option_type_id' => $optionTypeId,
				                    'table_name'     => $tableName,
				                    'column_name'    => $columnName,
				                    'data'           => $columnData,
				                    'created_at'     => Carbon::now(),
				                    'updated_at'     => Carbon::now()
			                    ]);

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}

	public function getAuditEditData($query = "new")
	{
		$data = [];

		$data = array_merge($data, $this->columnSelectionData());

		//$data = array_merge($data, $this->getAuditData());
		$productType = Input::get('productType');
		$column = Input::get('column');
		$table = "";
		$optionData = [];
		if ($productType && $column)
		{
			switch ($productType)
			{
				case config('evibe.ticket_type.couple-experiences'):
					//$pendingReviewOptions =
					$table = 'planner_package';

					if ($query == 'pending')
					{
						$auditOptions = OptionAudit::where('option_type_id', $productType)
						                           ->where('table_name', $table)
						                           ->where('column_name', $column)
						                           ->whereNull('accepted_at')
						                           ->whereNull('rejected_at')
						                           ->get();
					}
					elseif ($query == 'rejected')
					{
						$auditOptions = OptionAudit::where('option_type_id', $productType)
						                           ->where('table_name', $table)
						                           ->where('column_name', $column)
						                           ->whereNull('accepted_at')
						                           ->whereNotNull('rejected_at')
						                           ->get();
					}

					$auditOptionIds = $auditOptions->pluck('option_id');

					//dd($auditOptionIds);

					$options = \VendorPackage::where('planner_package.type_ticket_id', $productType)
					                         ->where('planner_package.is_live', 1)
					                         ->whereNull('planner_package.deleted_at');

					$options = $options->whereIn('planner_package.id', $auditOptionIds);

					$options = $options->get();

					foreach ($auditOptions as $auditOption)
					{
						$option = $options->where('id', $auditOption->option_id)->first();

						if (!$option)
						{
							return "Issue while fetching audit option data";
						}

						array_push($optionData, [
							'auditId'       => $auditOption->id,
							'id'            => $option->id,
							'name'          => $option->name,
							'code'          => $option->code,
							'url'           => config('evibe.host') . 'packages/view/' . $option->id,
							'price'         => $option->price,
							'priceWorth'    => $option->price_worth,
							'profilePicUrl' => $option->getProfilePic(),
							'auditColumn'   => $auditOption->data,
							'rejectReason'  => $auditOption->reject_comments
						]);
					}

				//dd($optionData);
			}

		}

		$perPage = 10;
		$optionData = $this->pagination($optionData, $perPage);

		$data['options'] = $optionData;
		$data['auditColumnName'] = $column;
		$data['auditTableName'] = $table;
		$data['optionTypeId'] = $productType;
		$data['filters'] = [
			'queryParams' => $this->getExistingQueryParams()
		];

		if ($query == 'pending')
		{
			$data['referenceTab'] = config('evibe.reference.audit.edit-pending');
		}
		elseif ($query == 'rejected')
		{
			$data['referenceTab'] = config('evibe.reference.audit.edit-rejected');
		}

		return view('audit.edit', ['data' => $data]);
	}

	public function saveAuditEditData($query = "new", Request $request)
	{
		try
		{

			$data = $request->all();
			$auditId = $request->input('auditId');
			$optionId = $request->input('optionId');
			$optionTypeId = $request->input('optionTypeId');
			$tableName = $request->input('tableName');
			$columnName = $request->input('columnName');
			$columnData = $request->input('columnData');

			$auditOption = OptionAudit::find($auditId);

			if (!$auditOption)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Unable to find the audit option row to update.'
				                        ]);
			}

			$auditOption->data = $columnData;
			$auditOption->rejected_at = null;
			$auditOption->updated_at = Carbon::now();
			$auditOption->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}

	public function getAuditReviewData()
	{
		$data = [];

		$data = array_merge($data, $this->columnSelectionData());

		$productType = Input::get('productType');
		$column = Input::get('column');
		$table = "";

		$auditOptions = OptionAudit::whereNull('accepted_at')
		                           ->whereNull('rejected_at')
		                           ->where('option_type_id', $productType)
		                           ->get();

		//dd($auditOptions);

		$optionData = [];
		if (count($auditOptions))
		{
			$auditOptionIds = $auditOptions->pluck('option_id');

			switch ($productType)
			{
				case config('evibe.ticket_type.couple-experiences'):
					$table = 'planner_package';

					$options = \VendorPackage::where('planner_package.type_ticket_id', $productType)
					                         ->where('planner_package.is_live', 1)
					                         ->whereNull('planner_package.deleted_at')
					                         ->whereIn('planner_package.id', $auditOptionIds)
					                         ->get();

					foreach ($auditOptions as $auditOption)
					{
						$option = $options->where('id', $auditOption->option_id)->first();

						if (!$option)
						{
							return "Issue while fetching audit option data";
						}

						array_push($optionData, [
							'auditId'       => $auditOption->id,
							'id'            => $option->id,
							'name'          => $option->name,
							'code'          => $option->code,
							'url'           => config('evibe.host') . 'packages/view/' . $option->id,
							'price'         => $option->price,
							'priceWorth'    => $option->price_worth,
							'profilePicUrl' => $option->getProfilePic(),
							'auditColumn'   => $auditOption->data,
						]);
					}
			}
		}

		//dd($optionData);

		$perPage = 10;
		$optionData = $this->pagination($optionData, $perPage);

		$data['options'] = $optionData;
		$data['auditColumnName'] = $column;
		$data['auditTableName'] = $table;
		$data['optionTypeId'] = $productType;
		$data['filters'] = [
			'queryParams' => $this->getExistingQueryParams()
		];

		$data['referenceTab'] = config('evibe.reference.audit.review');

		return view('audit.review', ['data' => $data]);
	}

	public function saveAuditReviewData($query = "accept", Request $request)
	{
		try
		{

			$data = $request->all();
			$auditId = $request->input('auditId');
			$optionId = $request->input('optionId');
			$optionTypeId = $request->input('optionTypeId');
			$tableName = $request->input('tableName');
			$columnName = $request->input('columnName');
			$columnData = $request->input('columnData');
			$rejectReason = $request->input('rejectReason');

			$auditOption = OptionAudit::find($auditId);

			if (!$auditOption)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Unable to find the audit option row to update.'
				                        ]);
			}

			switch ($auditOption->option_type_id)
			{
				case config('evibe.ticket_type.couple-experiences'):
					\VendorPackage::where('id', $auditOption->option_id)
					              ->update([
						                       $auditOption->column_name => $auditOption->data
					                       ]);

					if ($query == 'accept')
					{
						$auditOption->accepted_at = Carbon::now();
					}
					elseif ($query == 'reject')
					{
						$auditOption->rejected_at = Carbon::now();
						$auditOption->reject_comments = $rejectReason;
					}
					$auditOption->updated_at = Carbon::now();
					$auditOption->save();
					break;
			}

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}

	private function columnSelectionData()
	{
		//$typeTicket = \TypeTicket::all();
		$productTypes = \TypeTicket::where('id', config('evibe.ticket_type.couple-experiences'))->get();

		$columnNames = [
			'info'
		];

		return [
			'productTypes' => $productTypes,
			'columnNames'  => $columnNames
		];
	}

	private function pagination($array, $perPage = 30)
	{
		// Paginate results
		$currentPage = request('page');
		$offset = ($currentPage * $perPage) - $perPage;

		return new LengthAwarePaginator(
			array_slice($array, $offset, $perPage, true),
			count($array),
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath()]
		);
	}

	public function getExistingQueryParams()
	{
		$existingQueryParams = [];

		foreach (request()->all() as $key => $value)
		{
			if ($key != 'page')
			{
				$existingQueryParams[$key] = urldecode($value);
			}
		}

		return $existingQueryParams;
	}

}
