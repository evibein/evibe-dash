<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\User;
use App\Models\VirtualParty\VirtualParty;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VirtualPartyController extends BaseController
{
	public function getVirtualPartiesList()
	{
		$virtualParties = VirtualParty::orderBy('created_at', 'DESC');
		$virtualParties = $virtualParties->paginate(20);
		$data = [
			'virtualParties' => $virtualParties
		];

		return view('virtual-party.home', ['data' => $data]);
	}

	public function getVirtualPartyDetails($vpId)
	{
		$virtualParty = VirtualParty::find($vpId);
		$data = [
			'virtualParty' => $virtualParty
		];

		return view('virtual-party.profile', ['data' => $data]);
	}

	public function createVirtualParty()
	{
		try
		{

			// validation
			$rules = [
				'name'          => 'required|min:3',
				'phone'         => 'required|phone|min:10',
				'email'         => 'required|email',
				'partyDateTime' => 'required|date|after:now',
				'zoomLink'      => 'required|min:3',
				'celebrityText' => 'required|min:3',
				'event'         => 'required|min:3',
				'url'           => 'required'
			];

			$messages = [
				'name.required'          => 'Please enter your name',
				'name.min'               => 'Name cannot be less than 3 character',
				'email.required'         => 'Please enter your email',
				'email.email'            => 'Please enter a valid email',
				'phone.required'         => 'Please enter your phone',
				'phone.phone'            => 'Please enter you 10 digit valid phone number',
				'partyDateTime.required' => 'Please enter the party date time',
				'partyDateTime.date'     => 'Please enter a valid party date time',
				'partyDateTime.after'    => 'Entered party date time has already passed',
				'zoomLink.required'      => 'Please enter the zoom link',
				'zoomLink.min'           => 'Please enter a valid zoom link',
				'celebrityText.required' => 'Please enter the celebrity text',
				'celebrityText.min'      => 'Celebrity text cannot be less than 3 character',
				'event.required'         => 'Please enter the event',
				'event.min'              => 'Please enter a proper event name',
				'url.required'           => 'Please enter the party url'
			];

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validator->messages()->first()
				                        ]);
			}

			// invite img validation
			$invite = Input::file('invite');
			$imageRules = [
				'file' => 'required|mimes:png,jpeg,jpg,JPG|max:2048'
			];

			$imageMessages = [
				'file.required' => 'Please upload an invite',
				'file.mimes'    => 'Image is not valid. (only .png, .jpeg, .jpg are accepted)',
				'file.max'      => 'Image size cannot be more than 1 MB'
			];

			// validation for proof front side
			$fileValidator = Validator::make(['file' => $invite], $imageRules, $imageMessages);

			if ($fileValidator->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $fileValidator->messages()->first()
				                        ]);
			}

			// @todo: in future - url will be formed in here

			$email = Input::get('email');
			$phone = Input::get('phone');
			$name = Input::get('name');

			$user = User::where('username', $email)
			            ->orWhere('phone', $phone)// @see: Is validation based only on email?
			            ->first();
			if (!$user)
			{
				$user = User::create([
					                     'name'       => $name,
					                     'phone'      => $phone,
					                     'username'   => $email,
					                     'role_id'    => config('evibe.roles.customer'),
					                     'created_at' => Carbon::now(),
					                     'updated_at' => Carbon::now(),
				                     ]);
			}
			$userId = $user->id;

			// validate url
			$url = Input::get('url');
			$existingParty = VirtualParty::where('url', $url)->first(); // even if the party is over
			if ($existingParty)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'A party already exists with the provided party url. Please change the url and try again'
				                        ]);
			}

			$virtualParty = VirtualParty::create([
				                                     'name'            => $name,
				                                     'phone'           => $phone,
				                                     'email'           => $email,
				                                     'user_id'         => $userId,
				                                     'invite_url'      => 'need_to_upload', // as it is not nullable in database
				                                     'party_date_time' => Input::get('partyDateTime'),
				                                     'zoom_link'       => Input::get('zoomLink'),
				                                     'event'           => Input::get('event'),
				                                     'celebrity_text'  => Input::get('celebrityText'),
				                                     'url'             => $url,
				                                     'created_at'      => Carbon::now(),
				                                     'updated_at'      => Carbon::now()
			                                     ]);

			// invite should be uploaded after creating virtual party to get correct path
			$directPath = '/virtual-party/' . $virtualParty->id . '/';
			$options = ['isWatermark' => false, 'isOptimize' => false, 'addTimeInName' => false];
			$fileName = $this->uploadImageToServer($invite, $directPath, $options);
			$virtualParty->invite_url = $fileName;
			$virtualParty->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}

	public function updateVirtualParty($vpId)
	{
		try
		{

			$virtualParty = VirtualParty::find($vpId);
			if (!$virtualParty)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Unable to find virtual party details of id: ' . $vpId . ' to update'
				                        ]);
			}

			// validation
			$rules = [
				'name'          => 'required|min:3',
				'phone'         => 'required|phone|min:10',
				'email'         => 'required|email',
				'partyDateTime' => 'required|date|after:now',
				'zoomLink'      => 'required|min:3',
				'celebrityText' => 'required|min:3',
				'event'         => 'required|min:3',
				'url'           => 'required'
			];

			$messages = [
				'name.required'          => 'Please enter your name',
				'name.min'               => 'Name cannot be less than 3 character',
				'email.required'         => 'Please enter your email',
				'email.email'            => 'Please enter a valid email',
				'phone.required'         => 'Please enter your phone',
				'phone.phone'            => 'Please enter you 10 digit valid phone number',
				'partyDateTime.required' => 'Please enter the party date time',
				'partyDateTime.date'     => 'Please enter a valid party date time',
				'partyDateTime.after'    => 'Entered party date time has already passed',
				'zoomLink.required'      => 'Please enter the zoom link',
				'zoomLink.min'           => 'Please enter a valid zoom link',
				'celebrityText.required' => 'Please enter the celebrity text',
				'celebrityText.min'      => 'Celebrity text cannot be less than 3 character',
				'event.required'         => 'Please enter the event',
				'event.min'              => 'Please enter a proper event name',
				'url.required'           => 'Please enter the party url'
			];

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validator->messages()->first()
				                        ]);
			}

			// invite img validation
			$invite = Input::file('invite');
			if($invite)
			{
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:2048'
				];

				$imageMessages = [
					'file.required' => 'Please upload an invite',
					'file.mimes'    => 'Image is not valid. (only .png, .jpeg, .jpg are accepted)',
					'file.max'      => 'Image size cannot be more than 2 MB'
				];

				// validation for proof front side
				$fileValidator = Validator::make(['file' => $invite], $imageRules, $imageMessages);

				if ($fileValidator->fails())
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => $fileValidator->messages()->first()
					                        ]);
				}
			}

			// validate url
			$url = Input::get('url');
			if($url != $virtualParty->url)
			{
				$existingParty = VirtualParty::where('url', $url)->first(); // even if the party is over
				if ($existingParty)
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => 'A party already exists with the provided party url. Please change the url and try again'
					                        ]);
				}
			}

			// @todo: how to deal with user_id

			$virtualParty->name = Input::get('name');
			$virtualParty->phone = Input::get('phone');
			$virtualParty->email = Input::get('email');
			$virtualParty->party_date_time = Input::get('partyDateTime');
			$virtualParty->zoom_link = Input::get('zoomLink');
			$virtualParty->celebrity_text = Input::get('celebrityText');
			$virtualParty->event = Input::get('event');
			$virtualParty->url = Input::get('url');
			$virtualParty->name = Input::get('name');
			if($invite)
			{
				$directPath = '/virtual-party/' . $virtualParty->id . '/';
				$options = ['isWatermark' => false, 'isOptimize' => false, 'addTimeInName' => false];
				$fileName = $this->uploadImageToServer($invite, $directPath, $options);
				$virtualParty->invite_url = $fileName;
			}
			$virtualParty->updated_at = Carbon::now();
			$virtualParty->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}
}