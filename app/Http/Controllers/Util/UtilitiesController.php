<?php namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\Util\SellingStatus;
use Area;
use AreaNearby;
use City;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class UtilitiesController extends BaseController
{
	public function showNearbyAreas()
	{
		$cities = City::all();
		$areas = Area::orderBy('city_id', 'ASC')
		              ->orderBy('name', 'ASC')
		              ->get();

		$data = [
			'cities' => $cities,
			'areas'  => $areas
		];

		return view('utilities/nearby-areas', ['data' => $data]);
	}

	public function getNearbyAreas($cityId, $areaId)
	{
		$nearbyAreas = [];

		$allAreaMappings = AreaNearby::where('area1', $areaId)
		                              ->orWhere('area2', $areaId)
		                              ->get();

		foreach ($allAreaMappings as $areaMapping)
		{
			if ($areaMapping->area1 == $areaId)
			{
				array_push($nearbyAreas, $areaMapping->area2);
			}
			if ($areaMapping->area2 == $areaId)
			{
				array_push($nearbyAreas, $areaMapping->area1);
			}
		}

		$nearbyAreas = array_unique($nearbyAreas); // get unique

		return response()->json([
			                        'success'     => true,
			                        'nearbyAreas' => Area::whereIn('id', $nearbyAreas)->get()->toArray()
		                        ]);
	}

	public function saveNearbyAreas($areaId)
	{
		$nearbyAreas = Input::get('areas');
		$nearbyAreas = explode(",", $nearbyAreas);
		$nearbyAreasIds = [];

		if (count($nearbyAreas))
		{
			$nearbyAreasIds = Area::where('name', 'LIKE', $nearbyAreas[0]);
			foreach ($nearbyAreas as $area)
				$nearbyAreasIds->orWhere('name', 'LIKE', $area);
			$nearbyAreasIds = $nearbyAreasIds->pluck('id');
		}

		foreach ($nearbyAreasIds as $nearbyAreaId)
		{
			// check if mapping exists
			$hasMapping = AreaNearby::where(function ($query1) use ($areaId, $nearbyAreaId) {
				$query1->where('area1', $nearbyAreaId)
				       ->where('area2', $areaId);
			})
			                         ->orWhere(function ($query2) use ($areaId, $nearbyAreaId) {
				                         $query2->where('area1', $areaId)
				                                ->where('area2', $nearbyAreaId);
			                         })
			                         ->get();

			if (!count($hasMapping))
			{
				AreaNearby::create([
					                    'area1' => $areaId,
					                    'area2' => $nearbyAreaId
				                    ]);
			}
		}

		return response()->json(['success' => true]);
	}

	public function deleteNearbyArea()
	{
		$area1 = Input::get('area1');
		$area2 = Input::get('area2');

		if (!$area1 || !$area2)
		{
			return response()->json(['success' => false]);
		}

		AreaNearby::where(function ($query1) use ($area1, $area2) {
			$query1->where('area1', $area1)
			       ->where('area2', $area2);
		})
		           ->orWhere(function ($query2) use ($area1, $area2) {
			           $query2->where('area1', $area2)
			                  ->where('area2', $area1);
		           })
		           ->forceDelete();

		return response()->json(['success' => true]);

	}

	public function updateSellingStatus($productTypeId, $productId, $statusId)
	{
		// validate map type id's (decor, packages, cakes, venue-halls & services)
		$validProductTypeIds = [config("evibe.ticket_type.packages"),
			config("evibe.ticket_type.services"),
			config("evibe.ticket_type.cakes"),
			config("evibe.ticket_type.decors"),
			config("evibe.ticket_type.venue_halls")];

		if (in_array($productTypeId, $validProductTypeIds))
		{
			// update status_id & handler_id if product_type_id & product_id exists else create new
			$sellingStatus = SellingStatus::updateOrCreate(
				[
					'product_type_id' => $productTypeId,
					'product_id'      => $productId
				],
				[
					'product_selling_status_id' => $statusId ?: config("evibe.selling.status.default"),
					'handler_id'                => Auth::user()->id
				]
			);

			if ($sellingStatus)
			{
				return response()->json([
					                        "success" => true
				                        ]);
			}
			else
			{
				return response()->json([
					                        "success" => false,
					                        "error"   => "Error while updating the status, Please try again."
				                        ]);
			}
		}
		else
		{
			return response()->json([
				                        "success" => false,
				                        "error"   => "Not a valid ticket type, Please reload the page & try again."
			                        ]);
		}
	}
}
