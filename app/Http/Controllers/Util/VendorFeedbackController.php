<?php

namespace App\Http\Controllers\Util;

use App\Http\Requests;
use Cake;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Queue;

use App\Models\User;
use App\Models\Review\PartnerReviewAnswer;
use App\Models\Util\TypePartnerReviewQuestion;
use App\Http\Controllers\Base\BaseController;

class VendorFeedbackController extends BaseController
{
	public function showVendorFeedbackInfo($vendorFeedbackId)
	{
		$data['success'] = false;
		$review = \Review::find($vendorFeedbackId);

		if ($review)
		{
			$vendorData = [
				'type'             => $review->booking->checkReviewType(),
				'overAllRating'    => $review->rating,
				'comment'          => $review->review,
				'confirmButtonUrl' => route('feedback.vendor.confirm', $review->id),
				'isAccepted'       => $review->is_accepted,
				'isRejected'       => $review->is_rejected,
			];

			$answers = $review->individualAnswers;

			foreach ($answers as $answer)
			{
				if ($answer->question)
				{
					$vendorData['extra'][] = [
						'question' => $answer->question->question,
						'rating'   => $answer->rating
					];
				}
			}

			if ($review->is_accepted)
			{
				$vendorData['username'] = User::find($review->accepted_by_id)->name;
			}

			if ($review->is_rejected)
			{
				$vendorData['username'] = User::find($review->rejected_by_id)->name;
			}

			$data['vendorData'] = $vendorData;
			$data['success'] = true;
		}

		return json_encode($data);
	}

	public function loadVendorFeedbackQuestion($ticketBookingId)
	{
		$data['success'] = false;
		$questionData = [];
		$booking = \TicketBooking::find($ticketBookingId);
		$typeTicketId = $booking->mapping->map_type_id;
		$questions = TypePartnerReviewQuestion::where('type_ticket_id', $typeTicketId)->get();

		if (count($questions) == 0)
		{
			$questions = TypePartnerReviewQuestion::where('type_ticket_id', $booking->map_type_id)->get();
		}

		foreach ($questions as $question)
		{
			$questionData[] = [
				'question'   => $question->question,
				'id'         => $question->id,
				'max_rating' => $question->max_rating
			];
		}
		$data['questionData'] = $questionData;
		$data['success'] = true;

		return json_encode($data);
	}

	public function loadEditVendorFeedback($reviewId)
	{
		$data['success'] = false;
		$answerData = [];

		$review = \Review::find($reviewId);

		if ($review)
		{
			$answers = $review->individualAnswers;

			if (count($answers) > 0)
			{
				foreach ($answers as $answer)
				{
					if ($answer->question)
					{
						$answerData[] = [
							'question'   => $answer->question->question,
							'id'         => $answer->id,
							'rating'     => $answer->rating,
							'max_rating' => $answer->question->max_rating
						];
					}
				}
			}
			$data['answers'] = $answerData;
			$data['comment'] = $review->review;
			$data['success'] = true;
		}

		return json_encode($data);
	}

	public function addVendorFeedback($ticketBookingId)
	{
		$res = ['success' => false];
		$ticketBooking = \TicketBooking::find($ticketBookingId);
		$questions = Input::get('questions');
		$answerIds = [];
		$review = null;

		if (isset($questions))
		{
			$validationError = $this->validateVendorReviewData($questions);
			if (isset($validationError))
			{
				return response()->json(['success' => false, 'error' => $validationError]);
			}
		}

		try
		{

			$ticket = $ticketBooking->ticket;
			$typeTicketId = $ticketBooking->mapping->map_type_id;

			$reviewData = [
				'review'            => Input::get('comments'),
				'map_id'            => $ticketBooking->map_id,
				'map_type_id'       => $ticketBooking->map_type_id,
				'type_ticket_id'    => $typeTicketId,
				'ticket_booking_id' => $ticketBooking->id,
			];

			if ($ticket)
			{
				$reviewData['name'] = $ticket->name;
				$reviewData['email'] = $ticket->email;
				$reviewData['phone'] = $ticket->phone;
				$reviewData['event_id'] = $ticket->event_id;
				$reviewData['review'] = Input::get('comments');
				$reviewData['location'] = $ticket->area ? $ticket->area->name : '';

				if ($ticketBooking->map_type_id == 2)
				{
					$reviewData['planner_id'] = $ticketBooking->map_id;
				}
			}
			$qCount = $qRating = 0;

			if (isset($questions))
			{
				foreach ($questions as $question)
				{
					$answer = PartnerReviewAnswer::create([
						                                      'question_id' => $question['id'],
						                                      'rating'      => $question['rating']
					                                      ]);
					$answerIds[] = $answer->id;
					$qRating += $question['rating'];
					$qCount++;
				}
			}

			$avgRating = $qCount > 0 ? round($qRating / $qCount, 2) : 0;
			$reviewData['rating'] = $avgRating;
			$review = \Review::create($reviewData);

			if ($review)
			{
				foreach ($answerIds as $answerId)
				{
					PartnerReviewAnswer::where('id', $answerId)->update(['review_id' => $review->id]);
				}

				$res = ['success' => true];
			}

		} catch (\Exception $e)
		{
			if (count($answerIds) > 0)
			{
				PartnerReviewAnswer::whereIn('id', $answerIds)->delete();

			}
			if ($review)
			{
				\Review::where('id', $review->id)->delete();
			}

			$this->sendErrorReport($e);
			$res = [
				'success' => false,
				'error'   => 'Some error occurred while saving'
			];
		}

		return response()->json($res);

	}

	public function editVendorFeedback($ticketBookingId, $vendorReviewId)
	{
		$res = ['success' => false];
		$reviewData = ['review' => Input::get('comments')];
		$questions = Input::get('questions');

		if (isset($questions))
		{
			$validationError = $this->validateVendorReviewData($questions);
			if (isset($validationError))
			{
				return response()->json(['success' => false, 'error' => $validationError]);
			}
		}

		$review = \Review::find($vendorReviewId);
		$qCount = $qRating = 0;

		if (isset($questions))
		{
			foreach ($questions as $question)
			{
				PartnerReviewAnswer::where('id', $question['id'])
				                   ->where('review_id', $vendorReviewId)
				                   ->update(['rating' => $question['rating']]);

				$qRating += $question['rating'];
				$qCount++;
			}
		}

		$avgRating = $qCount > 0 ? round($qRating / $qCount, 2) : 0;
		$reviewData['rating'] = $avgRating;

		if ($review->update($reviewData))
		{
			$res = ['success' => true];
		}

		return response()->json($res);
	}

	// confirm vendor feedback
	public function confirmVendorFeedback($vendorFeedbackId)
	{
		$vendorFeedback = \Review::find($vendorFeedbackId);
		$booking = $vendorFeedback->booking;

		// @see: issue with cake auto booking: map_id is cake_id and not provider id
		// @todo: need to handle this special case
		$provider = null;
		if ($booking->ticket && $booking->ticket->is_auto_booked && $booking->map_type_id == config('evibe.ticket_type.cakes'))
		{
			$bookingCake = Cake::find($booking->map_id);
			if ($bookingCake)
			{
				$provider = $bookingCake->provider;
			}
		}
		else
		{
			$provider = $booking->provider;
		}

		if (!is_null($provider) && $vendorFeedback)
		{
			$userId = Auth::user()->id;
			$additionalRatings = [];
			$ccAddresses = [config('evibe.business_group_email')];
			$replyTo = [config('evibe.business_group_email')];
			$sub = '[Evibe.in] You got a new review from ' . $booking->ticket->name . ' for party on ' . date("d M Y", strtotime($vendorFeedback->created_at));

			$answers = $vendorFeedback->individualAnswers;

			foreach ($answers as $answer)
			{
				if ($answer->question)
				{
					$additionalRatings[] = [
						'question'  => $answer->question->question,
						'rating'    => $answer->rating,
						'maxRating' => $answer->question->max_rating
					];
				}
			}

			$to = $provider->email;
			if (!$to)
			{
				$to = config('evibe.tech_group_email'); // quick fix to understand the issue
			}

			$data = [
				'to'                => [$to],
				'ccAddresses'       => $ccAddresses,
				'replyTo'           => $replyTo,
				'subject'           => $sub,
				'vendorName'        => $provider->person,
				'reviewData'        => $vendorFeedback->review ? $vendorFeedback->review : "---",
				'vendorRating'      => $vendorFeedback->rating,
				'additionalRatings' => $additionalRatings,
				'partyDate'         => date("d M Y", $booking->party_date_time),
				'clientName'        => $booking->ticket->name
			];

			Queue::push('\Evibe\Utilities\SendEmail@mailReviewToVendor', $data);

			$userName = User::find($userId)->name;
			$vendorFeedback->update([
				                        'is_accepted'    => 1,
				                        'accepted_by_id' => $userId
			                        ]);

			$res = [
				'success' => true,
				'message' => 'Review has been confirmed by <b>' . $userName . '</b>'
			];
		}
		else
		{
			$res = [
				'success'      => false,
				'errorMessage' => "Provider not found for booking: " . $booking->id
			];
		}

		return response()->json($res);
	}

	private function validateVendorReviewData($questions)
	{
		$error = null;

		foreach ($questions as $question)
		{
			$name = str_replace(" ", "", $question['name']);
			$errorMsg = "please rate " . $question['name'] . ' on scale of ' . $question['maxRating'];

			$rules = [
				$name => 'required|numeric|min:0.5|max:' . $question['maxRating']
			];

			$messages = [
				$name . '.required' => $errorMsg,
				$name . '.numeric'  => $errorMsg,
				$name . '.min'      => $errorMsg,
				$name . '.max'      => $errorMsg
			];

			$data = [
				$name => $question['rating']
			];

			$validator = validator($data, $rules, $messages);

			if ($validator->fails())
			{
				$error = $validator->messages()->first();

				return $error;
			}
		}

		return $error;
	}
}
