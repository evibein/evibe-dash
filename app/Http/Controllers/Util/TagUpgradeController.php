<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\DecorEvent;
use App\Models\TypeEvent;
use Carbon\Carbon;
use City;
use Decor;
use DecorGallery;
use DecorTags;
use Illuminate\Support\Facades\DB;
use PackageGallery;
use PackageTag;
use Tags;
use TypeTicket;
use VendorPackage;

class TagUpgradeController extends BaseController
{
	public function showList()
	{
		$typeEvent = TypeEvent::select('id', 'name')->get();
		$typeTicket = TypeTicket::select('id', 'name')->where('is_page', 1)->get();
		$cities = City::select("id", "name")->get();
		$productsData = [];
		$relevantParentTags = [];
		$relevantEvents = [];
		$typeTags = [];

		// assigning default values
		$eventId = request("eventId") ?: config("evibe.event.kids_birthday");
		$cityId = request("cityId") ?: config("evibe.city.Bengaluru");
		$productTypeId = request("typeTicketId") ?: config("evibe.ticket_type.decors");

		switch ($productTypeId)
		{
			// Decor
			case config('evibe.ticket_type.decors'):
				$productsData = $this->getDecorData($eventId, $cityId);

				switch ($eventId)
				{
					case config('evibe.event.kids_birthday'):
						$relevantParentTags = [
							'Category'             => 15,
							'Gender'               => 55,
							'Themes'               => 48,
							'Place Type Mapping'   => 204,
							'Guests Count Mapping' => 205
						];

						$relevantEvents = TypeEvent::select('id', 'name')
						                           ->where('parent_id', $eventId)
						                           ->get();

						$typeTags = Tags::select('id', 'name', 'parent_id', 'map_type_id')
						                 ->whereIn('id', array_values($relevantParentTags))
						                 ->orWhereIn('parent_id', array_values($relevantParentTags))
						                 ->get();
						break;
				}

				break;

			// Package & package related
			case config('evibe.ticket_type.packages'):
			case config('evibe.ticket_type.food'):
			case config('evibe.ticket_type.priests'):
			case config('evibe.ticket_type.couple-experiences'):
			case config('evibe.ticket_type.tents'):
			case config('evibe.ticket_type.villa'):
				/*
				$productsData = $this->getPackageData($eventId, $cityId, $productTypeId);
				*/
				break;
		}

		$data = [
			'typeEvent'          => $typeEvent,
			'typeTicket'         => $typeTicket,
			'typeTags'           => $typeTags,
			'cities'             => $cities,
			'currentEventId'     => $eventId,
			'productTypeId'      => $productTypeId,
			'productsData'       => $productsData,
			'relevantParentTags' => $relevantParentTags,
			'relevantEvents'     => $relevantEvents
		];

		return view('utilities.tags_revamp', ["data" => $data]);
	}

	public function getDecorData($eventId, $cityId)
	{
		$decors = Decor::select('decor.id', 'decor.name', 'decor.code', 'decor.min_price')
		                ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
		                ->join('planner', 'planner.id', '=', 'decor.provider_id')
		                ->where('decor_event.event_id', $eventId)
		                ->where('decor.is_live', 1)
		                ->whereNull('decor.deleted_at')
		                ->where("planner.city_id", $cityId)
		                ->whereNull('decor_event.deleted_at')
		                ->orderBy('decor.code')
		                ->paginate(50);

		$decorTags = DecorTags::select('decor_tags.id', 'decor_tags.decor_id', 'decor_tags.tag_id', 'decor_tags.updated_at', 'type_tags.name', 'type_tags.parent_id')
		                       ->join('type_tags', 'type_tags.id', '=', 'decor_tags.tag_id')
		                       ->whereIn('decor_tags.decor_id', $decors->pluck('id'))
		                       ->whereNull('decor_tags.deleted_at')
		                       ->whereNull('type_tags.deleted_at')
		                       ->get();

		$decorGallery = DecorGallery::select('id', 'url', 'decor_id')
		                             ->whereIn('decor_id', $decors->pluck('id'))
		                             ->get();

		$decorEvents = DecorEvent::select('decor_event.id', 'decor_event.decor_id', 'decor_event.event_id', 'type_event.name AS name')
		                         ->join('type_event', 'type_event.id', '=', 'decor_event.event_id')
		                         ->whereIn('decor_event.decor_id', $decors->pluck('id'))
		                         ->whereNull('decor_event.deleted_at')
		                         ->whereNull('type_event.deleted_at')
		                         ->get();

		return [
			"products" => $decors,
			'tags'     => $decorTags,
			'gallery'  => $decorGallery,
			'events'   => $decorEvents
		];
	}

	public function getPackageData($eventId, $cityId, $productTypeId)
	{
		$packages = VendorPackage::select('id', 'name', 'code', 'type_ticket_id', 'map_type_id', 'map_id')
		                          ->where('event_id', $eventId)
		                          ->where('type_ticket_id', $productTypeId)
		                          ->where("city_id", $cityId)
		                          ->where('is_live', 1)
		                          ->whereNull('deleted_at')
		                          ->orderBy('code')
		                          ->paginate(50);

		$packageTags = PackageTag::select('planner_package_tags.id', 'planner_package_tags.planner_package_id', 'planner_package_tags.tile_tag_id', 'planner_package_tags.updated_at', 'type_tags.name', 'type_tags.parent_id')
		                          ->join('type_tags', 'type_tags.id', '=', 'planner_package_tags.tile_tag_id')
		                          ->whereIn('planner_package_tags.planner_package_id', $packages->pluck('id'))
		                          ->whereNull('planner_package_tags.deleted_at')
		                          ->whereNull('type_tags.deleted_at')
		                          ->get();

		$packageGallery = PackageGallery::select('id', 'url', 'planner_package_id')
		                                 ->whereIn('planner_package_id', $packages->pluck('id'))
		                                 ->get();

		return [
			"products" => $packages,
			'tags'     => $packageTags,
			'gallery'  => $packageGallery
		];
	}

	public function deleteTag($tagId, $decorId)
	{
		$decorTag = DecorTags::where('decor_id', $decorId)
		                      ->where('tag_id', $tagId)
		                      ->first();

		// If tag found updating deleted, else returning success as tag is not found
		if ($decorTag)
		{
			$decorTag->update([
				                  "deleted_at" => Carbon::now()
			                  ]);
		}

		return response()->json([
			                        'success' => true
		                        ]);
	}

	public function addTag($decorId)
	{
		// If child exists then override parent tag id with child tag id
		$tagId = request('parentTagId');
		if (request('childTagId') > 0)
		{
			$tagId = request('childTagId');
		}

		$currentTag = DecorTags::where('decor_id', $decorId)
		                        ->where('tag_id', $tagId)
		                        ->get();

		// If that tag already exists return back
		if ($currentTag->count() == 0)
		{
			$decorTag = DecorTags::create([
				                               "decor_id"   => $decorId,
				                               "tag_id"     => $tagId,
				                               "created_at" => Carbon::now(),
				                               "updated_at" => Carbon::now()
			                               ]);

			if ($decorTag)
			{
				$tag = DB::table('type_tags AS t')
				         ->leftjoin('type_tags AS pt', 't.parent_id', '=', 'pt.id')
				         ->where('t.id', '=', $tagId)
				         ->select('t.id AS id', 't.name AS name', 'pt.name AS parent_name', 't.parent_id')
				         ->first();

				$tagName = $tag ? $tag->name : "Deleted Tag";
				$parentTagId = $tag ? $tag->parent_id : "-";

				// template for the tag
				$tagTemplate = '<div class="alert-info mar-r-5 mar-b-5 pad-5 in-blk revamp-option-tag-' . $tagId . '-' . $decorId . '" data-parentTagId="' . $parentTagId . '">' . $tagName
					. ' <i data-value="' . $tagId . '" data-decor-id="' . $decorId . '" class="glyphicon glyphicon-trash delete-revamp-tag"></i></div>';

				return response()->json([
					                        "success"     => true,
					                        "productId"   => $decorId,
					                        "categoryId"  => config('evibe.ticket_type.decors'),
					                        "tagTemplate" => $tagTemplate
				                        ]);
			}

			return response()->json([
				                        "success" => false,
				                        "error"   => "Unable to process the request, Please try again."
			                        ]);
		}

		return response()->json([
			                        "success" => false,
			                        "error"   => "Tag already added, Please add another new appropriate tag."
		                        ]);
	}

	public function addEvent($decorId)
	{
		// If child exists then override parent tag id with child tag id
		$eventId = request('eventId');

		// If that tag already exists return back
		if (count(DecorEvent::where('decor_id', $decorId)->where('event_id', $eventId)->first()) == 0)
		{
			$decorTag = DecorEvent::create([
				                               "decor_id" => $decorId,
				                               "event_id" => $eventId
			                               ]);

			if ($decorTag)
			{
				$event = TypeEvent::find($eventId);

				// template for the tag
				$eventTemplate = '<div class="alert-danger mar-r-5 mar-b-5 pad-5 in-blk revamp-option-event-' . $eventId . '-' . $decorId . '">' . $event->name
					. ' <i data-value="' . $eventId . '" data-decor-id="' . $decorId . '" class="glyphicon glyphicon-trash delete-revamp-tag-event"></i></div>';

				return response()->json([
					                        "success"       => true,
					                        "decorId"       => $decorId,
					                        "eventTemplate" => $eventTemplate
				                        ]);
			}

			return response()->json([
				                        "success" => false,
				                        "error"   => "Unable to process the request, Please try again."
			                        ]);
		}

		return response()->json([
			                        "success" => false,
			                        "error"   => "Event mapping already exists, Please add another new appropriate event"
		                        ]);
	}

	public function deleteEvent($eventId, $decorId)
	{
		$decorEvent = DecorEvent::where('decor_id', $decorId)
		                        ->where('event_id', $eventId)
		                        ->first();

		// If tag found updating deleted, else returning success as tag is not found
		if ($decorEvent)
		{
			$decorEvent->update(["deleted_at" => Carbon::now()]);
		}

		return response()->json(['success' => true]);
	}
}