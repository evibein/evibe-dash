<?php namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\SavePriority;
use App\Models\DecorEvent;
use App\Models\ServiceEvent;
use App\Models\TrendEvents;
use App\Models\TypeEvent;
use App\Models\TypeServices;
use App\Models\VenueHallEvent;
use Cake;
use CakeEvent;
use CakeTags;
use City;
use Decor;
use DecorTags;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PackageTag;
use Tags;
use Trend;
use TypeTicket;
use VendorPackage;

class ReorderingController extends BaseController
{
	public function showFilter()
	{
		$allOccasions = TypeEvent::all();
		$pageTypes = TypeTicket::where('is_reorder', 1)->get();
		$cities = City::all();
		$tagTypes = [];

		return view('reorder.filter', compact('allOccasions', 'pageTypes', 'cities', 'tagTypes'));
	}

	public function nextRequest(Request $request)
	{
		$rules = [
			'city'     => 'required|integer|min:1',
			'occasion' => 'required|integer|min:1',
			'pageType' => 'required|integer|min:1'
		];

		$messages = [
			'occasion.required' => 'Please select an occasion type',
			'occasion.min'      => 'Please select a valid occasion type',
			'pageType.required' => 'Please select a page type type',
			'pageType.min'      => 'Please select a valid page type',
			'city.required'     => 'Please select a city to proceed',
			'city.min'          => 'Please select a valid city'
		];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()
			                 ->withInput()
			                 ->withErrors($validator);
		}

		// @see: maintain this order
		$routeParams = [
			$request->input('city'),
			$request->input('occasion'),
			$request->input('pageType'),
			$request->input('tagType')
		];

		return redirect()->route('reorder.show', $routeParams);
	}

	public function showItemsList($cityId, $occasionId, $pageId, $tagId = null)
	{
		$tagId = ($tagId == -1) ? null : $tagId;
		if ($occasionId && $pageId)
		{
			$pageName = TypeTicket::where('id', $pageId)->select('name')->first();
			$occasionName = TypeEvent::where('id', $occasionId)->select('name')->first();
			$cityName = City::where('id', $cityId)->select('name')->first();
			$items = $this->getItemsData($cityId, $occasionId, $pageId, $tagId);

			if (is_object($items))
			{
				$itemCount = request()->has('per_page') ? (int)request('per_page') : 1000;

				$data = [
					'pageType'      => $pageId,
					'occasion'      => $occasionId,
					'tagTypeId'     => $tagId,
					'itemCount'     => $itemCount,
					'itemType'      => $pageName,
					'eventType'     => $occasionName,
					'city'          => $cityName,
					'cityId'        => $cityId,
					'allItems'      => $items->get(),
					'priorityItems' => $items->take($itemCount)->get(),
				];

				return view('reorder.show', compact('data'));
			}

			else
			{
				return view('reorder.error', compact('itemType'));
			}
		}
		else
		{
			return "Unauthorized Access, <a href=" . route('reorder.filter') . "> Click here</a> to filter items to reorder";
		}
	}

	public function postOrder(Request $request)
	{
		$res = ['success' => false, 'error' => 'No correct table found'];
		$params = [
			'order'      => $request->input('order'),
			'cityId'     => $request->input('cityId'),
			'occasionId' => $request->input('occasionId'),
			'pageId'     => $request->input('pageId'),
			'tagId'      => $request->input('tagId'),
		];

		$this->dispatch(new SavePriority($params));
		$res = ['success' => true];

		return response()->json($res);
	}

	public function getTags($occasionId, $pageId)
	{
		if (is_null($pageId) && is_null($occasionId))
		{
			return response()->json(['success' => false]);
		}

		$tagTypes = Tags::where('type_event', $occasionId)
		                 ->where('map_type_id', $pageId)
		                 ->where('is_filter', 1)
		                 ->select('id', 'name')
		                 ->get()
		                 ->toArray();

		$res = [
			'success' => true,
			'tags'    => $tagTypes,
			'count'   => count($tagTypes)
		];

		return response()->json($res);
	}

	/*
	 * @author Vikash <vikash@evibe.in>
	 * @Since 14 May 2016
	 * enhance reordering based on type_event and type_ticket
	 */
	private function getItemsData($cityId, $occasionId, $pageId, $tagTypeId)
	{
		$items = '';

		// filter the data base of occasion and page type
		switch ($pageId)
		{
			//@todo: change the raw DB queries into model queries
			case config('evibe.ticket_type.entertainments'):

				$serviceIdEvent = ServiceEvent::where('type_event_id', $occasionId)
				                              ->pluck('type_service_id');

				$items = TypeServices::where('city_id', $cityId)
				                     ->whereIn('id', $serviceIdEvent)
				                     ->select('id', 'code', 'name', 'min_price', 'max_price',
				                              DB::raw('(select(priority) from priority WHERE priority.map_id = type_service.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ') as servicePriority'))
				                     ->orderBy(DB::raw('ISNULL(servicePriority), servicePriority'), 'ASC');

				break;

			case config('evibe.ticket_type.trends'):

				$trendsIdEvent = TrendEvents::where('type_event_id', $occasionId)
				                            ->pluck('trend_id');

				$items = Trend::forCity($cityId)
				               ->whereIn('trending.id', $trendsIdEvent)
				               ->where('trending.is_live', 1)
				               ->select('trending.id', 'trending.code', 'trending.name', 'trending.price', 'trending.price_max',
				                        DB::raw('(select(priority) from priority WHERE priority.map_id = trending.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ') as trendPriority'))
				               ->orderBy(DB::raw('ISNULL(trendPriority), trendPriority'), 'ASC');
				break;

			case config('evibe.ticket_type.cakes'):

				$cakeIds = CakeEvent::where('event_id', $occasionId)
				                     ->pluck('cake_id')
				                     ->toArray();

				if (is_null($tagTypeId))
				{
					$items = Cake::forCity($cityId)
					              ->whereIn('cake.id', $cakeIds)
					              ->where('cake.is_live', 1)
					              ->select('cake.id', 'cake.code', 'cake.title', 'cake.price',
					                       DB::raw('(select(priority) from priority WHERE priority.map_id = cake.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ' AND priority.type_tag_id IS NULL ) as cakePriority'))
					              ->orderBy(DB::raw('ISNULL(cakePriority), cakePriority'), 'ASC');
				}
				else
				{
					$items = Cake::forCity($cityId)
					              ->whereIn('cake.id', $cakeIds)
					              ->where('cake.is_live', 1)
					              ->select('cake.id', 'cake.code', 'cake.title', 'cake.price',
					                       DB::raw('(select(priority) from priority WHERE priority.map_id = cake.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ' AND priority.type_tag_id =' . $tagTypeId . ' ) as cakePriority'))
					              ->orderBy(DB::raw('ISNULL(cakePriority), cakePriority'), 'ASC');
					$tagTypeIdEvent = CakeTags::where('tag_id', $tagTypeId)
					                           ->pluck('cake_id');
					if (count($tagTypeIdEvent) > 0)
					{
						$items = $items->whereIn('cake.id', $tagTypeIdEvent);
					}
				}

				break;

			case config('evibe.ticket_type.decors'):

				$decorIds = DecorEvent::where('event_id', $occasionId)
				                      ->pluck('decor_id')
				                      ->toArray();

				if (is_null($tagTypeId))
				{
					$items = Decor::forCity($cityId)
					               ->whereIn('decor.id', $decorIds)
					               ->where('decor.is_live', 1)
					               ->select('decor.id', 'decor.code', 'decor.name', 'decor.min_price as price',
					                        DB::raw('(select(priority) from priority WHERE priority.map_id = decor.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ' AND priority.type_tag_id IS NULL) as decorPriority'))
					               ->orderBy(DB::raw('ISNULL(decorPriority), decorPriority'), 'ASC');
				}
				else
				{
					$items = Decor::forCity($cityId)
					               ->whereIn('decor.id', $decorIds)
					               ->where('decor.is_live', 1)
					               ->select('decor.id', 'decor.code', 'decor.name', 'decor.min_price as price',
					                        DB::raw('(select(priority) from priority WHERE priority.map_id = decor.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ' AND priority.type_tag_id =' . $tagTypeId . ' ) as decorPriority'))
					               ->orderBy(DB::raw('ISNULL(decorPriority), decorPriority'), 'ASC');
					$tagTypeIdEvent = DecorTags::where('tag_id', $tagTypeId)
					                            ->pluck('decor_id');
					if (!is_null($tagTypeIdEvent))
					{
						$items = $items->whereIn('decor.id', $tagTypeIdEvent);
					}
				}

				break;

			case config('evibe.ticket_type.venue_halls'):

				$venueHallIdEvent = VenueHallEvent::where('event_id', $occasionId)
				                                  ->pluck('venue_hall_id');

				$venue = DB::table('venue')
				           ->join('area', 'venue.area_id', '=', 'area.id')
				           ->join('venue_hall', 'venue.id', '=', 'venue_hall.venue_id')
				           ->where('venue.city_id', $cityId)
				           ->where('venue.is_live', 1)
				           ->whereNull('venue_hall.deleted_at')
				           ->whereNull('venue.deleted_at')
				           ->whereIn('venue_hall.id', $venueHallIdEvent);

				$items = $venue->select('venue_hall.id as id', 'venue.id as venue_id', 'venue.name as venue_name', 'venue_hall.code as hall_code', 'venue_hall.name as hall_name', 'area.name as location',
				                        DB::raw('(select(priority) from priority WHERE priority.map_id = venue_hall.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ') as hallPriority'))
				               ->orderBy(DB::raw('ISNULL(hallPriority), hallPriority'), 'ASC');

				break;

			//@see for resort, villa and lounges are packages only, so a single function is taking care of it;
			case config('evibe.ticket_type.packages'):
			case config('evibe.ticket_type.resort'):
			case config('evibe.ticket_type.villa'):
			case config('evibe.ticket_type.lounge'):
			case config('evibe.ticket_type.food'):
			case config('evibe.ticket_type.tents'):
				$items = $this->getPackageList($occasionId, $pageId, $cityId, $tagTypeId);
				break;

			case config('evibe.ticket_type.couple-experiences'):
				$items = $this->getCoupleExpList($occasionId, $pageId, $cityId, $tagTypeId);
		}

		return $items;
	}

	private function getPackageList($occasionId, $pageId, $cityId, $tagTypeId)
	{

		$data = VendorPackage::where('event_id', $occasionId)
		                      ->where('city_id', $cityId)
		                      ->where('is_live', 1)
		                      ->whereNotIn('event_id', [config('evibe.event.special_experience')]);

		if ($pageId != config('evibe.ticket_type.packages'))
		{
			$packageIds = $this->getPackageIdsFilterByTag($occasionId, $pageId);
			$data = $data->whereIn('id', $packageIds);
		}

		if (is_null($tagTypeId))
		{
			$data = $data->select('planner_package.id', 'code', 'name', 'price', DB::raw('(select(priority) from priority WHERE priority.map_id = planner_package.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ' AND priority.type_tag_id IS NULL ) as packagePriority'))
			             ->orderBy(DB::raw('ISNULL(packagePriority), packagePriority'), 'ASC');
		}
		else
		{
			$data = $data->select('planner_package.id', 'code', 'name', 'price', DB::raw('(select(priority) from priority WHERE priority.map_id = planner_package.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ' AND priority.type_tag_id =' . $tagTypeId . ' ) as packagePriority'))
			             ->orderBy(DB::raw('ISNULL(packagePriority), packagePriority'), 'ASC');
			$tagTypeIdEvent = PackageTag::where('tile_tag_id', $tagTypeId)
			                             ->pluck('planner_package_id');
			if (!is_null($tagTypeIdEvent))
			{
				$data = $data->whereIn('planner_package.id', $tagTypeIdEvent);
			}
		}

		return $data;
	}

	private function getCoupleExpList($occasionId, $pageId, $cityId, $tagTypeId)
	{
		$data = VendorPackage::where('event_id', $occasionId)
		                      ->where('city_id', $cityId)
		                      ->where('is_live', 1);
		if (is_null($tagTypeId))
		{
			$data = $data->select('planner_package.id', 'code', 'name', 'price', DB::raw('(select(priority) from priority WHERE priority.map_id = planner_package.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ' AND priority.type_tag_id IS NULL ) as packagePriority'))
			             ->orderBy(DB::raw('ISNULL(packagePriority), packagePriority'), 'ASC');
		}
		else
		{
			$data = $data->select('planner_package.id', 'code', 'name', 'price', DB::raw('(select(priority) from priority WHERE priority.map_id = planner_package.id AND priority.map_type_id = ' . $pageId . ' AND priority.occasion_id = ' . $occasionId . ' AND priority.city_id =' . $cityId . ' AND priority.type_tag_id =' . $tagTypeId . ' ) as packagePriority'))
			             ->orderBy(DB::raw('ISNULL(packagePriority), packagePriority'), 'ASC');
			$tagTypeIdEvent = PackageTag::where('tile_tag_id', $tagTypeId)
			                             ->pluck('planner_package_id');
			if (!is_null($tagTypeIdEvent))
			{
				$data = $data->whereIn('planner_package.id', $tagTypeIdEvent);
			}
		}

		return $data;
	}
}
