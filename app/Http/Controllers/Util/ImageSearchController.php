<?php namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use DecorGallery;
use Illuminate\Support\Facades\Input;
use Evibe\Facades\AppUtilFacade as AppUtil;
use PackageGallery;

/**
 * Able to search images by IDs
 *
 * @author Anji <anji@evibe.in>
 * @since  12 Mar 2015
 */
class ImageSearchController extends BaseController
{
	public function showIndex()
	{
		$data = ['images' => []];
		$imageId = Input::get('q');

		// seach in packages
		if ($imageId)
		{
			// seach package gallery
			$packageImage = PackageGallery::with('package')->find($imageId);
			if ($packageImage)
			{
				$package = $packageImage->package;
				if ($package && $package->provider)
				{
					$provider = $package->provider;
					array_push($data['images'], [
						'path'     => AppUtil::getPackageGalleryUrl($packageImage),
						'code'     => 'PIC-' . $packageImage->id,
						'provider' => [
							'link' => $package->provider->getLocalLink(),
							'code' => $provider->code
						],
						'mapper'   => [
							'type'  => 'Package',
							'id'    => $package->id,
							'code'  => $package->code,
							'price' => $package->price,
							'url'   => $this->getPackageUrl($package)
						]
					]);
				}
			}

			// search decors
			$decorImage = DecorGallery::with('decor', 'decor.provider')->orWhere('code', 'LIKE', $imageId)->orWhere('id', $imageId)->first();
			if ($decorImage)
			{
				array_push($data['images'], [
					'path'     => $decorImage->getLink(),
					'code'     => $decorImage->code,
					'provider' => [
						'link' => $decorImage->decor->provider->getLocalLink(),
						'code' => $decorImage->decor->provider->code
					],
					'mapper'   => [
						'type'  => 'Decor',
						'id'    => $decorImage->decor->id,
						'code'  => $decorImage->decor->code,
						'price' => $decorImage->decor->min_price,
						'url'   => route('decor.info.view', $decorImage->decor->id)
					]
				]);
			}
		} //return count($data);

		return view('images-search/list', ['data' => $data]);
	}

	private function getPackageUrl($package)
	{
		$url = route('package.view', $package->id);

		return $url;
	}

}
