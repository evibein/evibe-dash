<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\Planner\Planner;
use App\Models\Settlement\Settlement;
use App\Models\Settlement\SettlementDetails;
use App\Models\Util\StarOption;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class DownloadSheetsController extends BaseController
{
	public function showDownloadableOptions()
	{
		$downloadOptionsData = [];
 		
		return view('downloads.home', ["downloadOptionsData" => $downloadOptionsData]);
	}

	public function starOrdersSettlementData($customDate)
	{
		if(!$customDate)
		{
			return  redirect()->back()
			                 ->withErrors("Date is must to process this request. try again");
		}
		// here the settlement week is from Monday to Sunday
		$startTime = Carbon::parse($customDate)->startOfWeek()->subWeeks(1)->toDateTimeString();
		$endTime = Carbon::parse($customDate)->startOfWeek()->subDays(1)->endOfDay()->toDateTimeString();
		$starOrders = \TicketBooking::with('provider')->select('ticket.name', 'ticket_mapping.map_id AS option_id', 'ticket_mapping.map_type_id AS option_type_id', 'schedule_settlement_bookings.settlement_amount', 'ticket_bookings.*')
		                            ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
		                            ->join('ticket', 'ticket.id', '=', 'ticket_mapping.ticket_id')
		                            ->join('schedule_settlement_bookings', 'schedule_settlement_bookings.ticket_booking_id', '=', 'ticket_bookings.id')
		                            ->where('ticket.status_id', config('evibe.status.booked'))
		                            ->where('ticket_bookings.is_advance_paid', 1)
		                            ->whereNull('ticket_bookings.cancelled_at')
		                            ->where('ticket_bookings.is_star_order', 1)
		                            ->where('ticket_bookings.party_date_time', '>=', strtotime($startTime))
		                            ->where('ticket_bookings.party_date_time', '<=', strtotime($endTime))
		                            ->get();

		$starOrdersData = [];

		if (count($starOrders))
		{
			$sheetTitle = date('d M Y', strtotime($startTime)) . " - " . date('d M Y', strtotime($endTime));
			$starOptions = StarOption::all();

			foreach ($starOrders as $starOrder)
			{
				$starOption = $starOptions->where('option_id', $starOrder->option_id)
				                          ->where('option_type_id', $starOrder->option_type_id)
				                          ->first();

				$transportCharges = $starOrder->transport_charges ?: 0;
				array_push($starOrdersData, [
					'name'               => $starOrder->name,
					'option_id'          => $starOrder->option_id,
					'option_type_id'     => $starOrder->option_type_id,
					'booking_id'         => $starOrder->booking_id,
					'partner'            => $starOrder->provider ? $starOrder->provider->name : null,
					'partner_id'         => $starOrder->map_id,
					'partner_type_id'    => $starOrder->map_type_id,
					'party_date'         => date('Y-m-d H:i:s', $starOrder->party_date_time),
					'booking_amount'     => $starOrder->booking_amount,
					'transport_charges'  => $transportCharges,
					'net_booking_amount' => $starOrder->booking_amount - $transportCharges,
					'advance_amount'     => $starOrder->advance_amount,
					'settlement_amount'  => $starOrder->settlement_amount,
					'vendor_amount'      => ($starOption && $starOption->partner_amount) ? $starOption->partner_amount : '--',
					'is_star_order'      => $starOrder->is_star_order,
				]);
			}

			Excel::create('Star Order Settlements', function ($excel) use ($starOrdersData, $sheetTitle) {
				$excel->sheet($sheetTitle, function ($sheet) use ($starOrdersData) {
					$sheet->loadView('downloads.star-orders-settlements-sheet', ['starOrdersData' => $starOrdersData]);
				});
			})->export('xls');
		}
		else
		{
			return redirect()->back()
			                 ->withErrors("Unable to find star order settlements between $startTime and $endTime");
		}
	}

	public function plannersData()
	{
		$planners = \Vendor::select('planner.*', 'bank_details.account_number', 'bank_details.name_in_bank', 'bank_details.bank_name', 'bank_details.branch_name', 'bank_details.branch_code', 'bank_details.branch_address', 'bank_details.ifsc_code', 'bank_details.micr_code')
		                   ->join('bank_details', 'bank_details.user_id', '=', 'planner.user_id')
		                   ->get()
		                   ->toArray();

		$sheetTitle = 'planners';
		Excel::create('Planners Data', function ($excel) use ($planners, $sheetTitle) {
			$excel->sheet($sheetTitle, function ($sheet) use ($planners) {
				$sheet->fromArray($planners);
			});
		})->export('xls');
	}

	public function venuesData()
	{
		$venues = \Venue::select('venue.*', 'bank_details.account_number', 'bank_details.name_in_bank', 'bank_details.bank_name', 'bank_details.branch_name', 'bank_details.branch_code', 'bank_details.branch_address', 'bank_details.ifsc_code', 'bank_details.micr_code')
		                ->join('bank_details', 'bank_details.user_id', '=', 'venue.user_id')
		                ->get()
		                ->toArray();

		$sheetTitle = 'venues';
		Excel::create('Venues Data', function ($excel) use ($venues, $sheetTitle) {
			$excel->sheet($sheetTitle, function ($sheet) use ($venues) {
				$sheet->fromArray($venues);
			});
		})->export('xls');
	}

	public function gstSettlementsData()
	{
		/* Important factors are service fee and gst calculations */
		/* Further division for normal orders (18% service fee as GST) and star orders (5% service fee as GST) */

		// will have a settlement month for sure
		$settlementMonth = Input::get('settlementMonth');
		$monthStart = Carbon::createFromTimestamp(strtotime($settlementMonth))->startOfMonth();
		$monthEnd = Carbon::createFromTimestamp(strtotime($settlementMonth))->endOfMonth();

		$settlements = Settlement::whereNotNull('settlement_done_at')
		                         ->where('settlement_done_at', '>=', $monthStart)
		                         ->where('settlement_done_at', '<=', $monthEnd)
		                         ->whereNull('deleted_at')
		                         ->get();

		if (!count($settlements))
		{
			return redirect()->back()
			                 ->withErrors("No settlements are available in the selected month");
		}

		$venues = \Venue::all();
		$planners = Planner::all();
		$cities = \City::all();

		$settlementArray = [];
		foreach ($settlements as $settlement)
		{
			$settlementId = $settlement->id;
			$partnerId = $settlement->partner_id;
			$partnerTypeId = $settlement->partner_type_id;

			$settlementData = [
				'settlementId'                => $settlementId,
				'settlementDoneAt'            => $settlement->settlement_done_at,
				'partnerId'                   => $partnerId,
				'partnerTypeId'               => $partnerTypeId,
				'settlementBookingAmount'     => $settlement->booking_amount,
				'settlementAdvanceAmount'     => $settlement->advance_amount,
				'settlementPartnerAmount'     => $settlement->settlement_amount,
				'settlementCGST'              => $settlement->cgst_amount,
				'settlementSGST'              => $settlement->sgst_amount,
				'settlementIGST'              => $settlement->igst_amount,
				'settlementGST'               => $settlement->gst_amount,
				'settlementEvibeFee'          => $settlement->evibe_service_fee,
				'settlementPartnerBoreRefund' => $settlement->partner_bore_refund,
			];

			if ($partnerTypeId == config('evibe.ticket_type.venues'))
			{
				$partner = $venues->where('id', $partnerId)->first();
			}
			else
			{
				$partner = $planners->where('id', $partnerId)->first();
			}

			if (!$partner)
			{
				return redirect()->back()
				                 ->withErrors("Unable to find partner details for the settlement: $settlementId with partnerId: $partnerId and partnerTypeId: $partnerTypeId");
			}

			$city = $cities->where('id', $partner->city_id)->first();

			$partnerData = [
				'company' => $partner->name,
				'person'  => $partner->person,
				'email'   => $partner->email,
				'phone'   => $partner->phone,
				'city'    => $city ? $city->name : null,
				'gstin'   => $partner->gstin
			];

			$settlementData['partnerData'] = $partnerData;

			$settlementDetails = SettlementDetails::select('schedule_settlement_bookings.booking_amount AS booking_amount', 'schedule_settlement_bookings.advance_amount AS advance_amount', 'schedule_settlement_bookings.evibe_service_fee AS evibe_service_fee', 'schedule_settlement_bookings.cgst_amount AS cgst_amount', 'schedule_settlement_bookings.sgst_amount AS sgst_amount', 'schedule_settlement_bookings.igst_amount AS igst_amount', 'schedule_settlement_bookings.gst_amount AS gst_amount', 'schedule_settlement_bookings.settlement_amount AS settlement_amount', 'schedule_settlement_bookings.partner_bore_refund AS partner_bore_refund', 'ticket_bookings.is_star_order AS is_star_order')
			                                      ->join('schedule_settlement_bookings', 'schedule_settlement_bookings.id', '=', 'settlement_details.settlement_booking_id')
			                                      ->join('ticket_bookings', 'ticket_bookings.id', '=', 'schedule_settlement_bookings.ticket_booking_id')
			                                      ->where('settlement_details.settlement_id', $settlementId)
			                                      ->wherenotNull('schedule_settlement_bookings.settlement_done_at')
			                                      ->whereNull('schedule_settlement_bookings.deleted_at')
			                                      ->whereNull('ticket_bookings.deleted_at')
			                                      ->whereNull('settlement_details.deleted_at')
			                                      ->get();

			$normalData = [
				'bookingAmount'     => 0,
				'advanceAmount'     => 0,
				'settlementAmount'  => 0,
				'evibeServiceFee'   => 0,
				'cgst'              => 0,
				'sgst'              => 0,
				'igst'              => 0,
				'gst'               => 0,
				'partnerBoreRefund' => 0
			];

			$starData = [
				'bookingAmount'     => 0,
				'advanceAmount'     => 0,
				'settlementAmount'  => 0,
				'evibeServiceFee'   => 0,
				'cgst'              => 0,
				'sgst'              => 0,
				'igst'              => 0,
				'gst'               => 0,
				'partnerBoreRefund' => 0
			];

			foreach ($settlementDetails as $datum)
			{
				if ($datum->is_star_order)
				{
					$starData['bookingAmount'] += $datum->booking_amount;
					$starData['advanceAmount'] += $datum->advance_amount;
					$starData['settlementAmount'] += $datum->settlement_amount;
					$starData['evibeServiceFee'] += $datum->evibe_service_fee;
					$starData['cgst'] += $datum->cgst_amount;
					$starData['sgst'] += $datum->sgst_amount;
					$starData['igst'] += $datum->igst_amount;
					$starData['gst'] += $datum->gst_amount;
					$starData['partnerBoreRefund'] += $datum->partner_bore_refund;
				}
				else
				{
					$normalData['bookingAmount'] += $datum->booking_amount;
					$normalData['advanceAmount'] += $datum->advance_amount;
					$normalData['settlementAmount'] += $datum->settlement_amount;
					$normalData['evibeServiceFee'] += $datum->evibe_service_fee;
					$normalData['cgst'] += $datum->cgst_amount;
					$normalData['sgst'] += $datum->sgst_amount;
					$normalData['igst'] += $datum->igst_amount;
					$normalData['gst'] += $datum->gst_amount;
					$normalData['partnerBoreRefund'] += $datum->partner_bore_refund;
				}
			}

			$settlementData['normalData'] = $normalData;
			$settlementData['starData'] = $starData;

			array_push($settlementArray, $settlementData);
		}

		$sheetTitle = "Monthly Settlement GST Sheet";

		Excel::create($sheetTitle, function ($excel) use ($settlementArray, $settlementMonth) {
			$excel->sheet($settlementMonth, function ($sheet) use ($settlementArray) {
				$sheet->loadView('downloads.monthly-settlements-gst-sheet', ['settlementArray' => $settlementArray]);
			});
		})->export('xls');

	}
}