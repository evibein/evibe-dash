<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\Ticket\TicketReview;

use App\Models\Util\TicketReviewQuestion;
use App\Models\Util\TicketReviewQuestionAnswer;
use App\Models\Util\TicketReviewQuestionOption;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class CustomerFeedbackController extends BaseController
{
	public function confirmCustomerFeedback($ticketId)
	{
		$ticketReview = TicketReview::where('ticket_id', $ticketId)->first();

		if ($ticketReview)
		{
			$ticketReview->update(['is_accepted' => 1, 'accepted_by' => Auth::user()->id]);

			$res = [
				'success' => true,
				'user'    => $ticketReview->handler ? $ticketReview->handler->name : 'Unknown user',
			];
		}
		else
		{
			$res = ['success' => 'false', 'error' => 'Ticket review not available'];
		}

		return response()->json($res);
	}

	public function addCustomerFeedback($ticketId)
	{
		$res = ['success' => false];

		$errorMessage = $this->validateEvibeFeedback();
		if ($errorMessage)
		{
			return response()->json(['success' => false, 'error' => $errorMessage]);
		}

		$extraQuestions = Input::get('extraQuestions');
		$hasExtraQuestion = count($extraQuestions) ? true : false;

		if ($hasExtraQuestion)
		{
			foreach ($extraQuestions as $questionData)
			{
				$question = TicketReviewQuestion::findOrFail($questionData['qId']);
				if (!array_key_exists('answerIds', $questionData) || !count($questionData['answerIds']))
				{
					return response()->json(['success' => false, 'error' => 'Question <b>' . $question->question . '</b> must have an answer']);
				}
			}
		}

		$newFeedback = TicketReview::create([
			                                    'ticket_id'          => $ticketId,
			                                    'customer_care'      => Input::get('customerCare'),
			                                    'ease_of_booking'    => Input::get('easeOfBooking'),
			                                    'price_for_quality'  => Input::get('priceForQuality'),
			                                    'billing_amount'     => Input::get('finalBillingAmount'),
			                                    'review'             => Input::get('feedbackComment'),
			                                    'is_recommend_evibe' => Input::get('evibeRecommendation')
		                                    ]);
		if ($newFeedback)
		{
			if ($hasExtraQuestion)
			{
				foreach ($extraQuestions as $questionData)
				{
					$answerData = [];
					foreach ($questionData['answerIds'] as $optionId)
					{
						$option = TicketReviewQuestionOption::find($questionData['answerIds']);

						if ($option)
						{
							$answerData[] = [
								'review_id'  => $newFeedback->id,
								'option_id'  => $optionId,
								'created_at' => Carbon::now(),
								'updated_at' => Carbon::now()
							];
						}
					}

					if (count($answerData) > 0)
					{
						TicketReviewQuestionAnswer::insert($answerData);
					}
				}
			}
			$res = ['success' => true];
		}

		return response()->json($res);
	}

	public function editCustomerFeedback($ticketId, $feedbackId)
	{
		$res = ['success' => false];
		$errorMessage = $this->validateEvibeFeedback();

		if ($errorMessage)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $errorMessage
			                        ]);
		}

		$extraQuestions = Input::get('extraQuestions');
		$hasExtraQuestion = count($extraQuestions) ? true : false;

		if ($hasExtraQuestion)
		{
			foreach ($extraQuestions as $questionData)
			{
				$question = TicketReviewQuestion::findOrFail($questionData['qId']);
				if (!array_key_exists('answerIds', $questionData) || !count($questionData['answerIds']))
				{
					return response()->json([
						                        'success' => false,
						                        'error'   => 'Question <b>' . $question->question . '</b> must have an answer'
					                        ]);
				}
			}
		}

		$ticketReview = TicketReview::find($feedbackId);
		if ($ticketReview)
		{
			$ticketReview = $ticketReview->update([
				                                      'customer_care'      => Input::get('customerCare'),
				                                      'ease_of_booking'    => Input::get('easeOfBooking'),
				                                      'price_for_quality'  => Input::get('priceForQuality'),
				                                      'billing_amount'     => Input::get('finalBillingAmount'),
				                                      'review'             => Input::get('feedbackComment'),
				                                      'is_recommend_evibe' => Input::get('evibeRecommendation')
			                                      ]);

			if ($ticketReview && $hasExtraQuestion)
			{
				$saveOptionIds = [];
				foreach ($extraQuestions as $questionData)
				{
					foreach ($questionData['answerIds'] as $optionId)
					{
						$saveOptionIds[] = $optionId;
						$option = TicketReviewQuestionOption::find($optionId);

						if ($option)
						{
							$answer = TicketReviewQuestionAnswer::where('review_id', $ticketReview->id)
							                                    ->where('option_id', $optionId)
							                                    ->first();
							if (!$answer)
							{
								TicketReviewQuestionAnswer::create([
									                                   'review_id'  => $ticketReview->id,
									                                   'option_id'  => $optionId,
									                                   'created_at' => Carbon::now(),
									                                   'updated_at' => Carbon::now()
								                                   ]);
							}
						}
					}
				}

				TicketReviewQuestionAnswer::whereNotIn("option_id", $saveOptionIds)
				                          ->where("review_id", $ticketReview->id)
				                          ->delete();

				$res = ['success' => true];
			}
		}

		return response()->json($res);
	}

	public function showPendingFeedback(Request $request)
	{
		return(view("errors.405"));

		$perPage = 30;
		$ticketIds = \Ticket::where('status_id', config('evibe.status.booked'))
		                    ->where('event_date', '<', time())
		                    ->whereNull('feedback_received_at')
		                    ->where(function ($query) {
			                    $query->whereNotNull('feedback_sms_sent_at')
			                          ->orWhereNotNull('feedback_email_sent_at');
		                    })
		                    ->pluck("id")
		                    ->toArray();

		$validTicketReviews = TicketReview::whereIn("ticket_id", $ticketIds)
		                                  ->pluck("ticket_id")
		                                  ->toArray();

		$ticketIds = array_diff($ticketIds, array_unique($validTicketReviews));

		$tickets = \Ticket::whereIn('id', $ticketIds)
		                  ->orderBy('event_date', 'DESC')
		                  ->paginate($perPage);

		$data = [
			'tickets'      => $tickets,
			'slStartCount' => $this->getPaginatedSlNo($perPage)
		];

		return view('feedback.pending')->with($data);
	}

	public function showPendingApproval(Request $request)
	{
		return(view("errors.405"));

		$perPage = 30;
		$ticketIds = [];

		$tickets = \Ticket::select("ticket.id", "event_date", "ticket.name", "phone", "alt_phone", "ticket_review.is_accepted")
		                  ->join('ticket_review', 'ticket_review.ticket_id', '=', 'ticket.id')
		                  ->where('status_id', config('evibe.status.booked'))
		                  ->where('event_date', '<', time())
		                  ->where(function ($query) {
			                  $query->whereNotNull('feedback_sms_sent_at')
			                        ->orWhereNotNull('feedback_email_sent_at');
		                  })->get();

		foreach ($tickets as $ticket)
		{
			if ($ticket->is_accepted == 0)
			{
				$ticketIds[] = $ticket->id;
			}
			else
			{
				$hasPendingApproval = false;
				$notReviewed = false;
				$bookings = $ticket->bookings;

				foreach ($bookings as $booking)
				{
					// checking if all of the bookings are accepted
					if (!$booking->vendorReview)
					{
						$notReviewed = true;
						break;
					}
				}

				if ($notReviewed)
				{
					continue;
				}

				foreach ($bookings as $booking)
				{
					if ($booking->vendorReview->is_accepted == 0)
					{
						$hasPendingApproval = true;
						break;
					}
				}

				if ($hasPendingApproval)
				{
					$ticketIds[] = $ticket->id;
				}
			}
		}

		$tickets = \Ticket::whereIn('id', $ticketIds)
		                  ->orderBy('event_date', 'DESC')
		                  ->paginate($perPage);

		$data = [
			'tickets'      => $tickets,
			'slStartCount' => $this->getPaginatedSlNo($perPage)
		];

		return view('feedback.pending')->with($data);
	}

	private function validateEvibeFeedback()
	{
		$error = '';
		$rules = [
			'customerCare'       => 'required|numeric|min:0.5|max:5',
			'easeOfBooking'      => 'required|numeric|min:0.5|max:5',
			'priceForQuality'    => 'required|numeric|min:0.5|max:5',
			'finalBillingAmount' => 'numeric'
		];

		$messages = [
			'customerCare.required'      => 'Please rate customer care rating on scale of 5',
			'customerCare.numeric'       => 'Please rate customer care rating on scale of 5',
			'customerCare.min'           => 'Please rate customer care rating on scale of 5',
			'customerCare.max'           => 'Please rate customer care rating on scale of 5',
			'easeOfBooking.required'     => 'Please rate ease of booking on scale of 5',
			'easeOfBooking.numeric'      => 'Please rate ease of booking on scale of 5',
			'easeOfBooking.min'          => 'Please rate ease of booking on scale of 5',
			'easeOfBooking.max'          => 'Please rate ease of booking on scale of 5',
			'priceForQuality.required'   => 'Please rate price for quality on scale of 5',
			'priceForQuality.numeric'    => 'Please rate price for quality on scale of 5',
			'priceForQuality.min'        => 'Please rate price for quality on scale of 5',
			'priceForQuality.max'        => 'Please rate price for quality on scale of 5',
			'finalBillingAmount.numeric' => 'Please enter valid final billing amount'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);
		if ($validator->fails())
		{
			$error = $validator->messages()->first();
		}

		return $error;
	}
}
