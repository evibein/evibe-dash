<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BasePackageController;
use App\Http\Controllers\Base\DecorBaseController;
use App\Http\Controllers\Package\CreatePackageController;
use App\Http\Controllers\Services\ServiceController;
use App\Http\Controllers\Trend\TrendsController;
use App\Models\Cake\CakeFieldValue;
use App\Models\DecorEvent;
use App\Models\InternalQuestion;
use App\Models\ServiceEvent;
use App\Models\ServiceGallery;
use App\Models\TrendEvents;
use App\Models\TypeEvent;
use App\Models\TypeServices;
use App\Models\Util\PackageFieldValue;
use Cake;
use CakeEvent;
use CakeGallery;
use CakeTags;
use Decor;
use DecorGallery;
use DecorTags;
use Illuminate\Support\Facades\Input;
use PackageGallery;
use PackageTag;
use Trend;
use TrendGallery;
use TypeTicket;
use Vendor;
use VendorPackage;
use Venue;

class CopyOptionController extends BasePackageController
{
	public function showCopyModel()
	{
		$pageType = null;
		$package = null;
		$provider = null;
		$typeTicketId = Input::get('typeTicketId');
		$occasion = TypeEvent::select("id", "name")->get()->toArray();
		$name = "select an option :";

		switch ($typeTicketId)
		{
			Case config('evibe.ticket_type.cakes'):
				$provider = Vendor::select("id", "name")->orderBy('name')->get()->toArray();
				$package = Cake::select("id", "title", "code")->get()->toArray();
				$name = "Select cake :";
				break;

			Case config('evibe.ticket_type.decors'):
				$provider = Vendor::select("id", "name")->orderBy('name')->get()->toArray();
				$package = Decor::select("id", "name", "code")->get()->toArray();
				$name = "Select decor :";
				break;

			Case config('evibe.ticket_type.trends'):
				$provider = Vendor::select("id", "name")->whereIn('type_id', [1, 2])->orderBy('name')->get()->toArray();
				$package = Trend::select("id", "name", "code")->get()->toArray();
				$name = "Select trend :";
				break;

			Case config('evibe.ticket_type.services'):
				$package = TypeServices::select("id", "name", "code")->get()->toArray();
				$name = "Select service :";
				break;

			Case config('evibe.ticket_type.planners'):
				$provider = Vendor::select("id", "name")->where('is_artist', 0)->orderBy('name')->get()->toArray();
				$package = VendorPackage::where('map_type_id', config('evibe.ticket_type.planners'))
				                         ->select("id", "name", "code")
				                         ->get()
				                         ->toArray();
				$pageType = TypeTicket::where('is_package', 1)->select("id", "name")->get()->toArray();
				$name = "Select package :";
				break;

			Case config('evibe.ticket_type.venues'):
				$provider = Venue::select("id", "name")->orderBy('name')->get()->toArray();
				$package = VendorPackage::where('map_type_id', config('evibe.ticket_type.venues'))
				                         ->select("id", "name", "code")
				                         ->get()
				                         ->toArray();
				$pageType = TypeTicket::where('is_package', 1)->select("id", "name")->get()->toArray();
				$name = "Select venue package :";
				break;

			Case config('evibe.ticket_type.artists'):
				$provider = Vendor::select("id", "name")->where('is_artist', 1)->orderBy('name')->get()->toArray();
				$package = VendorPackage::where('map_type_id', config('evibe.ticket_type.artists'))
				                         ->select("id", "name", "code")
				                         ->get()
				                         ->toArray();
				$pageType = TypeTicket::where('is_package', 1)->select("id", "name")->get()->toArray();
				$name = "Select artist package :";
				break;
		};

		return response()->json([
			                        'name'           => $name,
			                        'success'        => true,
			                        'providers'      => $provider,
			                        'occasions'      => $occasion,
			                        'packages'       => $package,
			                        'pageTypes'      => $pageType,
			                        'countProviders' => count($provider),
			                        'countOccasions' => count($occasion),
			                        'countPackages'  => count($package),
			                        'countPageTypes' => count($pageType)
		                        ]);
	}

	public function saveCopyModel()
	{
		$data = request()->all();
		$redirectUrl = route('ticket.list');

		switch ($data['mapTypeId'])
		{
			Case config('evibe.ticket_type.cakes'):
				$redirectUrl = $this->copyCakeData($data['mapId'], $data['providerId'], $data['occasionId']);
				break;

			Case config('evibe.ticket_type.decors'):
				$redirectUrl = $this->copyDecorData($data['mapId'], $data['providerId'], $data['occasionId']);
				break;

			Case config('evibe.ticket_type.trends'):
				$redirectUrl = $this->copyTrendData($data['mapId'], $data['providerId'], $data['occasionId']);
				break;

			Case config('evibe.ticket_type.services'):
				$redirectUrl = $this->copyServiceData($data['mapId'], $data['occasionId']);
				break;

			Case config('evibe.ticket_type.planners'):
			Case config('evibe.ticket_type.venues'):
			Case config('evibe.ticket_type.artists'):
				$redirectUrl = $this->copyPackageData(['copyData' => $data]);
				break;
		};

		return response()->json([
			                        'success'  => true,
			                        'redirect' => $redirectUrl
		                        ]);
	}

	private function copyCakeData($cakeId, $providerId, $occasionId)
	{
		$cake = Cake::find($cakeId);
		$cakeFieldValues = CakeFieldValue::where('cake_id', $cakeId)->get();
		$cakeTags = CakeTags::where('cake_id', $cakeId)->get();
		$iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $cakeId, 'map_type_id' => config('evibe.ticket_type.cakes')])
		                       ->get();

		// old path of the images
		$oldPath = config('evibe.gallery.root') . '/cakes/' . $cake->id . '/';

		// getting the data of the row we need to copy
		$copyCake = $cake->replicate();

		// modifying the data for the new cake
		$copyCake->is_live = 0;
		$copyCake->provider_id = $providerId;
		$copyCake->title = $copyCake->title . " copy";
		$copyCake->url = $this->generateUrl($copyCake->title);
		$copyCake->save();

		// updating the code based on the new id generated after creating the copy of the row.
		$copyCake->code = config('evibe.cake.code') . (config('evibe.cake.base_id') + $copyCake->id);
		$copyCake->save();

		//new path of the images
		$newPath = config('evibe.gallery.root') . '/cakes/' . $copyCake->id . '/';

		// getting the old images and creating new images one by one
		$images = CakeGallery::where('cake_id', $cakeId)->select("url", "title", "type")->get();
		foreach ($images as $image)
		{
			$copyImage = $image->replicate();
			$copyImage->cake_id = $copyCake->id;
			$copyImage->save();
			//move the images to new path
			if ($image->type == 0)
			{
				$this->changeImagePath($oldPath, $newPath, $image->url);
			}
		}

		// Creating cake field values for copy cake from selected cake one by one
		foreach ($cakeFieldValues as $cakeFieldValue)
		{
			$copyCakeFieldValue = $cakeFieldValue->replicate();
			$copyCakeFieldValue->cake_id = $copyCake->id;
			$copyCakeFieldValue->save();
		}

		// Creating cake tags for copy cake from selected cake one by one
		foreach ($cakeTags as $cakeTag)
		{
			$copyCakeTag = $cakeTag->replicate();
			$copyCakeTag->cake_id = $copyCake->id;
			$copyCakeTag->save();
		}

		// Creating cake event for copy cake from selected cake
		CakeEvent::create([
			                   'cake_id'  => $copyCake->id,
			                   'event_id' => $occasionId
		                   ]);

		// Creating cake Iqs for copy cake from selected cake one by one
		foreach ($iqs as $iq)
		{
			$copyCakeIq = $iq->replicate();
			$copyCakeIq->map_id = $copyCake->id;
			$copyCakeIq->save();
		}

		return route('cakes.edit', $copyCake->id);
	}

	private function copyDecorData($decorId, $providerId, $occasionId)
	{
		$decor = Decor::find($decorId);
		$decorBaseController = new DecorBaseController();
		$decorTags = DecorTags::where('decor_id', $decorId)->get();
		$iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $decorId, 'map_type_id' => config('evibe.ticket_type.decors')])
		                       ->get();

		// old path of the images
		$oldPath = config('evibe.gallery.root') . '/decors/' . $decor->id . '/images/';

		// getting the data of the row we need to copy
		$copyDecor = $decor->replicate();

		// modifying the data for the new decor
		$copyDecor->is_live = 0;
		$copyDecor->provider_id = $providerId;
		$copyDecor->name = $copyDecor->name . " copy";
		$copyDecor->url = $this->generateUrl($copyDecor->name);
		$copyDecor->code = $decorBaseController->generateDecorCode();
		$copyDecor->save();

		//new path of the images
		$newPath = config('evibe.gallery.root') . '/decors/' . $copyDecor->id . '/images/';

		// getting the old images and creating new images one by one
		$images = DecorGallery::where('decor_id', $decorId)->select("url", "title", "type_id")->get();
		foreach ($images as $image)
		{
			$copyImage = $image->replicate();
			$copyImage->decor_id = $copyDecor->id;
			$copyImage->code = $decorBaseController->generateDecorGalleryCode();
			$copyImage->save();
			//move the images to new path
			if ($image->type_id == 0)
			{
				$this->changeImagePath($oldPath, $newPath, $image->url);
			}
		}

		// Creating decor tags for copy decor from selected decor one by one
		foreach ($decorTags as $decorTag)
		{
			$copyDecorTag = $decorTag->replicate();
			$copyDecorTag->decor_id = $copyDecor->id;
			$copyDecorTag->save();
		}

		// Creating decor event for copy decor from selected Decor
		DecorEvent::create([
			                   'decor_id' => $copyDecor->id,
			                   'event_id' => $occasionId
		                   ]);

		// Creating decor Iqs for copy decor from selected decor one by one
		foreach ($iqs as $iq)
		{
			$copyDecorIq = $iq->replicate();
			$copyDecorIq->map_id = $copyDecor->id;
			$copyDecorIq->save();
		}

		return route('decor.info.view', $copyDecor->id);
	}

	private function copyTrendData($trendId, $providerId, $occasionId)
	{
		$trend = Trend::find($trendId);
		$trendsController = new TrendsController();
		$iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $trendId, 'map_type_id' => config('evibe.ticket_type.trends')])
		                       ->get();

		// old path of the images
		$oldPath = config('evibe.gallery.root') . '/trends/' . $trend->id . '/images/';

		// getting the data of the row we need to copy
		$copyTrend = $trend->replicate();

		// modifying the data for the new packages
		$copyTrend->is_live = 0;
		$copyTrend->planner_id = $providerId;
		$copyTrend->name = $copyTrend->name . " copy";
		$copyTrend->url = $this->generateUrl($copyTrend->name);
		$copyTrend->code = $trendsController->generateCode($providerId);
		$copyTrend->save();

		//new path of the images
		$newPath = config('evibe.gallery.root') . '/trends/' . $copyTrend->id . '/images/';

		// getting the old images and creating new images one by one
		$images = TrendGallery::where('trending_id', $trendId)->select("url", "title", "type")->get();
		foreach ($images as $image)
		{
			$copyImage = $image->replicate();
			$copyImage->trending_id = $copyTrend->id;
			$copyImage->is_profile = 0;
			$copyImage->save();
			//move the images to new path
			if ($image->type == 0)
			{
				$this->changeImagePath($oldPath, $newPath, $image->url);
			}
		}

		// Creating trend event for copy trend from selected trend
		$trendEvent = TrendEvents::create([
			                                  'trend_id'      => $copyTrend->id,
			                                  'type_event_id' => $occasionId
		                                  ]);

		// Creating trend Iqs for copy trend from selected trend one by one
		foreach ($iqs as $iq)
		{
			$copyTrendIq = $iq->replicate();
			$copyTrendIq->map_id = $copyTrend->id;
			$copyTrendIq->save();
		}

		return config('evibe.host') . 'trends/edit/' . $copyTrend->id;
	}

	private function copyServiceData($serviceId, $occasionId)
	{
		$service = TypeServices::find($serviceId);
		$serviceController = new ServiceController();
		$iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $serviceId, 'map_type_id' => config('evibe.ticket_type.services')])
		                       ->get();

		// old path of the images
		$oldPath = config('evibe.gallery.root') . '/services/' . $serviceId . '/images/';

		// getting the data of the row we need to copy
		$copyService = $service->replicate();

		// modifying the data for the new packages
		$copyService->is_live = 0;
		$copyService->name = $copyService->name . " copy";
		$copyService->url = $this->generateUrl($copyService->name);
		$copyService->code = $serviceController->generateCode();
		$copyService->save();

		//new path of the images
		$newPath = config('evibe.gallery.root') . '/services/' . $copyService->id . '/images/';

		// getting the old images and creating new images one by one
		$images = ServiceGallery::where('type_service_id', $serviceId)->select("url", "title", "type_id")->get();
		foreach ($images as $image)
		{
			$copyImage = $image->replicate();
			$copyImage->type_service_id = $copyService->id;
			$copyImage->is_profile = 0;
			$copyImage->save();
			//move the images to new path
			if ($image->type_id == 0)
			{
				$this->changeImagePath($oldPath, $newPath, $image->url);
			}
		}

		// Creating service event for copy service from selected service
		ServiceEvent::create([
			                     'type_service_id' => $copyService->id,
			                     'type_event_id'   => $occasionId
		                     ]);

		// Creating service Iqs for copy service from selected service one by one
		foreach ($iqs as $iq)
		{
			$copyServiceIq = $iq->replicate();
			$copyServiceIq->map_id = $copyService->id;
			$copyServiceIq->save();
		}

		return route('services.edit', $copyService->id);
	}

	private function copyPackageData($data)
	{
		if (is_array($data))
		{
			$packageId = $data['copyData']['mapId'];
			$occasionId = $data['copyData']['occasionId'];
			$mapTypeId = $data['copyData']['mapTypeId'];
			$newProviderId = $data['copyData']['providerId'];
			$pageTypeId = $data['copyData']['pageTypeId'];

			$package = VendorPackage::find($packageId);
			$createPackageController = new CreatePackageController();
			$packageFieldValues = PackageFieldValue::where('package_id', $packageId)->get();
			$packageTags = PackageTag::where('planner_package_id', $packageId)->get();
			$iqs = InternalQuestion::withTrashed()
			                       ->where(['map_id' => $packageId, 'map_type_id' => config('evibe.ticket_type.packages')])
			                       ->get();

			$provider = Vendor::find($package->map_id);
			$newProvider = Vendor::find($newProviderId);
			$providerType = 'planner';
			if ($mapTypeId == config('evibe.ticket_type.venues'))
			{
				$provider = Venue::find($package->map_id);
				$newProvider = Venue::find($newProviderId);
				$providerType = 'venues';
			}

			// old path of the images
			$oldPath = config('evibe.gallery.root') . '/' . $providerType . '/' . $provider->id . '/images/packages/' . $packageId . '/';

			// getting the data of the row we need to copy
			$copyPackage = $package->replicate();

			// modifying the data for the new packages
			$copyPackage->is_live = 0;
			$copyPackage->map_id = $newProviderId;
			$copyPackage->name = $copyPackage->name . " copy";
			if($package->map_type_id == config("evibe.ticket_type.venues")) {
				$copyPackage->url = $this->generateUrl($package->name . " " . $newProvider->area->name . " " . $copyPackage->id, false);
			}
			else
			{
				$copyPackage->url = $this->generateUrl($package->name . " " . $copyPackage->id, false);
			}
			$copyPackage->event_id = $occasionId;
			$copyPackage->city_id = $newProvider->city->id;
			$copyPackage->type_ticket_id = $pageTypeId;
			$copyPackage->code = $createPackageController->generatePkgCode($newProvider);
			$copyPackage->save();

			$duplicateName = VendorPackage::where("url", "LIKE", $copyPackage->url)->get();
			if ($duplicateName->count() > 1)
			{
				$copyPackage->update([
					                     "url" => $package->url . "-" . $copyPackage->id
				                     ]);
			}

			//new path of the images
			$newPath = config('evibe.gallery.root') . '/' . $providerType . '/' . $newProviderId . '/images/packages/' . $copyPackage->id . '/';

			// getting the old images and creating new images one by one
			$images = PackageGallery::where('planner_package_id', $packageId)->select("url", "title", "type")->get();
			foreach ($images as $image)
			{
				$copyImage = $image->replicate();
				$copyImage->planner_package_id = $copyPackage->id;
				$copyImage->save();
				//move the images to new path
				if ($image->type == 0)
				{
					$this->changeImagePath($oldPath, $newPath, $image->url);
				}
			}

			// Creating cake tags for copy cake from selected cake one by one
			foreach ($packageTags as $packageTag)
			{
				$copyPackageTag = $packageTag->replicate();
				$copyPackageTag->planner_package_id = $copyPackage->id;
				$copyPackageTag->save();
			}

			// Creating cake Iqs for copy cake from selected cake one by one
			foreach ($iqs as $iq)
			{
				$copyPackageIq = $iq->replicate();
				$copyPackageIq->map_id = $copyPackage->id;
				$copyPackageIq->save();
			}

			// Dynamic fields for the venue
			if ($packageFieldValues)
			{
				foreach ($packageFieldValues as $packageFieldValue)
				{
					$copyPackageField = $packageFieldValue->replicate();
					$copyPackageField->package_id = $copyPackage->id;
					$copyPackageField->save();
				}
			}

			return route('package.edit', $copyPackage->id);
		}

		return null;
	}
}