<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\Util\OptionAvailability;
use Carbon\Carbon;

class OptionAvailabilityController extends BaseController
{
	public function getAvailabilitySlots()
	{
		$availabilityDate = "2020-02-14 00:00:00";
		$availabilityDateStart = Carbon::createFromTimestamp(strtotime($availabilityDate))->startOfDay()->timestamp;
		$availabilityDateEnd = Carbon::createFromTimestamp(strtotime($availabilityDate))->endOfDay()->timestamp;

		$cityId = request('city') ?: 1; // default bangalore

		$vDayVenuePackages = \VendorPackage::select('planner_package.id', 'planner_package.code', 'planner_package.name', 'planner_package.price', 'venue.name AS partner_name', 'planner_package.map_id AS partner_id', 'planner_package.map_type_id AS partner_type_id', 'area.name AS area')
		                                   ->join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
		                                   ->join('venue', 'venue.id', '=', 'planner_package.map_id')
		                                   ->leftJoin('area', 'area.id', '=', 'planner_package.area_id')
		                                   ->where('planner_package_tags.tile_tag_id', config('evibe.tags.valentines-day-2020'))
		                                   ->where('planner_package.map_type_id', config('evibe.ticket_type.venues'))
		                                   ->where('planner_package.is_live', 1)
		                                   ->where('planner_package.type_ticket_id', config('evibe.ticket_type.couple-experiences'))
		                                   ->where('planner_package.city_id', $cityId)
		                                   ->get();

		$vDayVenuePackageIds = $vDayVenuePackages->pluck('id');

		$optionAvailabilitySlots = OptionAvailability::where('map_type_id', config('evibe.ticket_type.couple-experiences'))
		                                             ->whereIn('map_id', $vDayVenuePackageIds)
		                                             ->get();

		$bookings = \TicketBooking::select('ticket_bookings.id', 'ticket_bookings.party_date_time', 'ticket.id AS ticket_id', 'ticket_mapping.map_id AS option_id', 'ticket_mapping.map_type_id AS option_type_id')
		                          ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
		                          ->join('ticket', 'ticket.id', '=', 'ticket_mapping.ticket_id')
		                          ->whereIn('ticket_mapping.map_id', $vDayVenuePackageIds)
		                          ->whereNull('ticket_bookings.deleted_at')
		                          ->whereNull('ticket_mapping.deleted_at')
		                          ->whereNull('ticket.deleted_at')
		                          ->whereNull('ticket_bookings.cancelled_at')
		                          ->where('ticket.status_id', config('evibe.status.booked'))
		                          ->where('ticket_bookings.is_advance_paid', 1)
		                          ->where('ticket_bookings.party_date_time', '>=', $availabilityDateStart)
		                          ->where('ticket_bookings.party_date_time', '<=', $availabilityDateEnd)
		                          ->get();

		$packagesData = [];

		foreach ($vDayVenuePackages AS $vDayVenuePackage)
		{
			// @check: if anything is not being replaced at every loop in packageData
			$packageData = [
				'id'            => $vDayVenuePackage->id,
				'optionTypeId'  => config('evibe.ticket_type.couple-experiences'),
				'name'          => $vDayVenuePackage->name,
				'code'          => $vDayVenuePackage->code,
				'link'          => config('evibe.host') . 'packages/view/' . $vDayVenuePackage->id,
				'price'         => $vDayVenuePackage->price,
				'area'          => $vDayVenuePackage->area,
				'partnerName'   => $vDayVenuePackage->partner_name,
				'partnerLink'   => config('evibe.host') . 'venues/view/' . $vDayVenuePackage->partner_id,
				'partnerId'     => $vDayVenuePackage->partner_id,
				'partnerTypeId' => $vDayVenuePackage->partner_type_id,
				'slots'         => []
			];

			$packageSlots = $optionAvailabilitySlots->where('map_id', $vDayVenuePackage->id)->where('map_type_id', config('evibe.ticket_type.couple-experiences'));

			if (count($packageSlots))
			{
				foreach ($packageSlots AS $packageSlot)
				{
					$todaySlotStart = Carbon::createFromTimestamp(strtotime($packageSlot->slot_start_time))->timestamp;
					$todaySlotEnd = Carbon::createFromTimestamp(strtotime($packageSlot->slot_end_time))->timestamp;
					$todayStart = Carbon::createFromTimestamp(strtotime($packageSlot->slot_start_time))->startOfDay()->timestamp;

					$availabilitySlotStart = $availabilityDateStart + ($todaySlotStart - $todayStart);
					$availabilitySlotEnd = $availabilityDateStart + ($todaySlotEnd - $todayStart);

					$slotBookings = $bookings->where('option_id', $vDayVenuePackage->id)
					                         ->where('option_type_id', config('evibe.ticket_type.couple-experiences'))
					                         ->where('party_date_time', '>=', $availabilitySlotStart)
					                         ->where('party_date_time', '<', $availabilitySlotEnd)
					                         ->count();

					array_push($packageData['slots'], [
						'slotStartTime' => $packageSlot->slot_start_time,
						'slotEndTime'   => $packageSlot->slot_end_time,
						'total'         => $packageSlot->bookings_available,
						'booked'        => $slotBookings,
						'available'     => $packageSlot->bookings_available - $slotBookings
					]);
				}
			}

			array_push($packagesData, $packageData);
		}

		$data = [
			'availabilityDate' => $availabilityDate,
			'cityId'           => $cityId,
			'packagesData'     => $packagesData
		];

		return view('util.option-availability', ['data' => $data]);
	}
}