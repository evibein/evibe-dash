<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\Planner\Planner;
use App\Models\ServiceGallery;
use App\Models\TypeEvent;
use App\Models\TypeServices;
use App\Models\Vendor\DeliveryGallery;
use App\Models\Vendor\SelectedDeliveryImages;
use Cake;
use CakeGallery;
use Carbon\Carbon;
use City;
use Decor;
use DecorGallery;
use Illuminate\Support\Facades\Auth;
use PackageGallery;
use Tags;
use TicketBooking;
use Trend;
use TrendGallery;
use TypeTicket;
use VendorPackage;
use Venue;
use VenueGallery;

class DeliveryImageController extends BaseController
{
	public function showDeliveryImages()
	{
		$cities = City::select("id", "name")->get();
		$typeEvent = TypeEvent::select('id', 'name')->get();
		$typeTicket = TypeTicket::select('id', 'name')->get();
		$cityHeader = request('cityId');
		$eventHeader = request('eventId');
		$typeTicketHeader = request('typeTicketId');
		$typeImages = request('typeImages');
		$plannerId = request('planner');
		$priceRange = request('priceRange');
		$deliveryImageData = null;
		$selectedImageData = null;
		$isProfile = $typeImages == 3 ? 1 : 0;
		$plannerData = $this->getPlannerData($typeImages, $cityHeader, $typeTicketHeader, $eventHeader);
		if ($typeImages == 1 || $typeImages == 3)
		{
			$selectedImageData = $this->getSelectedImageData($cityHeader, $typeTicketHeader, $eventHeader, $plannerId, $isProfile, $priceRange);
		}
		else
		{
			$deliveryImageData = $this->getImageData($cityHeader, $typeTicketHeader, $eventHeader, $typeImages, $plannerId, $priceRange);
		}

		$data = [
			'plannerData'   => $plannerData,
			'selectedImage' => $selectedImageData,
			'imageUrl'      => $deliveryImageData,
			'cities'        => $cities,
			'typeEvent'     => $typeEvent,
			'typeTicket'    => $typeTicket,
		];

		return view('delivery-image/list', ['data' => $data]);
	}

	public function getSelectedImageData($city, $mapType, $event, $planner, $isProfile, $priceRange)
	{
		$mapTypeIds = SelectedDeliveryImages::where('is_profile', $isProfile);
		$selectedImages = [];
		if ($isProfile == 0)
		{
			$selectedImages = $mapTypeIds->select('image_url as url', 'id')
			                             ->get();
		}
		else
		{
			$mapTypeIds = $mapTypeIds->select('selected_delivery_images.map_type_id', 'selected_delivery_images.id')
			                         ->join('ticket_bookings', 'selected_delivery_images.booking_id', '=', 'ticket_bookings.id')
			                         ->join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id');

			if ($city)
			{
				$mapTypeIds = $mapTypeIds->where('ticket.city_id', $city);
			}
			if ($mapType)
			{
				$mapTypeIds = $mapTypeIds->where('selected_delivery_images.map_type_id', $mapType);
			}
			if ($event)
			{
				$mapTypeIds = $mapTypeIds->where('ticket.event_id', $event);
			}
			if ($planner)
			{
				$mapTypeIds = $mapTypeIds->where('ticket_bookings.map_id', '=', $planner);
			}
			if ($priceRange)
			{
				$minPrice = config('evibe.price_filter.' . $priceRange . '.min');
				$maxPrice = config('evibe.price_filter.' . $priceRange . '.max');
				if ($maxPrice == 0)
				{
					$mapTypeIds = $mapTypeIds->where('ticket_bookings.booking_amount', '>', $minPrice);
				}
				else
				{
					$mapTypeIds = $mapTypeIds->whereBetween('ticket_bookings.booking_amount', [$minPrice - 1, $maxPrice + 1]);
				}
			}
			$mapTypeIds = $mapTypeIds->get();
			foreach ($mapTypeIds as $mapTypeId)
			{
				switch ($mapTypeId->map_type_id)
				{
					case config('evibe.ticket_type.priests'):
					case config('evibe.ticket_type.tents'):
					case config('evibe.ticket_type.food'):
					case config('evibe.ticket_type.packages'):
					case config('evibe.ticket_type.venues'):
						$data = SelectedDeliveryImages::join('planner_package', 'selected_delivery_images.map_id', '=', 'planner_package.id')
						                              ->select('planner_package.code', 'selected_delivery_images.image_url', 'selected_delivery_images.id')
						                              ->where('selected_delivery_images.id', $mapTypeId->id)
						                              ->first();
						array_push($selectedImages, ['id' => $data['id'], 'code' => $data['code'], 'url' => $data['image_url']]);
						break;
					case config('evibe.ticket_type.planners'):
						$data = SelectedDeliveryImages::join('planner', 'selected_delivery_images.map_id', '=', 'planner.id')
						                              ->select('planner.code', 'selected_delivery_images.image_url', 'selected_delivery_images.id')
						                              ->where('selected_delivery_images.id', $mapTypeId->id)
						                              ->first();
						array_push($selectedImages, ['id' => $data['id'], 'code' => $data['code'], 'url' => $data['image_url']]);
						break;
					case config('evibe.ticket_type.services'):
						$data = SelectedDeliveryImages::join('type_service', 'selected_delivery_images.map_id', '=', 'type_service.id')
						                              ->select('type_service.code', 'selected_delivery_images.image_url', 'selected_delivery_images.id')
						                              ->where('selected_delivery_images.id', $mapTypeId->id)
						                              ->first();
						array_push($selectedImages, ['id' => $data['id'], 'code' => $data['code'], 'url' => $data['image_url']]);
						break;
					case config('evibe.ticket_type.trends'):
						$data = SelectedDeliveryImages::join('trending', 'selected_delivery_images.map_id', '=', 'trending.id')
						                              ->select('trending.code', 'selected_delivery_images.image_url', 'selected_delivery_images.id')
						                              ->where('selected_delivery_images.id', $mapTypeId->id)
						                              ->first();
						array_push($selectedImages, ['id' => $data['id'], 'code' => $data['code'], 'url' => $data['image_url']]);
						break;
					case config('evibe.ticket_type.cakes'):
						$data = SelectedDeliveryImages::join('cake', 'selected_delivery_images.map_id', '=', 'cake.id')
						                              ->select('cake.code', 'selected_delivery_images.image_url', 'selected_delivery_images.id')
						                              ->where('selected_delivery_images.id', $mapTypeId->id)
						                              ->first();
						array_push($selectedImages, ['id' => $data['id'], 'code' => $data['code'], 'url' => $data['image_url']]);
						break;
					case config('evibe.ticket_type.decors'):
						$data = SelectedDeliveryImages::join('decor', 'selected_delivery_images.map_id', '=', 'decor.id')
						                              ->select('decor.code', 'selected_delivery_images.image_url', 'selected_delivery_images.id')
						                              ->where('selected_delivery_images.id', $mapTypeId->id)
						                              ->first();
						array_push($selectedImages, ['id' => $data['id'], 'code' => $data['code'], 'url' => $data['image_url']]);
						break;
					default:

				}

			}
		}

		return $selectedImages;
	}

	public function getPlannerData($imageStatus, $city, $category, $event)
	{
		$plannerData = DeliveryGallery::join('ticket_bookings', 'delivery_gallery.ticket_booking_id', '=', 'ticket_bookings.id')
		                              ->join('planner', 'ticket_bookings.map_id', '=', 'planner.id')
		                              ->join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                              ->join('ticket_mapping', 'ticket_bookings.ticket_mapping_id', '=', 'ticket_mapping.id')
		                              ->where('ticket_bookings.map_type_id', config('evibe.ticket_type.planners'))
		                              ->where('delivery_gallery.image_status', $imageStatus)
		                              ->whereNull('delivery_gallery.deleted_at')
		                              ->select('planner.code as planner_code', 'planner.name as planner_name', 'planner.id as planner_id')
		                              ->distinct();
		if ($city)
		{
			$plannerData = $plannerData->where('ticket.city_id', '=', $city);
		}
		if ($category)
		{
			$plannerData = $plannerData->where('ticket_mapping.map_type_id', '=', $category);
		}
		if ($event)
		{
			$plannerData = $plannerData->where('ticket.event_id', '=', $event);
		}

		$plannerData = $plannerData->get();

		return $plannerData;
	}

	public function getImageData($city, $mapTypeId, $occasion, $typeImages, $planner, $priceRange)
	{

		$imageData = DeliveryGallery::join('ticket_bookings', 'delivery_gallery.ticket_booking_id', '=', 'ticket_bookings.id')
		                            ->join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                            ->join('ticket_mapping', 'ticket_bookings.ticket_mapping_id', '=', 'ticket_mapping.id')
		                            ->join('planner', 'ticket_bookings.map_id', '=', 'planner.id')
		                            ->whereNull('delivery_gallery.deleted_at')
		                            ->select('delivery_gallery.id', 'delivery_gallery.url', 'delivery_gallery.image_status', 'delivery_gallery.title', 'delivery_gallery.ticket_booking_id', 'ticket_bookings.ticket_id', 'ticket_mapping.map_type_id', 'ticket_mapping.map_id', 'planner.code as planner_code', 'planner.name as planner_name', 'planner.id as planner_id');

		if ($typeImages)
		{
			$imageData = $imageData->where('delivery_gallery.image_status', $typeImages);
		}
		if ($city)
		{
			$imageData = $imageData->where('ticket.city_id', '=', $city);
		}
		if ($mapTypeId)
		{
			$imageData = $imageData->where('ticket_mapping.map_type_id', '=', $mapTypeId);
		}
		if ($occasion)
		{
			$imageData = $imageData->where('ticket.event_id', '=', $occasion);
		}
		if ($planner)
		{
			$imageData = $imageData->where('ticket_bookings.map_id', $planner);
		}
		if ($priceRange)
		{
			$minPrice = config('evibe.price_filter.' . $priceRange . '.min');
			$maxPrice = config('evibe.price_filter.' . $priceRange . '.max');
			if ($maxPrice == 0)
			{
				$imageData = $imageData->where('ticket_bookings.booking_amount', '>', $minPrice);
			}
			else
			{
				$imageData = $imageData->whereBetween('ticket_bookings.booking_amount', [$minPrice - 1, $maxPrice + 1]);
			}
		}

		$imageData = $imageData->paginate(100);

		return $imageData;
	}

	public function rejectImage()
	{
		$imageId = request('imageId');
		$data = DeliveryGallery::where('id', $imageId)
		                       ->whereNull('deleted_at');
		if ($data)
		{
			$data = $data->update(['image_status' => 2]);
			if ($data)
			{
				return response()->json([
					                        'success' => true,
				                        ]);
			}
			else
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to process the request, Please try again.',
				                        ]);
			}
		}
		else
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Image is already ',
			                        ]);
		}
	}

	public function acceptImage()
	{
		$mapId = request('mapId');
		$mapTypeId = request('mapTypeId');
		$subCategory = request('subCategory');
		$mapId = $mapId == 0 ? null : $mapId;
		$mapTypeId = $mapTypeId == 0 ? null : $mapTypeId;
		$imageId = request('imageId');
		$bookingId = request('bookingId');
		$ticketId = request('ticketId');
		$imageTitle = request('imageTitle');
		$angle = request('imageRotate');
		$option = $angle == 0 ? ['isDeliveryImage' => 1] : ['imageAngle' => $angle, 'isDeliveryImage' => 1];
		$rules = [
			'mapTypeId'   => 'required',
			'subCategory' => 'required'
		];
		$messages = [
			'mapTypeId.required' => 'Please Select the Category',
			'subCategory'        => 'Please Select the Id of particular category'
		];
		$validator = validator(['mapTypeId' => $mapTypeId, 'subCategory' => $subCategory], $rules, $messages);
		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}
		$data = DeliveryGallery::where('id', $imageId)
		                       ->select('url')
		                       ->whereNull('deleted_at');
		if ($data)
		{
			$data->update(['image_status' => 1, 'updated_at' => Carbon::now()]);
			$url = $data->get()->first()['url'];
			$fullUrl = config('evibe.gallery.host') . "/ticket/" . $ticketId . "/" . $bookingId . "/partner/" . $url;
			$directPath = '/live-partner-images/';
			$newUrl = $this->uploadImageToServer($fullUrl, $directPath, $option);
			$bookingPrice = TicketBooking::where('id', $bookingId)
			                              ->select('booking_amount')
			                              ->first();
			SelectedDeliveryImages::insert(['image_url' => $newUrl, 'booking_id' => $bookingId, 'is_profile' => 0, 'handler_id' => Auth::user()->id, 'map_id' => $mapId, 'map_type_id' => $mapTypeId, 'image_title' => $imageTitle, 'updated_at' => Carbon::now(), 'image_id' => $imageId, 'booking_price' => $bookingPrice['booking_amount'], 'sub_category' => $subCategory]);
			if ($data)
			{
				$response = (['success' => true]);

				return response()->json($response);
			}
			else
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to process the request, Please try again.',
				                        ]);
			}
		}
		else
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Image is already ',
			                        ]);
		}

	}

	public function setAsProfile()
	{
		$mapTypeId = request('mapTypeId');
		$mapId = request('mapId');
		$mapId = $mapId == 0 ? null : $mapId;
		$mapTypeId = $mapTypeId == 0 ? null : $mapTypeId;
		$angle = request('imageRotate');
		$url = request('imageUrl');
		$title = request('imageTitle');
		$imageId = request('imageId');
		$ticketId = request('ticketId');
		$bookingId = request('bookingId');
		$bookingPrice = TicketBooking::where('id', $bookingId)->select('booking_amount')->first();
		$fullUrl = config('evibe.gallery.host') . "/ticket/" . $ticketId . "/" . $bookingId . "/partner/" . $url;
		$option = $angle == 0 ? ['isDeliveryImage' => 1] : ['imageAngle' => $angle, 'isDeliveryImage' => 1];
		$directPath = '/live-partner-images/';
		$rules = [
			'mapId'     => 'required',
			'mapTypeId' => 'required'
		];
		$messages = [
			'mapType.required' => 'Please Select the category',
			'mapId.required'   => 'Please Select the Id of particular category'
		];
		$validator = validator(['mapId' => $mapId, 'mapTypeId' => $mapTypeId], $rules, $messages);
		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}
		switch ($mapTypeId)
		{

			case config('evibe.ticket_type.artists'):
			case config('evibe.ticket_type.packages'):

				$plannerId = VendorPackage::select('map_id')
				                           ->where('id', '=', $mapId)
				                           ->first();
				$plannerId = $plannerId['map_id'];
				$newPath = "/planner/" . $plannerId . "/images/packages/" . $mapId . "/";
				$packageUrl = $this->uploadImageToServer($fullUrl, $newPath, $option);
				PackageGallery::insert([
					                        'title'              => $title,
					                        'url'                => $packageUrl,
					                        'planner_package_id' => $mapId,
					                        'type'               => 0,
					                        'created_at'         => Carbon::now(),
					                        'updated_at'         => Carbon::now()
				                        ]
				);

				break;
			case config('evibe.ticket_type.venues'):
				$venueId = VendorPackage::select('map_id')
				                         ->where('id', '=', $mapId)
				                         ->first();
				$venueId = $venueId['map_id'];
				$newPath = "/venues/" . $venueId . "/images/packages/" . $mapId . "/";
				$venueUrl = $this->uploadImageToServer($fullUrl, $newPath, $option);
				VenueGallery::insert([
					                      'title'      => $title,
					                      'url'        => $venueUrl,
					                      'venue_id'   => $mapId,
					                      'type_id'    => 1,
					                      'created_at' => Carbon::now(),
					                      'updated_at' => Carbon::now()
				                      ]);

				break;

			case config('evibe.ticket_type.services'):
				$newPath = "/services/" . $mapId . "/images/";
				$serviceUrl = $this->uploadImageToServer($fullUrl, $newPath, $option);
				ServiceGallery::insert([
					                       'title'           => $title,
					                       'url'             => $serviceUrl,
					                       'type_service_id' => $mapId,
					                       'type_id'         => 0,
					                       'created_at'      => Carbon::now(),
					                       'updated_at'      => Carbon::now()
				                       ]);

				break;
			case config('evibe.ticket_type.trends'):
				$newPath = "/cakes/" . $mapId . "/images/";
				$trendUrl = $this->uploadImageToServer($fullUrl, $newPath, $option);
				TrendGallery::insert([
					                      'title'       => $title,
					                      'url'         => $trendUrl,
					                      'trending_id' => $mapId,
					                      'type'        => 0,
					                      'created_at'  => Carbon::now(),
					                      'updated_at'  => Carbon::now()
				                      ]);

				break;
			case config('evibe.ticket_type.cakes'):
				$newPath = "/trends/" . $mapId . "/images/";
				$cakeUrl = $this->uploadImageToServer($fullUrl, $newPath, $option);
				CakeGallery::insert([
					                     'title'      => $title,
					                     'url'        => $cakeUrl,
					                     'cake_id'    => $mapId,
					                     'type'       => 0,
					                     'created_at' => Carbon::now(),
					                     'updated_at' => Carbon::now()
				                     ]);

				break;
			case config('evibe.ticket_type.decors'):
				$newPath = "/decors/" . $mapId . "/images/";
				$decorUrl = $this->uploadImageToServer($fullUrl, $newPath, $option);
				$code = self::generateDecorGalleryCode();
				DecorGallery::insert([
					                      'title'      => $title,
					                      'url'        => $decorUrl,
					                      'decor_id'   => $mapId,
					                      'type_id'    => 0,
					                      'code'       => $code,
					                      'created_at' => Carbon::now(),
					                      'updated_at' => Carbon::now()
				                      ]);

				break;
			default:
				return response()->json([
					                        'success' => false,
					                        'error'   => 'This type of mapping is not handled'
				                        ]);
		}
		//saving images in select images table also
		$imageName = $this->uploadImageToServer($fullUrl, $directPath, $option);
		SelectedDeliveryImages::insert(['image_url' => $imageName, 'booking_id' => $bookingId, 'is_profile' => 1, 'handler_id' => Auth::user()->id, 'map_id' => $mapId, 'map_type_id' => $mapTypeId, 'image_title' => $title, 'updated_at' => Carbon::now(), 'image_id' => $imageId, 'booking_price' => $bookingPrice['booking_amount']]);

		DeliveryGallery::where('id', $imageId)
		               ->whereNull('deleted_at')
		               ->update(['image_status' => 3, 'updated_at' => Carbon::now()]);

		return response()->json([
			                        'success' => true
		                        ]);
	}

	public function populateSubCategory($mapTypeId)
	{
		$tags = "";

		switch ($mapTypeId)
		{
			case config('evibe.ticket_type.decors'):
				$tags = Tags::where("map_type_id", config("evibe.ticket_type.decors"))->WhereNotNull("parent_id")->pluck("identifier", "id");
				break;
			case config('evibe.ticket_type.cakes'):
				$tags = Tags::where("map_type_id", config("evibe.ticket_type.cakes"))->WhereNotNull("parent_id")->pluck("identifier", "id");
				break;
			default:
		}

		return $tags;
	}

	public function populateMapId()
	{
		$mapTypeId = request('mapTypeId');
		$subCategory = $this->populateSubCategory($mapTypeId);
		$data = "";
		if ($mapTypeId)
		{
			switch ($mapTypeId)
			{
				case  config('evibe.ticket_type.tents'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->where('type_ticket_id', config('evibe.ticket_type.tents'))->get();
					break;
				case  config('evibe.ticket_type.priests'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->where('type_ticket_id', config('evibe.ticket_type.priests'))->get();
					break;
				case  config('evibe.ticket_type.couple-experiences'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->where('type_ticket_id', config('evibe.ticket_type.couple-experiences'))->get();
					break;
				case  config('evibe.ticket_type.experience'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->where('type_ticket_id', config('evibe.ticket_type.experience'))->get();
					break;
				case  config('evibe.ticket_type.food'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->where('type_ticket_id', config('evibe.ticket_type.food'))->get();
					break;
				case  config('evibe.ticket_type.villa'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->where('type_ticket_id', config('evibe.ticket_type.villa'))->get();
					break;
				case  config('evibe.ticket_type.lounge'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->where('type_ticket_id', config('evibe.ticket_type.lounge'))->get();
					break;
				case  config('evibe.ticket_type.resort'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->where('type_ticket_id', config('evibe.ticket_type.resort'))->get();
					break;
				case config('evibe.ticket_type.packages'):
					$data = VendorPackage::select('id', 'code', 'name', 'price')->get();
					break;
				case config('evibe.ticket_type.planners'):
					$data = Planner::select('id', 'code', 'name', 'price')->get();
					break;
				case config('evibe.ticket_type.venues'):
					$data = Venue::select('id', 'code', 'name', 'min_veg_worth as price')->get();
					break;
				case config('evibe.ticket_type.services'):
					$data = TypeServices::select('id', 'code', 'name', 'min_price as price')->get();
					break;
				case config('evibe.ticket_type.trends'):
					$data = Trend::select('id', 'code', 'name', 'price')->get();
					break;
				case config('evibe.ticket_type.cakes'):
					$data = Cake::select('id', 'code', 'title as name', 'price')->get();
					break;
				case config('evibe.ticket_type.decors'):
					$data = Decor::select('id', 'code', 'name', 'min_price as price')->get();
					break;
				default:

			}

			return response()->json([
				                        'success'     => 'true',
				                        'data'        => $data,
				                        'subCategory' => $subCategory
			                        ]);
		}
		else
		{
			return response()->json(([

				'success' => 'false',
				'error'   => 'Please Refresh the page'
			]));
		}
	}

	public function rejectSelectedImage()
	{
		$imageId = request('imageId');
		$selectImageId = SelectedDeliveryImages::select('image_id')
		                                       ->where('id', $imageId)
		                                       ->first();
		$selectedDeliveryImages = SelectedDeliveryImages::where('id', $imageId)
		                                                ->update(['deleted_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

		$deliveryGallery = DeliveryGallery::where('id', $selectImageId['image_id'])
		                                  ->update(['image_status' => 2]);
		if ($selectedDeliveryImages && $deliveryGallery)
		{
			return response()->json([
				                        'success' => 'true'
			                        ]);
		}

		return response()->json([
			                        'success' => 'false'
		                        ]);
	}
}
