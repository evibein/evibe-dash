<?php namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Support\Facades\Auth;

class MetricsDisplayController extends BaseController
{
	public function showMetricsData()
	{
		$user = Auth::user();
		if ($user->id == config("evibe.default_handler.metrics_data_user_id"))
		{
			$data = [];

			return view('dashboard.metrics.home', ['data' => $data]);
		}
		else
		{
			return redirect()->back();
		}
	}

	public function showMetricsNewTicketsData()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/metrics/new-tickets",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard.metrics.new-ticket', ['data' => $data]);
	}

	public function showMetricsFollowupsData()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/metrics/followups",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return view('dashboard.metrics.followups', ['data' => $data]);
	}

	public function showMetricsInProgressData()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "crm/dashboard/tickets/metrics/in-progress",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);
		return view('dashboard.metrics.new-ticket', ['data' => $data]);
	}
}