<?php namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\TypeEvent;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Ticket;
use TicketUpdate;
use TypeTicketStatus;

class SalesDashBoardController extends BaseController
{
	public function showTicketList()
	{
		$ticketStatus = TypeTicketStatus::select("id", "name")->get();
		$occasionNames = TypeEvent::all()->pluck("name", "id");
		$users = User::whereIn('role_id', [config("evibe.roles.customer_delight"), config("evibe.roles.sr_crm"), config("evibe.roles.admin"), config("evibe.roles.super_admin")])
		             ->whereNull('deleted_at')
		             ->pluck("name", "id");

		$searchTicket = "";
		$ticketUpdates = "";
		$phone = request("query");
		$email = request("email");
		$ticketId = request("ticketId");

		if (($phone && is_numeric($phone)) || ($email && !is_null($email)))
		{
			// Sort results
			$sortType = 'DESC';
			$sortOrder = 'ticket.updated_at';

			$searchTicket = Ticket::where(function ($query1) use ($phone, $email) {
				if ($phone && is_numeric($phone))
				{
					$query1->where("phone", $phone)
					       ->orWhere("alt_phone", $phone);
				}
				elseif ($email && !is_null($email))
				{
					$query1->where("email", $email);
				}
				elseif (($phone && is_numeric($phone)) && ($email && !is_null($email)))
				{
					$query1->where("phone", $phone)
					       ->orWhere("alt_phone", $phone)
					       ->orWhere("email", $email);
				}
			})->orderBy($sortOrder, $sortType)
			                      ->get();

			if ($searchTicket->count() > 0)
			{
				$ticketUpdates = TicketUpdate::select("ticket_id", "created_at", "comments")
				                             ->whereIn("ticket_id", $searchTicket->pluck("id")->toArray())
				                             ->get()
				                             ->sortByDesc("created_at")
				                             ->unique("ticket_id")
				                             ->keyBy("ticket_id")
				                             ->toArray();
			}
		}
		else
		{
			$searchTicket = Ticket::where("id", $ticketId)
			                      ->get();

			if ($searchTicket->count() > 0)
			{
				$ticketUpdates = TicketUpdate::select("ticket_id", "created_at", "comments")
				                             ->where("ticket_id", $searchTicket->pluck("id")->toArray())
				                             ->get()
				                             ->sortByDesc("created_at")
				                             ->unique("ticket_id")
				                             ->keyBy("ticket_id")
				                             ->toArray();
			}
		}

		$defaultStartTime = Carbon::yesterday()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);
		$defaultEndTime = Carbon::today()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);

		$returnBookingsCount = Ticket::select("id", "handler_id")
		                             ->whereBetween('paid_at', [strtotime($defaultStartTime), strtotime($defaultEndTime)])
		                             ->where('created_at', "<", $defaultStartTime)
		                             ->where('status_id', config('evibe.ticket.status.booked'))
		                             ->get();

		$data = [
			'ticketStatus'                      => $ticketStatus,
			'searchTicket'                      => $searchTicket,
			'ticketUpdates'                     => $ticketUpdates,
			'occasionNames'                     => $occasionNames,
			'handlerDetails'                    => $users,
			'returnTicketBookingCount'          => $returnBookingsCount->count(),
			'returnTicketBookingCountByHandler' => $returnBookingsCount->where("handler_id", Auth::user()->id)->count()
		];

		if (request()->is('*/search/*'))
		{
			return view("utilities.sales.search-data", ["data" => $data]);
		}
		else
		{
			return view("utilities.sales.dashboard", ["data" => $data]);
		}
	}

	public function getTicketData()
	{
		$inputData = request()->all();
		if (request("dfilter") == "prospect")
		{
			$url = config("evibe.api.base_url") . "crm/dashboard/tickets/prospect";
		}
		elseif (request("dfilter") == "followup")
		{
			$url = config("evibe.api.base_url") . "sales/dashboard/tickets/followup";
		}
		elseif (request("dfilter") == "missed")
		{
			$url = config("evibe.api.base_url") . "crm/dashboard/tickets/missed";
		}
		elseif (request("dfilter") == "return")
		{
			$url = config("evibe.api.base_url") . "crm/dashboard/tickets/prospect";
		}
		elseif (request("dfilter") == "callLogs")
		{
			$url = config("evibe.api.base_url") . "crm/dashboard/tickets/new";
			$inputData["enquirySource"] = 1;
		}
		elseif (request("dfilter") == "scheduled")
		{
			$url = config("evibe.api.base_url") . "crm/dashboard/tickets/new";
			$inputData["enquirySource"] = "scheduled";
		}
		elseif (request("dfilter") == "progress")
		{
			$url = config("evibe.api.base_url") . "crm/dashboard/tickets/progress";
		}
		else
		{
			$url = config("evibe.api.base_url") . "sales/dashboard/tickets/new";
		}

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => $url,
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => $inputData
		];

		$data = $this->makeGuzzleRequest($options);
		$data["dfilter"] = request("dfilter");

		return view("utilities.sales.data", ["data" => $data]);
	}

	public function showAdminMetricsData()
	{
		$inputData = request()->all();
		$url = config("evibe.api.base_url") . "sales/dashboard/tickets/admin-data";

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => $url,
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => $inputData
		];

		$data = $this->makeGuzzleRequest($options);

		return response()->json($data);
	}
}