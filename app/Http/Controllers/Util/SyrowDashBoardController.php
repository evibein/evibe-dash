<?php namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\Ticket\MailSyrowCommentsUpdateRequestToTeamJob;
use App\Jobs\Emails\Ticket\MailSyrowInvalidRequestToTeamJob;
use App\Jobs\Emails\Ticket\MailSyrowTicketUpdateRequestToTeamJob;
use App\Models\Types\TypeEnquirySource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Ticket;
use TicketUpdate;
use TypeTicketStatus;

class SyrowDashBoardController extends BaseController
{
	public function showTicketList()
	{
		$ticketStatus = TypeTicketStatus::select("id", "name")->get();
		$searchTicket = "";
		$ticketUpdates = "";
		$phone = request("query");
		if ($phone && $phone > 1111111111)
		{
			$searchTicket = Ticket::where(function ($query1) use ($phone) {
				$query1->where("phone", $phone)
				       ->orWhere("alt_phone", $phone);
			})
			                       ->orderBy("status_id")
			                       ->get();

			if ($searchTicket->count() > 0)
			{
				$ticketUpdates = TicketUpdate::select("ticket_id", "created_at", "comments")
				                              ->whereIn("ticket_id", $searchTicket->pluck("id")->toArray())
				                              ->get()
				                              ->sortByDesc("created_at")
				                              ->unique("ticket_id")
				                              ->keyBy("ticket_id")
				                              ->toArray();
			}
		}

		$data = [
			'ticketStatus'  => $ticketStatus,
			'searchTicket'  => $searchTicket,
			'ticketUpdates' => $ticketUpdates
		];

		return view("utilities.syrow.dashboard", ["data" => $data]);
	}

	public function getTicketData()
	{
		if (request("dfilter") == "today")
		{
			$threeDaysBeforeTimestamp = Carbon::now()->startOfDay();
			$endDate = Carbon::now();
		}
		elseif (request("dfilter") == "yesterday")
		{
			$threeDaysBeforeTimestamp = Carbon::now()->startOfDay()->subDays(1);
			$endDate = Carbon::now()->startOfDay();
		}
		elseif (request("dfilter") == "dayBefore")
		{
			$threeDaysBeforeTimestamp = Carbon::now()->startOfDay()->subDays(2);
			$endDate = Carbon::now()->startOfDay()->subDays(1);
		}
		else
		{
			$threeDaysBeforeTimestamp = Carbon::now()->startOfDay()->subDays(3);
			$endDate = Carbon::now();
		}

		$tickets = Ticket::whereBetween("created_at", [$threeDaysBeforeTimestamp, $endDate])
		                  ->where("enquiry_source_id", config("evibe.ticket_enquiry_source.phone"))
		                  ->whereNull("deleted_at")
		                  ->get();

		$validHandlerIds = User::where("role_id", config("evibe.roles.syrow"))
		                       ->pluck("id")
		                       ->toArray();

		$ticketUpdates = TicketUpdate::whereIn("ticket_id", $tickets->pluck("id")->toArray())
		                              ->whereIn("handler_id", $validHandlerIds)
		                              ->orderBy("created_at")
		                              ->get();

		$ticketStatus = TypeTicketStatus::select("id", "name")->get();

		$data = [
			'tickets'           => $tickets,
			'ticketStatus'      => $ticketStatus,
			'ticketUpdates'     => $ticketUpdates,
			"typeEnquirySource" => TypeEnquirySource::all()->pluck("name", "id")
		];

		return view("utilities.syrow.data", ["data" => $data]);
	}

	public function saveInvalidTicket()
	{
		$rules = [
			'comments' => 'required',
			'phone'    => 'required|phone|digits:10'
		];

		$messages = [
			'phone.required'    => 'Please enter customer phone number (10 digits)',
			'phone.phone'       => 'Phone number is not valid',
			'comments.required' => 'Please enter comments'
		];

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			return response()->json([
				                        "success" => false,
				                        "error"   => $validation->messages()->first()
			                        ]);
		}

		$comments = request("comments");
		$phone = request("phone");
		$handlerId = Auth::user()->id;

		$ticket = Ticket::updateOrCreate([
			                                  "id" => request("ticketId")
		                                  ],
		                                  [
			                                  "comments"   => $comments,
			                                  "phone"      => $phone,
			                                  "handler_id" => $handlerId,
			                                  "status_id"  => config("evibe.status.irrelevant"),
			                                  "source_id"  => config("evibe.ticket_enquiry_source.phone")
		                                  ]);

		if ($ticket)
		{
			$updateData = [
				'ticket_id'   => $ticket->id,
				'status_id'   => config("evibe.status.irrelevant"),
				'handler_id'  => $handlerId,
				'comments'    => $comments,
				'status_at'   => time(),
				'type_update' => config('evibe.ticket_type_update.manual')
			];
			TicketUpdate::create($updateData);

			$emailData = [
				"ticketId" => $ticket->id,
				"comments" => $comments
			];

			$this->dispatch(new MailSyrowInvalidRequestToTeamJob($emailData));

			return response()->json([
				                        "success" => true
			                        ]);
		}

		return response()->json([
			                        "success" => false
		                        ]);
	}

	public function updateTicketComments($ticketId)
	{
		$rules = [
			'tuComments' => 'required'
		];

		$messages = [
			'tuComments.required' => 'Please enter comments'
		];

		$validation = Validator::make(request()->all(), $rules, $messages);

		if ($validation->fails())
		{
			return redirect()->back()->with("error", $validation->messages()->first());
		}

		$comments = request("tuComments");
		$ticket = Ticket::find($ticketId);

		if ($ticket)
		{
			if (in_array($ticket->status_id, [config("evibe.status.irrelevant"), config("evibe.status.enquiry")]))
			{
				$ticket->update([
					                "status_id" => config("evibe.status.followup")
				                ]);
			}

			$updateData = [
				'ticket_id'   => $ticket->id,
				'status_id'   => $ticket->status_id,
				'handler_id'  => Auth::user()->id,
				'comments'    => $comments,
				'status_at'   => time(),
				'type_update' => config('evibe.ticket_type_update.manual')
			];
			TicketUpdate::create($updateData);

			$emailData = [
				"ticketId" => $ticketId,
				"comments" => $comments,
				"to"       => $ticket->status_id == config("evibe.status.booked") ? config("evibe.enquiry_group_email") : config("evibe.enquiry_group_email")
			];

			$this->dispatch(new MailSyrowCommentsUpdateRequestToTeamJob($emailData));

			return redirect()->back()->with('successMsg', "Comments Added Successfully.");
		}

		return redirect()->back()->with("error", "Error occurred while submitting your request, Please try again.");
	}

	public function updateTicketNotFoundComments($phone)
	{
		$rules = [
			'ticketNotFoundComments' => 'required'
		];

		$messages = [
			'ticketNotFoundComments.required' => 'Please enter comments'
		];

		$validation = Validator::make(request()->all(), $rules, $messages);

		if ($validation->fails())
		{
			return redirect()->back()->with("error", $validation->messages()->first());
		}

		$comments = request("ticketNotFoundComments");

		$emailData = [
			"comments" => $comments,
			"phone"    => $phone,
			"to"       => config("evibe.enquiry_group_email")
		];

		$this->dispatch(new MailSyrowCommentsUpdateRequestToTeamJob($emailData));

		return redirect()->back()->with('successMsg', "Comments Added Successfully.");
	}

	public function saveRequest($ticketId)
	{
		$user = Auth::user();
		$ticket = Ticket::find($ticketId);

		if ($user && $ticket)
		{
			$requestType = request("requestType");
			$requestReason = request("requestReason");

			$updateData = [
				'ticket_id'   => $ticket->id,
				'status_id'   => $ticket->status_id,
				'handler_id'  => $user->id,
				'comments'    => "[" . request("requestType") . "], " . request("requestReason"),
				'status_at'   => time(),
				'type_update' => config('evibe.ticket_type_update.manual')
			];
			TicketUpdate::create($updateData);

			$emailData = [
				"ticketId"      => $ticketId,
				"requestType"   => $requestType,
				"requestReason" => $requestReason,
			];

			$this->dispatch(new MailSyrowTicketUpdateRequestToTeamJob($emailData));

			return response()->json([
				                        "success" => true,
			                        ]);
		}

		return response()->json([
			                        "success" => false,
			                        "error"   => "Error while submitting your request, Please try again."
		                        ]);
	}
}
