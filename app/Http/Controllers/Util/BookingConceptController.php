<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\Review\PartnerReviewAnswer;
use App\Models\TypeEvent;
use App\Models\Types\TypeBookingConcept;
use Carbon\Carbon;
use City;
use Review;
use TicketBooking;
use TypeTicketSource;

class BookingConceptController extends BaseController
{
	public function showBookings()
	{
		$cities = City::all();
		$bookingSources = TypeTicketSource::all();
		$bookingConceptTypes = TypeBookingConcept::all();
		$typeEvents = TypeEvent::all();
		$isShowReset = false;
		$bookings = TicketBooking::select('ticket_bookings.*')
		                          ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
		                          ->where('ticket.status_id', config('evibe.status.booked'))
		                          ->where('ticket_bookings.is_advance_paid', 1)
		                          ->whereNull('ticket_bookings.cancelled_at')
		                          ->whereNull('ticket.deleted_at');

		$searchQuery = request()->has('query') ? request()->input('query') : false;

		//search for name and Id
		if ($searchQuery)
		{
			$bookings->where(function ($innerQuery) use ($searchQuery)
			{
				$innerQuery->where('ticket.name', 'LIKE', '%' . $searchQuery . '%')
				           ->orWhere('ticket.phone', 'LIKE', '%' . $searchQuery . '%')
				           ->orWhere('ticket.enquiry_id', 'LIKE', '%' . $searchQuery . '%');
			});
			$isShowReset = true;
		}

		//Filter By City
		$cityId = request()->input('city', 'all');
		if ($cityId != 'all' && ($cityId = (int)$cityId) && $cityId > 0)
		{
			$allCityIds = $cities->pluck('id')->toArray();
			if (in_array($cityId, $allCityIds))
			{
				$isShowReset = true;
				$bookings->where('ticket.city_id', $cityId);
			}
		}

		//Filter By Source
		$sourceId = request()->input('source', 'all');
		if ($sourceId != 'all' && ($sourceId = (int)$sourceId) && $sourceId > 0)
		{
			$allSourceIds = $bookingSources->pluck('id')->toArray();
			if (in_array($sourceId, $allSourceIds))
			{
				$isShowReset = true;
				$bookings->where('ticket.source_id', $sourceId);
			}
		}

		//Filter By Occasion
		$eventId = request()->input('eventType', 'all');
		if ($eventId != 'all' && ($eventId = (int)$eventId) && $eventId > 0)
		{
			$allEventIds = $typeEvents->pluck('id')->toArray();
			if (in_array($eventId, $allEventIds))
			{
				$isShowReset = true;
				$bookings->where('ticket.event_id', $eventId);
			}
		}

		//Filter By Concept Type
		$conceptTypeId = request()->input('conceptType', 'all');
		if ($conceptTypeId != 'all' && ($conceptTypeId = (int)$conceptTypeId) && $conceptTypeId > 0)
		{
			$allConceptTypeIds = $bookingConceptTypes->pluck('id')->toArray();
			if (in_array($conceptTypeId, $allConceptTypeIds))
			{
				$isShowReset = true;
				$bookings->where('type_booking_concept_id', $conceptTypeId);
			}
		}

		//Filter By Date of booking
		$fromDate = request()->input('fromDate');
		$toDate = request()->input('toDate');
		if ($fromDate || $toDate)
		{
			$startDay = Carbon::createFromFormat('m/d/Y H:i:s', '02/10/2014 00:00:00')->timestamp;
			$today = Carbon::now()->endOfDay()->timestamp;
			$fromDate = !is_null($fromDate) ? Carbon::createFromFormat('m/d/Y H:i:s', $fromDate . '00:00:00')->timestamp : $startDay;
			$toDate = !is_null($toDate) ? Carbon::createFromFormat('m/d/Y H:i:s', $toDate . '23:59:59')->timestamp : $today;
			if ($fromDate < $startDay)
			{
				$fromDate = $startDay;
			}
			elseif ($toDate > $today)
			{
				$toDate = $today;
			}

			$isShowReset = true;
			$bookings->whereBetween('ticket.event_date', [$fromDate, $toDate]);
		}

		$data = [
			'bookings'     => $bookings->orderBy('ticket_bookings.party_date_time', 'DESC')->paginate(30),
			'cities'       => $cities,
			'sources'      => $bookingSources,
			'ConceptTypes' => $bookingConceptTypes,
			'events'       => $typeEvents,
			'isShowReset'  => $isShowReset
		];

		return view('booking-concept/list', ['data' => $data]);
	}

	public function showBookingInfo($bookingId)
	{
		$booking = TicketBooking::find($bookingId);
		if (!$booking)
		{
			return redirect(route('booking.concept'));
		}

		$review = Review::where('is_accepted', 1)->get();
		$reviewAnswer = PartnerReviewAnswer::all();
		$data = [
			'booking'      => $booking,
			'reviews'      => $review,
			'reviewAnswer' => $reviewAnswer
		];

		return view('booking-concept/info')->with($data);
	}
}
