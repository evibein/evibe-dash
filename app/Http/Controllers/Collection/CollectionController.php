<?php

namespace App\Http\Controllers\Collection;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\saveCollectionPriority;
use App\Models\Collection\Collection;
use App\Models\TypeEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\File;

class CollectionController extends BaseController
{
	public function showCollections(Request $request)
	{
		$searchQuery = $request->has('query') ? $request->input('query') : false;
		$collection = Collection::orderBy('updated_at', 'DESC');
		$typeEvents = TypeEvent::all();
		$cities = \City::all();

		$city = $request->has('city') ? $request->input('city') : \City::first()->id;
		if ($city)
		{
			$collection->where('city_id', $city);
		}

		if ($searchQuery)
		{
			$collection->where('name', 'LIKE', '%' . $searchQuery . '%')
			           ->orWhere('description', 'LIKE', '%' . $searchQuery . '%');

			if (is_int($searchQuery))
			{
				$collection->orWhere('min_price', $searchQuery)
				           ->orWhere('max_price', $searchQuery);
			}
		}

		$collections = $collection->paginate(12);

		$data = compact('typeEvents', 'cities', 'collections');

		return view('collection.list', $data);
	}

	public function showCreateCollection()
	{
		$cities = \City::all();
		$events = TypeEvent::all();

		$data = compact('cities', 'events');

		return view('collection.create', $data);
	}

	public function saveCollection(Request $request)
	{
		$this->validate($request, [
			'name'        => 'required|min:10',
			'city'        => 'required',
			'description' => 'required|min:15',
			'seo_desc'    => 'required',
			'seo_title'   => 'required',
			'seo_keyword' => 'required'
		]);

		$name = $request->input('name');
		$collectionData = [
			'name'            => $name,
			'code'            => $this->generateCode(),
			'description'     => $request->input('description'),
			'url'             => $this->generateUrl($name),
			'created_by'      => auth()->user()->id,
			'city_id'         => $request->input('city'),
			'seo_keywords'    => $request->input('seo_keyword'),
			'seo_title'       => $request->input('seo_title'),
			'seo_description' => $request->input('seo_desc')
		];

		$eventId = $request->input('event') && $request->input('event') > 0 ? $request->input('event') : false;
		if ($eventId)
		{
			$collectionData['event_id'] = $eventId;
		}

		$new = Collection::create($collectionData);

		if ($new)
		{
			return redirect()->route('collection.details', $new->id)->with('success', 'New collection added successfully');
		}
		else
		{
			return redirect()->route('collection.list')->with('errorMsg', 'couldn\'t save the collection successfully, please try again');
		}
	}

	public function showEditCollection($collectionId)
	{
		$collectionObj = Collection::findOrFail($collectionId);
		$events = TypeEvent::all();
		$cities = \City::all();

		$data = compact('collectionObj', 'events', 'cities');

		return view('collection.edit', $data);
	}

	public function updateCollection($collectionId, Request $request)
	{
		$collection = Collection::findOrFail($collectionId);

		$this->validate($request, [
			'name'        => 'required|min:10',
			'city'        => 'required',
			'description' => 'required|min:15',
			'seo_desc'    => 'required',
			'seo_title'   => 'required',
			'seo_keyword' => 'required'
		]);

		$name = $request->input('name');
		$eventId = $request->input('event') && $request->input('event') > 0 ? $request->input('event') : null;

		$collectionData = [
			'name'            => $name,
			'description'     => $request->input('description'),
			'url'             => $this->generateUrl($name),
			'city_id'         => $request->input('city'),
			'seo_keywords'    => $request->input('seo_keyword'),
			'seo_title'       => $request->input('seo_title'),
			'seo_description' => $request->input('seo_desc'),
			'event_id'        => $eventId
		];

		$new = $collection->update($collectionData);

		if ($new)
		{
			return redirect()->route('collection.details', $collection->id)->with('success', 'collection updated successfully');
		}
		else
		{
			return redirect()->route('collection.details', $collection->id)->with('error', 'couldn\'t update the collection successfully, please try again');
		}
	}

	public function showCollectionDetails($collectionId)
	{
		$collectionDetails = Collection::findOrFail($collectionId);

		$data = compact('collectionDetails');

		return view('collection.details.info', $data);

	}

	public function makeLive($collectionId)
	{
		$collection = Collection::findOrFail($collectionId);
		$now = Carbon::now()->timestamp;

		$updateData = [
			'is_live'     => 1,
			'approved_at' => $now,
			'approved_by' => auth()->user()->id
		];

		if ($collection->update($updateData))
		{
			return redirect()->back();
		}
	}

	public function makeNonLive($collectionId)
	{
		$collection = Collection::findOrFail($collectionId);

		$updateData = ['is_live' => 0];

		if ($collection->update($updateData))
		{
			return redirect()->back();
		}
	}

	public function deleteCollection($collectionId)
	{
		$collection = Collection::findOrFail($collectionId);
		$coverImage = $collection->cover_image;
		$profileImage = $collection->profile_image;

		if ($collection->delete())
		{
			if ($coverImage)
			{
				$this->deleteImageFile($collectionId, $coverImage);
			}
			if ($profileImage)
			{
				$this->deleteImageFile($collectionId, $profileImage);
			}

			return redirect()->route('collection.list')->with('successMsg', 'Collection deleted successfully');
		}
	}

	public function uploadGallery($collectionId, Request $request)
	{
		$collection = Collection::findOrFail($collectionId);
		$redirectRoute = route('collection.details', $collectionId) . '#gallery';
		$photoType = $request->input('photoType');
		$photo = $request->file('photo');

		// validation
		$imageRules = [
			'photo' => 'required|mimes:png,jpeg,jpg,JPG|max:5120'
		];

		$imageMessages = [
			'photo.required' => 'Please select an image to upload',
			'photo.mimes'    => 'Image is not valid. (only .png .jpeg .jpg are accepted)',
			'photo.max'      => 'Image size cannot be more than 5 MB'
		];

		$imageValidator = validator($request->all(), $imageRules, $imageMessages);

		if ($imageValidator->fails())
		{
			return redirect($redirectRoute)->withErrors($imageValidator)->withInput();
		}

		// upload images
		$uploadPath = "/collection/$collectionId/images/";
		$uploadedImg = $this->uploadImageToServer($photo, $uploadPath, ['isWatermark' => false, 'isOptimize' => true, 'isCollection' => true, 'isProfile' => $photoType]);

		if ($uploadedImg)
		{
			$update = null;
			$type = '';

			if ($photoType == 1)
			{
				$oldUrl = $collection->profile_image;
				$update = $collection->update(['profile_image' => $uploadedImg]);
				$type = 'profile';

				if ($oldUrl)
				{
					$this->deleteImageFile($collectionId, $oldUrl);
				}
			}
			elseif ($photoType == 2)
			{
				$oldUrl = $collection->cover_image;
				$update = $collection->update(['cover_image' => $uploadedImg]);
				$type = 'cover';

				if ($oldUrl)
				{
					$this->deleteImageFile($collectionId, $oldUrl);
				}
			}

			if ($update)
			{
				return redirect($redirectRoute)->with('successMsg', "Collection $type photo uploaded successfully");
			}
		}
	}

	public function deleteGallery($collectionId, Request $request)
	{
		$collection = Collection::findOrFail($collectionId);
		$redirectRoute = route('collection.details', $collectionId) . '#gallery';
		$photoType = $request->has('type') ? $request->input('type') : false;
		$hasUpdated = false;
		$type = '';
		$imageUrl = '';

		if ($photoType == 1)
		{
			$imageUrl = $collection->profile_image;
			$collection->update(['profile_image' => null]);
			$hasUpdated = true;
			$type = 'profile';
		}
		elseif ($photoType == 2)
		{
			$imageUrl = $collection->cover_image;
			$collection->update(['cover_image' => null]);
			$hasUpdated = true;
			$type = 'cover';
		}

		if ($hasUpdated)
		{
			if ($imageUrl)
			{
				$this->deleteImageFile($collectionId, $imageUrl);
			}

			return redirect($redirectRoute)->with('successMsg', "$type photo deleted successfully");
		}
		else
		{
			return redirect($redirectRoute)->with('errorMsg', "$type image couldn't deleted successfully, please try again");
		}
	}

	public function showCollectionPriority(Request $request)
	{
		$collection = Collection::orderBy('priority', 'asc');
		$cities = \City::all();

		$city = $request->has('city') ? $request->input('city') : \City::first()->id;
		if ($city)
		{
			$collection->where('city_id', $city);
		}
		$collections = $collection->get();

		$data = compact('collections', 'cities');

		return view('collection.reorder', $data);

	}

	public function saveCollectionPriority(Request $request)
	{
		$res = ['success' => true];
		$itemIds = $request->input('sortableOrder');
		$cityId = $request->input('cityId');

		try
		{
			dispatch(new saveCollectionPriority($itemIds, $cityId));
		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);

			$res = ['success' => false];
		}

		return response()->json($res);
	}

	private function deleteImageFile($collectionId, $url)
	{
		$path = config('evibe.gallery.root') . '/collection/' . $collectionId . '/images/' . $url;

		try
		{
			File::delete($path);

		} catch (\Exception $e)
		{
			$this->sendErrorReport($e, false);
			// show error message
		}
	}

	private function generateCode()
	{
		$lastId = 0;

		$lastCollection = Collection::withTrashed()->orderBy('id', 'DESC')->first();
		if ($lastCollection)
		{
			$lastId = $lastCollection->id;
		}

		return config('evibe.code.prefix.collection') . str_pad($lastId + 1, 4, '0', STR_PAD_LEFT);
	}
}
