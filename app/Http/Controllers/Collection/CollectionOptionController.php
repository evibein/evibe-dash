<?php

namespace App\Http\Controllers\Collection;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\saveCollectionOptionPriority;
use Evibe\Facades\AppUtilFacade as AppUtil;

use App\Models\Collection\Collection;
use App\Models\CollectionOption;
use App\Models\DecorEvent;
use App\Models\ServiceEvent;
use App\Models\TrendEvents;
use App\Models\TypeServices;
use App\Models\VenueHallEvent;
use Illuminate\Http\Request;

class CollectionOptionController extends BaseController
{
	public function showOptions($collectionId)
	{
		$optionData = [];
		$collectionDetails = Collection::findOrFail($collectionId);
		$mapTypes = \TypeTicket::where('is_collection', 1)->get();
		$options = $collectionDetails->options;

		foreach ($options as $option)
		{
			$optionMapTypeId = $option->map_type_id;

			$mapTypePackages = [
				config('evibe.ticket_type.packages'),
				config('evibe.ticket_type.resort'),
				config('evibe.ticket_type.villa'),
				config('evibe.ticket_type.lounge'),
				config('evibe.ticket_type.food'),
				config('evibe.ticket_type.venue-deals'),
				config('evibe.ticket_type.couple-experiences'),
				config('evibe.ticket_type.priests'),
				config('evibe.ticket_type.tents')
			];

			if (in_array($optionMapTypeId, $mapTypePackages))
			{
				$optionMapTypeId = config('evibe.ticket_type.packages');
			}
			elseif ($optionMapTypeId == config('evibe.ticket_type.entertainments'))
			{
				$optionMapTypeId = config('evibe.ticket_type.services');
			}
			elseif ($optionMapTypeId == config('evibe.ticket_type.venue_halls'))
			{
				$optionMapTypeId = config('evibe.ticket_type.venues');
			}

			$mappingData = AppUtil::fillMappingValues([
				                                          'isCollection' => false,
				                                          'mapId'        => $option->map_id,
				                                          'mapTypeId'    => $optionMapTypeId
			                                          ]);
			$mappingData['optId'] = $option->id;
			$optionData[] = $mappingData;

		}

		$data = compact('collectionDetails', 'mapTypes', 'optionData');

		return view('collection.details.option', $data);
	}

	public function fetchItems($collectionId, Request $request)
	{
		$res = ['success' => false];
		$typeId = $request->input('type');
		$collection = Collection::findOrFail($collectionId);

		try
		{
			$data = $this->getMappingTypeData($typeId, $collection->city_id, $collection->event_id);

			return response()->json(['success' => true, 'mapData' => $data]);
		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);
		}

		return response()->json($res);
	}

	public function saveOption($collectionId, Request $request)
	{
		$res = ['success' => false];
		$collection = Collection::findOrFail($collectionId);
		$mapTypeId = $request->input('mapTypeId');
		$mapId = $request->input('itemId');
		$eventId = $collection->event_id;
		$cityId = $collection->city_id;

		$hasExisting = CollectionOption::where([
			                                       'map_id'        => $mapId,
			                                       'map_type_id'   => $mapTypeId,
			                                       'collection_id' => $collectionId
		                                       ])->count();
		if ($hasExisting)
		{
			return response()->json(['success' => false, 'error' => 'This item has already added for this collection']);
		}

		try
		{
			$new = CollectionOption::create([
				                                'collection_id' => $collectionId,
				                                'map_id'        => $mapId,
				                                'map_type_id'   => $mapTypeId,
				                                'event_id'      => $eventId,
				                                'city_id'       => $cityId
			                                ]);
			if ($new)
			{
				$res['success'] = true;
			}
		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);
		}

		return response()->json($res);
	}

	public function removeOption($collectionId, $optionId)
	{
		$option = CollectionOption::findOrFail($optionId);

		$option->delete();

		return redirect()->back()->with('successMsg', 'Option removed successfully');
	}

	public function showReorder($collectionId)
	{
		$optionData = [];
		$collectionDetails = Collection::findOrFail($collectionId);
		$options = CollectionOption::where('collection_id', $collectionId)->orderBy('priority', 'asc')->get();

		foreach ($options as $option)
		{
			$optionMapTypeId = $option->map_type_id;

			$mapTypePackages = [
				config('evibe.ticket_type.packages'),
				config('evibe.ticket_type.resort'),
				config('evibe.ticket_type.villa'),
				config('evibe.ticket_type.lounge'),
				config('evibe.ticket_type.food'),
				config('evibe.ticket_type.venue-deals')
			];

			if (in_array($optionMapTypeId, $mapTypePackages))
			{
				$optionMapTypeId = config('evibe.ticket_type.packages');
			}
			elseif ($optionMapTypeId == config('evibe.ticket_type.entertainments'))
			{
				$optionMapTypeId = config('evibe.ticket_type.services');
			}

			$mappingData = AppUtil::fillMappingValues([
				                                          'isCollection' => false,
				                                          'mapId'        => $option->map_id,
				                                          'mapTypeId'    => $optionMapTypeId
			                                          ]);
			$mappingData['optId'] = $option->id;
			$optionData[] = $mappingData;

		}

		$data = compact('collectionDetails', 'optionData');

		return view('collection.details.option_reorder', $data);
	}

	public function saveOptionPriority($collectionId, Request $request)
	{
		$res = ['success' => true];
		$sortableOrder = $request->input('sortableOrder');

		try
		{
			dispatch(new saveCollectionOptionPriority($sortableOrder, $collectionId));
		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);
			$res = ['success' => false];
		}

		return response()->json($res);
	}
}
