<?php

namespace App\Http\Controllers\Pinterest;

use App\Http\Controllers\Base\BaseController;
use GuzzleHttp\Client;

class PinterestController extends BaseController
{
	public $client_id;
	public $client_secret;
	public $state;
	public $access_token;
	public $code;
	public $curl;
	const AUTH_HOST = "https://api.pinterest.com/oauth/";

	public function getPinterestCode()
	{
		// Create and set new instance of the OAuth class
		$this->client_id = "5068753536177716828";
		$this->client_secret = "ff575ffa96002dd0f6f7a2f405d177648ada191f19f2d84920716783654f4b26";

		// Generate and set the state
		$this->state = substr(md5(rand()), 0, 7);

		$queryparams = [
			"response_type" => "code",
			"redirect_uri"  => "https://local.evibe-dash.in/",
			"client_id"     => $this->client_id,
			"scope"         => implode(",", ['read_public', 'write_public']),
			"state"         => $this->state
		];

		// Build url and return it
		$url = sprintf("%s?%s", self::AUTH_HOST, http_build_query($queryparams));

		return redirect($url);
	}

	public function callbackFromPinterest()
	{
		$state = request("state");
		$code = request("code");

		// Build data array
		$data = [
			"grant_type"    => "authorization_code",
			"redirect_uri"  => "http://local.evibe-dash.in/pinterest/callback",
			"client_id"     => $this->client_id,
			"client_secret" => $this->client_secret,
			"code"          => $code
		];

		return $this->execute1("POST", sprintf(" % s % s", "https://api.pinterest.com/v1/", "oauth/token"), $data);

		return $response;
	}

	public function getPinterestAccessToken()
	{
		//https://local.evibe-dash.in/?state=4d8ab89&code=10b938025a593c0c
		//https://local.evibe-dash.in/?state=db182d2&code=253a943c9cc2d58d
		//https://local.evibe-dash.in/?state=1be3bc3&code=0ea6347521593b6c
		//https://local.evibe-dash.in/?state=741adf4&code=2377595178d280b9
		$this->state = "741adf4";
		$this->code = "2377595178d280b9";
		$this->client_id = "5068753536177716828";
		$this->client_secret = "ff575ffa96002dd0f6f7a2f405d177648ada191f19f2d84920716783654f4b26";

		// Build data array
		$data = [
			"grant_type"    => "authorization_code",
			"client_id"     => $this->client_id,
			"client_secret" => $this->client_secret,
			"code"          => $this->code
		];

		//return sprintf("%s?%s", "https://api.pinterest.com/v1/oauth/token", http_build_query($data));

		// Perform post request
		//$response = $this->request->post("oauth/token", $data);
		return $this->execute1("POST", sprintf("%s%s", "https://api.pinterest.com/v1/", "oauth/token"), $data);

		return $response;
	}

	public function getProfileInfo()
	{
		//$this->access_token = "AlUVnZI0oU__LCkPq45L_lknG4VKFdxQjnlHKLtGV9R8WeC-XAEhgDAAANFtRlfWGFkAoYYAAAAA";
		$this->access_token = "AhZhv0xOIfzAmPQi-6AwJA2Zr1hRFdxQyAPbHfJGV9R8WeC-XAEhgDAAANFtRlfWGFkAoYYAAAAA";

		//$response = $this->get("me", []);
		//$response = $this->get(sprintf("boards/%s", "jeevan"), []);
		$response = $this->get("me/boards", []);

		return $response;
	}

	public function get($endpoint, array $parameters = [])
	{
		if (!empty($parameters))
		{
			$path = sprintf("%s/?%s", $endpoint, http_build_query($parameters));
		}
		else
		{
			$path = $endpoint;
		}

		return $this->execute1("GET", sprintf("%s%s", "https://api.pinterest.com/v1/", $path));
	}

	public function pinterestOAuth()
	{
		// Create and set new instance of the OAuth class
		$this->client_id = "5068753536177716828";
		$this->client_secret = "ff575ffa96002dd0f6f7a2f405d177648ada191f19f2d84920716783654f4b26";

		// Generate and set the state
		//$this->state = substr(md5(rand()), 0, 7);
		$this->state = "b87546c";

		$queryparams = [
			"response_type" => "code",
			"redirect_uri"  => "https://local.evibe-dash.in/",
			"client_id"     => $this->client_id,
			"scope"         => implode(",", ['read_public', 'write_public']),
			"state"         => $this->state
		];

		// Build url and return it
		$url = sprintf("%s?%s", self::AUTH_HOST, http_build_query($queryparams));

		//dd($url);

		//$client = new Client();
		//$res = $client->request("POST", $url);
		//
		//$res = $res->getBody();
		//
		//$res = \GuzzleHttp\json_decode($res, true);
		//dd($res);
		//
		//dd($res);

		$board = [
			"name"        => "Test from API",
			"description" => "Test"
		];

		return $this->execute("POST", sprintf("%s%s", "https://api.pinterest.com/v1/", "oauth/token"), []);

		//return new Board($this->master, $response);

		// Set request instance
		$data = [
			"grant_type"    => "authorization_code",
			"client_id"     => $this->client_id,
			"client_secret" => $this->client_secret,
			"code"          => "a23e6ab9eecba80e"
		];

		// Perform post request
		// Use guzzle http
		return $this->execute1("POST", sprintf("%s%s", "https://api.pinterest.com/v1/", "oauth/token"), []);
	}

	public function execute1($method, $apiCall, array $parameters = [], $headers = [])
	{
		// Check if the access token needs to be added
		if ($this->access_token != null)
		{
			$headers = array_merge($headers, [
				"Authorization: Bearer " . $this->access_token,
			]);
		}

		// Force cURL to not send Expect header to workaround bug with Akamai CDN not handling
		// this type of requests correctly
		$headers = array_merge($headers, [
			"Expect:",
		]);

		//dd("TEs");
		// Setup CURL
		$ch = curl_init();

		// Set default options
		$ch = $this->setOptions($ch, [
			CURLOPT_URL            => $apiCall,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_CONNECTTIMEOUT => 20,
			CURLOPT_TIMEOUT        => 90,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_HEADER         => false,
			CURLINFO_HEADER_OUT    => true
		]);

		//dd($method);
		switch ($method)
		{
			case 'POST':
				$ch = $this->setOptions($ch, [
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POST          => count($parameters),
					CURLOPT_POSTFIELDS    => $parameters
				]);

				if (!class_exists('\CURLFile') && defined('CURLOPT_SAFE_UPLOAD'))
				{
					$ch->setOption(CURLOPT_SAFE_UPLOAD, false);
				}
				elseif (class_exists('\CURLFile') && defined('CURLOPT_SAFE_UPLOAD'))
				{
					$ch = $this->setOption($ch, CURLOPT_SAFE_UPLOAD, true);
				}

				break;
			case 'DELETE':
				$ch->setOption(CURLOPT_CUSTOMREQUEST, "DELETE");
				break;
			case 'PATCH':
				$ch->setOptions([
					                CURLOPT_CUSTOMREQUEST => "PATCH",
					                CURLOPT_POST          => count($parameters),
					                CURLOPT_POSTFIELDS    => $parameters
				                ]);
				break;
			default:
				$ch = $this->setOption($ch, CURLOPT_CUSTOMREQUEST, "GET");
				break;
		}

		// Execute request and catch response
		//$cUrlResponse = json_decode(curl_exec($ch), true);

		//dd($ch);
		$mr = 5;
		$body = null;

		//dd("in");
		if (ini_get("open_basedir") == "" && ini_get("safe_mode" == "Off"))
		{
			$ch = $this->setOptions($ch, [
				CURLOPT_FOLLOWLOCATION => $mr > 0,
				CURLOPT_MAXREDIRS      => $mr
			]);
		}
		else
		{
			$ch = $this->setOption($ch, CURLOPT_FOLLOWLOCATION, false);

			if ($mr > 0)
			{
				$original_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

				//dd($original_url);
				$newurl = $original_url;

				$ch = $this->setOptions($ch, [
					CURLOPT_HEADER       => true,
					CURLOPT_FORBID_REUSE => false
				]);

				do
				{
					$ch = $this->setOption($ch, CURLOPT_URL, $newurl);

					$response = curl_exec($ch);

					$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
					$header = substr($response, 0, $header_size);
					$body = substr($response, $header_size);

					$number = curl_errno($ch);

					if ($number)
					{
						$code = 0;
					}
					else
					{
						$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

						if ($code == 301 || $code == 302)
						{
							preg_match('/Location:(.*?)\n/i', $header, $matches);
							$newurl = trim(array_pop($matches));
						}
						else
						{
							$code = 0;
						}
					}
				} while ($code && --$mr);

				if (!$mr)
				{
					if ($mr === null)
					{
						trigger_error('Too many redirects.', E_USER_WARNING);
					}

					return false;
				}

				$this->headers = $this->parseHeaders($header);
			}
		}

		if (!$body)
		{
			$ch = $this->setOption($ch, CURLOPT_HEADER, true);
			$response = $this->execute($ch);

			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			dd($header_size);
			$header_size = $this->getInfo(CURLINFO_HEADER_SIZE);
			$header = substr($response, 0, $header_size);
			$body = substr($response, $header_size);

			$this->headers = $this->parseHeaders($header);
		}

		return $body;
		$this->curl = $ch;
		$response_data = $this->execute();

		dd($response_data);
		if ($response_data === false && !$ch->hasErrors())
		{
			dd("fail1");
			//throw new CurlException("Error: Curl request failed");
		}
		else
		{
			if ($ch->hasErrors())
			{
				dd("fail2");
				//throw new PinterestException('Error: execute() - cURL error: ' . $ch->getErrors(), $ch->getErrorNumber());
			}
		}

		// Initiate the response
		$this->response = $response_data;

		if (is_string($response_data))
		{
			$this->response = json_decode($response_data, true);
		}

		// Check the response code
		if ($ch->getInfo(CURLINFO_HTTP_CODE) >= 400)
		{
			dd("fail3");
			//throw new PinterestException('Pinterest error (code: ' . $response->getResponseCode() . ') with message: ' . $response->getMessage(), $response->getResponseCode());
		}

		// Get headers from last request
		$this->headers = $ch->getHeaders();

		// Close curl resource
		$ch->close();

		// Return the response
		return $response;
	}

	public function getInfo($ch, $key)
	{
		return curl_getinfo($ch, $key);
	}

	private function parseHeaders($headers)
	{
		$result = [];
		foreach (explode("\n", $headers) as $row)
		{
			$header = explode(':', $row, 2);
			if (count($header) == 2)
			{
				$result[$header[0]] = trim($header[1]);
			}
			else
			{
				$result[] = $header[0];
			}
		}

		return $result;
	}

	public function setOption($ch, $option, $value)
	{
		curl_setopt($ch, $option, $value);

		return $ch;
	}

	public function setOptions($ch, array $options = [])
	{
		curl_setopt_array($ch, $options);

		return $ch;
	}

	public function execute()
	{
		return $this->execFollow();
	}

	private function execFollow()
	{

	}
}
