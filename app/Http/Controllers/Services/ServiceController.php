<?php

namespace App\Http\Controllers\Services;

/**
 * @author Vikash <Vikash@evibe.in>
 * @Since  26 Feb 2016
 */
use App\Http\Controllers\Approval\BaseApprovalController;
use App\Models\InternalQuestion;
use App\Models\ServiceEvent;
use App\Models\ServiceGallery;
use App\Models\ServiceTags;
use App\Models\TypeEvent;
use App\Models\TypeServices;

use App\Models\Util\SellingStatus;
use App\Models\Util\TypeSellingStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ServiceController extends BaseApprovalController
{
	//list of all services
	public function showServices(Request $request)
	{
		$searchQuery = $request->has('query') ? $request->input('query') : false;
		$services = TypeServices::selectCols()->orderBy('updated_at', 'DESC');
		$typeEvents = TypeEvent::all();

		// putting city filter first to get only city related relevant results
		$city = $request->input('city', 0);
		if ($city)
		{
			$services->where('city_id', $city);
		}

		if ($searchQuery)
		{
			$services->where('type_service.name', 'LIKE', '%' . $searchQuery . '%')
			         ->orWhere('type_service.code', 'LIKE', '%' . $searchQuery . '%')
			         ->orWhere('type_service.info', 'LIKE', '%' . $searchQuery . '%');

			if (is_int($searchQuery))
			{
				$services->orWhere('min_price', $searchQuery)
				         ->orWhere('max_price', $searchQuery);
			}
		}

		// Sort by selling status
		$sellingStatusId = request()->input('sellingStatus');
		if ($sellingStatusId > 0)
		{
			if ($sellingStatusId != config("evibe.selling.status.default"))
			{
				$sellingStatusDecorIds = SellingStatus::where("product_type_id", config("evibe.ticket_type.services"))
				                                      ->where("product_selling_status_id", $sellingStatusId)
				                                      ->pluck("product_id");

				$services->whereIn('id', $sellingStatusDecorIds);
			}
			else
			{
				$sellingStatusDecorIds = SellingStatus::where("product_type_id", config("evibe.ticket_type.services"))
				                                      ->where("product_selling_status_id", '<>', $sellingStatusId)
				                                      ->pluck("product_id");

				$services->whereNotIn('id', $sellingStatusDecorIds);
			}
		}

		$eventId = $request->has('event-type') ? $request->input('event-type') : null;
		if (!is_null($eventId))
		{
			$services->join('service_event', 'service_event.type_service_id', '=', 'type_service.id')
			         ->where('service_event.type_event_id', $eventId);
		}

		$services = $services->paginate(12);
		$cities = \City::all();
		$typeSellingStatus = TypeSellingStatus::all();

		return view('services.list', compact('services', 'typeEvents', 'cities', 'typeSellingStatus'));
	}

	public function showAddServices()
	{
		$typeEvents = TypeEvent::all();
		$cities = \City::all();

		return view('services.add_service', compact('typeEvents', 'cities'));
	}

	//saving services
	public function saveServices(Request $request)
	{
		$name = $request->input('name');
		$priceMin = $request->input('price_min');
		$priceMax = $request->input('price_max');
		$priceWorth = $request->input('price_worth');
		$rangeInfo = $request->input('range_info');
		$info = $request->input('info');
		$terms = $request->input('terms');
		$preReq = $request->input('prereq');
		$facts = $request->input('facts');
		$events = $request->input('events');

		$rules = [
			'name'      => 'required|min:4',
			'price_min' => 'required|Integer|min:1',
			'info'      => 'required|min:10',
			'terms'     => 'min:10',
			'prereq'    => 'min:10',
			'facts'     => 'min:10',
			'events'    => 'required',
		];
		$message = [
			'name.required'            => 'Service name is required',
			'info.required'            => 'Inclusion field is required',
			'name.min'                 => 'Service name should be 4 character long',
			'price_min.required'       => 'Price min field is mandatory',
			'price_min.min'            => 'Price min cannot be zero',
			'price_max.min_as'         => 'Price max should be greater than Price Min',
			'price_worth.min_as'       => 'Price worth should be greater than price Min',
			'range_info.required_with' => 'Range info is required with max price',
			'price_max.numeric'        => 'Price max must be a number',
			'price_worth.numeric'      => 'Price worth should be a number',
			'events.required'          => 'Please select at-least one supported event.'
		];
		if (!($priceWorth == 0 || (!$priceWorth)))
		{
			$rules['price_worth'] = 'min_as:price_min';
		}
		elseif (!($priceMax == 0 || !$priceMax))
		{
			$rules['price_max'] = 'min_as:price_min';
			$rules['range_info'] = 'required_with:price_max';
		}

		$validator = Validator::make($request->all(), $rules, $message);
		if ($validator->fails())
		{
			if ($validator->fails())
			{
				$oldEvents = "";
				if ($events)
				{
					$oldEvents = implode(",", $events);
				}

				$allInput = $request->input();
				$allInput['events'] = $oldEvents; // convert to string for showing old events

				return redirect()->back()->withErrors($validator)->withInput($allInput);
			}
		}

		$serviceId = TypeServices::insertGetId([
			                                       'name'        => $name,
			                                       'info'        => $info,
			                                       'range_info'  => $rangeInfo,
			                                       'url'         => $this->generateUrl($name),
			                                       'code'        => $this->generateCode(),
			                                       'prereq'      => $preReq,
			                                       'city_id'     => $request->input('city'),
			                                       'terms'       => $terms,
			                                       'facts'       => $facts,
			                                       'min_price'   => $priceMin,
			                                       'max_price'   => $priceMax,
			                                       'worth_price' => $priceWorth,
			                                       'created_at'  => Carbon::now(),
			                                       'updated_at'  => Carbon::now(),
		                                       ]);
		foreach ($events as $event)
		{
			ServiceEvent::create([
				                     'type_service_id' => $serviceId,
				                     'type_event_id'   => $event,
			                     ]);
		}
		$message = 'Service added successfully';

		return redirect()->route('services.show')->with('message', $message);
	}

	public function showEditServices($id)
	{
		$service = TypeServices::find($id);
		if ($service)
		{
			$typeEvents = TypeEvent::all();
			$serviceEvents = ServiceEvent::where('type_service_id', $id)->pluck('type_event_id');
			$cities = \City::all();

			return view('services.edit_service', compact('service', 'typeEvents', 'serviceEvents', 'cities'));
		}
		else
		{
			return "No services Found";
		}
	}

	public function updateServices($id, Request $request)
	{
		$service = TypeServices::find($id);
		$name = trim($request->input('name'));
		$priceMin = $request->input('price_min');
		$priceMax = $request->input('price_max');
		$priceWorth = $request->input('price_worth');
		$rangeInfo = $request->input('range_info');
		$info = $request->input('info');
		$terms = $request->input('terms');
		$preReq = $request->input('prereq');
		$facts = $request->input('facts');
		$events = $request->input('events');

		$rules = [
			'name'      => 'required|min:4',
			'price_min' => 'required|Integer|min:1',
			'info'      => 'required|min:10',
			'terms'     => 'min:10',
			'prereq'    => 'min:10',
			'facts'     => 'min:10',
			'events'    => 'required',
		];
		$message = [
			'name.required'            => 'Service name is required',
			'info.required'            => 'Inclusion field is required',
			'name.min'                 => 'Service name should be 4 character long',
			'price_min.required'       => 'Price min field is mandatory',
			'price_min.min'            => 'Price cannot be zero',
			'price_max.min_as'         => 'Price max should be greater than Price Min',
			'price_worth.min_as'       => 'Price worth should be greater than price Min',
			'range_info.required_with' => 'Range info is required with max price',
			'price_max.numeric'        => 'Price max must be a number',
			'price_worth.numeric'      => 'Price worth should be a number',
			'events.required'          => 'Please select at-least one supported event.'
		];
		if (!($priceWorth == 0 || (!$priceWorth)))
		{
			$rules['price_worth'] = 'min_as:price_min';
		}
		elseif (!($priceMax == 0 || !$priceMax))
		{
			$rules['price_max'] = 'min_as:price_min';
			$rules['range_info'] = 'required_with:price_max';
		}

		$validator = Validator::make($request->all(), $rules, $message);
		if ($validator->fails())
		{
			$oldEvents = "";
			if ($events)
			{
				$oldEvents = implode(",", $events);
			}

			$allInput = $request->input();
			$allInput['events'] = $oldEvents; // convert to string for showing old events

			return redirect()->back()->withErrors($validator)->withInput($allInput);
		}

		$service->update([
			                 'name'        => $name,
			                 'info'        => $info,
			                 'url'         => ($service->name != $name) ? $this->generateUrl($name) : $service->url,
			                 'range_info'  => $rangeInfo,
			                 'prereq'      => $preReq,
			                 'terms'       => $terms,
			                 'facts'       => $facts,
			                 'city_id'     => $request->input('city'),
			                 'min_price'   => $priceMin,
			                 'max_price'   => $priceMax,
			                 'worth_price' => $priceWorth,
			                 'updated_at'  => Carbon::now()
		                 ]);

		//Flushing the Tags which is made in main based on the Code of that product
		Cache::tags($service->code)->flush();
		$this->updateApprovalOnEditItem($service);
		$message = 'Service updated successfully';

		ServiceEvent::where('type_service_id', $id)->forceDelete();

		foreach ($events as $event)
		{
			ServiceEvent::create([
				                     'type_service_id' => $id,
				                     'type_event_id'   => $event,
			                     ]);
		}

		return redirect()->route('services.details', $id)->with('updateMsg', $message);
	}

	//Service Details
	public function showServiceDetails($id)
	{
		$service = TypeServices::find($id);
		$Iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $id, 'map_type_id' => config('evibe.ticket_type.services')])
		                       ->orderBy('deleted_by')->orderBy('updated_at', 'DESC')->get();
		$serviceEvent = ServiceEvent::where('type_service_id', $id)->get();

		$galleries = ServiceGallery::orderBy('created_at', 'DESC')->where('type_service_id', $id)->get();

		$images = $galleries->filter(function ($query) {
			return $query->type_id == config('evibe.gallery.type.image');
		});

		$videos = $galleries->filter(function ($query) {
			return $query->type_id == config('evibe.gallery.type.video');
		});

		$typeSellingStatus = TypeSellingStatus::select("id", "name", "color")
		                                      ->get();
		$sellingStatus = SellingStatus::where('product_type_id', config("evibe.ticket_type.services"))
		                              ->where('product_id', $service->id)
		                              ->first();

		// get only those tags whose parent id is entertainment parent id
		$categoriesList = \Tags::select('id', 'identifier')
		                       ->where("parent_id", config("evibe.service.parentCategoryId"))
		                       ->where(function ($query) use ($serviceEvent) {
			                       $query->whereNull('type_event')
			                             ->orWhereIn('type_event', $serviceEvent->pluck('id')->toArray());
		                       })
		                       ->get();

		// get all the tags
		$serviceCategories = ServiceTags::select('type_tags.id', 'type_tags.identifier')
		                                ->join('type_tags', 'type_tags.id', '=', 'service_tags.tag_id')
		                                ->where('service_id', $id)
		                                ->get()
		                                ->toArray();

		if ($service)
		{
			$data = [
				'service'           => $service,
				'iqs'               => $Iqs,
				'serviceEvent'      => $serviceEvent,
				'images'            => $images,
				'videos'            => $videos,
				'sellingStatus'     => $sellingStatus,
				'typeSellingStatus' => $typeSellingStatus,
				'categoriesList'    => $categoriesList,
				'serviceCategories' => $serviceCategories
			];

			$approvalVisibility = $this->getApprovalVisibility($service->id, config('evibe.ticket_type.services'));
			$data = array_merge($approvalVisibility, $data);

			return view('services.details', ['data' => $data]);
		}
		else
		{
			return 'No service found';
		}
	}

	// delete services
	public function deleteServices($id)
	{
		$service = TypeServices::find($id);
		if ($service->delete())
		{
			Cache::tags($service->code)->flush();
			$message = 'Service deleted successfully';

			return redirect()->route('services.show')->with('deleteMsg', $message);
		}

	}

	// Add service categories
	public function addServiceCategory($id)
	{
		$serviceCategoryIds = request('selectServiceCategory');

		$validServiceCategoryIds = \Tags::where("parent_id", config("evibe.service.parentCategoryId"))
		                                ->pluck('id')
		                                ->toArray();

		foreach ($serviceCategoryIds as $categoryId)
		{
			// verify if the tag exists in the database & should be child tag of the entertainment parent tag
			if (in_array($categoryId, $validServiceCategoryIds))
			{
				// If the tag already exists update the updated_at timestamp
				ServiceTags::updateOrCreate([
					                            'tag_id'     => $categoryId,
					                            'service_id' => $id
				                            ], ['updated_at' => Carbon::now()]);
			}
		}

		return redirect()->route('services.details', $id);
	}

	// Delete service category
	public function deleteServiceCategory($serviceId, $categoryId)
	{
		$serviceCategory = ServiceTags::where('service_id', $serviceId)
		                              ->where('tag_id', $categoryId)
		                              ->first();

		// delete if the tag exists otherwise redirect to the profile page
		if ($serviceCategory)
		{
			$serviceCategory->delete();
		}

		return redirect()->route('services.details', $serviceId);
	}

	public function uploadGallery($id)
	{
		$type = Input::get('gal-type');
		$redirectUrl = route('services.details', $id);

		if ($type == config('evibe.gallery.type.image'))
		{
			$images = Input::file('serviceImages');
			$watermark = is_null(Input::get('watermark')) ? true : false;

			if (!count($images) || (count($images) && is_null($images[0])))
			{
				return redirect($redirectUrl)
					->with('imageError', 'Please select at least one image.')
					->withInput();
			}

			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:5120'
				];

				$imageMessages = [
					'file.required' => 'Please upload at-least one image',
					'file.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
					'file.max'      => 'Please upload images with size less than 5 MB.'
				];

				$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

				if ($imageValidator->fails())
				{
					return redirect($redirectUrl)
						->with('imageError', $imageValidator->messages()->first())
						->withInput();
				}
			}

			// upload images
			$directPath = '/services/' . $id . '/images/';

			foreach ($images as $image)
			{
				$option = ['isWatermark' => $watermark];

				$imageName = $this->uploadImageToServer($image, $directPath, $option);
				$imgInsertData = [
					'url'             => $imageName,
					'title'           => $this->getImageTitle($image),
					'type_service_id' => $id,
					'type_id'         => $type
				];

				ServiceGallery::create($imgInsertData);
			}

			return redirect($redirectUrl)->with('successMsg', 'Image uploaded successfully.');
		}
		elseif ($type == config('evibe.gallery.type.video'))
		{
			$video = Input::get('video');

			$rules = [
				'video' => 'required'
			];

			$message = [
				'video.required' => 'Video url is required (Ex: https://www.youtube.com/watch?v=videoId).',
			];

			$validator = validator(['video' => $video], $rules, $message);
			if ($validator->fails())
			{
				return redirect($redirectUrl)
					->with('imageError', $validator->messages()->first())
					->withInput();
			}

			$videoId = $this->getYouTubeVideoId($video);
			if (!$videoId)
			{
				return redirect()->back()->with('imageError', 'Please enter an valid youtube url')->withInput();
			}

			ServiceGallery::create([
				                       'url'             => $videoId,
				                       'type_service_id' => $id,
				                       'type_id'         => $type
			                       ]);

			return redirect($redirectUrl)->with(['successMsg' => "Video uploaded successfully.", 'type' => 'video']);
		}
	}

	public function saveEditedTitle($serviceId, $imageId)
	{
		$galleryTitle = Input::get('title');
		$image = ServiceGallery::find($imageId);

		if ($image && $galleryTitle)
		{
			$image->title = $galleryTitle;
			$image->save();

			return redirect()->route('services.details', $serviceId);
		}

		return redirect()->back();
	}

	//delete Gallery
	public function deleteGallery($id, $imageId)
	{
		$gallery = ServiceGallery::find($imageId);

		if ($gallery->delete())
		{
			return redirect()->route('services.details', $id);
		}
	}

	/**
	 * @Since 3 March 2016
	 * Set profile Picture
	 */
	public function setProfilePic($serviceId, $imageId)
	{
		$image = ServiceGallery::find($imageId);
		$image->update(['is_profile' => 1]);

		$nonProfileImages = ServiceGallery::where('type_service_id', $serviceId)
		                                  ->where('id', '!=', $imageId)
		                                  ->get();

		foreach ($nonProfileImages as $nonProfileImage)
		{
			ServiceGallery::where('id', $nonProfileImage->id)->update(['is_profile' => 0]);
		}

		return redirect()->back();
	}

	/**
	 * @Since  5 Jan 2017
	 * @author :: Vikash <vikash@evibe.in>
	 * Set profile Picture
	 */

	public function makeLive($serviceId)
	{
		$service = TypeServices::findOrFail($serviceId);
		$service->update(['is_live' => 1]);

		return redirect()->back();
	}

	public function makeNonLive($serviceId)
	{
		$service = TypeServices::findOrFail($serviceId);
		$service->update(['is_live' => 0]);
		Cache::tags($service->code)->flush();

		return redirect()->back();
	}

	public function generateCode()
	{
		$lastId = TypeServices::withTrashed()->orderBy('id', 'DESC')->first();

		return config('evibe.code.prefix.services') . str_pad($lastId->id + 1, 4, '0', STR_PAD_LEFT);
	}
}
