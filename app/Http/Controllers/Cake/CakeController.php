<?php

namespace App\Http\Controllers\Cake;

use App\Http\Controllers\Approval\ApprovalController;
use App\Models\Cake\CakeFieldValue;
use App\Models\InternalQuestion;
use App\Models\TypeEvent;

use App\Models\Types\TypeCakeCategory;
use App\Models\Types\TypeCakeCategoryValues;
use App\Models\Types\TypeCakeFlavour;
use App\Models\Util\SellingStatus;
use App\Models\Util\TypeSellingStatus;
use Cake;
use CakeEvent;
use CakeGallery;
use CakeTags;
use City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Vendor;

/**
 * @author   Anji <anji@evibe.in>
 * @since    1 June 2015
 * @modified by Vikash<vikash@evibe.in>
 * @since    22 Oct 2016
 */
class CakeController extends ApprovalController
{
	private $categories;

	public function __construct()
	{
		$this->categories = TypeCakeCategory::all();
	}

	// show cake list
	public function showCakesList(Request $request)
	{
		$isShowReset = false;
		$searchQuery = Input::has('query') ? Input::get('query') : false;

		$cakes = Cake::join('planner', 'planner.id', '=', 'cake.provider_id')
		              ->whereNull('planner.deleted_at')
		              ->whereNull('cake.deleted_at')
		              ->orderBy('cake.updated_at', 'DESC')->select('cake.*');

		$selectedCity = "all cities";
		$cityId = request()->input('city', 'all');

		if ($cityId != 'all' && ($cityId = (int)$cityId) && $cityId > 0)
		{
			$isShowReset = true;
			$allCities = City::all();
			$allCityIds = $allCities->pluck('id')->toArray();

			if (in_array($cityId, $allCityIds))
			{
				$selectedCityPos = $allCities->search(function ($city) use ($cityId) {
					return $city->id == $cityId;
				});

				$selectedCity = $allCities[$selectedCityPos]->name;
				$cakes->where('planner.city_id', $cityId);
			}
		}

		// selling status filter
		$sellingStatusId = request()->input('sellingStatus');
		if ($sellingStatusId > 0)
		{
			if ($sellingStatusId != config("evibe.selling.status.default"))
			{
				$sellingStatusCakeIds = SellingStatus::where("product_type_id", config("evibe.ticket_type.cakes"))
				                                     ->where("product_selling_status_id", $sellingStatusId)
				                                     ->pluck("product_id");

				$cakes->whereIn('cake.id', $sellingStatusCakeIds);
			}
			else
			{
				$sellingStatusCakeIds = SellingStatus::where("product_type_id", config("evibe.ticket_type.cakes"))
				                                     ->where("product_selling_status_id", '<>', $sellingStatusId)
				                                     ->pluck("product_id");

				$cakes->whereNotIn('cake.id', $sellingStatusCakeIds);
			}

			$isShowReset = true;
		}

		// provider filter
		$providerIds = $cakes->pluck('provider_id')->toArray();
		$providers = Vendor::whereIn('id', array_unique($providerIds))->get();

		$providerId = request()->input('provider', 'all');

		if ($providerId != 'all' && ($providerId = (int)$providerId) && $providerId > 0)
		{
			$isShowReset = true;
			$cakes->where('provider_id', $providerId);

		}

		// search filter
		if ($searchQuery)
		{
			$isShowReset = true;
			$cakes->where(function ($innerQuery) use ($searchQuery) {
				$innerQuery->where('cake.title', 'LIKE', '%' . $searchQuery . '%')
				           ->orWhere('cake.code', 'LIKE', '%' . $searchQuery . '%');

				if (is_int($searchQuery))
				{
					$innerQuery->orWhere('cake.price_per_kg', $searchQuery)
					           ->orWhere('cake.price_per_kg', $searchQuery);
				}
			});
		}

		// price filters
		if (Input::has('price_min') || Input::has('price_max'))
		{
			$priceMin = Input::get('price_min');
			$priceMax = Input::get('price_max');
			if ($priceMin && $priceMax)
			{
				$isShowReset = true;
				$cakes->where(function ($innerQuery) use ($priceMin, $priceMax) {
					$innerQuery->whereBetween('cake.price_per_kg', [$priceMin, $priceMax])->get();
				});
			}
			elseif ($priceMin && !$priceMax)
			{
				$isShowReset = true;
				$cakes->where('cake.price_per_kg', '>=', $priceMin);
			}
			elseif (!$priceMin && $priceMax)
			{
				$isShowReset = true;
				$cakes->where('cake.price_per_kg', '<=', $priceMax);
			}
			else
			{
				// do nothing
			}
		}

		$cakes = $cakes->paginate(18);

		$data = [
			'cakes'             => $cakes,
			'cities'            => City::all(),
			'selectedCity'      => $selectedCity,
			'isShowReset'       => $isShowReset,
			'providers'         => $providers,
			'typeSellingStatus' => TypeSellingStatus::all()
		];

		return view('cakes/list', ['data' => $data]);
	}

	// show create new cake page (only the basic info)
	public function showCreateNewCakePage()
	{
		$providers = Vendor::all();
		$flavours = TypeCakeFlavour::all();

		$data = [
			'providers'  => $providers,
			'flavours'   => $flavours,
			'categories' => $this->categories
		];

		return view('cakes/create/info', ['data' => $data]);
	}

	// save new cake (only basic info)
	public function createNewCake(Request $request)
	{
		$rules = [
			'title'        => 'required|min:4',
			'price'        => 'required|integer',
			'price-worth'  => 'integer|min_as:price',
			'price-per-kg' => 'required|integer',
			'min-weight'   => 'required|numeric',
			'info'         => 'required|min:10',
		];

		$messages = [
			'title.required'        => 'Cake title is required',
			'title.min'             => 'Cake title should be minimum 4 characters long',
			'info.required'         => 'Cake description is required',
			'price.required'        => 'Cake minimum order price is required (ex: 599)',
			'price.integer'         => 'Please enter valid price (ex: 599)',
			'price-worth.numeric'   => 'Please enter valid price worth (integer)',
			'price-worth.min_as'    => 'Price worth cannot be less than price',
			'price-per-kg.required' => 'Price / Kg is required',
			'price-per-kg.integer'  => 'Price / Kg must be a valid integer value',
			'min-weight.required'   => 'Minimum weight field is required',
			'min-weight.numeric'    => 'Please enter a valid minimum weight value. (Ex: 2.5)',
		];

		$validation = validator($request->all(), $rules, $messages);

		if ($validation->fails())
		{
			return redirect(route('cakes.new'))->withErrors($validation)->withInput();
		}
		else
		{
			$flavour = $request->input('flavour') == -1 ? null : $request->input('flavour');
			// save cake details
			$cake = Cake::create([
				                      'title'        => $request->input('title'),
				                      'price'        => $request->input('price'),
				                      'price_worth'  => $request->input('price-worth'),
				                      'price_per_kg' => $request->input('price-per-kg'),
				                      'info'         => $request->input('info'),
				                      'url'          => $this->generateUrl($request->input('title')),
				                      'provider_id'  => $request->input('provider'),
				                      'flavour_id'   => $flavour,
				                      'min_order'    => $request->input('min-weight')
			                      ]);

			// save code
			$cake->code = config('evibe.cake.code') . (config('evibe.cake.base_id') + $cake->id);
			$cake->save();

			return redirect()->route('cakes.view.info', $cake->id);
		}
	}

	// update cake details (only basic info)
	public function updateCakeDetails($cakeId, Request $request)
	{
		$rules = [
			'title'        => 'required|min:4',
			'price'        => 'required|integer',
			'price-worth'  => 'integer|min_as:price',
			'price-per-kg' => 'required|integer',
			'min-weight'   => 'required|numeric',
			'info'         => 'required|min:10',
		];

		$messages = [
			'title.required'        => 'Cake title is required',
			'title.min'             => 'Cake title should be minimum 4 characters long',
			'info.required'         => 'Cake description is required',
			'price.required'        => 'Cake minimum order price is required (ex: 599)',
			'price.integer'         => 'Please enter valid price (ex: 599)',
			'price-worth.numeric'   => 'Please enter valid price worth (integer)',
			'price-worth.min_as'    => 'Price worth cannot be less than price',
			'price-per-kg.required' => 'Price / Kg is required',
			'price-per-kg.integer'  => 'Price / Kg must be a valid integer value',
			'min-weight.required'   => 'Minimum weight field is required',
			'min-weight.numeric'    => 'Please enter a valid minimum weight value. (Ex: 2.5)',
		];

		$validation = validator($request->all(), $rules, $messages);

		if ($validation->fails())
		{
			return redirect()->back()->withErrors($validation)->withInput();
		}

		$cake = Cake::findOrFail($cakeId);
		$flavour = $request->input('flavour') == -1 ? null : $request->input('flavour');
		$updateData = [
			'title'        => $request->input('title'),
			'price'        => $request->input('price'),
			'price_worth'  => $request->input('price-worth'),
			'price_per_kg' => $request->input('price-per-kg'),
			'info'         => $request->input('info'),
			'url'          => ($cake->title != trim($request->input('title'))) ? $this->generateUrl($request->input('title')) : $cake->url,
			'provider_id'  => $request->input('provider'),
			'flavour_id'   => $flavour,
			'min_order'    => $request->input('min-weight')
		];

		$this->updateApprovalOnEditItem($cake);

		if ($cake->update($updateData))
		{
			//Flushing the Tags which is made in main based on the Code of that product
			Cache::tags($cake->code)->flush();

			return redirect()->route('cakes.view.info', $cakeId);
		}
		else
		{
			return "some Error occurred while editing ";
		}
	}

	// show create gallery with all the existing data
	public function showCreateGallery($cakeId)
	{
		$cake = Cake::with('provider')->findOrFail($cakeId);

		$gallery = CakeGallery::orderBy('created_at', 'DESC')->where('cake_id', $cakeId)->get();

		$images = $gallery->filter(function ($query) {
			return $query->type == config('evibe.gallery.type.image');
		});

		$videos = $gallery->filter(function ($query) {
			return $query->type == config('evibe.gallery.type.video');
		});

		$data = [
			'cake'       => $cake,
			'categories' => $this->categories,
			'images'     => $images,
			'videos'     => $videos
		];

		return view('cakes/create/gallery', ['data' => $data]);
	}

	// show the page for edit and delete the category
	public function showCakeCategoryCreate($cakeId, $catId)
	{
		$cake = Cake::find($cakeId);
		$category = TypeCakeCategory::find($catId);
		$existingCakeFieldsValues = CakeFieldValue::where(['cake_id' => $cakeId, 'cake_cat_id' => $catId])->get();

		if ($cake && $category)
		{
			$data = [
				'cake'                    => $cake,
				'categories'              => $this->categories,
				'category'                => $category,
				'existingCakeFieldValues' => $existingCakeFieldsValues
			];

			return view('cakes/create/categories', ['data' => $data]);
		}

		return "Invalid cake of category id";

	}

	// view the category field values
	public function showCategoryValues($cakeId, $catId)
	{
		$cake = Cake::find($cakeId);
		$category = TypeCakeCategory::find($catId);
		$cakeFields = CakeFieldValue::where(['cake_id' => $cakeId, 'cake_cat_id' => $catId])->get();

		if ($cake && $category)
		{
			$data = [
				'cake'       => $cake,
				'categories' => $this->categories,
				'category'   => $category,
				'cakeFields' => $cakeFields
			];

			return view('cakes/details/categories', ['data' => $data]);
		}

		return "Invalid cake of category id";

	}

	// show the cake basic details with tags, and event tagging
	public function showCakeDetailsInfo($cakeId)
	{
		$cake = Cake::with('provider', 'events')->findOrFail($cakeId);

		$Iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $cakeId, 'map_type_id' => config('evibe.ticket_type.cakes')])
		                       ->orderBy('deleted_by')
		                       ->orderBy('updated_at', 'DESC')
		                       ->get();

		$typeEvents = TypeEvent::all();
		$typeSellingStatus = TypeSellingStatus::select("id", "name", "color")->get();
		$sellingStatus = SellingStatus::where('product_type_id', config("evibe.ticket_type.cakes"))->where('product_id', $cake->id)->first();

		$cakeEvents = $cake->events;

		$cakeEventIds = [];
		if ($cakeEvents->count() > 0)
		{
			$cakeEventIds = $cakeEvents->pluck("id")->toArray();
		}

		$data = [
			'cake'              => $cake,
			'cakeEvents'        => $cakeEvents,
			'tags'              => $cake->getTags(),
			'tagsList'          => $this->getTagsList(config('evibe.ticket_type.cakes'), $cakeEventIds),
			'iqs'               => $Iqs,
			'typeEvents'        => $typeEvents,
			'categories'        => $this->categories,
			'sellingStatus'     => $sellingStatus,
			'typeSellingStatus' => $typeSellingStatus
		];

		$approvalVisibility = $this->getApprovalVisibility($cakeId, config('evibe.ticket_type.cakes'));
		$data = array_merge($approvalVisibility, $data);

		return view('cakes/details/info', ['data' => $data]);
	}

	// function for adding and updating the dynamic cake values
	public function saveCakeCategoryValues($cakeId, $catId, Request $request)
	{
		$res = ['success' => false];
		$uniqueCatValues = [];
		$newCatValueIds = [];
		$cakeValueIds = [];
		$allInputs = $request->input('allInputs');
		$redirectUrl = route('cakes.category.view', [$cakeId, $catId]);
		$cake = Cake::findOrFail($cakeId);

		foreach ($allInputs as $allInput)
		{
			$labelTextPrice = $allInput['priceType'] == config('evibe.cake_price_type.per_kg_price') ? "Price / Kg" : "Price";
			$labelTextWorth = $allInput['priceType'] == config('evibe.cake_price_type.per_kg_price') ? "Price / Kg Worth" : "Price Worth";
			$catVal = TypeCakeCategoryValues::findOrFail($allInput['catType'])->name;

			$rules = [
				'type'        => 'required',
				'price'       => 'required|integer|required_with:price_worth',
				'price_worth' => 'integer|min_as:price'
			];

			$messages = [
				'type.required'       => "Please select a type.",
				'price.required'      => "$labelTextPrice for $catVal  is required. (Ex: 200).",
				'price.integer'       => "$labelTextPrice for $catVal should be a valid integer value.",
				'price.required_with' => "$labelTextPrice for $catVal is mandatory when you enter $labelTextWorth.",
				'price_worth.integer' => "$labelTextWorth for $catVal should be a valid integer value.",
				'price_worth.min_as'  => "$labelTextWorth for $catVal can't be less than $labelTextPrice."
			];

			$data = [
				'type'        => $allInput['priceType'],
				'price'       => $allInput['catTypePrice'],
				'price_worth' => $allInput['catTypeWorth']
			];

			// if category is weight type then don't allow to fill less than min price
			if ($catId == config('evibe.cake_cat_value.weight'))
			{
				array_set($rules, 'price', 'required|integer|required_with:price_worth|min:' . $cake->price);
				$messages['price.min'] = "$labelTextWorth for $catVal can't be less than $cake->price (i.e, base price of this cake)";
			}

			// if different price has entered for same types of category values(Ex: two price for 1Kg cake), then throw the Error.
			if (in_array($allInput['catType'], $uniqueCatValues))
			{
				$res = [
					'success' => false,
					'error'   => "You cannot put two different prices for same category values $catVal, select the different category values."
				];

				return response()->json($res);
			}

			// if category is flavour then price is not mandatory
			if ($catId == config('evibe.cake_cat_value.flavour'))
			{
				$rules['price'] = 'integer|min:0';
				if ($data['price'] == 0)
				{
					$rules['price_worth'] = 'integer';
				}
			}

			// validation error
			$validator = validator($data, $rules, $messages);
			if ($validator->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validator->messages()->first()
				                        ]);
			}

			$uniqueCatValues[] = $allInput['catType'];
		}

		// save the values in database
		foreach ($allInputs as $allInput)
		{
			$catVal = TypeCakeCategoryValues::findOrFail($allInput['catType'])->name;
			$isPricePerKg = $allInput['priceType'] == config('evibe.cake_price_type.per_kg_price') ? 1 : 0;
			$fixedPrice = $fixedWorth = $extraPrice = $extraWorth = 0;

			// calculate the price, based on is_per_kg
			// if price is for weight then add the values
			if ($allInput['priceType'] == config('evibe.cake_price_type.per_kg_price'))
			{
				$extraPrice = $allInput['catTypePrice'];
				$extraWorth = $allInput['catTypeWorth'] ? $allInput['catTypeWorth'] : 0;
			}
			elseif (($allInput['priceType'] == config('evibe.cake_price_type.fixed_price')))
			{
				$fixedPrice = $allInput['catTypePrice'];
				$fixedWorth = $allInput['catTypeWorth'] ? $allInput['catTypeWorth'] : 0;
			}

			// check if the value of same type is already exist
			$valueExist = CakeFieldValue::where(['cake_id' => $cakeId, 'cake_cat_value_id' => $allInput['catType']])->first();
			if (!$valueExist)
			{
				$newId = CakeFieldValue::insertGetId([
					                                     'cake_id'           => $cakeId,
					                                     'cake_cat_id'       => $catId,
					                                     'cake_cat_value_id' => $allInput['catType'],
					                                     'meta_cat_value'    => $catVal,
					                                     'extra_price'       => $extraPrice,
					                                     'extra_worth'       => $extraWorth,
					                                     'fixed_price'       => $fixedPrice,
					                                     'fixed_worth'       => $fixedWorth,
					                                     'is_per_kg'         => $isPricePerKg
				                                     ]);
				if ($newId)
				{
					$cakeValueIds[] = $newId;
					$newCatValueIds[] = $allInput['catType'];
					$res = ['success' => true];
				}
				else
				{
					// delete the saved values and throw the error
					if (count($cakeValueIds))
					{
						CakeFieldValue::whereIn($cakeValueIds)->delete();
					}
					$res = [
						'success' => false,
						'error'   => 'Error while inserting the values.'
					];

					return $res;
				}
			}
			else
			{
				$valueUpdate = $valueExist->update([
					                                   'extra_price' => $extraPrice,
					                                   'extra_worth' => $extraWorth,
					                                   'fixed_price' => $fixedPrice,
					                                   'fixed_worth' => $fixedWorth,
					                                   'is_per_kg'   => $isPricePerKg
				                                   ]);
				if ($valueUpdate)
				{
					$newCatValueIds[] = $allInput['catType'];
					$res = ['success' => true];
				}
			}

		}
		// @see:: we are deleting the values which is removed by the user before saving
		$oldValuesIds = CakeFieldValue::where(['cake_id' => $cakeId, 'cake_cat_id' => $catId])->pluck('cake_cat_value_id');

		foreach ($oldValuesIds as $oldValuesId)
		{
			if (!in_array($oldValuesId, $newCatValueIds))
			{
				CakeFieldValue::where(['cake_id' => $cakeId, 'cake_cat_value_id' => $oldValuesId])->delete();
			}
		}

		if ($res['success'])
		{
			$res['redirectUrl'] = $redirectUrl;
		}

		return response()->json($res);
	}

	/**
	 * @author Vikash<vikash@evibe.in>
	 * @Since  18 Jan 2016
	 */
	public function deleteCake($cakeId)
	{
		$cake = Cake::find($cakeId);
		if ($cake)
		{
			if ($cake->delete())
			{
				Cache::tags($cake->code)->flush();
				$successMsg = 'Cake deleted successfully';

				return redirect()->route('cakes.list')
				                 ->with('customSuccessMessage', $successMsg);
			}
		}
		else
		{
			return redirect()->route('cakes.view')
			                 ->with('customErrorMessage', 'please select valid cake to delete');
		}
	}

	/**
	 * @Since 5 May 2016, show edit cake
	 */

	public function showEditCake($cakeId)
	{
		$cake = Cake::findOrFail($cakeId);
		$providers = Vendor::whereNull('deleted_at')->get();
		$flavours = TypeCakeFlavour::all();

		$data = [
			'cake'       => $cake,
			'providers'  => $providers,
			'flavours'   => $flavours,
			'categories' => $this->categories
		];

		return view('cakes.edit.info', ['data' => $data]);
	}

	public function activateCake($cakeId)
	{
		$cake = Cake::findOrFail($cakeId);
		$event = $cake->events->first();
		if (!$event)
		{
			return redirect()->back()->with('customError', 'Please tag an event before making live on website');
		}
		else
		{
			$cake->update(['is_live' => 1]);
			$this->checkAndSendFirstActivationEmail($event->id, config('evibe.ticket_type.cakes'), $cake);
		}

		return redirect()->back();
	}

	public function deactivateCake($cakeId)
	{
		$cake = Cake::findOrFail($cakeId);

		if ($cake->update(['is_live' => 0]))
		{
			Cache::tags($cake->code)->flush();
			$this->removeItemsFromAutoBookAndPriorityTable(config('evibe.ticket_type.cakes'), $cakeId);
		}

		return redirect()->back();
	}

	public function uploadImages($cakeId, Request $request)
	{
		$type = $request->input('gal-type');
		$images = $request->file('images');
		$cake = Cake::with('provider')->findOrFail($cakeId);

		if ($type == config('evibe.gallery.type.image'))
		{

			if ((count($images) > 0 && is_null($images[0])) || count($images) == 0)
			{
				return redirect()->back()
				                 ->with('customError', 'Please choose at least one image.')
				                 ->withInput();
			}

			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'mimes:png,jpeg,jpg,JPG|max:5120',
				];

				$imageMessages = [
					'file.mimes' => 'One of the images is not valid (only .png, .jpeg, .jpg are accepted)',
					'file.max'   => 'File size is above 5 MB. Please compress the image'
				];

				$imageValidator = validator(['file' => $image], $imageRules, $imageMessages);

				if ($imageValidator->fails())
				{
					return redirect()->back()->withErrors($imageValidator)->withInput();
				}
			}

			// upload images

			$directPath = '/cakes/' . $cake->id . '/';

			foreach ($images as $image)
			{
				if ($image)
				{
					$newFileName = $this->uploadImageToServer($image, $directPath);

					$imageInsertData = [
						'cake_id'    => $cakeId,
						'title'      => $this->getImageTitle($image),
						'url'        => $newFileName,
						'is_profile' => 0,
						'type'       => config('evibe.gallery.type.image')
					];

					CakeGallery::create($imageInsertData);
				}
			}

			return redirect()->back()->with('customSuccess', 'Images uploaded successfully');
		}
		elseif ($type == config('evibe.gallery.type.video'))
		{
			$video = $request->input('video');

			$rules = [
				'video' => 'required'
			];
			$message = [
				'video.required' => 'Video url is required (Ex: https://www.youtube.com/watch?v=videoId).',
			];

			$validator = validator(['video' => $video], $rules, $message);
			if ($validator->fails())
			{
				return redirect()->back()
				                 ->withErrors($validator)
				                 ->withInput();
			}

			$videoId = $this->getYouTubeVideoId($video);

			if (!$videoId)
			{
				return redirect()->back()->with('customError', 'Please enter an valid youtube url')->withInput();
			}

			CakeGallery::create([
				                     'cake_id' => $cakeId,
				                     'type'    => $type,
				                     'url'     => $videoId
			                     ]);

			return redirect()->back()->with(['customSuccess' => "Video uploaded successfully.", 'type' => 'video']);
		}
	}

	public function unsetImageAsProfilePic($cakeId, $imageId)
	{
		$image = CakeGallery::find($imageId);

		if ($image)
		{
			$image->is_profile = config('evibe.gallery.profile_pic.unset');
			$image->save();
		}

		return redirect()->back();
	}

	public function setImageAsProfilePic($cakeId, $imageId)
	{
		$image = CakeGallery::find($imageId);

		if ($image)
		{
			$image->is_profile = config('evibe.gallery.profile_pic.set');
			$image->save();
		}

		return redirect()->back();
	}

	public function editCakeTitle($cakeId, $imageId, Request $request)
	{
		$image = CakeGallery::find($imageId);
		if ($image && $image->update(['title' => $request->input('img-title-input')]))
		{
			return redirect()->back()->with('successMsg', 'Title updated successfully');
		}
	}

	public function deleteImage($cakeId, $imageId)
	{
		$image = CakeGallery::find($imageId);
		if ($image)
		{
			$image->delete();
		}

		return redirect()->back();
	}

	public function showCakeDetailsPrices($cakeId)
	{
		$cake = Cake::with('prices')->findOrFail($cakeId);

		$data = [
			'cake' => $cake
		];

		return view('cakes/prices', ['data' => $data]);
	}

	public function addNewTag($cakeId, Request $request)
	{
		$parentTagId = $request->input('selectParentTag');
		$childTagId = $request->input('selectChildTag');
		$tagId = $parentTagId;

		if ($childTagId != -1)
		{
			$tagId = $childTagId;
		}

		if ($tagId != -1)
		{
			$existingTag = CakeTags::where(['cake_id' => $cakeId, 'tag_id' => $tagId])->count();
			if ($existingTag == 0)
			{
				CakeTags::create(['cake_id' => $cakeId, 'tag_id' => $tagId]);
			}
		}

		return redirect()->route('cakes.view.info', $cakeId);
	}

	public function deleteCakeTag($cakeId, $cakeTagId)
	{
		$cakeTag = CakeTags::where('cake_id', $cakeId)
		                    ->where('tag_id', $cakeTagId)
		                    ->first();

		if ($cakeTag)
		{
			$cakeTag->delete();
		}

		return redirect(route('cakes.view.info', $cakeId));
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  30 Apr 2016
	 */
	public function deleteCakeEvent($cakeId, $cakeEventId)
	{
		$cakeEvent = CakeEvent::where('cake_id', $cakeId)
		                       ->where('event_id', $cakeEventId)
		                       ->first();

		if ($cakeEvent)
		{
			$cakeEvent->delete();
		}

		return redirect()->route('cakes.view.info', $cakeId);
	}

	public function addNewEventSupported($cakeId, Request $request)
	{
		$eventId = $request->input('event');
		$mappingExists = CakeEvent::where('cake_id', $cakeId)
		                           ->where('event_id', $eventId)
		                           ->get();

		if ($mappingExists->count() == 0)
		{
			CakeEvent::create([
				                   'cake_id'  => $cakeId,
				                   'event_id' => $eventId
			                   ]);
		}

		return redirect()->route('cakes.view.info', $cakeId);
	}
}
