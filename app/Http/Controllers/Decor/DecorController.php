<?php

namespace App\Http\Controllers\Decor;

use App\Models\DecorEvent;
use App\Models\InternalQuestion;
use App\Models\TypeEvent;
use App\Models\TypeFacts;
use App\Models\TypePrerequisite;
use App\Models\User;
use App\Models\Util\SellingStatus;
use App\Models\Util\TypeSellingStatus;
use Evibe\Facades\AppUtilFacade as AppUtil;
use App\Http\Controllers\Base\DecorBaseController;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

/**
 * @author Anji <anji@evibe.in>
 * @since  1 June 2015
 */
class DecorController extends DecorBaseController
{
	public function showDecorStylesList()
	{
		$isShowReset = false;

		$searchQuery = Input::has('query') ? Input::get('query') : false;
		$liveCheck = [1];
		$decors = \Decor::with('provider')
		                ->join('planner', 'planner.id', '=', 'decor.provider_id')
		                ->whereNull('planner.deleted_at')
		                ->whereNull('decor.deleted_at')
		                ->orderBy('decor.is_live', 'DESC')
		                ->orderBy('decor.priority', 'DESC')
		                ->orderBy('decor.created_at', 'DESC');

		// show non-live decors as well
		if (AppUtil::isTabVisible('decors::nonlive'))
		{
			array_push($liveCheck, 0);
		}
		$decors->whereIn('decor.is_live', $liveCheck);

		// city filter
		$selectedCity = "all cities";
		$cityId = request()->input('city', 'all');
		if ($cityId != 'all' && ($cityId = (int)$cityId) && $cityId > 0)
		{
			$isShowReset = true;
			$allCities = \City::all();
			$allCityIds = $allCities->pluck('id')->toArray();

			if (in_array($cityId, $allCityIds))
			{
				$selectedCityPos = $allCities->search(function ($city) use ($cityId) {
					return $city->id == $cityId;
				});

				$selectedCity = $allCities[$selectedCityPos]->name;
				$decors->where('planner.city_id', $cityId);
			}
		}

		// selling status filter
		$sellingStatusId = request()->input('sellingStatus');
		if ($sellingStatusId > 0)
		{
			if ($sellingStatusId != config("evibe.selling.status.default"))
			{

				$sellingStatusDecorIds = SellingStatus::where("product_type_id", config("evibe.ticket_type.decors"))
				                                      ->where("product_selling_status_id", $sellingStatusId)
				                                      ->pluck("product_id");

				$decors->whereIn('decor.id', $sellingStatusDecorIds);
			}
			else
			{
				$sellingStatusDecorIds = SellingStatus::where("product_type_id", config("evibe.ticket_type.decors"))
				                                      ->where("product_selling_status_id", '!=', $sellingStatusId)
				                                      ->pluck("product_id");

				$decors->whereNotIn('decor.id', $sellingStatusDecorIds);
			}

			$isShowReset = true;
		}

		// provider filter
		$decors = $decors->select('decor.*');
		$providerIds = $decors->pluck('provider_id')->toArray();
		$providers = \Vendor::whereIn('id', array_unique($providerIds))->get();

		$providerId = request()->input('provider', 'all');

		if ($providerId != 'all' && ($providerId = (int)$providerId) && $providerId > 0)
		{
			$isShowReset = true;
			$decors->where('provider_id', $providerId);
		}

		//occasion filter
		$occasionId = request()->input('occasion', 'all');
		$occasions = TypeEvent::select('id', 'name')->get();

		if ($occasionId != 'all' && ($occasionId = (int)$occasionId) && $occasionId > 0)
		{
			$allOccasionRelatedIds = DecorEvent::where('event_id', $occasionId)->pluck('decor_id');
			if ($allOccasionRelatedIds && (count($allOccasionRelatedIds) > 0))
			{
				$isShowReset = true;
				$decors->whereIn('decor.id', $allOccasionRelatedIds);
			}
		}

		// search filter
		if ($searchQuery)
		{
			$isShowReset = true;
			$decors->where(function ($innerQuery) use ($searchQuery) {
				$innerQuery->where('decor.name', 'LIKE', '%' . $searchQuery . '%')
				           ->orWhere('decor.code', 'LIKE', '%' . $searchQuery . '%');

				if (is_int($searchQuery))
				{
					$innerQuery->orWhere('decor.min_price', $searchQuery)
					           ->orWhere('decor.max_price', $searchQuery);
				}
			});
		}

		// price filters
		if (Input::has('price_min') || Input::has('price_max'))
		{
			$priceMin = Input::get('price_min');
			$priceMax = Input::get('price_max');
			if ($priceMin && $priceMax)
			{
				$isShowReset = true;
				$decors->where(function ($innerQuery) use ($priceMin, $priceMax) {
					$innerQuery->whereBetween('decor.min_price', [$priceMin, $priceMax])
					           ->orWhereBetween('decor.max_price', [$priceMin, $priceMax]);
				});
			}
			elseif ($priceMin && !$priceMax)
			{
				$isShowReset = true;
				$decors->where('decor.min_price', '>=', $priceMin);
			}
			elseif (!$priceMin && $priceMax)
			{
				$isShowReset = true;
				$decors->where('decor.min_price', '<=', $priceMax);
			}
			else
			{
				// do nothing
			}
		}

		$decors = $decors->paginate(21);

		$data = [
			'decors'            => $decors,
			'cities'            => \City::all(),
			'selectedCity'      => $selectedCity,
			'isShowReset'       => $isShowReset,
			'providers'         => $providers,
			'occasions'         => $occasions,
			'typeSellingStatus' => TypeSellingStatus::all()
		];

		return view('decors/list', ['data' => $data]);

	}

	public function showDecorDetails($decorId)
	{
		$vendors = \Vendor::orderBy('name', 'ASC')->get();
		$decor = \Decor::with('provider', 'provider.area', 'gallery')->findOrFail($decorId);
		$Iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $decorId, 'map_type_id' => config('evibe.ticket_type.decors')])
		                       ->orderBy('deleted_by')->orderBy('updated_at', 'DESC')
		                       ->get();
		$decorEvents = DecorEvent::where('decor_id', $decorId);
		$typeEvents = TypeEvent::all();
		$terms = \BookingTerms::all();
		$facts = TypeFacts::all();
		$preReq = TypePrerequisite::all();

		$typeSellingStatus = TypeSellingStatus::select("id", "name", "color")->get();
		$sellingStatus = SellingStatus::where('product_type_id', config("evibe.ticket_type.decors"))->where('product_id', $decor->id)->first();

		$handlers = User::whereIn('role_id', [config('evibe.roles.bd'), config('evibe.roles.sr_bd')])->get();

		$gallery = \DecorGallery::orderBy('created_at', 'DESC')->where('decor_id', $decorId)->get();

		$images = $gallery->filter(function ($query) {
			return $query->type_id == config('evibe.gallery.type.image');
		});

		$videos = $gallery->filter(function ($query) {
			return $query->type_id == config('evibe.gallery.type.video');
		});

		$allDecorEvents = $decor->events;

		$allDecorEventsIds = [];
		if ($allDecorEvents->count() > 0)
		{
			$allDecorEventsIds = $allDecorEvents->pluck("event_id")->toArray();
		}

		$productTypeId = config('evibe.ticket_type.decors');

		$data = [
			'decor'             => $decor,
			'tags'              => $decor->getTags(),
			'vendors'           => $vendors,
			'tagsList'          => self::getTagsList($productTypeId, $allDecorEventsIds),
			'terms'             => $terms,
			'iqs'               => $Iqs,
			'facts'             => $facts,
			'preReq'            => $preReq,
			'typeEvents'        => $typeEvents,
			'decorEventsId'     => $decorEvents->pluck('event_id'),
			'decorEvents'       => $decorEvents->get(),
			'images'            => $images,
			'videos'            => $videos,
			'handlers'          => $handlers,
			'sellingStatus'     => $sellingStatus,
			'typeSellingStatus' => $typeSellingStatus
		];

		$approvalVisibility = $this->getApprovalVisibility($decorId, config('evibe.ticket_type.decors'));
		$data = array_merge($approvalVisibility, $data);

		return view('decors/info', ['data' => $data]);
	}

	public function showCreateNewDecorPage()
	{
		$vendors = \Vendor::all();
		$terms = \BookingTerms::all();
		$typeEvents = TypeEvent::all();
		$facts = TypeFacts::all();
		$preReq = TypePrerequisite::all();
		$handlers = User::whereIn('role_id', [config('evibe.roles.bd'), config('evibe.roles.sr_bd')])->get();

		$data = [
			'vendors'    => $vendors,
			'terms'      => $terms,
			'facts'      => $facts,
			'preReq'     => $preReq,
			'typeEvents' => $typeEvents,
			'handlers'   => $handlers
		];

		return view('decors/add_decor', ['data' => $data]);
	}

	public function saveNewDecor()
	{
		$minPrice = Input::get('dminprice');
		$maxPrice = Input::get('dmaxprice');
		$events = Input::get('events');

		$rules = [
			'dprovider'     => 'required|integer',
			'dname'         => 'required|min:3',
			'dworth'        => 'numeric|min_as:dminprice',
			'dminprice'     => 'required|numeric',
			'dtimesetup'    => 'required|numeric',
			'dtimeduration' => 'required|numeric',
			'dkmsfree'      => 'required|numeric',
			'dkmsmax'       => 'required|numeric',
			'dtransmin'     => 'required|numeric',
			'dtransmax'     => 'required|numeric',
			'dinfo'         => 'required|min:10',
			'dterms'        => 'min:10',
			'handler'       => 'required|integer|min:1',
			'dfacts'        => 'min:10',
			'events'        => 'required'
		];

		$messages = [
			'dprovider.required'       => 'Please select provider',
			'dname.required'           => 'Decor name is required',
			'dworth.numeric'           => 'Please enter valid price worth (ex: 25000)',
			'dworth.min_as'            => 'Price worth should be more than minimum price',
			'dname.min'                => 'Decor name should be minimum 3 chars',
			'dminprice.required'       => 'Decor minimum price is required',
			'dminprice.numeric'        => 'Please enter valid min price (ex: 18500)',
			'dmaxprice.numeric'        => 'Please enter valid max price (ex: 27500)',
			'dmaxprice.min_as'         => 'Maximum price should be more than minimum price',
			'dtimesetup.required'      => 'Setup time is required (in hours, ex: 2.5)',
			'dtimesetup.numeric'       => 'Setup time is invalid (in hours, ex: 2.5)',
			'dtimeduration.required'   => 'Rent duration is required (in hours, ex: 4)',
			'dtimeduration.numeric'    => 'Rent durations is invalid (in hours, ex: 4)',
			'dkmsfree.required'        => 'Free KMS is required',
			'dkmsfree.numeric'         => 'Free KMS is invalid',
			'dkmsmax.required'         => 'Max KMS is required. (if n/a, enter 0)',
			'dkmsmax.numeric'          => 'Max KMS is invalid. (if n/a, enter 0)',
			'dtransmin.required'       => 'Minimum transport charges is required (if n/a, enter 0)',
			'dtransmin.numeric'        => 'Minimum transport charges is invalid (if n/a, enter 0)',
			'dtransmax.required'       => 'Maximum transport charges is required (if n/a, enter 0)',
			'dtransmax.numeric'        => 'Maximum transport charges is invalid (if n/a, enter 0)',
			'dinfo.required'           => 'Please enter all inclusions',
			'dinfo.min'                => 'Inclusions should be min. 10 chars long',
			'dterms.min'               => 'Booking terms should be min. 10 chars long',
			'dfacts.min'               => 'Decor facts should be min. 10 chars long',
			'events.required'          => 'Event supported field is required',
			'drangeinfo'               => 'Price range info is required with Max price',
			'drangeinfo.required_with' => 'Range info is required with Max price',
			'handler.required'         => 'Please select valid handler',
			'handler.integer'          => 'Handler is not a number',
			'handler.min'              => 'Handler id is invalid'
		];

		if (!(!$maxPrice || $maxPrice == 0))
		{
			$rules['dmaxprice'] = 'integer|min_as:dminprice';
			$rules['drangeinfo'] = 'required_with:dmaxprice';
		}

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$allInput = Input::all();
			$events ? $oldEvents = implode(',', $events) : $oldEvents = '';
			$allInput['events'] = $oldEvents;

			return redirect()->route('decor.new.show')
			                 ->withErrors($validation)
			                 ->withInput($allInput);
		}

		$newDecor = \Decor::create([
			                           'provider_id'   => Input::get('dprovider'),
			                           'name'          => Input::get('dname'),
			                           'url'           => self::generateDecorUrl(Input::get('dname')),
			                           'info'          => Input::get('dinfo'),
			                           'code'          => self::generateDecorCode(),
			                           'worth'         => Input::get('dworth'),
			                           'more_info'     => Input::get('dmoreinfo'),
			                           'min_price'     => $minPrice,
			                           'max_price'     => $maxPrice,
			                           'handler_id'    => Input::get('handler') > 0 ? Input::get('handler') : null,
			                           'range_info'    => Input::get('drangeinfo'),
			                           'terms'         => Input::get('dterms'),
			                           'facts'         => Input::get('dfacts'),
			                           'time_setup'    => Input::get('dtimesetup'),
			                           'time_duration' => Input::get('dtimeduration'),
			                           'kms_free'      => Input::get('dkmsfree'),
			                           'kms_max'       => Input::get('dkmsmax'),
			                           'trans_min'     => Input::get('dtransmin'),
			                           'trans_max'     => Input::get('dtransmax'),
		                           ]);

		if ($newDecor)
		{
			foreach ($events as $event)
			{
				DecorEvent::create([
					                   'event_id' => $event,
					                   'decor_id' => $newDecor->id,
				                   ]);
			}

			return redirect()->route('decor.info.view', $newDecor->id);
		}
	}

	public function updateDecorInfoDetails($decorId)
	{
		$res = ['success' => true];
		$minPrice = Input::get('min_price');
		$maxPrice = Input::get('max_price');

		$rules = [
			'name'          => 'required',
			'provider_id'   => 'required',
			'handler'       => 'required|integer|min:1',
			'worth'         => 'integer|min_as:min_price',
			'min_price'     => 'required|integer',
			'time_setup'    => 'required|numeric',
			'time_duration' => 'required|numeric',
			'kms_free'      => 'required|numeric',
			'kms_max'       => 'required|numeric',
			'trans_min'     => 'required|numeric',
			'trans_max'     => 'required|numeric',
			'info'          => 'required|min:10',
			'terms'         => 'min:10',
			'facts'         => 'min:10',
			'events'        => 'required'
		];

		$messages = [
			'provider_id.required'     => 'Provider is required',
			'name.required'            => 'Decor Name is required',
			'worth.integer'            => 'Price worth is invalid',
			'worth.min_as'             => 'Price worth should be more than minimum price',
			'min_price.required'       => 'Minimum price is required',
			'min_price.integer'        => 'Minimum price is invalid',
			'max_price.integer'        => 'Maximum price is invalid',
			'max_price.min_as'         => 'Maximum price should be more than minimum price',
			'time_setup.required'      => 'Setup time is required in hrs (ex: 2.5)',
			'time_setup.numeric'       => 'Setup time is invalid (ex: 3.5)',
			'time_duration.required'   => 'Rent duration time is required (ex: 3.5)',
			'time_duration.numeric'    => 'Rent duration time is invalid (ex: 3.5)',
			'kms_free.required'        => 'Free KMs is required (ex: 20)',
			'kms_free.numeric'         => 'Free KMs is invalid (ex: 20)',
			'kms_max.required'         => 'Max. KMs is invalid (if n/a, enter 0)',
			'kms_max.numeric'          => 'Max. KMs is invalid (if n/a, enter 0)',
			'trans_min.required'       => 'Min. transport charge is required (if n/a, enter 0)',
			'trans_min.numeric'        => 'Min. transport charge is invalid (if n/a, enter 0)',
			'trans_max.required'       => 'Max. transport charge is required (if n/a, enter 0)',
			'trans_max.numeric'        => 'Max. transport charge is invalid (if n/a, enter 0)',
			'info.required'            => 'Inclusions is required',
			'info.min'                 => 'Inclusions should be min. 10 chars',
			'terms.min'                => 'Booking terms should be min. 10 chars',
			'facts.min'                => 'Decor facts should be min. 10 chars',
			'events.required'          => 'Event supported field is required',
			'range_info.required_with' => 'Price range info is required with Max price',
			'handler.required'         => 'Please select valid handler',
			'handler.integer'          => 'Please select valid handler',
			'handler.min'              => 'Please select valid handler'
		];

		if (!(!$maxPrice || $maxPrice == 0))
		{
			$rules['max_price'] = 'integer|min_as:min_price';
			$rules['range_info'] = 'required_with:max_price';
		}
		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}
		else
		{
			$decor = \Decor::findOrFail($decorId);

			$decor->provider_id = Input::get('provider_id');
			$decor->worth = Input::get('worth') ? Input::get('worth') : 0;
			$decor->min_price = $minPrice;
			$decor->max_price = $maxPrice;
			$decor->time_setup = Input::get('time_setup');
			$decor->time_duration = Input::get('time_duration');
			$decor->kms_free = Input::get('kms_free');
			$decor->kms_max = Input::get('kms_max');
			$decor->trans_min = Input::get('trans_min') ? Input::get('trans_min') : 0;
			$decor->trans_max = Input::get('trans_max') ? Input::get('trans_max') : 0;
			$decor->info = Input::get('info');
			$decor->more_info = Input::get('more_info') ? Input::get('more_info') : '';
			$decor->range_info = Input::get('range_info') ? Input::get('range_info') : '';
			$decor->terms = Input::get('terms') ? Input::get('terms') : '';
			$decor->facts = Input::get('facts') ? Input::get('facts') : '';
			$decor->handler_id = Input::get('handler') > 0 ? Input::get('handler') : null;

			if ($decor->name != trim(Input::get('name')))
			{
				$decor->name = trim(Input::get('name'));
				$decor->url = self::generateDecorUrl(trim(Input::get('name')));
			}
			if ($decor->save())
			{

				//Flushing the Tags which is made in main based on the Code of that product
				Cache::tags($decor->code)->flush();

				DecorEvent::where('decor_id', $decorId)->forceDelete();

				foreach (Input::get('events') as $event)
				{
					DecorEvent::create([
						                   'event_id' => $event,
						                   'decor_id' => $decorId,
					                   ]);
				}
				$this->updateApprovalOnEditItem($decor);
			}
			else
			{
				$res = [
					'success' => false,
					'error'   => 'Failed to save your data, please refresh and try again.'
				];
			}
		}

		return response()->json($res);
	}

	public function uploadDecorGallery($decorId)
	{
		$type = Input::get('gal-type');
		$redirectUrl = route('decor.info.view', $decorId);

		if ($type == config('evibe.gallery.type.image'))
		{
			$images = Input::file('decorImages');
			$ignoreWatermark = is_null(Input::get('ignoreWatermark')) ? false : true;

			if (!count($images) || (count($images) && is_null($images[0])))
			{
				return redirect($redirectUrl)
					->with('imageError', 'Please select at least one image.')
					->withInput();
			}

			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:5120'
				];

				$imageMessages = [
					'file.required' => 'Please upload atleast one image or video',
					'file.mimes'    => 'One of the images/video is not valid. (only .png, .jpeg, .jpg, .mp4, .x-flv, .x-mpegURL, .MP2T, .3gpp, .quicktime, .x-msvideo, .x-ms-wmv are accepted)',
					'file.max'      => 'Image size cannot br more than 5 MB'
				];

				$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

				if ($imageValidator->fails())
				{
					return redirect($redirectUrl)
						->with('imageError', $imageValidator->messages()->first())
						->withInput();
				}
			}

			$directPath = '/decors/' . $decorId . '/images/';

			foreach ($images as $image)
			{
				$option = ['isWatermark' => false];

				if (!$ignoreWatermark)
				{
					$option = ['isWatermark' => true];
				}
				$imageName = $this->uploadImageToServer($image, $directPath, $option);

				$code = self::generateDecorGalleryCode();

				$imgInsertData = [
					'url'      => $imageName,
					'title'    => $this->getImageTitle($image),
					'decor_id' => $decorId,
					'type_id'  => config('evibe.gallery.type.image'),
					'code'     => $code
				];

				\DecorGallery::create($imgInsertData);
			}

			return redirect($redirectUrl)->with('successMsg', "Images uploaded successfully.");
		}
		elseif ($type == config('evibe.gallery.type.video'))
		{
			$video = Input::get('video');

			$rules = [
				'video' => 'required'
			];
			$message = [
				'video.required' => 'Video url is required (Ex: https://www.youtube.com/watch?v=videoId).',
			];

			$validator = validator(['video' => $video], $rules, $message);
			if ($validator->fails())
			{
				return redirect($redirectUrl)
					->with('imageError', $validator->messages()->first())
					->withInput();
			}

			$videoId = $this->getYouTubeVideoId($video);
			if (!$videoId)
			{
				return redirect()->back()->with('imageError', 'Please enter an valid youtube url')->withInput();
			}

			\DecorGallery::create([
				                      'decor_id' => $decorId,
				                      'type_id'  => $type,
				                      'url'      => $videoId
			                      ]);

			return redirect($redirectUrl)->with(['successMsg' => "Video uploaded successfully.", 'type' => 'video']);
		}
	}

	public function setAsProfile($decorId, $galleryId)
	{
		$decorGallery = \DecorGallery::findOrFail($galleryId);
		$decorGallery->is_profile = 1;
		$decorGallery->save(); // test comment.

		return redirect()->route('decor.info.view', $decorId);
	}

	public function unsetAsProfile($decorId, $galleryId)
	{
		$decorGallery = \DecorGallery::findOrFail($galleryId);
		$decorGallery->is_profile = 0;
		$decorGallery->save();

		return redirect()->route('decor.info.view', $decorId);
	}

	public function saveImgTitle($decorId, $galleryId)
	{
		$decorGallery = \DecorGallery::findOrFail($galleryId);
		$galleryTitle = Input::get('title');

		if ($galleryTitle)
		{
			$decorGallery->title = $galleryTitle;
			$decorGallery->save();

			return redirect()->route('decor.info.view', $decorId);
		}
	}

	public function deleteDecorGallery($decorId, $galleryId)
	{
		$gallery = \DecorGallery::find($galleryId);

		if ($gallery && $gallery->delete())
		{
			return redirect()->route('decor.info.view', $decorId);
		}
	}

	public function addDecorTag($decorId)
	{
		$parentTagId = Input::get('selectParentTag');
		$childTagId = Input::get('selectChildTag');
		$tagId = $parentTagId;

		if ($childTagId != -1)
		{
			$tagId = $childTagId;
		}

		if ($tagId != -1)
		{
			$existingTag = \DecorTags::where(['decor_id' => $decorId, 'tag_id' => $tagId])->count();
			if ($existingTag == 0)
			{
				\DecorTags::create(['decor_id' => $decorId, 'tag_id' => $tagId]);
			}
		}

		return redirect()->route('decor.info.view', $decorId);
	}

	public function deleteDecorTag($decorId, $tagId)
	{
		$decorTag = \DecorTags::where('decor_id', $decorId)
		                      ->where('tag_id', $tagId)
		                      ->first();
		$decorTag->delete();

		return redirect()->route('decor.info.view', $decorId);
	}

	public function activateDecor($decorId)
	{
		$decor = \Decor::findOrFail($decorId);

		$event = $decor->events->first();
		if ($event)
		{
			$decor->update(['is_live' => 1]);
			$this->checkAndSendFirstActivationEmail($event->id, config('evibe.ticket_type.decors'), $decor);
		}
		else
		{
			return redirect()->back()->with('customError', 'Please tag some event before making live');
		}

		return redirect()->back();
	}

	public function deactivateDecor($decorId)
	{
		$decor = \Decor::findOrFail($decorId);
		$decor->is_live = 0;

		if ($decor->save())
		{
			Cache::tags($decor->code)->flush();
			$this->removeItemsFromAutoBookAndPriorityTable(config('evibe.ticket_type.decors'), $decorId);

			return redirect()->route('decor.info.view', $decorId);
		}
	}

	public function deleteDecor($decorId)
	{
		$decor = \Decor::findOrFail($decorId);

		if ($decor->delete())
		{
			Cache::tags($decor->code)->flush();
			return redirect()->route('decors.list')
			                 ->with('customSuccessMessage', 'Decor style has been successfully deleted');
		}
	}
}