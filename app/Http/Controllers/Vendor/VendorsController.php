<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Base\BaseVendorController;
use App\Models\Partner\PartnerDocument;
use App\Models\VendorFollowup;
use App\Models\VendorUpdate;
use App\Models\TypeVendorStatus;
use App\Models\User;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VendorsController extends BaseVendorController
{
	public function showList()
	{
		$query = Input::get('query');
		$typeNames = [];
		$vendors = \Vendor::whereNull('deleted_at')->orderBy('created_at', 'DESC');

		if ($query)
		{
			$vendors = $vendors->where(function ($q) use ($query) {
				$q->where('name', 'LIKE', '%' . $query . '%')
				  ->orWhere('phone', 'LIKE', '%' . $query . '%')
				  ->orWhere('person', 'LIKE', '%' . $query . '%')
				  ->orWhere('email', 'LIKE', '%' . $query . '%')
				  ->orWhere('code', 'LIKE', '%' . $query . '%');
			});
		}

		$cityId = Input::get('city', 0);
		if ($cityId)
		{
			$vendors = $vendors->where('city_id', $cityId);
		}

		// area filter
		$areaId = Input::has('area') ? Input::get('area') : null;
		if ($areaId && $areaId > 0)
		{
			$vendors = $vendors->where('area_id', $areaId);
		}

		//filter by status
		$allStatus = TypeVendorStatus::where('is_existing', 1)->get();
		$status = Input::get('status');
		if ($status)
		{
			//filter by status
			$statusFilter = Input::get('status');
			if ($statusFilter)
			{
				$statusTypeId = '';
				foreach ($allStatus as $status)
				{
					if ($status->name == $statusFilter)
					{
						$statusTypeId = $status->id;
						break;
					}
				}
				$vendors->where('status_id', $statusTypeId);
			}
		}

		// select page count
		$pageCount = Input::has('count') ? (int)Input::get('count') : 20;
		$vendors->take($pageCount);

		//filter by type
		$type = Input::get('type');
		if ($type)
		{
			$typeNames = explode(',', $type);
			$typeIds = \TypeVendor::whereIn('name', $typeNames)->pluck('id');

			$vendors->whereIn('type_id', $typeIds);
		}

		$vendorTypes = \TypeVendor::all();

		$vendors = $vendors->paginate($pageCount);

		$data = [
			'vendors'     => $vendors,
			'vendorTypes' => $vendorTypes,
			'typeNames'   => $typeNames,
			'allStatus'   => $allStatus,
			'cities'      => \City::all(),
			'areas'       => \Area::select("id", "name")->where('city_id', $cityId)->get()
		];

		return view('vendors.existing.list', ['data' => $data]);
	}

	// Show all details of a vendor
	public function showDetails($vendorId)
	{
		$vendor = \Vendor::findOrFail($vendorId);
		$vendorSignupId = $vendor->signup_id;

		$vendorUpdate = VendorUpdate::where('map_type_id', config('evibe.ticket_type.planners'))
		                            ->where(function ($query) use ($vendorSignupId, $vendorId) {
			                            $query->where('map_id', $vendorId);
			                            if (!is_null($vendorSignupId))
			                            {
				                            $query->orWhere('vendor_signup_id', $vendorSignupId);
			                            }
		                            })->orderBy('status_at', 'DESC')
		                            ->get();

		$followupCollection = VendorFollowup::where('map_id', $vendorId)
		                                    ->where('map_type_id', config('evibe.ticket_type.planners'))
		                                    ->orwhere(function ($query) use ($vendorSignupId) {
			                                    $query->orWhere('vendor_signup_id', $vendorSignupId);
		                                    })->get();

		$activeFollowups = $followupCollection->filter(function ($query) {
			return $query->is_complete == null;
		});

		$archiveFollowups = $followupCollection->filter(function ($query) {
			return $query->is_complete == 1;
		});

		$documents = PartnerDocument::where('partner_id', '=', $vendorId)
		                            ->get();

		$data = [
			'vendor'           => $vendor,
			'vendorUpdate'     => $vendorUpdate,
			'activeFollowups'  => $activeFollowups,
			'archiveFollowups' => $archiveFollowups,
			'handlers'         => User::where('role_id', '5')->get(),
			'statusUpdate'     => TypeVendorStatus::where('is_existing', 1)->get(),
			'documents'        => $documents,
			'vendorId'         => $vendorId,
		];

		return view('vendors.existing.details')->with($data);
	}

	public function deleteDocument($documentId)
	{
		PartnerDocument::where('id', '=', $documentId)->delete();

		return redirect()->back();
	}

	// Show add new vendor form
	public function showAddNewForm()
	{
		$handlers = User::whereIn('role_id', [config('evibe.roles.bd'), config('evibe.roles.sr_bd'), config('evibe.roles.operations')])->get();
		$data = [
			'vendorTypes' => \TypeVendor::all(),
			'cities'      => \City::all(),
			'areas'       => \Area::orderBy('name')->get(),
			'handlers'    => $handlers,
			'area'        => \Area::all()
		];

		return view('vendors/add', ['data' => $data]);
	}

	// Show edit vendor form
	public function showEditForm($vendorId)
	{
		$vendor = \Vendor::find($vendorId);
		if (!$vendor)
		{
			return redirect()->back();
		}
		$userInfo = User::find($vendor->user_id);
		if (!$vendor)
		{
			return redirect()->back();
		}
		$handlers = User::whereIn('role_id', [config('evibe.roles.bd'), config('evibe.roles.sr_bd')])->get();

		$data = [
			'vendorTypes' => \TypeVendor::all(),
			'source'      => \TypeTicketSource::all(),
			'leadStatus'  => \TypeLeadStatus::all(),
			'cities'      => \City::all(),
			'areas'       => \Area::orderBy('name')->get(),
			'handlers'    => $handlers,
			'vendor'      => $vendor,
			'userInfo'    => $userInfo
		];

		return view('vendors.existing.edit', ['data' => $data]);
	}

	// Edit vendor information
	public function saveVendorEdit($vendorId)
	{
		$rules = [
			'vendorName'           => 'required|unique:planner,name,' . $vendorId,
			'vendorType'           => 'required',
			'vendorPerson'         => 'required',
			'vendorPhone'          => 'required|digits:10',
			'vendorSubLocation'    => 'min:5',
			'vendorZipCode'        => 'required|integer|digits:6',
			'vendorShortURL'       => 'min:4|unique:planner,short_url,' . $vendorId,
			'vendorCommissionRate' => 'required|numeric|min:8',
			'altEmail1'            => 'email',
			'altEmail2'            => 'email',
			'altPhone'             => 'phone'
		];

		$messages = [
			'vendorName.required'           => 'Please enter company name',
			'vendorName.unique'             => 'This vendor name is already taken. Try appending location.',
			'vendorPerson.required'         => 'Please enter contact person name',
			'vendorPhone.required'          => 'Please enter phone number',
			'vendorPhone.phone'             => 'Please enter valid phone number',
			'vendorSubLocation.min'         => 'Sub Location (landmark) must be minimum 4 characters long',
			'vendorShortURL.min'            => 'Short URL should be minimum 4 characters long',
			'vendorShortURL.unique'         => 'This short URL is already taken, please try something else',
			'vendorCommissionRate.required' => 'Commission % is required',
			'vendorCommissionRate.numeric'  => 'Commission % is invalid',
			'vendorCommissionRate.min'      => 'Commission % cannot be that low',
			'altEmail1.email'               => 'Alternative email 1 must be a valid email address',
			'altEmail2.email'               => 'Alternative email 2 must be a valid email address',
			'altPhone.phone'                => 'Alternative phone must be a valid 10 digit phone number',
			'vendorZipCode.required'        => 'please enter the ZIP Code of the partner',
			'vendorZipCode.digits'          => 'please enter a valid ZIP Code',
			'vendorZipCode.integer'         => 'please enter a valid ZIP Code'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->route('vendor.edit.show', $vendorId)->withErrors($validator)->withInput();
		}
		else
		{
			$vendor = \Vendor::with('user')->findOrFail($vendorId);
			$vendorType = Input::get('vendorType');
			$roleId = Input::get('roleId');

			// Update role id
			if ($roleId != $vendor->user->role_id)
			{
				$user = $vendor->user;
				$user->role_id = $roleId;
				$user->updated_at = date('Y-m-d H:i:s');
				$user->save();
			}

			$cityId = Input::get('city');
			$areaId = Input::get('area');

			// change url if vendor type / area has changed
			if ($vendor->type_id != $vendorType || $vendor->area_id != $areaId)
			{
				$vendor->url = $this->generateVendorUrl($vendorType, $areaId);
			}

			// change code only if type changed
			if ($vendor->type_id != $vendorType || $vendor->city_id != $cityId)
			{
				$vendor->code = $this->generateVendorCode($cityId, $vendorType);
			}

			$vendor->name = Input::get('vendorName');
			$vendor->person = Input::get('vendorPerson');
			$vendor->phone = Input::get('vendorPhone');
			$vendor->handler_id = Input::get('handler');
			$vendor->sub_location = Input::get('vendorSubLocation');
			$vendor->short_url = Input::get('vendorShortURL');
			$vendor->commission_rate = Input::get('vendorCommissionRate');
			$vendor->alt_email_1 = Input::get('altEmail1');
			$vendor->alt_email_2 = Input::get('altEmail2');
			$vendor->alt_phone = Input::get('altPhone');
			$vendor->city_id = $cityId;
			$vendor->area_id = $areaId;
			$vendor->type_id = $vendorType;
			$vendor->updated_at = date('Y-m-d H:i:s');
			$vendor->source_id = Input::get('source');
			$vendor->lead_status_id = Input::get('leadStatus');
			$vendor->source_specific = Input::get('sourceSpecific');
			$vendor->facebook_url = Input::get('sourceFacebook');
			$vendor->google_plus_url = Input::get('sourceGooglePlus');
			$vendor->instagram_url = Input::get('sourceInstagram');
			$vendor->comments = Input::get('comments');
			$vendor->zip = Input::get('vendorZipCode');
			$vendor->save();

			return redirect()->route('vendor.view', $vendorId);
		}
	}

	// Delete a vendor
	public function deleteVendor($vendorId)
	{
		$vendor = \Vendor::findOrFail($vendorId);

		// Make vendor not active
		$vendor->update([
			                'is_live'    => 0,
			                'status_id'  => config('evibe.vendor.status.non_live'),
			                'updated_at' => date('Y-m-d H:i:s')
		                ]);

		return redirect('/vendors?delete=true');
	}

	//save vendor update
	public function saveExitingVendorUpdate($vendorId)
	{
		$vendor = \Vendor::find($vendorId);

		return $this->saveVendorUpdate($vendor);
	}

	//save vendor followup
	public function saveExistingVendorFollowup($vendorId)
	{
		$vendor = \Vendor::find($vendorId);

		return $this->saveFollowup($vendor);
	}

	public function followupMarkComplete($followupId)
	{
		return $this->completeFollowup($followupId);
	}

	// Actions
	public function activateVendor($vendorId)
	{
		$vendor = \Vendor::findOrFail($vendorId);
		if ($vendor && $vendor->update(['is_live' => 1, 'status_id' => config('evibe.vendor.status.live')]))
		{
			return redirect()->route('vendor.view', $vendorId);
		}

		return redirect()->route('vendors.list');
	}

	public function deActivateVendor($vendorId)
	{
		$vendor = \Vendor::findOrFail($vendorId);
		if ($vendor && $vendor->update(['is_live' => 0, 'status_id' => config('evibe.vendor.status.non_live')]))
		{
			return redirect()->route('vendor.view', $vendorId);
		}

		return redirect()->route('vendors.list');
	}

	public function showFile()
	{
		return redirect()->route('image.show');
	}
}