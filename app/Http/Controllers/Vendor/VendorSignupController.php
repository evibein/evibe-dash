<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Base\BaseVendorController;
use App\Http\Controllers\Venue\VenuesController;
use App\Models\VendorFollowup;
use App\Models\VendorUpdate;
use App\Models\TypeVendorStatus;
use App\Models\User;

use App\Models\VendorSignup;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VendorSignupController extends BaseVendorController
{
	private $venueController;

	public function __construct(VenuesController $venuesController)
	{
		$this->venueController = $venuesController;
	}

	public function showNewVendorList()
	{
		$typeNames = [];
		$typeVenueNames = [];
		$query = Input::get('query');
		$vendors = VendorSignup::whereNull('deleted_at');

		//search query
		if ($query)
		{
			$vendors = $vendors->where(function ($q) use ($query) {
				$q->where('company_name', 'LIKE', '%' . $query . '%')
				  ->orWhere('phone', 'LIKE', '%' . $query . '%')
				  ->orWhere('person', 'LIKE', '%' . $query . '%')
				  ->orWhere('email', 'LIKE', '%' . $query . '%');
			});
		}

		// city filter
		$cityId = Input::get('city', 0);
		if ($cityId)
		{
			$vendors = $vendors->where('city_id', $cityId);
		}

		// area filter
		$areaId = Input::has('area') ? Input::get('area') : null;
		if ($areaId && $areaId > 0)
		{
			$vendors = $vendors->where('area_id', $areaId);
		}

		//sort filter
		$sortColumn = 'created_at';
		$sortType = 'DESC';
		$sortOptions = Input::get('sort');
		if ($sortOptions)
		{
			switch ($sortOptions)
			{
				case 'created-first':
					$sortType = 'DESC';
					$sortColumn = 'created_at';
					break;
				case 'updated-first':
					$sortType = 'DESC';
					$sortColumn = 'updated_at';
					break;
				case 'created-last':
					$sortType = 'ASC';
					$sortColumn = 'created_at';
					break;
				case 'updated-last':
					$sortType = 'ASC';
					$sortColumn = 'updated_at';
					break;
				default:
					$sortType = 'DESC';
					$sortColumn = 'created_at';
			}
		}

		// select page count
		$pageCount = Input::has('count') ? (int)Input::get('count') : 20;
		$vendors->take($pageCount);

		// filter by status
		$allStatus = TypeVendorStatus::where('is_existing', 0)->get();
		$statusFilter = Input::get('status');
		if ($statusFilter)
		{
			$statusTypeId = '';
			foreach ($allStatus as $status)
			{
				if ($status->name == $statusFilter)
				{
					$statusTypeId = $status->id;
					break;
				}
			}
			$vendors->where('status_id', $statusTypeId);
		}
		$leadStatusFilter = Input::get('leadStatus');
		if ($leadStatusFilter)
		{
			$leadStatusId = -1;
			switch ($leadStatusFilter)
			{
				case 'hot':
					$leadStatusId = 1;
					break;
				case 'medium':
					$leadStatusId = 2;
					break;
				case 'cold':
					$leadStatusId = 3;
					break;
			}
			$vendors->where('lead_status_id', $leadStatusId);
		}

		// filter by vendorType;
		$vendorTypes = \TypeVendor::all();
		$venueTypes = \TypeVenue::all();

		$typeFilter = Input::get('type');
		$typeVenueFilter = Input::get('venueType');

		if ($typeFilter && $typeVenueFilter)
		{
			$typeNames = explode(",", $typeFilter);
			$typeVenueNames = explode(",", $typeVenueFilter);
			$typeIds = \TypeVendor::whereIn('name', $typeNames)->pluck('id')->toArray();
			$typeVenueIds = \TypeVenue::whereIn('name', $typeVenueNames)->pluck('id')->toArray();

			$allTypeId = array_merge($typeIds, $typeVenueIds);
			$uniqueId = array_unique($allTypeId);
			$vendors->whereIn('type_id', $uniqueId);
		}
		else
		{
			if ($typeFilter)
			{
				$typeNames = explode(",", $typeFilter);
				$typeIds = \TypeVendor::whereIn('name', $typeNames)->pluck('id');
				$vendors->whereIn('type_id', $typeIds)->where('is_venue', 0);
			}

			if ($typeVenueFilter)
			{
				$typeVenueNames = explode(",", $typeVenueFilter);
				$typeVenueIds = \TypeVenue::whereIn('name', $typeVenueNames)->pluck('id');
				$vendors->whereIn('type_id', $typeVenueIds)->where('is_venue', 1);
			}
		}

		$vendors->orderBy($sortColumn, $sortType);
		$vendors = $vendors->paginate($pageCount);

		$data = [
			'vendors'        => $vendors,
			'statuses'       => $allStatus,
			'vendorTypes'    => $vendorTypes,
			'venueTypes'     => $venueTypes,
			'typeVenueNames' => $typeVenueNames,
			'typeNames'      => $typeNames,
			'cities'         => \City::all(),
			'areas'          => \Area::select("id", "name")->where('city_id', $cityId)->get()
		];

		return view('vendors/new/list', ['data' => $data]);
	}

	// details page
	public function showNewVendorDetails($vendorId)
	{
		$vendor = VendorSignup::find($vendorId);
		if (!$vendor)
		{
			return redirect()->back();
		}
		$vendorUpdate = VendorUpdate::where('vendor_signup_id', $vendorId)
		                            ->whereNotNull('vendor_signup_id')
		                            ->get();
		$allStatus = TypeVendorStatus::where('is_existing', 0)->get();
		$followupCollection = VendorFollowup::where('map_type_id', config('evibe.ticket_type.planners'))
		                                    ->where('vendor_signup_id', $vendorId)
		                                    ->whereNull('map_id')
		                                    ->get();

		$activeFollowups = $followupCollection->filter(function ($query) {
			return $query->is_complete == null;
		});

		$archiveFollowups = $followupCollection->filter(function ($query) {
			return $query->is_complete == 1;
		});

		$data = [
			'vendor'           => $vendor,
			'vendorUpdate'     => $vendorUpdate,
			'activeFollowups'  => $activeFollowups,
			'archiveFollowups' => $archiveFollowups,
			'allStatus'        => $allStatus
		];

		return view('vendors.new.details')->with($data);
	}

	public function showAddNewForm()
	{
		$handlers = User::whereIn('role_id', [config('evibe.roles.bd'), config('evibe.roles.sr_bd'), config('evibe.roles.operations')])->get();
		$data = [
			'source'      => \TypeTicketSource::all(),
			'leadStatus'  => \TypeLeadStatus::all(),
			'vendorTypes' => \TypeVendor::all(),
			'venueTypes'  => \TypeVenue::all(),
			'cities'      => \City::all(),
			'areas'       => \Area::orderBy('name')->get(),
			'handlers'    => $handlers,
			'area'        => \Area::all()
		];

		return view('vendors.new.add', ['data' => $data]);
	}

	// Save vendor information
	public function saveNewVendor()
	{
		$userInput = Input::all();
		$isVenue = Input::get('type');

		$rules = [
			'vendorName'    => 'required|unique:vendor_signup,company_name',
			'vendorPerson'  => 'required',
			'vendorPhone'   => 'required_without:vendorEmail|phone|digits:10',
			'vendorEmail'   => 'required_without:vendorPhone|email',
			'altEmail1'     => 'email',
			'altEmail2'     => 'email',
			'altPhone'      => 'phone|required',
			'vendorZipCode' => 'required|integer|digits:6',
			'source'        => 'required|integer|min:1',
			'leadStatus'    => 'required|integer|min:1'
		];

		$messages = [
			'vendorName.required'          => 'Please enter company name (if individual, enter contact person)',
			'vendorName.unique'            => 'This vendor name is already taken.',
			'vendorPerson.required'        => 'Please enter contact person name',
			'vendorPhone.required_without' => 'Please enter at-least one from phone number and email id',
			'vendorPhone.phone'            => 'Please enter valid phone number',
			'vendorEmail.required_without' => 'Please enter at-least one from email id and phone number',
			'vendorEmail.email'            => 'Please enter a valid email address',
			'altEmail1.email'              => 'Alternative email 1 must be a valid email address',
			'altEmail2.email'              => 'Alternative email 2 must be a valid email address',
			'altPhone.phone'               => 'Alternative phone must be a valid 10 digit phone number',
			'source.required'              => 'Please select a source',
			'source.min'                   => 'Please select a source',
			'leadStatus.required'          => 'please select the lead status',
			'leadStatus.min'               => 'please select the lead status',
			'vendorZipCode.required'       => 'please enter the ZIP Code of the partner',
			'vendorZipCode.digits'         => 'please enter a valid ZIP Code',
			'vendorZipCode.integer'        => 'please enter a valid ZIP Code'
		];

		$longitude = null;
		$latitude = null;

		if ($isVenue)
		{
			$rules['venueType'] = 'required';
			$longitude = Input::get('vMappingLongitude') ? Input::get('vMappingLongitude') : null;
			$latitude = Input::get('vMappingLatitude') ? Input::get('vMappingLatitude') : null;
		}
		else
		{
			$rules['vendorType'] = 'required';
		}
		$validator = Validator::make($userInput, $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}
		else
		{
			$isArtist = 0;
			$vendorType = Input::get('vendorType');
			$cityId = Input::get('city');
			$areaId = Input::get('area');
			$landmark = Input::get('vendorSubLocation');

			//check isArtist if vendor is not venue type
			if ($isVenue == 0)
			{
				$artistVendorTypes = \TypeVendor::where('role_id', config('evibe.roles.artist'))->pluck('id');

				if (in_array($vendorType, $artistVendorTypes->toArray()))
				{
					$isArtist = 1;
				}
			}
			elseif ($isVenue == 1)
			{
				$vendorType = Input::get('venueType');
			}

			$vendorInsertData = [
				'company_name'    => Input::get('vendorName'),
				'person'          => Input::get('vendorPerson'),
				'phone'           => Input::get('vendorPhone'),
				'email'           => Input::get('vendorEmail'),
				'handler_id'      => Input::get('handler'),
				'city_id'         => $cityId,
				'area_id'         => $areaId,
				'is_artist'       => $isArtist,
				'is_confirm'      => 0,
				'status_id'       => 1,
				'is_venue'        => $isVenue,
				'sub_location'    => $landmark,
				'alt_email_1'     => Input::get('altEmail1'),
				'alt_email_2'     => Input::get('altEmail2'),
				'alt_phone'       => Input::get('altPhone'),
				'type_id'         => $vendorType,
				'created_at'      => date('Y-m-d H:i:s'),
				'updated_at'      => date('Y-m-d H:i:s'),
				'source_id'       => Input::get('source'),
				'source_specific' => Input::get('sourceSpecific'),
				'facebook_url'    => Input::get('sourceFacebook'),
				'google_plus_url' => Input::get('sourceGooglePlus'),
				'instagram_url'   => Input::get('sourceInstagram'),
				'lead_status_id'  => Input::get('leadStatus'),
				'comments'        => Input::get('comments'),
				'zip'             => Input::get('vendorZipCode'),
				'map_long'        => $longitude,
				'map_lat'         => $latitude
			];

			VendorSignup::create($vendorInsertData);

			return redirect()->to(route('vendor.new.list'));
		}
	}

	public function showEditNewVendor($vendorId)
	{
		$vendor = VendorSignup::findOrFail($vendorId);
		$handlers = User::whereIn('role_id', [config('evibe.roles.bd'), config('evibe.roles.sr_bd')])->get();

		$data = [
			'vendorTypes' => \TypeVendor::all(),
			'source'      => \TypeTicketSource::all(),
			'leadStatus'  => \TypeLeadStatus::all(),
			'venueTypes'  => \TypeVenue::all(),
			'cities'      => \City::all(),
			'areas'       => \Area::orderBy('name')->get(),
			'handlers'    => $handlers,
			'vendor'      => $vendor,
		];

		return view('vendors.new.edit', ['data' => $data]);
	}

	public function updateNewVendor($vendorId)
	{
		$vendor = VendorSignup::find($vendorId);
		if (!$vendor)
		{
			return redirect()->back();
		}
		$isArtist = 0;
		$isVenue = Input::get('type');

		$rules = [
			'vendorName'    => 'required|unique:vendor_signup,company_name,' . $vendorId,
			'vendorPerson'  => 'required',
			'vendorPhone'   => 'required_without:vendorEmail|phone|digits:10',
			'vendorEmail'   => 'required_without:vendorPhone|email',
			'handler'       => 'required',
			'altEmail1'     => 'email',
			'altEmail2'     => 'email',
			'altPhone'      => 'phone',
			'vendorZipCode' => 'required|integer|digits:6',
			'source'        => 'required|integer|min:1',
			'leadStatus'    => 'required|integer|min:1'
		];

		$messages = [
			'vendorName.required'          => 'Please enter company name (if individual, enter contact person)',
			'vendorName.unique'            => 'This vendor name is already taken.',
			'vendorPerson.required'        => 'Please enter contact person name',
			'vendorPhone.required_without' => 'Please enter at-least one from phone number and email id',
			'vendorPhone.phone'            => 'Please enter valid phone number',
			'vendorEmail.required_without' => 'Please enter at-least one from email id and phone number',
			'vendorEmail.email'            => 'Please enter a valid email address',
			'handler.required'             => 'Please select a handler',
			'altEmail1.email'              => 'Alternative email 1 must be a valid email address',
			'altEmail2.email'              => 'Alternative email 2 must be a valid email address',
			'altPhone.phone'               => 'Alternative phone must be a valid 10 digit phone number',
			'source.required'              => 'Please select a source',
			'source.min'                   => 'Please select a source',
			'leadStatus.required'          => 'please select the lead status',
			'leadStatus.min'               => 'please select the lead status',
			'vendorZipCode.required'       => 'please enter the ZIP Code of the partner',
			'vendorZipCode.digits'         => 'please enter a valid ZIP Code',
			'vendorZipCode.integer'        => 'please enter a valid ZIP Code'
		];

		$latitude = null;
		$longitude = null;

		if ($isVenue)
		{
			$rules['venueType'] = 'required';
			$longitude = Input::get('vMappingLongitude') ? Input::get('vMappingLongitude') : null;
			$latitude = Input::get('vMappingLatitude') ? Input::get('vMappingLatitude') : null;
		}
		else
		{
			$rules['vendorType'] = 'required';
		}

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}
		else
		{

			$vendorType = Input::get('vendorType');
			$cityId = Input::get('city');
			$areaId = Input::get('area');
			$landmark = Input::get('vendorSubLocation');

			if ($isVenue == 0)
			{
				$artistVendorTypes = \TypeVendor::where('role_id', config('evibe.roles.artist'))->pluck('id');

				if (in_array($vendorType, $artistVendorTypes->toArray()))
				{
					$isArtist = 1;
				}
			}
			elseif ($isVenue == 1)
			{
				$vendorType = Input::get('venueType');
			}

			$vendorUpdateData = [
				'company_name'    => Input::get('vendorName'),
				'person'          => Input::get('vendorPerson'),
				'phone'           => Input::get('vendorPhone'),
				'email'           => Input::get('vendorEmail'),
				'handler_id'      => Input::get('handler'),
				'city_id'         => $cityId,
				'alt_email_1'     => Input::get('altEmail1'),
				'alt_email_2'     => Input::get('altEmail2'),
				'alt_phone'       => Input::get('altPhone'),
				'area_id'         => $areaId,
				'is_venue'        => $isVenue,
				'is_artist'       => $isArtist,
				'sub_location'    => $landmark,
				'type_id'         => $vendorType,
				'source_id'       => Input::get('source'),
				'source_specific' => Input::get('sourceSpecific'),
				'facebook_url'    => Input::get('sourceFacebook'),
				'google_plus_url' => Input::get('sourceGooglePlus'),
				'instagram_url'   => Input::get('sourceInstagram'),
				'lead_status_id'  => Input::get('leadStatus'),
				'comments'        => Input::get('comments'),
				'zip'             => Input::get('vendorZipCode'),
				'map_long'        => $longitude,
				'map_lat'         => $latitude
			];

			$vendor->update($vendorUpdateData);

			return redirect()->route('vendor.new.view', $vendorId);
		}
	}

	public function deleteNewVendor($vendorId)
	{
		$vendor = VendorSignup::findOrFail($vendorId);

		// Make vendor not active
		$vendor->delete();

		return redirect()->route('vendor.new.list');
	}

	/** Edit lead status details */
	public function editLeadStatus($vendorId)
	{
		$vendor = VendorSignup::find($vendorId);
		$leadStatusId = request('leadStatus', null);
		$validLeadStatusIds = \TypeLeadStatus::all()->pluck("id")->toArray();
		$errorMessage = null;

		if (!$vendor)
		{
			$errorMessage = "Invalid ticket id";
		}
		elseif (is_null($leadStatusId))
		{
			$errorMessage = "Lead status id cannot be null";
		}
		elseif (!in_array($leadStatusId, $validLeadStatusIds))
		{
			$errorMessage = "Invalid lead status id";
		}
		else
		{
			// do nothing
		}

		if (!is_null($errorMessage))
		{
			return response()->json([
				                        "success" => false,
				                        "error"   => $errorMessage
			                        ]);
		}

		$vendor->update(['lead_status_id' => $leadStatusId]);

		$res = [
			'success' => true,
			'value'   => $vendor->leadStatus->name
		];

		return response()->json($res);
	}

	// confirm the new vendor:
	//      + will push the code in `planner` (or) `venue` table and
	//      + delete from `vendor_signup` table
	public function confirmVendor($newVendorId)
	{
		$type = Input::get('type');
		$res = ['success' => true];

		$rules = [
			'email'                 => 'required|email',
			'password'              => 'required|min:6|confirmed',
			'password_confirmation' => 'required',
			'commission'            => 'required|numeric|min:8'
		];

		$messages = [
			'email.required'                 => 'Email is required',
			'email.email'                    => 'Please enter a valid email',
			'password.required'              => 'Password filed is mandatory',
			'password.min'                   => 'Password should be minimum 6 digits long',
			'password.confirmed'             => "Password didn't matched",
			'password_confirmation.required' => 'Password confirmation filed is required',
			'commission.required'            => 'Commission rate is required',
			'commission.numeric'             => 'Commission rate must be number',
			'commission.min'                 => 'Commission rate cannot be less than 8 %',
		];

		$venue = \Venue::where('email', request('email'))->first();

		if ($venue)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => "Email Id already exists for an existing venue partner"
			                        ]);
		}

		$planner = \Vendor::where('email', request('email'))->first();

		if ($planner)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => "Email Id already exists for an existing non-venue partner"
			                        ]);
		}

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validation->messages()->first()
			                        ]);
		}

		$newVendor = VendorSignup::find($newVendorId);
		$email = Input::get('email');
		$password = Hash::make(trim(Input::get('password')));
		$cityId = $newVendor->city_id;
		$areaId = $newVendor->area_id;
		$typeId = $newVendor->type_id;

		if ($type == 1)
		{
			$roleId = config('evibe.roles.venue');
		}
		else
		{
			$roleId = \TypeVendor::find($typeId)->role->id;
		}

		$newUser = User::updateOrCreate([
			                                'username' => $email
		                                ],
		                                [
			                                'name'       => $newVendor->person,
			                                'phone'      => $newVendor->phone,
			                                'password'   => $password,
			                                'role_id'    => $roleId,
			                                'short_url'  => $this->generateShortUrl($newVendor->name, $newVendor->id),
			                                'created_at' => date('Y-m-d H:i:s'),
			                                'updated_at' => date('Y-m-d H:i:s')
		                                ]);
		$commission = Input::get('commission');

		try
		{
			if ($type == 0)
			{
				$vendorCode = $this->generateVendorCode($cityId, $typeId);

				$vendorInsertData = [
					'name'            => $newVendor->company_name,
					'person'          => $newVendor->person,
					'phone'           => $newVendor->phone,
					'email'           => $email,
					'handler_id'      => $newVendor->handler_id,
					'is_live'         => 0,
					'status_id'       => config('evibe.vendor.status.non_live'),
					'user_id'         => $newUser->id,
					'code'            => $vendorCode,
					'city_id'         => $cityId,
					'area_id'         => $areaId,
					'is_artist'       => $newVendor->is_artist,
					'url'             => $this->generateVendorUrl($typeId, $areaId),
					'sub_location'    => $newVendor->sub_location,
					'signup_id'       => $newVendor->id,
					'type_id'         => $typeId,
					'alt_email_1'     => $newVendor->alt_email_1,
					'alt_email_2'     => $newVendor->alt_email_2,
					'alt_phone'       => $newVendor->alt_phone,
					'commission_rate' => trim($commission),
					'created_at'      => date('Y-m-d H:i:s'),
					'updated_at'      => date('Y-m-d H:i:s'),
					'source_specific' => $newVendor->source_specific,
					'facebook_url'    => $newVendor->facebook_url,
					'google_plus_url' => $newVendor->google_plus_url,
					'instagram_url'   => $newVendor->instagram_url,
					'comments'        => $newVendor->comments,
					'source_id'       => $newVendor->source_id,
					'lead_status_id'  => $newVendor->lead_status_id,
					'zip'             => $newVendor->zip
				];

				$saveVendor = \Vendor::create($vendorInsertData);
			}
			elseif ($type == 1)
			{
				if ((is_null($newVendor->map_long) || is_null($newVendor->map_lat)))
				{
					$res = ['success' => false,
					        'error'   => "Longitude and latitude of the venue is required ,<a href=" . config('evibe.host') . "vendors/new/edit/" . $newVendorId . ">Click here to edit</a>"];

					return response()->json($res);
				}
				$venueCode = $this->venueController->generateVenueCode($typeId);
				$venueInsertData = [

					'name'            => $newVendor->company_name,
					'person'          => $newVendor->person,
					'phone'           => $newVendor->phone,
					'email'           => $email,
					'user_id'         => $newUser->id,
					'code'            => $venueCode,
					'city_id'         => $cityId,
					'area_id'         => $areaId,
					'url'             => $this->venueController->generateVenueUrl($typeId, $areaId),
					'type_id'         => $typeId,
					'alt_email_1'     => $newVendor->alt_email_1,
					'alt_email_2'     => $newVendor->alt_email_2,
					'alt_phone'       => $newVendor->alt_phone,
					'is_partner'      => 0,
					'is_verified'     => 0,
					'created_at'      => date('Y-m-d H:i:s'),
					'updated_at'      => date('Y-m-d H:i:s'),
					'source_specific' => $newVendor->source_specific,
					'facebook_url'    => $newVendor->facebook_url,
					'google_plus_url' => $newVendor->google_plus_url,
					'instagram_url'   => $newVendor->instagram_url,
					'comments'        => $newVendor->comments,
					'source_id'       => $newVendor->source_id,
					'lead_status_id'  => $newVendor->lead_status_id,
					'commission_rate' => trim($commission),
					'zip'             => $newVendor->zip,
					'map_long'        => $newVendor->map_long,
					'map_lat'         => $newVendor->map_lat
				];

				$saveVendor = \Venue::create($venueInsertData);
			}
			else
			{
				$saveVendor = false;
			}

			if ($saveVendor)
			{
				$updateData = ['is_confirm' => 1];
				if (!$newVendor->email == $email)
				{
					$updateData['email'] = $email;
				}
				$newVendor->update($updateData);

				// archive all followup to complete after confirming.
				$activeFollowups = VendorFollowup::where('vendor_signup_id', $newVendor->id)
				                                 ->get();
				foreach ($activeFollowups as $followup)
				{
					$followup->update([
						                  'is_complete'  => 1,
						                  'done_comment' => 'Marked completed by DEFAULT after vendor confirmation'
					                  ]);
				}
				$res = ['success' => true];
			}
		} catch (\Exception $e)
		{
			// roll-up previous actions: delete newly created user account
			$this->sendErrorReport($e);
			$newUser->forceDelete();
			$res = ['success' => false, 'error' => $e->getMessage()];
		}

		return response()->json($res);
	}

	public function saveNewVendorUpdate($vendorId)
	{
		$vendor = VendorSignup::find($vendorId);

		return $this->saveVendorUpdate($vendor, false);
	}

	public function saveNewVendorFollowup($vendorId)
	{
		$vendor = VendorSignup::find($vendorId);

		return $this->saveFollowup($vendor, false);
	}

	public function markCompleteNewFollowup($followupId)
	{
		return $this->completeFollowup($followupId, false);
	}
}