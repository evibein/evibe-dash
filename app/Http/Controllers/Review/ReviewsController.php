<?php namespace App\Http\Controllers\Review;

use App\Http\Controllers\Base\BaseController;
use App\Models\TypeEvent;
use App\Models\Util\AccessToken;
use App\Models\Util\AuthClient;
use Evibe\Facades\AppUtilFacade as AppUtil;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;

class ReviewsController extends BaseController
{
	/**
	 * SHOW ALL REVIEWS BASED ON STATUS
	 */
	public function getReviewsList($status)
	{
		$reviews = \Review::orderBy('created_at', 'DESC');

		if (request("partnerId") > 0 && request("partnerTypeId") > 0)
		{
			$reviews = $reviews->where("map_type_id", request("partnerTypeId"))
			                   ->where(function ($query) {
				                   $query->where("planner_id", request("partnerId"))
				                         ->orWhere("map_id", request("partnerId"));
			                   });
		}

		if ($status == "pending")
		{
			$reviews = $reviews->whereNull('is_accepted')
			                   ->where('is_rejected', 0)
			                   ->paginate(15);
		}
		elseif ($status == "accepted")
		{
			$reviews = $reviews->where('is_accepted', 1)
			                   ->paginate(15);
		}
		elseif ($status == "rejected")
		{
			$reviews = $reviews->where('is_rejected', 1)
			                   ->paginate(15);
		}
		else
		{
			return redirect()->back();
		}

		$data = [
			'reviews' => $reviews
		];

		return view('reviews/reviews', ['data' => $data]);
	}

	/**
	 * SHOW ADD REVIEW FORM
	 */
	public function addNewReview()
	{
		$vendors = \Vendor::all();

		$data = [
			'vendors'    => $vendors,
			'typeEvents' => TypeEvent::select("id", "name")->get()
		];

		return view('reviews/new_review', ['data' => $data]);
	}

	/**
	 * SAVE A REVIEW
	 */
	public function saveNewReview()
	{
		$rules = [
			'clientName'      => 'required|min:4',
			'location'        => 'required|min:4',
			'reviewPartyDate' => 'required|date',
			'reviewData'      => 'min:10',
			'rating'          => 'required|numeric|min:1|max:5',
		];

		$messages = [
			'clientName.required'      => 'Please enter valid customer name',
			'clientName.min'           => 'customer name should be minimum 4 characters long',
			'location.required'        => 'Please enter valid location name',
			'location.min'             => 'Location should be minimum 4 characters long',
			'reviewPartyDate.required' => 'Please enter valid party date',
			'reviewPartyDate.date'     => 'Please enter valid party date',
			'reviewData.required'      => 'Please enter valid review',
			'reviewData.min'           => 'Please enter valid review',
			'rating.required'          => 'Please enter valid rating',
			'rating.numeric'           => 'Please enter valid rating',
			'rating.min'               => 'Rating should be between 1 and 5',
			'rating.max'               => 'Rating should be between 1 and 5'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->passes())
		{
			// create a package first
			$inputData = [
				'name'           => Input::get('clientName'),
				'review'         => Input::get('reviewData'),
				'location'       => Input::get('location'),
				'event_id'       => Input::get('occasionId'),
				'email'          => '',
				'phone'          => '',
				'planner_id'     => Input::get('vendor'),
				'created_at'     => date('Y-m-d H:i:s', strtotime(str_replace('-', '/', Input::get('reviewPartyDate')))),
				'updated_at'     => date('Y-m-d H:i:s'),
				'is_accepted'    => 1,
				'accepted_by_id' => Auth::user()->id,
				'rating'         => Input::get('rating')
			];

			$review = \Review::create($inputData);
			$vendor = \Vendor::findOrFail(Input::get('vendor'));
			if ($review && $vendor)
			{
				$ccAddresses = [config('evibe.business_group_email')];
				$replyTo = [config('evibe.business_group_email')];
				$sub = '[Evibe.in] You got a new review from ' . Input::get('clientName') . ' for party on ' . Input::get('reviewPartyDate');

				$data = [
					'to'           => [$vendor->email],
					'ccAddresses'  => $ccAddresses,
					'replyTo'      => $replyTo,
					'subject'      => $sub,
					'vendorName'   => $vendor->person,
					'reviewData'   => Input::get('reviewData'),
					'vendorRating' => Input::get('rating'),
					'partyDate'    => AppUtil::formatDate(Input::get('reviewPartyDate')),
					'clientName'   => Input::get('clientName')
				];

				Queue::push('\Evibe\Utilities\SendEmail@mailReviewToVendor', $data);

				return redirect(route("reviews.list", "accepted"))->with('custom_success', 'New review has been added successfully');
			}
		}
		else
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}
	}

	/**
	 * DELETE REVIEW
	 */
	public function deleteReview($reviewId)
	{
		$review = \Review::findOrFail($reviewId);

		if ($review->delete())
		{
			return redirect(route("reviews.list", "accepted"))->with('custom_success', 'Review deleted successfully');
		}
	}

	/**
	 * EDIT A REVIEW
	 */
	public function editReview()
	{
		$rules = [
			'editReviewData' => 'required|min:10',
		];

		$messages = [
			'editReviewData.required' => 'Review cannot be empty',
			'editReviewData.min'      => 'Review should be a mininum of 10 characters'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->passes())
		{
			$reviewId = Input::get('editReviewId');

			$review = \Review::findOrFail($reviewId);
			$review->review = Input::get('editReviewData');

			if ($review->save())
			{
				return redirect(route("reviews.list", "accepted"))->with('custom_success', 'Review has been edited succesfully');
			}
		}
		else
		{
			return redirect(route("reviews.list", "accepted"))->withErrors($validator)->withInput();
		}
	}

	/**
	 * ADD PARTNER REPLY TO A REVIEW
	 */
	public function addPartnerReply()
	{
		$rules = [
			'replyData' => 'required|min:10',
		];

		$messages = [
			'replyData.required' => 'Reply cannot be empty',
			'replyData.min'      => 'Reply should be a mininum of 10 characters'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->passes())
		{
			$inputData = [
				'reply'      => Input::get('replyData'),
				'review_id'  => Input::get('reviewId'),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			$reviewReply = \ReviewReply::create($inputData);

			if ($reviewReply)
			{
				return redirect(route("reviews.list", "accepted"))->with('custom_success', 'New partner reply added succesfully');
			}
		}
		else
		{
			return redirect(route("reviews.list", "accepted"))->withErrors($validator)->withInput();
		}
	}

	/**
	 * DELETE A PARTNER REPLY
	 */
	public function deletePartnerReply()
	{
		$replyId = Input::get('replyId');

		$reply = \ReviewReply::findOrFail($replyId);

		if ($reply->delete())
		{
			return response()->json(['success' => true]);
		}
		else
		{
			return response()->json(['success' => false]);
		}

	}

	/**
	 * Accept review less than 4
	 */
	public function acceptReview($reviewId)
	{
		$review = \Review::find($reviewId);
		if ($review->update(["is_accepted" => 1, "accepted_by_id" => Auth::user()->id, "comments" => request("acceptRejectComment")]))
		{
			return redirect(route("reviews.list", "accepted"));
		}

		return redirect()->back();
	}

	/**
	 * Reject review less than 4
	 */
	public function rejectReview($reviewId)
	{
		$review = \Review::find($reviewId);
		if ($review->update(["is_rejected" => 1, "rejected_by_id" => Auth::user()->id, "comments" => request("acceptRejectComment")]))
		{
			return redirect(route("reviews.list", "accepted"));
		}

		return redirect()->back();
	}

	/**
	 * TODO: Implement edit review
	 */
	public function getEdit()
	{
		return redirect('/reviews/pending');
	}

	public function getMappedList()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.reviews-mapping.mapped-reviews');
		$request= request()->all();
		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => $request
		];

		$data = $this->makeApiCallWithUrl($options);
		//dd($data);

		return view('reviews/reviews-mapping/mapped-list', ['data' => $data]);
	}

	public function getPendingList()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.reviews-mapping.pending-reviews');
		$request= request()->all();

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => $request
		];

		$data = $this->makeApiCallWithUrl($options);

		return view('reviews/reviews-mapping/pending-list', ['data' => $data]);
	}

	public function saveOptionMapping()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.reviews-mapping.save');

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => request()->all()
		];

		$data = $this->makeApiCallWithUrl($options);

		return $data;
	}


	public function getOptionsDataByType()
	{
		$optionTypeId = request("optionTypeId");

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.reviews-mapping.get-options'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => [
				"optionTypeId" => $optionTypeId,
			]
		];

		$mappingValues = $this->makeGuzzleRequest($options);

		return view("tickets.details.mapping.type-ticket-options", ["mappingValues" => $mappingValues]);
	}
}