<?php

namespace App\Http\Controllers\Tracking;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\Tracking\OrderTrackingUpdate;
use App\Jobs\Emails\Tracking\PartnerUnavailable;
use App\Models\Tracking\OrderTracking;
use App\Models\Types\TypeOrderStatus;
use App\Models\Types\TypeTrackingQuestions;
use App\Models\User;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TrackingController extends BaseController
{
	public function showList($all = null)
	{
		$timings = null;
		$bookings = null;
		$isShowReset = false;
		$provider = null;
		$providers = [];
		$checkoutFields = [];

		$vendors = \Vendor::select("id", "name", "code", "deleted_at", "person", "phone", "alt_phone")->get();
		$venues = \Venue::select("id", "name", "code", "deleted_at", "person", "phone", "alt_phone")->get();
		$trackingQuestions = TypeTrackingQuestions::all();
		$orderStatus = TypeOrderStatus::select("id", "status", "color")->get();
		$isShowAll = 0;

		$bookings = \TicketBooking::select("ticket_bookings.*")
		                          ->join("ticket", "ticket.id", "=", "ticket_bookings.ticket_id")
		                          ->where('is_advance_paid', 1)
		                          ->whereNull("ticket.deleted_at")
		                          ->whereNull("ticket_bookings.cancelled_at")
		                          ->where('ticket.status_id', config("evibe.status.booked"));

		// show "To Track Orders"
		if (is_null($all))
		{
			$bookings = $bookings->where('party_date_time', '>=', Carbon::now()->timestamp)
			                     ->where(function ($inner) {
				                     $inner->whereNull("order_status_id")
				                           ->orWhere("order_status_id", "<>", config("evibe.order_status.tracked"));
			                     })
			                     ->orderBy('party_date_time');
		}
		else
		{
			$bookings = $bookings->where(function ($inner) {
				$inner->where('party_date_time', '<', Carbon::now()->timestamp)
				      ->orWhere(function ($inner2) {
					      $inner2->whereNotNull("order_status_id")
					             ->where("order_status_id", config("evibe.order_status.tracked"));
				      });
			})->orderBy('party_date_time', 'desc');

			$isShowAll = 1;
		}

		foreach ($vendors as $vendor)
		{
			$vendor = $vendor->toArray();
			$vendor['type'] = config("evibe.ticket_type.planners");
			$providers[] = $vendor;
		}
		foreach ($venues as $venue)
		{
			$venue = $venue->toArray();
			$venue['type'] = config("evibe.ticket_type.venues");
			$providers[] = $venue;
		}

		// city filter
		$cityId = request('city', 'all');
		if ($cityId != 'all' && ($cityId = (int)$cityId) && $cityId > 0)
		{
			$isShowReset = true;
			$allCities = \City::all();
			$allCityIds = $allCities->pluck('id')->toArray();

			if (in_array($cityId, $allCityIds))
			{
				$bookings->where('ticket.city_id', $cityId);
			}
		}

		// search filter
		$searchQuery = request('query', false) ?: false;
		if ($searchQuery)
		{
			$isShowReset = true;
			$bookings->where(function ($innerQuery) use ($searchQuery) {
				$innerQuery->where('ticket.name', 'LIKE', '%' . $searchQuery . '%')
				           ->orWhere('ticket.phone', 'LIKE', '%' . $searchQuery . '%')
				           ->orWhere('ticket.alt_phone', 'LIKE', '%' . $searchQuery . '%')
				           ->orWhere('booking_id', 'LIKE', '%' . $searchQuery . '%');
			});
		}

		// party date filters
		if (request('start_date') && $filterPDStart = strtotime(request('start_date')))
		{
			$isShowReset = true;
			$bookings->where('party_date_time', '>=', $filterPDStart);
		}
		if (request('end_date') && $filterPDEnd = (strtotime(request('end_date')) + 24 * 60 * 60 - 1))
		{
			$isShowReset = true;
			$bookings->where('party_date_time', '<=', $filterPDEnd);
		}

		// provider filters
		$provider = explode('-', request('p_id'));
		$providerId = isset($provider[0]) ? $provider[0] : null;
		$providerTypeId = isset($provider[1]) ? $provider[1] : null;
		if ($providerId != 'all' && $providerTypeId != 'all' && $providerId > 0 && $providerTypeId > 0)
		{
			$isShowReset = true;
			$bookings->where('map_type_id', $providerTypeId)
			         ->where('map_id', $providerId);
		}

		$count = $bookings->count();
		$bookings = $bookings->paginate(25);

		$orderTracking = OrderTracking::whereIn('booking_id', $bookings->pluck("id")->toArray())
		                              ->orderBy('updated_at')
		                              ->get();

		// handlers (code optimised for minimal queries)
		$partnerTrackHandlers = $bookings->pluck("order_tracking_partner_handler_id")->toArray();
		$customerTrackHandlers = $bookings->pluck("order_tracking_customer_handler_id")->toArray();
		$handlerIds = array_unique(array_merge($partnerTrackHandlers, $customerTrackHandlers));
		$handlers = User::select('id', 'name')
		                ->whereIn('id', $handlerIds)
		                ->get();

		// get all check fields
		$allCheckoutFields = CheckoutField::select('id', 'type_ticket_booking_id', 'event_id', 'identifier')
		                                  ->where('is_crm', 1)
		                                  ->where('is_auto_booking', 0)
		                                  ->whereIn('type_ticket_booking_id', $bookings->pluck('type_ticket_booking_id')->toArray())
		                                  ->get();

		$checkoutFieldValues = CheckoutFieldValue::select('ticket_id', 'checkout_field_id', 'value')
		                                         ->whereIn('checkout_field_id', $allCheckoutFields->pluck('id')->toArray())
		                                         ->whereIn('ticket_id', $bookings->pluck('ticket_id')->toArray())
		                                         ->get();

		foreach ($bookings as $booking)
		{
			$trackingPartnerQuestions = $trackingQuestions->where('is_partner', 1);
			$partnerTracking = $orderTracking->where('booking_id', $booking->id)
			                                 ->whereIn('tracking_question_id', $trackingPartnerQuestions->pluck('id')->toArray())
			                                 ->first();

			$trackingCustomerQuestions = $trackingQuestions->where('is_customer', 1);
			$customerTracking = $orderTracking->where('booking_id', $booking->id)
			                                  ->whereIn('tracking_question_id', $trackingCustomerQuestions->pluck('id')->toArray())
			                                  ->first();

			$timings[$booking->id] = [
				'customer' => $customerTracking ?: null,
				'partner'  => $partnerTracking ?: null
			];

			$booking["provider"] = [];

			if (($booking->map_type_id == config("evibe.ticket_type.planners"))
				|| ($booking->map_type_id == config("evibe.ticket_type.artists")))
			{
				$vendor = $vendors->find($booking->map_id);
				if ($vendor)
				{
					$booking["provider"] = $vendor->toArray();
				}

			}
			elseif ($booking->map_type_id == config("evibe.ticket_type.venues"))
			{
				$venue = $venues->find($booking->map_id);
				if ($venue)
				{
					$booking["provider"] = $venue->toArray();
				}
			}

			$booking["checkoutFields"] = [];
			$relatedCheckoutField = $allCheckoutFields->where('type_ticket_booking_id', $booking->type_ticket_booking_id);
			if ($relatedCheckoutField)
			{
				foreach ($relatedCheckoutField as $checkoutField)
				{
					$checkoutFieldValue = $checkoutFieldValues->where('ticket_id', $booking->ticket_id)
					                                          ->where('checkout_field_id', $checkoutField->id)
					                                          ->first();
					if ($checkoutFieldValue)
					{
						$checkoutFields[$booking->id][] = [
							"identifier" => $checkoutField->identifier,
							"value"      => $checkoutFieldValue->value
						];
					}
				}
			}
		}

		$data = [
			'bookings'          => $bookings,
			'orderStatus'       => $orderStatus,
			'trackingQuestions' => $trackingQuestions,
			'orderTracking'     => $orderTracking,
			'handlers'          => $handlers,
			'timings'           => $timings,
			'count'             => $count,
			'allParties'        => $isShowAll,
			'cities'            => \City::all(),
			'isShowReset'       => $isShowReset,
			'providers'         => array_unique($providers, SORT_REGULAR),
			'checkoutFields'    => $checkoutFields
		];

		return view('tracking.list', ['data' => $data]);
	}

	public function saveModel()
	{
		$inputData = request()->all();
		$bookingId = request('orderBookingId');

		$trackQuestions = TypeTrackingQuestions::where('is_customer', 1)
		                                       ->orWhere('is_partner', 1)
		                                       ->pluck('id')
		                                       ->toArray();

		// inserting the values given by the handler
		foreach ($inputData as $key => $value)
		{
			if (in_array($key, $trackQuestions))
			{
				OrderTracking::updateOrCreate(['tracking_question_id' => $key, 'booking_id' => $bookingId], ['value' => $value]);
			}
		}

		// updating the order status of the booking
		$booking = \TicketBooking::find($bookingId);
		if ($booking)
		{
			$userId = Auth::user()->id;
			$emailData = [
				'userId'              => $userId,
				'booking'             => $booking,
				'provider'            => $booking->provider ?: "",
				'ticket'              => $booking->ticket,
				'customerOrderUpdate' => request("customerOrderUpdate"),
				'partnerOrderUpdate'  => request("partnerOrderUpdate"),
				'isCustomer'          => request("isCustomer")
			];

			if (request('isCustomer') == 1)
			{
				$booking->update([
					                 'order_tracking_customer_handler_id' => $userId,
					                 'customer_order_update'              => request("customerOrderUpdate")
				                 ]);

				if (!empty(request("customerOrderUpdate")))
				{
					dispatch(new OrderTrackingUpdate($emailData));
				}
			}
			else
			{
				$booking->update([
					                 'order_tracking_partner_handler_id' => $userId,
					                 'partner_order_update'              => request("partnerOrderUpdate"),
					                 'partner_delivery_person_details'   => request("partnerDeliveryPersonDetails"),
					                 'partner_delivery_time_details'     => request("partnerDeliveryTimeDetails")
				                 ]);
				if (!empty(request("partnerOrderUpdate")))
				{
					dispatch(new OrderTrackingUpdate($emailData));
				}
			}

			$status = config('evibe.order_status.in_progress');
			if (!is_null($booking->order_tracking_partner_handler_id) && !is_null($booking->order_tracking_customer_handler_id))
			{
				$status = config('evibe.order_status.tracked');
			};

			$booking->update(['order_status_id' => $status]);
		}

		return redirect()->back()->with(['successMsg' => "Order Tracking updated successfully."]);
	}

	public function sendPartnerUnavailableMail()
	{
		$booking = \TicketBooking::find(request('orderBookingId'));
		if ($booking)
		{
			$emailData = [
				'booking'  => $booking,
				'ticket'   => $booking->ticket,
				'comments' => request("partnerUnavailableInput")
			];

			dispatch(new PartnerUnavailable($emailData));

			return redirect()->back()->with(['successMsg' => "Team has been alerted successfully."]);
		}

		return redirect()->back()->with(['errorMsg' => "Some error occured, please try again."]);
	}
}