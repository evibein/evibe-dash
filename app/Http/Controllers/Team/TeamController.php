<?php

namespace App\Http\Controllers\Team;

use App\Http\Controllers\Base\BaseController;

use App\Models\Util\Team;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class TeamController extends BaseController
{
	public function showList()
	{
		$teams = Team::orderBy('first_name', 'ASC')->paginate(16);

		return view('team/list', compact('teams'));
	}

	public function showCreateMember()
	{
		return view('team/create');
	}

	public function createTeamMember(Request $request)
	{
		// validation rules
		$validationRules = $this->validationRules();
		$this->validate($request, $validationRules['rules'], $validationRules['messages']);

		// upload the picture first
		$uploadPath = '/img/team/';
		$imageName = $this->uploadImage($request->file('profileImg'), $uploadPath);

		if ($imageName)
		{
			// save the new team member data
			Team::create([
				             'first_name'   => $request->input('fname'),
				             'last_name'    => $request->input('lname'),
				             'nick_name'    => $request->input('nickname'),
				             'full_name'    => $request->input('fname') . ' ' . $request->input('lname'),
				             'bio'          => $request->input('bio'),
				             'email'        => $request->input('email'),
				             'facebook_url' => $request->input('facebook'),
				             'linkedin_url' => $request->input('linkdin'),
				             'twitter_url'  => $request->input('twitter'),
				             'website_url'  => $request->input('website'),
				             'image_url'    => $imageName,
				             'joined_at'    => $request->input('joined') ? date('Y-m-d H:i:s', strtotime($request->input('joined'))) : null,
				             'left_at'      => $request->input('left') ? date('Y-m-d H:i:s', strtotime($request->input('left'))) : null,
				             'join_pos'     => ucwords($request->input('joinedPost')),
				             'current_pos'  => ucwords($request->input('currentPost'))
			             ]);

			return redirect()->route('team.list');
		}
		else
		{
			return redirect()->back()->withInput()
			                 ->with('customError', 'Some error occurred while saving the profile picture. please try again');
		}
	}

	public function showEditTeamMember($id)
	{
		$member = Team::findOrFail($id);

		return view('team/edit', compact('member'));
	}

	public function updateTeamMember(Request $request, $id)
	{
		// validation rules
		$member = Team::findOrFail($id);
		$validationRules = $this->validationRules(true);
		$this->validate($request, $validationRules['rules'], $validationRules['messages']);
		$imageName = "";

		if ($request->hasFile('profileImg'))
		{
			// upload the picture and delete the existing one;
			$oldImage = $member->image_url;
			$uploadPath = '/img/team/';
			$imageName = $this->uploadImage($request->file('profileImg'), $uploadPath);

			if ($imageName && $oldImage)
			{
				$oldPath = config('evibe.gallery.root') . $uploadPath . $oldImage;
				try
				{
					File::delete($oldPath);
				} catch (Exception $e)
				{
					Log::info("Team Profile image could not deleted, Link :: " . $oldPath);
				}
			}
			else
			{
				return redirect()->back()->withInput()
				                 ->with('customError', 'Some error occurred while saving the profile picture. please try again');
			}
		}

		// update the data of existing team member
		$updateData = [
			'first_name'   => $request->input('fname'),
			'last_name'    => $request->input('lname'),
			'nick_name'    => $request->input('nickname'),
			'full_name'    => $request->input('fname') . ' ' . $request->input('lname'),
			'bio'          => $request->input('bio'),
			'email'        => $request->input('email'),
			'facebook_url' => $request->input('facebook'),
			'linkedin_url' => $request->input('linkdin'),
			'twitter_url'  => $request->input('twitter'),
			'website_url'  => $request->input('website'),
			'joined_at'    => $request->input('joined') ? date('Y-m-d H:i:s', strtotime($request->input('joined'))) : null,
			'left_at'      => $request->input('left') ? date('Y-m-d H:i:s', strtotime($request->input('left'))) : null,
			'join_pos'     => ucwords($request->input('joinedPost')),
			'current_pos'  => ucwords($request->input('currentPost'))
		];
		if ($imageName)
		{
			$updateData['image_url'] = $imageName;
		}

		if ($member->update($updateData))
		{
			return redirect()->route('team.member.details', $member->id);
		}
	}

	public function showTeamMemberDetails($id)
	{
		$member = Team::findOrFail($id);

		return view('team.details', compact('member'));
	}

	private function validationRules($hasUpdate = false)
	{
		$rules = [
			'fname'      => 'required',
			'email'      => 'email',
			'bio'        => 'min:10',
			'linkdin'    => 'url',
			'website'    => 'url',
			'twitter'    => 'url',
			'facebook'   => 'url',
			'joined'     => 'date|before:tomorrow',
			'left'       => 'date|before:tomorrow|after:joined',
			'profileImg' => 'required|mimes:png,jpeg,jpg,JPG|max:1024',
		];

		if ($hasUpdate)
		{
			$rules['profileImg'] = 'mimes:png,jpeg,jpg,JPG|max:1024';
		}

		$messages = [
			'fname.required'      => 'First Name is required',
			'email.email'         => 'Please enter valid email',
			'bio.min'             => 'Bio field cannot be less than 10 character',
			'linkdin.url'         => 'Pleas enter a valid url for linkdin profile',
			'website.url'         => 'Pleas enter a valid url for website profile',
			'twitter.url'         => 'Pleas enter a valid url for twitter profile',
			'facebook.url'        => 'Pleas enter a valid url for facebook profile',
			'joined.date'         => 'Please enter a valid date for Joined At field',
			'joined.before'       => 'Joined At date cannot be a future date',
			'left.date'           => 'Please enter a valid date for Left At field',
			'left.before'         => 'Left At date cannot be a future date',
			'left.after'          => 'Left At date must be after joined At date',
			'profileImg.required' => 'Please upload profile image',
			'profileImg.mimes'    => 'Profile image is not valid. (only .png, .jpeg are accepted)',
			'profileImg.max'      => 'Profile image size cannot br more than 1 MB'
		];

		return ['rules' => $rules, 'messages' => $messages];
	}

	public function uploadImage($imageFile, $imageUploadTo)
	{
		$parentDirs = config('evibe.gallery.root');
		$fullUploadTo = $parentDirs . $imageUploadTo; // append with parent dirs
		$imageName = $imageFile->getClientOriginalName();
		$originalName = str_replace(" ", "", $imageName);
		$newImageName = trim(time() . '-' . $originalName);
		$width = 300;
		$height = 300;

		if (!file_exists($fullUploadTo))
		{
			mkdir($fullUploadTo, 0777, true);
		}

		try
		{
			$img = Image::make($imageFile->getRealPath())->resize($width, $height);
			$img->save($fullUploadTo . $newImageName);

			return $newImageName;

		} catch (Exception $e)
		{
			$this->sendErrorReport($e);
		}

		return false; // failed case
	}
}
