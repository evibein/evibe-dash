<?php

namespace App\Http\Controllers\Settlement;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\Settlements\sendIndividualSettlementEmailJob;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PaymentSettlementController extends BaseController
{
	public function getRequirement()
	{
		// $partners = $this->getPartners($this->getBookings());

		// get all partners
		$partners = [];
		$nonVenues = \Vendor::all();
		foreach ($nonVenues as $partner)
		{
			$partners[2 . '-' . $partner->id] = $partner->person . ', ' . $partner->name; // 2 = planner
		}

		$venues = \Venue::all();
		foreach ($venues as $partner)
		{
			$partners[3 . '-' . $partner->id] = $partner->person . ', ' . $partner->name; // 3 = venues
		}

		return view('settlement.requirement', compact('partners'));
	}

	public function getPaymentSettlement()
	{
		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');
		$cities = \City::all();

		$totalBookingAmount = $totalAdvanceAmount = $totalCommissionAmount = $totalBalanceAmount = $totalCGSTAmount = $totalSGSTAmount = 0;
		$bookingsData = [
			'total'        => [],
			'vendors'      => [],
			'checkBalance' => []
		];

		// validation
		$rules = [
			'fromDate' => 'required|date',
			'toDate'   => 'date|after:fromDate'
		];
		$messages = [
			'fromDate.required' => 'From date is required',
			'fromDate.date'     => 'From date is invalid',
			'toDate.date'       => 'To date is invalid',
			'toDate.after'      => '<b>To Date</b> Must be after <b>From Date</b>'
		];
		$validator = Validator::make(Input::all(), $rules, $messages);
		if ($validator->fails())
		{
			return redirect()->back()
			                 ->withErrors($validator->messages())
			                 ->withInput();
		}

		// get end of toDate
		$formattedFromDate = strtotime($fromDate);
		$formattedToDate = $toDate ? strtotime($toDate) : Carbon::today()->startOfDay()->timestamp;
		$formattedToDate = $formattedToDate + 24 * 60 * 60 - 1; // add full day
		$dateRange = [$formattedFromDate, $formattedToDate];

		// fetch result
		$bookings = $this->getBookings($dateRange, null, null, true);
		$serialNumber = 1;
		$bookingCount = 2;

		foreach ($bookings as $booking)
		{
			$ticket = $booking->ticket;
			$vendor = $booking->provider;

			if (!$ticket || !$vendor || $ticket->status_id == config('evibe.status.cancelled'))
			{
				continue;
			}

			$bookingAmount = $booking->booking_amount;
			$advanceAmount = $booking->advance_amount;
			$commissionPercentage = $vendor->commission_rate ? $vendor->commission_rate : 15;
			$commissionAmount = $bookingAmount * ($commissionPercentage / 100);
			$sgstAmount = $commissionAmount * 0.09;
			$cgstAmount = $commissionAmount * 0.09;
			$balanceAmount = $advanceAmount - $commissionAmount - $sgstAmount - $cgstAmount;

			$totalBookingAmount += $bookingAmount;
			$totalAdvanceAmount += $advanceAmount;
			$totalCommissionAmount += $commissionAmount;
			$totalBalanceAmount += $balanceAmount;
			$totalSGSTAmount += $sgstAmount;
			$totalCGSTAmount += $cgstAmount;

			$aggregateData = [
				'Sl No.'          => $serialNumber,
				'Booking Date'    => date('d M Y', $ticket->paid_at),
				'Party Date'      => date('d M Y', $booking->party_date_time),
				'Customer Name'   => $ticket->name,
				'Partner Company' => $vendor->name,
				'Partner Name'    => $vendor->person,
				'Booking Amount'  => $bookingAmount,
				'Advance Amount'  => $advanceAmount,
				'Service Fee %'   => $commissionPercentage,
				'Service Fee'     => $commissionAmount,
				'SGST'            => $sgstAmount,
				'CGST'            => $cgstAmount,
				'Balance Amount'  => $balanceAmount,
				'City'            => $cities->where("id", $ticket->city_id)->first() ? $cities->where("id", $ticket->city_id)->first()->name : "--"
			];

			if ($balanceAmount < 0)
			{
				$bookingsData['checkBalance'][] = $bookingCount;
			}

			$bookingsData['total'][$bookingCount++] = $aggregateData;
			$uniqueKey = $booking->map_type_id . "-" . $booking->map_id;

			if (!array_key_exists($uniqueKey, $bookingsData['vendors']))
			{
				$bookingsData['vendors'][$uniqueKey] = [
					'info'          => [
						'vendor_company' => $vendor->name,
						'vendor_name'    => $vendor->person,
					],
					'bookings'      => [
						0 => [
							'Sl No.'         => 'Total',
							'Booking Date'   => '',
							'Party Date'     => '',
							'Customer Name'  => '',
							'Booking Amount' => 0,
							'Advance Amount' => 0,
							'Service Fee %'  => '',
							'Service Fee'    => 0,
							'SGST'           => 0,
							'CGST'           => 0,
							'Balance Amount' => 0
						]
					],
					'negativeCells' => []
				];
			}

			$currentVendorData = $bookingsData['vendors'][$uniqueKey]['bookings'];
			$len = count($currentVendorData);
			$bookingsData['vendors'][$uniqueKey]['bookings'][$len] = $aggregateData;
			$bookingsData['vendors'][$uniqueKey]['bookings'][$len]['Sl No.'] = $len;
			$vendorSettlement = array_except($bookingsData['vendors'][$uniqueKey]['bookings'][$len], ['Partner Name', 'Partner Company']);
			$bookingsData['vendors'][$uniqueKey]['bookings'][$len] = $vendorSettlement;

			if ($balanceAmount < 0)
			{
				$bookingsData['vendors'][$uniqueKey]['negativeCells'][$len] = $len + 2;
			}

			// update individual counts
			$bookingsData['vendors'][$uniqueKey]['bookings'][0]['Booking Amount'] += $bookingAmount;
			$bookingsData['vendors'][$uniqueKey]['bookings'][0]['Advance Amount'] += $advanceAmount;
			$bookingsData['vendors'][$uniqueKey]['bookings'][0]['Balance Amount'] += $balanceAmount;
			$bookingsData['vendors'][$uniqueKey]['bookings'][0]['Service Fee'] += $commissionAmount;
			$bookingsData['vendors'][$uniqueKey]['bookings'][0]['SGST'] += $sgstAmount;
			$bookingsData['vendors'][$uniqueKey]['bookings'][0]['CGST'] += $cgstAmount;

			if ($bookingsData['vendors'][$uniqueKey]['bookings'][0]['Balance Amount'] < 0)
			{
				$bookingsData['vendors'][$uniqueKey]['negativeCells'][0] = 2;
			}

			$serialNumber++;
		}

		$bookingsData['total'][1] = [
			'Sl No.'          => 'Total',
			'Booking Date'    => '',
			'Party Date'      => '',
			'Customer Name'   => '',
			'Partner Company' => '',
			'Partner Name'    => '',
			'Booking Amount'  => $totalBookingAmount,
			'Advance Amount'  => $totalAdvanceAmount,
			'Service Fee %'   => '',
			'Service Fee'     => $totalCommissionAmount,
			'SGST'            => $totalCommissionAmount * 0.09,
			'CGST'            => $totalCommissionAmount * 0.09,
			'Balance Amount'  => $totalAdvanceAmount - $totalCommissionAmount - ($totalCommissionAmount * 0.09) - ($totalCommissionAmount * 0.09)
		];

		if ($bookingsData['total'][1]['Balance Amount'] < 0)
		{
			$negativeBalanceCount = count($bookingsData['checkBalance']);
			$bookingsData['checkBalance'][$negativeBalanceCount] = $bookingCount + 1;
		}

		$readableFromDate = date('d M Y', strtotime($fromDate));
		$readableToDate = date('d M Y', $formattedToDate);

		if (count($bookingsData['vendors']) == 0)
		{
			return redirect()->back()->with('InfoMsg', 'No payment settled between ' . $readableFromDate . ' to ' . $readableToDate)->withInput();
		}

		// show in excel sheet
		Excel::create('Settlement-from-' . $readableFromDate . '-to-' . $readableToDate, function ($excel) use ($bookingsData, $serialNumber) {
			// totals sheet
			$excel->sheet('Total', function ($sheet) use ($bookingsData, $serialNumber) {
				$sheet->fromArray($bookingsData['total']);
				$sheet->row(1, function ($row) {
					$row->setBackground('#faca39');
					$row->setAlignment('center');
				});

				// styling last row, which have the total value
				$sheet->row($serialNumber + 1, function ($row) {
					$row->setFontSize(12);
					$row->setFontColor('#0000fd');
					$row->setAlignment('left');
				});
				$sheet->setHeight($serialNumber + 1, 15);
				$sheet->setHeight(1, 20);
				$sheet->setPageMargin(0.8);

				// Col "J" is hard-coded based on the col count
				foreach ($bookingsData['checkBalance'] as $key => $value)
				{
					$sheet->cells('J' . $value, function ($cells) {
						$cells->setFontColor('#ff0000');
					});
				}

			});

			foreach ($bookingsData['vendors'] as $key => $vendorData)
			{
				$excel->sheet($vendorData['info']['vendor_name'], function ($vendorSheet) use ($vendorData) {
					$vendorSheet->fromArray($vendorData['bookings']);
					$vendorSheet->row(1, function ($row) {
						$row->setBackground('#11f523');
						$row->setAlignment('center');
					});
					$vendorSheet->row(2, function ($row) {
						$row->setFontColor('#00007f');
						$row->setAlignment('center');
					});

					$vendorSheet->setHeight(1, 20);
					$vendorSheet->setHeight(2, 20);

					// Col "H" is hard-coded based on the col count
					foreach ($vendorData['negativeCells'] as $key => $value)
					{
						$vendorSheet->cells('H' . $value, function ($cells) {
							$cells->setFontColor('#ff0000');
						});
					}
				});
			}
		})->export('xls');

	}

	public function downloadReport()
	{
		$fromDate = Input::get('from');
		$toDate = Input::get('to');

		if (!$fromDate || !$toDate)
		{
			return "Nothing to download";
		}

		// initialize
		$totalBookingAmount = 0;
		$totalAdvanceAmount = 0;
		$totalCommissionAmount = 0;
		$totalBalanceAmount = 0;
		$bookingsData = [
			'total'        => [],
			'vendors'      => [],
			'checkBalance' => []
		];

		$formattedFromDate = strtotime($fromDate);
		$formattedToDate = $toDate ? strtotime($toDate) : Carbon::today()->startOfDay()->timestamp;
		$formattedToDate = $formattedToDate + 24 * 60 * 60 - 1; // add full day
		$dateRange = [$formattedFromDate, $formattedToDate];

		$bookings = \TicketBooking::join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                          ->select('ticket_bookings.*')
		                          ->whereNull('ticket.deleted_at')
		                          ->whereBetween('ticket_bookings.party_date_time', $dateRange)
		                          ->where('ticket_bookings.is_advance_paid', 1)
		                          ->get();

		$serialNumber = 1;
		$bookingCount = 2;

		foreach ($bookings as $booking)
		{
			$ticket = $booking->ticket;
			$vendor = $booking->provider;

			if (!$ticket || !$vendor)
			{
				continue;
			}

			$bookingAmount = $booking->booking_amount;
			$advanceAmount = $booking->advance_amount;
			$commissionPercentage = $vendor->commission_rate ? $vendor->commission_rate : 15;
			$commissionAmount = $bookingAmount * ($commissionPercentage / 100);
			$balanceAmount = $advanceAmount - $commissionAmount;

			$totalBookingAmount += $bookingAmount;
			$totalAdvanceAmount += $advanceAmount;
			$totalCommissionAmount += $commissionAmount;
			$totalBalanceAmount += $balanceAmount;
			$partyDate = $booking->party_date_time ? date('d/m/y', $booking->party_date_time) : '';

			$aggregateData = [
				'Sl No.'                               => $serialNumber,
				'Booking Id'                           => $booking->booking_id,
				'Party Date'                           => $partyDate,
				'Customer Name'                        => $ticket->name,
				'Customer Email'                       => $ticket->email,
				'Partner Company'                      => $vendor->name,
				'Partner Name'                         => $vendor->person,
				'Booking Amount'                       => $bookingAmount,
				'Advance Paid'                         => $advanceAmount,
				'Evibe Earning (service fee)'          => $commissionAmount,
				'Balance Payment Date (from Customer)' => date('d/m/y', $booking->party_date_time),
				'Settlement To Partner (from Evibe)'   => '',
			];

			$bookingsData['total'][$bookingCount++] = $aggregateData;
			$serialNumber++;
		}

		if (!count($bookingsData['total']))
		{
			return "Nothing to download";
		}

		$readableFromDate = date('d M Y', $formattedFromDate);
		$readableToDate = date('d M Y', $formattedToDate);

		// show in excel sheet
		Excel::create('Evibe Booking ' . $readableFromDate . ' - ' . $readableToDate, function ($excel) use ($bookingsData, $serialNumber) {
			// totals sheet
			$excel->sheet('Total', function ($sheet) use ($bookingsData, $serialNumber) {
				$sheet->fromArray($bookingsData['total']);

				$sheet->row(1, function ($row) {
					$row->setBackground('#faca39');
					$row->setAlignment('center');
				});

			});

		})->export('xls');

		return "Success";
	}

	// By Vikash @since 14 Feb 2017 (send partner settlement email)
	public function showPartnerSettlement(Request $request)
	{
		$rules = [
			'fromDate' => 'required|date',
			'toDate'   => 'date|after:fromDate',
			'partner'  => 'required'
		];

		$messages = [
			'fromDate.required' => 'From date is required',
			'fromDate.date'     => 'From date is invalid',
			'toDate.date'       => 'To date is invalid',
			'toDate.after'      => '<b>To Date</b> Must be after <b>From Date</b>',
			'partner.require'   => 'Please select a partner'
		];

		$this->validate($request, $rules, $messages);
		$partnerFilter = $request->input('partner');
		$pData = explode("-", $partnerFilter);

		if (count($pData) == 2 && $pData[0] && $pData[1])
		{
			$mapTypeId = $pData[0];
			$mapId = $pData[1];

			if ($mapId && $mapTypeId)
			{
				$dateSlug = '?fromDate=' . $request->input('fromDate') . '&toDate=' . $request->input('toDate');

				return redirect(route('settlement.partner.analyse', [$mapId, $mapTypeId]) . $dateSlug);
			}
			else
			{
				return redirect()->back('error', 'please select a valid partner');
			}
		}
		else
		{
			return redirect()->back()->with('error', 'Please select a valid partner');
		}
	}

	public function showAnalysePartner($mapId, $mapTypeId, Request $request)
	{
		if (!$this->validBookingMapType($mapTypeId))
		{
			return "<h1>Invalid Partner</h1>";
		}

		$fromDate = $request->input('fromDate');
		$toDate = $request->input('toDate');

		$formattedFromDate = strtotime($fromDate);
		$formattedToDate = $toDate ? strtotime($toDate) : Carbon::today()->startOfDay()->timestamp;
		$formattedToDate = $formattedToDate + 24 * 60 * 60 - 1; // add full day
		$dateRange = [$formattedFromDate, $formattedToDate];

		if ($formattedToDate > $formattedFromDate)
		{
			$bookings = $this->getBookings($dateRange, $mapId, $mapTypeId);
			$partnerName = $this->getPartnerName($mapId, $mapTypeId);

			$data = [
				'bookings'    => $bookings,
				'partnerName' => $partnerName,
				'mapId'       => $mapId,
				'mapTypeId'   => $mapTypeId
			];

			return view('settlement.partner_report')->with($data);
		}
		else
		{
			return "<h2>Invalid date range</h2>";
		}
	}

	public function sendEmailToPartner($mapId, $mapTypeId, Request $request)
	{
		$settlements = $request->input('settlements');
		$partner = $this->getSinglePartner($mapId, $mapTypeId);

		if ($partner)
		{
			$data = [
				'settlements' => $settlements,
				'partner'     => $partner,
				'fromDate'    => $request->input('fromDate'),
				'toDate'      => $request->input('toDate'),
				'total'       => $request->input('total')
			];

			dispatch(new sendIndividualSettlementEmailJob($data));
		}

		return response()->json(['success' => true]);
	}

	private function getPartners($bookings)
	{
		$partners = [];
		foreach ($bookings as $booking)
		{
			$partner = $booking->provider;
			if ($partner)
			{
				$key = $booking->map_type_id . '-' . $partner->id;
				if (!array_key_exists($key, $partners))
				{
					$partners[$key] = $partner->person . ', ' . $partner->name;
				}
			}
		}

		return $partners;
	}

	private function getBookings($dateRange = [], $mapId = null, $mapTypeId = null, $sortByPartyDate = false)
	{
		$bookings = \TicketBooking::join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                          ->select('ticket_bookings.*')
		                          ->where('ticket.status_id', config('evibe.status.booked'))
		                          ->where('ticket_bookings.is_advance_paid', 1)
		                          ->whereNull('ticket.deleted_at');

		if ($sortByPartyDate)
		{
			$bookings = $bookings->orderBy('party_date_time');
		}

		if ($mapId && $mapTypeId)
		{
			$bookings = $bookings->where(['ticket_bookings.map_id' => $mapId, 'ticket_bookings.map_type_id' => $mapTypeId]);
		}

		if (count($dateRange) == 2)
		{
			$bookings = $bookings->whereBetween('party_date_time', $dateRange);
		}

		return $bookings->get();

	}

	private function validBookingMapType($mapTypeId)
	{
		return in_array($mapTypeId, [config('evibe.ticket_type.planners'), config('evibe.ticket_type.venues')]);
	}

}