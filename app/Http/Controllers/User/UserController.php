<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Base\BaseController;
use App\Models\Role;

use App\Http\Requests;

use App\Models\User;
use App\Models\Util\BankDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class UserController extends BaseController
{
	// show list of all users
	public function showList()
	{
		$users = User::orderBy('updated_at', 'DESC');

		$searchQuery = Input::has('query') ? Input::get('query') : null;
		if ($searchQuery)
		{
			$searchValues = '%' . $searchQuery . '%';

			$users = $users->where(function ($query) use ($searchValues, $searchQuery)
			{
				$query->where('name', 'LIKE', $searchValues)
				      ->orWhere('username', 'LIKE', $searchValues)
				      ->orWhere('phone', 'LIKE', $searchValues);
			});
		}

		$users = $users->paginate(30);
		$allRoles = Role::all();

		return view('user.list', compact('users', 'allRoles'));
	}

	public function deleteUser($userId)
	{
		$user = User::find($userId);
		if ($user->delete())
		{
			if ($user->bankDetails)
			{
				$user->bankDetails->delete();
			}

			return redirect()->back()->with('successMsg', 'User deleted successfully');
		}
		else
		{
			return redirect()->back();
		}
	}

	// show Add user form
	public function showAddUser()
	{
		$allRoles = Role::all();

		return view('user.add_user', compact('allRoles'));
	}

	// save new user
	public function addUser()
	{
		$rules = [
			'role'     => 'integer|min:1',
			'name'     => 'required|min:4',
			'email'    => 'required|email|unique:user,username',
			'phone'    => 'required|phone',
			'password' => 'required|min:6|confirmed'
		];
		$messages['phone.phone'] = 'Phone number must be a valid 10 digits number';
		$messages['role.min'] = 'Please select a valid role';
		$messages['email.unique'] = 'This email address is already exist';
		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->messages()->first())->withInput();
		}
		$userData = [
			'name'     => Input::get('name'),
			'username' => Input::get('email'),
			'phone'    => Input::get('phone'),
			'password' => Hash::make(Input::get('password')),
			'role_id'  => Input::get('role')
		];

		$newUser = User::create($userData);
		if ($newUser)
		{
			return redirect()->route('user.add.details.show', $newUser->id);
		}
	}

	public function showAddUserDetails($userId)
	{
		$user = User::find($userId);
		if ($user)
		{
			return view('user.add_user_details', compact('user'));
		}
		else
		{
			return redirect()->route('user.list');
		}
	}

	public function saveUserDetails($userId, Request $request)
	{
		// @todo: see whether this is being used anywhere and remove if not required

		$user = User::findorFail($userId);

		$rules = [
			'account_number' => 'required|integer|digits_between:9,17',
			'name'           => 'required|min:3',
			'ifsc_code'      => 'required|alpha_num|size:11',
			'bank_name'      => 'required|min:3',
			'branch_code'    => 'required|integer|min:4'
		];

		$validator = validator($request->all(), $rules);
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		// collect data
		$accountNo = trim($request->input('account_number'));
		$name = trim($request->input('name'));
		$ifscCode = trim($request->input('ifsc_code'));
		$bankName = trim($request->input('bank_name'));
		$branchCode = trim($request->input('branch_code'));
		$branchName = trim($request->input('branch_name'));
		$micrCode = trim($request->input('micr_code'));
		$branchAddress = $request->input('branch_address');

		if ($user->bankDetails)
		{
			$user->bankDetails->update([
				                           'account_number' => $accountNo,
				                           'name_in_bank'   => $name,
				                           'bank_name'      => $bankName,
				                           'branch_name'    => $branchName,
				                           'branch_code'    => $branchCode,
				                           'branch_address' => $branchAddress,
				                           'ifsc_code'      => $ifscCode,
				                           'micr_code'      => $micrCode
			                           ]);
		}
		else
		{
			BankDetails::create([
				                    'user_id'        => $userId,
				                    'account_number' => $accountNo,
				                    'name_in_bank'   => $name,
				                    'bank_name'      => $bankName,
				                    'branch_name'    => $branchName,
				                    'branch_code'    => $branchCode,
				                    'branch_address' => $branchAddress,
				                    'ifsc_code'      => $ifscCode,
				                    'micr_code'      => $micrCode
			                    ]);
		}

		return redirect()->route('user.list')->with('successMsg', 'User details saved successfully');
	}

	public function showChangePassword($userId)
	{
		$user = User::findOrFail($userId);

		return view('user.change-password', compact('user'));
	}

	public function saveChangePassword($userId, Request $request)
	{
		$user = User::findOrFail($userId);
		$validator = validator($request->all(), ['password' => 'required|min:6|confirmed']);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		if ($user->update(['password' => Hash::make(trim($request->input('password')))]))
		{
			return redirect()->route('user.list')->with('successMsg', 'User Password has successfully changed');
		}
	}

	// update user
	public function updateUser($userId)
	{
		$res = ['success' => false];
		$user = User::find($userId);

		$rules = [
			'name'  => 'required|min:4',
			'phone' => 'phone',
			'email' => 'required|email|unique:user,username,' . $userId
		];

		$messages['phone.phone'] = "Phone number must be a valid 10 digits number";
		$messages['email.unique'] = 'This email address is already exist';

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validator->messages()->first()
			];
		}
		else
		{
			$userData = [
				'name'     => Input::get('name'),
				'username' => Input::get('email'),
				'phone'    => Input::get('phone'),
				'role_id'  => Input::get('role')
			];

			if ($user->update($userData))
			{
				$res = ['success' => true];
			}
		}

		return response()->json($res);
	}

	// show role
	public function showRoleList()
	{
		$roles = Role::all();

		return view('user.show_roles', compact('roles'));
	}

	public function showAddRole()
	{
		return view('user.add_role');
	}

	public function saveRole()
	{
		$rules = [
			'name' => 'required|min:4',
			'info' => 'required|min:10'
		];

		$messages = [
			'info.required' => "Description field is required",
			'info.min'      => "Description field must be 10 characters long"
		];

		$validator = Validator::make(Input::all(), $rules, $messages);
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->messages()->first())->withInput();
		}

		$roleData = [
			'name' => Input::get('name'),
			'info' => Input::get('info'),
		];

		if (Role::create($roleData))
		{
			return redirect()->route('user.role.list')->with('successMsg', 'New role added successfully');
		}

		return redirect()->route('user.role.list');
	}

	public function updateRole($id)
	{
		$res['success'] = false;
		$role = Role::find($id);

		$rules = [
			'name' => 'required|min:4',
			'info' => 'required|min:10',
		];

		$roleData = [
			'name' => Input::get('name'),
			'info' => Input::get('info')
		];
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validator->messages()->first()
			];
		}
		else
		{
			if ($role->update($roleData))
			{
				$res = ['success' => true];
			}
		}

		return response()->json($res);
	}

	public function deleteRole($id)
	{
		$role = Role::find($id);
		if ($role->delete())
		{
			return redirect()->back()->with('successMsg', 'Role deleted successfully');
		}

		return redirect()->back();
	}
}
