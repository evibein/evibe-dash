<?php

namespace App\Http\Controllers\Coupon;

use App\Http\Controllers\Base\BaseController;
use App\Models\Coupon\Coupon;
use App\Models\Coupon\CouponMappings;
use App\Models\Coupon\LogCouponUsage;
use App\Models\TypeEvent;
use App\Models\User;

class CouponController extends BaseController
{
	public function showRedeemed()
	{
		$bookings = \TicketBooking::select('ticket_bookings.booking_amount', 'ticket_bookings.party_date_time', 'ticket_bookings.discount_amount',
		                                   'ticket_bookings.coupon_id', 'ticket.name AS customerName', 'ticket.city_id AS cityId',
		                                   'ticket.event_id AS eventId')
		                          ->leftJoin('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
		                          ->where('is_advance_paid', 1)
		                          ->whereIn('ticket.status_id', [config("evibe.status.booked"), config("evibe.status.auto_pay"), config("evibe.status.service_auto_pay")])
		                          ->whereNull('ticket.deleted_at')
		                          ->whereNotNull('coupon_id')
		                          ->get()
		                          ->toArray();

		$cities = \City::all()
		               ->pluck('name', 'id')
		               ->toArray();

		$occasions = TypeEvent::all()
		                      ->pluck('name', 'id')
		                      ->toArray();

		$coupons = Coupon::all()
		                 ->pluck('coupon_code', 'id')
		                 ->toArray();

		$data = [
			"totalBookingAmount"    => array_sum(array_column($bookings, 'booking_amount')),
			"totalDiscountAmount"   => array_sum(array_column($bookings, 'discount_amount')),
			"totalRedeemedBookings" => count($bookings),
			"bookings"              => $bookings,
			"cities"                => $cities,
			"occasions"             => $occasions,
			"coupons"               => $coupons
		];

		return view('coupon/redeemed', ['data' => $data]);
	}

	public function showAvailableCoupons()
	{
		$coupons = Coupon::whereNull('usage_done_at')
		                 ->get()
		                 ->toArray();

		$couponMappings = CouponMappings::whereIn('coupon_id', array_column($coupons, 'id'))
		                                ->get()
		                                ->toArray();

		$sortedCouponMappings = [];
		foreach ($couponMappings as $couponMapping)
		{
			$sortedCouponMappings[$couponMapping["coupon_id"]][] = $couponMapping;
		}

		$data = [
			"coupons"              => $coupons,
			"sortedCouponMappings" => $sortedCouponMappings
		];

		return view('coupon/available', ['data' => $data]);
	}

	public function showTrails()
	{
		$logCouponUsage = LogCouponUsage::orderBy('created_at', 'DESC')
		                                ->get()
		                                ->toArray();

		$cities = \City::all()
		               ->pluck('name', 'id')
		               ->toArray();

		$occasions = TypeEvent::all()
		                      ->pluck('name', 'id')
		                      ->toArray();

		$users = User::whereIn('id', array_column($logCouponUsage, 'user_id'))
		             ->get()
		             ->pluck('username', 'id')
		             ->toArray();

		$data = [
			"logCouponUsage" => $logCouponUsage,
			"cities"         => $cities,
			"occasions"      => $occasions,
			"users"          => $users
		];

		return view('coupon/trails', ['data' => $data]);
	}
}