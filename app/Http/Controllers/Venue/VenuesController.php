<?php

namespace App\Http\Controllers\Venue;

use App\Http\Controllers\Base\BaseController;
use App\Models\InternalQuestion;
use App\Models\Partner\PartnerDocument;
use App\Models\Partner\VenueDocuments;
use App\Models\User;
use App\Models\VenueHallEvent;
use Evibe\Facades\AppUtilFacade as AppUtil;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VenuesController extends BaseController
{
	/**
	 * Show list of all venues
	 */
	public function getIndex()
	{
		$prefilledLocs = [];
		$hallTypeSelected = [];
		$venueTypeSelected = [];
		$locationSuggestions = [];
		$typeVenueHall = \TypeVenueHall::all();
		$venueType = \TypeVenue::all();
		$locationsList = \Area::pluck('name');
		$priceMinSelected = null;
		$priceMaxSelected = null;
		$capMinSelected = null;
		$capMaxSelected = null;

		$venueHalls = \VenueHall::join('venue', 'venue_hall.venue_id', '=', 'venue.id')
		                        ->with('venue', 'type', 'venue.area')
		                        ->where('venue.is_live', 1)
		                        ->whereNull('venue.deleted_at')
		                        ->select('venue_hall.id', 'venue_hall.name', 'venue_hall.code', 'venue_hall.url',
		                                 'venue_hall.floor', 'venue_hall.cap_min', 'venue_hall.cap_max',
		                                 'venue_hall.cap_float', 'venue_hall.venue_id', 'venue_hall.type_id',
		                                 'venue_hall.created_at', 'venue_hall.updated_at', 'venue_hall.deleted_at')
		                        ->orderBy('created_at', 'DESC');

		// search
		if (Input::has('query') && $searchQuery = Input::get('query'))
		{
			$percentQuery = '%' . $searchQuery . '%';
			$venueHalls = $venueHalls->where(function ($query) use ($percentQuery) {
				$query->where('venue_hall.name', 'LIKE', $percentQuery)
				      ->orwhere('venue_hall.code', 'LIKE', $percentQuery)
				      ->orwhere('venue.name', 'LIKE', $percentQuery);
			});

		}

		// city filter
		$city = Input::has('city') ? Input::get('city') : \City::first()->id;
		if ($city)
		{
			$venueHalls->where('venue.city_id', $city);
		}

		// location filter
		if (Input::has('location') && Input::get('location'))
		{
			$selectedAreaIds = [];
			$locationList = explode(",", urldecode(Input::get('location')));
			$venueIds = null;

			foreach ($locationList as $key => $value)
			{
				$selectedArea = \Area::where('name', $value)->first();
				$areaId = $selectedArea->id;
				array_push($prefilledLocs, $value);
				array_push($selectedAreaIds, $areaId);

				if ($key == 0)
				{
					$venueIds = \Venue::select('id')->where('area_id', $areaId);
				}
				else
				{
					$venueIds->orWhere('area_id', '=', $areaId);
				}

				// location suggestions
				foreach ($selectedArea->getNearbyAreas() as $nearbyAreaId)
				{
					array_push($locationSuggestions, $nearbyAreaId);
				}
			}

			$venueIds = $venueIds->get()->toArray();
			$venueHalls->whereIn('venue_id', $venueIds);
			$locationSuggestions = \Area::whereIn('id', array_unique($locationSuggestions))
			                            ->whereNotIn('id', $selectedAreaIds)->pluck('name');
		}

		// hall type filter
		if (Input::has('hallType') && Input::get('hallType'))
		{
			$hallTypeSelected = explode(",", Input::get('hallType'));
			$venueHalls->whereIn('venue_hall.type_id', $hallTypeSelected);
		}

		//venue type filter
		if (Input::has('venueType') && Input::get('venueType'))
		{
			$venueTypeSelected = explode(",", Input::get('venueType'));
			$venueHalls->whereIn('venue.type_id', $venueTypeSelected);
		}

		// capacity filter
		if (Input::has('capacityRange') && Input::get('capacityRange'))
		{
			$capacityRanges = Input::get('capacityRange');
			$capacityRanges = explode(",", $capacityRanges);
			$capMinSelected = trim($capacityRanges[0]) ? trim($capacityRanges[0]) : null;
			$capMaxSelected = trim($capacityRanges[1]) ? trim($capacityRanges[1]) : null;

			// min is set
			if (!is_null($capMinSelected))
			{
				$venueHalls->where('venue_hall.cap_min', '<=', $capMinSelected);
			}

			// max is set, consider `cap_float` for maximums
			if (!is_null($capMaxSelected))
			{
				$venueHalls->where('venue_hall.cap_float', '>=', $capMaxSelected);
			}
		}

		// price filter
		if (Input::has('priceRange') && Input::get('priceRange'))
		{
			$ids = [];
			$priceRange = Input::get('priceRange');
			$priceRangeList = explode(",", $priceRange);
			$priceMinSelected = trim($priceRangeList[0]) ? trim($priceRangeList[0]) : null;
			$priceMaxSelected = trim($priceRangeList[1]) ? trim($priceRangeList[1]) : null;

			// gives values are numbers
			$venueIdsMatchingPrice = \Venue::select('id')->distinct();

			if (!is_null($priceMinSelected) && !is_null($priceMaxSelected))
			{

				$ids = $venueIdsMatchingPrice->where('price_min_veg', '>=', $priceMinSelected)
				                             ->where('price_min_veg', '<=', $priceMaxSelected)
				                             ->get()->toArray();

			}
			elseif (is_null($priceMinSelected) && !is_null($priceMaxSelected))
			{

				$ids = $venueIdsMatchingPrice->where('price_min_veg', '<=', $priceMaxSelected)
				                             ->get()->toArray();

			}
			elseif (!is_null($priceMinSelected) && is_null($priceMaxSelected))
			{

				$ids = $venueIdsMatchingPrice->where('price_min_veg', '>=', $priceMinSelected)
				                             ->get()->toArray();
			}

			if (count($ids))
			{
				$venueHalls->whereIn('venue_id', $ids);
			}
		}

		$venueLocation = [];
		$tagList = [];
		$data = [
			'venueType'           => $venueType,
			'typeVenueHall'       => $typeVenueHall,
			'venueHall'           => $venueHalls->paginate(20),
			'prefilledLocs'       => $prefilledLocs,
			'hallTypeSelected'    => $hallTypeSelected,
			'venueTypeSelected'   => $venueTypeSelected,
			'capMinSelected'      => $capMinSelected,
			'capMaxSelected'      => $capMaxSelected,
			'priceMinSelected'    => $priceMinSelected,
			'priceMaxSelected'    => $priceMaxSelected,
			'locationsList'       => $locationsList,
			'locationSuggestions' => $locationSuggestions,
			'venueLocation'       => $venueLocation,
			'tagList'             => $tagList,
			'cities'              => \City::all()
		];

		return view('venues/venues', ['data' => $data]);
	}

	/**
	 * SHOW ADD NEW VENUE FORM
	 */
	public function getNew()
	{
		$data = [
			'cities'    => \City::all(),
			'areas'     => \Area::orderBy('name')->get(),
			'typeVenue' => \TypeVenue::all()
		];

		return view('venues/add_new_venues', ['data' => $data]);
	}

	/**
	 * POST NEW VENUE
	 */
	public function postCreateVenues()
	{
		$userInput = Input::all();

		// validate rules
		$rules = [

			'venueName'                  => 'required|min:4|unique:venue,name',
			'venueUsername'              => 'required|email|unique:user,username',
			'venuePassword'              => 'required|min:6|confirmed',
			'venuePassword_confirmation' => 'required',
			'typeVenue'                  => 'required',
			'venuePerson'                => 'required|min:3',
			'venuePhone'                 => 'required|phone',
			'venueIsVerified'            => 'required',
			'venueIsPartner'             => 'required',
			'altEmail1'                  => 'email',
			'altEmail2'                  => 'email',
			'altPhone'                   => 'phone|required'
		];

		$messages = [

			'venueName.required'                  => 'Please enter company name',
			'venueName.unique'                    => 'This Venue name is already taken. Try appending location.',
			'venueUsername.required'              => 'Please enter username',
			'venueUsername.email'                 => 'Username is not a valid email address',
			'venueUsername.unique'                => 'This username has already been taken',
			'venuePassword.required'              => 'Password is required',
			'venuePassword.min'                   => 'Password should be minimum 6 characters',
			'venuePassword.confirmed'             => 'Password and confirm password do not match',
			'venuePassword_confirmation.required' => 'Confirm password is required',
			'venuePerson.required'                => 'Please enter contact person name',
			'venuePerson.min'                     => 'Contact person name must be minimum 4 characters long',
			'venuePhone.required'                 => 'Please enter phone number',
			'venuePhone.phone'                    => 'Please enter valid phone number',
			'venueIsVerified.required'            => 'Please check for verified',
			'venueIsPartner.required'             => 'Please check for partner',
			'altEmail1.email'                     => 'Alternative email 1 must be a valid email address',
			'altEmail2.email'                     => 'Alternative email 2 must be a valid email address',
			'altPhone.phone'                      => 'Alternative phone must be a valid 10 digit phone number'

		];

		$validator = Validator::make($userInput, $rules, $messages);

		if ($validator->fails())
		{
			return redirect('/venues/new')->withErrors($validator)->withInput();
		}
		else
		{
			$roleId = config('evibe.roles.venue');
			$typeVenueId = Input::get('typeVenue');

			$userInsertData = [
				'username'   => Input::get('venueUsername'),
				'password'   => Hash::make(Input::get('venuePassword')),
				'role_id'    => $roleId,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			$newUser = User::create($userInsertData);

			if ($newUser)
			{
				$cityId = Input::get('city');
				$areaId = Input::get('area');
				$venueCode = self::generateVenueCode($typeVenueId);
				$venueUrl = self::generateVenueUrl($typeVenueId, $areaId);

				$venueInsertData = [

					'name'        => Input::get('venueName'),
					'person'      => Input::get('venuePerson'),
					'phone'       => Input::get('venuePhone'),
					'email'       => Input::get('venueUsername'),
					'user_id'     => $newUser->id,
					'code'        => $venueCode,
					'city_id'     => $cityId,
					'area_id'     => $areaId,
					'url'         => $venueUrl,
					'type_id'     => $typeVenueId,
					'alt_email_1' => Input::get('altEmail1'),
					'alt_email_2' => Input::get('altEmail2'),
					'alt_phone'   => Input::get('altPhone'),
					'is_partner'  => Input::get('venueIsPartner'),
					'is_verified' => Input::get('venueIsVerified'),
					'created_at'  => date('Y-m-d H:i:s'),
					'updated_at'  => date('Y-m-d H:i:s')

				];

				$newVenues = \Venue::create($venueInsertData);

				if (!$newVenues)
				{
					// @todo: delete user, venue
					return redirect('/venues/new')->withErrors($validator)->withInput();
				}
				else
				{
					return redirect('/venues?new=true');
				}

			}
			else
			{
				// @todo: delete the user
				return redirect('/venues/new')->withErrors($validator)->withInput();
			}
		}
	}

	/**
	 * GENERATE VENUE CODE BASED ON TYPE
	 */
	public function generateVenueCode($typeId, $lastCode = null)
	{
		$venuePrefix = config('evibe.code.prefix.venue');
		$typeObj = \TypeVenue::findOrFail($typeId);

		if (!$lastCode)
		{
			$lastObj = \Venue::withTrashed()
			                 ->where('type_id', $typeId)
			                 ->orderBy('code', 'DESC')
			                 ->first();
			if (!$lastObj)
			{
				$lastCode = $typeObj->default_code;
			}
			else
			{
				$lastCode = $lastObj->code;
			}

			$digits = ((int)substr($lastCode, -4)) + 1;
		}
		else
		{
			$digits = (int)substr($lastCode, -4);
		}

		$digits = str_pad($digits, 4, '0', STR_PAD_LEFT);

		return $venuePrefix . $typeObj->code . $digits;
	}

	/**
	 * GENERATE URL
	 */
	public function generateVenueUrl($typeVenueId, $areaId)
	{
		$area = \Area::findOrFail($areaId);
		$areaName = strtolower($area->name);
		$areaName = str_replace(' ', '-', $areaName);

		$vt = \TypeVenue::findOrFail($typeVenueId);
		$venueTypeName = strtolower($vt->name);
		$venueTypeName = str_replace(' ', '-', $venueTypeName);

		$urlTpl = config('evibe.urls.venue');
		$replaces = [
			'#field1#' => time(),
			'#field2#' => $venueTypeName,
			'#field3#' => $areaName
		];

		$url = str_replace(array_keys($replaces), array_values($replaces), $urlTpl);

		return $url;
	}

	public function showVenueHall($hallId)
	{
		$venueHall = \VenueHall::with('type', 'venue', 'venue.area')
		                       ->find($hallId);

		if (!$venueHall)
		{
			abort(404);
		}

		$venueId = $venueHall->venue_id;
		$cuisineNames = [];
		$cuisineList = \VenueCuisine::with('type')
		                            ->where('venue_id', $venueId)
		                            ->get();

		foreach ($cuisineList as $cuisine)
		{
			$cuisineNames[] = $cuisine->type->name;
		}

		$cuisineNames = implode(',', $cuisineNames);
		$venueHallGallery = \VenueHallGallery::where('venue_hall_id', $hallId)->get();
		$venueMenu = \VenueMenu::where('venue_id', $venueId)->get();

		$iqs = InternalQuestion::withTrashed()
		                       ->orWhere(['map_id' => $hallId, 'map_type_id' => config('evibe.ticket_type.venue_halls')])
		                       ->orWhere(['map_id' => $venueId, 'map_type_id' => config('evibe.ticket_type.venues')])
		                       ->orderBy('deleted_by')
		                       ->orderBy('updated_at', 'DESC')->get();

		$eventSupported = VenueHallEvent::where('venue_hall_id', $hallId)->get();

		$data = [
			'venue'            => $venueHall,
			'venueCuisine'     => $cuisineNames,
			'venueHallGallery' => $venueHallGallery,
			'venueMenuDetails' => $venueMenu,
			'iqs'              => $iqs,
			'eventSupported'   => $eventSupported
		];

		return view('/venues/hall', $data);
	}

	public function showVenueDetails($venueId)
	{
		$Venue = \Venue::where('id', $venueId);
		$Venue = $Venue->with('venueType');
		$Venue = $Venue->with('area');
		$VenueCuisine = \VenueCuisine::where('venue_id', $venueId)->get();
		$Cuisine = [];

		foreach ($VenueCuisine as $cuisineId)
		{
			$cuisineTypeId = $cuisineId->type_id;
			$Cuisine[] = \TypeVenueCuisine::where('id', $cuisineTypeId)->get();
		}

		$CuisineName = [];

		foreach ($Cuisine as $CuisineValue)
		{
			$Cuisine = json_decode($CuisineValue);

			if (!empty($Cuisine))
			{
				$CuisineName[] = $Cuisine[0]->name;
			}
		}

		if (!empty($CuisineName))
		{
			$CuisineName = implode(',', $CuisineName);
		}

		$VenueGallery = \VenueGallery::where('venue_id', $venueId)
		                             ->orderBy('updated_at', 'DESC')
		                             ->get();

		$VenueMenu = \VenueMenu::where('venue_id', $venueId)
		                       ->get();

		$VenueHall = \VenueHall::where('venue_id', $venueId)
		                       ->with('type')
		                       ->get();

		// set all venues gallery image path
		$imgUrl = [];
		$i = 0;
		if ($VenueGallery)
		{
			if (!empty($VenueGallery))
			{
				foreach ($VenueGallery as $value)
				{
					$imgUrl[$i]['type'] = 'Venue';
					$imgUrl[$i]['url'] = AppUtil::getGalleryBaseUrl() . '/venues/' . $venueId . '/images/' . $value->url;
					$imgUrl[$i]['title'] = $value->title;
					$i++;
				}
			}
		}

		$HallArrayImg = [];
		if ($VenueHall)
		{
			if (!empty($VenueHall))
			{
				foreach ($VenueHall as $value)
				{
					$HallArrayImg[] = \VenueHallGallery::select('venue_hall_id', 'url', 'id', 'title')->where('venue_hall_id', $value->id)
					                                   ->orderBy('updated_at', 'DESC')
					                                   ->get();

				}
			}
		}

		if ($i == 0)
		{
			$j = 0;
		}
		else
		{
			$j = $i;
		}

		// set all venues halls gallery image path and push to same array
		if (!empty($HallArrayImg))
		{
			foreach ($HallArrayImg as $valueArr)
			{
				if (!empty($valueArr))
				{
					foreach ($valueArr as $value)
					{
						if (!empty($value))
						{
							if ($value->id && $value->url)
							{
								$imgUrl[$j]['type'] = 'hall';
								$imgUrl[$j]['url'] = AppUtil::getGalleryBaseUrl() . '/venues/' . $venueId . '/halls/' . $value->venue_hall_id . '/images/' . $value->url;
								$imgUrl[$j]['title'] = $value->title;
								$j++;
							}
						}
					}
				}
			}
		}

		$iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $venueId, 'map_type_id' => config('evibe.ticket_type.venues')])
		                       ->orderBy('deleted_by')
		                       ->orderBy('updated_at', 'DESC')
		                       ->get();

		$documents = PartnerDocument::where('partner_id', '=', $venueId)
		                            ->get();

		$Venue = $Venue->get();
		$data = [
			'venue'        => $Venue,
			'VenueCuisine' => $CuisineName,
			'VenueGallery' => $VenueGallery,
			'imgUrl'       => $imgUrl,
			'VenueMenu'    => $VenueMenu,
			'venueHall'    => $VenueHall,
			'iqs'          => $iqs,
			'documents'    => $documents,
			'venueId'      => $venueId
		];

		return view('/venues/view')->with('data', $data);
	}

	public function deleteDocument($documentId)
	{
		PartnerDocument::where('id', '=', $documentId)->delete();

		return redirect()->back();
	}

	public function getCuisineIds($cuisineString)
	{
		$cuisineIds = [];
		$cuisineList = explode(",", $cuisineString);

		foreach ($cuisineList as $cuisineName)
		{
			$cuisine = \TypeVenueCuisine::where('name', $cuisineName)->first();

			if ($cuisine)
			{
				$cuisineIds[] = $cuisine->id;
			}
		}

		return $cuisineIds;
	}

	// Delete a hall
	public function deleteVenue($id)
	{
		$venue = \Venue::find($id);

		if ($venue && $venue->delete())
		{
			$VenueHall = \VenueHall::where('venue_id', $id);
			if ($VenueHall && $VenueHall->delete())
			{
				return redirect('/venues?delete=true');
			}

			// redirect venue
			return redirect('/venues?delete=true');
		}
		else
		{
			return redirect('/venues?delete=false');
		}
	}

	/**
	 * SHOW EDIT VENUE FORM
	 */
	public function getEdit($venueId)
	{
		$venue = \Venue::findOrFail($venueId);
		$userInfo = User::findOrFail($venue->user_id);
		$handlers = User::where('role_id', '=', 4)->get();

		$data = [
			'venueTypes' => \TypeVenue::all(),
			'source'     => \TypeTicketSource::all(),
			'leadStatus' => \TypeLeadStatus::all(),
			'cities'     => \City::all(),
			'areas'      => \Area::orderBy('name')->get(),
			'handlers'   => $handlers,
			'venue'      => $venue,
			'userInfo'   => $userInfo
		];

		return view('venues/edit_venues', ['data' => $data]);
	}

	/**
	 * EDIT VENUE INFORMATION
	 */
	public function postUpdateInfo($venueId)
	{
		$userInput = Input::all();
		$venue = \Venue::findOrFail($venueId);

		$longitude = Input::get('vMappingLongitude') ? Input::get('vMappingLongitude') : null;
		$latitude = Input::get('vMappingLatitude') ? Input::get('vMappingLatitude') : null;

		// validate rules
		$rules = [
			'venueName'      => 'required|unique:venue,name,' . $venueId,
			'venueType'      => 'required',
			'venuePerson'    => 'required',
			'venuePhone'     => 'required|phone',
			'altEmail1'      => 'email',
			'altEmail2'      => 'email',
			'altPhone'       => 'phone',
			'commissionRate' => 'required'
		];

		$messages = [
			'venueName.required'      => 'Please enter company name',
			'venueName.unique'        => 'This vendor name is already taken. Try appending location.',
			'venuePerson.required'    => 'Please enter contact person name',
			'venuePhone.required'     => 'Please enter phone number',
			'venuePhone.phone'        => 'Please enter valid phone number',
			'altEmail1.email'         => 'Alternative email 1 must be a valid email address',
			'altEmail2.email'         => 'Alternative email 2 must be a valid email address',
			'altPhone.phone'          => 'Alternative phone must be a valid 10 digit phone number',
			'commissionRare.required' => 'please enter commission rate'
		];

		if ($venue->is_live)
		{
			$rules['vMappingLongitude'] = 'required|numeric';
			$rules['vMappingLatitude'] = 'required|numeric';
			$rules['vMappingAddress'] = 'required';
			$rules['vMappingLandmark'] = 'required';
			$messages['vMappingLongitude.required'] = 'Please enter Longitude value';
			$messages['vMappingLatitude.required'] = 'Please enter latitude value';
			$messages['vMappingAddress.required'] = 'Please enter Address';
			$messages['vMappingLandmark.required'] = 'Please enter landmark';
			$messages['vMappingLongitude.numeric'] = 'Longitude should be number only';
			$messages['vMappingLatitude.numeric'] = 'Latitude should be number only';
		}

		$validator = Validator::make($userInput, $rules, $messages);

		if ($validator->fails())
		{
			return redirect('/venues/edit/' . $venueId)->withErrors($validator)->withInput();
		}
		else
		{
			$venueType = Input::get('venueType');

			$cityId = Input::get('city');
			$areaId = Input::get('area');

			$venue->is_partner = Input::get('venueIsPartner');
			$venue->is_verified = Input::get('venueIsVerified');
			$venue->name = Input::get('venueName');
			$venue->person = Input::get('venuePerson');
			$venue->phone = Input::get('venuePhone');
			$venue->city_id = $cityId;
			$venue->area_id = $areaId;
			$venue->updated_at = date('Y-m-d H:i:s');
			$venue->alt_email_1 = Input::get('altEmail1');
			$venue->alt_email_2 = Input::get('altEmail2');
			$venue->alt_phone = Input::get('altPhone');
			$venue->source_id = Input::get('source');
			$venue->lead_status_id = Input::get('leadStatus');
			$venue->source_specific = Input::get('sourceSpecific');
			$venue->facebook_url = Input::get('sourceFacebook');
			$venue->google_plus_url = Input::get('sourceGooglePlus');
			$venue->instagram_url = Input::get('sourceInstagram');
			$venue->comments = Input::get('comments');
			$venue->commission_rate = Input::get('commissionRate');
			$venue->map_long = $longitude;
			$venue->map_lat = $latitude;
			$venue->full_address = Input::get('vMappingAddress');
			$venue->landmark = Input::get('vMappingLandmark');

			if ($venue->type_id != $venueType)
			{
				$venue->type_id = $venueType;
				$venue->code = self::generateVenueCode($venueType); // generate new code
				$venue->url = self::generateVenueUrl($venueType, $areaId);
			}

			$venue->save();

			return redirect(route("venues.view", $venue->id));
		}
	}

	// Delete a hall
	public function deleteHall($hallId)
	{
		$hall = \VenueHall::find($hallId);

		if ($hall && $hall->delete())
		{
			// redirect hall
			return redirect('/venues?delete=true');
		}
		else
		{
			return redirect('/venues?delete=false');
		}
	}

	// Make Active a Venue
	public function setVenueActive($id)
	{
		$venue = \Venue::find($id);

		if ($venue)
		{
			$venue->update(['is_live' => 1]);

			return redirect('/venues?active=true'); // redirect venue
		}
		else
		{
			return redirect('/venues?active=false');
		}
	}

	// Make In-active a Venue
	public function setVenueInActive($id)
	{
		$venue = \Venue::find($id);

		if ($venue && $venue->update(['is_live' => 0]))
		{
			$this->removeItemsFromAutoBookAndPriorityTable(config('evibe.ticket_type.venues'), $id);

			return redirect('/venues?inactive=true'); // redirect venue
		}
		else
		{
			return redirect('/venues?inactive=false');
		}
	}

	// Make In-active a Venue
	public function showVenueList()
	{
		$query = Input::get('query');
		$venueList = \Venue::orderBy('venue.created_at', 'DESC')
		                   ->join('type_venue', 'venue.type_id', '=', 'type_venue.id')
		                   ->whereNull('type_venue.deleted_at')
		                   ->select('venue.id', 'venue.phone', 'venue.email', 'venue.person',
		                            'venue.is_live', 'venue.name', 'venue.code', 'type_venue.name as type',
		                            'venue.created_at', 'venue.updated_at', 'venue.deleted_at');

		if ($query)
		{
			$venueList = $venueList->where(function ($q) use ($query) {
				$q->where('venue.name', 'LIKE', '%' . $query . '%')
				  ->orwhere('venue.phone', 'LIKE', '%' . $query . '%')
				  ->orwhere('venue.person', 'LIKE', '%' . $query . '%')
				  ->orwhere('venue.email', 'LIKE', '%' . $query . '%')
				  ->orwhere('venue.code', 'LIKE', '%' . $query . '%')
				  ->orwhere('type_venue.name', 'LIKE', '%' . $query . '%');
			});
		}
		// city filter
		$city = Input::has('city') ? Input::get('city') : \City::first()->id;
		if ($city)
		{
			$venueList->where('venue.city_id', $city);
		}

		$data = [
			'venues' => $venueList->get(),
			'cities' => \City::all()
		];

		return view('venues/venues_list', ['data' => $data]);

	}
}