<?php namespace App\Http\Controllers\Venue;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class VenueExtRatingsController extends BaseController
{

	public function viewRatings($id)
	{
		$venueId = $id;
		$venueRatings = [];

		$venueRatings = \Venue::orderBy('created_at', 'DESC');
		$venueRatings = $venueRatings->join('venue_ext_ratings', 'venue.id', '=', 'venue_ext_ratings.venue_id');
		$venueRatings = $venueRatings->join('type_ratings_website', 'venue_ext_ratings.type_ratings_website_id', '=', 'type_ratings_website.id')->where('venue.id', $venueId)->whereNull('venue_ext_ratings.deleted_at')
		                             ->select('venue.id as venue_id', 'venue_ext_ratings.id', 'venue_ext_ratings.url', 'venue_ext_ratings.rating_value', 'venue_ext_ratings.rating_count', 'venue_ext_ratings.rating_max', 'venue_ext_ratings.type_ratings_website_id', 'venue_ext_ratings.created_at', 'venue_ext_ratings.deleted_at', 'type_ratings_website.name')
		                             ->get();

		$ratingsData = [
			'venueId'      => $venueId,
			'venueRatings' => $venueRatings
		];

		//print_r($ratingsData);
		//echo count($data['venueRatings']);
		//exit;

		return View::make('venues/ext-ratings/view')->with('data', $ratingsData);
	}

	public function editRatings($venue_id)
	{
		$venueId = $venue_id;
		$venue = \Venue::findOrFail($venueId);
		$venueRatings = \VenueExtRatings::where('venue_id', $venue->id)->get();
		$typeExtRatings = \TypeExtRatings::all();

		$ratingsData = [
			'venueId'        => $venueId,
			'venue'          => $venue,
			'venueRatings'   => $venueRatings,
			'typeExtRatings' => $typeExtRatings
		];

		return View::make('venues/ext-ratings/edit')->with('data', $ratingsData);
	}

	public function updateRatings($venueId)
	{

		$extRows = Input::get('extRow');
		$insertRows = [];

		foreach ($extRows as $key => $extRow)
		{
			$rowInput = [];

			$rules = [
				'extUrl'         => 'required|url',
				'extRatingValue' => 'required|numeric|min:0',
				'extRatingCount' => 'required|integer',
				'extRatingMax'   => 'required|integer'
			];

			$messages = [
				'extUrl.required'         => 'All values are required for inclusion (Ext URL, Rating Value, Rating Count, Rating Max)',
				'extUrl.url'              => 'Ext URL is not valid (should contain http://)',
				'extRatingValue.required' => 'All values are required for inclusion (Ext URL, Rating Value, Rating Count, Rating Max)',
				'extRatingValue.numeric'  => 'Rating value must be a number (if not available input 0)',
				'extRatingValue.min'      => 'Rating value must be a valid number (if not available input 0)',
				'extRatingCount.required' => 'All values are required for inclusion (Ext URL, Rating Value, Rating Count, Rating Max)',
				'extRatingCount.integer'  => 'Rating count must be an integer (if not available input 0)',
				'extRatingMax.required'   => 'All values are required for inclusion (Ext URL, Rating Value, Rating Count, Rating Max)',
				'extRatingMax.integer'    => 'Rating max must be an integer (if not available input 0)',
			];

			if ($extRow['extId'])
			{
				$rowInput['extId'] = trim($extRow['extId']);
			}
			if ($extRow['extUrl'] !== "")
			{
				$rowInput['extUrl'] = trim($extRow['extUrl']);
			}
			if ($extRow['extRatingValue'] !== "")
			{
				$rowInput['extRatingValue'] = trim($extRow['extRatingValue']);
			}
			if ($extRow['extRatingCount'] !== "")
			{
				$rowInput['extRatingCount'] = trim($extRow['extRatingCount']);
			}
			if ($extRow['extRatingMax'] !== "")
			{
				$rowInput['extRatingMax'] = trim($extRow['extRatingMax']);
			}

			if (count($rowInput) > 0)
			{
				$validation = Validator::make($rowInput, $rules, $messages);

				if ($validation->fails())
				{
					return Redirect::route('venueExtRatingsEdit', $venueId)->withErrors($validation)->withInput();
				}
				else
				{
					$insertRows[$key] = $rowInput;
				}
			}
		}

		foreach ($insertRows as $key => $extRow)
		{

			$venueExtRating = \VenueExtRatings::where('venue_id', $venueId)
			                                  ->where('type_ratings_website_id', $key)
			                                  ->first();

			if (!$venueExtRating)
			{
				$venueExtRating = new \VenueExtRatings;
				$venueExtRating->venue_id = $venueId;
				$venueExtRating->type_ratings_website_id = $key;
			}
			if (!empty($extRow['extId']))
			{
				$venueExtRating->id = $extRow['extId'];
			}
			$venueExtRating->url = $extRow['extUrl'];
			$venueExtRating->rating_value = $extRow['extRatingValue'];
			$venueExtRating->rating_count = $extRow['extRatingCount'];
			$venueExtRating->rating_max = $extRow['extRatingMax'];
			$venueExtRating->updated_at = date('Y-m-d H:i:s');
			$venueExtRating->save();
		}

		return Redirect::route('venueExtRatings', $venueId);
	}

	public function deleteRating()
	{

		$ratingVenueId = Input::get('ratingVenueId');
		$ratingRowId = Input::get('ratingRowId');

		$venueRating = \VenueExtRatings::find($ratingRowId);

		if ($venueRating && $venueRating->delete())
		{
			return Redirect::route('venueExtRatings', $ratingVenueId);
		}
	}

}
