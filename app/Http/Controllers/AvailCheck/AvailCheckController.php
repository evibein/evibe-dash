<?php

namespace App\Http\Controllers\AvailCheck;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Support\Facades\Auth;

class AvailCheckController extends BaseController
{
	public function showInitialPage()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.avail-check.prefix") . "/initialize",
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => []
		];

		$data = $this->makeGuzzleRequest($options);

		return view("availability.avail-check.initialize", ["data" => $data]);
	}

	public function showPartnerData($partnerTypeId, $partnerCode)
	{
		if ($partnerTypeId == config("evibe.ticket_type.venues") && (!(request("productId")) || (request("productId") == "")))
		{
			return redirect(route("avail.check.initialize"));
		}

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.avail-check.prefix") . "/" . $partnerTypeId . "/" . $partnerCode,
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => [
				"productId" => request("productId")
			]
		];

		$data = $this->makeGuzzleRequest($options);

		return view("availability.avail-check.summary", ["data" => $data]);
	}

	public function showPartnerVenueData($partnerTypeId, $partnerCode)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.avail-check.prefix") . "/venue/" . $partnerTypeId . "/" . $partnerCode,
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => []
		];

		$data = $this->makeGuzzleRequest($options);

		return $data;
	}

	public function savePartnerUnavailableSlot($partnerCode)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.avail-check.prefix") . "/add/" . $partnerCode,
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return response()->json([
			                        "success" => isset($data["success"]) ? $data["success"] : false,
			                        "error"   => isset($data["error"]) ? $data["error"] : ""
		                        ]);
	}

	public function editPartnerUnavailableSlot($partnerCode)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.avail-check.prefix") . "/edit/" . $partnerCode,
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return response()->json([
			                        "success" => isset($data["success"]) ? $data["success"] : false,
			                        "error"   => isset($data["error"]) ? $data["error"] : ""
		                        ]);
	}

	public function deletePartnerUnavailableSlot($partnerCode, $date, $slot)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.avail-check.prefix") . "/delete/" . $partnerCode,
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => [
				"date"           => $date,
				"slot"           => $slot,
				"venuePackageId" => request("productId")
			]
		];

		$this->makeGuzzleRequest($options);

		return redirect()->back();
	}

	public function checkPartnerUnavailableSlot($partnerCode)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . config("evibe.api.avail-check.prefix") . "/check/" . $partnerCode,
			"method"      => "POST",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];

		$data = $this->makeGuzzleRequest($options);

		return $data;
	}
}