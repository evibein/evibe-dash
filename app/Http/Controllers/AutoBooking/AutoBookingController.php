<?php

namespace App\Http\Controllers\AutoBooking;

use App\Models\DecorEvent;
use App\Models\TrendEvents;
use App\Models\TypeEvent;
use App\Models\Util\AutoBooking;
use App\Models\VenueHallEvent;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Base\BaseController;

class AutoBookingController extends BaseController
{
	public function showItemList(Request $request)
	{
		$itemTypeName = '';
		$allItems = '';
		$autoBookings = '';

		$pageTypes = \TypeTicket::where('is_page', 1)->get();
		$typeEvents = TypeEvent::all();

		$itemType = $request->input('type', config('evibe.ticket_type.packages'));
		$occasionId = $request->input('occasion', config('evibe.event.kids_birthday'));

		$pageType = \TypeTicket::find($itemType);

		if ($pageType)
		{
			$itemTypeName = str_replace("_", " ", $pageType->name);
		}
		$items = $this->collectItemData($itemType, $occasionId);

		if ($items['isValid'])
		{
			$allItems = $items['allItems']->get();
			$autoBookings = $items['autoBooking']->get();
		}

		$data = [
			'pageTypes'    => $pageTypes,
			'allItems'     => $allItems,
			'items'        => $autoBookings,
			'isValid'      => $items['isValid'],
			'itemTypeName' => $itemTypeName,
			'typeEvents'   => $typeEvents,
		];

		return view('utilities.auto_booking')->with($data);
	}

	public function saveAutoBookings(Request $request)
	{
		$res = ['success' => false];
		$mapTypeId = $request->input('itemTypeId');
		$itemIds = $request->input('itemIds');

		$oldAutoBuyIds = AutoBooking::where('map_type_id', $mapTypeId)->pluck('map_id')->toArray();

		//when all element will be removed from auto booking list;
		if (count($itemIds) == 0)
		{
			if (count($oldAutoBuyIds) > 0)
			{
				AutoBooking::where('map_type_id', $mapTypeId)->delete();
				$res['success'] = true;
			}
			else
			{
				$res = ['success' => false, 'error' => 'No items in the list'];
			}

			return response()->json($res);
		}

		if (count($itemIds) > 0)
		{
			//create new elements;
			foreach ($itemIds as $itemId)
			{
				if (!in_array($itemId, $oldAutoBuyIds))
				{
					$res['success'] = false;
					$newAutoBooking = AutoBooking::create([
						                                      'map_type_id' => $mapTypeId,
						                                      'map_id'      => $itemId
					                                      ]);
					if ($newAutoBooking)
					{
						$res['success'] = true;
					}
				}
			}
		}

		//delete removed items
		foreach ($oldAutoBuyIds as $oldAutoBuyId)
		{
			if (!in_array($oldAutoBuyId, $itemIds))
			{
				AutoBooking::where('map_type_id', $mapTypeId)->where('map_id', $oldAutoBuyId)->first()->delete();
				$res['success'] = true;
			}
		}

		return response()->json($res);
	}

	public function collectItemData($itemType, $occasionId)
	{
		$isValid = false;
		$items = '';
		$autoBookings = '';

		switch ($itemType)
		{
			Case config('evibe.ticket_type.packages'):
				if ($occasionId == config('evibe.event.special_experience'))
				{
					$isValid = false;
				}
				else
				{
					$items = \VendorPackage::with('provider')
					                       ->where('is_live', 1)
					                       ->where('event_id', $occasionId)
					                       ->orderBy('updated_at', 'DESC');

					$autoBookings = $this->getAutoBookablePackage($itemType);
					$isValid = true;
				}
				break;

			case config('evibe.ticket_type.trends'):
				$trendsIds = TrendEvents::where('type_event_id', $occasionId)->pluck('trend_id');

				$items = \Trend::where('is_live', 1)
				               ->whereIn('id', $trendsIds)
				               ->select('id', 'code', 'name', 'price')
				               ->orderBy('updated_at', 'DESC');
				$autoBookings = AutoBooking::join('trending', 'auto_booking.map_id', '=', 'trending.id')->where('auto_booking.map_type_id', $itemType)
				                           ->select('trending.id', 'code', 'name', 'price');
				$isValid = true;
				break;

			case config('evibe.ticket_type.cakes'):
				$cakeIds = \CakeEvent::where('event_id', $occasionId)->pluck('cake_id');

				$items = \Cake::where('is_live', 1)
				              ->whereIn('id', $cakeIds)
				              ->select('id', 'code', 'title as name', 'price');
				$autoBookings = AutoBooking::join('cake', 'auto_booking.map_id', '=', 'cake.id')->where('auto_booking.map_type_id', $itemType)
				                           ->select('cake.id', 'code', 'title as name', 'price');
				$isValid = true;
				break;

			case config('evibe.ticket_type.decors'):
				$decorIds = DecorEvent::where('event_id', $occasionId)->pluck('decor_id');

				$items = \Decor::where('is_live', 1)
				               ->whereIn('id', $decorIds)
				               ->select('id', 'code', 'name', 'min_price as price')
				               ->orderBy('updated_at', 'DESC');
				$autoBookings = AutoBooking::join('decor', 'auto_booking.map_id', '=', 'decor.id')->where('auto_booking.map_type_id', $itemType)
				                           ->select('decor.id', 'code', 'name', 'min_price as price');
				$isValid = true;
				break;

			case config('evibe.ticket_type.venue_halls'):

				$hallIds = VenueHallEvent::where('event_id', $occasionId)->pluck('venue_hall_id');

				$venue = DB::table('venue')
				           ->join('area', 'venue.area_id', '=', 'area.id')
				           ->join('venue_hall', 'venue.id', '=', 'venue_hall.venue_id')
				           ->where('venue.is_live', 1)
				           ->whereNull('venue_hall.deleted_at')
				           ->whereNull('venue.deleted_at')
				           ->whereIn('venue_hall.id', $hallIds);

				$items = $venue->select('venue_hall.id as id', 'venue.id as venue_id', 'venue.name as venue_name', 'venue_hall.code as code', 'venue_hall.name as name', 'area.name as location');

				$autoBookingsWithVenue = DB::table('venue')
				                           ->join('area', 'venue.area_id', '=', 'area.id')
				                           ->join('venue_hall', 'venue.id', '=', 'venue_hall.venue_id')
				                           ->whereNull('venue_hall.deleted_at')
				                           ->whereNull('venue.deleted_at')
				                           ->where('venue.is_live', 1);

				$autoBookings = $autoBookingsWithVenue->select('venue_hall.id as id', 'venue.id as venue_id', 'venue.name as venue_name', 'venue_hall.code as code', 'venue_hall.name as name', 'area.name as location');
				$autoBookings = $autoBookings->join('auto_booking', 'venue_hall.id', '=', 'auto_booking.map_id')->where('auto_booking.map_type_id', $itemType);
				$isValid = true;
				break;

			case config('evibe.ticket_type.resort'):

				$items = $this->getPackageList($occasionId, $itemType);
				$autoBookings = $this->getAutoBookablePackage($itemType);
				$isValid = true;
				break;

			case config('evibe.ticket_type.villa'):
				$items = $this->getPackageList($occasionId, $itemType);
				$autoBookings = $this->getAutoBookablePackage($itemType);
				$isValid = true;
				break;

			case config('evibe.ticket_type.lounge'):
				$items = $this->getPackageList($occasionId, $itemType);
				$autoBookings = $this->getAutoBookablePackage($itemType);
				$isValid = true;
				break;

			case config('evibe.ticket_type.food'):
				$items = $this->getPackageList($occasionId, $itemType);
				$autoBookings = $this->getAutoBookablePackage($itemType);
				$isValid = true;
				break;

			case config('evibe.ticket_type.venue-deals'):
				$items = $this->getPackageList($occasionId, $itemType);
				$autoBookings = $this->getAutoBookablePackage($itemType);
				$isValid = true;
				break;

			Case config('evibe.ticket_type.couple-experiences'):

				if ($occasionId == config('evibe.event.special_experience'))
				{
					$items = \VendorPackage::with('provider')
					                       ->where('is_live', 1)
					                       ->where('event_id', $occasionId)
					                       ->orderBy('updated_at', 'DESC');

					$autoBookings = $this->getAutoBookablePackage($itemType);
					$isValid = true;
				}
				break;
		}

		return ['allItems' => $items, 'autoBooking' => $autoBookings, 'isValid' => $isValid];

	}

	private function getPackageList($occasionId, $itemType)
	{
		$packageIds = $this->getPackageIdsFilterByTag($occasionId, $itemType);

		$items = \VendorPackage::with('provider')
		                       ->where('is_live', 1)
		                       ->select('planner_package.*')
		                       ->whereIn('id', $packageIds)
		                       ->where('event_id', $occasionId)
		                       ->orderBy('updated_at', 'DESC');

		return $items;

	}

	private function getAutoBookablePackage($itemType)
	{
		$autoBookings = AutoBooking::join('planner_package', 'auto_booking.map_id', '=', 'planner_package.id')->where('auto_booking.map_type_id', $itemType)
		                           ->select('planner_package.id', 'code', 'name', 'price');

		return $autoBookings;
	}
}
