<?php

namespace App\Http\Controllers\Automation;

use App\Http\Controllers\Ticket\TicketUtilController;
use App\Models\Ticket\LogTicketReminders;
use App\Models\Ticket\TicketRemindersStack;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class AutomationController extends TicketUtilController
{
	public function getAutoFollowupReminders()
	{
		$tickets = \Ticket::join('ticket_reminders_stack', 'ticket.id', '=', 'ticket_reminders_stack.ticket_id')
		                  ->join('type_ticket_status', 'type_ticket_status.id', '=', 'status_id')
		                  ->orderBy('ticket.created_at', 'desc')
		                  ->select('ticket.*', DB::raw('type_ticket_status.name AS status_name, ticket_reminders_stack.start_time AS reco_sent_time'))
		                  ->whereNull('ticket_reminders_stack.deleted_at')
		                  ->whereNull('ticket.deleted_at')
		                  ->get();
		//dd($tickets);

		$ticketId = request('ticketId');

		$reminders = [];
		$customerDetails = [];

		if ($ticketId)
		{
			$logReminders = LogTicketReminders::where('ticket_id', '=', $ticketId)
			                                  ->select('created_at', 'type_reminder_group_id')
			                                  ->get();

			if (count($logReminders))
			{
				foreach ($logReminders as $logReminder)
				{
					array_push($reminders, [
						'reminderDate'        => date('d M Y H:i:s', strtotime($logReminder->created_at)),
						'typeReminderGroupId' => $logReminder->type_reminder_group_id,
						'smsTemplate'         => config('evibe.test.reminders.followup.rr' . $logReminder->type_reminder_group_id),
						'isSMSSent'           => "Yes"
					]);
				}
			}

			$stackReminder = TicketRemindersStack::where('ticket_id', '=', $ticketId)
			                                     ->whereNull('deleted_at')
			                                     ->first();
			if ($stackReminder && !is_null($stackReminder->terminated_at) && !is_null($stackReminder->invalid_at))
			{
				array_push($reminders, [
					'reminderDate'        => date('d M Y H:i:s', $stackReminder->next_reminder_at),
					'typeReminderGroupId' => $stackReminder->type_reminder_group_id,
					'smsTemplate'         => config('evibe.test.reminders.followup.rr' . $stackReminder->type_reminder_group_id),
					'isSMSSent'           => "No"
				]);
			}

			$customerTicketData = $tickets->find($ticketId);

			$customerDetails = [
				'customerName'      => $customerTicketData->name,
				'customerEmail'     => $customerTicketData->email,
				'customerMobileNo'  => $customerTicketData->phone,
				'statusOfParty'     => $customerTicketData->status_name,
				'followupStartTime' => $stackReminder ? date('d M Y H:i:s', $stackReminder->start_time) : null,
				'followupEndTime'   => $stackReminder ? date('d M Y H:i:s', $stackReminder->end_time) : null
			];
		}

		return view('automation.auto-followup-reminders', [
			'tickets'         => $tickets,
			'reminders'       => $reminders,
			'customerDetails' => $customerDetails,
			'ticketId'        => $ticketId
		]);
	}

	public function fetchFollowupReminders()
	{
		$startTime = Input::get('startTime');
		$endTime = Input::get('endTime');
		$followupTime = Input::get('followupTime') ? Input::get('followupTime') : null;
		$switchTime = Input::get('switchTime') ? Input::get('switchTime') : null;

		if ($startTime && $endTime)
		{
			// make API call to initiate auto follow-ups
			$user = Auth::user();
			$accessToken = $this->getAccessTokenFromUserId($user);
			$url = config('evibe.api.base_url') . config('evibe.api.auto-followups.prefix') . 'test/followup-reminders';

			$T = strtotime($startTime);
			$P = strtotime($endTime);

			$options = [
				'method'      => "GET",
				'url'         => $url,
				'accessToken' => $accessToken,
				'jsonData'    => [
					'T'                => $T,
					'P'                => $P,
					'followupDateTime' => strtotime($followupTime),
					'switchDateTime'   => strtotime($switchTime)
				]
			];

			$diff = ((strtotime(date("Y-m-d", $P)) - strtotime(date("Y-m-d", $T))) / (24 * 60 * 60)) - 1;

			$res = $this->makeApiCallWithUrl($options);
			$res['startTime'] = $startTime;
			$res['endTime'] = $endTime;
			$res['followupTime'] = $followupTime;
			$res['switchTime'] = $switchTime;

			if (isset($res['success']) && !$res['success'])
			{
				$error = "Some error occurred while fetching followup cases";
				if (isset($res['error']))
				{
					$error = $res['error'];
				}

				return redirect()->back()->withErrors($error, 'APIError');
			}

			if (isset($res['reminderData']) && $res['reminderData'])
			{
				$res['diff'] = $diff;

				return view('automation.auto-followup-test', ['data' => $res]);
			}
		}

		return view('automation.auto-followup-test');
	}
}