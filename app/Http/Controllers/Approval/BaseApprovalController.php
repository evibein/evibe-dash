<?php

namespace App\Http\Controllers\Approval;

use App\Http\Controllers\Base\BaseController;
use App\Models\TypeServices;
use Evibe\Facades\AppUtilFacade as AppUtil;
use App\Models\Util\Approval;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class BaseApprovalController extends BaseController
{
	protected function getApprovalVisibility($mapId, $mapTypeId)
	{
		$data = [
			'askApprovalButton' => false,
			'askApprovalAlert'  => "",
			'approvalButton'    => false,
			'isActivateAlert'   => "",
			'isApprovalReason'  => false,
			'rejectionComment'  => ""
		];

		$user = Auth::user();
		$approval = Approval::where('map_id', $mapId)->where('map_type_id', $mapTypeId)->first();

		if ($approval)
		{
			// check for approval reason
			if ($approval->is_rejected)
			{
				$data['isApprovalReason'] = true;
				$data['rejectionComment'] = "<div class='alert-danger pad-6' ><b>Reason:</b>$approval->reject_comment</div>";

			}
		}

		// show ask approval button and message to bd team
		if ($user->role_id == config('evibe.roles.bd'))
		{
			$data['askApprovalButton'] = true;
			if ($approval)
			{
				if ($approval->is_ask_active)
				{
					$data['askApprovalButton'] = false;

					$askDate = $approval->approve_asked_updated_at ? $approval->approve_asked_updated_at : $approval->approve_asked_at;
					$date = $this->formatTime($askDate);
					$data['askApprovalAlert'] = "<div class='alert-info pad-6'>Asked for approval at $date, but operation team didn't respond.</div>";
				}
				else
				{
					if ($approval->is_live)
					{
						$askedDate = $approval->approve_asked_at ? $approval->approve_asked_at : $approval->approve_asked_updated_at;
						$askedDate = $this->formatTime($askedDate);
						$repliedAt = $approval->live_updated_at ? $approval->live_updated_at : $approval->live_at;
						$date = $this->formatTime($repliedAt);
						$username = $approval->liveBy ? $approval->liveBy->name : "--";

						$data['askApprovalAlert'] = "<div class='alert-success pad-6'>Asked for approval at $askedDate, request accepted and made live by $username at $date</div>";
					}
					elseif ($approval->is_rejected)
					{
						$askedDate = $approval->approve_asked_updated_at ? $approval->approve_asked_updated_at : $approval->approve_asked_at;
						$askedDate = $this->formatTime($askedDate);
						$repliedAt = $approval->reject_updated_at ? $approval->reject_updated_at : $approval->reject_created_at;
						$date = $this->formatTime($repliedAt);
						$username = $approval->rejectBy->name;

						$data['rejectionComment'] = "<div class='alert-danger pad-6' ><b>Reason:</b> $approval->reject_comment</div>";
						$data['askApprovalAlert'] = "<div class='alert-danger pad-6'>Asked for approval at $askedDate, request rejected by $username at $date</div>";
					}

				}
			}
		}
		elseif ($user->role_id == config('evibe.roles.operations') || AppUtil::isAdmin() || AppUtil::isSrBd())
		{
			$data['approvalButton'] = false;
			if ($approval)
			{
				if ($approval->is_ask_active)
				{
					$data['approvalButton'] = true;

					$askedAt = $approval->approve_asked_updated_at ? $approval->approve_asked_updated_at : $approval->approve_asked_at;
					$date = $this->formatTime($askedAt);
					$user = $approval->approveAskedBy->name;
					$reason = "";
					if ($data['isApprovalReason'] && $approval->ask_latest_comment)
					{
						$reason = "<b>Reason for accept :: </b> " . $approval->ask_latest_comment;
					}

					$data['isActivateAlert'] = "<div class='alert-info pad-6'>Asked for approval by $user at $date, but operation team didn't respond. $reason</div>";
				}
				else
				{
					$askedDate = $approval->approve_asked_updated_at ? $approval->approve_asked_updated_at : $approval->approve_asked_at;
					$askedDate = $this->formatTime($askedDate);
					if ($approval->is_live)
					{
						$repliedAt = $approval->live_updated_at ? $approval->live_updated_at : $approval->live_at;
						$date = $this->formatTime($repliedAt);
						$username = $approval->liveBy ? $approval->liveBy->name : "--";
						$askedBy = $approval->approveAskedBy ? $approval->approveAskedBy->name : "default";

						$data['isActivateAlert'] = "<div class='alert-success pad-6'>Asked for approval by $askedBy at $askedDate, request accepted and made live by $username at $date</div>";
					}
					elseif ($approval->is_rejected)
					{
						$repliedAt = $approval->reject_created_at ? $approval->reject_created_at : $approval->reject_updated_at;
						$date = $this->formatTime($repliedAt);
						$username = $approval->rejectBy->name;
						$askedBy = $approval->approveAskedBy->name;

						$data['rejectionComment'] = "<div class='alert-danger pad-6' ><b>Reason:</b> $approval->reject_comment</div>";
						$data['isActivateAlert'] = "<div class='alert-danger pad-6'>Asked for approval by $askedBy at $askedDate, request rejected by $username at $date</div>";
					}
				}
			}
		}

		return $data;
	}

	protected function updateApprovalOnEditItem($item)
	{
		// check if the role is not admin + operation
		if (auth()->user()->role_id == config('evibe.roles.bd'))
		{
			// make item non live
			$item->update(['is_live' => 0]);
		}
	}

	protected function saveApprovalData($mapId, $mapTypeId, $reason = null)
	{
		$approval = Approval::where('map_id', $mapId)->where('map_type_id', $mapTypeId)->first();
		$userId = Auth::user()->id;
		$now = Carbon::now()->timestamp;

		if ($approval)
		{
			$updateData = [
				'is_ask_active'            => 1,
				'is_live'                  => 0,
				'approve_asked_by'         => $userId,
				'approve_asked_updated_at' => $now,
			];
			if ($reason)
			{
				$updateData['ask_latest_comment'] = $reason;
			}
			$approval->update($updateData);
		}
		else
		{
			$insertData = [
				'is_live'          => 0,
				'map_id'           => $mapId,
				'map_type_id'      => $mapTypeId,
				'is_ask_active'    => 1,
				'approve_asked_by' => $userId,
				'approve_asked_at' => $now
			];
			Approval::create($insertData);
		}
	}

	public function updateAcceptApprovalData($mapId, $mapTypeId)
	{
		$approval = Approval::where('map_id', $mapId)->where('map_type_id', $mapTypeId)->first();
		$userId = Auth::user()->id;
		$now = Carbon::now()->timestamp;

		$insertData = [
			'is_ask_active' => 0,
			'is_live'       => 1,
			'is_rejected'   => 0,
			'live_by'       => $userId,
		];

		is_null($approval->live_at) ? $insertData['live_at'] = $now : $insertData['live_updated_at'] = $now;

		$approval->update($insertData);
	}

	public function updateRejectApprovalData($mapId, $mapTypeId, $comment)
	{
		$approval = Approval::where('map_id', $mapId)->where('map_type_id', $mapTypeId)->first();
		$userId = Auth::user()->id;
		$now = Carbon::now()->timestamp;

		$insertData = [
			'is_ask_active'     => 0,
			'is_live'           => 0,
			'is_rejected'       => 1,
			'reject_created_at' => $now,
			'reject_comment'    => $comment,
			'reject_updated_at' => $now,
			'rejected_by'       => $userId,
		];

		is_null($approval->live_at) ? $insertData['live_at'] = $now : $insertData['live_updated_at'] = $now;

		$approval->update($insertData);
	}

	protected function formatTime($timestamp)
	{
		return date('d M Y, h:i A', $timestamp);
	}

	protected function getItemData($mapId, $mapTypeId, $isLiveLink = false)
	{
		$data = [
			'item'     => '',
			'title'    => '',
			'type'     => '',
			'link'     => '',
			'provider' => '',
			'liveLink' => '',
			'code'     => ''
		];

		switch ($mapTypeId)
		{
			case config('evibe.ticket_type.cakes'):
				$data['item'] = \Cake::find($mapId);
				$data['title'] = $data['item'] && $data['item']->title ? $data['item']->title : 'Default Title';
				$data['type'] = 'cake';
				break;

			case config('evibe.ticket_type.packages'):
				$data['item'] = \VendorPackage::find($mapId);
				$data['title'] = $data['item'] && $data['item']->name ? $data['item']->name : 'Default Name';
				$data['type'] = 'package';
				break;

			case config('evibe.ticket_type.decors'):
				$data['item'] = \Decor::find($mapId);
				$data['title'] = $data['item'] && $data['item']->name ? $data['item']->name : 'Default Name';
				$data['type'] = 'decor';
				break;

			case config('evibe.ticket_type.services'):
				$data['item'] = TypeServices::find($mapId);
				$data['title'] = $data['item'] && $data['item']->name ? $data['item']->name : 'Default Name';
				$data['type'] = 'entertainment';
				break;
		}

		if (!empty($data['item']))
		{
			$data['link'] = $data['item']->getLink();
			$data['code'] = $data['item']->code;
			$provider = $data['item']->provider;
			$data['provider'] = $provider ? $provider->name . ($provider->area ? ", " . $provider->area->name : "") : "";
		}

		if ($isLiveLink && isset($data['item']))
		{
			$mappingData = AppUtil::fillMappingValues([
				                                          'mapId'        => $data['item']->id,
				                                          'mapTypeId'    => $mapTypeId,
				                                          'isCollection' => false
			                                          ]);
			$data['liveLink'] = $mappingData['publicLink'];
		}

		return $data;
	}
}
