<?php

namespace App\Http\Controllers\Approval;

use App\Jobs\Emails\Approval\AcceptApprovalJob;
use App\Jobs\Emails\Approval\AskForApprovalJob;
use App\Jobs\Emails\Approval\RejectApprovalJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ApprovalController extends BaseApprovalController
{
	// asking the approval
	public function askApproval($mapId, $mapTypeId, Request $request)
	{
		$newApprovalId = null;
		$newSavedLogId = null;
		$reason = $request->input('comment', null);

		try
		{
			// began the transaction
			DB::beginTransaction();

			$itemData = $this->getItemData($mapId, $mapTypeId);

			$item = $itemData['item'];
			$item->update(['is_live' => 0]);

			// save the values in approval table
			$this->saveApprovalData($mapId, $mapTypeId, $reason);

			// pushing data to log listing activity
			$logMsg = "Approval asked for " . strtolower($itemData['type']) . " [$item->code]";

			$logActivityData = [
				'mapId'     => $mapId,
				'mapTypeId' => $mapTypeId,
				'message'   => $logMsg
			];

			$user = Auth::check() ? Auth::user() : false;

			$userName = ($user && $user->name) ? $user->name : "Default User";

			$emailData = [
				'userName' => $userName,
				'reason'   => $reason,
				'title'    => $itemData['title'],
				'link'     => $itemData['link']
			];

			$this->logListingActivity($logActivityData);
			dispatch(new AskForApprovalJob($emailData));

		} catch (\Exception $e)
		{
			DB::rollback();
			$this->sendErrorReport($e);

			return response()->json(['success' => false, 'error' => "Couldn't ask for approval successfully"]);
		}
		DB::commit();

		return response()->json(['success' => true, 'message' => "Approval asked successfully"]);
	}

	public function acceptApproval($mapId, $mapTypeId)
	{
		try
		{
			// began the transaction
			DB::beginTransaction();

			$itemData = $this->getItemData($mapId, $mapTypeId, true);
			$item = $itemData['item'];
			$item->update(['is_live' => 1]);
			$logMessage = "Approval accepted for " . strtolower($itemData['type']) . " [$item->code]";

			// save the values in approval table
			$this->updateAcceptApprovalData($mapId, $mapTypeId);

			// pushing data to log listing activity
			$logActivityData = [
				'mapId'     => $mapId,
				'mapTypeId' => $mapTypeId,
				'message'   => $logMessage
			];

			// retrieve username
			$user = Auth::check() ? Auth::user() : false;
			$userName = ($user && $user->name) ? $user->name : "Default User";

			$emailData = [
				'userName' => $userName,
				'message'  => $logMessage,
				'title'    => $itemData['title'],
				'link'     => $itemData['link'],
				'liveLink' => $itemData['liveLink']
			];

			$this->logListingActivity($logActivityData);
			dispatch(new AcceptApprovalJob($emailData));

		} catch (\Exception $e)
		{
			DB::rollback();
			$this->sendErrorReport($e);

			return redirect()->back()->with('customError', "Couldn't accept the approval");
		}
		DB::commit();

		return redirect()->back()->with('customSuccess', 'Approval accepted successfully');
	}

	public function rejectApproval($mapId, $mapTypeId)
	{
		try
		{
			// began the transaction
			DB::beginTransaction();

			$itemData = $this->getItemData($mapId, $mapTypeId);
			$item = $itemData['item'];
			$item->update(['is_live' => 0]);
			$comment = Input::get('comment');

			// save the values in approval table
			$this->updateRejectApprovalData($mapId, $mapTypeId, $comment);

			// pushing data to log listing activity
			$logMessage = "Approval rejected for " . strtolower($itemData['type']) . " [$item->code]";
			$logActivityData = [
				'mapId'     => $mapId,
				'mapTypeId' => $mapTypeId,
				'message'   => $logMessage
			];

			$this->logListingActivity($logActivityData);

			// retrieve username
			$user = Auth::check() ? Auth::user() : false;
			$userName = ($user && $user->name) ? $user->name : "Default User";

			$emailData = [
				'userName' => $userName,
				'message'  => $logMessage,
				'title'    => $itemData['title'],
				'link'     => $itemData['link'],
				'comment'  => $comment
			];

			$this->logListingActivity($logActivityData);
			dispatch(new RejectApprovalJob($emailData));

		} catch (\Exception $e)
		{
			DB::rollback();
			$this->sendErrorReport($e);

			return response()->json(['success' => true, 'error' => "Couldn't reject the approval"]);
		}
		DB::commit();

		return response()->json(['success' => true, 'message' => 'Approval rejected successfully']);
	}
}
