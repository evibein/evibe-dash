<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Ticket\TicketUtilController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PartnerAdjustmentController extends TicketUtilController
{
	public function getPartnerAdjustmentsList($query = 'pending')
	{
		// filters
		$filPartnerTypeId = request('partnerTypeId');
		$filPartnerId = request('partnerId');
		$filScheduleDate = request('scheduleDate');

		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.adjustments.prefix') . '/' . $query;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'partnerTypeId' => $filPartnerTypeId,
				'partnerId'     => $filPartnerId,
				'scheduleDate'  => $filScheduleDate,
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (isset($res['success']) && !$res['success'])
		{
			$error = 'Some error occurred while fetching data';

			if (isset($res['error']) && $res['error'])
			{
				$error = $res['error'];
			}

			return view("finance.partner-adjustments", ['error' => $error]);
		}

		$res['query'] = $query;
		$res['referenceTab'] = config('evibe.reference.finance.partner-adjustments');

		return view("finance.partner-adjustments", ['data' => $res]);
	}

	public function createPartnerAdjustment()
	{
		$partnerId = request('partnerId');
		$partnerTypeId = request('partnerTypeId');

		if (!$partnerId || !$partnerTypeId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly select a partner'
			                        ]);
		}
		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.adjustments.prefix') . '/' . $partnerId . '/' . $partnerTypeId . '/create';

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'ticketBookingId'  => request('ticketBookingId') ? request('ticketBookingId') : null,
				'adjustmentAmount' => request('adjustmentAmount'),
				'isPayback'        => request('isPayback'),
				'comments'         => request('comments')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function updatePartnerAdjustment()
	{
		$adjustmentId = request('adjustmentId');

		if (!$adjustmentId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Unable to find adjustment id to edit'
			                        ]);
		}
		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.adjustments.prefix') . '/' . $adjustmentId . '/update';

		$options = [
			'method'      => "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'adjustmentAmount' => request('adjustmentAmount'),
				'isPayback'        => request('isPayback'),
				'scheduleDate'     => request('scheduleDate')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function deletePartnerAdjustment()
	{
		$adjustmentId = request('adjustmentId');

		if (!$adjustmentId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Unable to find adjustment id to delete'
			                        ]);
		}
		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.adjustments.prefix') . '/' . $adjustmentId . '/delete';

		$options = [
			'method'      => "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => []
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}
}