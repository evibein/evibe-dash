<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Ticket\TicketUtilController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class RefundController extends TicketUtilController
{
	public function getCustomerRefundsList($query = 'pending')
	{
		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.refunds.prefix') . '/' . $query;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'minRefundDate' => request('minRefundDate'),
				'maxRefundDate' => request('maxRefundDate'),
				'search'        => request('search')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (isset($res['success']) && !$res['success'])
		{
			$error = 'Some error occurred while fetching data';

			if (isset($res['error']) && $res['error'])
			{
				$error = $res['error'];
			}

			return view("finance.customer-refunds", ['error' => $error]);
		}

		$res['query'] = $query;
		$res['referenceTab'] = config('evibe.reference.finance.customer-refunds');

		return view("finance.customer-refunds", ['data' => $res]);
	}

	public function createCustomerRefund()
	{
		$ticketBookingId = request('ticketBookingId');
		$partnerBear = request('partnerBear');
		$refundComments = request('refundComments');
		$successMsg = 'Customer refund created successfully.';

		if (!$ticketBookingId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly select a ticket booking'
			                        ]);
		}
		// make API call to create customer refund
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.refunds.prefix') . '/' . $ticketBookingId . '/create';

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'refundAmount' => request('refundAmount'),
				'evibeBear'    => request('evibeBear'),
				'partnerBear'  => $partnerBear,
				'comments'     => $refundComments,
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function updateCustomerRefund()
	{
		$ticketBookingId = request('ticketBookingId');
		$successMsg = 'Customer refund updated successfully.';

		if (!$ticketBookingId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly select a ticket booking'
			                        ]);
		}
		// make API call to create customer refund
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.refunds.prefix') . '/' . $ticketBookingId . '/update';

		$options = [
			'method'      => "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'refundAmount' => request('refundAmount'),
				'evibeBear'    => request('evibeBear'),
				'partnerBear'  => request('partnerBear')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function rejectCustomerRefund()
	{
		$ticketBookingId = request('ticketBookingId');

		if (!$ticketBookingId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly select a ticket booking'
			                        ]);
		}
		// make API call to create customer refund
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.refunds.prefix') . '/' . $ticketBookingId . '/reject';

		$options = [
			'method'      => "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'comments' => request('rejectionComments')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function approveCustomerRefund()
	{
		$ticketBookingId = request('ticketBookingId');

		if (!$ticketBookingId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly select a ticket booking'
			                        ]);
		}
		// make API call to create customer refund
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.refunds.prefix') . '/' . $ticketBookingId . '/approve';

		$options = [
			'method'      => "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'refundReference' => request('refundReference')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}
}