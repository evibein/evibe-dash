<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Ticket\TicketUtilController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Finance\ScheduleSettlementBookings;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class SettlementController extends TicketUtilController
{
	public function getSettlementsList($query = 'created')
	{

		// filters
		$filPartnerTypeId = request('partnerTypeId');
		$filPartnerId = request('partnerId');

		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/' . $query;
		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'partnerTypeId'            => $filPartnerTypeId,
				'partnerId'                => $filPartnerId,
				'minSettlementCreateDate'  => request('minSettlementCreateDate'),
				'maxSettlementCreateDate'  => request('maxSettlementCreateDate'),
				'minSettlementProcessDate' => request('minSettlementProcessDate'),
				'maxSettlementProcessDate' => request('maxSettlementProcessDate'),
			]
		];

		$res = $this->makeApiCallWithUrl($options);
	
		if (isset($res['success']) && !$res['success'])
		{
			$error = 'Some error occurred while fetching data';

			if (isset($res['error']) && $res['error'])
			{
				$error = $res['error'];
			}

			return view("finance.settlements", ['error' => $error]);
		}

		$res['query'] = $query;
		$res['referenceTab'] = config('evibe.reference.finance.settlements');

		return view("finance.settlements", ['data' => $res]);
	}

	public function getSettlementDetails($settlementId)
	{
		if (!$settlementId)
		{
			return redirect()->back();
		}

		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/' . $settlementId . '/info';

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => []
		];

		$res = $this->makeApiCallWithUrl($options);

		$res['referenceTab'] = config('evibe.reference.finance.settlements');

		return view("finance.settlement-info", ['data' => $res]);
	}

	public function createSettlements()
	{
		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/create';
		$customDate = request()->input('customSelectedDate');
		if(!($customDate))
		{
			return response()->json([
				'success' => false,
				'message' => "Schedule shouldn't be empty"
			]);
		}
		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'isFromDash'          => true,
				'settlementStartTime' => Carbon::parse($customDate)->startOfDay()->timestamp, // this week wednesday
				'settlementEndTime'   => Carbon::parse($customDate)->endOfDay()->timestamp, // this week wednesday end
				'partyStartTime'      => Carbon::parse($customDate)->startOfWeek()->subWeek(1)->timestamp, // last Monday
				'partyEndTime'        => Carbon::parse($customDate)->startOfWeek()->subDays(1)->endOfDay()->timestamp // this Sunday
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function processMultipleSettlements()
	{
		$idsString = request('selectedIds');

		if (!$idsString)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Some error occurred while processing the settlement. Kindly refresh the page and try again'
			                        ]);
		}

		$settlementIds = explode("#", $idsString);

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		foreach ($settlementIds as $settlementId)
		{
			if ($settlementId && ($settlementId != ""))
			{
				$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/' . $settlementId . '/process';
				$options = [
					'method'      => "PUT",
					'url'         => $url,
					'accessToken' => $accessToken,
					'jsonData'    => [
						'settlementReference' => request('settlementReference')
					]
				];

				$res = $this->makeApiCallWithUrl($options);
				if (!$res || (isset($res['success']) && !$res['success']))
				{
					$errorMsg = "Some error occurred while processing the settlement: $settlementId";
					if (isset($res['error']) && $res['error'])
					{
						$errorMsg = $res['error'];
					}

					// @see: error is already being reported in APIs
					Log::error("Multiple Settlements: $errorMsg");

					// break and throw message
					return response()->json([
						                        'success' => false,
						                        'error'   => 'Some error occurred while processing the settlement with id: ' . $settlementId
					                        ]);
				}
			}
		}
		
		return response()->json([
			                        'success' => true,
		                        ]);

	}

	public function processSettlement($settlementId)
	{
		if (!$settlementId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Some error occurred while processing the settlement. Kindly refresh the page and try again'
			                        ]);
		}

		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/' . $settlementId . '/process';

		$options = [
			'method'      => "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'settlementReference' => request('settlementReference')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function downloadSettlementSheet($query = 'created')
	{
		$settlementMonth = Input::get('settlementMonth');

		// make API call to fetch settlements list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/download/' . $query;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'settlementMonth' => $settlementMonth
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (isset($res) && $res['success'])
		{
			if (isset($res['settlementsList']) && count($res['settlementsList']))
			{
				if ($query == 'created')
				{
					Excel::create('Created Settlements List', function ($excel) use ($res) {
						$excel->sheet('Settlements - ' . date('d M Y', time()), function ($sheet) use ($res) {
							$sheet->loadView('finance.settlements-sheet', ['data' => $res]);
						});
					})->export('xls');
				}
				elseif ($query == 'created-bank')
				{
					Excel::create('Created Settlements Bank List', function ($excel) use ($res) {
						$excel->sheet('Settlements_Bank_' . date('d_m_Y', time()), function ($sheet) use ($res) {
							$sheet->loadView('finance.settlements-bank-sheet', ['data' => $res]);
							$sheet->setColumnFormat([
								                        'D' => '0.00',
								                        'O' => '0'
							                        ]);
						});
					})->export('xls');
				}
				elseif ($query == 'settled')
				{
					Excel::create('Settled Settlements List', function ($excel) use ($res, $settlementMonth) {
						$excel->sheet('Settlements - ' . $settlementMonth, function ($sheet) use ($res) {
							$sheet->loadView('finance.settlements-sheet', ['data' => $res]);
						});
					})->export('xls');
				}
				elseif ($query == 'pending')
				{
					Excel::create('Pending Settlements List', function ($excel) use ($res) {
						$excel->sheet('Settlements - ' . date('d M Y', time()), function ($sheet) use ($res) {
							$sheet->loadView('finance.settlements-sheet', ['data' => $res]);
						});
					})->export('xls');
				}
				elseif ($query == 'deviations')
				{
					Excel::create('Settlement Deviations', function ($excel) use ($res) {
						$excel->sheet('Deviations - ' . date('d M Y', time()), function ($sheet) use ($res) {
							$sheet->loadView('finance.settlements-deviations', ['data' => $res]);
						});
					})->export('xls');
				}
			}
		}
	}

	public function downloadPdf()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/test';

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => []
		];

		$res = $this->makeApiCallWithUrl($options);
	}

	public function downloadSettlementsCommission()
	{
		$scheduleSettlementBookings = ScheduleSettlementBookings::join('ticket_bookings', 'ticket_bookings.id', '=', 'schedule_settlement_bookings.ticket_booking_id')
		                                                        ->select('schedule_settlement_bookings.*', 'ticket_bookings.booking_amount')
		                                                        ->whereNull('schedule_settlement_bookings.deleted_at')
		                                                        ->get();

		$bookingSettlementDetails = [];

		if (count($scheduleSettlementBookings))
		{
			foreach ($scheduleSettlementBookings as $bookingSettlement)
			{
				$partner = $bookingSettlement->provider;

				array_push($bookingSettlementDetails, [
					'bookingSettlementId' => $bookingSettlement->id,
					'ticketBookingId'     => $bookingSettlement->ticket_booking_id,
					'advanceAmount'       => $bookingSettlement->advance_amount,
					'evibeServiceFee'     => $bookingSettlement->evibe_service_fee,
					'commissionRate'      => $partner ? $partner->commission_rate : null,
					'actualServiceFee'    => $partner ? ($bookingSettlement->booking_amount * $partner->commission_rate / 100) : null
				]);
			}
		}

		Excel::create('Settlement Bookings', function ($excel) use ($bookingSettlementDetails) {
			// totals sheet
			$excel->sheet('Total', function ($sheet) use ($bookingSettlementDetails) {
				$sheet->fromArray($bookingSettlementDetails);
				$sheet->row(1, function ($row) {
					$row->setBackground('#faca39');
					$row->setAlignment('center');
				});
			});
		})->download('xls');

	}

	public function getQuickSettlementsData()
	{
		// params
		$partnerTypeId = request('partnerTypeId');
		$partnerId = request('partnerId');

		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.quick-settlements.prefix');

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'partnerTypeId' => $partnerTypeId,
				'partnerId'     => $partnerId
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (isset($res['success']) && !$res['success'])
		{
			$error = 'Some error occurred while fetching data';

			if (isset($res['error']) && $res['error'])
			{
				$error = $res['error'];
			}

			return view("finance.quick-settlements", ['error' => $error]);
		}

		$res['partnerId'] = $partnerId;
		$res['partnerTypeId'] = $partnerTypeId;
		$res['referenceTab'] = config('evibe.reference.finance.quick-settlements');

		//dd($res);
		return view("finance.quick-settlements", ['data' => $res]);
	}

	public function processQuickSettlement()
	{
		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.quick-settlements.prefix') . '/process';

		$options = [
			'method'      => "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'partnerId'           => request('partnerId'),
				'partnerTypeId'       => request('partnerTypeId'),
				'settlementReference' => request('settlementReference')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}
}