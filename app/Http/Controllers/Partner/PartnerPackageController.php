<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Base\BaseController;
use App\Http\Controllers\Base\DecorBaseController;
use App\Http\Controllers\Package\CreatePackageController;
use App\Jobs\Emails\Partner\SendUpgradeMailToPartner;
use App\Models\DecorEvent;
use App\Models\Partner\PartnerPackageOptions;
use App\Models\Partner\PartnerPackageOptionsDiscussions;
use App\Models\Partner\PartnerPackageOptionsGallery;
use App\Models\Partner\PartnerPackageOptionsValues;
use App\Models\Types\TypePartnerOptionsFields;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PartnerPackageController extends BaseController
{
	public function showList()
	{
		$newPackages = PartnerPackageOptions::where('status_id', config('evibe.partner.package_status.submitted'))->orderBy('updated_at', 'DESC')->paginate(16);
		$packageGallery = PartnerPackageOptionsGallery::whereIn('partner_option_id', $newPackages->pluck('id'))->get();
		$data = [
			'newPackages'    => $newPackages,
			'packageGallery' => $packageGallery,
			'cities'         => $cities = \City::all(),
			'typeTicket'     => \TypeTicket::all()
		];

		return view('partner.add_package', ['data' => $data]);
	}

	public function showDetails($packageId)
	{
		$values = [];
		$package = PartnerPackageOptions::where('id', $packageId)->first();
		if ($package)
		{
			$partnerData = $this->getPartnerByUserId($package->user_id);

			$packageValues = PartnerPackageOptionsValues::where('partner_option_id', $package->id)->get();
			$typePartnerFields = TypePartnerOptionsFields::whereIn('id', $packageValues->pluck("type_partner_option_field_id"))->get();

			foreach ($packageValues as $value)
			{
				$values[] = [
					'name'  => $typePartnerFields->where('id', $value->type_partner_option_field_id)->first()->field_name,
					'value' => $value->value
				];
			}

			$discussions = PartnerPackageOptionsDiscussions::where('partner_option_id', $packageId)
			                                               ->orderBy('updated_at', 'desc')
			                                               ->get();
			$messages = [];
			foreach ($discussions as $discussion)
			{
				$messages[] = [
					"upgrades"       => explode(',', $discussion->upgrade_option_ids),
					"comment"        => $discussion->user_comment,
					"partnerComment" => $discussion->partner_comment,
					"is_complete"    => $discussion->is_complete,
					"created_at"     => $discussion->created_at,
					"respond_at"     => $discussion->responded_at,
					"read_at"        => $discussion->read_at
				];
			}

			$data = [
				'partnerData'    => $partnerData,
				'package'        => $package,
				'packageValues'  => $values,
				'messages'       => $messages,
				'packageGallery' => PartnerPackageOptionsGallery::where('partner_option_id', $package->id)->get()
			];

			return view('partner.view_package', ['data' => $data]);
		}

		return redirect()->back();
	}

	public function acceptPackageApproval($packageId)
	{
		$values = [];
		$package = PartnerPackageOptions::where('id', $packageId)->first();
		$errorMessage = "Package accept failed";

		if ($package)
		{
			$packageValues = PartnerPackageOptionsValues::where('partner_option_id', $package->id)->get();
			$validPackageFieldIds = $packageValues->pluck("type_partner_option_field_id");
			$typePartnerFields = TypePartnerOptionsFields::whereIn('id', $validPackageFieldIds)->get();

			foreach ($packageValues as $value)
			{
				$currentField = $typePartnerFields->where('id', $value->type_partner_option_field_id)->first();

				if ($currentField)
				{
					$fieldName = $currentField->original_field_name;
					$values[$fieldName] = $value->value;
				}
			}

			switch ($package->package_type_id)
			{
				Case config('evibe.ticket_type.decors'):
					$response = $this->createNewDecor($package, $values);
					if ($response['success'])
					{
						return redirect(($response['redirectTo']));
					}
					else
					{
						$errorMessage = $response['message'];
					}
					break;
			}
		}

		return redirect()->back()->with("customError", $errorMessage);
	}

	public function rejectPackageApproval($packageId)
	{
		$package = PartnerPackageOptions::where('id', $packageId)->first();

		if ($package)
		{
			$package->update(['rejected_at'     => Carbon::now(),
			                  "reject_comments" => request("comments") ?: "",
			                  "status_id"       => config("evibe.partner.package_status.rejected")]);

			return response()->json(["success" => true]);
		}

		return response()->json(["success" => false]);
	}

	public function packageUpgrade($packageId)
	{
		if (!request('reasons'))
		{
			return response()->json(["success" => false, "error" => "Please select atleast one reason to proceed"]);
		}

		if ($oldDiscussion = PartnerPackageOptionsDiscussions::where('partner_option_id', $packageId)
		                                                     ->whereNull('is_complete')
		                                                     ->first()
		)
		{
			$oldDiscussion->update(['is_complete' => 1]);
		}

		$reasonIds = "";
		foreach (request('reasons') as $reason)
		{
			$reasonIds = $reason . "," . $reasonIds;
		}

		PartnerPackageOptionsDiscussions::create([
			                                         "title"              => "Message from Team regarding the package upgrades",
			                                         "partner_option_id"  => $packageId,
			                                         "upgrade_option_ids" => $reasonIds,
			                                         "user_comment"       => request('comments')
		                                         ]);

		$package = PartnerPackageOptions::find($packageId)->update(['is_edit' => 1]);

		$data = ['packageId' => $packageId,
		         'upgrades'  => request()->all()];

		dispatch(new SendUpgradeMailToPartner($data));

		return response()->json(["success" => true]);
	}

	private function createNewDecor($newPackage, $values)
	{
		$provider = \Vendor::select("id", "name")
		                   ->where('user_id', $newPackage->user_id)
		                   ->first();

		if (!$provider)
		{
			return ["success" => false, "message" => "Provider not found with user id, $newPackage->user_id"];
		}

		$providerId = $provider->id;
		$decorBaseController = new DecorBaseController();
		$newDecor = \Decor::create([
			                           'provider_id'   => $providerId,
			                           'name'          => $newPackage->package_name,
			                           'url'           => $this->generateUrl($newPackage->package_name),
			                           'info'          => $newPackage->inclusions,
			                           'code'          => $decorBaseController->generateDecorCode(),
			                           'worth'         => $newPackage->worth,
			                           'more_info'     => "",
			                           'min_price'     => $newPackage->price,
			                           'max_price'     => 0,
			                           'handler_id'    => Auth::user()->id,
			                           'range_info'    => "",
			                           'terms'         => "",
			                           'facts'         => "",
			                           'time_setup'    => isset($values["time_setup"]) ? $values["time_setup"] : null,
			                           'time_duration' => isset($values["time_duration"]) ? $values["time_duration"] : null,
			                           'kms_free'      => isset($values["kms_free"]) ? $values["kms_free"] : null,
			                           'kms_max'       => isset($values["kms_max"]) ? $values["kms_max"] : null,
			                           'trans_min'     => isset($values["trans_min"]) ? $values["trans_min"] : null,
			                           'trans_max'     => isset($values["trans_max"]) ? $values["trans_max"] : null,
		                           ]);

		if (!$newDecor)
		{
			return ["success" => false, "message" => "Could not create new decor"];
		}

		try
		{
			DecorEvent::create([
				                   'event_id' => $newPackage->event_id,
				                   'decor_id' => $newDecor->id,
			                   ]);
			$oldPath = config('evibe.gallery.root') . '/package/new/' . $newPackage->id . '/';
			$newPath = config('evibe.gallery.root') . '/decors/' . $newDecor->id . '/images/';

			$images = PartnerPackageOptionsGallery::where('partner_option_id', $newPackage->id)
			                                      ->select("url", "title")
			                                      ->get();

			foreach ($images as $image)
			{
				$imgInsertData = [
					'url'      => $image->url,
					'title'    => $image->title,
					'decor_id' => $newDecor->id,
					'type_id'  => config('evibe.gallery.type.image'),
					'code'     => $decorBaseController->generateDecorGalleryCode()
				];

				// @todo: watermark images after they are optimised
				$this->changeImagePath($oldPath, $newPath, $image->url);
				\DecorGallery::create($imgInsertData);
			}

			$newPackage->update(['accepted_at' => Carbon::now()]);

			return [
				'success'    => true,
				'redirectTo' => route('decor.info.view', $newDecor->id)
			];
		} catch (\Exception $e)
		{
			// rollback new decor
			\DecorGallery::where('decor_id', $newDecor->id)->forceDelete();
			DecorEvent::where('decor_id', $newDecor->id)->forceDelete();
			$newDecor->forceDelete();

			return [
				"success" => false,
				"message" => "Could not copy images"
			];
		}
	}

	// @todo: optimised & handle all the error cases
	private function createNewPackage($newPackage, $values)
	{
		$provider = \Venue::where('user_id', $newPackage->user_id)->first();
		$providerTypeId = config("evibe.ticket_type.venues");
		$providerType = 'venues';
		if (!$provider)
		{
			$provider = \Vendor::where('user_id', $newPackage->user_id)->first();
			$providerTypeId = $provider->is_artist == 1 ? config("evibe.ticket_type.artists") : config("evibe.ticket_type.planners");
			$providerType = 'planner';
		}

		$createPackageController = new CreatePackageController();
		$package = \VendorPackage::create([
			                                  'name'                  => $newPackage->package_name,
			                                  'url'                   => $this->generateUrl($newPackage->package_name),
			                                  'info'                  => $newPackage->inclusions,
			                                  'code'                  => $createPackageController->generatePkgCode($provider),
			                                  'price_worth'           => $newPackage->worth,
			                                  'price'                 => $newPackage->price,
			                                  'event_id'              => $newPackage->event_id,
			                                  'city_id'               => $provider->city_id,
			                                  'map_id'                => $provider->id,
			                                  'map_type_id'           => $providerTypeId,
			                                  'price_max'             => 0,
			                                  'range_info'            => "",
			                                  'terms'                 => "",
			                                  'facts'                 => "",
			                                  'prerequisites'         => "",
			                                  'time_setup'            => isset($values["time_setup"]) ? $values["time_setup"] : null,
			                                  'time_duration'         => isset($values["time_duration"]) ? $values["time_duration"] : null,
			                                  'kms_free'              => isset($values["kms_free"]) ? $values["kms_free"] : null,
			                                  'kms_max'               => isset($values["kms_max"]) ? $values["kms_max"] : null,
			                                  'trans_min'             => isset($values["trans_min"]) ? $values["trans_min"] : null,
			                                  'trans_max'             => isset($values["trans_max"]) ? $values["trans_max"] : null,
			                                  'hall_type_id'          => isset($values["hall_type_id"]) ? $values["hall_type_id"] : null,
			                                  'guests_min'            => isset($values["guests_min"]) ? $values["guests_min"] : null,
			                                  'guests_max'            => isset($values["guests_max"]) ? $values["guests_max"] : null,
			                                  'guests_kid_min'        => isset($values["guests_kid_min"]) ? $values["guests_kid_min"] : null,
			                                  'guests_kid_max'        => isset($values["guests_kid_max"]) ? $values["guests_kid_max"] : null,
			                                  'duration_min'          => isset($values["duration_min"]) ? $values["duration_min"] : null,
			                                  'duration_max'          => isset($values["duration_max"]) ? $values["duration_max"] : null,
			                                  'group_count'           => isset($values["group_count"]) ? $values["group_count"] : null,
			                                  'price_per_extra_guest' => isset($values["price_per_extra_guest"]) ? $values["price_per_extra_guest"] : null,
			                                  'token_advance'         => isset($values["token_advance"]) ? $values["token_advance"] : null,
			                                  'check_in'              => isset($values["check_in"]) ? $values["check_in"] : null,
			                                  'check_out'             => isset($values["check_out"]) ? $values["check_out"] : null
		                                  ]);

		$oldPath = config('evibe.gallery.root') . '/package/new/' . $newPackage->id . '/';

		$newPath = config('evibe.gallery.root') . '/' . $providerType . '/' . $provider->id . '/images/packages/' . $package->id . '/';

		$images = PartnerPackageOptionsGallery::where('partner_option_id', $newPackage->id)->select("url", "title")->get();

		foreach ($images as $image)
		{
			$imgInsertData = [
				'url'                => $image->url,
				'title'              => $image->title,
				'planner_package_id' => $package->id,
				'type'               => config('evibe.gallery.type.image'),
			];
			$this->changeImagePath($oldPath, $newPath, $image->url);

			\PackageGallery::create($imgInsertData);
		}

		return route('package.edit', $package->id);
	}

	public function getPartnerByUserId($userId)
	{
		$planner = \Vendor::where('user_id', $userId)->first();
		$data = [
			'partner'   => null,
			'mapTypeId' => null
		];

		if ($planner)
		{
			$data['partner'] = $planner;
			$data['mapTypeId'] = config('evibe.ticket_type.planners');

			return $data;
		}

		$venue = \Venue::where('user_id', $userId)->first();
		if ($venue)
		{
			$data['partner'] = $venue;
			$data['mapTypeId'] = config('evibe.ticket_type.venues');

			return $data;
		}

		return $data;
	}
}
