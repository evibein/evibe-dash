<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Approval\ApprovalController;
use App\Models\Util\Approval;

use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class PartnerActivationController extends ApprovalController
{
	public function showAcceptedActivationList()
	{
		$perPage = 30;
		$activatedItems = [];
		$activations = Approval::where('is_ask_active', 0)
		                       ->where('is_live', 1)
		                       ->whereNotNull('live_at')
		                       ->orderBy('updated_at', 'DESC')
		                       ->get();

		foreach ($activations as $activation)
		{
			$activatedItems[] = $this->getItemData($activation->map_id, $activation->map_type_id);
		}

		$data = [
			'activatedItems' => $this->pagination($activatedItems, $perPage),
			'slNoCount'      => $this->getPaginatedSlNo($perPage)
		];

		return view('partner.activation.accepted')->with($data);
	}

	public function showRejectedActivationList()
	{
		$perPage = 30;
		$rejectedItems = [];
		$rejections = Approval::where('is_ask_active', 0)
		                      ->where('is_live', 0)
		                      ->whereNotNull('reject_created_at')
		                      ->orderBy('updated_at', 'DESC')
		                      ->get();

		foreach ($rejections as $rejection)
		{
			$rejectedItems[] = $this->getItemData($rejection->map_id, $rejection->map_type_id);
		}

		$data = [
			'rejectedActivations' => $this->pagination($rejectedItems, $perPage),
			'slNoCount'           => $this->getPaginatedSlNo($perPage)
		];

		return view('partner.activation.rejected')->with($data);
	}

	private function pagination($array, $perPage = 30)
	{
		// Paginate results
		$currentPage = request('page');
		$offset = ($currentPage * $perPage) - $perPage;

		return new LengthAwarePaginator(
			array_slice($array, $offset, $perPage, true),
			count($array),
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath()]
		);
	}
}
