<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Base\BaseController;

use App\Jobs\Emails\sendErrorAlertToTechTeam;
use App\Models\Package\Package;
use App\Models\Partner\PartnerDocument;
use App\Models\Types\TypeDocument;
use App\Models\User;
use App\Models\Util\AccessToken;
use App\Models\Util\AuthClient;
use App\Models\Util\BankDetails;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PartnerProfileController extends BaseController
{
	public function showEditBankDetails($userId)
	{
		$user = User::find($userId);
		$partnerData = $this->getPartnerInfoRoutes($userId);
		$partnerProfileUrl = $partnerData['route'];
		$title = $partnerData['title'];

		if ($user)
		{
			return view('partner.profile.partner_bank_details', compact('user', 'partnerProfileUrl', 'title'));
		}
		else
		{
			return redirect()->back();
		}
	}

	public function saveBankDetails($userId, Request $request)
	{
		$user = User::findorFail($userId);

		$partnerData = $this->getPartnerInfoRoutes($userId);
		$redirectRoute = $partnerData['route'];

		$rules = [
			'account_number' => 'required|numeric|digits_between:9,26',
			'name'           => 'required|min:3',
			'ifsc_code'      => 'required|alpha_num|size:11',
			'bank_name'      => 'required|min:3',
			'branch_code'    => 'required|alpha_num|min:4'
		];

		$validator = validator($request->all(), $rules);
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		// collect data
		$accountNo = trim($request->input('account_number'));
		$name = trim($request->input('name'));
		$ifscCode = trim($request->input('ifsc_code'));
		$bankName = trim($request->input('bank_name'));
		$branchCode = trim($request->input('branch_code'));
		$branchName = trim($request->input('branch_name'));
		$micrCode = trim($request->input('micr_code'));
		$branchAddress = $request->input('branch_address');

		if ($user->bankDetails)
		{
			$user->bankDetails->update([
				                           'account_number' => $accountNo,
				                           'name_in_bank'   => $name,
				                           'bank_name'      => $bankName,
				                           'branch_name'    => $branchName,
				                           'branch_code'    => $branchCode,
				                           'branch_address' => $branchAddress,
				                           'ifsc_code'      => $ifscCode,
				                           'micr_code'      => $micrCode
			                           ]);
		}
		else
		{
			BankDetails::create([
				                    'user_id'        => $userId,
				                    'account_number' => $accountNo,
				                    'name_in_bank'   => $name,
				                    'bank_name'      => $bankName,
				                    'branch_name'    => $branchName,
				                    'branch_code'    => $branchCode,
				                    'branch_address' => $branchAddress,
				                    'ifsc_code'      => $ifscCode,
				                    'micr_code'      => $micrCode
			                    ]);
		}

		return redirect($redirectRoute)->with('successMsg', 'Bank details saved successfully');
	}

	private function getPartnerInfoRoutes($userId)
	{
		$data = [
			'route' => '',
			'title' => ''
		];

		$venue = \Venue::where('user_id', $userId)->first();
		if ($venue)
		{
			$route = route('venues.view', $venue->id);
			$title = $venue->name;
			$title .= $venue->area && $venue->area->name ? ", " . $venue->area->name : "";
			$data['route'] = $route;
			$data['title'] = $title;
		}

		$vendor = \Vendor::where('user_id', $userId)->first();
		if ($vendor)
		{
			$route = route('vendor.view', $vendor->id);
			$title = $vendor->name;
			$title .= $vendor->area && $vendor->area->name ? ", " . $vendor->area->name : "";
			$data['route'] = $route;
			$data['title'] = $title;
		}

		return $data;
	}

	public function uploadPartnerDocuments($partnerId, $partnerTypeId)
	{
		$typeDocumentIds = TypeDocument::select('id', 'name')
		                               ->get();
		$viewData = [
			'typeDocumentIds' => $typeDocumentIds,
			'partnerId'       => $partnerId,
			'partnerTypeId'   => $partnerTypeId
		];

		return view('vendors.existing.upload-vendor-documents', $viewData);
	}

	public function saveUploadedDocuments($partnerId, $partnerTypeId, Request $request)
	{
		$rules = [
			'type_document_id' => 'required',
			'uploaded_file'    => 'required|mimes:jpeg,png,jpg,pdf|max:2048'
		];

		$messages = [
			'type_document_id.required' => 'Please choose the type of document',
			'uploaded_file.mimes'       => 'Please upload valid file',
			'uploaded_file.max'         => 'Please upload file less than 2 MB'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->route('upload.documents', [$partnerId, $partnerTypeId])
			                 ->withErrors($validator)
			                 ->withInput();
		}
		else
		{
			$uploadedFile = request()->file('uploaded_file');

			if ($uploadedFile)
			{
				$galleryPath = config('evibe.gallery.root');
				$originalFileName = $uploadedFile->getClientOriginalName();
				$tempServerPath = $uploadedFile->getRealPath();

				if ($partnerTypeId == config('evibe.ticket_type.planners'))
				{
					$fileUploadTo = $galleryPath . "/planner/" . $partnerId . "/documents/";
				}
				else
				{
					$fileUploadTo = $galleryPath . "/venue/" . $partnerId . "/documents/";
				}

				$typeFileNumber = 1;
				if ((substr($originalFileName, -4)) == ".pdf")
				{
					$typeFileNumber = 2;
				}

				$uploadedDocumentDetails = [
					"type_document_id"   => request('type_document_id'),
					"partner_id"         => $partnerId,
					"partner_type_id"    => $partnerTypeId,
					"file_name"          => $fileUploadTo,
					"title"              => request('title'),
					"type_file"          => $typeFileNumber,
					"original_file_name" => $originalFileName
				];

				try
				{
					if (!file_exists($fileUploadTo))
					{
						mkdir($fileUploadTo, 0777, true);
					}

					move_uploaded_file($tempServerPath, $fileUploadTo . $originalFileName);

					PartnerDocument::create($uploadedDocumentDetails);

					return redirect()->back()->with('successMessage', "Successfully Uploaded the Files");

				} catch (\Exception $e)
				{
					$sub = "Error occurred while uploading the partner documents for partnerId " . $partnerId;

					$data = [
						'sub'        => $sub,
						'errorTitle' => $e->getMessage(),
						'code'       => $e->getCode(),
						'details'    => $e->getTraceAsString()
					];

					dispatch(new sendErrorAlertToTechTeam($data));

					return redirect()->back()->with('failureMessage', "Could not upload the file. Please try again later.");
				}
			}
			else
			{
				return redirect()->back()->with('fileUploadFailure', "Unable to get the file. Please try again later.");
			}

		}
	}

	public function getAllPartners()
	{
		$allVendors = \Vendor::select('id', 'name', 'person', 'email', 'type_id')->whereNull('deleted_at')->get();
		$allVenues = \Venue::select('id', 'name', 'person', 'email', 'type_id')->whereNull('deleted_at')->get();

		$data = [
			'allVendors' => $allVendors,
			'allVenues'  => $allVenues
		];

		return view('partner/profile/all_partners', ['data' => $data]);
	}

	public function showPackages($partnerTypeId, $partnerId)
	{
		if (!in_array($partnerTypeId, [2, 3]) || $partnerId < 0)
		{
			return redirect()->back()->withErrors("Please select a valid partner");
		}

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'options/' . $partnerId . '/' . $partnerTypeId;

		$options = [
			'method'      => 'GET',
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		if (!$res["success"])
		{
			return redirect(route("partner.options.home"))->withErrors($res["error"])->withInput();
		}

		$data = [
			"data"             => $res['data'],
			"nameOfThePartner" => $res['nameOfThePartner']
		];

		return view('partner/profile/all-options-of-partner', ['data' => $data]);
	}

	public function makeApiCallWithUrl($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			return $res;

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;
			Log::error($res['error']);

			return false;
		}
	}

	public function getAccessTokenFromUserId($user)
	{
		// default client id
		$clientId = config('evibe.api.client_id');

		$token = AccessToken::where('user_id', $user->id)
		                    ->where('auth_client_id', $clientId)
		                    ->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			if ($client)
			{
				$accessToken = $this->issueAccessToken($user, $client);

				return $accessToken;
			}
			else
			{
				return redirect(route("vendor.list"));
			}
		}
		else
		{
			return $token->access_token;
		}
	}
}