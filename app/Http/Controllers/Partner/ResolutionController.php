<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Base\BaseController;
use App\Models\ServiceGallery;

class ResolutionController extends BaseController
{
	public function getImageResolutions($typeOfOption)
	{
		$galleryPath = config('evibe.gallery.root');
		$fileText = "";

		switch ($typeOfOption)
		{
			case 'cakes':
				\CakeGallery::select('url', 'cake_id', 'type')
				            ->whereNull('deleted_at')
				            ->orderBy('id')
				            ->chunk(1000, function ($imagesChunk) use ($galleryPath, &$fileText) {
					            $imagesChunk = $imagesChunk->toArray();
					            foreach ($imagesChunk as $image)
					            {
						            if ($image && $image['type'] == config('evibe.gallery.type.image'))
						            {
							            $path = $galleryPath . "/cakes/" . $image['cake_id'] . "/" . $image['url'];
							            $this->appendFile($path, $fileText, "Cake", $image['cake_id']);
						            }
					            }
				            });
				break;

			case 'decors':
				\DecorGallery::select('url', 'decor_id', 'type_id AS type')
				             ->whereNull('deleted_at')
				             ->orderBy('id')
				             ->chunk(1000, function ($imagesChunk) use ($galleryPath, &$fileText) {
					             $imagesChunk = $imagesChunk->toArray();
					             foreach ($imagesChunk as $image)
					             {
						             if ($image && $image['type'] == config('evibe.gallery.type.image'))
						             {
							             $path = $galleryPath . "/decors/" . $image['decor_id'] . "/images/" . $image['url'];;
							             $this->appendFile($path, $fileText, "Decor", $image['decor_id']);
						             }
					             }
				             });
				break;

			case 'packages':
				\PackageGallery::with('package', 'package.provider')
				               ->whereNull('deleted_at')
				               ->orderBy('id')
				               ->chunk(2000, function ($imagesChunk) use (&$fileText) {
					               foreach ($imagesChunk as $image)
					               {
						               if ($image && $image->type == config('evibe.gallery.type.image'))
						               {
							               $path = $image->getGalleryPath();
							               $this->appendFile($path, $fileText, "Package");
						               }
					               }
				               });
				break;

			case 'services':
				ServiceGallery::whereNull("deleted_at")
				              ->orderBy('id')
				              ->chunk(1000, function ($imagesChunk) use ($galleryPath, &$fileText) {
					              foreach ($imagesChunk as $image)
					              {
						              if ($image && $image->type == config('evibe.gallery.type.image'))
						              {
							              $path = $galleryPath . '/services/' . $image->type_service_id . '/images/' . $image->url;
							              $this->appendFile($path, $fileText, "Service", $image->type_service_id);
						              }
					              }
				              });
				break;

			case 'trends':
				\TrendGallery::whereNull('deleted_at')
				             ->orderBy('id')
				             ->chunk(1000, function ($imagesChunk) use ($galleryPath, &$fileText) {
					             $imagesChunk = $imagesChunk->toArray();
					             foreach ($imagesChunk as $image)
					             {
						             if ($image && $image['type'] == config('evibe.gallery.type.image'))
						             {
							             $path = $galleryPath . "/trends/" . $image['trending_id'] . "/images/" . $image['url'];
							             $this->appendFile($path, $fileText, "Trend", $image['trending_id']);
						             }
					             }
				             });
				break;

			default:
				return "Invalid Request";
				break;
		}

		$fileName = $typeOfOption . ".csv";
		$headers = [
			'Content-type'        => 'text/plain', 'test' => 'YoYo',
			'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
			'X-BooYAH'            => 'WorkyWorky', 'Content-Length' => sizeof($fileText)
		];

		return response()->make($fileText, 200, $headers);
	}

	private function appendFile($imagePath, &$csvText, $productType, $productId = 0)
	{
		try
		{
			if (!is_null($imagePath) && $imagePath && file_exists($imagePath))
			{
				$res = getimagesize($imagePath);
				if (isset($res[0]) && isset($res[1]))
				{
					$csvText .= $res[0] . ", " . $res[1] . " \n";
				}
			}
		} catch (\Exception $e)
		{
			Log::info("Exception for $productType: $productId. Error: " . $e->getTraceAsString());
		}
	}
}