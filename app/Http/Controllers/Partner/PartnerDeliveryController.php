<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Base\BaseController;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Types\TypeTicketBooking;
use App\Models\Vendor\DeliveryGallery;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PartnerDeliveryController extends BaseController
{
	public function showDeliveries(Request $request)
	{
		$headerString = "";
		$partners = [];
		$typeTicketBooking = TypeTicketBooking::select('id', 'name')->get()->toArray();
		$planners = \Vendor::select("id", "person", "name")
		                   ->get()
		                   ->keyBy('id')
		                   ->toArray();

		$venues = \Venue::select("id", "person", "name")
		                ->get()
		                ->keyBy('id')
		                ->toArray();

		foreach ($planners as $planner)
		{
			$key = config("evibe.ticket_type.planners") . '-' . $planner["id"];
			$partners[$key] = $planner["person"] . ', ' . $planner["name"];
		}

		foreach ($venues as $venue)
		{
			$key = config("evibe.ticket_type.venues") . '-' . $venue["id"];
			$partners[$key] = $venue["person"] . ', ' . $venue["name"];
		}

		// filter by date
		$startDate = $request->input('pds');
		$endDate = $request->input('pde');
		$startDateTimestamp = $startDate ? strtotime($startDate) : Carbon::now()->yesterday()->timestamp;
		$endDateTimestamp = $endDate ? strtotime($endDate) : Carbon::now()->yesterday()->addDays(1)->timestamp;

		$tickets = \Ticket::where('status_id', config('evibe.status.booked'))
		                  ->orderBy('event_date', 'DESC')
		                  ->whereBetween('ticket.event_date', [$startDateTimestamp, $endDateTimestamp]);

		// Search results
		if ($request->has('query') && $searchQuery = $request->input('query'))
		{
			$padQuery = '%' . $searchQuery . '%';

			$tickets->where(function ($query) use ($padQuery, $searchQuery) {
				$query->where('name', 'LIKE', $padQuery)
				      ->orWhere('email', 'LIKE', $padQuery)
				      ->orWhere('phone', 'LIKE', $padQuery)
				      ->orWhere('enquiry_id', 'LIKE', $padQuery);
			});
		}

		$mapTypeId = $mapId = null;
		$partner = $request->has('p') ? $request->input('p') : null;
		if ($partner)
		{
			$partnerData = explode('-', $request->input('p'));
			$mapTypeId = !empty($partnerData[0]) ? $partnerData[0] : null;
			$mapId = !empty($partnerData[1]) ? $partnerData[1] : null;
		}

		$ticketBookingsIds = \TicketBooking::select("ticket_id", "delivery_created_at", "is_advance_paid")
		                                   ->whereNotNull('delivery_created_at')
		                                   ->whereBetween('party_date_time', [$startDateTimestamp, $endDateTimestamp])
		                                   ->where('is_advance_paid', 1);

		$typeBooking = $request->has('typeBooking') ? $request->input('typeBooking') : null;
		if (!is_null($typeBooking))
		{
			$ticketBookingsIds->where('type_ticket_booking_id', $typeBooking);
		}

		if ($mapId && $mapTypeId)
		{
			$ticketBookingsIds->where(['map_id' => $mapId, 'map_type_id' => $mapTypeId]);
		}

		$ticketBookingsIds = $ticketBookingsIds->pluck("ticket_id")
		                                       ->toArray();

		$tickets = $tickets->whereIn('id', $ticketBookingsIds)
		                   ->paginate(20);

		$paginatedTicketIds = $tickets->pluck("id")->toArray();

		$params = $this->getParams();

		if (!$startDate && !$endDate)
		{
			$params['pds'] = date("d-m-Y H:i", $startDateTimestamp);
			$params['pde'] = date("d-m-Y H:i", $endDateTimestamp);
		}

		$ticketBookings = \TicketBooking::select("id", "map_type_id", "map_id", "ticket_id", 'is_delivered', 'delivery_done_at')
		                                ->whereIn("ticket_id", $paginatedTicketIds)
		                                ->whereNotNull('delivery_created_at')
		                                ->where('is_advance_paid', 1)
		                                ->get()
		                                ->toArray();

		$deliveryImageUrls = [];
		$referenceImageUrls = [];

		$deliveryImages = DeliveryGallery::select("id", "url", "type", "ticket_booking_id")
		                                 ->whereIn("ticket_booking_id", array_unique(array_column($ticketBookings, "id")))
		                                 ->get();

		$referenceImages = TicketBookingGallery::select("id", "url", "type", "ticket_booking_id")
		                                       ->whereIn("ticket_booking_id", array_unique(array_column($ticketBookings, "id")))
		                                       ->get();

		foreach ($ticketBookings as $ticketBooking)
		{
			$deliveries = $deliveryImages->where("ticket_booking_id", $ticketBooking["id"]);

			if ($deliveries->count())
			{
				foreach ($deliveries as $delivery)
				{
					$deliveryImageUrls[$ticketBooking["id"]][] = config('evibe.gallery.host') . "/ticket/" . $ticketBooking["ticket_id"] . "/" . $ticketBooking["id"] . "/partner/" . $delivery->url;
				}
			}

			$references = $referenceImages->where("ticket_booking_id", $ticketBooking["id"]);

			if ($references->count())
			{
				foreach ($references as $image)
				{
					if ($image->type == 1)
					{
						$referenceImageUrls[$ticketBooking["id"]][] = $image->url;
					}
					else
					{
						$referenceImageUrls[$ticketBooking["id"]][] = config('evibe.gallery.host') . '/ticket/' . $ticketBooking["ticket_id"] . '/' . $ticketBooking["id"] . '/' . $image->url;
					}
				}
			}
		}

		$data = [
			"tickets"           => $tickets,
			"partners"          => $partners,
			"headerString"      => $headerString,
			"mapId"             => $mapId,
			"mapTypeId"         => $mapTypeId,
			"params"            => $params,
			"typeTicketBooking" => $typeTicketBooking,
			"deliveryImages"    => $deliveryImageUrls,
			"referenceImages"   => $referenceImageUrls,
			'ticketBookings'    => $ticketBookings
		];

		return view('partner.delivery.list', $data);
	}

	public function showDeliveryDetails($ticketId)
	{
		$ticket = \Ticket::findOrFail($ticketId);

		return view('partner.delivery.details', compact('ticket'));
	}
}
