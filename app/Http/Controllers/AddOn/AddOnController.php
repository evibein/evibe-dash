<?php

namespace App\Http\Controllers\AddOn;

use App\Http\Controllers\Base\BaseController;
use App\Models\AddOn\AddOn;
use App\Models\TypeServices;
use App\Models\Util\OptionAddOnMapping;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AddOnController extends BaseController
{
	public function showAddOns()
	{
		$addOns = AddOn::all();

		$data = [
			'addOns'       => $addOns,
			'referenceTab' => config('evibe.reference.add-on.details')
		];

		return view('add-ons.list', ['data' => $data]);
	}

	public function showAddOnInfo($id)
	{
		$addOn = AddOn::find($id);

		$data = [
			'addOn'        => $addOn,
			'referenceTab' => config('evibe.reference.add-on.details')
		];

		return view('add-ons.info', ['data' => $data]);
	}

	public function showOptionMappings(Request $request)
	{
		$requiredMapTypeIds = [
			config('evibe.ticket_type.packages'),
			config('evibe.ticket_type.decors'),
			config('evibe.ticket_type.cakes'),
			config('evibe.ticket_type.entertainments'),
			config('evibe.ticket_type.food'),
			config('evibe.ticket_type.tents'),
			config('evibe.ticket_type.priests'),
			config('evibe.ticket_type.couple-experiences')
		];
		$mapTypes = \TypeTicket::whereIn('id', $requiredMapTypeIds)->get();
		$cities = \City::all();

		$cityId = $request->input('cityId');
		$mapTypeId = $request->input('mapTypeId');
		$mapId = $request->input('mapId');

		$data = [
			'mapTypes'     => $mapTypes,
			'cities'       => $cities,
			'mapTypeId'    => $mapTypeId,
			'mapId'        => $mapId,
			'referenceTab' => config('evibe.reference.add-on.option-mappings')
		];

		if ($mapTypeId && $mapId)
		{
			$name = "";
			$code = "";
			$price = "";
			$priceWorth = "";
			$profileImgUrl = "";

			switch ($mapTypeId)
			{
				case config('evibe.ticket_type.packages'):
				case config('evibe.ticket_type.food'):
				case config('evibe.ticket_type.tents'):
				case config('evibe.ticket_type.priests'):
				case config('evibe.ticket_type.couple-experiences'):
					$package = \VendorPackage::find($mapId);

					if ($package)
					{
						$name = $package->name;
						$code = $package->code;
						$price = $package->price;
						$priceWorth = $package->price_worth;
						$profileImgUrl = $package->getProfilePic();
					}

					break;

				case config('evibe.ticket_type.decors'):
					$decor = \Decor::find($mapId);

					if ($decor)
					{
						$name = $decor->name;
						$code = $decor->code;
						$price = $decor->min_price;
						$priceWorth = $decor->worth;
						$profileImgUrl = $decor->getProfilePic();
					}

					break;

				case config('evibe.ticket_type.cakes'):
					$cake = \Cake::find($mapId);

					if ($cake)
					{
						$name = $cake->title;
						$code = $cake->code;
						$price = $cake->price;
						$priceWorth = $cake->price_worth;
						$profileImgUrl = $cake->getProfilePic();
					}

					break;

				case config('evibe.ticket_type.entertainments'):
					$ent = TypeServices::find($mapId);

					if ($ent)
					{
						$name = $ent->name;
						$code = $ent->code;
						$price = $ent->min_price;
						$priceWorth = $ent->worth_price;
						$profileImgUrl = $ent->getProfilePic();
					}

					break;
			}

			$applicableAddOns = AddOn::where('city_id', $cityId)
			                         ->where('is_live', 1)
			                         ->get();

			$enabledAddOns = OptionAddOnMapping::where('option_id', $mapId)
			                                   ->where('option_type_id', $mapTypeId)
			                                   ->get()
			                                   ->keyBy('add_on_id')->toArray();

			if (count($applicableAddOns) && count($enabledAddOns))
			{
				foreach ($applicableAddOns as $applicableAddOn)
				{
					if (isset($enabledAddOns[$applicableAddOn->id]) && $enabledAddOns[$applicableAddOn->id])
					{
						$applicableAddOn->is_enable = 1;
					}
					else
					{
						$applicableAddOn->is_enable = 0;
					}
				}
			}

			$optionData = [
				'name'          => $name,
				'code'          => $code,
				'price'         => $price,
				'priceWorth'    => $priceWorth,
				'profileImgUrl' => $profileImgUrl,
				'addOns'        => $applicableAddOns
			];

			$data['optionData'] = $optionData;
		}

		return view('add-ons.option-mappings', ['data' => $data]);
	}

	public function fetchOptions(Request $request)
	{
		$res = ['success' => false];
		$typeId = $request->input('type');
		$cityId = $request->input('city');

		try
		{
			$data = $this->getMappingTypeData($typeId, $cityId);

			return response()->json(['success' => true, 'mapData' => $data]);
		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);
		}

		return response()->json($res);
	}

	public function toggleAddOnAvailability(Request $request)
	{
		$addOnId = $request->input('addOnId');
		$mapId = $request->input('mapId');
		$mapTypeId = $request->input('mapTypeId');
		$isEnable = $request->input('isEnable');

		// @see: only one way functionality
		// no check for enable/disable case mismatch

		$existingMapping = OptionAddOnMapping::withTrashed()->where('add_on_id', $addOnId)
		                                     ->where('option_id', $mapId)
		                                     ->where('option_type_id', $mapTypeId)
		                                     ->first();

		if ($isEnable)
		{
			// enable the option add-on mapping
			if ($existingMapping)
			{
				if ($existingMapping->deleted_at)
				{
					$existingMapping->deleted_at = null;
					$existingMapping->updated_at = Carbon::now();
					$existingMapping->save();
				}
				else
				{
					// already enabled
					return response()->json([
						                        'success' => false
					                        ]);
				}
			}
			else
			{
				OptionAddOnMapping::create([
					                           'add_on_id'      => $addOnId,
					                           'option_id'      => $mapId,
					                           'option_type_id' => $mapTypeId,
					                           'created_at'     => Carbon::now(),
					                           'updated_at'     => Carbon::now()
				                           ]);
			}
		}
		else
		{
			// disable the option add-on mapping
			if ($existingMapping)
			{
				if ($existingMapping->deleted_at)
				{
					// already deleted
					return response()->json([
						                        'success' => false
					                        ]);
				}
				else
				{
					$existingMapping->deleted_at = Carbon::now();
					$existingMapping->updated_at = Carbon::now();
					$existingMapping->save();
				}
			}
			else
			{
				// mapping doesn't exist to delete
				return response()->json([
					                        'success' => false
				                        ]);
			}
		}

		return response()->json([
			                        'success' => true
		                        ]);
	}
}