<?php

namespace App\Http\Controllers\Package;

use App\Models\Util\PreviousProductURLs;
use App\Models\Util\TypePackageField;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\TypeEvent;
use App\Models\TypeFacts;
use App\Models\TypePrerequisite;
use App\Http\Controllers\Base\BasePackageController;
use App\Models\Types\TypeField;
use App\Models\Util\TypePackageFieldCategory;
use App\Models\Util\PackageFieldValue;
use App\Models\Util\TypePackageFieldValue;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Exception\ClientException;

class CreatePackageController extends BasePackageController
{
	// Showing the details of the venue and occasion for creating a package
	public function showCreateNewPackagePage()
	{
		$typeEvents = TypeEvent::all();
		$typePackages = \TypeTicket::where('is_package', 1)->pluck('name', 'id');
		$vendors = \Vendor::orderBy('name')->get();
		$venues = \Venue::orderBy('name')->get();

		$data = [
			'typePackages' => $typePackages,
			'typeEvents'   => $typeEvents,
			'vendors'      => $vendors,
			'venues'       => $venues
		];

		return view('packages.add.first', ['data' => $data]);
	}

	// Showing the fields(blade files) based in vendor, venue and artist
	public function showFieldsBasedOnProvider(Request $request)
	{
		return $this->setFieldsBasedOnProvider($request->all());
	}

	// Edit package
	public function editPackage($packageId)
	{
		$package = \VendorPackage::find($packageId);
		$inputData = [
			'map_type_id' => $package->map_type_id,
			'occasion'    => $package->event_id,
			'type'        => ($package->map_type_id == config('evibe.ticket_type.venues')) ? 1 : 0
		];

		return $this->setFieldsBasedOnProvider($inputData, $package, true);
	}

	// Save submitted data
	public function savePackageData(Request $request)
	{
		$provider = null;
		$inputData = $request->all();
		$isUpdate = $request->get('isUpdate');
		$successMsg = "Package updated successfully";

		if ($isUpdate != 1)
		{
			$inputData['mapTypeId'] = config('evibe.ticket_type.venues');
			if ($request->get('type') == 0)
			{
				$provider = \Vendor::find($request->get('vendorId'));
				$roleId = $provider->type()->get()->pluck('role_id');
				$inputData['mapTypeId'] = ($roleId[0] == config('evibe.roles.planner')) ? config('evibe.ticket_type.planners') : config('evibe.ticket_type.artists');
			}
			else
			{
				$provider = \Venue::find($request->get('venueId'));
			}
			$inputData['mapId'] = $provider->id;
			$successMsg = "Package created successfully";
		}

		$inputData = $this->setCheckOutTimeData($inputData);

		$validation = $this->validatePackageInfo($inputData);

		if ($validation['error'])
		{
			return redirect()->back()->withErrors($validation['error'])->withInput($inputData);
		}
		else
		{
			$packageId = $this->createPackage($inputData, $provider, $isUpdate != 1 ? false : true);

			try
			{
				$package = \VendorPackage::find($packageId);
				$cityUrl = null;
				if (($package->provider) && ($package->provider->city->url))
				{
					$cityUrl = $package->provider->city->url;
				}

				$url = config("evibe.live.host") . "/liveUrl/delete-html-cache/" . $cityUrl . "/" . config('evibe.ticket_type.packages') . "/" . $package->id;
				$accessToken = Hash::make('deleteCacHedHTML' . Carbon::today()->startOfDay()->timestamp);

				$client = new Client([
					                     'curl' => [
						                     CURLOPT_TIMEOUT => 60,
					                     ],
				                     ]);
				$client->request('POST', $url, ['verify' => false, 'json' => ["accessToken" => $accessToken]]);
			} catch (ClientException $e)
			{
				$errorData['details'] = $e->getTraceAsString();
			}

			return redirect()->route('package.view', $packageId)->with('successMsg', $successMsg);
		}
	}

	// It will redirect to the specific page based on vendor for both new & edit package
	private function setFieldsBasedOnProvider($inputData, $package = null, $isUpdate = false)
	{
		$occasionId = $inputData['occasion'];
		$occasionObj = TypeEvent::find($occasionId);
		$occasionName = $occasionObj ? $occasionObj->name : null;

		$data = [
			'occasionName'  => $occasionName,
			'dynamicFields' => $this->generateDynamicFields($occasionId, $package, $isUpdate),
			'hallTypes'     => \TypeVenueHall::all(),
			'facts'         => TypeFacts::all(),
			'terms'         => \BookingTerms::all(),
			'preReqs'       => TypePrerequisite::all(),
			'venueTypes'    => \TypeVenue::all(),
			'package'       => $package,
			'inputData'     => $inputData
		];
		if ($isUpdate)
		{
			$inputData['vendorId'] = $package->map_id;
		}

		$isVenue = $inputData['type'];
		if ($isVenue == 0)
		{
			$vendor = \Vendor::find($inputData['vendorId']);
			if ($vendor->city_id)
			{
				$data['areas'] = \Area::where('city_id', $vendor->city_id)->get();
			}
			else
			{
				$data['areas'] = \Area::all();
			}

			$vendorType = $vendor->type()->pluck('role_id');
			if ($vendorType[0] == config('evibe.roles.artist'))
			{
				return view('packages.add.artist')->with($data);
			}
			else
			{
				return view('packages.add.vendor')->with($data);
			}
		}
		elseif ($isVenue == 1)
		{
			$venueId = (isset($inputData['venueId']) && $inputData['venueId']) ? $inputData['venueId'] : null;
			if ($isUpdate)
			{
				$venueId = $package->map_id;
			}
			if ($venueId)
			{
				$venue = \Venue::find($venueId);
				if ($venue)
				{
					// venue partner city specific areas
					if ($venue->city_id)
					{
						$data['areas'] = \Area::where('city_id', $venue->city_id)->get();
					}
					else
					{
						$data['areas'] = \Area::all();
					}

					// other venue related data that needs to be copied to the package
					$data['partner'] = [
						'areaId'      => $venue->area_id,
						'zip'         => $venue->zip,
						'landmark'    => $venue->landmark,
						'mapLat'      => $venue->map_lat,
						'mapLong'     => $venue->map_long,
						'fullAddress' => $venue->full_address,
						'typeVenueId' => $venue->type_id
					];
				}
				else
				{
					$data['areas'] = \Area::all();
				}
			}

			return view('packages.add.venue')->with($data);
		}
		else
		{
			return view('packages.add.first');
		}
	}

	// Dynamic field generation
	private function generateDynamicFields($occasionId, $package = false, $isUpdate = false)
	{
		$markupArray = [];
		$packageFieldValue = [];
		$existingValue = null;
		$dynamicFields = TypePackageField::where('occasion_id', $occasionId)->where('is_live', 1)->orderBy('type_cat_id', 'DESC')->get();

		foreach ($dynamicFields as $field)
		{
			$typeField = TypeField::find($field->type_field_id);
			$typeCat = TypePackageFieldCategory::find($field->type_cat_id);
			if (!$typeField || !$typeCat)
			{
				continue;
			}

			if ($isUpdate)
			{
				$packageFieldValue = PackageFieldValue::where('type_package_field_id', $field->id)
				                                      ->where('package_id', $package->id)
				                                      ->get();

				if ($typeField->id == config('evibe.input.checkbox'))
				{
					foreach ($packageFieldValue as $item)
					{
						$typeValue = TypePackageFieldValue::where('value', $item->value)
						                                  ->where('type_package_field_id', $item->type_package_field_id)
						                                  ->first();

						if ($typeValue)
						{
							$existingValue[] = $typeValue->id;
						}
					}
				}
			}

			$markupArray[$typeCat->name][] = [
				'typeField'      => $typeField->id,
				'field'          => $field,
				'options'        => $field->typeField,
				'oldValueId'     => count($packageFieldValue) ? $packageFieldValue[0]->meta_child_id : null,
				'oldValue'       => count($packageFieldValue) ? $packageFieldValue[0]->value : null,
				'checkBoxValues' => $existingValue
			];
		}

		return $markupArray;
	}

	// Checking for the checkIn time & checkOut field, including next day checkbox
	private function setCheckOutTimeData($allInput)
	{
		if (array_key_exists('checkOut', $allInput))
		{
			$nextDay = request()->get('checkoutNextDay') == "on" ? " (Next day)" : null;

			if ($allInput['checkOut'])
			{
				$allInput['checkOut'] = $allInput['checkOut'] . $nextDay;
			}
			$allInput['checkoutNextDay'] = request()->get('checkoutNextDay') == "on" ? "on" : "off";
		}

		return $allInput;
	}

	/** Generic function for validating all type of packages,
	 * This function except an array parameter with Valid key for validate the input data,
	 * add key here for common validation field
	 */
	private function validatePackageInfo($pkgData)
	{
		$response = [
			'success' => true,
			'error'   => false
		];

		$rules = [
			'pkgName'                  => 'required|min:4',
			'pkgPrice'                 => 'required|numeric|min:1',
			'priceMax'                 => array_key_exists('priceMax', $pkgData) ? 'integer|min_as:pkgPrice' : '',
			'rangeInfo'                => array_key_exists('rangeInfo', $pkgData) ? 'min:10|required_with:priceMax' : '',
			'pkgPriceWorth'            => 'numeric|min_as:pkgPrice',
			'setupTime'                => array_key_exists('setupTime', $pkgData) ? 'required|numeric' : '',
			'setupDuration'            => array_key_exists('setupDuration', $pkgData) ? 'required|numeric' : '',
			'kmsFree'                  => array_key_exists('kmsFree', $pkgData) ? 'required|numeric' : '',
			'kmsMax'                   => array_key_exists('kmsMax', $pkgData) ? 'required|numeric' : '',
			'transMin'                 => array_key_exists('transMin', $pkgData) ? 'required|numeric' : '',
			'transMax'                 => array_key_exists('transMax', $pkgData) ? 'required|numeric' : '',
			'pkgInfo'                  => 'required|min:10',
			'pkgTerms'                 => 'min:10',
			'pkgFacts'                 => 'min:10',
			'pkgPrereq'                => 'min:10',
			'hallType'                 => array_key_exists('hallType', $pkgData) ? 'required|integer|min:1' : '',
			'guestMin'                 => array_key_exists('guestMin', $pkgData) ? 'integer' : '',
			'guestMax'                 => array_key_exists('guestMax', $pkgData) ? 'integer|min_as:guestMin' : '',
			'guestKidMin'              => array_key_exists('guestKidMin', $pkgData) ? 'integer' : '',
			'guestKidMax'              => array_key_exists('guestKidMax', $pkgData) ? 'integer|min_as:guestKidMin' : '',
			'minDur'                   => array_key_exists('minDur', $pkgData) ? 'integer' : '',
			'maxDur'                   => array_key_exists('maxDur', $pkgData) ? 'integer|min_as:minDur' : '',
			'groupCount'               => 'required|integer',
			'pricePerPersonExtraGuest' => 'required|integer',
			'tokenAdvance'             => array_key_exists('tokenAdvance', $pkgData) ? 'integer' : ''
		];

		$messages = [
			'pkgName.required'                  => 'Name field is required',
			'pkgName.min'                       => 'Package name should be minimum 4 characters long',
			'pkgPrice.required'                 => 'Min price  is required',
			'pkgPrice.min'                      => 'Min price cannot be zero',
			'pkgPrice.numeric'                  => 'Min Price must be a number',
			'pkgPriceWorth.required'            => 'Package worth Price is required',
			'pkgPriceWorth.min_as'              => 'Price worth should be more than Min price',
			'setupTime.required'                => 'Setup time is required',
			'setupTime.numeric'                 => 'Please enter valid setup time',
			'setupDuration.required'            => 'Party duration field is required(Ex:-3)',
			'setupDuration.numeric'             => 'Party duration must be numeric',
			'pkgInfo.min'                       => 'Inclusions field should have a minimum of 10 characters',
			'pkgInfo.required'                  => 'Inclusions field is required',
			'pkgFacts.min'                      => 'Package Facts field should have a minimum of 10 characters',
			'pkgTerms.min'                      => 'Package Terms field should have a minimum of 10 characters',
			'pkgPrereq.min'                     => 'Package Terms field should have a minimum of 10 characters',
			'kmsFree.required'                  => 'Free Kms is required(if n / a, enter 0)',
			'kmsFree.numeric'                   => 'Free Kms must be a number(if n / a, enter 0)',
			'kmsMax.required'                   => 'Maximum kms is required(if n / a, enter 0)',
			'kmsMax.numeric'                    => 'Maximum KMs must be a number(if n / a, enter 0)',
			'kmsMax.min_as'                     => 'Kms Max should be greater than kms Min',
			'transMin.required'                 => 'Minimum transportation charge is required(if n / a, enter 0)',
			'transMin.numeric'                  => 'Minimum transportation charge must be a number(if n / a, enter 0)',
			'transMax.required'                 => 'Maximum transportation charge must be a number(if n / a, enter 0)',
			'transMax.numeric'                  => 'Minimum transportation charge is required(if n / a, enter 0)',
			'transMax.min_as'                   => 'Maximum transportation charge should be greater than Minimum transportation charge',
			'guestMin.integer'                  => 'Min guests is invalid',
			'guestMax.integer'                  => 'Max guests is invalid',
			'guestMax.min_as'                   => 'Max guests count cannot be less than min guests',
			'guestKidMin.integer'               => 'Min kids guests in valid',
			'guestKidMax.integer'               => 'Max kids guests in valid',
			'guestKidMax.min_as'                => 'Max kids guests cannot be less than min kid guests',
			'minDur.integer'                    => 'Min duration is invalid(enter in minutes)',
			'maxDur.integer'                    => 'Max duration is invalid(enter in minutes)',
			'maxDur.min_as'                     => 'Max duration cannot be less than min duration',
			'priceMax.min_as'                   => 'Price Max cannot be less than Price Min',
			'hallType.min'                      => 'Please select a valid property type',
			'pricePerPersonExtraGuest.required' => 'Price per person extra guest is required (if N/A enter 0)',
			'pricePerPersonExtraGuest.integer'  => 'Price per person extra guest must be a number',
			'groupCount.required'               => 'Group count is required, if N/A (enter 0)',
			'groupCount.integer'                => 'Group count must be a number',
			'tokenAdvance.integer'              => 'Please enter a valid token advance amount'
		];

		if (array_key_exists('kmsFree', $pkgData) && array_key_exists('kmsMax', $pkgData))
		{
			($pkgData['kmsFree'] == 0 && $pkgData['kmsMax'] == 0) ? null : array_set($rules, 'kmsMax', 'required|numeric|min_as:kmsFree');
		}
		if (array_key_exists('transMin', $pkgData) && array_key_exists('transMax', $pkgData))
		{
			($pkgData['transMin'] == 0 && $pkgData['transMax'] == 0) ? null : array_set($rules, 'transMax', 'required|numeric|min_as:transMin');
		}
		if ($pkgData['mapTypeId'] == config('evibe.ticket_type.artists'))
		{
			array_set($rules, 'pkgPriceWorth', 'min_as:pkgPrice');
			array_set($rules, 'pkgInfo', 'required|min:10');
			$messages['pkgInfo.required'] = 'Package Description field is required';
			$messages['pkgInfo.required'] = 'Package Description field is required';
		}

		//this validation for dynamically generated field of venue packages
		if ($pkgData['mapTypeId'] == config('evibe.ticket_type.venues') && array_key_exists('occasion', $pkgData))
		{
			array_set($rules, 'groupCount', 'required|integer|min:1');

			// validation for venue address
			$rules['area'] = 'required|numeric|min:1';
			$rules['zip'] = 'required|numeric|digits:6';
			$rules['landmark'] = 'required|min:4';
			$rules['mapLat'] = 'required|numeric';
			$rules['mapLong'] = 'required|numeric';
			$rules['fullAddress'] = 'required|min:10';
			$rules['venueType'] = 'required|numeric|min:1';

			$messages['area.required'] = 'Kindly select a venue area';
			$messages['area.min'] = 'Kindly select a venue area';
			$messages['zip.required'] = 'Kindly enter venue area zip';
			$messages['zip.digits'] = 'Kindly enter a valid zip';
			$messages['landmark.required'] = 'Kindly enter a venue landmark';
			$messages['landmark.min'] = 'Venue landmark cannot be less that 4 characters';
			$messages['mapLat.required'] = 'Kindly enter venue location latitude';
			$messages['mapLat.numeric'] = 'Kindly enter a valid venue location latitude';
			$messages['mapLong.required'] = 'Kindly enter venue location longitude';
			$messages['mapLong.numeric'] = 'Kindly enter a valid venue location longitude';
			$messages['fullAddress.required'] = 'Kindly enter venue full address';
			$messages['fullAddress.min'] = 'Venue address cannot be less than 10 characters';
			$messages['venueType.required'] = 'Venue select a venue type';
			$messages['venueType.min'] = 'Venue select a venue type';

			// CLD related fields
			$rules['packageTagLine'] = 'min:10';
			$rules['aboutPackage'] = 'min:10';
			$rules['bookBeforeHrs'] = 'numeric';
			$rules['tableSetup'] = 'min:4';

			$messages['packageTagLine.min'] = 'Package tag line should not be less than 10 characters';
			$messages['aboutPackage.min'] = 'About package description should not be less than 10 characters';
			$messages['bookBeforeHrs.numeric'] = 'Book before hours should be a number';
			$messages['tableSetup.min'] = 'Table setup should not be less than 10 characters (Eg: Rooftop, Poolside, etc,.)';

			$rules['checkIn'] = 'required|required_with:checkOut';
			$messages['checkIn.required'] = 'Check-in time is required';
			$messages['checkIn.required_with'] = 'Check in time is required with check out time';
			$messages['groupCount.min'] = 'Group count must be greater than zero';
			$messages['groupCount.required'] = 'Group count is required';

			$typeFields = TypePackageField::where('occasion_id', $pkgData['occasion'])->where('is_live', 1)->get();
			foreach ($typeFields as $typeField)
			{
				if ($typeField->validation && $typeField->fieldType)
				{
					$rules[$typeField->name] = $typeField->validation;
				}
			}
		}
		if ($pkgData['mapTypeId'] == config('evibe.ticket_type.planners'))
		{
			$messages['pkgPrice.required'] = 'Package price  is required';
			$messages['pkgPrice.min'] = 'Package price  cannot be zero';
			$messages['pkgPrice.numeric'] = 'Package price must be a number';
			$messages['pkgPriceWorth.min_as'] = 'Package price worth should be more than price';

		}

		$validation = Validator::make($pkgData, $rules, $messages);
		if ($validation->fails())
		{
			$response['success'] = false;
			$response['error'] = $validation->messages()->first();
		}

		return $response;
	}

	// generic function for adding all type of packages
	private function createPackage($pkgData, $provider, $isUpdate = false)
	{
		$packageData = $this->filterData($pkgData, $provider, $isUpdate);

		if ($isUpdate)
		{
			$package = \VendorPackage::find($pkgData['packageId']);

			if ($package && isset($pkgData['pkgName']) && ($package->name != $pkgData['pkgName']))
			{
				if ($package->map_type_id == config("evibe.ticket_type.venues"))
				{
					$areaName = $package->area ? $package->area->name : "";
					$packageData['url'] = $this->generateUrl($pkgData['pkgName'] . " " . $areaName, false);
				}
				else
				{
					$packageData['url'] = $this->generateUrl($pkgData['pkgName'], false);
				}

				$isValidUrl = \VendorPackage::where("url", $packageData['url'])->first();
				$packageData["url"] = $isValidUrl ? $packageData["url"] . "-" . $package->id : $packageData["url"];

				PreviousProductURLs::create([
					                            "product_type_id" => config("evibe.ticket_type.packages"),
					                            "product_id"      => $package->id,
					                            "url"             => $package->url
				                            ]);
			}

			$package->update($packageData);
			$this->updateDynamicField($pkgData);

			//Flushing the Tags which is made in main based on the Code of that product
			//Cache::tags($package->code)->flush();

			return $package->id;
		}
		else
		{
			$package = \VendorPackage::create($packageData);

			$duplicateName = \VendorPackage::where("url", "LIKE", $package->url)->get();
			if ($duplicateName->count() > 1)
			{
				$package->update([
					                 "url" => $package->url . "-" . $package->id
				                 ]);
			}
			$this->createDynamicField($pkgData, $package->id);

			return $package->id;
		}
	}

	// create dynamic field in DB
	private function createDynamicField($pkgData, $pkgId)
	{
		$isDynamicField = false;
		try
		{
			if (array_key_exists('occasion', $pkgData))
			{
				if (($pkgData['occasion'] == config("evibe.event.special_experience")) || (($pkgData['occasion'] != config("evibe.event.special_experience")) && ($pkgData['mapTypeId'] == config('evibe.ticket_type.venues'))))
				{
					$dynamicFields = TypePackageField::where('occasion_id', $pkgData['occasion'])->where('is_live', 1)->get();

					foreach ($dynamicFields as $dynamicField)
					{
						// saving the checkbox value directly in package_field_value table
						if (isset($pkgData[$dynamicField->name]))
						{
							$this->createEachTypeDynamicField($dynamicField, $pkgData, $pkgId);
						}
					}
				}
				$isDynamicField = true;
			}
			else
			{
				$isDynamicField = true;
			}
		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);
			session()->flash('packageErrorMsg', 'Package Couldn\'t created successfully, Error while saving generated fields');
			if (!$isDynamicField)
			{
				\VendorPackage::find($pkgId)->delete();
			}
		}

		return $isDynamicField;
	}

	// Create each dynamic field in db
	private function createEachTypeDynamicField($dynamicField, $pkgData, $pkgId)
	{
		if (!$dynamicField->fieldType)
		{
			return false;
		}

		if ($dynamicField->type_field_id == config('evibe.input.checkbox'))
		{
			foreach ($pkgData[$dynamicField->name] as $checkbox)
			{
				$typePackageFieldValue = TypePackageFieldValue::where('id', $checkbox)->first();

				PackageFieldValue::create([
					                          'package_id'            => $pkgId,
					                          'type_package_field_id' => $dynamicField->id,
					                          'value'                 => trim($typePackageFieldValue->value),
					                          'meta_child_id'         => $typePackageFieldValue->id,
				                          ]);
			}
		}
		elseif ($dynamicField->type_field_id == config('evibe.input.option'))
		{
			$typePkgFieldVal = TypePackageFieldValue::where('id', $pkgData[$dynamicField->name])->first();

			PackageFieldValue::create([
				                          'package_id'            => $pkgId,
				                          'type_package_field_id' => $dynamicField->id,
				                          'value'                 => $typePkgFieldVal->value,
				                          'meta_child_id'         => $typePkgFieldVal->id
			                          ]);
		}
		else
		{
			PackageFieldValue::create([
				                          'package_id'            => $pkgId,
				                          'type_package_field_id' => $dynamicField->id,
				                          'value'                 => trim($pkgData[$dynamicField->name])
			                          ]);
		}
	}

	// Update the existing values of dynamic fields or create a new one
	private function updateDynamicField($pkgData)
	{
		if (array_key_exists('occasion', $pkgData))
		{
			if (($pkgData['occasion'] == config("evibe.event.special_experience")) || (($pkgData['occasion'] != config("evibe.event.special_experience")) && ($pkgData['mapTypeId'] == config('evibe.ticket_type.venues'))))
			{
				// it means that the package field value is available it means dynamic field value is created at the time of package creation
				$dynamicFields = TypePackageField::where('occasion_id', $pkgData['occasion'])->where('is_live', 1)->get();

				if (!is_null($dynamicFields))
				{
					foreach ($dynamicFields as $dynamicField)
					{
						if (!$dynamicField->fieldType)
						{
							continue;
						}
						if (PackageFieldValue::where('package_id', $pkgData['packageId'])->where('type_package_field_id', $dynamicField->id)->first())
						{
							if ($dynamicField->type_field_id == config('evibe.input.checkbox'))
							{
								//delete the save;
								PackageFieldValue::where('package_id', $pkgData['packageId'])
								                 ->where('type_package_field_id', $dynamicField->id)
								                 ->forceDelete();

								foreach ($pkgData[$dynamicField->name] as $checkbox)
								{
									$typePackageFieldValue = TypePackageFieldValue::where('id', $checkbox)->first();

									PackageFieldValue::create([
										                          'package_id'            => $pkgData['packageId'],
										                          'type_package_field_id' => $dynamicField->id,
										                          'value'                 => $typePackageFieldValue->value,
										                          'meta_child_id'         => $typePackageFieldValue->id
									                          ]);
								}

							}
							elseif ($dynamicField->type_field_id == config('evibe.input.option'))
							{
								$packageOptionFieldValue = PackageFieldValue::where('package_id', $pkgData['packageId'])
								                                            ->where('type_package_field_id', $dynamicField->id)
								                                            ->first();
								$typePackageFieldVal = TypePackageFieldValue::where('id', $pkgData[$dynamicField->name])->first();

								$packageOptionFieldValue->update([
									                                 'value'         => $typePackageFieldVal->value,
									                                 'meta_child_id' => $typePackageFieldVal->id
								                                 ]);

							}
							else
							{
								$packageOtherFieldValue = PackageFieldValue::where('package_id', $pkgData['packageId'])
								                                           ->where('type_package_field_id', $dynamicField->id)
								                                           ->first();

								$packageOtherFieldValue->update(['value' => $pkgData[$dynamicField->name]]);
							}
						}
						else
						{
							$this->createEachTypeDynamicField($dynamicField, $pkgData, $pkgData['packageId']);
						}
					}
				}
			}
		}
	}

	/**
	 * Filter the data based on different input values of packages
	 * Remember to add the key in the parameter array, when you add a new field in the database
	 */
	private function filterData($pkgData, $provider, $isUpdate = false)
	{
		$packageData = [
			'name'                  => $pkgData['pkgName'],
			'price'                 => $pkgData['pkgPrice'],
			'info'                  => $pkgData['pkgInfo'],
			'price_worth'           => $pkgData['pkgPriceWorth'],
			'facts'                 => array_key_exists('pkgFacts', $pkgData) ? $pkgData['pkgFacts'] : null,
			'range_info'            => array_key_exists('rangeInfo', $pkgData) ? $pkgData['rangeInfo'] : null,
			'prerequisites'         => array_key_exists('pkgPrereq', $pkgData) ? $pkgData['pkgPrereq'] : null,
			'terms'                 => array_key_exists('pkgTerms', $pkgData) ? $pkgData['pkgTerms'] : null,
			'time_setup'            => array_key_exists('setupTime', $pkgData) ? $pkgData['setupTime'] : null,
			'time_duration'         => array_key_exists('setupDuration', $pkgData) ? $pkgData['setupDuration'] : null,
			'kms_free'              => array_key_exists('kmsFree', $pkgData) ? $pkgData['kmsFree'] : null,
			'kms_max'               => array_key_exists('kmsMax', $pkgData) ? $pkgData['kmsMax'] : null,
			'trans_min'             => array_key_exists('transMin', $pkgData) ? $pkgData['transMin'] : null,
			'trans_max'             => array_key_exists('transMax', $pkgData) ? $pkgData['transMax'] : null,
			'duration_min'          => array_key_exists('minDur', $pkgData) ? $pkgData['minDur'] : null,
			'duration_max'          => array_key_exists('maxDur', $pkgData) ? $pkgData['maxDur'] : null,
			'guests_kid_min'        => array_key_exists('guestKidMin', $pkgData) ? $pkgData['guestKidMin'] : null,
			'guests_kid_max'        => array_key_exists('guestKidMax', $pkgData) ? $pkgData['guestKidMax'] : null,
			'guests_min'            => array_key_exists('guestMin', $pkgData) ? $pkgData['guestMin'] : null,
			'guests_max'            => array_key_exists('guestMax', $pkgData) ? $pkgData['guestMax'] : null,
			'price_max'             => array_key_exists('priceMax', $pkgData) ? $pkgData['priceMax'] : null,
			'hall_type_id'          => array_key_exists('hallType', $pkgData) ? $pkgData['hallType'] : null,
			'check_in'              => array_key_exists('checkIn', $pkgData) ? $pkgData['checkIn'] : null,
			'check_out'             => array_key_exists('checkOut', $pkgData) ? $pkgData['checkOut'] : null,
			'token_advance'         => array_key_exists('tokenAdvance', $pkgData) ? $pkgData['tokenAdvance'] : null,
			'price_per_extra_guest' => $pkgData['pricePerPersonExtraGuest'],
			'group_count'           => $pkgData['groupCount'],
			'area_id'               => array_key_exists('area', $pkgData) ? $pkgData['area'] : null,
			'zip'                   => array_key_exists('zip', $pkgData) ? $pkgData['zip'] : null,
			'landmark'              => array_key_exists('landmark', $pkgData) ? $pkgData['landmark'] : null,
			'full_address'          => array_key_exists('fullAddress', $pkgData) ? $pkgData['fullAddress'] : null,
			'map_lat'               => array_key_exists('mapLat', $pkgData) ? $pkgData['mapLat'] : null,
			'map_long'              => array_key_exists('mapLong', $pkgData) ? $pkgData['mapLong'] : null,
			'type_venue_id'         => array_key_exists('venueType', $pkgData) ? $pkgData['venueType'] : null,
			'package_tag_line'      => array_key_exists('packageTagLine', $pkgData) ? $pkgData['packageTagLine'] : null,
			'about_package'         => array_key_exists('aboutPackage', $pkgData) ? $pkgData['aboutPackage'] : null,
			'book_before_hrs'       => array_key_exists('bookBeforeHrs', $pkgData) ? $pkgData['bookBeforeHrs'] : null,
			'table_setup'           => array_key_exists('tableSetup', $pkgData) ? $pkgData['tableSetup'] : null,
			'updated_at'            => Carbon::now(),
			'is_live'               => 0
		];

		// Will not update the following data if its a ticket update
		if (!$isUpdate)
		{
			$packageData['map_id'] = $provider->id;
			$packageData['event_id'] = $pkgData['occasion'];
			$packageData['type_ticket_id'] = $pkgData['typePackage'];
			$packageData['code'] = $this->generatePkgCode($provider);
			$packageData['url'] = $this->generateUrl($pkgData['pkgName'] . " " . $provider->area->name, false);
			$packageData['map_type_id'] = $pkgData['mapTypeId'];
			$packageData['city_id'] = $provider->city->id;
			$packageData['created_at'] = Carbon::now();
		}

		return $packageData;
	}

	// Generate code for packages
	public function generatePkgCode($vendor, $lastCode = null)
	{
		$prefix = config('evibe.code.prefix.package');
		$cityCode = $vendor->city->code;

		if (!$lastCode)
		{
			$lastObj = \VendorPackage::withTrashed()->orderBy('id', 'DESC')->first();
			$lastCode = $lastObj->code;
			$digits = ((int)substr($lastCode, -4)) + 1;
		}
		else
		{
			$digits = (int)substr($lastCode, -4);
		}

		return $cityCode . $prefix . $digits;
	}
}
