<?php

namespace App\Http\Controllers\Package;

use App\Models\InternalQuestion;
use App\Models\Package\PackageItineraryName;
use App\Models\TypeEvent;
use App\Models\Util\SellingStatus;
use App\Models\Util\SurpriseTagMappings;
use App\Models\Util\TypeSellingStatus;
use Carbon\Carbon;
use App\Http\Controllers\Base\BasePackageController;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PackageController extends BasePackageController
{
	public function showAllPackagesList()
	{
		return $this->showPackagesList();
	}

	public function showVendorPackagesList()
	{
		return redirect(route('packages.all.list'));
	}

	public function showVenuePackagesList()
	{
		return redirect(route('packages.all.list'));
	}

	public function showArtistPackagesList()
	{
		return redirect(route('packages.all.list'));
	}

	public function viewVendorPackageDetails($packageId)
	{
		return $this->viewPackageDetails($packageId);
	}

	public function viewVenuePackageDetails($packageId)
	{
		return $this->viewPackageDetails($packageId);
	}

	public function viewArtistPackageDetails($packageId)
	{
		return $this->viewPackageDetails($packageId);
	}

	/**
	 * DELETE A PACKAGE
	 */
	public function getDelete($packageId)
	{
		$package = \VendorPackage::findOrFail($packageId);

		try
		{
			$cityUrl = null;
			if (($package->provider) && ($package->provider->city->url))
			{
				$cityUrl = $package->provider->city->url;
			}

			$url = config("evibe.live.host") . "/liveUrl/delete-html-cache/" . $cityUrl . "/" . config('evibe.ticket_type.packages') . "/" . $package->id;
			$accessToken = Hash::make('deleteCacHedHTML' . Carbon::today()->startOfDay()->timestamp);

			$client = new Client([
				                     'curl' => [
					                     CURLOPT_TIMEOUT => 60,
				                     ],
			                     ]);
			$client->request('POST', $url, ['verify' => false, 'json' => ["accessToken" => $accessToken]]);
		} catch (ClientException $e)
		{
			$errorData['details'] = $e->getTraceAsString();
		}

		if ($package->delete())
		{
			//Cache::tags($package->code)->flush();

			return response()->json(['success' => true]);
		}
	}

	/**
	 * ACTIVATE A PACKAGE
	 */
	public function activate($packageId)
	{
		$package = \VendorPackage::findOrFail($packageId);
		$package->update(['is_live' => 1]);

		$this->checkAndSendFirstActivationEmail($package->event_id, config('evibe.ticket_type.packages'), $package, $package->map_type_id);

		return redirect()->back();
	}

	/**
	 * DEACTIVATE A PACKAGE
	 */
	public function deActivate($packageId)
	{
		$package = \VendorPackage::findOrFail($packageId);
		$package->is_live = 0;

		if ($package->save())
		{
			//Cache::tags($package->code)->flush();
			//$this->removeItemsFromAutoBookAndPriorityTable(config('evibe.ticket_type.packages'), $packageId);

			try
			{
				$cityUrl = null;
				if (($package->provider) && ($package->provider->city->url))
				{
					$cityUrl = $package->provider->city->url;
				}

				$url = config("evibe.live.host") . "/liveUrl/delete-html-cache/" . $cityUrl . "/" . config('evibe.ticket_type.packages') . "/" . $package->id;
				$accessToken = Hash::make('deleteCacHedHTML' . Carbon::today()->startOfDay()->timestamp);

				$client = new Client([
					                     'curl' => [
						                     CURLOPT_TIMEOUT => 60,
					                     ],
				                     ]);
				$client->request('POST', $url, ['verify' => false, 'json' => ["accessToken" => $accessToken]]);
			} catch (ClientException $e)
			{
				$errorData['details'] = $e->getTraceAsString();
			}

			return redirect()->back();
		}
	}

	// Generic function for showing all type of package list
	private function showPackagesList()
	{
		$priceMinSelected = [];
		$priceMaxSelected = [];
		$categorySelected = [];
		$areas = \Area::all();
		$allPlanners = \Vendor::select('id', 'name', 'area_id')->get();
		$allVenues = \Venue::select('id', 'name', 'area_id')->get();

		// @todo: Another solution to get tags, existing query is taking too much time to load
		//$packageTagIds = \PackageTag::distinct()->pluck('tile_tag_id');
		//$packageFilterTag = $this->getFilterableCategories($packageTagIds);

		$packageFilterTag = "";
		$packages = \VendorPackage::with('provider');

		// Sort by city
		$cityId = request()->has('city') && request('city') > 0 ? request()->input('city') : "";
		if ($cityId)
		{
			$packages->where('city_id', $cityId);
		}

		// Sort by package type
		$typePackageId = request()->has('typePackage') && request('typePackage') > 0 ? request()->input('typePackage') : "";
		if ($typePackageId)
		{
			$packages->where('map_type_id', $typePackageId);
		}

		// Sort by occasion type
		$eventId = request()->has('event') && request('event') > 0 ? request()->input('event') : "";
		if ($eventId)
		{
			$packages->where('event_id', $eventId);
		}

		// Search name, code, phone
		if (request()->has('query') && $searchQuery = request()->input('query'))
		{
			$perecentilify = '%' . $searchQuery . '%';

			$packages = $packages->where(function ($query) use ($perecentilify, $searchQuery) {
				$query->where('name', 'LIKE', $perecentilify)
				      ->orWhere('code', 'LIKE', $perecentilify)
				      ->orWhere('price', 'LIKE', $perecentilify);
			});
		}

		// Sort by price range
		if (request()->has('priceRange'))
		{
			$priceRange = request()->input('priceRange');
			$priceRangeList = explode(",", $priceRange);
			$priceMinSelected = trim($priceRangeList[0]) ? trim($priceRangeList[0]) : null;
			$priceMaxSelected = trim($priceRangeList[1]) ? trim($priceRangeList[1]) : null;
			if (!is_null($priceMinSelected) && !is_null($priceMaxSelected))
			{
				$packages->whereBetween('price', [$priceMinSelected, $priceMaxSelected])
				         ->orwhereBetween('price_max', [$priceMinSelected, $priceMaxSelected]);
			}
			elseif (!is_null($priceMinSelected) && is_null($priceMaxSelected))
			{
				$packages->where('price', '>=', $priceMinSelected)
				         ->orwhere('price_max', '>=', $priceMinSelected);
			}
			elseif (is_null($priceMinSelected) && !is_null($priceMaxSelected))
			{
				$packages->where('price', '<=', $priceMaxSelected)
				         ->orwhere('price_max', '<=', $priceMaxSelected);
			}
		}

		// Sort by selling status
		$sellingStatusId = request()->input('sellingStatus');
		if ($sellingStatusId > 0)
		{
			if ($sellingStatusId != config("evibe.selling.status.default"))
			{
				$sellingStatusDecorIds = SellingStatus::where("product_type_id", config("evibe.ticket_type.packages"))
				                                      ->where("product_selling_status_id", $sellingStatusId)
				                                      ->pluck("product_id");

				$packages->whereIn('id', $sellingStatusDecorIds);
			}
			else
			{
				$sellingStatusDecorIds = SellingStatus::where("product_type_id", config("evibe.ticket_type.packages"))
				                                      ->where("product_selling_status_id", '<>', $sellingStatusId)
				                                      ->pluck("product_id");

				$packages->whereNotIn('id', $sellingStatusDecorIds);
			}
		}

		// Sort by location
		if (request()->has('location') && request()->input('location'))
		{
			$locationId = intval(request()->input('location'));

			$plannerIdsList = $allPlanners->where('area_id', $locationId)->pluck('id');
			$venueIdsList = $allVenues->where('area_id', $locationId)->pluck('id');

			$packages->where(function ($query) use ($plannerIdsList, $venueIdsList) {
				$query->whereIn('map_type_id', [config("evibe.ticket_type.planners"), config("evibe.ticket_type.artists")])
				      ->WhereIn('map_id', $plannerIdsList->toArray());

				$query->orWhere('map_type_id', config("evibe.ticket_type.venues"))
				      ->WhereIn('map_id', $venueIdsList->toArray());
			});
		}

		// Sort by partner
		if (request()->has('partner') && request()->input('partner'))
		{
			$partnerTypeId = intval(explode('_', request()->input('partner'))[0]);
			$partnerId = intval(explode('_', request()->input('partner'))[1]);

			$partnerTypeId = isset($partnerTypeId) ? $partnerTypeId : "";
			$partnerId = isset($partnerId) ? $partnerId : "";

			$packages->where('map_type_id', $partnerTypeId)
			         ->where('map_id', $partnerId);
		}

		// Sort by tags
		if (request()->has('category'))
		{
			$categorySelected = explode(",", urldecode(request()->input('category')));
			$packageId = \PackageTag::whereIn('tile_tag_id', $categorySelected)->pluck('planner_package_id');
			$packages->whereIn('id', $packageId);
		}

		$locationList = [];

		// @todo: optimize the query
		//get the location list from vendor list
		//foreach ($packages->get() as $package)
		//{
		//	if ($package->provider && ($area = $areas->where('id', $package->provider->area_id)->first()))
		//	{
		//		if (!in_array($area->name, $locationList))
		//		{
		//			$locationList[$area->id] = $area->name;
		//		}
		//	}
		//}

		// Sort results
		if (request()->has('sort'))
		{
			switch (request()->input('sort'))
			{
				case 'created-first':
					$packages->orderBy('created_at', 'DESC')
					         ->orderBy('priority', 'DESC');
					break;
				case 'created-last':
					$packages->orderBy('created_at', 'ASC')
					         ->orderBy('priority', 'DESC');
					break;
				case 'price-asc':
					$packages->orderBy('price', 'ASC')
					         ->orderBy('priority', 'DESC');
					break;
				case 'price-desc':
					$packages->orderBy('price', 'DESC')
					         ->orderBy('priority', 'DESC');
					break;
			}
		}
		else
		{
			// DEFAULT SORT
			$packages->orderBy('created_at', 'DESC');
		}

		$packages = $packages->paginate(24);
		$packageGallery = \PackageGallery::whereIn('planner_package_id', $packages->pluck('id'))->get();

		$data = [
			'packages'            => $packages,
			'locationList'        => $locationList,
			'filterCategoryTag'   => $packageFilterTag,
			'existingQueryParams' => self::getExistingQueryParams(),
			'priceMinSelected'    => $priceMinSelected,
			'priceMaxSelected'    => $priceMaxSelected,
			'categorySelected'    => $categorySelected,
			'cities'              => \City::all(),
			'occasions'           => TypeEvent::all(),
			'planners'            => $allPlanners,
			'venues'              => $allVenues,
			'packageGallery'      => $packageGallery,
			'typeSellingStatus'   => TypeSellingStatus::all()
		];

		return view('packages/packages', ['data' => $data]);
	}

	// Generic function for showing all types of package details
	public function viewPackageDetails($packageId)
	{
		$package = \VendorPackage::find($packageId);
		if (!$package)
		{
			redirect()->route('packages.all.list');
		}

		$dynamicFields = $this->getVenuePackageDynamicField($package);

		$itineraries = PackageItineraryName::where('package_id', $packageId)->get();
		$Iqs = InternalQuestion::withTrashed()
		                       ->where(['map_id' => $packageId, 'map_type_id' => config('evibe.ticket_type.packages')])
		                       ->orderBy('deleted_by')
		                       ->orderBy('updated_at', 'DESC')
		                       ->get();

		$gallery = \PackageGallery::where('planner_package_id', $packageId)->orderBy('updated_at', "DESC")->get();

		$images = $gallery->filter(function ($query) {
			return $query->type == config('evibe.gallery.type.image');
		});

		$videos = $gallery->filter(function ($query) {
			return $query->type == config('evibe.gallery.type.video');
		});

		$typeSellingStatus = TypeSellingStatus::select("id", "name", "color")->get();
		$sellingStatus = SellingStatus::where('product_type_id', config("evibe.ticket_type.packages"))->where('product_id', $package->id)->first();

		$sTags = [];
		$surCatTags = [];

		if ($package->event_id == config("evibe.event.special_experience"))
		{
			$allTags = \Tags::all();

			$surpriseTags = SurpriseTagMappings::where('is_negative', '!=', 1)->get();
			foreach ($surpriseTags as $tag)
			{
				$sTags[$allTags->find($tag->sr_id) ? $allTags->find($tag->sr_id)->name : "Other"][] = ($allTags->find($tag->so_id) ? $allTags->find($tag->so_id)->name : ($allTags->find($tag->sa_id) ? $allTags->find($tag->sa_id)->name : "Other"));
			}

			$surCatTags = \PackageTag::join('type_tags', 'planner_package_tags.tile_tag_id', '=', 'type_tags.id')
			                         ->where('planner_package_id', $package->id)
			                         ->whereIn('type_tags.parent_id', [config('evibe.tags.surprise_relation'), config('evibe.tags.surprise_occasion'), config('evibe.tags.surprise_age_group')])
			                         ->select('type_tags.id', 'type_tags.name', 'type_tags.parent_id')
			                         ->get();
		}

		$productTypeId = $package->type_ticket_id;

		$data = [
			'package'           => $package,
			'itineraries'       => $itineraries,
			'iqs'               => $Iqs,
			'dynamicFields'     => $dynamicFields,
			'videos'            => $videos,
			'images'            => $images,
			'tagList'           => $this->getTagsList($productTypeId),
			'tags'              => $package->getTags(),
			'sTags'             => $sTags,
			'surCatTags'        => $surCatTags,
			'sellingStatus'     => $sellingStatus,
			'typeSellingStatus' => $typeSellingStatus,
			'typeTicket'        => \TypeTicket::select('id', 'name')->where('is_package', 1)->get()->toArray()
		];

		$approvalVisibility = $this->getApprovalVisibility($packageId, config('evibe.ticket_type.packages'));
		$data = array_merge($approvalVisibility, $data);

		return view('packages/viewPackageDetails', $data);

	}

	public function getTypeTickets()
	{
		$typeTicketOptions = \TypeTicket::select('id', 'name')->where('is_package', 1)->get()->toArray();

		$res = [
			'success'    => true,
			'typeTicket' => $typeTicketOptions
		];

		return response()->json($res);
	}

	public function updatePackageTypeTicket($packageId)
	{
		$package = \VendorPackage::find($packageId);

		if ($package)
		{

			// make item non live if its from BD
			$package->update([
				                 "is_live"        => (auth()->user()->role_id == config('evibe.roles.bd')) ? 0 : 1,
				                 "type_ticket_id" => Input::get('typeTicketId')
			                 ]);

			$res = [
				'success' => true,
				'message' => "Successfully updated the package occasion"
			];

			return response()->json($res);
		}
		else
		{
			$res = [
				'success' => false,
				'message' => "Package not found"
			];

			return response()->json($res);
		}
	}

	public function editPackage($packageId)
	{
		$package = \VendorPackage::find($packageId);
		$provider = $package->provider;

		if ($provider)
		{
			$user = $provider->user;
			if ($user)
			{
				$host = config('evibe.pro_host');
				$token = Hash::make($user->username);

				$redirectUrl = $host . '/login/auto/' . $user->id . '?token=' . $token . '&pId=' . $packageId;

				return redirect()->away($redirectUrl);
			}
		}

		return false;
	}

	public function addPackageTag($packageId)
	{
		$parentTagId = request('selectParentTag');
		$childTagId = request('selectChildTag');
		$tagId = $parentTagId;

		if ($childTagId != -1)
		{
			$tagId = $childTagId;
		}

		if ($tagId != -1)
		{
			$existingTag = \PackageTag::where(['planner_package_id' => $packageId, 'tile_tag_id' => $tagId])->count();
			if ($existingTag == 0)
			{
				\PackageTag::create(['planner_package_id' => $packageId, 'tile_tag_id' => $tagId]);
			}
		}

		return redirect()->route('package.view', $packageId);
	}

	public function deletePackageTag($packageId, $tagId)
	{
		$decorTag = \PackageTag::where('planner_package_id', $packageId)
		                       ->where('tile_tag_id', $tagId)
		                       ->first();
		$decorTag->delete();

		return redirect()->route('package.view', $packageId);
	}

	public function setAsProfile($packageId, $galleryId)
	{
		$packageGallery = \PackageGallery::find($galleryId);
		if (!$packageGallery)
		{
			return redirect()->route('package.view', $packageId)->with('imgError', 'Image not found in gallery, Try again after some time.');
		}
		$packageGallery->is_profile = 1;
		$packageGallery->save(); // test comment.

		return redirect()->route('package.view', $packageId);
	}

	public function unsetAsProfile($packageId, $galleryId)
	{
		$packageGallery = \PackageGallery::find($galleryId);
		if (!$packageGallery)
		{
			return redirect()->route('package.view', $packageId)->with('imageError', 'Image not found in gallery, Try again after some time.');
		}
		$packageGallery->is_profile = 0;
		$packageGallery->save();

		return redirect()->route('package.view', $packageId);
	}

	public function deletePackageGallery($packageId, $galleryId)
	{
		$gallery = \PackageGallery::find($galleryId);

		if ($gallery && $gallery->delete())
		{
			return redirect()->route('package.view', $packageId)->with('successMsg', 'Image deleted successfully.');
		}
		else
		{
			return redirect()->route('package.view', $packageId)->with('imageError', 'Some error occured while deleting the image, please try again after some time.');
		}
	}

	public function saveImgTitle($packageId, $galleryId)
	{
		$packageGallery = \PackageGallery::find($galleryId);
		$galleryTitle = request('title');

		if ($galleryTitle)
		{
			$packageGallery->title = $galleryTitle;
			$packageGallery->save();

			return redirect()->route('package.view', $packageId)->with('successMsg', 'Image title changed successfully.');
		}
		else
		{
			return redirect()->route('package.view', $packageId)->with('imageError', 'Some error occurred while updating the image, please try again after some time.');
		}
	}

	public function uploadPackageGallery($packageId)
	{
		$type = request('gal-type');
		$redirectUrl = route('package.view', $packageId);

		if ($type == config('evibe.gallery.type.image'))
		{
			$images = Input::file('packageImages');
			$ignoreWatermark = is_null(request('ignoreWatermark')) ? false : true;

			if (!count($images) || (count($images) && is_null($images[0])))
			{
				return redirect($redirectUrl)
					->with('imageError', 'Please select at least one image.')
					->withInput();
			}

			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:5120'
				];

				$imageMessages = [
					'file.required' => 'Please upload atleast one image or video',
					'file.mimes'    => 'One of the images/video is not valid. (only .png, .jpeg, .jpg, .mp4, .x-flv, .x-mpegURL, .MP2T, .3gpp, .quicktime, .x-msvideo, .x-ms-wmv are accepted)',
					'file.max'      => 'Image size cannot br more than 5 MB'
				];

				$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

				if ($imageValidator->fails())
				{
					return redirect($redirectUrl)
						->with('imageError', $imageValidator->messages()->first())
						->withInput();
				}
			}

			$package = \VendorPackage::find($packageId);

			if ($package->map_type_id == config('evibe.ticket_type.venues'))
			{
				$packageUploadPath = '/venues/' . $package->map_id . '/images/packages/' . $package->id . '/';
			}
			else
			{
				$packageUploadPath = '/planner/' . $package->map_id . '/images/packages/' . $package->id . '/';
			}

			foreach ($images as $image)
			{
				$option = ['isWatermark' => false];

				if (!$ignoreWatermark)
				{
					$option = ['isWatermark' => true];
				}
				$imageName = $this->uploadImageToServer($image, $packageUploadPath, $option);

				$imgInsertData = [
					'url'                => $imageName,
					'title'              => $this->getImageTitle($image),
					'planner_package_id' => $packageId,
					'type'               => config('evibe.gallery.type.image')
				];

				\PackageGallery::create($imgInsertData);
			}

			return redirect($redirectUrl)->with('successMsg', "Images uploaded successfully.");
		}
		elseif ($type == config('evibe.gallery.type.video'))
		{
			$video = request('video');

			$rules = [
				'video' => 'required'
			];
			$message = [
				'video.required' => 'Video url is required (Ex: https://www.youtube.com/watch?v=videoId).',
			];

			$validator = validator(['video' => $video], $rules, $message);
			if ($validator->fails())
			{
				return redirect($redirectUrl)
					->with('imageError', $validator->messages()->first())
					->withInput();
			}

			$videoId = $this->getYouTubeVideoId($video);
			if (!$videoId)
			{
				return redirect()->back()->with('imageError', 'Please enter an valid youtube url')->withInput();
			}

			\PackageGallery::create([
				                        'planner_package_id' => $packageId,
				                        'type'               => $type,
				                        'url'                => $videoId
			                        ]);

			return redirect($redirectUrl)->with(['successMsg' => "Video uploaded successfully.", 'type' => 'video']);
		}
	}

	public function editPackageProvider($packageId)
	{
		$data = $this->getPackageProviders($packageId);
		if (!is_array($data) || !isset($data['provider']))
		{
			return response()->json(['success' => false]);
		}

		return response()->json([
			                        'success'   => true,
			                        'providers' => $data['provider'],
			                        'count'     => count($data['provider'])
		                        ]);
	}

	public function saveEditPackageProvider($packageId)
	{
		$package = \VendorPackage::find($packageId);
		if (!$package)
		{
			return redirect()->route('package.view', $packageId)->with('imageError', 'Error occured while updating, try again after some time');
		}

		$provider = $this->getPackageProviders($packageId);
		$imagePath = config('evibe.gallery.root') . '/' . $provider['type'] . '/' . $package->map_id . '/images/packages/' . $package->id . '/';

		$package->map_id = request('packageProviderId');

		if ($package->save())
		{
			$this->updateApprovalOnEditItem($package);

			$images = \PackageGallery::where('planner_package_id', $packageId)->select("url")->get();
			foreach ($images as $image)
			{
				$newImagePath = config('evibe.gallery.root') . '/' . $provider['type'] . '/' . $package->map_id . '/images/packages/' . $package->id . '/';
				$this->changeImagePath($imagePath, $newImagePath, $image->url);
			}

			$this->deleteImagesAfterProviderChange($imagePath);

			return redirect()->route('package.view', $packageId)->with('successMsg', 'provider changed successfully');
		}

		return redirect()->route('package.view', $packageId)->with('imageError', 'Error occured while updating, try again after some time');
	}

	public function addSurprisePackageTag()
	{
		$typeTags = \Tags::all();
		$packageId = request('packageId');
		$relationTags = request('selectRelationShipTag');
		$occasionTags = request('selectOccasionTag');
		$ageGroupTags = request('selectAgeGroupTag');

		$allTags = array_merge($relationTags, $occasionTags);

		if (count($ageGroupTags) > 0)
		{
			$allTags = array_merge($allTags, $ageGroupTags);
		}

		// validate mappings before saving
		$negativeTags = SurpriseTagMappings::where('is_negative', 1)->get()->toArray();
		foreach ($negativeTags as $nTag)
		{
			if (in_array($nTag['sr_id'], $relationTags) && in_array($nTag['so_id'], $occasionTags))
			{
				$relation = $typeTags->where('id', $nTag['sr_id'])->first();
				$occasion = $typeTags->where('id', $nTag['so_id'])->first();
				$relationName = $relation ? $relation->name : "";
				$occasionName = $occasion ? $occasion->name : "";

				return redirect()->back()->with('imageError', 'Invalid relation ship added (' . $relationName . ' - ' . $occasionName . ')');
			}
		}

		foreach ($allTags as $tag)
		{
			\PackageTag::updateOrCreate([
				                            'tile_tag_id'        => $tag,
				                            'planner_package_id' => $packageId
			                            ], ['updated_at' => Carbon::now()]);
		}

		return redirect()->back();
	}
}
