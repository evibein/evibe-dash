<?php

namespace App\Http\Controllers\VenueHall;

use App\Http\Controllers\Base\BaseController;
use App\Models\TypeEvent;
use App\Models\TypeVenueGallery;
use App\Models\VenueHallEvent;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class VenueHallController extends BaseController
{
	public function showList()
	{
		$prefilledLocs = [];
		$hallTypeSelected = [];
		$venueTypeSelected = [];
		$locationSuggestions = [];
		$typeVenueHall = \TypeVenueHall::all();
		$venueType = \TypeVenue::all();
		$locationsList = \Area::pluck('name');
		$priceMinSelected = null;
		$priceMaxSelected = null;
		$capMinSelected = null;
		$capMaxSelected = null;

		$venueHalls = \VenueHall::join('venue', 'venue_hall.venue_id', '=', 'venue.id')
		                        ->with('venue', 'type', 'venue.area')
		                        ->where('venue.is_live', 1)
		                        ->whereNull('venue.deleted_at')
		                        ->select('venue_hall.id', 'venue_hall.name', 'venue_hall.code', 'venue_hall.url',
		                                 'venue_hall.floor', 'venue_hall.cap_min', 'venue_hall.cap_max',
		                                 'venue_hall.cap_float', 'venue_hall.venue_id', 'venue_hall.type_id',
		                                 'venue_hall.created_at', 'venue_hall.updated_at', 'venue_hall.deleted_at')
		                        ->orderBy('created_at', 'DESC');

		// search
		if (Input::has('query') && $searchQuery = Input::get('query'))
		{
			$percentQuery = '%' . $searchQuery . '%';
			$venueHalls = $venueHalls->where(function ($query) use ($percentQuery) {
				$query->where('venue_hall.name', 'LIKE', $percentQuery)
				      ->orwhere('venue_hall.code', 'LIKE', $percentQuery)
				      ->orwhere('venue.name', 'LIKE', $percentQuery);
			});

		}

		// city filter
		$city = Input::has('city') ? Input::get('city') : \City::first()->id;
		if ($city)
		{
			$venueHalls->where('venue.city_id', $city);
		}

		// location filter
		if (Input::has('location') && Input::get('location'))
		{
			$selectedAreaIds = [];
			$locationList = explode(",", urldecode(Input::get('location')));
			$venueIds = null;

			foreach ($locationList as $key => $value)
			{
				$selectedArea = \Area::where('name', $value)->first();
				$areaId = $selectedArea->id;
				array_push($prefilledLocs, $value);
				array_push($selectedAreaIds, $areaId);

				if ($key == 0)
				{
					$venueIds = \Venue::select('id')->where('area_id', $areaId);
				}
				else
				{
					$venueIds->orWhere('area_id', '=', $areaId);
				}

				// location suggestions
				foreach ($selectedArea->getNearbyAreas() as $nearbyAreaId)
				{
					array_push($locationSuggestions, $nearbyAreaId);
				}
			}

			$venueIds = $venueIds->get()->toArray();
			$venueHalls->whereIn('venue_id', $venueIds);
			$locationSuggestions = \Area::whereIn('id', array_unique($locationSuggestions))
			                            ->whereNotIn('id', $selectedAreaIds)->pluck('name');
		}

		// hall type filter
		if (Input::has('hallType') && Input::get('hallType'))
		{
			$hallTypeSelected = explode(",", Input::get('hallType'));
			$venueHalls->whereIn('venue_hall.type_id', $hallTypeSelected);
		}

		//venue type filter
		if (Input::has('venueType') && Input::get('venueType'))
		{
			$venueTypeSelected = explode(",", Input::get('venueType'));
			$venueHalls->whereIn('venue.type_id', $venueTypeSelected);
		}

		// capacity filter
		if (Input::has('capacityRange') && Input::get('capacityRange'))
		{
			$capacityRanges = Input::get('capacityRange');
			$capacityRanges = explode(",", $capacityRanges);
			$capMinSelected = trim($capacityRanges[0]) ? trim($capacityRanges[0]) : null;
			$capMaxSelected = trim($capacityRanges[1]) ? trim($capacityRanges[1]) : null;

			// min is set
			if (!is_null($capMinSelected))
			{
				$venueHalls->where('venue_hall.cap_min', '<=', $capMinSelected);
			}

			// max is set, consider `cap_float` for maximums
			if (!is_null($capMaxSelected))
			{
				$venueHalls->where('venue_hall.cap_float', '>=', $capMaxSelected);
			}
		}

		// price filter
		if (Input::has('priceRange') && Input::get('priceRange'))
		{
			$ids = [];
			$priceRange = Input::get('priceRange');
			$priceRangeList = explode(",", $priceRange);
			$priceMinSelected = trim($priceRangeList[0]) ? trim($priceRangeList[0]) : null;
			$priceMaxSelected = trim($priceRangeList[1]) ? trim($priceRangeList[1]) : null;

			// gives values are numbers
			$venueIdsMatchingPrice = \Venue::select('id')->distinct();

			if (!is_null($priceMinSelected) && !is_null($priceMaxSelected))
			{

				$ids = $venueIdsMatchingPrice->where('price_min_veg', '>=', $priceMinSelected)
				                             ->where('price_min_veg', '<=', $priceMaxSelected)
				                             ->get()->toArray();

			}
			elseif (is_null($priceMinSelected) && !is_null($priceMaxSelected))
			{

				$ids = $venueIdsMatchingPrice->where('price_min_veg', '<=', $priceMaxSelected)
				                             ->get()->toArray();

			}
			elseif (!is_null($priceMinSelected) && is_null($priceMaxSelected))
			{

				$ids = $venueIdsMatchingPrice->where('price_min_veg', '>=', $priceMinSelected)
				                             ->get()->toArray();
			}

			if (count($ids))
			{
				$venueHalls->whereIn('venue_id', $ids);
			}
		}

		$venueLocation = [];
		$tagList = [];
		$data = [
			'venueType'           => $venueType,
			'typeVenueHall'       => $typeVenueHall,
			'venueHall'           => $venueHalls->paginate(20),
			'prefilledLocs'       => $prefilledLocs,
			'hallTypeSelected'    => $hallTypeSelected,
			'venueTypeSelected'   => $venueTypeSelected,
			'capMinSelected'      => $capMinSelected,
			'capMaxSelected'      => $capMaxSelected,
			'priceMinSelected'    => $priceMinSelected,
			'priceMaxSelected'    => $priceMaxSelected,
			'locationsList'       => $locationsList,
			'locationSuggestions' => $locationSuggestions,
			'venueLocation'       => $venueLocation,
			'tagList'             => $tagList,
			'cities'              => \City::all(),
			'venues'              => \Venue::all()
		];

		return view('venue-halls.list', ['data' => $data]);
	}

	public function showDetails($hallId)
	{
		$venueHall = \VenueHall::find($hallId);

		if (!$venueHall)
		{
			abort(404);
		}

		$venueHallGallery = \VenueHallGallery::where('venue_hall_id', $hallId)->get();

		$eventSupported = VenueHallEvent::where('venue_hall_id', $hallId)->get();

		$galleryTypes = TypeVenueGallery::all();

		$data = [
			'venueHall'        => $venueHall,
			'venueHallGallery' => $venueHallGallery,
			'eventSupported'   => $eventSupported,
			'galleryTypes'     => $galleryTypes
		];

		return view('venue-halls.profile', ['data' => $data]);
	}

	public function deleteVenueHall($id)
	{
		$venueHall = \VenueHall::find($id);

		// delete gallery
		\VenueHallGallery::where('venue_hall_id', $id)->delete();

		// delete hall
		\VenueHall::findOrFail($id)->delete();

		return redirect()->route('venue-halls.list')->with('succes', true);
	}

	public function showAddNewHall($venueId)
	{
		$venue = \Venue::find($venueId);
		$hallTypes = \TypeVenueHall::all();
		$typeEvents = TypeEvent::orderBy('id')->get();

		$data = [
			'venue'      => $venue,
			'hallTypes'  => $hallTypes,
			'typeEvents' => $typeEvents
		];

		return view('venue-halls.new')->with('data', $data);
	}

	public function saveNewHall($venueId)
	{
		$venue = \Venue::find($venueId);
		$venueId = $venue->id;
		$eventSupported = Input::get('events');
		$venueMinRent = $venue->price_min_rent ? $venue->price_min_rent : 500;
		$venueMinVeg = $venue->price_min_veg ? $venue->price_min_veg : 0;
		$venueMinNonVeg = $venue->price_min_nonveg ? $venue->price_min_nonveg : 0;

		// @todo: check for correct capacity rules
		$rules = [
			'type'            => 'required|integer|min:1',
			'name'            => 'min:3',
			'floor'           => 'required|integer|min:0',
			'events'          => 'required',
			'tax'             => 'numeric',
			'rentMin'         => 'integer|required_with:rentMax,rentWorth|min:' . $venueMinRent,
			'rentMax'         => 'integer|min_as:rentMin',
			'rentWorth'       => 'integer|min_as:rentMax',
			'rentMinDuration' => 'numeric:required_with:rent_min,rent_max|min:' . $venue->min_rent_duration,
			'capMin'          => 'required|integer|min:' . $venue->cap_min . '|max:' . ($venue->cap_max - 1),
			'capMax'          => 'required|integer|min:' . ($venue->cap_min + 1) . '|max:' . $venue->cap_max,
			'capFloat'        => 'required|integer|min:' . ($venue->min + 1) . '|max:' . $venue->cap_float,
			'foodMinVeg'      => 'integer|required_with:foodMaxVeg,foodWorthVeg|min:' . $venueMinVeg,
			'foodMaxVeg'      => 'integer|min_as:foodMinVeg',
			'foodWorthVeg'    => 'integer|min_as:foodMaxVeg',
			'foodMinNonVeg'   => 'integer|required_with:foodMaxNonVeg,foodWorthNonVeg|min:' . $venueMinNonVeg,
			'foodMaxNonVeg'   => 'integer|min_as:foodMinNonVeg',
			'foodWorthNonVeg' => 'integer|min_as:foodMaxNonVeg',
		];

		$messages = [
			'type.required'                 => 'Select a hall type',
			'type.integer'                  => 'Selected a valid hall type',
			'type.min'                      => 'Selected a valid hall type',
			'name.min'                      => 'Hall name should be minimum 3 characters',
			'floor.required'                => 'Floor number of the hall is required (enter 0 for ground floor)',
			'floor.integer'                 => 'Floor number of the hall is invalid (enter 0 for ground floor)',
			'floor.min'                     => 'Floor number of the hall is invalid (enter 0 for ground floor)',
			'capMin.required'               => 'Minimum capacity is required',
			'capMin.integer'                => 'Minimum capacity is invalid (range: ' . $venue->cap_min . ' - ' . ($venue->cap_max - 1) . ')',
			'capMin.min'                    => 'Minimum capacity is invalid (range: ' . $venue->cap_min . ' - ' . ($venue->cap_max - 1) . ')',
			'capMin.max'                    => 'Minimum capacity is invalid (range: ' . $venue->cap_min . ' - ' . ($venue->cap_max - 1) . ')',
			'capMax.required'               => 'Maximum capacity is required',
			'capMax.integer'                => 'Maximum capacity is invalid (range: ' . ($venue->cap_min + 1) . ' - ' . $venue->cap_max . ')',
			'capMax.min'                    => 'Maximum capacity is invalid (range: ' . ($venue->cap_min + 1) . ' - ' . $venue->cap_max . ')',
			'capMax.max'                    => 'Maximum capacity is invalid (range: ' . ($venue->cap_min + 1) . ' - ' . $venue->cap_max . ')',
			'capFloat.required'             => 'Floating capacity is required',
			'capFloat.integer'              => 'Floating capacity is invalid (range: ' . ($venue->min + 1) . ' - ' . $venue->cap_float . ')',
			'capFloat.min'                  => 'Floating capacity is invalid (range: ' . ($venue->min + 1) . ' - ' . $venue->cap_float . ')',
			'capFloat.max'                  => 'Floating capacity is invalid (range: ' . ($venue->min + 1) . ' - ' . $venue->cap_float . ')',
			'events.required'               => 'Event supported field is required',
			'rentMin.integer'               => 'Minimum rent is invalid',
			'rentMin.min'                   => 'Minimum rent cannot be less than ' . $venueMinRent,
			'rentMax.integer'               => 'Maximum rent is invalid',
			'rentMax.min_as'                => 'Maximum rent cannot be less than minimum rent',
			'rentWorth.integer'             => 'Worth rent is invalid',
			'rentWorth.min_as'              => 'Worth rent cannot be less than maximum rent',
			'rentMinDuration.numeric'       => 'Minimum rent duration is invalid (Ex: 2)',
			'rentMinDuration.required_with' => 'Minimum rent duration is required with Min, max Rent',
			'rentMin.required_with'         => 'Minimum rent is required with maximum rent / worth rent',
			'tax.numeric'                   => 'Tax % is invalid (Ex 4.38)',
			'foodMinVeg.integer'            => 'Min veg price is invalid (Ex: 100)',
			'foodMinVeg.required_with'      => 'Min veg price is mandatory with Max veg price/Worth veg price',
			'foodMinVeg.min'                => 'Minimum veg price cannot be less than ' . $venueMinVeg,
			'foodMaxVeg.integer'            => 'Max veg price is invalid (Ex: 200)',
			'foodMaxVeg.min_as'             => 'Max veg price should be more than Min veg price',
			'foodWorthVeg.integer'          => 'Max veg price is invalid (Ex: 300)',
			'foodWorthVeg.min_as'           => 'Worth veg price should be more than Max veg price',
			'foodMinNonVeg.integer'         => 'Min non-veg price is invalid (Ex: 300)',
			'foodMinNonVeg.required_with'   => 'Min non-veg price is required with Max non-veg price / Worth non-veg price',
			'foodMinNonVeg.min'             => 'Minimum non-veg price cannot be less than ' . $venueMinNonVeg,
			'foodMaxNonVeg.integer'         => 'Max non-veg price is invalid (Ex: 400)',
			'foodMaxNonVeg.min_as'          => 'Max non-veg price should be more than Minimum non-veg price',
			'foodMaxWorthVeg.integer'       => 'Worth non-veg price is invalid (Ex: 500)',
			'foodMaxWorthVeg.min_as'        => 'Worth-veg price should be more than Maximum non-veg price'
		];

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$allInput = Input::all();
			$eventSupported ? $oldEvents = implode(',', $eventSupported) : $oldEvents = '';
			$allInput['events'] = $oldEvents;

			return redirect()->route('venue-halls.new.view', $venueId)
			                 ->withErrors($validation)
			                 ->withInput($allInput);
		}

		$typeId = Input::get('type');
		$capMax = Input::get('capMax');
		$capMin = Input::get('capMin');
		$capFloat = Input::get('capFloat');
		$urlData = [
			'type_id'   => $typeId,
			'venue_id'  => $venueId,
			'cap_min'   => $capMin,
			'cap_float' => $capFloat,
		];

		if (!$capFloat || $capFloat < $capMax)
		{
			$capFloat = $capMax;
		}

		// save data
		$hall = \VenueHall::create([
			                           'venue_id'           => $venueId,
			                           'type_id'            => $typeId,
			                           'name'               => Input::get('name'),
			                           'floor'              => Input::get('floor'),
			                           'has_ac'             => Input::get('hasAc'),
			                           'cap_min'            => $capMin,
			                           'cap_max'            => $capMax,
			                           'cap_float'          => $capFloat,
			                           'rent_min'           => Input::get('rentMin'),
			                           'rent_max'           => Input::get('rentMax'),
			                           'rent_worth'         => Input::get('rentWorth'),
			                           'min_rent_duration'  => Input::get('rentMinDuration'),
			                           'price_min_veg'      => Input::get('foodMinVeg'),
			                           'price_max_veg'      => Input::get('foodMaxVeg'),
			                           'price_worth_veg'    => Input::get('foodWorthVeg'),
			                           'price_min_nonveg'   => Input::get('foodMinNonVeg'),
			                           'price_max_nonveg'   => Input::get('foodMaxNonVeg'),
			                           'price_worth_nonveg' => Input::get('foodWorthNonVeg'),
			                           'tax'                => Input::get('tax'),
			                           'code'               => $this->generateHallCode($typeId),
			                           'url'                => $this->generateHallUrl($urlData),
		                           ]);

		// Event supported
		foreach ($eventSupported as $event)
		{
			$supportedEvent = new VenueHallEvent;
			$supportedEvent->venue_hall_id = $hall->id;
			$supportedEvent->event_id = $event;
			$supportedEvent->save();
		}

		return redirect()->route('venue-halls.list')->with('success', true);
	}

	public function showVenueHallEdit($hallId)
	{
		$hall = \VenueHall::findOrFail($hallId);
		$venue = \Venue::find($hall->venue_id);
		$hallTypes = \TypeVenueHall::all();
		$typeEvents = TypeEvent::orderBy('id')->get();
		$hallEvents = VenueHallEvent::where('venue_hall_id', $hallId)->pluck('event_id');

		$data = [
			'venue'      => $venue,
			'hall'       => $hall,
			'hallTypes'  => $hallTypes,
			'typeEvents' => $typeEvents,
			'hallEvents' => $hallEvents
		];

		return view('venue-halls.edit')->with('data', $data);
	}

	public function saveVenueHallEdit($hallId)
	{
		$hall = \VenueHall::find($hallId);
		$venue = \Venue::find($hall->venue_id);

		$venueId = $venue->id;
		$eventSupported = Input::get('events');
		$venueMinRent = $venue->price_min_rent ? $venue->price_min_rent : 500;
		$venueMinVeg = $venue->price_min_veg ? $venue->price_min_veg : 0;
		$venueMinNonVeg = $venue->price_min_nonveg ? $venue->price_min_nonveg : 0;

		// @todo: check for correct capacity rules
		$rules = [
			'type'            => 'required|integer|min:1',
			'name'            => 'min:3',
			'floor'           => 'required|integer|min:0',
			'events'          => 'required',
			'rentMin'         => 'integer|required_with:rentMax,rentWorth|min:' . $venueMinRent,
			'rentMax'         => 'integer|min_as:rentMin',
			'rentWorth'       => 'integer|min_as:rentMax',
			'tax'             => 'numeric',
			'rentMinDuration' => 'numeric:required_with:rent_min,rent_max|min:' . $venue->min_rent_duration,
			'capMin'          => 'required|integer|min:' . $venue->cap_min . '|max:' . ($venue->cap_max - 1),
			'capMax'          => 'required|integer|min:' . ($venue->cap_min + 1) . '|max:' . $venue->cap_max,
			'capFloat'        => 'required|integer|min:' . ($venue->min + 1) . '|max:' . $venue->cap_float,
			'foodMinVeg'      => 'integer|required_with:foodMaxVeg,foodWorthVeg|min:' . $venueMinVeg,
			'foodMaxVeg'      => 'integer|min_as:foodMinVeg',
			'foodWorthVeg'    => 'integer|min_as:foodMaxVeg',
			'foodMinNonVeg'   => 'integer|required_with:foodMaxNonVeg,foodWorthNonVeg|min:' . $venueMinNonVeg,
			'foodMaxNonVeg'   => 'integer|min_as:foodMinNonVeg',
			'foodWorthNonVeg' => 'integer|min_as:foodMaxNonVeg',
		];

		$messages = [
			'type.required'                 => 'Select a hall type',
			'type.integer'                  => 'Selected a valid hall type',
			'type.min'                      => 'Selected a valid hall type',
			'name.min'                      => 'Hall name should be minimum 3 characters',
			'floor.required'                => 'Floor number of the hall is required (enter 0 for ground floor)',
			'floor.integer'                 => 'Floor number of the hall is invalid (enter 0 for ground floor)',
			'floor.min'                     => 'Floor number of the hall is invalid (enter 0 for ground floor)',
			'capMin.required'               => 'Minimum capacity is required',
			'capMin.integer'                => 'Minimum capacity is invalid (range: ' . $venue->cap_min . ' - ' . ($venue->cap_max - 1) . ')',
			'capMin.min'                    => 'Minimum capacity is invalid (range: ' . $venue->cap_min . ' - ' . ($venue->cap_max - 1) . ')',
			'capMin.max'                    => 'Minimum capacity is invalid (range: ' . $venue->cap_min . ' - ' . ($venue->cap_max - 1) . ')',
			'capMax.required'               => 'Maximum capacity is required',
			'capMax.integer'                => 'Maximum capacity is invalid (range: ' . ($venue->cap_min + 1) . ' - ' . $venue->cap_max . ')',
			'capMax.min'                    => 'Maximum capacity is invalid (range: ' . ($venue->cap_min + 1) . ' - ' . $venue->cap_max . ')',
			'capMax.max'                    => 'Maximum capacity is invalid (range: ' . ($venue->cap_min + 1) . ' - ' . $venue->cap_max . ')',
			'capFloat.required'             => 'Floating capacity is required',
			'capFloat.integer'              => 'Floating capacity is invalid (range: ' . ($venue->min + 1) . ' - ' . $venue->cap_float . ')',
			'capFloat.min'                  => 'Floating capacity is invalid (range: ' . ($venue->min + 1) . ' - ' . $venue->cap_float . ')',
			'capFloat.max'                  => 'Floating capacity is invalid (range: ' . ($venue->min + 1) . ' - ' . $venue->cap_float . ')',
			'events.required'               => 'Event supported field is required',
			'rentMin.integer'               => 'Minimum rent is invalid',
			'rentMin.min'                   => 'Minimum rent cannot be less than ' . $venueMinRent,
			'rentMax.integer'               => 'Maximum rent is invalid',
			'rentMax.min_as'                => 'Maximum rent cannot be less than minimum rent',
			'rentWorth.integer'             => 'Worth rent is invalid',
			'rentWorth.min_as'              => 'Worth rent cannot be less than maximum rent',
			'rentMinDuration.numeric'       => 'Minimum rent duration is invalid',
			'rentMinDuration.required_with' => 'Minimum rent duration is required with min, max Rent',
			'rentMin.required_with'         => 'minimum rent is required with max rent / worth rent',
			'tax.numeric'                   => 'Tax % is invalid (Ex 4.38)',
			'foodMinVeg.integer'            => 'Min veg price is invalid (Ex: 100)',
			'foodMinVeg.required_with'      => 'Min veg price is mandatory with Max veg price/Worth veg price',
			'foodMinVeg.min'                => 'Minimum veg price cannot be less than ' . $venueMinVeg,
			'foodMaxVeg.integer'            => 'Max veg price is invalid (Ex: 200)',
			'foodMaxVeg.min_as'             => 'Max veg price should be more than Min veg price',
			'foodWorthVeg.integer'          => 'Max veg price is invalid (Ex: 300)',
			'foodWorthVeg.min_as'           => 'Worth veg price should be more than Max veg price',
			'foodMinNonVeg.integer'         => 'Min non-veg price is invalid (Ex: 300)',
			'foodMinNonVeg.required_with'   => 'Min non-veg price is required with Max non-veg price / Worth non-veg price',
			'foodMinNonVeg.min'             => 'Minimum non-veg price cannot be less than ' . $venueMinNonVeg,
			'foodMaxNonVeg.integer'         => 'Max non-veg price is invalid (Ex: 400)',
			'foodMaxNonVeg.min_as'          => 'Max non-veg price should be more than Minimum non-veg price',
			'foodMaxWorthVeg.integer'       => 'Worth non-veg price is invalid (Ex: 500)',
			'foodMaxWorthVeg.min_as'        => 'Worth-veg price should be more than Maximum non-veg price'
		];

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$allInput = Input::all();
			$eventSupported ? $oldEvents = implode(',', $eventSupported) : $oldEvents = '';
			$allInput['events'] = $oldEvents;

			return redirect()->route('venue.hall.edit.show', $hallId)
			                 ->withErrors($validation)
			                 ->withInput($allInput);
		}

		$typeId = Input::get('type');
		$capMax = Input::get('capMax');
		$capMin = Input::get('capMin');
		$capFloat = Input::get('capFloat');

		if (!$capFloat || $capFloat < $capMax)
		{
			$capFloat = $capMax;
		}

		// save data
		$hall->venue_id = $venueId;
		$hall->name = Input::get('name');
		$hall->floor = Input::get('floor');
		$hall->has_ac = Input::get('hasAc');
		$hall->cap_min = $capMin;
		$hall->cap_max = $capMax;
		$hall->cap_float = $capFloat;
		$hall->rent_min = Input::get('rentMin');
		$hall->rent_max = Input::get('rentMax');
		$hall->rent_worth = Input::get('rentWorth');
		$hall->min_rent_duration = Input::get('rentMinDuration');
		$hall->price_min_veg = Input::get('foodMinVeg');
		$hall->price_max_veg = Input::get('foodMaxVeg');
		$hall->price_worth_veg = Input::get('foodWorthVeg');
		$hall->price_min_nonveg = Input::get('foodMinNonVeg');
		$hall->price_max_nonveg = Input::get('foodMaxNonVeg');
		$hall->price_worth_nonveg = Input::get('foodWorthNonVeg');
		$hall->tax = Input::get('tax');

		// update hall code if any change in type
		if ($hall->type_id != $typeId)
		{
			$urlData = [
				'type_id'   => $typeId,
				'venue_id'  => $venueId,
				'cap_min'   => $capMin,
				'cap_float' => $capFloat,
			];

			$hall->type_id = $typeId;
			$hall->code = self::generateHallCode($typeId);
			$hall->url = self::generateHallUrl($urlData);
		}

		$hall->save();

		VenueHallEvent::where('venue_hall_id', $hallId)->forceDelete();

		foreach ($eventSupported as $eventId)
		{
			$supportedEvent = new VenueHallEvent();
			$supportedEvent->venue_hall_id = $hall->id;
			$supportedEvent->event_id = $eventId;
			$supportedEvent->save();
		}

		return redirect()->route('venue-halls.profile.view', $hallId)->with('success', true);
	}

	public function uploadImages($hallId)
	{
		$hall = \VenueHall::findOrFail($hallId);
		$venueId = $hall->venue_id;
		$files = Input::file('images');

		// create directory if not exists
		$hallPath = '/venues/' . $venueId . '/halls/' . $hallId . '/images/';

		foreach ($files as $file)
		{
			$rules = [
				'file' => 'required|mimes:png,jpeg,jpg,JPG',
			];

			$messages = [
				'file.required' => 'Please upload at least one image',
				'file.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
			];

			$validator = Validator::make(['file' => $file], $rules, $messages);

			if ($validator->passes())
			{
				$fileName = $this->uploadImageToServer($file, $hallPath);
				$typeId = Input::get('type');
				$type = TypeVenueGallery::findOrFail($typeId);

				$insertData = [
					'venue_hall_id' => $hallId,
					'type_id'       => $typeId,
					'title'         => $hall->code . ' - ' . $type->name,
					'url'           => $fileName,
				];

				$vg = \VenueHallGallery::create($insertData);

				if (!$vg)
				{
					return redirect()->route('venue-halls.profile.view', $hallId)
					                 ->withErrors($validator)
					                 ->withInput();
				}
			}
			else
			{
				return redirect()->route('venue-halls.profile.view', $hallId)->withErrors($validator)->withInput();
			}
		}

		return redirect()->route('venue-halls.profile.view', $hallId)->with('success', true);
	}

	public function deleteImage($hallId, $galleryId)
	{
		\VenueHallGallery::findOrFail($galleryId)->delete();

		return redirect()->route('venue-halls.profile.view', $hallId)
		                 ->with('success', true);
	}

	public function saveImageEdit($hallId, $galleryId)
	{
		$rules = [
			'editTitle' => 'required|min:3',
			'editType'  => 'required|integer|min:1',
		];

		$messages = [
			'editTitle.min'     => 'Title should be minimum 3 characters',
			'editType.requried' => 'Select correct image category',
			'editType.integer'  => 'Image category is invalid',
			'editType.min'      => 'Image category is invalid',
		];

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			return redirect()->route('venue-halls.profile.view', $hallId)
			                 ->withErrors($validation)
			                 ->withInput();
		}

		$vhg = \VenueHallGallery::findOrFail($galleryId);
		$typeId = Input::get('editType');
		$title = Input::get('editTitle');

		if (!$title)
		{
			$hall = \VenueHall::findOrFail($hallId);
			$typeObj = TypeVenueGallery::findOrFail($typeId);
			$title = $hall->code . ' - ' . $typeObj->name;
		}

		$vhg->type_id = $typeId;
		$vhg->title = $title;
		$vhg->save();

		return redirect()->route('venue-halls.profile.view', $hallId)
		                 ->with('success', true);
	}

	public function setAsProfilePic($hallId, $galleryId)
	{
		$galleryItem = \VenueHallGallery::find($galleryId);
		if ($galleryItem)
		{
			$galleryItem->update(['is_profile' => 1]);
		}

		Return redirect()->back();
	}

	public function unsetAsProfilePic($hallId, $galleryId)
	{
		$galleryItem = \VenueHallGallery::find($galleryId);
		if ($galleryItem)
		{
			$galleryItem->update(['is_profile' => 0]);
		}

		Return redirect()->back();
	}

	private function generateHallCode($typeId)
	{
		$hallPrefix = config('evibe.code.prefix.hall');
		$typeObj = \TypeVenueHall::findOrFail($typeId);
		$lastObj = \VenueHall::withTrashed()
		                     ->where('type_id', $typeId)
		                     ->orderBy('code', 'DESC')
		                     ->first();
		if (!$lastObj)
		{
			$lastCode = $typeObj->default_code;
		}
		else
		{
			$lastCode = $lastObj->code;
		}

		$digits = ((int)substr($lastCode, -4)) + 1;
		$digits = str_pad($digits, 4, '0', STR_PAD_LEFT);

		return $hallPrefix . $typeObj->code . $digits;
	}

	private function generateHallUrl($values)
	{
		$typeId = $values['type_id'];
		$venueId = $values['venue_id'];
		$capMin = $values['cap_min'];
		$capFloat = $values['cap_float'];

		$venue = \Venue::find($venueId);
		$location = strtolower($venue->area->name);
		$location = str_replace(' ', '-', $location);

		$typeVenue = \TypeVenueHall::find($typeId);
		$venueTypeName = strtolower($typeVenue->name);
		$venueTypeName = str_replace(' ', '-', $venueTypeName);

		$urlTpl = config('evibe.urls.hall');
		$replaces = [
			'#field1#' => time(),
			'#field2#' => $venueTypeName,
			'#field3#' => $capMin,
			'#field4#' => $capFloat,
			'#field5#' => $location,
		];

		$url = str_replace(array_keys($replaces), array_values($replaces), $urlTpl);

		return $url;
	}
}