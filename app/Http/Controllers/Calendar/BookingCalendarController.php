<?php

namespace App\Http\Controllers\Calendar;

use App\Http\Controllers\Base\BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;

class BookingCalendarController extends BaseController
{
	public function showCalendar()
	{
		$planners = $venues = [];
		$bookingUrl = route('calendar.booking.getData');
		$plannerIds = \TicketBooking::whereIn('map_type_id', [config('evibe.ticket_type.planners'), config('evibe.ticket_type.artists')])->distinct()->pluck('map_id');
		$venueIds = \TicketBooking::whereIn('map_type_id', [config('evibe.ticket_type.venues')])->distinct()->pluck('map_id');

		$plannerPartner = \Vendor::whereIn('id', $plannerIds)->select('id', 'name', 'person')->get();

		$venuePartner = \Venue::whereIn('id', $venueIds)
		                      ->select('id', 'name', 'person')
		                      ->get();

		foreach ($plannerPartner as $item)
		{
			$planners[] = [
				'id'          => $item->id,
				'name'        => $item->name,
				'person'      => $item->person,
				'map_type_id' => config('evibe.ticket_type.planners')
			];
		}

		foreach ($venuePartner as $item)
		{
			$venues[] = [
				'id'          => $item->id,
				'name'        => $item->name,
				'person'      => $item->person,
				'map_type_id' => config('evibe.ticket_type.venues')
			];
		}

		$partners = collect($planners)->merge($venues)->sortByDesc('id')->all();

		return view('calendar.booking', compact('bookingUrl', 'partners'));
	}

	public function getBookingData(Request $request)
	{
		$data = [];
		$monthStart = Carbon::now()->startOfMonth()->timestamp;
		$monthEnd = Carbon::now()->endOfMonth()->timestamp;

		if ($request->input('hasRange') == 1)
		{
			$monthStart = str_replace(" GMT+0530 (India Standard Time)", "", $request->input('start'));
			$monthEnd = str_replace(" GMT+0530 (India Standard Time)", "", $request->input('end'));

			$monthStart = strtotime(substr($monthStart, 4));
			$monthEnd = strtotime(substr($monthEnd, 4));
		}

		$dateRange = [$monthStart, $monthEnd];

		$type = $request->has('type') ? $request->input('type') : false;

		if ($type)
		{
			$keys = explode("-", $type);

			if (count($keys) == 2)
			{
				$partner = null;
				if ($keys[0] == 2)
				{
					$partner = \Vendor::find($keys[1]);
				}
				elseif ($keys[0] == 3)
				{
					$partner = \Venue::find($keys[1]);
				}

				if (!is_null($partner))
				{
					// fetch the data for particular partner
					$bookings = \TicketBooking::where('map_id', $keys[1])
					                          ->where('map_type_id', $keys[0])
					                          ->whereBetween('party_date_time', $dateRange)
					                          ->whereNull('cancelled_at')
					                          ->where('is_advance_paid', 1)->get();

					foreach ($bookings as $booking)
					{
						$ticket = $booking->ticket;
						if (!$ticket)
						{
							//Log::info("Booking calendar data error :: Ticket not found for booking id $booking->id");
							continue;
						}

						if ($ticket->status_id != config('evibe.status.booked'))
						{
							//Log::info("Booking calendar data error :: Ticket status is not booked for booking id $booking->id");
							continue;
						}

						$data[] = [
							'title' => $ticket->name . ' Party by ' . $partner->person,
							'start' => date('m/d/Y H:i', $booking->party_date_time)
						];
					}
				}
				else
				{
					$data = $this->fetchAllBookingData($dateRange);
				}
			}
			else
			{
				$data = $this->fetchAllBookingData($dateRange);
			}
		}
		else
		{
			$data = $this->fetchAllBookingData($dateRange);
		}

		$res = [
			'success'  => true,
			'bookings' => $data
		];

		return response()->json($res);
	}

	private function fetchAllBookingData($dateRange)
	{
		$bookings = \TicketBooking::with('ticket')
		                          ->where('is_advance_paid', 1)
		                          ->whereNull('cancelled_at')
		                          ->whereBetween('party_date_time', $dateRange)
		                          ->get();
		$data = [];

		foreach ($bookings as $booking)
		{
			$provider = $booking->provider;
			if (!$provider)
			{
				//Log::info("Booking calendar data error :: Provider not found for booking id $booking->id");
				continue;
			}

			$ticket = $booking->ticket;
			if (!$ticket)
			{
				//Log::info("Booking calendar data error :: Ticket not found for booking id $booking->id");
				continue;
			}

			if ($ticket->status_id != config('evibe.status.booked'))
			{
				//Log::info("Booking calendar data error :: Ticket status is not booked for booking id $booking->id");
				continue;
			}

			$data[] = [
				'title' => $ticket->name . ' Party by ' . $provider->person,
				'start' => date('m/d/Y H:i', $booking->party_date_time)
			];
		}

		return $data;
	}
}
