<?php namespace App\Http\Controllers\Trend;

use App\Models\InternalQuestion;
use App\Models\TrendEvents;
use App\Models\TypeEvent;
use App\Models\TypeFacts;
use App\Models\TypePrerequisite;
use App\Http\Controllers\Base\BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TrendsController extends BaseController
{
	/**
	 * SHOW ALL TRENDING
	 */
	public function getIndex(Request $request)
	{
		$trends = \Trend::orderBy('updated_at', 'DESC')->orderBy('created_at', 'DESC');

		$searchQuery = $request->has('query') ? $request->input('query') : false;

		$cityId = $request->has('city') ? $request->input('city') : \City::first()->id;

		if ($cityId)
		{
			$trends->where('city_id', $cityId);
		}

		if ($searchQuery)
		{
			$likeSearchQuery = '%' . $searchQuery . '%';
			$trends->where(function ($query) use ($searchQuery, $likeSearchQuery)
			{

				$query->where('name', 'LIKE', $likeSearchQuery)
				      ->orWhere('code', 'LIKE', $likeSearchQuery);
				if (is_int($searchQuery))
				{
					$query->orWhere('price', $searchQuery);
				}
			});
		}
		$trends = $trends->paginate(12);

		$data = [
			'trends' => $trends,
			'cities' => \City::all()
		];

		return view('trends/trends', ['data' => $data]);
	}

	/**
	 * SHOW TREND DETAILS
	 */
	public function getView($trendId)
	{
		$trend = \Trend::findOrFail($trendId);
		$Iqs = InternalQuestion::withTrashed()->where(['map_id' => $trendId, 'map_type_id' => config('evibe.ticket_type.trends')])
		                       ->orderBy('deleted_by')
		                       ->orderBy('updated_at', 'DESC')
		                       ->get();

		$galleries = \TrendGallery::orderBy('created_at', 'DESC')->where('trending_id', $trendId)->get();

		$images = $galleries->filter(function ($query)
		{
			return $query->type == config('evibe.gallery.type.image');
		});

		$videos = $galleries->filter(function ($query)
		{
			return $query->type == config('evibe.gallery.type.video');
		});

		$data = [
			'trend'  => $trend,
			'iqs'    => $Iqs,
			'images' => $images,
			'videos' => $videos
		];

		return view('trends/view_trend_details', ['data' => $data]);
	}

	/**
	 * SHOW ADD NEW TREND PAGE
	 */
	public function getNew()
	{
		$city = \City::all();
		$vendors = \Vendor::whereIn('type_id', [1, 2])->get();
		$typeEvents = TypeEvent::all();
		$terms = \BookingTerms::all();
		$facts = TypeFacts::all();
		$preReq = TypePrerequisite::all();

		$data = [
			'city'       => $city,
			'vendors'    => $vendors,
			'typeEvents' => $typeEvents,
			'terms'      => $terms,
			'facts'      => $facts,
			'preReq'     => $preReq
		];

		return view('trends/add_trend', ['data' => $data]);
	}

	/**
	 * POST NEW TREND
	 */
	public function postCreateTrend()
	{
		$events = Input::get('events');
		$rules = [
			'tname'      => 'required|min:4',
			'tprice'     => 'required|integer|min:1',
			'worthPrice' => 'integer|min_as:maxPrice',
			'maxPrice'   => 'integer|min_as:tprice',
			'rangeInfo'  => 'required_with:maxPrice',
			'tinfo'      => 'required|min:4',
			'prereq'     => 'min:10',
			'facts'      => 'min:10',
			'terms'      => 'min:10',
			'events'     => 'required',
		];

		$messages = [
			'tname.required'          => 'Please enter valid Trend name',
			'tname.min'               => 'Trend name should be minimum 4 characters long',
			'tinfo.required'          => 'Please enter valid Trend info',
			'tinfo.min'               => 'Trend info should be minimum 4 characters long',
			'tprice.required'         => 'Please enter valid Trend price',
			'tprice.integer'          => 'Trend price is not valid',
			'tprice.min'              => 'Trend price is not valid',
			'prereq.min'              => 'Prerequisites cannot be less than 10 character',
			'facts.min'               => 'Facts cannot be less than 10 characters',
			'terms.min'               => 'Terms cannot be less than 10 characters',
			'events.required'         => 'Please select at-least one supported events',
			'worthPrice.integer'      => 'Worth price must be number',
			'worthPrice.min_as'       => 'Worth price cannot be less than max price',
			'maxPrice.min_as'         => 'Max price cannot be less than trend price',
			'maxPrice.integer'        => 'Max price must be a number',
			'rangeInfo.required_with' => 'Price range info is mandatory when max price is available'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			$oldEvents = $events ? implode(",", $events) : '';
			$allInput = Input::all();
			$allInput['events'] = $oldEvents;

			return redirect()->back()
			                 ->withErrors($validator)
			                 ->withInput($allInput);
		}

		// generate url
		$trendUrl = $this->generateUrl(Input::get('tname'));

		// create a trend first
		$inputData = [
			'name'          => Input::get('tname'),
			'info'          => Input::get('tinfo'),
			'price'         => Input::get('tprice'),
			'planner_id'    => Input::get('tprovider'),
			'city_id'       => Input::get('tcity'),
			'prerequisites' => Input::get('prereq'),
			'facts'         => Input::get('facts'),
			'terms'         => Input::get('terms'),
			'price_worth'   => Input::get('worthPrice'),
			'price_max'     => Input::get('maxPrice'),
			'range_info'    => Input::get('rangeInfo'),
			'url'           => $trendUrl,
			'code'          => $this->generateCode(Input::get('tprovider')),
			'created_at'    => date('Y-m-d H:i:s'),
			'updated_at'    => date('Y-m-d H:i:s')
		];

		$trend = \Trend::create($inputData);
		$trendId = $trend->id;

		//save Events
		if ($trendId)
		{
			foreach ($events as $event)
			{
				TrendEvents::create([
					                    'type_event_id' => $event,
					                    'trend_id'      => $trendId
				                    ]);
			}
		}

		return redirect('/trends')->with('custom_success', 'New trend has been added successfully');
	}

	/**
	 * DELETE TREND
	 */
	public function getDelete($trendId)
	{
		$trend = \Trend::findOrFail($trendId);

		if ($trend->delete())
		{
			Cache::tags($trend->code)->flush();

			return redirect('/trends')->with('custom_success', 'Trend deleted successfully');
		}
	}

	/**
	 * SHOW EDIT TREND PAGE
	 */
	public function getEdit($trendId)
	{
		$trend = \Trend::findOrFail($trendId);
		$city = \City::all();
		$vendors = \Vendor::whereIn('type_id', [1, 2])->get();
		$trendsEventId = TrendEvents::where('trend_id', $trendId)->pluck('type_event_id');
		$typeEvents = TypeEvent::all();
		$terms = \BookingTerms::all();
		$facts = TypeFacts::all();
		$preReq = TypePrerequisite::all();

		$data = [
			'trend'         => $trend,
			'city'          => $city,
			'vendors'       => $vendors,
			'typeEvents'    => $typeEvents,
			'trendEventIds' => $trendsEventId,
			'terms'         => $terms,
			'facts'         => $facts,
			'preReq'        => $preReq
		];

		return view('trends/edit_trend', ['data' => $data]);
	}

	/**
	 * POST TREND EDITS
	 */
	public function postEditTrend()
	{
		$trendId = Input::get('trendId');
		$trend = \Trend::findOrFail($trendId);
		$events = Input::get('events');

		$rules = [
			'tname'      => 'required|min:4',
			'tprice'     => 'required|integer|min:1',
			'worthPrice' => 'integer|min_as:maxPrice',
			'maxPrice'   => 'integer|min_as:tprice',
			'rangeInfo'  => 'required_with:maxPrice',
			'tinfo'      => 'required|min:4',
			'prereq'     => 'min:10',
			'facts'      => 'min:10',
			'terms'      => 'min:10',
			'events'     => 'required'
		];

		$messages = [
			'tname.required'          => 'Please enter valid Trend name',
			'tname.min'               => 'Trend name should be minimum 4 characters long',
			'tinfo.required'          => 'Please enter valid Trend info',
			'tinfo.min'               => 'Trend info should be minimum 4 characters long',
			'tprice.required'         => 'Please enter valid Trend price',
			'tprice.integer'          => 'Trend price is not valid',
			'tprice.min'              => 'Trend price is not valid',
			'prereq.min'              => 'Prerequisites cannot be less than 10 character',
			'facts.min'               => 'Facts cannot be less than 10 characters',
			'terms.min'               => 'Terms cannot be less than 10 characters',
			'events.required'         => 'Please select at-least one supported events',
			'worthPrice.integer'      => 'Worth price must be number',
			'worthPrice.min_as'       => 'Worth price cannot be less than max price',
			'maxPrice.min_as'         => 'Max price cannot be less than trend price',
			'maxPrice.integer'        => 'Max price must be a number',
			'rangeInfo.required_with' => 'Price range info is mandatory when max price is available'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			$allInput = Input::all();
			$oldEvents = $events ? implode(',', $events) : '';
			$allInput['events'] = $oldEvents;

			return redirect()->back()
			                 ->withErrors($validator)
			                 ->withInput($allInput);
		}

		// update trend info
		$trend->name = Input::get('tname');
		$trend->info = Input::get('tinfo');
		$trend->price = Input::get('tprice');
		$trend->planner_id = Input::get('tprovider');
		$trend->prerequisites = Input::get('prereq');
		$trend->facts = Input::get('facts');
		$trend->url = ($trend->name != trim(Input::get('tname'))) ? $this->generateUrl(trim(Input::get('tname'))) : $trend->url;
		$trend->terms = Input::get('terms');
		$trend->city_id = Input::get('tcity');
		$trend->updated_at = date('Y-m-d H:i:s');
		$trend->price_worth = Input::get('worthPrice');
		$trend->price_max = Input::get('maxPrice');
		$trend->range_info = Input::get('rangeInfo');

		if ($trend->save())
		{

			//Flushing the Tags which is made in main based on the Id of that product
			Cache::tags($trend->code)->flush();

			//deleting old events and creating new.
			$oldTrendEvents = TrendEvents::where('trend_id', $trendId)->get();
			foreach ($oldTrendEvents as $oldTrendEvent)
			{
				$oldTrendEvent->forceDelete();
			}

			foreach ($events as $event)
			{
				TrendEvents::create([
					                    'type_event_id' => $event,
					                    'trend_id'      => $trendId
				                    ]);
			}
		}

		return redirect('/trends')->with('custom_success', 'Trend edit successful');

	}

	/**
	 * @author Vikash <vikash@evibe.in>
	 * @Since  6th May 2016
	 */
	public function getActivate($id)
	{
		$trend = \Trend::findOrFail($id);
		$event = $trend->events->first();

		if ($event)
		{
			$trend->update(['is_live' => 1]);
			$this->checkAndSendFirstActivationEmail($event->id, config('evibe.ticket_type.trends'), $trend);
		}
		else
		{
			return redirect()->back()->with('custom_error', 'Please tag a event before make it live');
		}

		return redirect()->back();
	}

	public function getDeactivate($id)
	{
		$trend = \Trend::findOrFail($id);

		if ($trend->update(['is_live' => 0]))
		{
			Cache::tags($trend->code)->flush();
			$this->removeItemsFromAutoBookAndPriorityTable(config('evibe.ticket_type.trends'), $id);
		}

		return redirect()->back();
	}

	// upload image
	public function postUploadImage($trendId)
	{
		$images = Input::file('images');
		$type = Input::get('gal-type');

		if ($type == config('evibe.gallery.type.image'))
		{

			if (count($images) == 0)
			{
				return redirect()->back()->with('custom_error', 'Please add at-least one image.')->withInput();
			}

			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:5120'
				];

				$imageMessages = [
					'file.required' => 'Please upload at least one image',
					'file.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
					'file.max'      => 'Image size cannot be more than 5 MB.'
				];

				$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

				if ($imageValidator->fails())
				{
					return redirect()->back()->withErrors($imageValidator)->withInput();
				}

				$directPath = '/trends/' . $trendId . '/images/';

				$imageName = $this->uploadImageToServer($image, $directPath);

				\TrendGallery::create([
					                      'url'         => $imageName,
					                      'type'        => $type,
					                      'title'       => $this->getImageTitle($image),
					                      'is_profile'  => 0,
					                      'trending_id' => $trendId,
				                      ]);
			}

			return redirect()->back()->with('successMsg', 'Image uploaded successfully');
		}
		elseif ($type == config('evibe.gallery.type.video'))
		{
			$video = Input::get('video');

			$rules = [
				'video' => 'required'
			];
			$message = [
				'video.required' => 'Video url is required (Ex: https://www.youtube.com/watch?v=videoId).',
			];

			$validator = validator(['video' => $video], $rules, $message);
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator)->withInput();
			}

			$videoId = $this->getYouTubeVideoId($video);

			if (!$videoId)
			{
				return redirect()->back()->with('custom_error', 'Please enter an valid youtube url')->withInput();
			}

			\TrendGallery::create([
				                      'trending_id' => $trendId,
				                      'type'        => $type,
				                      'url'         => $videoId
			                      ]);

			return redirect()->back()->with(['successMsg' => "Video uploaded successfully.", 'type' => 'video']);
		}

	}

	public function getProfilePic($imageId, $trendId)
	{
		$allImage = \TrendGallery::where('type', config('evibe.gallery.type.image'))
		                         ->where('trending_id', $trendId)
		                         ->where('id', '!=', $imageId)->get();

		\TrendGallery::findOrFail($imageId)->update(['is_profile' => 1]);

		foreach ($allImage as $image)
		{
			$image->update(['is_profile' => 0]);
		}

		return redirect()->back();
	}

	public function postImageTitle($imageId, $trendId)
	{
		$image = \TrendGallery::find($imageId);

		if ($image && $image->update(['title' => Input::get('img-title-input')]))
		{
			return redirect()->back()->with('successMsg', 'Image title updated successfully');
		}
	}

	public function getDeletePic($imageId)
	{
		$deleteImage = \TrendGallery::findOrFail($imageId)->delete();

		if ($deleteImage)
		{
			return redirect()->back();
		}
	}

	// generate Trends Code
	public function generateCode($vendorId)
	{
		$vendor = \Vendor::find($vendorId);
		$prefix = config('evibe.code.prefix.trend');
		$cityCode = $vendor->city->code;

		$lastObj = \Trend::withTrashed()->orderBy('code', 'DESC')->first();

		if ($lastObj)
		{
			if ($lastObj->code)
			{
				$digits = ((int)substr($lastObj->code, 5)) + 1;
			}
			else
			{
				$digits = $lastObj->id;
			}
		}
		else
		{
			$newObj = \Trend::orderBy('id', 'DESC')->first();
			if ($newObj)
			{
				if ($newObj->code)
				{
					$digits = ((int)substr($lastObj->code, 5)) + 1;
				}
				else
				{
					$digits = $newObj->id + 1;
				}
			}
			else
			{
				$digits = '0001';
			}
		}

		return $cityCode . $prefix . str_pad($digits, 4, "0", STR_PAD_LEFT);
	}

}
