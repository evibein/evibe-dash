<?php

namespace App\Http\Controllers\Analytics;

use App\Models\Coupon\LogReferralFriendActions;
use App\Models\Coupon\LogReferralPreActions;
use App\Models\TypeEvent;
use App\Models\Types\TypeEnquirySource;
use App\Models\User;
use App\Models\Util\TicketAnalytics;
use App\Models\Util\TicketAnalyticsNew;
use App\Models\Util\TicketReviewQuestionAnswer;
use Illuminate\Support\Facades\DB;
use \Ticket;
use App\Http\Controllers\Base\BaseController;
use Carbon\Carbon;

class AnalyticsController extends BaseController
{
	public function showCumulativeData()
	{
		$dates = $this->fetchDates();
		$data = [
			'dates'          => $dates,
			'tickets'        => [
				'count' => 0
			],
			'sources'        => [],
			'whyChooseEvibe' => [],
			'bookings'       => [
				'count'      => 0,
				'conversion' => 0,
				'gmv'        => [
					'total' => 0,
					'avg'   => 0,
					'max'   => 0,
					'min'   => 0
				],
				'revenue'    => 0,
				'category'   => [
					'city'     => [],
					'occasion' => []
				]
			]
		];

		// tickets
		$invalidStatuses = [
			config('evibe.status.duplicate'),
			config('evibe.status.related'),
			config('evibe.status.irrelevant')
		];
		$timestamps = [Carbon::parse($dates['from'])->timestamp, Carbon::parse($dates['to'])->timestamp];
		$tickets = Ticket::with('source')
		                 ->distinct('phone')
		                 ->whereBetween('created_at', array_flatten($dates))
		                 ->get();

		$data['tickets']['allCount'] = $tickets->count();

		$validTickets = $tickets->filter(function ($item, $key) use ($invalidStatuses) {
			return !in_array($item->status_id, $invalidStatuses);
		});

		$duplicateTickets = $tickets->filter(function ($item, $key) {
			if ($item->status_id == config('evibe.status.duplicate'))
			{
				return true;
			}
		});

		$relatedTickets = $tickets->filter(function ($item, $key) {
			if ($item->status_id == config('evibe.status.related'))
			{
				return true;
			}
		});

		$irrelevantTickets = $tickets->filter(function ($item, $key) {
			if ($item->status_id == config('evibe.status.irrelevant'))
			{
				return true;
			}
		});

		$data['tickets']['count'] = $validTickets->count();
		$data['tickets']['duplicate'] = $duplicateTickets->count();
		$data['tickets']['related'] = $relatedTickets->count();
		$data['tickets']['irrelevant'] = $irrelevantTickets->count();

		// sources (tickets)
		$ticketsBySources = $tickets->groupBy('source_id');
		$ticketsBySources->each(function ($items, $key) use (&$data) {
			if (!isset($data['sources'][$key]))
			{
				$ticketSource = \TypeTicketSource::find($key);
				$data['sources'][$key] = [
					'id'           => $key,
					'name'         => $ticketSource ? $ticketSource->name : "Unknown",
					'ticketsCount' => 0,
					'bookingCount' => 0,
					'conversion'   => 0,
					'gmv'          => [
						'total' => 0,
						'max'   => 0,
						'min'   => 0,
						'avg'   => 0
					]
				];
			}

			$data['sources'][$key]['ticketsCount'] = $items->count();
		});

		// bookings
		$bookedTickets = Ticket::where('status_id', config('evibe.status.booked'))
		                       ->whereBetween('paid_at', $timestamps)
		                       ->get();

		$bookedTicketIds = $bookedTickets->pluck('id');

		$bookings = \TicketBooking::select('ticket_bookings.*', 'ticket.source_id', 'ticket.event_id', 'ticket.city_id')
		                          ->with('mapping')
		                          ->join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                          ->whereIn('ticket_bookings.ticket_id', $bookedTicketIds)
		                          ->whereNull('ticket.deleted_at')
		                          ->where('ticket_bookings.is_advance_paid', 1)
		                          ->get();

		// get total unique orders = multiple orders
		// for same partner for same ticket is considered as one
		$uniqueOrders = [];
		foreach ($bookings as $booking)
		{
			$uniqueOrderKey = $booking->ticket_id . "_" . $booking->map_type_id . "_" . $booking->map_id;
			if (!array_key_exists($uniqueOrderKey, $uniqueOrders))
			{
				$uniqueOrders[$uniqueOrderKey] = 0;
			}

			$uniqueOrders[$uniqueOrderKey] = $uniqueOrders[$uniqueOrderKey] + $booking->booking_amount;
		}

		$totalUniqueOrders = count($uniqueOrders);
		$sumUniqueOrders = array_sum($uniqueOrders);

		$data['bookings']['count'] = $totalUniqueOrders;
		$data['bookings']['conversion'] = $data['tickets']['count'] ? round($data['bookings']['count'] / $data['tickets']['count'] * 100, 2) : 0;
		$data['bookings']['gmv'] = [
			'total' => $sumUniqueOrders,
			'max'   => count($uniqueOrders) ? max($uniqueOrders) : 0,
			'min'   => count($uniqueOrders) ? min($uniqueOrders) : 0,
			'avg'   => $totalUniqueOrders != 0 ? round($sumUniqueOrders / $totalUniqueOrders) : 0
		];

		// calculate revenue
		$data['bookings']['revenue'] = round($bookings->sum('evibe_service_fee') + $bookings->sum('sgst_amount') + $bookings->sum('cgst_amount') + $bookings->sum('igst_amount'));

		// by parties: grouping by phone as same customer might book in multiple tickets.
		// @todo: need to optimise this
		$partyBookings = \TicketBooking::select('ticket.phone', DB::raw('SUM(ticket_bookings.booking_amount) AS booking_amount'))
		                               ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
		                               ->whereIn('ticket_bookings.ticket_id', $bookedTicketIds)
		                               ->where('ticket_bookings.is_advance_paid', 1)
		                               ->groupBy('ticket.phone')
		                               ->get();

		$data['parties']['count'] = $partyBookings->count();
		$data['parties']['conversion'] = $data['tickets']['count'] ? round($data['parties']['count'] / $data['tickets']['count'] * 100, 2) : 0;
		$data['parties']['gmv'] = [
			'max' => $partyBookings->max('booking_amount'),
			'min' => $partyBookings->min('booking_amount'),
			'avg' => round($partyBookings->avg('booking_amount'))
		];

		// bookings by city
		$cities = [];
		$allCities = \City::select('id', 'name')->get();
		foreach ($allCities as $city)
		{
			$cities[$city->id] = $city->name;
		}

		$bookingsByCity = $bookings->pluck('city_id');
		$bookingsByCity = $bookingsByCity->filter(function ($value, $key) {
			return !is_null($value);
		})->sort()->values()->all();

		$bookingsByCity = array_count_values($bookingsByCity);
		$current = &$data['bookings']['category']['city'];

		foreach ($bookingsByCity as $cityId => $count)
		{
			$current[$cityId] = [
				"name"  => $cities[$cityId],
				"count" => $count
			];
		}

		// bookings by occasion
		$occasions = [];
		$allOccasions = TypeEvent::select('id', 'name')->get();
		foreach ($allOccasions as $occasion)
		{
			$occasions[$occasion->id] = $occasion->name;
		}

		$bookingsByOccasion = $bookings->pluck('event_id');
		$bookingsByOccasion = $bookingsByOccasion->filter(function ($value) {
			return !is_null($value);
		})->sort()->values()->all();

		$bookingsByOccasion = array_count_values($bookingsByOccasion);
		$current = &$data['bookings']['category']['occasion'];

		foreach ($bookingsByOccasion as $occasionId => $count)
		{
			$current[$occasionId] = [
				"name"  => $occasions[$occasionId],
				"count" => $count
			];
		}

		// sources (bookings)
		$bookingsBySources = $bookings->groupBy('source_id');
		$bookingsBySources->each(function ($items, $key) use (&$data) {
			$current = &$data['sources'][$key];
			$current['bookingCount'] = $items->count();
			$current['gmv'] = [
				'total' => $items->sum('booking_amount'),
				'max'   => $items->max('booking_amount'),
				'min'   => $items->min('booking_amount'),
				'avg'   => round($items->avg('booking_amount'))
			];
		});

		// source (conversions)
		$conversionsBySources = $bookings->unique('ticket_id')->groupBy('source_id');
		$conversionsBySources->each(function ($items, $key) use (&$data) {
			$current = &$data['sources'][$key];
			if (isset($current['ticketsCount']))
			{
				$current['conversion'] = $current['ticketsCount'] ? (int)($items->count() / $current['ticketsCount'] * 100) : 0;
			}
		});

		// why customer booked with evibe.in
		$data['whyChooseEvibe'] = [];
		$whyChooseEvibeQuestionId = config('evibe.why_choose_evibe');
		$answerByOption = TicketReviewQuestionAnswer::join('evibe_review_question_option', 'evibe_review_question_option.id', '=', 'evibe_review_question_answer.option_id')
		                                            ->where('evibe_review_question_option.question_id', $whyChooseEvibeQuestionId)
		                                            ->whereBetween('evibe_review_question_answer.created_at', [$dates['from'], $dates['to']])
		                                            ->groupBy('evibe_review_question_answer.option_id')
		                                            ->select(DB::raw('count(*) as count'), 'evibe_review_question_answer.option_id', 'evibe_review_question_option.text')
		                                            ->get();

		foreach ($answerByOption as $answerOption)
		{
			$data['whyChooseEvibe'][$answerOption->option_id] = [
				"id"          => (int)$answerOption->option_id,
				"name"        => $answerOption->text,
				"optionCount" => (int)$answerOption->count
			];
		}

		return view('analytics.cumulative', $data);
	}

	public function funnelChart()
	{
		$dates = $this->fetchDates();

		$data = [
			'dates'      => $dates,
			'funnelData' => $this->prepareData($dates)
		];

		return view('analytics.funnel', $data);
	}

	public function referAndEarnStatistics()
	{
		$dates = $this->fetchDates();
		$views = LogReferralPreActions::where("type_action", "view")->wherebetween("created_at", array_flatten($dates))->count();
		$share = LogReferralPreActions::where("type_action", "share")->wherebetween("created_at", array_flatten($dates))->count();
		$booked = LogReferralFriendActions::where("type_referral_event", "book")->wherebetween("created_at", array_flatten($dates))->count();
		$signups = LogReferralFriendActions::where("type_referral_event", "signup")->wherebetween("created_at", array_flatten($dates))->count();
		$funnelData = [
			[
				'label' => 'Share',
				'value' => $share,
			],
			[
				'label' => 'Views',
				'value' => $views,
			],

			[
				'label' => 'Signups',
				'value' => $signups,
			],
			[
				'label' => 'Booked',
				'value' => $booked,
			]
		];
		$data = [
			'dates'      => $dates,
			'funnelData' => $funnelData,
			'views'      => $views,
			'share'      => $share,
			'booked'     => $booked,
			'signups'    => $signups
		];

		return view('analytics.refer-earn', $data);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showDetailedRevenueData()
	{
		$dates = $this->fetchDates();
		$level = request("level", false);
		$level = ($level && $level == "summary") ? false : $level;
		$timestamps = [Carbon::parse($dates['from'])->timestamp, Carbon::parse($dates['to'])->timestamp];
		$rData = [
			"by_city"        => [
				"values" => [
					"name"    => "By City",
					"count"   => 0,
					"revenue" => 0
				],
				"list"   => []
			],
			"by_occasion"    => [
				"values" => [
					"name"    => "By Occasion",
					"count"   => 0,
					"revenue" => 0
				],
				"list"   => []
			],
			"by_category"    => [
				"values" => [
					"name"    => "By Category",
					"count"   => 0,
					"revenue" => 0
				],
				"list"   => []
			],
			"by_source"      => [
				"values" => [
					"name"    => "By Source",
					"count"   => 0,
					"revenue" => 0
				],
				"list"   => []
			],
			"by_book_method" => [
				"values" => [
					"name"    => "By Booking Method",
					"count"   => 0,
					"revenue" => 0
				],
				"list"   => [
					"normal" => [
						"values" => [
							"name"    => "Normal",
							"count"   => 0,
							"revenue" => 0
						],
						"levels" => []
					],
					"auto"   => [
						"values" => [
							"name"    => "Auto",
							"count"   => 0,
							"revenue" => 0
						],
						"levels" => []
					]
				]
			]
		];

		// get all bookings
		$allBookings = \TicketBooking::with("ticket", "ticketBookingType", "ticket.city", "ticket.event")
		                             ->select("ticket_bookings.*")
		                             ->join("ticket", "ticket.id", "=", "ticket_bookings.ticket_id")
		                             ->whereBetween("ticket.paid_at", $timestamps)
		                             ->where("ticket_bookings.is_advance_paid", 1)
		                             ->where("ticket.status_id", config("evibe.status.booked"))
		                             ->whereNull("ticket.deleted_at")
		                             ->whereNull("ticket_bookings.deleted_at")
		                             ->get();

		foreach ($allBookings as $booking)
		{
			$ticket = $booking->ticket;
			$bookingRevenue = round($booking->booking_amount * 0.143, 2);

			$cityId = $ticket->city_id ?: "other";
			$eventId = $ticket->event_id ?: "other";
			$categoryId = $booking->type_ticket_booking_id ?: "other";
			$sourceId = $ticket->source ? $ticket->source->id : "Other";
			$cityName = $ticket->city ? $ticket->city->name : "Other";
			$eventName = $ticket->event ? $ticket->event->name : "Other";
			$sourceName = $ticket->source ? $ticket->source->name : "Other";
			$categoryName = $booking->ticketBookingType ? $booking->ticketBookingType->name : "Other";
			$bookingType = $ticket->is_auto_booked ? "auto" : "normal";

			$keys = [
				"level"        => $level,
				"cityId"       => $cityId,
				"cityName"     => $cityName,
				"methodId"     => $bookingType,
				"methodName"   => $bookingType,
				"eventId"      => $eventId,
				"eventName"    => $eventName,
				"categoryId"   => $categoryId,
				"sourceId"     => $sourceId,
				"categoryName" => $categoryName,
				"bookingType"  => $bookingType,
				"sourceName"   => $sourceName,
				"revenue"      => $bookingRevenue
			];

			// by_city
			if ($level == "by_city" || !$level)
			{
				$keys["currentSub"] = "by_city";
				$keys["subId"] = $cityId;
				$keys["subName"] = $cityName;
				$this->fetchRevenueSubData($rData["by_city"], $keys);
			}

			// by occasion
			if ($level == "by_occasion" || !$level)
			{
				$keys["currentSub"] = "by_occasion";
				$keys["subId"] = $eventId;
				$keys["subName"] = $eventName;
				$this->fetchRevenueSubData($rData["by_occasion"], $keys);
			}

			// by category
			if ($level == "by_category" || !$level)
			{
				$keys["currentSub"] = "by_category";
				$keys["subId"] = $categoryId;
				$keys["subName"] = $categoryName;
				$this->fetchRevenueSubData($rData["by_category"], $keys);
			}

			// by source
			if ($level == "by_source" || !$level)
			{
				$keys["currentSub"] = "by_source";
				$keys["subId"] = $sourceId;
				$keys["subName"] = $sourceName;
				$this->fetchRevenueSubData($rData["by_source"], $keys);
			}

			// by booking method
			if ($level == "by_book_method" || !$level)
			{
				$keys["currentSub"] = "by_book_method";
				$keys["subId"] = $bookingType;
				$keys["subName"] = $bookingType;

				$this->fetchRevenueSubData($rData["by_book_method"], $keys);
			}
		}

		// sort data
		if (!$level)
		{
			foreach ($rData as $key => $subValues)
			{
				$sorted = collect($subValues["list"])->sortByDesc(function ($item) {
					return $item["values"]["revenue"];
				});

				$rData[$key]["list"] = $sorted->values()->all();
			}
		}
		else
		{
			foreach ($rData[$level]["list"] as $subKey => $subValues)
			{
				foreach ($subValues["levels"] as $levelKey => $levelValues)
				{
					$sorted = collect($levelValues["list"])->sortByDesc(function ($item) {
						return $item["values"]["revenue"];
					});

					$rData[$level]["list"][$subKey]["levels"][$levelKey]["list"] = $sorted->values()->all();
				}
			}
		}

		$data = [
			"level" => $level,
			"dates" => $dates,
			"rData" => $rData
		];

		if (!$level)
		{
			return view("analytics.rev.summary", $data);
		}
		else
		{
			return view("analytics.rev.level", $data);
		}
	}

	public function showDetailedEnquiryData()
	{
		$resetFilter = false;
		$dates = $this->fetchDates(true);

		/*
		 * Showing reset filter if date is applied
		 */
		if (isset($dates["isFilter"]) && $dates["isFilter"])
		{
			$resetFilter = true;
		}

		/*
		 * Fetching all the tickets that are created between the selected dates
		 * Dont do "get" on this collection because, ram will not be sufficient to render all the data & store it in a variable
		 */
		$enquiries = Ticket::whereBetween("updated_at", [$dates["from"], $dates["to"]]);

		if (request()->has("timeline"))
		{
			if (request("timeline") == "new")
			{
				$resetFilter = true;
				$enquiries->whereBetween("created_at", [$dates["from"], $dates["to"]]);
			}
			elseif (request("timeline") == "return")
			{
				$resetFilter = true;
				$enquiries->where("created_at", "<", $dates["from"]);
			}
		}

		/*
		 * Filters
		 */
		$filters = $this->applyFilters($enquiries);
		$enquiries = $filters["enquiries"];
		$resetFilter = isset($filters["resetFilter"]) && $filters["resetFilter"] ? true : $resetFilter;

		/*
		 * Fetch all the main event Ids
		 */
		$primaryOccasionIds = [config("evibe.event.kids_birthday"), config("evibe.event.special_experience"), config("evibe.event.house_warming"), config("evibe.event.bachelor_party"), config("evibe.event.pre_post"), ""];
		$parentOccasionIds = array_filter($enquiries->pluck("event_id")->toArray(), function ($var) use ($primaryOccasionIds) {
			return in_array($var, $primaryOccasionIds);
		});
		/*
		 * Using name AS name because, in the function getUniqueKeyAssignedOptions we are using key value as "name".
		 * If name is not there while fetching, we should rename it like "title AS name"
		 */
		$occasions = $this->getUniqueKeyAssignedOptions($parentOccasionIds, TypeEvent::select("id", "name AS name")->get()->keyBy("id")->toArray());

		/*
		 * Fetching all the enquiry source Ids
		 */
		$typeEnquirySources = TypeEnquirySource::select("id", "name AS name")->get()->keyBy("id")->toArray();
		$enquirySources = $this->getUniqueKeyAssignedOptions($enquiries->pluck("enquiry_source_id")->toArray(), $typeEnquirySources);

		/*
		 * Fetching all the ticket source Ids
		 */
		$typeTicketSources = \TypeTicketSource::select("id", "name AS name")->get()->keyBy("id")->toArray();
		$sources = $this->getUniqueKeyAssignedOptions($enquiries->pluck("source_id")->toArray(), $typeTicketSources);

		/*
		 * Fetching all the city Id's
		 */
		$allCities = \City::select("id", "name AS name")->get()->keyBy("id")->toArray();
		$cities = $this->getUniqueKeyAssignedOptions($enquiries->pluck("city_id")->toArray(), $allCities);

		/*
		 * Fetching all the status Id's
		 */
		$typeTicketStatus = \TypeTicketStatus::select("id", "name AS name")->get()->keyBy("id")->toArray();
		$endStates = $this->getUniqueKeyAssignedOptions($enquiries->pluck("status_id")->toArray(), $typeTicketStatus);

		/*
		 * Filter data
		 */
		$typeEvents = TypeEvent::WhereNull("parent_id")->get()->keyBy("id")->toArray();

		$totalCount = $enquiries->count();
		$newCount = $enquiries->whereBetween("created_at", [$dates["from"], $dates["to"]])->count();

		$data = [
			"count"       => [
				"occasions"      => $occasions,
				"enquirySources" => $enquirySources,
				"sources"        => $sources,
				"cities"         => $cities,
				"endStatus"      => $endStates,
				"timeline"       => [
					"New"    => $newCount,
					"Return" => $totalCount - $newCount
				]
			],
			"filters"     => [
				"typeEvents"     => $typeEvents,
				"enquirySources" => $typeEnquirySources,
				"ticketSources"  => $typeTicketSources,
				"cities"         => $allCities,
				"ticketStatus"   => $typeTicketStatus
			],
			"dates"       => $dates,
			"resetFilter" => $resetFilter
		];

		return view("analytics.enquiry.summary", ["data" => $data]);
	}

	public function showDetailedBookingsData()
	{
		$resetFilter = false;
		$dates = $this->fetchDates(true);

		/*
		 * Showing reset filter if date is applied
		 */
		if (isset($dates["isFilter"]) && $dates["isFilter"])
		{
			$resetFilter = true;
		}

		/*
		 * Fetching all the tickets who paid between the selected dates & currently in booked status
		 * Dont do "get" on this collection because, ram will not be sufficient to render all the data & store it in a variable
		 */
		$enquiries = Ticket::whereBetween("paid_at", [strtotime($dates["from"]), strtotime($dates["to"])])
		                   ->where('status_id', config("evibe.status.booked"));

		if (request()->has("timeline"))
		{
			if (request("timeline") == "new")
			{
				$resetFilter = true;
				$enquiries->whereBetween("created_at", [$dates["from"], $dates["to"]]);
			}
			elseif (request("timeline") == "return")
			{
				$resetFilter = true;
				$enquiries->where("created_at", "<", $dates["from"]);
			}
		}

		/*
		 * Filters
		 */
		$filters = $this->applyFilters($enquiries);
		$enquiries = $filters["enquiries"];
		$resetFilter = isset($filters["resetFilter"]) && $filters["resetFilter"] ? true : $resetFilter;

		/*
		 * Fetch all the main event Ids
		 */
		$primaryOccasionIds = [config("evibe.event.kids_birthday"), config("evibe.event.special_experience"), config("evibe.event.house_warming"), config("evibe.event.bachelor_party"), config("evibe.event.pre_post"), ""];
		$parentOccasionIds = array_filter($enquiries->pluck("event_id")->toArray(), function ($var) use ($primaryOccasionIds) {
			return in_array($var, $primaryOccasionIds);
		});
		/*
		 * Using name AS name because, in the function getUniqueKeyAssignedOptions we are using key value as "name".
		 * If name is not there while fetching, we should rename it like "title AS name"
		 */
		$occasions = $this->getUniqueKeyAssignedOptions($parentOccasionIds, TypeEvent::select("id", "name AS name")->get()->keyBy("id")->toArray());

		/*
		 * Fetch all the secondary event Ids which have parent Ids
		 */
		$secondaryOccasionIds = TypeEvent::whereNotNull("parent_id")->pluck("id")->toArray();
		$subOccasionIds = array_filter($enquiries->pluck("event_id")->toArray(), function ($var) use ($secondaryOccasionIds) {
			return in_array($var, $secondaryOccasionIds);
		});
		$subOccasions = $this->getUniqueKeyAssignedOptions($subOccasionIds, TypeEvent::select("id", "name AS name")->get()->keyBy("id")->toArray());

		/*
		 * Adding sub-occasion count to the main occasion count
		 */
		$secondaryOccasions = TypeEvent::select("name", "parent_id")->whereNotNull("parent_id")->get()->keyBy("name")->toArray();
		foreach ($subOccasions as $key => $subOccasion)
		{
			if (isset($secondaryOccasions[$key]))
			{
				$parentId = $secondaryOccasions[$key]["parent_id"];
				if ($parentId == config("evibe.event.kids_birthday"))
				{
					if (isset($occasions["Kids Events"]))
					{
						$occasions["Kids Events"] = $occasions["Kids Events"] + $subOccasion;
					}
					else
					{
						$occasions["Kids Events"] = $subOccasion;
					}
				}
				elseif ($parentId == config("evibe.event.special_experience"))
				{
					if (isset($occasions["Surprises"]))
					{
						$occasions["Surprises"] = $occasions["Surprises"] + $subOccasion;
					}
					else
					{
						$occasions["Surprises"] = $subOccasion;
					}
				}
			}
		}

		/*
		 * Fetching all the customer genders
		 */
		$typeCustomerGenders = [
			1 => ["id" => 1, "name" => "Male"], 2 => ["id" => 2, "name" => "Female"]
		];
		$customerGender = $this->getUniqueKeyAssignedOptions($enquiries->pluck("customer_gender")->toArray(), $typeCustomerGenders);

		/*
		 * Fetching all Booking process types
		 */
		$typeBookingProcessTypes = [
			1 => ["id" => 1, "name" => "Auto Booking"], "0" => ["id" => "0", "name" => "Normal Booking"]
		];
		$customerBookingTypes = $this->getUniqueKeyAssignedOptions($enquiries->pluck("is_auto_booked")->toArray(), $typeBookingProcessTypes);

		/*
		 * Fetching all the enquiry source Ids
		 */
		$typeEnquirySources = TypeEnquirySource::select("id", "name AS name")->get()->keyBy("id")->toArray();
		$enquirySources = $this->getUniqueKeyAssignedOptions($enquiries->pluck("enquiry_source_id")->toArray(), $typeEnquirySources);

		/*
		 * Fetching all the ticket source Ids
		 */
		$typeTicketSources = \TypeTicketSource::select("id", "name AS name")->get()->keyBy("id")->toArray();
		$sources = $this->getUniqueKeyAssignedOptions($enquiries->pluck("source_id")->toArray(), $typeTicketSources);

		/*
		 * Fetching all the city Id's
		 */
		$allCities = \City::select("id", "name AS name")->get()->keyBy("id")->toArray();
		$cities = $this->getUniqueKeyAssignedOptions($enquiries->pluck("city_id")->toArray(), $allCities);

		/*
		 * Filter data
		 */
		$typeEvents = TypeEvent::whereIn('id', $primaryOccasionIds)->orWhereNotNull("parent_id")->get()->keyBy("id")->toArray();

		$totalCount = $enquiries->count();
		$newCount = $enquiries->whereBetween("created_at", [$dates["from"], $dates["to"]])->count();

		$data = [
			"count"       => [
				"occasions"      => $occasions,
				"subOccasions"   => $subOccasions,
				"enquirySources" => $enquirySources,
				"sources"        => $sources,
				"cities"         => $cities,
				"gender"         => $customerGender,
				"bookingTypes"   => $customerBookingTypes,
				"timeline"       => [
					"New"    => $newCount,
					"Return" => $totalCount - $newCount
				]
			],
			"filters"     => [
				"typeEvents"     => $typeEvents,
				"enquirySources" => $typeEnquirySources,
				"ticketSources"  => $typeTicketSources,
				"cities"         => $allCities
			],
			"dates"       => $dates,
			"resetFilter" => $resetFilter
		];

		return view("analytics.bookings.summary", ["data" => $data]);
	}

	public function getBookingsTicketData()
	{
		$dates = $this->fetchDates(true);

		/*
		 * Fetching all the tickets who paid between the selected dates & currently in booked status
		 * Dont do "get" on this collection because, ram will not be sufficient to render all the data & store it in a variable
		 */
		$enquiries = TicketAnalytics::whereBetween("paid_at", [strtotime($dates["from"]), strtotime($dates["to"])])
		                            ->where('status_id', config("evibe.status.booked"));

		/*
		 * Filters
		 */
		$filters = $this->applyFilters($enquiries);
		$enquiries = $filters["enquiries"];

		if (request()->has("timeline"))
		{
			if (request("timeline") == "new")
			{
				$enquiries->whereBetween("ticket_created_at", [$dates["from"], $dates["to"]]);
			}
			elseif (request("timeline") == "return")
			{
				$enquiries->where("ticket_created_at", "<", $dates["from"]);
			}
		}

		// CRM, SR_CRM & ADMIN users
		$users = User::whereIn('role_id', [config("evibe.roles.customer_delight"), config("evibe.roles.sr_crm"), config("evibe.roles.admin"), config("evibe.roles.super_admin")])
		             ->whereNull('deleted_at')
		             ->pluck("name", "id");

		$data = [
			"cityDetails"             => \City::all()->pluck("name", "id"),
			"eventTypeDetails"        => TypeEvent::all()->pluck("name", "id"),
			"typeTicketSource"        => \TypeTicketSource::all()->pluck("name", "id"),
			"typeTicketStatus"        => \TypeTicketStatus::all()->pluck("name", "id"),
			"typeEnquirySource"       => TypeEnquirySource::all()->pluck("name", "id"),
			"typeTicketBookingSource" => \TypeTicket::all()->pluck("name", "id"),
			"handlerDetails"          => $users,
			"ticketData"              => $enquiries->get()
		];

		return view("analytics.bookings.ticket_data", ["data" => $data]);
	}

	public function getBookingsTicketStatusData()
	{
		$dates = $this->fetchDates(true);
		$dateLabels = [];
		$ticketStatus = [];
		$totalCount = [];

		$timeDifference = strtotime($dates["to"]) - strtotime($dates["from"]);

		// For not only showing data if it between 2 to 30 days
		if ($timeDifference > 86400 && $timeDifference < 5184000)
		{
			$startUnixTimestampFormat = strtotime($dates["from"]);
			$endUnixTimestampFormat = strtotime($dates["to"]);
			$ticketIdsFromView = TicketAnalyticsNew::whereBetween("date", [$dates["from"], $dates["to"]])->pluck("ticket_id")->toArray();

			$enquiries = TicketAnalytics::whereIn("ticket_id", $ticketIdsFromView);
			$filters = $this->applyFilters($enquiries);
			$ticketIds = $filters["enquiries"]->pluck("ticket_id")->toArray();

			$ticketStatus = [
				"Kids Birthday" => [],
				"House Warming" => [],
				"Surprises"     => []
			];
			$totalCount = [];

			$kidsSubOccasion = TypeEvent::where("parent_id", config("evibe.event.kids_birthday"))
			                            ->orWhere("id", config("evibe.event.kids_birthday"))
			                            ->pluck("id")
			                            ->toArray();

			for ($i = $startUnixTimestampFormat; $i < $endUnixTimestampFormat; $i = $i + 86400)
			{
				$dateFormat = date('d/m/Y', $i);
				$dateLabels[] = $dateFormat;

				$tickets = Ticket::select("event_id")
				                 ->whereIn("id", $ticketIds)
				                 ->where("status_id", config("evibe.status.booked"))
				                 ->whereBetween("paid_at", [($i - ($i % 86400)), ($i - ($i % 86400)) + 86399])
				                 ->pluck("event_id")
				                 ->toArray();

				$ticketStatus["Kids Birthday"][$dateFormat] = 0;
				$ticketStatus["House Warming"][$dateFormat] = 0;
				$ticketStatus["Surprises"][$dateFormat] = 0;
				$totalCount[$dateFormat] = count($tickets);

				foreach ($tickets as $ticketEventId)
				{
					if (in_array($ticketEventId, $kidsSubOccasion))
					{
						$ticketStatus["Kids Birthday"][$dateFormat]++;
					}
					elseif ($ticketEventId == config("evibe.event.house_warming"))
					{
						$ticketStatus["House Warming"][$dateFormat]++;
					}
					elseif ($ticketEventId == config("evibe.event.special_experience"))
					{
						$ticketStatus["Surprises"][$dateFormat]++;
					}
				}
			}
		}

		return response()->json([
			                        "success"      => true,
			                        "ticketStatus" => $ticketStatus,
			                        "totalCount"   => $totalCount,
			                        "labels"       => array_keys($totalCount)
		                        ]);
	}

	public function fetchADDOns()
	{
		$dates = $this->fetchDates(true);

		/*
		 * Fetching all the tickets who paid between the selected dates & currently in booked status
		 * Dont do "get" on this collection because, ram will not be sufficient to render all the data & store it in a variable
		 */
		$enquiries = Ticket::whereBetween("paid_at", [strtotime($dates["from"]), strtotime($dates["to"])])
		                   ->where('status_id', config("evibe.status.booked"));

		/*
		 * Filters
		 */
		$filters = $this->applyFilters($enquiries);
		$enquiries = $filters["enquiries"];

		$ticketBookings = \TicketBooking::whereIn("ticket_id", $enquiries->pluck("id")->toArray());

		$addOnsCount = \TicketMapping::whereIn("id", $ticketBookings->pluck("ticket_mapping_id")->toArray())
		                             ->where("map_type_id", config("evibe.ticket_type.add-on"))
		                             ->count();

		return response()->json([
			                        "success" => true,
			                        "data"    => [
				                        "Add Ons" => $addOnsCount
			                        ]
		                        ]);
	}

	private function getUniqueKeyAssignedOptions($array, $arrayKeys)
	{
		foreach ($array as $key => $value)
		{
			if (is_null($value))
			{
				$array[$key] = "Not Set";
			}
		}

		$uniqueArray = array_count_values($array);

		foreach ($uniqueArray as $key => $value)
		{
			$name = isset($arrayKeys[$key]["name"]) ? $arrayKeys[$key]["name"] : $key;
			$uniqueArray[$name] = $value;

			if (isset($arrayKeys[$key]["name"]))
			{
				unset($uniqueArray[$key]);
			}
		}

		asort($uniqueArray);

		return $uniqueArray;
	}

	private function applyFilters($enquiries)
	{
		$resetFilter = false;
		if (request()->has("occasion"))
		{
			$resetFilter = true;
			$enquiries->whereIn("event_id", $this->getOccasionFilterData(request("occasion")));
		}
		if (request()->has("customerGender"))
		{
			$resetFilter = true;
			$enquiries->where("customer_gender", request("customerGender"));
		}
		if (request()->has("bookingProcess"))
		{
			$resetFilter = true;
			$enquiries->where("is_auto_booked", request("bookingProcess"));
		}
		if (request()->has("enquirySource"))
		{
			$resetFilter = true;
			$enquiries->whereIn("enquiry_source_id", $this->getEnquirySourceData(request("enquirySource")));
		}
		if (request()->has("customerSource"))
		{
			$resetFilter = true;
			$enquiries->whereIn("source_id", request("customerSource"));
		}
		if (request()->has("city"))
		{
			$resetFilter = true;
			$enquiries->whereIn("city_id", request("city"));
		}
		if (request()->has("ticketStatus"))
		{
			$resetFilter = true;
			$enquiries->whereIn("status_id", $this->getStatusFilterData(request("ticketStatus")));
		}

		return [
			"enquiries"   => $enquiries,
			"resetFilter" => $resetFilter
		];
	}

	private function getOccasionFilterData($array)
	{
		if (in_array(config("evibe.event.kids_birthday"), $array))
		{
			$array = array_merge($array, TypeEvent::where("parent_id", config("evibe.event.kids_birthday"))->get()->pluck("id")->toArray());
		}
		if (in_array(config("evibe.event.special_experience"), $array))
		{
			$array = array_merge($array, TypeEvent::where("parent_id", config("evibe.event.special_experience"))->get()->pluck("id")->toArray());
		}

		return $array;
	}

	private function getEnquirySourceData($array)
	{
		if (in_array("scheduled", $array))
		{
			$array = TypeEnquirySource::where("id", "!=", config("evibe.ticket_enquiry_source.phone"))->get()->pluck("id")->toArray();
		}

		return $array;
	}

	private function getStatusFilterData($array)
	{
		if (in_array("valid", $array))
		{
			$array = array_merge($array, [
				config("evibe.status.initiated"),
				config("evibe.status.progress"),
				config("evibe.status.confirmed"),
				config("evibe.status.followup"),
				config("evibe.status.auto_pay"),
				config("evibe.status.auto_cancel"),
				config("evibe.status.service_auto_pay"),
				config("evibe.status.no_response"),
				config("evibe.status.invalid_email")
			]);
		}

		if (in_array("invalid", $array))
		{
			$array = array_merge($array, [
				config("evibe.status.duplicate"),
				config("evibe.status.related"),
				config("evibe.status.irrelevant")
			]);
		}

		return $array;
	}

	private function fetchDates($resetFilter = false)
	{
		// default
		$dates = [
			'from' => Carbon::now()->subDay(1)->startOfDay()->addHour(20)->toDateTimeString(),
			'to'   => Carbon::now()->startOfDay()->addHour(20)->toDateTimeString()
		];

		if (request()->get('to'))
		{
			$dates['to'] = Carbon::parse(request()->get('to'))->toDateTimeString();
		}

		if (request()->get('from'))
		{
			$dates['from'] = Carbon::parse(request()->get('from'))->toDateTimeString();
		}

		// reset `from` if not valid
		if ($dates['from'] > $dates['to'])
		{
			$dates['from'] = Carbon::parse($dates['to'])->startOfMonth()->toDateTimeString();
		}

		if ($resetFilter && (request()->get('to') || request()->get('from')))
		{
			$dates['isFilter'] = true;
		}

		return $dates;
	}

	private function prepareData($dates)
	{
		$timestamps = [Carbon::parse($dates['from'])->timestamp, Carbon::parse($dates['to'])->timestamp];
		$recommendationsCount = $bookingCount = 0;
		$ticketCount = Ticket::wherebetween('created_at', array_flatten($dates))->count();
		$bookings = \Ticket::whereBetween('paid_at', $timestamps)->count();
		$mappings = \TicketMapping::select(DB::raw('count(*) as count'))->whereBetween('recommended_at', array_flatten($dates))->groupBy('ticket_id')->get();

		foreach ($mappings as $mapping)
		{
			$recommendationsCount += $mapping->count;
		}
		$funnelData = [

			[
				'label' => 'Tickets',
				'value' => $ticketCount,
			],
			[
				'label' => 'Recommendation Email Sent',
				'value' => $recommendationsCount
			],
			[
				'label' => 'Bookings',
				'value' => $bookings,
			]
		];

		return $funnelData;
	}

	private function fetchRevenueSubData(&$subData, $keys)
	{
		$subId = $keys["subId"];
		$subName = $keys["subName"];
		$revenue = $keys["revenue"];

		if (!isset($subData["list"][$subId]))
		{
			$subData["list"][$subId] = [
				"values" => [
					"name"    => $subName,
					"count"   => 0,
					"revenue" => 0,
				],
				"levels" => [

				]
			];
		}

		$subData["values"]["count"] += 1;
		$subData["values"]["revenue"] += $revenue;
		$subData["list"][$subId]["values"]["count"] += 1;
		$subData["list"][$subId]["values"]["revenue"] += $revenue;

		// levels data
		$this->fetchRevenueSubLevelsData($subData["list"][$subId]["levels"], $keys);
	}

	private function fetchRevenueSubLevelsData(&$levelData, $keys)
	{
		$level = $keys["level"];

		// by_city
		if ($level != "by_city")
		{
			if (!isset($levelData["by_city"]))
			{
				$levelData["by_city"] = [
					"values" => [
						"name"    => "By City",
						"count"   => 0,
						"revenue" => 0
					],
					"list"   => []
				];
			}

			$this->fetchRevLevelData($levelData["by_city"], $keys["cityId"], $keys["cityName"], $keys["revenue"]);
		}

		// by_occasion
		if ($level != "by_occasion")
		{
			if (!isset($levelData["by_occasion"]))
			{
				$levelData["by_occasion"] = [
					"values" => [
						"name"    => "By Occasion",
						"count"   => 0,
						"revenue" => 0
					],
					"list"   => []
				];
			}

			$this->fetchRevLevelData($levelData["by_occasion"], $keys["eventId"], $keys["eventName"], $keys["revenue"]);
		}

		// by_category
		if ($level != "by_category")
		{
			if (!isset($levelData["by_category"]))
			{
				$levelData["by_category"] = [
					"values" => [
						"name"    => "By Category",
						"count"   => 0,
						"revenue" => 0
					],
					"list"   => []
				];
			}

			$this->fetchRevLevelData($levelData["by_category"], $keys["categoryId"], $keys["categoryName"], $keys["revenue"]);
		}

		// by_source
		if ($level != "by_source")
		{
			if (!isset($levelData["by_source"]))
			{
				$levelData["by_source"] = [
					"values" => [
						"name"    => "By Source",
						"count"   => 0,
						"revenue" => 0
					],
					"list"   => []
				];
			}

			$this->fetchRevLevelData($levelData["by_source"], $keys["sourceId"], $keys["sourceName"], $keys["revenue"]);
		}

		// by_book_method
		if ($level != "by_book_method")
		{
			if (!isset($levelData["by_book_method"]))
			{
				$levelData["by_book_method"] = [
					"values" => [
						"name"    => "By Booking Method",
						"count"   => 0,
						"revenue" => 0
					],
					"list"   => []
				];
			}

			$this->fetchRevLevelData($levelData["by_book_method"], $keys["methodId"], $keys["methodName"], $keys["revenue"]);
		}
	}

	private function fetchRevLevelData(&$levelData, $levId, $levName, $rev)
	{
		if (!isset($levelData["list"][$levId]))
		{
			$levelData["list"][$levId] = [
				"values" => [
					"name"    => $levName,
					"count"   => 0,
					"revenue" => 0,
				]
			];
		}

		$levelData["values"]["count"] += 1;
		$levelData["values"]["revenue"] += $rev;
		$levelData["list"][$levId]["values"]["count"] += 1;
		$levelData["list"][$levId]["values"]["revenue"] += $rev;
	}
}