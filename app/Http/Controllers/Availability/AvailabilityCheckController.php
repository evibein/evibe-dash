<?php

namespace App\Http\Controllers\Availability;

use App\Http\Controllers\Enquiry\PartnerEnquiryController;
use App\Models\AddOn\AddOn;
use App\Models\Planner\Planner;
use App\Models\TypeServices;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class AvailabilityCheckController extends PartnerEnquiryController
{
	public function fetchAvailabilityData($ticketId)
	{
		try
		{
			$availCheckOptionsData = request('availCheckOptionsData');
			$venueTypeId = request('venueTypeId');
			$price = null;
			$info = "";
			if ($venueTypeId == config('evibe.ticket_type.planners'))
			{
				$optionData = $availCheckOptionsData[0]; // only one product exists for non-venue
				$optionUnavailable = false;
				$name = null;
				switch ($optionData['optionTypeId'])
				{
					case config('evibe.ticket_type.packages'):
					case config('evibe.ticket_type.villa'):
					case config('evibe.ticket_type.food'):
					case config('evibe.ticket_type.couple-experiences'):
					case config('evibe.ticket_type.priests'):
					case config('evibe.ticket_type.tents'):
					case config('evibe.ticket_type.generic-package'):
						$option = \VendorPackage::find($optionData['optionId']);
						if (!$option)
						{
							$optionUnavailable = true;
							break;
						}

						$price = $option->price;
						$info = $option->info;
						$name = $option->name;
						break;
					case config('evibe.ticket_type.decors'):
						$option = \Decor::find($optionData['optionId']);
						if (!$option)
						{
							$optionUnavailable = true;
							break;
						}

						$price = $option->min_price;
						$info = $option->info;
						$name = $option->name;
						break;
					case config('evibe.ticket_type.cakes'):
						$option = \Cake::find($optionData['optionId']);
						if (!$option)
						{
							$optionUnavailable = true;
							break;
						}

						$price = $option->price;
						$info = $option->info;
						$name = $option->title;
						break;
					case config('evibe.ticket_type.entertainments'):
						// todo: test services
						$option = TypeServices::find($optionData['optionId']);
						if (!$option)
						{
							$optionUnavailable = true;
							break;
						}

						$price = $option->min_price;
						$info = $option->info;
						$name = $option->name;
						break;
					case config('evibe.ticket_type.add-on'):
						$option = AddOn::find($optionData['optionId']);
						if (!$option)
						{
							$optionUnavailable = true;
							break;
						}

						$price = $option->price_per_unit;
						$info = $option->info;
						$name = $option->name;
						break;
					case config('evibe.ticket_type.planners'):
						$option = Planner::find($optionData['optionId']);
						if (!$option)
						{
							$optionUnavailable = true;
							break;
						}

						$price = null;
						$info = null;
						$name = $option->name;
						break;
				}

				if ($optionUnavailable)
				{
					Log::error("Unable to fetch option data from optionId: " . $optionData['optionId'] . ", optionTypeId: " . $optionData['optionTypeId']);

					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => 'Unable to fetch option data. Kindly refresh the page and try again'
					                        ]);
				}

				$availCheckOptionsData[0]['name'] = $name;

				// fetch price
				// fetch info
			}
			elseif ($venueTypeId == config('evibe.ticket_type.venues'))
			{
				foreach($availCheckOptionsData as $key => $optionData)
				{
					$optionUnavailable = false;

					$option = \VendorPackage::find($optionData['optionId']);
					if(!$option)
					{
						$optionUnavailable = true;
						break;
					}

					if ($optionUnavailable)
					{
						Log::error("Unable to fetch venue option data from optionId: " . $optionData['optionId']);

						return response()->json([
							                        'success'  => false,
							                        'errorMsg' => 'Unable to fetch option data. Kindly refresh the page and try again'
						                        ]);
					}

					$availCheckOptionsData[$key]['optionTypeId'] = $option->type_ticket_id;
					$availCheckOptionsData[$key]['name'] = $option->name;
				}
			}

			return response()->json([
				                        'success'     => true,
				                        'venueTypeId' => $venueTypeId,
				                        'optionsData' => $availCheckOptionsData,
				                        'price'       => $price,
				                        'info'        => $info
			                        ]);
		} catch (\Exception $exception)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}

	public function postAvailabilityChecks($ticketId)
	{
		try
		{
			$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'availability-checks?isFromDash=true';

			$data = request()->all();

			$optionsData = json_decode(request('optionsData'));
			$data['optionsData'] = $optionsData;

			$partnersData = json_decode(request('partnersData'));
			$data['partnersData'] = $partnersData;

			$data['ticketId'] = $ticketId;
			$data['handlerId'] = auth()->user()->id;

			// first validate the image
			$images = request()->file('images');
			if (request()->hasFile('images') && request('enq-image-type') == 1)
			{
				foreach ($images as $image)
				{
					$imageRules = [
						'file' => 'mimes:png,jpeg,jpg,JPG|max:4096'
					];

					$imageMessages = [
						'file.mimes' => 'One of the images/video is not valid. (only .png, .jpeg, .jpg are accepted)',
						'file.max'   => 'Image size cannot br more than 1 MB'
					];

					$imageValidator = validator(['file' => $image], $imageRules, $imageMessages);
					if ($imageValidator->fails())
					{
						return response()->json(['success' => false, 'errorMsg' => $imageValidator->messages()->first()]);
					}
				}
			}

			$response = ['success' => false];

			try
			{
				$client = new Client();

				$response = $client->request('POST', $url, [
					'json' => $data
				]);

				$response = $response->getBody();
				$response = \GuzzleHttp\json_decode($response, true);

				if (array_key_exists('errorMsg', $response) || array_key_exists('hasImageError', $response))
				{
					$invalidImage = array_key_exists('invalidImage', $response) ? explode(",", $response['invalidImage']) : [];
					return response()->json(['success' => false, 'errorMsg' => $response['errorMsg'], 'imageError' => $invalidImage]);
				}

			} catch (ClientException $e)
			{
				$res['success'] = false;
				$apiResponse = $e->getResponse()->getBody(true);
				$apiResponse = \GuzzleHttp\json_decode($apiResponse);

				if (isset($apiResponse['errorMessage']))
				{
					return response()->json(['success' => false, 'error' => $apiResponse['errorMessage']]);
				}
			}

			/* todo: uploading avail check images */
			if (request()->hasFile('images') && request('enq-image-type') == 1 && $response['success'] && isset($response['mappingIds']) && count($response['mappingIds']))
			{
				$iCount = 0;
				$imageLinks = [];

				// @see :: here for multiple quote, if file is there then uploading first images by file and
				// uploading images by the same link for other custom quote
				foreach ($response['mappingIds'] as $mappingId)
				{
					$mapping = \TicketMapping::find($mappingId);
					$ticketId = $mapping->ticket->id;

					if ($iCount == 0)
					{
						$imageLinks = $this->uploadEnquiryImages($ticketId, $mappingId, $images);
					}
					elseif ($iCount >= 1 && count($imageLinks))
					{
						$this->uploadEnquiryImages($ticketId, $mappingId, null, $imageLinks);
					}

					$iCount++;
				}
			}

			return response()->json($response);

		} catch (\Exception $exception)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}
}