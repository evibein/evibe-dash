<?php

namespace App\Http\Controllers\Availability;

use Evibe\Facades\AppUtilFacade as AppUtil;
use App\Http\Controllers\Base\BaseAvailabilityController;
use App\Http\Requests;
use App\Models\CheckAvailability;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;

class VenueAvailabilityController extends BaseAvailabilityController
{
	// venue new availability
	public function availabilityLiveCheckList()
	{
		$checkFor = config('evibe.ticket_type.venues');
		$searchQuery = Input::has('query') ? Input::get('query') : null;
		$tickets = $this->getTicketsList($checkFor, true, $searchQuery);

		return view('availability.venue.live_availability_list', ['tickets' => $tickets]);
	}

	// venue old availability
	public function availabilityArchiveList()
	{
		$checkFor = config('evibe.ticket_type.venues');
		$searchQuery = Input::has('query') ? Input::get('query') : null;
		$tickets = $this->getTicketsList($checkFor, false, $searchQuery);

		return view('availability.venue.archive_availability_list', ['tickets' => $tickets]);
	}

	// enquiry details page
	public function liveAvailabilityDetails($ticketId)
	{
		$ticket = \Ticket::with('mappings')->findOrFail($ticketId);
		$liveAvailability = CheckAvailability::where('ticket_id', $ticketId)->get();
		$data = [
			'ticket'           => $ticket,
			'liveAvailability' => $liveAvailability,
		];
		if ($ticket)
		{
			return view('availability.venue.live_availability_details', ['data' => $data]);
		}
		else
		{
			return "No venue found for availability check , contact Tech Team";
		}
	}

	public function archiveAvailabilityDetails($ticketId)
	{
		$ticket = \Ticket::with('mappings')->findOrFail($ticketId);
		$archiveAvailability = CheckAvailability::where('ticket_id', $ticketId)->whereNotNull('respond_at')->get();
		$data = [
			'ticket'              => $ticket,
			'archiveAvailability' => $archiveAvailability,
		];
		if ($ticket)
		{
			return view('availability.venue.archive_availability_details', ['data' => $data]);
		}
		else
		{
			return "No venue found for availability check , contact Tech Team";
		}
	}

	// delete availability check of thw whole ticket
	public function deleteAvailabilityCheck($ticketId)
	{
		$deleteAvailability = CheckAvailability::where('ticket_id', $ticketId)->delete();
		if ($deleteAvailability)
		{
			return redirect()->back();
		}
	}

	// confirm the availability of venue
	public function loadVenueConfirmAvailability()
	{
		$data = [
			'success'   => false,
			'venueData' => []
		];

		$selectedVenues = Input::get('selection');

		foreach ($selectedVenues as $selectedVenue)
		{
			$venueAvlCheck = CheckAvailability::find($selectedVenue['enqId']);
			array_push($data['venueData'],
			           [
				           'name'        => $venueAvlCheck->hall->venue->name,
				           'code'        => $venueAvlCheck->hall->code,
				           'link'        => route('venues.hall', $venueAvlCheck->hall->id),
				           'ticketMapId' => $venueAvlCheck->ticket_mapping_id,
				           'enqId'       => $venueAvlCheck->id
			           ]);
			$data['success'] = true;
		}

		return json_encode($data);

	}

	public function confirmVenueAvailability($ticketId)
	{
		$res = ['success' => false];
		$venueEmailData = [];
		$ticket = \Ticket::find($ticketId);
		$selectedVenues = Input::get('selectedVenue');
		$venueCount = 0;

		//validation for all asked availability
		foreach ($selectedVenues as $selectedVenue)
		{
			$venueAvailability = CheckAvailability::find($selectedVenue['enqId']);

			$date1 = $selectedVenue['date1'] == 'true' ? $ticket->venue_date_1 : null;
			$date2 = $selectedVenue['date2'] ? ($selectedVenue['date2'] == 'true' ? $ticket->venue_date_2 : null) : null;
			$date3 = $selectedVenue['date3'] ? ($selectedVenue['date3'] == 'true' ? $ticket->venue_date_3 : null) : null;

			$rules = [
				'date'           => '',
				'tax'            => 'required|numeric',
				'pricePerPerson' => 'numeric'
			];
			$data = [
				'date'           => '',
				'tax'            => $selectedVenue['tax'],
				'pricePerPerson' => $selectedVenue['pricePerPerson']
			];
			$messages = [
				'tax.required'           => 'Please enter the tax percentage for ' . $venueAvailability->hall->code . '( if N/A enter 0 )',
				'tax.numeric'            => 'Tax field is mandatory for hall ' . $venueAvailability->hall->code,
				'pricePerPerson.numeric' => 'Price / Person for ' . $venueAvailability->hall->code . ' is invalid',
				'Please check at least one available date slot for ' . $venueAvailability->hall->code
			];

			if ($selectedVenue['date1'] || $selectedVenue['date2'] || $selectedVenue['date3'])
			{
				$checkDate1 = 'unCheck';
				$checkDate2 = 'unCheck';
				$checkDate3 = 'unCheck';
				if (!is_null($date1))
				{
					$checkDate1 = $selectedVenue['date1'] == 'true' ? 'check' : 'unCheck';
				}
				elseif (!is_null($date2))
				{
					$checkDate2 = $selectedVenue['date2'] == 'true' ? 'check' : 'unCheck';
				}
				elseif (!is_null($date3))
				{
					$checkDate3 = $selectedVenue['date3'] == 'true' ? 'check' : 'unCheck';
				}

				if (($checkDate1 == 'unCheck') && ($checkDate2 == 'unCheck') && ($checkDate3 == 'unCheck'))
				{
					array_set($rules, 'date', 'required');
					array_set($data, 'date', $date1);
				}
			}

			$validation = Validator::make($data, $rules, $messages);
			if ($validation->fails())
			{
				$res = [
					'success' => false,
					'error'   => $validation->messages()->first()
				];

				return response()->json($res);
			}
			else
			{
				$res = ['success' => true];
			}

		}
		if ($res['success'] == true)
		{
			foreach ($selectedVenues as $selectedVenue)
			{
				$venueAvailability = CheckAvailability::find($selectedVenue['enqId']);
				$date1 = $selectedVenue['date1'] == 'true' ? $ticket->venue_date_1 : null;
				$date2 = $selectedVenue['date2'] ? ($selectedVenue['date2'] == 'true' ? $ticket->venue_date_2 : null) : null;
				$date3 = $selectedVenue['date3'] ? ($selectedVenue['date3'] == 'true' ? $ticket->venue_date_3 : null) : null;
				$slot1 = $date1 ? $ticket->venue_slot_1 : null;
				$slot2 = $date2 ? $ticket->venue_slot_2 : null;
				$slot3 = $date3 ? $ticket->venue_slot_3 : null;
				$exchangeContact = $selectedVenue['exchangeContact'] == "true" ? 1 : 0;
				$venueAvailabilityData = [
					'checked_by'       => Auth::user()->id,
					'is_available'     => 1,
					'respond_at'       => time(),
					'tax'              => $selectedVenue['tax'],
					'price_per_person' => $selectedVenue['pricePerPerson'],
					'date_1'           => $date1,
					'slot_1'           => $slot1,
					'date_2'           => $date2,
					'slot_2'           => $slot2,
					'date_3'           => $date3,
					'slot_3'           => $slot3,
					'note'             => $selectedVenue['comments']
				];

				if ($exchangeContact == 1)
				{
					$this->exchangeVenueContact($selectedVenue['ticketMapId']);
				}

				if ($venueAvailability->update($venueAvailabilityData))
				{
					$venueEmailData[] = [
						'venue'            => $venueAvailability->hall->venue->name,
						'code'             => $venueAvailability->hall->code,
						'available'        => 1,
						'date1'            => $date1 ? AppUtil::formatDateTimeForBooking($date1) : null,
						'slot1'            => AppUtil::getSlot($slot1),
						'date2'            => $date2 ? AppUtil::formatDateTimeForBooking($date2) : null,
						'slot2'            => AppUtil::getSlot($slot2),
						'date3'            => $date3 ? AppUtil::formatDateTimeForBooking($date3) : null,
						'slot3'            => AppUtil::getSlot($slot3),
						'link'             => route('venues.hall', $venueAvailability->hall->id),
						'tax'              => $selectedVenue['tax'],
						'pricePerPerson'   => $selectedVenue['pricePerPerson'],
						'contactExchanged' => $exchangeContact,
						'comments'         => $selectedVenue['comments']
					];

					$res = ['success' => true];
				}
				$venueCount++;
			}
		}
		if ($res['success'] == true)
		{
			if ($venueCount == 1)
			{
				$emailSubject = $venueEmailData[0]['venue'] . ' is available for ' . $ticket->name . '\'s party';
			}
			else
			{
				$emailSubject = '[' . $venueCount . '] venue(s) are available for ' . $ticket->name . '\'s party';
			}

			$emailData = [
				'to'          => [config('evibe.enquiry_group_email')],
				'name'        => $ticket->name,
				'phone'       => $ticket->phone,
				'email'       => $ticket->email,
				'sub'         => $emailSubject,
				'ccAddresses' => [config('evibe.business_group_email')],
				'replyTo'     => [config('evibe.business_group_email')],
				'ticketLink'  => route('ticket.details.mappings', $ticket->id),
				'enquiryId'   => $ticket->enquiry_id,
				'venueData'   => $venueEmailData,
				'bdName'      => AppUtil::getHandlerName(Auth::user()->username),
			];

			$resEmail = $this->validateEmail($emailData['to'], $ticket, "CRM");

			if (!$resEmail['success'])
			{
				return response()->json($resEmail);
			}

			Queue::push('\Evibe\Utilities\SendEmail@mailVenueAvailabilityConfirm', $emailData);

			//update ticket action
			$hallCode = $this->groupHallCode($venueEmailData);
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.followup'),
				'comments' => 'Venue Availability Checked and marked as available for venue(s) :' . $hallCode
			];
			$this->updateTicketAction($ticketUpdate, false);
			$res['success'] = true;
		}

		return response()->json($res);
	}

	// Load Deny Venue Availability Modal
	public function loadVenueDenyAvailability()
	{
		$data = [
			'success'          => false,
			'availabilityData' => []
		];
		$selectedVenues = Input::get('selectData');

		foreach ($selectedVenues as $selectedVenue)
		{
			$venueAvlCheck = CheckAvailability::find($selectedVenue['enqId']);
			array_push($data['availabilityData'],
			           [
				           'name'        => $venueAvlCheck->hall->venue->name,
				           'code'        => $venueAvlCheck->hall->code,
				           'link'        => route('venues.hall', $venueAvlCheck->hall->id),
				           'ticketMapId' => $venueAvlCheck->ticket_mapping_id,
				           'enqId'       => $venueAvlCheck->id
			           ]);
			$data['success'] = true;
		}

		return json_encode($data);

	}

	public function denyVenueAvailability($ticketId)
	{
		$venueCount = 0;
		$ticket = \Ticket::find($ticketId);
		$venueEmailData = [];
		$availabilityDeny = Input::get('venueData');

		foreach ($availabilityDeny as $denyAvailability)
		{
			$availability = CheckAvailability::find($denyAvailability['enqId']);
			$availabilityDenyData = [
				'note'         => $denyAvailability['comments'],
				'respond_at'   => time(),
				'checked_by'   => Auth::user()->id,
				'is_available' => 0
			];

			if ($availability->update($availabilityDenyData))
			{
				$venueEmailData[] = [
					'venue'    => $availability->hall->venue->name,
					'code'     => $availability->hall->code,
					'comments' => $denyAvailability['comments'],
					'link'     => route('venues.hall', $availability->hall->id),
				];
				$res = ['success' => true];
			}
			$venueCount++;
		}
		if ($venueCount == 1)
		{
			$emailSubject = $venueEmailData[0]['venue'] . " is not available for " . $ticket->name . "'s party";
		}
		else
		{
			$emailSubject = "[" . $venueCount . "] venue(s) are not available for " . $ticket->name . "'s party";
		}
		$emailData = [
			'to'          => [config('evibe.enquiry_group_email')],
			'name'        => $ticket->name,
			'phone'       => $ticket->phone,
			'email'       => $ticket->email,
			'sub'         => $emailSubject,
			'ccAddresses' => [config('evibe.business_group_email')],
			'replyTo'     => [config('evibe.business_group_email')],
			'venueData'   => $venueEmailData,
			'bdName'      => AppUtil::getHandlerName(Auth::user()->username),
			'ticketLink'  => route('ticket.details.mappings', $ticket->id),
			'enquiryId'   => $ticket->enquiry_id,
			'venueCount'  => $venueCount
		];

		$resEmail = $this->validateEmail($emailData['to'], $ticket, "CRM");

		if (!$resEmail['success'])
		{
			return response()->json($resEmail);
		}

		Queue::push('\Evibe\Utilities\SendEmail@mailVenueAvailabilityDeny', $emailData);

		$res['success'] = true;

		//update ticket action
		$hallCode = $this->groupHallCode($venueEmailData);
		$ticketUpdate = [
			'ticket'   => $ticket,
			'statusId' => config('evibe.status.followup'),
			'comments' => 'Venue Availability checked and marked as unavailable for venue(s) : ' . $hallCode
		];
		$this->updateTicketAction($ticketUpdate, false);

		return response()->json($res);

	}
}
