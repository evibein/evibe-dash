<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use App\Models\TypeVenueGallery;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeVenueGalleryController extends BaseContentController
{
	public function showTypeVenueGalleryList()
	{
		$allTypeGalleryView = TypeVenueGallery::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_venue_gallery', compact('allTypeGalleryView'));
	}

	public function addTypeVenueGallery()
	{
		$res = ['success' => false];
		$galleryViewName = Input::get('typeVenueGallery');
		$isPublic = Input::get('isPublic') == "true" ? 1 : 0;
		$rules = ['typeVenueGallery' => 'required'];
		$messages = ['typeVenueGallery.required' => 'Type venue gallery name filed is required'];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$saveTypeGalleryView = TypeVenueGallery::create([
			                                                'name'      => $galleryViewName,
			                                                'is_public' => $isPublic
		                                                ]);

		if ($saveTypeGalleryView)
		{
			$res = ['success' => true];
		}

		return response()->json($res);

	}

	public function deleteTypeVenueGallery($id)
	{
		$typeGalleryView = TypeVenueGallery::findOrFail($id);

		if ($typeGalleryView->delete())
		{
			return redirect()->back();
		}
	}
}
