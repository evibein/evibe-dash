<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use App\Models\TypeSlot;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeSlotController extends BaseContentController
{
	public function showTypeSlotList()
	{
		$allSlots = TypeSlot::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_slot', compact('allSlots'));
	}

	public function addSlotEvent()
	{
		$res = ['success' => false];
		$slotName = Input::get('typeSlot');
		$rules = ['typeSlot' => 'required'];
		$messages = ['typeSlot.required' => 'Type slot name filed is required'];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$saveTypeSlot = TypeSlot::create([
			                                 'name' => $slotName,
		                                 ]);

		if ($saveTypeSlot)
		{
			$res = ['success' => true];
		}

		return response()->json($res);

	}

	public function deleteSlotEvent($id)
	{
		$typeSlot = TypeSlot::findOrFail($id);

		if ($typeSlot->delete())
		{
			return redirect()->back();
		}
	}
}
