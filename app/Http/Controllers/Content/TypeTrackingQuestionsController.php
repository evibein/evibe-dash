<?php

namespace App\Http\Controllers\Content;

use App\Models\Types\TypeTrackingQuestions;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class TypeTrackingQuestionsController extends Controller
{
	public function showList()
	{
		$trackQuestions = TypeTrackingQuestions::select("id", "question","is_partner","is_customer")->orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_track_questions', compact('trackQuestions'));
	}

	public function addTrackingQuestion(Request $request)
	{
		$res = ['success' => false];
		$question = trim($request->input('trackQuestion'));
		$questionType = $request->input('trackQuestionType');

		$rules = ['trackQuestion' => 'required'];
		$messages = ['trackQuestion.required' => 'tracking Question is required'];

		$validator = validator($request->all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$isPartner = null;
		$isCustomer = null;
		if ($questionType == 1)
		{
			$isPartner = 1;
		}
		else
		{
			$isCustomer = 1;
		}

		$saveTrackingQuestion = TypeTrackingQuestions::create([
			                                                      'question'    => $question,
			                                                      'is_customer' => $isCustomer,
			                                                      'is_partner'  => $isPartner
		                                                      ]);

		if ($saveTrackingQuestion)
		{
			return response()->json(['success' => true]);
		}

		return response()->json($res);
	}

	public function deleteTrackingQuestion($id)
	{
		$trackQuestion = TypeTrackingQuestions::find($id);

		if ($trackQuestion && $trackQuestion->delete())
		{
			return redirect()->back();
		}
		return redirect()->back();
	}
}
