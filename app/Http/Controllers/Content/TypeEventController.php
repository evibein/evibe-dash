<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use App\Models\TypeEvent;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeEventController extends BaseContentController
{
	public function showTypeEventList()
	{
		$allEvents = TypeEvent::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_event', compact('allEvents'));
	}

	public function addTypeEvent()
	{
		$res = ['success' => false];
		$eventName = Input::get('typeEvent');
		$rules = ['typeEvent' => 'required'];
		$messages = ['typeEvent.required' => 'Type event name filed is required'];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$saveTypeEvent = TypeEvent::create([
			                                   'name' => $eventName,
			                                   'url'  => $this->generateTypeUrl($eventName)
		                                   ]);

		if ($saveTypeEvent)
		{
			$res = ['success' => true];
		}

		return response()->json($res);

	}

	public function deleteTypeEvent($id)
	{
		$typeEvent = TypeEvent::findOrFail($id);

		if ($typeEvent->delete())
		{
			return redirect()->back();
		}
	}
}
