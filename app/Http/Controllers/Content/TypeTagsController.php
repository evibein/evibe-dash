<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use App\Models\TypeEvent;
use App\Models\Types\TypeTicketBooking;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeTagsController extends BaseContentController
{
	public function showTagsList()
	{

		$tags = \Tags::whereNull('deleted_at')->orderBy('created_at', 'DESC')->paginate(15);

		return view('content.tags.list', compact('tags'));
	}

	public function createTags()
	{
		$data = $this->getTagsInformation();
		session()->forget('successMsg');
		session()->forget('errorMsg');

		return view('content.tags.add')->with($data);
	}

	public function saveTags()
	{
		$parentId = null;
		$isParent = 0;
		$isFilter = is_null(Input::get('isFilter')) ? 0 : 1;

		$rules = [
			'occasion'    => 'integer|min:-1',
			'type'        => 'required|integer|min:1',
			'typeBooking' => 'integer|min:-1'
		];

		$messages = [
			'occasion.min'        => 'Please select a valid occasion type',
			'type.required'       => 'Please select a page type',
			'type.min'            => 'Please select a valid page type',
			'typeBooking.min'     => 'Please select a valid booking type',
			'parentTags.required' => 'Please select parent tag',
			'parentTags.min'      => 'Please select a valid parent tag',
			'tagName.required'    => 'Please enter tag name',
			'seoTitle.required'   => 'SEO Title is required when is filter is checked',
			'seoDesc.required'    => 'SEO Desc is required when is filter is checked'
		];

		if (!is_null(Input::get('isParent')))
		{
			$rules['parentTags'] = 'required|integer|min:1';
			$parentId = Input::get('parentTags');
			$isParent = 1;
		}

		if ($isFilter == 1)
		{
			$rules['seoTitle'] = 'required';
			$rules['seoDesc'] = 'required';
		}
		$rules['tagName'] = 'required';
		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			$allInput = Input::all();
			$allInput['isParent'] = $isParent;
			$allInput['isFilter'] = $isFilter;

			return redirect()->back()
			                 ->withInput($allInput)
			                 ->withErrors($validator);
		}
		$identifier = trim(Input::get('identifier'));
		$url = $identifier ? $identifier : Input::get('tagName');
		$url = $this->generateTypeUrl($url);
		$occasion = Input::get('occasion') > 0 ? Input::get('occasion') : null;
		$typeBooking = Input::get('typeBooking') > 0 ? Input::get('typeBooking') : null;

		\Tags::create([
			              'type_event'      => $occasion,
			              'map_type_id'     => Input::get('type'),
			              'is_filter'       => $isFilter,
			              'seo_title'       => Input::get('seoTitle'),
			              'seo_desc'        => Input::get('seoDesc'),
			              'parent_id'       => $parentId,
			              'url'             => $url,
			              'name'            => Input::get('tagName'),
			              'identifier'      => $identifier,
			              'type_booking_id' => $typeBooking
		              ]);

		return redirect()->route('content.tags.list')->with('successMsg', 'Tags added successfully');
	}

	public function editTags($tagId)
	{
		$tag = \Tags::find($tagId);
		if (!$tag)
		{
			return redirect()->back()->with('errorMsg', 'Couldn\'t edit tag, some error occurred. please try again');
		}

		$data = $this->getTagsInformation();
		$data = array_merge($data, ['tag' => $tag]);

		session()->forget('successMsg');
		session()->forget('errorMsg');

		return view('content.tags.edit')->with($data);

	}

	public function saveEditedTags($tagId)
	{
		$parentId = null;
		$isParent = 0;
		$isFilter = is_null(Input::get('isFilter')) ? 0 : 1;

		$rules = [
			'occasion'    => 'integer|min:-1',
			'type'        => 'required|integer|min:1',
			'typeBooking' => 'integer|min:-1'
		];

		$messages = [
			'occasion.min'        => 'Please select a valid occasion type',
			'type.required'       => 'Please select a page type',
			'type.min'            => 'Please select a valid page type',
			'typeBooking.min'     => 'Please select a valid booking type',
			'parentTags.required' => 'Please select parent tag',
			'parentTags.min'      => 'Please select a valid parent tag',
			'tagName.required'    => 'Please enter tag name',
			'seoTitle.required'   => 'SEO Title is required when is filter is checked',
			'seoDesc.required'    => 'SEO Desc is required when is filter is checked'
		];

		if (!is_null(Input::get('isParent')))
		{
			$rules['parentTags'] = 'required|integer|min:1';
			$parentId = Input::get('parentTags');
			$isParent = 1;
		}

		if ($isFilter == 1)
		{
			$rules['seoTitle'] = 'required';
			$rules['seoDesc'] = 'required';
		}
		$rules['tagName'] = 'required';
		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			$allInput = Input::all();
			$allInput['isParent'] = $isParent;
			$allInput['isFilter'] = $isFilter;

			return redirect()->back()
			                 ->withInput($allInput)
			                 ->withErrors($validator);
		}
		$identifier = trim(Input::get('identifier'));
		$url = $identifier ? $identifier : Input::get('tagName');
		$url = $this->generateTypeUrl($url);
		$occasion = Input::get('occasion') > 0 ? Input::get('occasion') : null;
		$typeBooking = Input::get('typeBooking') > 0 ? Input::get('typeBooking') : null;

		\Tags::where('id', $tagId)->update([
			                                   'type_event'      => $occasion,
			                                   'map_type_id'     => Input::get('type'),
			                                   'is_filter'       => $isFilter,
			                                   'seo_title'       => Input::get('seoTitle'),
			                                   'seo_desc'        => Input::get('seoDesc'),
			                                   'parent_id'       => $parentId,
			                                   'url'             => $url,
			                                   'name'            => Input::get('tagName'),
			                                   'identifier'      => $identifier,
			                                   'type_booking_id' => $typeBooking
		                                   ]);

		return redirect()->route('content.tags.list')->with('successMsg', 'Tag edited successfully');
	}

	public function deleteTags($tagId)
	{
		$tag = \Tags::findOrFail($tagId);

		if ($tag->delete())
		{
			return redirect()->back()->with('successMsg', 'Tag deleted successfully');
		}
		else
		{
			return redirect()->back()->with('errorMsg', 'Couldn\'t delete tag, some error occurred. please try again');
		}
	}

	private function getTagsInformation()
	{
		$occasions = TypeEvent::all();
		$typePages = \TypeTicket::all();
		$parentTags = \Tags::whereNull('parent_id')->get();
		$typeBookings = TypeTicketBooking::all();

		$data = [
			'occasions'    => $occasions,
			'typePages'    => $typePages,
			'parentTags'   => $parentTags,
			'typeBookings' => $typeBookings
		];

		return $data;
	}

}
