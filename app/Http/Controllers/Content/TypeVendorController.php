<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use App\Models\Role;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeVendorController extends BaseContentController
{
	public function showTypeVendor()
	{

		$vendors = \TypeVendor::orderBy('created_at', 'DESC')->paginate(15);
		$roles = Role::whereIn('id', [config('evibe.roles.planner'), config('evibe.roles.artist'), config('evibe.roles.venue')])->get();

		return view('content.vendor', compact('vendors', 'roles'));
	}

	public function createTypeVendor()
	{
		$res = ['success' => false];
		$codeList = \TypeVendor::pluck('code');
		$rules = ['typeVendor' => 'required|min:3'];

		$messages = [
			'typeVendor.required' => 'Please Enter Vendor Type',
			'typeVendor.min'      => 'Type vendor cannot be less than 3 character'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);

		}
		$typeVendor = Input::get('typeVendor');

		$createVendorType = \TypeVendor::create([
			                                        'name'    => $typeVendor,
			                                        'code'    => $this->getUniqueCode($typeVendor, $codeList),
			                                        'role_id' => Input::get('role'),
			                                        'url'     => $this->generateTypeUrl($typeVendor)
		                                        ]);
		if ($createVendorType)
		{
			$res = ['success' => true];
		}

		return response()->json($res);
	}

	public function deleteTypeVendor($id)
	{
		$typeVendor = \TypeVendor::findOrFail($id);

		if ($typeVendor->delete())
		{
			return redirect()->back()->with('successMsg', 'Type vendor deleted successfully');
		}
		else
		{
			return redirect()->back()->with('errorMsg', 'Some error occurred, please try again');
		}
	}
}
