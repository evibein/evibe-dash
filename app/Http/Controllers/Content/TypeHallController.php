<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeHallController extends BaseContentController
{
	public function showTypeHalls()
	{
		$typeHalls = \TypeVenueHall::orderBy('updated_at', 'DESC');

		$typeHalls = $typeHalls->paginate(12);

		return view('content.halls', compact('typeHalls'));
	}

	public function addTypeHall()
	{
		$code = \TypeVenueHall::pluck('code');
		$res = ['success' => false];
		$hallType = Input::get('typeHall');
		$isFilter = Input::get('isFilter') == "true" ? 1 : 0;
		$identifier = Input::get('identifier');

		$rules = [
			'typeHall'   => 'required|min:3',
			'identifier' => 'required'
		];

		$messages = [
			'typeHall.required'   => 'Hall Type is required',
			'typeHall.min'        => 'Hall Type Name cannot be less than 3 characters',
			'identifier.required' => 'Identifier is required'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$newHall = \TypeVenueHall::create([
			                                  'name'       => $hallType,
			                                  'code'       => $this->getUniqueCode($hallType, $code),
			                                  'identifier' => $identifier,
			                                  'is_filter'  => $isFilter
		                                  ]);

		if ($newHall)
		{
			$res['success'] = true;
		}

		return response()->json($res);
	}

	public function deleteTypeHall($id)
	{
		$typeHall = \TypeVenueHall::findOrFail($id);

		if ($typeHall->delete())
		{
			return redirect()->back();
		}
	}
}
