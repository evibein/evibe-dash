<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use App\Models\TypePackageGallery;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypePackageGalleryController extends BaseContentController
{
	public function showTypePackageGalleryList()
	{
		$allTypePackageGallery = TypePackageGallery::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_package_gallery', compact('allTypePackageGallery'));
	}

	public function addTypePackageGallery()
	{
		$res = ['success' => false];
		$packageGalleryType = Input::get('typePackageGallery');
		$rules = ['typePackageGallery' => 'required'];
		$messages = ['typePackageGallery.required' => 'Type package gallery  name filed is required'];
		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$saveTypeMenuView = TypePackageGallery::create([
			                                               'category_name' => $packageGalleryType,
		                                               ]);

		if ($saveTypeMenuView)
		{
			$res = ['success' => true];
		}

		return response()->json($res);
	}

	public function deleteTypePackageGallery($id)
	{
		$packageGalleryType = TypePackageGallery::findOrFail($id);

		if ($packageGalleryType->delete())
		{
			return redirect()->back();
		}
	}
}
