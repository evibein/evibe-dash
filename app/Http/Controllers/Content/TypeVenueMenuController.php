<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;
use App\Models\TypeVenueMenu;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeVenueMenuController extends BaseContentController
{
	public function showTypeVenueMenuList()
	{
		$allTypeVenueMenu = TypeVenueMenu::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_venue_menu', compact('allTypeVenueMenu'));
	}

	public function addTypeVenueMenu()
	{
		$res = ['success' => false];
		$venueMenuName = Input::get('typeVenueMenu');
		$rules = ['typeVenueMenu' => 'required'];
		$messages = ['typeVenueMenu.required' => 'Type venue menu name filed is required'];
		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$saveTypeMenuView = TypeVenueMenu::create([
			                                          'name' => $venueMenuName,
		                                          ]);

		if ($saveTypeMenuView)
		{
			$res = ['success' => true];
		}

		return response()->json($res);

	}

	public function deleteTypeVenueMenu($id)
	{
		$typeVenueMenu = TypeVenueMenu::findOrFail($id);

		if ($typeVenueMenu->delete())
		{
			return redirect()->back();
		}
	}
}
