<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;
use App\Models\TypeFacts;
use App\Models\TypePrerequisite;
use Carbon\Carbon;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class PrereqFactsTermsController extends BaseContentController
{
	// content of prerequisites, facts, terms.

	//Content tab
	public function showContent()
	{
		return redirect()->route('content.terms');
	}

	//terms list
	public function showTerms()
	{
		$mapType = 'terms';
		$terms = \BookingTerms::orderBy('created_at', 'DESC')->paginate(5);

		return view('content.terms', compact('mapType', 'terms'));
	}

	//prerequisites list
	public function showPrereq()
	{
		$mapType = 'prereq';
		$preReqs = TypePrerequisite::orderBy('created_at', 'DESC')->paginate(5);

		return view('content.prereq', compact('mapType', 'preReqs'));
	}

	//facts list
	public function showFacts()
	{
		$mapType = 'facts';
		$facts = TypeFacts::orderBy('created_at', 'DESC')->paginate(5);

		return view('content.facts', compact('mapType', 'facts'));
	}

	//adding content here
	public function addContent()
	{
		$name = Input::get('name');
		$value = Input::get('value');
		$mapType = Input::get('mapType');

		if ($name && $value)
		{
			switch ($mapType)
			{
				case 'terms':
					$model = new \BookingTerms;
					$response = true;
					break;
				case 'prereq':
					$model = new TypePrerequisite;
					$response = true;
					break;
				case 'facts':
					$model = new TypeFacts;
					$response = true;
					break;
				default:
					$response = false;
			}

			$model->create([
				               'name'       => $name,
				               'value'      => $value,
				               'created_at' => Carbon::now(),
				               'updated_at' => Carbon::now()
			               ]);

			$res = ['success' => $response];
		}
		else
		{
			$res = ['success' => false];
		}

		return response()->json($res);
	}

	//deleted content
	public function deleteContent($id, $mapType)
	{
		switch ($mapType)
		{
			case 'terms':
				$model = new \BookingTerms;
				break;
			case 'prereq':
				$model = new TypePrerequisite;
				break;
			case 'facts':
				$model = new TypeFacts;
				break;
		}
		$model->where('id', $id)->forceDelete();

		return redirect()->route('content.' . $mapType)->with('success', $mapType . ' deleted successfully');
	}

	//Edit Content

	public function editContent($id, $mapType)
	{
		$name = Input::get('name');
		$value = Input::get('value');
		switch ($mapType)
		{
			case 'terms':
				$model = \BookingTerms::find($id);
				$response = true;
				break;
			case 'prereq':
				$model = TypePrerequisite::find($id);
				$response = true;
				break;
			case 'facts':
				$model = TypeFacts::find($id);
				$response = true;
				break;
			default:
				$response = false;
		}

		$model->update([
			               'name'       => $name,
			               'value'      => $value,
			               'updated_at' => Carbon::now()
		               ]);

		$res = ['success' => $response];

		return response()->json($res);
	}
}
