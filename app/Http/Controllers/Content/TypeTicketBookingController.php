<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Models\Types\TypeTicketBooking;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Mockery\CountValidator\Exception;

class TypeTicketBookingController extends BaseContentController
{
	public function showList()
	{
		$allTypeBookings = TypeTicketBooking::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_ticket_booking', compact('allTypeBookings'));

	}

	public function addTypeTicketBookingField()
	{
		$res = ['success' => false];
		$typeTicketBooking = Input::get('typeTicketBooking');
		$forAutoBooking = Input::get('forAutoBooking');
		$rules = ['typeTicketBooking' => 'required'];
		$messages = ['typeTicketBooking.required' => 'Ticket booking type filed is required'];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$saveTypeTicketBooking = TypeTicketBooking::create([
			                                                   'name'             => $typeTicketBooking,
			                                                   'for_auto_booking' => $forAutoBooking
		                                                   ]);

		if ($saveTypeTicketBooking)
		{
			$res = ['success' => true];
		}

		return response()->json($res);

	}

	public function deleteTypeTicketBookingField($id)
	{
		$typeTicketBooking = TypeTicketBooking::findOrFail($id);

		if ($typeTicketBooking->delete())
		{
			return redirect()->back();
		}
	}

	public function getTypeTicketBookingsTable()
	{
		try
		{
			$typeTicketBookings = TypeTicketBooking::with('checkoutFields')
			                                       ->get();
			$abTypeTicketBookings = $typeTicketBookings->where('for_auto_booking', 1);
			$normalTypeTicketBookings = $typeTicketBookings->where('for_auto_booking', 0);

			$data = [
				'typeTicketBookings' => $typeTicketBookings,
				'abTtbCount'         => $abTypeTicketBookings->count(),
				'normalTtbCount'     => $normalTypeTicketBookings->count()
			];

			return view('content/type_booking_cf_table', $data);

		} catch (Exception $exception)
		{
			Log::error($exception->getMessage());
		}
	}
}
