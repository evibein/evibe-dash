<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeTicketSourceController extends BaseContentController
{
	public function showTicketSourceList()
	{
		$allSource = \TypeTicketSource::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_source', compact('allSource'));

	}

	public function addTypeSource()
	{
		$res = ['success' => false];
		$typeSource = Input::get('typeSource');

		$rules = ['typeSource' => 'required'];
		$messages = ['typeSource.required' => 'Type Source is required'];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		if (\TypeTicketSource::create(['name' => $typeSource]))
		{
			$res = ['success' => true];
		}

		return response()->json($res);
	}

	public function deleteTypeSource($id)
	{
		$source = \TypeTicketSource::findOrFail($id);

		if ($source->delete())
		{
			return redirect()->back();
		}
	}
}
