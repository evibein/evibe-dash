<?php

namespace App\Http\Controllers\Content;

use App\Models\Util\TypePackageFieldCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypePackageFieldCategoryController extends Controller
{
	public function showList()
	{
		$typeCategoryFields = TypePackageFieldCategory::paginate(15);

		return view('content.type_package_field_cat', compact('typeCategoryFields'));
	}

	public function addCategoryField(Request $request)
	{
		$res = ['success' => false];
		$name = $request->input('catField');

		$newField = TypePackageFieldCategory::create(['name' => $name]);

		if ($newField)
		{
			$res = ['success' => true];
		}

		return response()->json($res);
	}

	public function deleteCategoryField($id)
	{
		$cateField = TypePackageFieldCategory::find($id);
		if ($cateField)
		{
			$cateField->delete();

			return redirect()->route('content.package.field.category.list');
		}
	}
}
