<?php

namespace App\Http\Controllers\Content;

use App\Models\Types\TypeCakeCategoryValues;
use App\Models\Types\TypeCakeFlavour;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeCakeFlavourController extends Controller
{
	public function showList()
	{
		$cakeFlavours = TypeCakeFlavour::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_cake_flavour', compact('cakeFlavours'));
	}

	public function addCakeFlavour(Request $request)
	{
		$res = ['success' => false];
		$flavour = trim($request->input('cakeFlavour'));
		$name = str_replace(" ", "_", $flavour);

		$rules = ['cakeFlavour' => 'required'];
		$messages = ['cakeFlavour.required' => 'Cake flavour name filed is required'];

		$validator = validator($request->all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$saveCakeFlavour = TypeCakeFlavour::create([
			                                           'name'       => $name,
			                                           'identifier' => $flavour
		                                           ]);

		if ($saveCakeFlavour)
		{
			// add a row for cake flavour in cake category value table also.
			TypeCakeCategoryValues::create(['name' => $flavour, 'type_cake_cat_id' => config('evibe.cake_cat_value.flavour')]);
			$res = ['success' => true];
		}

		return response()->json($res);

	}

	public function deleteCakeFlavour($id)
	{
		$cakeFlavour = TypeCakeFlavour::findOrFail($id);
		$identifier = $cakeFlavour->identifier;

		if ($cakeFlavour->delete())
		{
			TypeCakeCategoryValues::where('name', 'LIKE', "%$identifier%")->where('type_cake_cat_id', config('evibe.cake_cat_value_flavour_id'))->delete();

			return redirect()->back();
		}
	}
}
