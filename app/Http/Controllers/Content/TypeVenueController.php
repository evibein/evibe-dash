<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeVenueController extends BaseContentController
{
	public function showTypeVenues()
	{
		$typeVenues = \TypeVenue::orderBy('updated_at', 'DESC');
		$typeVenues = $typeVenues->paginate(15);

		return view('content.venues', compact('typeVenues'));
	}

	public function addTypeVenue()
	{
		$code = \TypeVenue::pluck('code');
		$res = ['success' => false];
		$venueType = Input::get('typeVenue');
		$isFilter = Input::get('isFilter') == "true" ? 1 : 0;
		$identifier = Input::get('identifier');

		$rules = [
			'typeVenue'  => 'required|min:3',
			'identifier' => 'required'
		];

		$messages = [
			'typeVenue.required'  => 'Venue Type is required',
			'typeVenue.min'       => 'Venue Type Name cannot be less than 3 characters',
			'identifier.required' => 'Identifier is required'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$newVenue = \TypeVenue::create([
			                               'name'       => $venueType,
			                               'code'       => $this->getUniqueCode($venueType, $code),
			                               'identifier' => $identifier,
			                               'is_filter'   => $isFilter
		                               ]);

		if ($newVenue)
		{
			$res['success'] = true;
		}

		return response()->json($res);
	}

	public function deleteTypeVenue($id)
	{
		$typeVenue = \TypeVenue::findOrFail($id);

		if ($typeVenue->delete())
		{
			return redirect()->back();
		}
	}
}
