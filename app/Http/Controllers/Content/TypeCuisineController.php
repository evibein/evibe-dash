<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Base\BaseContentController;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TypeCuisineController extends BaseContentController
{
	public function showTypeCuisineList()
	{
		$allCuisine = \TypeVenueCuisine::orderBy('updated_at', 'DESC')->paginate(15);

		return view('content.type_cuisine', compact('allCuisine'));
	}

	public function addTypeCuisine()
	{
		$res = ['success' => false];
		$cuisineName = Input::get('typeCuisine');
		$rules = ['typeCuisine' => 'required'];
		$messages = ['typeCuisine.required' => 'Type cuisine name filed is required'];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$saveTypeCuisine = \TypeVenueCuisine::create([
			                                          'name' => $cuisineName,
		                                          ]);

		if ($saveTypeCuisine)
		{
			$res = ['success' => true];
		}

		return response()->json($res);

	}

	public function deleteTypeCuisine($id)
	{
		$typeCuisine = \TypeVenueCuisine::findOrFail($id);

		if ($typeCuisine->delete())
		{
			return redirect()->back();
		}
	}
}
