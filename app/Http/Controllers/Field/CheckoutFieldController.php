<?php

namespace App\Http\Controllers\Field;

use App\Http\Controllers\Base\BaseController;

use App\Models\TypeEvent;
use App\Models\Types\TypeField;
use App\Models\Types\TypeTicketBooking;
use App\Models\Util\CheckoutField;
use Illuminate\Http\Request;

class CheckoutFieldController extends BaseController
{
	public function showList()
	{
		$checkoutFields = CheckoutField::orderBy('updated_at', 'DESC')->paginate(20);

		$data = [
			'checkoutFields' => $checkoutFields
		];

		return view('field.checkout.list')->with($data);
	}

	public function showAddField()
	{
		$validFieldTypes = [config('evibe.input.text'), config('evibe.input.textarea')];
		$typeFields = TypeField::whereIn('id', $validFieldTypes)->get();
		$typeEvents = TypeEvent::all();
		$typeBookings = TypeTicketBooking::all();

		$data = [
			'typeFields'   => $typeFields,
			'typeEvents'   => $typeEvents,
			'typeBookings' => $typeBookings
		];

		return view('field.checkout.add', $data);
	}

	public function saveField(Request $request)
	{
		$rules = [
			'occasionType' => 'integer',
			'fieldType'    => 'integer|min:1',
			'bookingType'  => 'integer|min:1',
			'name'         => 'required|alpha_dash|unique:checkout_field,name',
			'identifier'   => 'required',
		];

		$messages = [
			'occasionType.min'    => 'Please select a valid Event type.',
			'fieldType.min'       => 'Please select a valid Field Type',
			'bookingType.min'     => 'Please select a valid booking type',
			'name.required'       => 'Field name is required',
			'name.alpha_dash'     => 'No space is allowed between letters, use underscore in stead of space (Ex: birthday_cake).',
			'identifier.required' => 'Identifier is required',
			'name.unique'         => 'This field name is already in use, please try something else.'
		];

		$validator = validator()->make($request->all(), $rules, $messages);
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$checkoutField = CheckoutField::create([
			                                       'event_id'               => $request->input('occasionType') > 0 ? $request->input('occasionType') : null,
			                                       'type_ticket_booking_id' => $request->input('bookingType'),
			                                       'type_field_id'          => $request->input('fieldType'),
			                                       'name'                   => trim($request->input('name')),
			                                       'identifier'             => $request->input('identifier'),
			                                       'hint_message'           => $request->input('hintMessage'),
			                                       'is_crm'                 => $request->input('isCrm'),
			                                       'is_auto_booking'        => $request->input('isAutoBook')
		                                       ]);
		if ($checkoutField)
		{
			return redirect()->route('field.checkout.list')
			                 ->with('successMsg', 'New checkout field added successfully');
		}
	}

	public function deleteField($id)
	{
		$checkoutField = CheckoutField::find($id);
		if ($checkoutField && $checkoutField->delete())
		{
			return redirect()->back()->with('successMsg', 'Checkout field deleted successfully');
		}
		else
		{
			return "<h4>Couldn't Delete, Some error occurred</h4>";
		}
	}
}
