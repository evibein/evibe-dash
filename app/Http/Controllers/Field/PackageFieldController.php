<?php

namespace App\Http\Controllers\Field;

use App\Models\TypeEvent;
use App\Models\Types\TypeField;
use App\Models\Util\PackageFieldValue;
use App\Models\Util\TypePackageField;
use App\Models\Util\TypePackageFieldCategory;
use App\Models\Util\TypePackageFieldValue;
use App\Http\Controllers\Base\BaseController;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageFieldController extends BaseController
{
	public function showFieldList()
	{
		$typePackage = TypePackageField::orderBy('updated_at', 'DESC');
		$typeEvents = TypeEvent::all();
		$data = [
			'typePackageField' => $typePackage->paginate(15),
			'typeEvents'       => $typeEvents
		];

		return view('field.package.list')->with($data);
	}

	public function showAddField(Request $request)
	{
		$occasion = $request->has('occasion') ? $request->input('occasion') : false;
		if ($occasion)
		{
			$typeFields = TypeField::all();
			$typeFieldCat = TypePackageFieldCategory::all();

			$data = [
				'typeFields'   => $typeFields,
				'typeFieldCat' => $typeFieldCat
			];

			return view('field.package.add')->with($data);
		}
		else
		{
			return "You cannot add a package field without selecting occasion, <a href=" . route('field.package.list') . ">click here</a> to go back";
		}
	}

	public function saveFieldDetails(Request $request)
	{
		$res['success'] = false;
		$name = trim($request->input('name'));
		$fieldType = $request->input('fieldType');
		$validations = $request->input('validation');
		$childs = $request->input('childs');

		$validationRules = $this->collectValidationData($validations, $request);
		//save the value of field

		$formattedName = $this->getFormattedname($name);

		$packageFieldData = [
			'name'          => $formattedName,
			'is_live'       => 0,
			'meta_name'     => $name,
			'occasion_id'   => $request->input('occasion'),
			'type_field_id' => $fieldType,
			'validation'    => $validationRules,
			'placeholder'   => trim($request->input('placeholder')),
			'default_value' => trim($request->input('defaultValue')),
			'type_cat_id'   => $request->input('fieldCategory')
		];

		$packageField = TypePackageField::create($packageFieldData);

		if ($packageField)
		{
			$res = ['success' => true];
		}
		else
		{
			$res['error'] = "Error while saving the messages";
		}

		//save child Field value
		if ($packageField && ($fieldType == config('evibe.input.checkbox') || $fieldType == config('evibe.input.option')))
		{
			$packageFieldValueCount = 0;
			foreach ($childs as $child)
			{
				TypePackageFieldValue::create([
					                              'type_package_field_id' => $packageField->id,
					                              'value'                 => trim($child['name'])
				                              ]);
				$packageFieldValueCount++;
			}

			if (count($childs) == $packageFieldValueCount)
			{
				$res = ['success' => true];
			}
			else
			{
				$res['custom_message'] = 'Error while saving child field value';
				TypePackageFieldValue::where('type_package_field_id', $packageField->id)->forceDelete();
			}
		}
		else
		{
			$res = ['success' => true,];
		}

		return response()->json($res);
	}

	public function showEditField($fieldId)
	{
		$packageField = TypePackageField::findOrFail($fieldId);
		$typeFieldCat = TypePackageFieldCategory::all();

		$data = [
			'packageField' => $packageField,
			'typeFieldCat' => $typeFieldCat
		];

		return view('field.package.edit')->with($data);
	}

	//update the value
	public function updateFieldDetails($fieldId, Request $request)
	{
		$res = ['success' => false];
		$packageUpdate = false;
		$packageField = TypePackageField::findOrfail($fieldId);
		$validations = $request->input('validation');
		$validateData = $this->collectValidationData($validations, $request);
		$metaName = trim($request->input('name'));
		$formattedName = $this->getFormattedName($metaName);

		$updateData = [
			'name'          => $formattedName,
			'meta_name'     => $metaName,
			'validation'    => $validateData,
			'placeholder'   => trim($request->input('placeholder')),
			'default_value' => trim($request->input('defaultValue')),
			'type_cat_id'   => $request->input('fieldCategory')
		];
		//update the packages
		if ($packageField->update($updateData))
		{
			$packageUpdate = true;
			if ($packageUpdate)
			{
				$res = ['success' => true];
			}
		}

		if ($packageUpdate && ($packageField->type_field_id == config('evibe.input.checkbox') || $packageField->type_field_id == config('evibe.input.option')))
		{
			$childCount = 0;
			$childs = $request->input('childs');
			if (count($childs) > 0)
			{
				foreach ($childs as $child)
				{
					if ($child['id'] == 'new')
					{
						TypePackageFieldValue::create([
							                              'type_package_field_id' => $fieldId,
							                              'value'                 => $child['name']
						                              ]);
					}
					else
					{
						$childObj = TypePackageFieldValue::find($child['id']);

						if ($childObj)
						{
							if ($child['name'] !== $childObj->value)
							{
								$childObj->update(['value' => $child['name']]);

								//update the corresponding field value of all packages

								$packageFieldValues = PackageFieldValue::where('meta_child_id', $child['id'])->get();

								foreach ($packageFieldValues as $packageFieldValue)
								{
									$packageFieldValue->update(['value' => $child['name']]);
								}
							}
						}
					}
					$childCount++;
				}
				if (count($childs) == $childCount)
				{
					$res['success'] = true;
				}
			}
		}

		return response()->json($res);
	}

	//delete the package field child for options and checkbox,
	public function deleteChild($childId)
	{
		$childField = TypePackageFieldValue::find($childId);
		$typePackageFieldId = $childField->type_package_field_id;
		$childFieldId = $childField->id;

		if ($childField && $childField->delete())
		{
			$packageFieldValues = PackageFieldValue::where('meta_child_id', $childFieldId)->get();

			$defaultValue = TypePackageFieldValue::where('type_package_field_id', $typePackageFieldId)->whereNull('deleted_at')->first();

			foreach ($packageFieldValues as $packageFieldValue)
			{
				$packageFieldValue->update(['value' => $defaultValue->value, 'meta_child_id' => $defaultValue->id]);
			}
		}

		return redirect()->route('field.package.list');
	}

	//Activate the package field
	public function activateField($id)
	{
		$packageField = TypePackageField::findOrfail($id);
		$packageField->update(['is_live' => 1]);

		return redirect()->route('field.package.list');
	}

	//InActive the package field
	public function inactivateField($id)
	{
		$packageField = TypePackageField::findOrfail($id);
		$packageField->update(['is_live' => 0]);

		return redirect()->route('field.package.list');
	}

	//reorder packages
	public function showReorderPackageField(Request $request)
	{
		$fieldCategories = TypePackageFieldCategory::all();
		$typeEvents = TypeEvent::all();
		$occasion = $request->input('occasion', 1);
		$fieldCat = $request->input('fieldCat', 1);

		$packageFields = TypePackageField::where(['occasion_id' => $occasion, 'type_cat_id' => $fieldCat])
		                                 ->orderBy(DB::raw('ISNULL(priority), priority'))
		                                 ->get();

		$data = [
			'fieldCategories' => $fieldCategories,
			'typeOccasion'    => $typeEvents,
			'packageFields'   => $packageFields
		];

		return view('field.package.reorder')->with($data);
	}

	public function reorderPackageField(Request $request)
	{
		$res = ['success' => false];
		$fieldIds = $request->input('sortableOrder');

		$priority = 1;

		foreach ($fieldIds as $fieldId)
		{
			$packageField = TypePackageField::find($fieldId);
			if ($packageField)
			{
				$packageField->update(['priority' => $priority]);
			}
			$priority++;
		}
		if ($priority = count($fieldIds) + 1)
		{
			$res = ['success' => true];
		}

		return response()->json($res);
	}

	private function collectValidationData($validationArray, $request)
	{
		$allValidation = '';
		$min = $request->input('min');
		$max = $request->input('max');
		if (count($validationArray) > 0)
		{
			foreach ($validationArray as $validation)
			{
				switch ($validation)
				{
					case config('evibe.validation.required.id'):
						$validation = config('evibe.validation.required.rules');
						break;

					case config('evibe.validation.numeric.id'):
						$validation = config('evibe.validation.numeric.rules');
						break;

					case config('evibe.validation.integer.id'):
						$validation = config('evibe.validation.integer.rules');
						break;

					case config('evibe.validation.min.id'):
						$validation = config('evibe.validation.min.rules') . $min;
						break;

					case config('evibe.validation.max.id'):
						$validation = config('evibe.validation.max.rules') . $max;
						break;

					case config('evibe.validation.phone.id'):
						$validation = config('evibe.validation.phone.rules');
						break;

					case config('evibe.validation.email.id');
						$validation = config('evibe.validation.email.rules');
						break;

				}
				$allValidation .= $validation . '|';

			}
			$formattedValidation = rtrim($allValidation, '|');

			return $formattedValidation;
		}

		return "";
	}

	private function getFormattedName($name)
	{
		$formattedName = preg_replace('/[^A-Za-z0-9\- ]/', '', strtolower($name));
		$formattedName = trim(preg_replace("/[ ]+/", "_", $formattedName), '_');

		return $formattedName;
	}
}
