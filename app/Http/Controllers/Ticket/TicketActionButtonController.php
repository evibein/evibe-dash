<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;
use App\Jobs\Emails\MailNoResponseToCust;
use App\Models\CheckAvailability;
use App\Models\Ticket\TicketPostBookingRequests;
use App\Models\TypeEvent;
use App\Models\Types\TypeCancellationReason;
use Carbon\Carbon;
use Evibe\Facades\AppUtilFacade as AppUtil;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;

class TicketActionButtonController extends BaseTicketController
{
	/**
	 * Send Custom Quote
	 *
	 * @author Anji <anji@evibe.in>
	 * @since  24 Aug 2015
	 */
	public function getDetailsForQuote($ticketId)
	{
		$selections = Input::get('selections');
		$data = [
			'success'          => true,
			'quoteMappings'    => [],
			'quoteSuggestions' => []
		];

		// get info for each selection of
		// get info for each selection of type = 2 (suggestion)
		foreach ($selections as $selection)
		{
			$action = (int)$selection['for'];

			if (!$action)
			{
				continue;
			}

			$values = AppUtil::fillMappingValues([
				                                     'mapTypeId'    => $selection['type'],
				                                     'mapId'        => $selection['id'],
				                                     'isCollection' => false
			                                     ]);

			$typeName = substr($values['type'], 0, -1); // remove plural "s"

			if ($action == 1) // for quote
			{
				array_push($data['quoteMappings'], [
					'map_type_id' => $selection['type'],
					'map_id'      => $selection['id'],
					'name'        => $values['name'],
					'code'        => $values['code'],
					'info'        => $values['info'],
					'price'       => $values['price'],
					'type'        => $typeName
				]);
			}

			if ($action == 2) // for suggestion
			{
				array_push($data['quoteSuggestions'], [
					'map_type_id' => $selection['type'],
					'map_id'      => $selection['id'],
					'name'        => $values['name'],
					'code'        => $values['code'],
					'link'        => $values['link'],
					'type'        => $typeName
				]);
			}
		}

		// @todo: save ticket quote details
		return json_encode($data);
	}

	public function sendCustomQuote($ticketId)
	{
		$ticket = \Ticket::with('handler')->findOrFail($ticketId);
		$rules = [
			'subject'  => 'required|min:5',
			'event_id' => 'required'
		];

		// get event_id from db itself
		$validateEventId = $ticket->event_id ? $ticket->event_id : "";
		$validateData = [
			'subject'  => Input::get('subject'),
			'event_id' => $validateEventId
		];

		$messages = [
			'subject.required'  => 'Email subject is required',
			'subject.min'       => 'Email subject should be minimum 5 characters long',
			'event_id.required' => 'Ticket occasion is required to send custom quote'
		];

		$validation = Validator::make($validateData, $rules, $messages);

		// validate email subject
		if ($validation->fails())
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $validation->messages()->first()
			                        ]);
		}
		else
		{
			$quoteMappings = Input::get('quotes');
			$quoteSuggestions = Input::get('suggestions');

			if (count($quoteMappings) > 0)
			{
				foreach ($quoteMappings as $quoteMapping)
				{
					$quoteRules = [
						'name'  => 'required|min:4',
						'info'  => 'required|min:4',
						'price' => 'required|min:1'
					];

					$quoteMessages = [
						'name.required'  => 'One of the quote mappings name is missing',
						'name.min'       => 'One of the quote mappings name is not valid (min 4 characters long)',
						'info.required'  => 'One of the quote mappings info is missing',
						'info.min'       => 'One of the quote mappings info is not valid (min 4 characters long)',
						'price.required' => 'One of the quote mappings price is missing',
						'price.min'      => 'One of the quote mappings price is not valid (range or single number)',
					];

					$quoteData = [
						'name'  => $quoteMapping['name'],
						'info'  => $quoteMapping['info'],
						'price' => $quoteMapping['price'],
					];

					$quoteValidation = Validator::make($quoteData, $quoteRules, $quoteMessages);

					// validate name, info, price
					if ($quoteValidation->fails())
					{
						return response()->json([
							                        'success'  => false,
							                        'errorMsg' => $quoteValidation->messages()->first()
						                        ]);
					}
				}
			}

			// all valid, save to database
			$emailData = [
				'subject'      => Input::get('subject'),
				'mailOpening'  => Input::get('mailOpening'),
				'mailEnding'   => Input::get('mailEnding'),
				'customerName' => ucfirst($ticket->name),
				'quotes'       => [],
				'suggestions'  => []
			];

			if (is_array($quoteMappings))
			{
				foreach ($quoteMappings as $quoteMapping)
				{
					$values = AppUtil::fillMappingValues([
						                                     'isCollection' => false,
						                                     'mapId'        => $quoteMapping['id'],
						                                     'mapTypeId'    => $quoteMapping['type']
					                                     ]);

					$insertData = [
						'name'        => $quoteMapping['name'],
						'info'        => $quoteMapping['info'],
						'price'       => $quoteMapping['price'],
						'map_id'      => $quoteMapping['id'],
						'map_type_id' => $quoteMapping['type'],
						'ticket_id'   => $ticketId,
						'handler_id'  => Auth::user()->id
					];

					\TicketQuote::create($insertData);

					$insertData['publicLink'] = $values['publicLink'];
					array_push($emailData['quotes'], $insertData);
				}
			}

			if (is_array($quoteSuggestions))
			{
				foreach ($quoteSuggestions as $quoteSuggestion)
				{
					$values = AppUtil::fillMappingValues([
						                                     'isCollection' => false,
						                                     'mapId'        => $quoteSuggestion['id'],
						                                     'mapTypeId'    => $quoteSuggestion['type']
					                                     ]);

					$insertData = [
						'name'          => ucfirst($values['name']),
						'info'          => $values['info'],
						'price'         => $values['price'],
						'map_id'        => $quoteSuggestion['id'],
						'map_type_id'   => $quoteSuggestion['type'],
						'ticket_id'     => $ticketId,
						'handler_id'    => Auth::user()->id,
						'is_suggestion' => 1
					];

					\TicketQuote::create($insertData);

					$insertData['publicLink'] = $values['publicLink'];
					array_push($emailData['suggestions'], $insertData);
				}
			}

			// send email
			$ccAddresses = [
				Auth::user()->username,
				config('evibe.enquiry_group_email')
			];
			if ($ticket->handler)
			{
				array_push($ccAddresses, $ticket->handler->username);
			}

			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email); // alt email in CC
			}

			$emailData['to'] = [$ticket->email];
			$emailData['ccAddresses'] = $ccAddresses;
			$emailData['handlerName'] = AppUtil::getHandlerName();

			$resEmail = $this->validateEmail($emailData['to'], $ticket, "Customer");
			if (!$resEmail['success'])
			{
				$resEmail['errorMsg'] = $resEmail['error'];

				return response()->json($resEmail);
			}

			Queue::push('\Evibe\Utilities\SendEmail@mailCustomQuote', $emailData);

			// send SMS
			$smsData = [
				'to'    => $ticket->phone,
				'name'  => ucfirst($ticket->name),
				'email' => $ticket->email
			];

			$resPhone = $this->validatePhone($smsData['to'], $ticket, "Customer");
			if ($resPhone['success'])
			{
				Queue::push('\Evibe\Utilities\SendSMS@smsCustomQuote', $smsData);
			}

			// ticket action update
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.followup'),
				'comments' => config('evibe.ticket_status_message.custom_quote')
			];
			$this->updateTicketAction($ticketUpdate);

			return response()->json(['success' => true]);
		}
	}
	/**
	 * Email methods
	 *
	 * Send recommendation email, booking confirmation email
	 * (or) just confirmation email. Payment reminders and feedback email
	 * are now automated.
	 *
	 */

	// Send recommendation email
	public function sendRecommendationEmail($ticketId)
	{
		$ticket = \Ticket::with('handler', 'mappings')->findOrFail($ticketId);
		$res = ['success' => true];
		$handler = Auth::user();

		// @see: not sending reco email to the CRM team
		$ccAddresses = [
			$handler->username,
			//config('evibe.enquiry_group_email')
		];

		if ($ticket->handler)
		{
			array_push($ccAddresses, $ticket->handler->username);
		}

		if ($ticket->alt_email)
		{
			array_push($ccAddresses, $ticket->alt_email); // alt email in CC
		}

		if (!count($ticket->mappings))
		{
			$res = [
				"success" => false,
				"error"   => "No vendor mappings found, could not send recommendation email."
			];
		}
		elseif (!$ticket->event_id)
		{
			$res = [
				'success' => false,
				'error'   => 'Ticket occasion not found, could not send recommendation email.'
			];
		}
		else
		{
			// get event
			$event = TypeEvent::where('id', $ticket->event_id)->first();
			$eventName = $event ? ($event->meta_name ? $event->meta_name : $event->name) : "party";

			// create the link
			$hashedUrl = config('evibe.live.host') . '/recommendation' . '?tId=' . $ticket->id . '&token=' . Hash::make($ticket->id);

			// Send email
			$data = [
				'to'                => [$ticket->email],
				'name'              => ucfirst($ticket->name),
				'mappings'          => $ticket->mappings,
				'partyDate'         => date("d M Y", $ticket->event_date),
				'ccAddresses'       => $ccAddresses,
				'replyTo'           => [config('evibe.enquiry_group_email')],
				'recommendationUrl' => $hashedUrl,
				'handlerName'       => $handler->name ? $handler->name : "Team Evibe.in",
				'eventName'         => $eventName,
				'enquiryId'         => $ticket->enquiry_id
			];

			// Send SMS
			$smsData = [
				'to'       => $ticket->phone,
				'name'     => ucfirst($ticket->name),
				'email'    => $ticket->email,
				'recoLink' => $this->getShortUrl($hashedUrl . "&ref=sms")
			];

			// validating email and phone
			$resEmail = $this->validateEmail($data['to'], $ticket, "Customer");
			if (!$resEmail['success'])
			{
				return response()->json($resEmail);
			}
			Queue::push('\Evibe\Utilities\SendEmail@mailMappingsReco', $data);

			$resPhone = $this->validatePhone($smsData['to'], $ticket, "Customer");
			if ($resPhone['success'])
			{
				Queue::push('\Evibe\Utilities\SendSMS@smsRecoAlert', $smsData);
			}

			// Set recommended_at timestamp
			foreach ($ticket->mappings as $mapping)
			{
				$mapping->update(['handler_id' => Auth::user()->id, 'recommended_at' => date('Y-m-d H:i:s')]);
			}

			// ticket action update
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.followup'),
				'comments' => config('evibe.ticket_status_message.recommendation')
			];
			$this->updateTicketAction($ticketUpdate);

			$ticket->reco_url = $data['recommendationUrl'];
			$ticket->save();

			$recoUrl = $this->getShortUrl($hashedUrl . "&ref=whatsapp");
			$res["recoUrl"] = $recoUrl;
			$res["name"] = $ticket->name;
			$res["custMobile"] = $ticket->phone;
			$res["callingCode"] = $ticket->calling_code ?: "+91";
		}

		return response()->json($res);
	}

	/**
	 * Exchange contacts
	 *
	 * @since 22 Jan 2015
	 */
	public function exchangeContacts($ticketId)
	{
		$res = ['success' => true];

		// get all tickets mapping with `contact_exchanged_at` NULL
		$mappings = Input::get('selectedMapping');
		$ticketMappings = \TicketMapping::with('ticket')
		                                ->where('ticket_id', $ticketId)
		                                ->whereIn('id', $mappings)
		                                ->get();

		if (!count($ticketMappings))
		{
			$res = [
				'success' => false,
				'error'   => 'No vendor mappings found (or) all contacts already exchanged.'
			];
		}
		else
		{
			// get customer details
			$ticket = $ticketMappings->first()->ticket;
			$resPhone = $this->validatePhone($ticket->phone, $ticket, "Customer");
			if (!$resPhone['success'])
			{
				return response()->json($resPhone);
			}

			foreach ($ticketMappings as $ticketMapping)
			{
				// services, venue: ignore
				if ($ticketMapping->map_type_id == config('evibe.ticket_type.services'))
				{
					continue;
				}

				if ($ticketMapping->map_type_id == config('evibe.ticket_type.venues'))
				{
					$this->exchangeVenueContact($ticketMapping->id);
					$ticketMapping->update(['contact_exchanged_at' => date('Y-m-d H:i:s')]);
				}
				else
				{
					$mappedValues = $ticketMapping->getMappedValues();
					$vendorPhone = $mappedValues['provider']['phone'];
					$vendorName = $mappedValues['provider']['person'];

					// for non-venues
					$customerTemplate = config('evibe.sms_tpl.leads.non_venue.customer');
					$customerReplaces = [
						'#field1#' => ucfirst($ticket->name),
						'#field2#' => $vendorName,
						'#field3#' => $vendorPhone,
						'#field4#' => config('evibe.phone_plain')
					];
					$customerSMS = str_replace(array_keys($customerReplaces), array_values($customerReplaces), $customerTemplate);

					$vendorTemplate = config('evibe.sms_tpl.leads.non_venue.vendor');
					$vendorReplaces = [
						'#field1#' => $vendorName,
						'#field2#' => ucfirst($ticket->name),
						'#field3#' => $ticket->area->name,
						'#field4#' => $ticket->phone,
						'#field5#' => config('evibe.phone_plain')
					];
					$vendorSMS = str_replace(array_keys($vendorReplaces), array_values($vendorReplaces), $vendorTemplate);

					// Send SMS to Customer
					if ($customerSMS)
					{
						$customerSMSData = [
							'to'   => $ticket->phone,
							'text' => $customerSMS
						];

						Queue::push('Evibe\Utilities\SendSMS@smsExchangeContacts', $customerSMSData);
					}

					// Send SMS to Vendor
					if ($vendorSMS)
					{
						$vendorSMSData = [
							'to'   => $vendorPhone,
							'text' => $vendorSMS
						];

						Queue::push('Evibe\Utilities\SendSMS@smsExchangeContacts', $vendorSMSData);
					}

					// update Contact exchanged date time
					if ($customerSMS && $vendorSMS)
					{
						$ticketMapping->update(['contact_exchanged_at' => date('Y-m-d H:i:s')]);
					}
				}
			}

			// ticket action update
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.followup'),
				'comments' => config('evibe.ticket_status_message.exchange_contact')
			];
			$this->updateTicketAction($ticketUpdate);
		}

		return response()->json($res);
	}

	/**
	 * @author Vikash <vikash@evibe.in>
	 * @Since  22 March 2016
	 * Ask about the venue availability from B.D.
	 */
	public function askVenueAvailability($ticketId)
	{
		$res = ['success' => false];
		$avlEmailData = [];
		$ticket = \Ticket::find($ticketId);
		$selectedVenues = Input::get('venues');
		$date1 = Input::get('date1');
		$slot1 = Input::get('slot1');
		$date2 = Input::get('date2');
		$slot2 = Input::get('slot2');
		$date3 = Input::get('date3');
		$slot3 = Input::get('slot3');
		$type = Input::get('type');
		$budget = Input::get('budget');
		$guests = Input::get('guests');
		$comments = Input::get('comments');

		$rules = [
			'date1'  => 'required|date|after:' . date("d-m-Y h:i", time() - 60),
			'date2'  => 'date|after:' . date("d-m-Y h:i", time() - 60),
			'date3'  => 'date|after:' . date("d-m-Y h:i", time() - 60),
			'slot1'  => 'required_with:date1|integer|min:0',
			'slot2'  => '',
			'slot3'  => '',
			'budget' => 'required',
			'guests' => 'required|min:1|integer',
		];
		$messages = [
			'budget.required' => 'Budget is required',
			'guests.required' => 'Please enter the minimum guests',
			'guests.min'      => 'Min guests is invalid',
			'guests.integer'  => 'Please enter valid number of guests',
			'date1.required'  => 'Please enter first preferred party date',
			'date1.date'      => 'Party Date ( pref-1 ) must be a valid date',
			'date2.date'      => 'Party Date ( pref-2 ) must be a valid date',
			'date3.date'      => 'Party Date ( pref-3 ) must be a valid date',
			'date1.after'     => 'Party Date ( pref-1 ) must be after party date',
			'date2.after'     => 'Party Date ( pref-2 ) must be after party date',
			'date3.after'     => 'Party Date ( pref-3 ) must be after party date',
			'slot1.min'       => 'Please select a valid slot type with party date (pref 1)',
			'slot2.min'       => 'Please select a valid slot type with party date (pref 2)',
			'slot3.min'       => 'Please select a valid slot type with party date (pref 3)',
		];
		$date2 ? array_set($rules, 'slot2', 'integer|min:0') : null;
		$date3 ? array_set($rules, 'slot3', 'integer|min:0') : null;

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];

			return response()->json($res);
		}
		else
		{
			foreach ($selectedVenues as $availabilityCheck)
			{
				$availabilityCheck = [
					'ticket_id'         => $ticketId,
					'map_id'            => $availabilityCheck['map_id'],
					'map_type_id'       => config('evibe.ticket_type.venues'),
					'ticket_mapping_id' => $availabilityCheck['ticket_mapping_id'],
					'type'              => $type,
					'enquired_by'       => Auth::user()->id,
					'budget'            => $budget,
					'min_guests'        => $guests,
					'remarks'           => $comments,
					'created_at'        => Carbon::now(),
					'updated_at'        => Carbon::now()
				];

				if ($availabilityCheck)
				{
					\TicketMapping::where('id', $availabilityCheck['ticket_mapping_id'])->update(['venue_availability_asked_at' => Carbon::now()]);
					$checkAvl = CheckAvailability::create($availabilityCheck);
					$avlEmailData[] = [
						'hall'   => $checkAvl->hall->name,
						'code'   => $checkAvl->hall->code,
						'venue'  => $checkAvl->hall->venue->name,
						'person' => $checkAvl->hall->venue->person,
						'phone'  => $checkAvl->hall->venue->phone,
						'link'   => route('venues.hall', $checkAvl->hall->id)
					];

					$res = ['success' => true];
				}
			}
			$ticketData = [
				'venue_budget'     => $budget,
				'venue_min_guests' => $guests,
				'venue_remarks'    => $comments,
				'venue_type'       => $type,
				'venue_date_1'     => $date1 ? strtotime($date1) : null,
				'venue_slot_1'     => ($slot1 && $slot1 != -1) ? $slot1 : null,
				'venue_date_2'     => $date2 ? strtotime($date2) : null,
				'venue_slot_2'     => ($slot2 && $slot2 != -1) ? $slot2 : null,
				'venue_date_3'     => $date3 ? strtotime($date3) : null,
				'venue_slot_3'     => ($slot3 && $slot3 != -1) ? $slot3 : null,
			];

			if (!$ticket->update($ticketData))
			{
				$res = ['success' => false, 'customError' => 'Error while updating ticket data'];
			}

			if ($res['success'] == true)
			{
				$emailSubject = 'Check venue(s) availability for ' . $ticket->name . '\'s party';

				$emailData = [
					'to'               => [config('evibe.business_group_email')],
					'sub'              => $emailSubject,
					'customerName'     => $ticket->name,
					'date1'            => $date1 ? AppUtil::formatDateTimeForBooking(strtotime($date1)) : null,
					'slot1'            => AppUtil::getSlot($ticketData['venue_slot_1']),
					'date2'            => $date2 ? AppUtil::formatDateTimeForBooking(strtotime($date2)) : null,
					'slot2'            => AppUtil::getSlot($ticketData['venue_slot_2']),
					'date3'            => $date3 ? AppUtil::formatDateTimeForBooking(strtotime($date3)) : null,
					'slot3'            => AppUtil::getSlot($ticketData['venue_slot_3']),
					'budget'           => $budget,
					'guests'           => $guests,
					'enquiryId'        => $ticket->enquiry_id,
					'ticketLink'       => route('availability.venue.live.details', $ticket->id) . '?ref=email',
					'type'             => AppUtil::getVenueType($type),
					'ccAddresses'      => [],
					'replyTo'          => [],
					'availabilityData' => $avlEmailData,
					'comments'         => trim($ticket->venue_remarks),
					'crmName'          => AppUtil::getHandlerName(Auth::user()->username),
				];

				$resEmail = $this->validateEmail($emailData['to'], $ticket, "Business Team");

				if (!$resEmail['success'])
				{
					return response()->json($resEmail);
				}

				Queue::push('\Evibe\Utilities\SendEmail@mailAvailabilityAsked', $emailData);

				$hallCode = $this->groupHallCode($avlEmailData);
				$ticketUpdate = [
					'ticket'   => \Ticket::find($ticketId),
					'statusId' => config('evibe.status.followup'),
					'comments' => 'Availability checked for venue(s): ' . $hallCode
				];
				$this->updateTicketAction($ticketUpdate);
				$res = ['success' => true];
			}
		}

		return response()->json($res);
	}

	/**
	 * @author Harish <harish.annavajjala@evibe.in>
	 * @since  4 Mar 2015
	 */
	public function toggleAutoCancellationMail($ticketId)
	{
		$res = ['success' => true];
		$logTicketActions = new \LogTicketActions;
		$ticket = \Ticket::findOrFail($ticketId);
		$updateComment = config('evibe.ticket_status_message.cancel_toggle_on');

		if ($ticket->is_auto_cancellation_mail == 1)
		{
			$ticket->is_auto_cancellation_mail = 0;
			$logTicketActions->action = config('evibe.cancellation_mail.toggle_on_off');
			$updateComment = config('evibe.ticket_status_message.cancel_toggle_off');
		}
		elseif ($ticket->is_auto_cancellation_mail == 0)
		{
			$ticket->is_auto_cancellation_mail = 1;
			$logTicketActions->action = config('evibe.cancellation_mail.toggle_off_on');
		}
		else
		{
			// do nothing.
		}

		$logTicketActions->ticket_id = $ticketId;
		$logTicketActions->is_auto = config('evibe.cancellation_mail.is_auto_off');
		$logTicketActions->handler_id = Auth::user()->id;
		$logTicketActions->is_success = config('evibe.cancellation_mail.is_success_off');
		$logTicketActions->save();

		$ticket->save();

		$logTicketActions->is_success = config('evibe.cancellation_mail.is_success_on');
		$logTicketActions->save();

		// ticket update action
		$ticketUpdate = [
			'ticket'   => $ticket,
			'comments' => $updateComment
		];
		$this->updateTicketAction($ticketUpdate);

		return response()->json($res);
	}

	public function sendAnyCustomEmail($ticketId)
	{
		$ticket = \Ticket::with('handler')->findOrFail($ticketId);
		$rules = [
			'sub'     => 'required|min:10',
			'opening' => 'required|min:10',
			'body'    => 'required|min:20',
			'ending'  => 'required|min:10',
			'sig'     => 'required|min:10'
		];
		$messages = [
			'sub.required'     => 'Email subject is required',
			'sub.min'          => 'Email subject cannot be that short',
			'opening.required' => 'Email opening line is required',
			'opening.min'      => 'Email opening line cannot be that short',
			'body.required'    => 'Email body is required',
			'body.min'         => 'Email body cannot be that short',
			'ending.required'  => 'Email ending is required',
			'ending.min'       => 'Email ending cannot be that short',
			'sig.required'     => 'Email signature is required',
			'sig.min'          => 'Email signature cannot be that short'
		];

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $validation->messages()->first()
			                        ]);
		}
		else
		{
			// send email
			$ccAddresses = [
				Auth::user()->username,
				config('evibe.enquiry_group_email')
			];

			if ($ticket->handler)
			{
				array_push($ccAddresses, $ticket->handler->username);
			}

			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email); // alt email in CC
			}

			$emailData = [
				'to'           => [$ticket->email],
				'customerName' => ucfirst($ticket->name),
				'sub'          => Input::get('sub'),
				'opening'      => Input::get('opening'),
				'ending'       => Input::get('ending'),
				'body'         => Input::get('body'),
				'sig'          => Input::get('sig'),
				'ccAddresses'  => $ccAddresses,
			];

			$resEmail = $this->validateEmail($emailData['to'], $ticket, "Customer");
			if (!$resEmail['success'])
			{
				$resEmail['errorMsg'] = $resEmail['error'];

				return response()->json($resEmail);
			}

			Queue::push('\Evibe\Utilities\SendEmail@mailAnyCustomEmail', $emailData);

			// ticket action update
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.followup'),
				'comments' => config('evibe.ticket_status_message.custom_email')
			];
			$this->updateTicketAction($ticketUpdate);

			return response()->json(['success' => true]);
		}
	}

	public function sendNoResponseAlert($ticketId)
	{
		if (!Auth::check())
		{
			return response()->json(["success" => false, "error" => "Auth user does not exist"]);
		}

		$authUser = Auth::user();
		$ticket = \Ticket::with("handler")->findOrFail($ticketId);
		$handler = $ticket->handler;

		if (!$handler)
		{
			$ticket->update([
				                'handler_id'         => $authUser->id,
				                'created_handler_id' => $authUser->id
			                ]);

			$handler = $authUser;
		}

		$handlerName = ucwords(trim($handler->name));
		$handlerPhone = config('evibe.default_handler.phone');
		$handlerEmail = strtolower($handler->username);

		// send Email
		if ($ticket->email)
		{
			$ccAddresses = [];
			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email); // alt email in CC
			}

			$emailData = [
				'to'               => [$ticket->email],
				'name'             => ucfirst($ticket->name),
				'phone'            => $ticket->phone,
				'ccAddresses'      => $ccAddresses,
				'replyTo'          => [$handlerEmail],
				'handlerName'      => $handlerName,
				'handlerFirstName' => explode(" ", $handlerName)[0],
				'handlerPhone'     => $handlerPhone,
				'partyDate'        => $ticket->event_date ? date('d M Y', $ticket->event_date) : null,
				'ticketId'         => $ticketId,
				'enquiryId'        => $ticket->enquiry_id
			];

			$resEmail = $this->validateEmail($emailData['to'], $ticket, "Customer");

			if (!$resEmail['success'])
			{
				return response()->json($resEmail);
			}

			$this->dispatch(new MailNoResponseToCust($emailData));

		}

		// send SMS
		$resPhone = $this->validatePhone($ticket->phone, $ticket, "Customer");
		if ($resPhone['success'])
		{
			$smsData = [
				'to'           => $ticket->phone,
				'name'         => $ticket->name ? ucfirst($ticket->name) : "Customer",
				'handlerPhone' => $handlerPhone
			];

			Queue::push('\Evibe\Utilities\SendSMS@smsNoResponse', $smsData);
		}

		$paymentStatus = [
			config('evibe.status.booked'),
			config('evibe.status.confirmed'),
			config('evibe.status.auto_pay'),
			config('evibe.status.service_auto_pay'),
		];

		if (in_array($ticket->status_id, $paymentStatus))
		{
			$updatedStatusId = $ticket->status_id;
		}
		else
		{
			$updatedStatusId = config('evibe.status.no_response');
		}

		// ticket action update
		$ticketUpdate = [
			'ticket'   => $ticket,
			'statusId' => $updatedStatusId,
			'comments' => config('evibe.ticket_status_message.no_response')
		];
		$this->updateTicketAction($ticketUpdate);

		return response()->json(['success' => true]);
	}

	/**
	 * @author Harish <harish.annavajjala@evibe.in>
	 * @since  4 Mar 2015
	 */
	public function sendCancellationMailToCustomer($ticketId)
	{
		$res = ['success' => true];
		$ticket = \Ticket::with('bookings')->findOrFail($ticketId);
		$typeCancellationId = Input::get('typeCancellationId');

		if ($typeCancellationId == -1)
		{
			$reason = Input::get('reason');
		}
		else
		{
			$typeCancellation = TypeCancellationReason::findorFail($typeCancellationId);
			$reason = $typeCancellation->message;
		}
		$ccAddresses = [
			Auth::user()->username,
			config('evibe.enquiry_group_email')
		];

		if ($ticket->handler)
		{
			array_push($ccAddresses, $ticket->handler->username);
		}

		if ($ticket->alt_email)
		{
			array_push($ccAddresses, $ticket->alt_email); // alt email in CC
		}

		$partyTime = date("d-M-Y", $ticket->event_date);
		if ($ticket->bookings)
		{
			$bookings = $ticket->bookings;
			$minBookTime = $bookings->min('party_date_time');
			$maxBookTime = $bookings->max('party_date_time');

			$partyTime = date("d M Y", $minBookTime);

			if ($minBookTime == $maxBookTime)
			{
				$partyTime = date("d M Y h:i A", $minBookTime);
			}
			elseif (date("d m y", $minBookTime) != date("d m y", $maxBookTime))
			{
				$partyTime = date("d/m/y (D)", $minBookTime) . ' - ' . date("d/m/y (D)", $maxBookTime);
			}
		}

		$data = [
			'name'             => ucfirst($ticket->name),
			'phone'            => $ticket->phone,
			'email'            => $ticket->email,
			'partyDate'        => $partyTime,
			'ccAddresses'      => $ccAddresses,
			'replyToAddresses' => $ccAddresses,
			'reason'           => ucwords($reason)
		];

		$logTicketActions = new \LogTicketActions;
		$logTicketActions->ticket_id = $ticket->id;
		$logTicketActions->action = config('evibe.cancellation_mail.manual_action');
		$logTicketActions->is_auto = config('evibe.cancellation_mail.is_auto_off');
		$logTicketActions->handler_id = Auth::user()->id;
		$logTicketActions->is_success = config('evibe.cancellation_mail.is_success_off');
		$logTicketActions->save();

		// send email and SMS to customer
		if ($data['email'])
		{
			$resEmail = $this->validateEmail($data['email'], $ticket, "Customer");

			if (!$resEmail['success'])
			{
				return response()->json($resEmail);
			}

			Queue::push('Evibe\Utilities\SendEmail@sendCancellationMailToCustomer', $data);
		}

		if ($data['phone'])
		{
			$resPhone = $this->validatePhone($data['phone'], $ticket, "Customer");

			if (!$resPhone['success'])
			{
				return response()->json($resPhone);
			}

			Queue::push('Evibe\Utilities\SendSMS@smsCancellationToCustomer', $data);
		}

		// Send SMS to all vendors
		$bookings = $ticket->bookings;
		foreach ($bookings as $booking)
		{
			$booking = \TicketBooking::with('provider')->find($booking->id);
			$vendor = $booking->provider;

			if ($vendor)
			{
				$vendorCc = [
					$vendor->email,
					//config('evibe.enquiry_group_email')
				];

				if ($vendor->alt_email)
				{
					array_push($vendorCc, $vendor->alt_email);
				}
				if ($vendor->alt_email_1)
				{
					array_push($vendorCc, $vendor->alt_email_1);
				}

				if ($vendor->alt_email_2)
				{
					array_push($vendorCc, $vendor->alt_email_2);
				}

				if ($vendor->alt_email_3)
				{
					array_push($vendorCc, $vendor->alt_email_3);
				}

				$vendorSmsData = [
					'phone'     => $vendor->phone,
					'name'      => ucfirst($vendor->person),
					'customer'  => ucfirst($ticket->name),
					'partyDate' => date("d-M-Y", $booking->party_date_time),
					'reason'    => ucwords($reason)
				];

				$subject = "[Evibe.in] " . $vendor->person . ", order cancelled for " . $data['partyDate'];

				$vendorEmailData = [
					'to'               => [$vendor->email],
					'ccAddresses'      => $vendorCc,
					'bccAddresses'     => [config('evibe.ops_alert_no_action_email')],
					'replyToAddresses' => [config('evibe.enquiry_group_email')],
					'subject'          => $subject,
					'customerName'     => ucfirst($ticket->name),
					'vendorName'       => $vendor->person,
					'partyDate'        => date("d-M-Y", $booking->party_date_time),
					'location'         => $ticket->area ? $ticket->area->name : '<i>--N/A--</i>',
					'reason'           => ucwords($reason)
				];

				Queue::push('Evibe\Utilities\SendEmail@sendCancellationMailToVendor', $vendorEmailData);
				Queue::push('Evibe\Utilities\SendSMS@smsCancellationToVendor', $vendorSmsData);
			}
		}

		$logTicketActions->is_success = config('evibe.cancellation_mail.is_success_on');
		$logTicketActions->save();

		// update ticket action
		$ticketUpdate = [
			'ticket'   => $ticket,
			'statusId' => config('evibe.status.cancelled'),
			'comments' => config('evibe.ticket_status_message.cancel_email'),
		];
		$this->updateTicketAction($ticketUpdate);
		$ticket->update([
			                'is_auto_cancellation_mail' => 0,
			                'type_cancellation_id'      => $typeCancellationId > 0 ? $typeCancellationId : null,
			                'cancellation_reason'       => $typeCancellationId > 0 ? null : ucwords($reason)
		                ]);

		return response()->json($res);
	}

	public function getAttachments()
	{
		$id = Input::get('id');
		$type = Input::get('type');

		if ($type == 'ask')
		{
			$attachments = \NotificationAttachements::where('notification_id', $id)->where('is_reply', 0)->get();
			$imagePath = [];

			foreach ($attachments as $images)
			{
				$url = AppUtil::getGalleryBaseUrl() . '/notification/' . $id . '/ask/' . $images->url;
				$imagePath[] = $url;
			}
		}
		else
		{
			$attachments = \NotificationAttachements::where('notification_id', $id)->where('is_reply', 1)->get();
			$imagePath = [];
			foreach ($attachments as $images)
			{
				$url = AppUtil::getGalleryBaseUrl() . '/notification/' . $id . '/reply/' . $images->url;
				$imagePath[] = $url;
			}
		}

		return $imagePath;
	}

	/**
	 * @since 22 Jan 2015
	 */
	public function setTicketMoreInfo($ticketId)
	{
		$rules = [
			'venueAddress1' => 'required_with:venueAddress2,pinCode',
			'pinCode'       => 'integer|digits:6|required_with:venueAddress1',
			'venueLandmark' => 'required_with:venueAddress1'
		];

		$messages = [
			'venueAddress1.required_with' => 'Address line 1 is required with Address line 2 / pin code',
			'pinCode.required_with'       => 'Pin code is required with venueAddress1',
			'pinCode.integer'             => 'Please enter an valid 6 digit pin code',
			'pinCode.digits'              => 'Please enter an valid 6 digit pin code',
			'venueLandmark.required_with' => 'Venue Landmark with required with Address line 1 '
		];

		$validation = Validator::make(Input::all(), $rules, $messages);
		$res = ['success' => true];

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}
		else
		{
			$ticket = \Ticket::findOrFail($ticketId);

			$venueFullAddress = "";
			$addressLine1 = Input::get('venueAddress1');
			$addressLine2 = Input::get('venueAddress2');
			$pinCode = Input::get('pinCode');

			if ($addressLine1)
			{
				$venueFullAddress .= "$addressLine1";
			}

			if ($addressLine2)
			{
				$venueFullAddress .= ", $addressLine2";
			}

			if ($pinCode)
			{
				$venueFullAddress .= ", Pin code: " . $pinCode;
			}

			$ticket->update([
				                'baby_name'      => Input::get('babyName'),
				                'venue_address'  => $venueFullAddress,
				                'address_line1'  => $addressLine1,
				                'address_line2'  => $addressLine2,
				                'zip_code'       => $pinCode,
				                'venue_landmark' => Input::get('venueLandmark'),
				                'alt_phone'      => Input::get('altPhone'),
				                'spl_notes'      => Input::get('splNotes'),
				                'updated_at'     => date('Y-m-d H:i:s')
			                ]);

			// save ticket update
			$ticketUpdate = [
				'ticket'   => $ticket,
				'comments' => config('evibe.ticket_status_message.more_info_edit')
			];
			$this->updateTicketAction($ticketUpdate);
		}

		return response()->json($res);
	}

	public function resolveCustomerQuery($queryId)
	{
		try
		{

			$query = TicketPostBookingRequests::find($queryId);
			if (!$query)
			{
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.update_function'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketActionButtonController.php - ' . __LINE__ . ']. Unable to find query details to resolve customer query.',
					                                   'exception' => '[TicketActionButtonController.php - ' . __LINE__ . ']. Unable to find query details to resolve customer query.',
					                                   'trace'     => '[Query Id: ' . $queryId . ']',
					                                   'details'   => '[Query Id: ' . $queryId . ']'
				                                   ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$ticket = \Ticket::find($query->ticket_id);
			if (!$ticket)
			{
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.update_function'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketActionButtonController.php - ' . __LINE__ . ']. Unable to find ticket details to resolving customer query.',
					                                   'exception' => '[TicketActionButtonController.php - ' . __LINE__ . ']. Unable to find ticket details to resolving customer query.',
					                                   'trace'     => '[Query Id: ' . $queryId . '] [Ticket Id: ' . $query->ticket_id . ']',
					                                   'details'   => '[Query Id: ' . $queryId . '] [Ticket Id: ' . $query->ticket_id . ']'
				                                   ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$query->handler_id = auth()->user()->id;
			$query->resolved_at = Carbon::now();
			$query->updated_at = Carbon::now();
			$query->save();

			return response()->json([
				                        'success'    => true,
				                        'successMsg' => 'Customer query has been marked as resolved.'
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			Log::error($exception->getMessage());

			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => 'Something unexpected happened. Kindly refresh the page and try again.'
			                        ]);
		}
	}
}