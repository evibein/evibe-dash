<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;

use App\Models\ServiceGallery;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Ticket\TicketMappingFieldValue;
use App\Models\Util\CheckoutField;
use App\Models\Util\StarOption;
use Carbon\Carbon;
use Evibe\Facades\AppUtilFacade as AppUtil;

use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Exception\ClientException;

class TicketMappingController extends BaseTicketController
{
	public function newTicketMapping($ticketId)
	{
		$mapTypeId = Input::get('mapTypeId');
		$mapId = Input::get('mappingOptions');

		// check if ticket, mapping combination exists
		$existingTicketMapping = \TicketMapping::where('ticket_id', $ticketId)
		                                       ->where('map_id', $mapId)
		                                       ->where('map_type_id', $mapTypeId)
		                                       ->get();

		if ($mapId && $mapTypeId && $mapId > 0 && $mapTypeId > 0)
		{
			if ($existingTicketMapping->count())
			{
				// combination exists
				return redirect()->route('ticket.details.mappings', $ticketId)
				                 ->with('customError', 'Mapping already exists.');
			}

			$insertData = [
				'ticket_id'   => $ticketId,
				'map_id'      => $mapId,
				'map_type_id' => $mapTypeId,
				'handler_id'  => Auth::user()->id,
				'updated_at'  => date('Y-m-d H:i:s')
			];

			if (\TicketMapping::create($insertData))
			{
				// update ticket status
				$ticket = \Ticket::findOrFail($ticketId);
				$userId = Auth::user()->id;
				$ticket->handler_id = $userId;

				if ($ticket->status_id == config('evibe.status.initiated'))
				{
					$ticket->status_id = config('evibe.status.progress');
				}

				if (!$ticket->created_handler_id)
				{
					$ticket->created_handler_id = $userId;
				}

				$ticket->save();

				return redirect()->route('ticket.details.mappings', $ticketId);
			}
		}

		return redirect()->route('ticket.details.mappings', $ticketId)
		                 ->with('customError', 'Failed to add ticket mapping. Please refresh and try again.');
	}

	public function showTicketMapBooking($ticketMappingId)
	{
		$ticketMapping = \TicketMapping::findOrFail($ticketMappingId);
		$isBookingAfterEnquiry = Input::get('isEnqBooking');

		$res = AppUtil::fillMappingValues([
			                                  'isCollection' => false,
			                                  'mapTypeId'    => $ticketMapping->map_type_id,
			                                  'mapId'        => $ticketMapping->map_id
		                                  ]);

		$enquiry = $ticketMapping->enquiry;
		if ($enquiry && $enquiry->type_id == config('evibe.enquiry.avl_check'))
		{
			$res['price'] = (int)$enquiry->budget_price;
		}

		$res['success'] = true;
		$res['ticket_mapping_id'] = $ticketMapping->id;
		$res['partyEndTime'] = $ticketMapping->ticket->party_end_time ? $ticketMapping->ticket->party_end_time : null;
		$res['isEnqBooking'] = $isBookingAfterEnquiry;
		$res['partnerType'] = $ticketMapping->target_partner_id ? 2 : 1;

		if ($isBookingAfterEnquiry)
		{
			$res['bookingTypeId'] = $ticketMapping->type_id;
			$res['info'] = $ticketMapping->info;
			$res['typeDetails'] = $ticketMapping->type_details;
			$res['dynamic'] = [];
			$res['textField'] = [];

			$allFields = CheckoutField::where('type_ticket_booking_id', $ticketMapping->type_id)->get();

			foreach ($allFields as $allField)
			{
				if ($allField->type_field_id == config('evibe.input.textarea'))
				{
					$res['textField'][$allField->name] = $allField->enquiryCheckoutValue($ticketMappingId);
				}
				else
				{
					$res['dynamic'][$allField->name] = $allField->enquiryCheckoutValue($ticketMappingId);
				}
			}
		}

		return response()->json($res);
	}

	public function newTicketMapBooking($ticketMappingId)
	{
		$venueHall = null;
		$ticketMapping = \TicketMapping::with('ticket')->findOrFail($ticketMappingId);
		$mapTypeId = $ticketMapping->map_type_id;
		$mapId = $ticketMapping->map_id;
		$typeTicketBookingId = Input::get('typeTicketBooking');
		$typeBookingConceptId = Input::get('typeBookingConcept');
		$ticket = $ticketMapping->ticket;
		$typeAvailabilityCheck = Input::get('availabilityCheckType');

		$res = ['success' => true];
		$rules = [
			'bookingAmount'         => 'required|integer|min:1',
			'advanceAmount'         => 'required|integer|max:' . Input::get('bookingAmount'),
			'partyDateTime'         => 'required|date',
			'reportBeforeTimeHrs'   => 'integer|between:0,23',
			'reportBeforeTimeMin'   => 'integer|between:0,59',
			'typeTicketBooking'     => 'sometimes|required|integer|min:1',
			'bookingTypeDetails'    => 'required_with:typeTicketBooking|max:255',
			'typeBookingConcept'    => 'required|integer|min:1',
			'availabilityCheckType' => 'required'
		];

		$messages = [
			'bookingAmount.required'           => 'Booking amount is required',
			'bookingAmount.integer'            => 'Booking amount entered is not valid (use ONLY numbers)',
			'bookingAmount.min'                => 'Booking amount cannot be zero',
			'advanceAmount.required'           => 'Advance amount is required',
			'advanceAmount.integer'            => 'Advance amount entered is not valid (use ONLY numbers)',
			'advanceAmount.min'                => 'Advance amount cannot be that low',
			'advanceAmount.max'                => 'Advance amount cannot be more than booking amount',
			'partyDateTime.required'           => 'Party date & time is required',
			'partyDateTime.date'               => 'Party date & time is not valid',
			'reportBeforeTimeHrs.between'      => 'select the valid hours of party before time(Ex-4 hrs)',
			'reportBeforeTimeMin.between'      => 'select the valid minutes of party before time(15 minutes)',
			'additionalInfo.required'          => 'Order details is required',
			'additionalInfo.min'               => 'Order details should be minimum 10 characters',
			'partyEndTime.after'               => 'Party End date must be after Party Start date ',
			'typeTicketBooking.required'       => 'Ticket booking type is required',
			'typeTicketBooking.min'            => 'Please select a valid ticket booking type',
			'bookingTypeDetails.required_with' => 'Booking type details is required with ticket type booking',
			'bookingTypeDetails.min'           => 'Booking type details should be less than 255 characters',
			'typeBookingConcept.required'      => 'Booking concept type is required',
			'typeBookingConcept.integer'       => 'Booking concept type is invalid',
			'typeBookingConcept.min'           => 'Booking concept type is invalid',
			'availabilityCheckType.required'   => 'Please Select type of availability check'
		];

		$crmFields = $this->getCheckoutField($ticket, $typeTicketBookingId, 1);
		foreach ($crmFields as $crmField)
		{
			$rules['checkout-' . $crmField->name] = "required";
			$messages['checkout-' . $crmField->name . '.required'] = "$crmField->identifier is required. if not applicable type <b>n/a</b>";
			if ($crmField->type_field_id == config('evibe.input.checkbox'))
			{
				// @see: options are stored as text separated by commas
				$messages['checkout-' . $crmField->name . '.required'] = "$crmField->identifier are required. Kindly select at least one choice.";
			}
		}

		$rules['additionalInfo'] = 'required|min:10';
		$rules['partyEndTime'] = 'date|after:partyDateTime';

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}
		else
		{
			$mapTypeIdBooking = $mapTypeId == config('evibe.ticket_type.venues') ? config('evibe.ticket_type.venues') : config('evibe.ticket_type.planners');
			$bookingId = 'EVB-BLR-' . $ticketMapping->id;
			$isVenueBooking = 0;
			$vendorId = null;
			$productImages = [];
			$productGallery = null;
			$typeImage = config('evibe.gallery.type.image');

			switch ($mapTypeId)
			{
				// @see: all of type packages
				case config('evibe.ticket_type.packages'):
				case config('evibe.ticket_type.priests'):
				case config('evibe.ticket_type.venue-deals'):
				case config('evibe.ticket_type.couple-experiences'):
				case config('evibe.ticket_type.food'):
				case config('evibe.ticket_type.lounge'):
				case config('evibe.ticket_type.villa'):
				case config('evibe.ticket_type.resort'):
					$vendorPackage = \VendorPackage::with('provider')->findOrFail($mapId);
					$vendorId = $vendorPackage->provider->id;
					$mapTypeIdBooking = $vendorPackage->map_type_id == config('evibe.ticket_type.venues') ? config('evibe.ticket_type.venues') : config('evibe.ticket_type.planners');
					if ($vendorPackage->map_type_id == config('evibe.ticket_type.venues'))
					{
						$isVenueBooking = 1;
					}
					$productGallery = \PackageGallery::where('planner_package_id', $mapId)->whereNull('deleted_at')->where('type', $typeImage)->get();
					if (count($productGallery))
					{
						foreach ($productGallery as $image)
						{
							array_push($productImages, $image->getGalleryUrl());
						}
					}
					break;
				case config('evibe.ticket_type.planners'):
					$vendorId = $mapId;
					break;
				case config('evibe.ticket_type.venues'):
					$venueHall = \VenueHall::with('venue')->find($mapId);
					$vendorId = $venueHall->venue->id;
					$isVenueBooking = 1;
					break;
				case config('evibe.ticket_type.trends'):
					$trend = \Trend::with('provider')->findOrFail($mapId);
					$vendorId = $trend->provider->id;
					$productGallery = \TrendGallery::where('trending_id', $mapId)->whereNull('deleted_at')->where('type', $typeImage)->get();
					if (count($productGallery))
					{
						foreach ($productGallery as $image)
						{
							array_push($productImages, $image->getLink("results"));
						}
					}
					break;
				case config('evibe.ticket_type.cakes'):
					$cake = \Cake::with('provider')->findOrFail($mapId);
					$vendorId = $cake->provider->id;
					$productGallery = \CakeGallery::where('cake_id', $mapId)->whereNull('deleted_at')->where('type', $typeImage)->get();
					if (count($productGallery))
					{
						foreach ($productGallery as $image)
						{
							array_push($productImages, $image->getLink("results"));
						}
					}
					break;
				case config('evibe.ticket_type.decors'):
					$decor = \Decor::with('provider')->findOrFail($mapId);
					$vendorId = $decor->provider->id;
					$productGallery = \DecorGallery::where('decor_id', $mapId)->whereNull('deleted_at')->where('type_id', $typeImage)->get();
					if (count($productGallery))
					{
						foreach ($productGallery as $image)
						{
							array_push($productImages, $image->getLink("results"));
						}
					}
					break;
				case config('evibe.ticket_type.services'):
				case config('evibe.ticket_type.entertainments'):
					$productGallery = ServiceGallery::where('type_service_id', $mapId)->whereNull('deleted_at')->where('type_id', $typeImage)->get();
					if (count($productGallery))
					{
						foreach ($productGallery as $image)
						{
							array_push($productImages, $image->getLink("results"));
						}
					}
					break;
			}

			if (is_null($vendorId) && $mapTypeId != (config('evibe.ticket_type.services') || config('evibe.ticket_type.entertainments')))
			{
				$e = new \Exception("PartnerId not found for mapping: $ticketMappingId");

				// issue with no map_id for booking
				$this->reportErrorToTechTeam($e, [
					'sub' => 'PartnerId missing for finalising order'
				]);
			}

			// calculate the time in minutes format to save in database
			$reportsBeforeTimeHrs = Input::get('reportBeforeTimeHrs');
			$reportsBeforeTimeMinutes = Input::get('reportBeforeTimeMin');
			$reportsBefore = AppUtil::getTimeInMinutesFormat($reportsBeforeTimeHrs, $reportsBeforeTimeMinutes);

			$partyDateTime = strtotime(Input::get('partyDateTime'));
			$partyEndTime = strtotime(Input::get('partyEndTime'));
			$reportsAtTimestamp = $partyDateTime - (($reportsBefore) * 60);
			$reportsAt = date("Y-m-d H:i:s", $reportsAtTimestamp);
			$prerequisites = Input::get('prerequisites');
			$facts = Input::get('facts');
			$bookingAmount = Input::get('bookingAmount');
			$advanceAmount = Input::get('advanceAmount');
			$partnerType = Input::get('partnerType');

			if ($partnerType == 2)
			{
				if ($mapTypeId == config('evibe.ticket_type.venues'))
				{
					if (Input::get('bookingVenueId'))
					{
						$targetPartnerId = Input::get('bookingVenueId');
					}
					else
					{
						return response()->json(['success' => false, 'error' => "Please Select The Venue"]);
					}
				}
				else
				{
					if (Input::get('bookingPartnerId'))
					{
						$targetPartnerId = Input::get('bookingPartnerId');
					}
					else
					{
						return response()->json(['success' => false, 'error' => "Please Select The Partner"]);
					}
				}
				$vendorId = $targetPartnerId;
			}
			if ($typeAvailabilityCheck == 1)
			{
				$data = [
					'party_date_time'    => date('Y-m-d H:i:s', $partyDateTime),
					'price'              => $bookingAmount,
					'type_id'            => $typeTicketBookingId,
					'type_details'       => trim(Input::get('bookingTypeDetails')),
					'info'               => Input::get('additionalInfo'),
					'enquiry_type_id'    => 2,
					'enquiry_created_at' => Carbon::now(),
					'target_partner_id'  => config("evibe.default_partner_id")
				];
				$ticketMapping->update($data);
				$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries/ask/' . $ticketMapping->id . '?isFromDash=true';

				try
				{
					$client = new Client();
					$client->request('POST', $url, ['json' => ['createdBy' => Auth::user()->id]]);

				} catch (ClientException $e)
				{
					$apiResponse = $e->getResponse()->getBody(true);
					$apiResponse = \GuzzleHttp\json_decode($apiResponse);
					$res['error'] = $apiResponse->errorMessage;

					return redirect()->back()->with('errorMsg', $res['error']);
				}

			}

			// @see: code to restrict CRM team from finalizing mapping, if balance amount is not rounded
			//$balanceAmount = $bookingAmount - $advanceAmount;
			//if (($balanceAmount % 100) != 0)
			//{
			//	return response()->json([
			//		                        'success' => false,
			//		                        'error'   => "Please enter advance amount, rounding off the balance amount (preferably to the nearest 100)"
			//	                        ]);
			//}

			// star option check
			$starOption = StarOption::where('option_id', $ticketMapping->map_id)
			                        ->where('option_type_id', $ticketMapping->map_type_id)
			                        ->whereNull('deleted_at')
			                        ->first();

			// insert into `ticket_bookings` table
			$insertData = [
				'booking_id'              => $bookingId,
				'party_date_time'         => $partyDateTime,
				'booking_info'            => Input::get('additionalInfo'),
				'booking_amount'          => $bookingAmount,
				'advance_amount'          => $advanceAmount,
				'is_venue_booking'        => $isVenueBooking,
				'ticket_id'               => $ticket->id,
				'ticket_mapping_id'       => $ticketMapping->id,
				'map_id'                  => $vendorId,
				'map_type_id'             => $mapTypeIdBooking,
				'party_end_time'          => $partyEndTime,
				'reports_at'              => $reportsAt,
				'prerequisites'           => $prerequisites,
				'facts'                   => $facts,
				'reports_before'          => $reportsBefore,
				'has_customization'       => Input::get('hasCustomization'),
				'area_id'                 => $ticket->area_id,
				'type_ticket_booking_id'  => $typeTicketBookingId,
				'type_booking_concept_id' => $typeBookingConceptId,
				'booking_type_details'    => trim(Input::get('bookingTypeDetails')),
				'is_star_order'           => $starOption ? 1 : 0,
				//'star_partner_amount'     => ($starOption && $starOption->partner_amount) ? $starOption->partner_amount : null,
				'updated_at'              => date('Y-m-d H:i:s')
			];

			if ($newTicketBooking = \TicketBooking::create($insertData))
			{
				// save the reference image if user check the copy reference images
				if (Input::get('enqBooking') == 1 && $ticketMapping->gallery->count())
				{
					foreach ($ticketMapping->gallery as $gallery)
					{
						$fullPath = $gallery->getOriginalImagePath();

						$validImage = getimagesize($fullPath);
						if ($validImage)
						{
							$filename = pathinfo($fullPath, PATHINFO_FILENAME);
							$galleryData = [
								'ticket_booking_id' => $newTicketBooking->id,
								'url'               => $fullPath,
								'type'              => 1,
								'title'             => $filename
							];

							TicketBookingGallery::create($galleryData);
						}
					}
				}

				// include product images if available
				if (count($productImages))
				{
					foreach ($productImages as $imageUrl)
					{
						$filename = pathinfo($imageUrl, PATHINFO_FILENAME);
						$galleryData = [
							'ticket_booking_id' => $newTicketBooking->id,
							'url'               => $imageUrl,
							'type'              => 1, // link
							'title'             => $filename
						];

						TicketBookingGallery::create($galleryData);
					}
				}

				// NULL-ify status, update event_date

				$bookingTime = \TicketBooking::select(DB::raw("MIN(party_date_time) AS min_book_time"))->where('ticket_id', $ticket->id)->groupBy('ticket_id')->first();

				$ticketUpdateData = [
					'success_email_sent_at' => null,
					'success_email_type'    => null,
					'event_date'            => $bookingTime->min_book_time,
					'updated_at'            => date('Y-m-d H:i:s')
				];

				// if venue booking, update details to ticket
				if ($isVenueBooking)
				{
					$venueMapping = $newTicketBooking->mapping;
					$venue = $newTicketBooking->provider;

					$ticketUpdateData['lat'] = $venue->map_lat;
					$ticketUpdateData['lng'] = $venue->map_long;
					$ticketUpdateData['area_id'] = $venue->area_id;
					$ticketUpdateData['city_id'] = $venue->city_id;
					$ticketUpdateData['zip_code'] = $venue->zip;
					$ticketUpdateData['venue_landmark'] = $venue->landmark;
					$ticketUpdateData['map_address'] = $venue->name . ", " . $venue->full_address;
					$ticketUpdateData['map_link'] = "http://maps.google.com/maps?q=$venue->map_lat, $venue->map_long";
					$ticketUpdateData['type_venue_id'] = $venue->venueType ? $venue->type_id : null;
					$ticketUpdateData['venue_address'] = $venue->name . ", " . $venue->full_address;

					// if the mapping is a venue package, use its address rather than direct venue itself
					if (in_array($venueMapping->map_type_id, [
						config('evibe.ticket_type.packages'),
						config('evibe.ticket_type.villa'),
						config('evibe.ticket_type.food'),
						config('evibe.ticket_type.couple-experiences'),
						config('evibe.ticket_type.priests'),
						config('evibe.ticket_type.tents'),
						config('evibe.ticket_type.generic-package')
					]))
					{
						$venuePackage = \VendorPackage::find($venueMapping->map_id);
						if ($venuePackage && ($venuePackage->map_type_id == config('evibe.ticket_type.venues')))
						{
							$ticketUpdateData['lat'] = $venuePackage->map_lat;
							$ticketUpdateData['lng'] = $venuePackage->map_long;
							$ticketUpdateData['area_id'] = $venuePackage->area_id;
							$ticketUpdateData['city_id'] = $venuePackage->city_id;
							$ticketUpdateData['zip_code'] = $venuePackage->zip;
							$ticketUpdateData['venue_landmark'] = $venuePackage->landmark;
							$ticketUpdateData['map_address'] = $venue->name . ", " . $venuePackage->full_address;
							$ticketUpdateData['map_link'] = "http://maps.google.com/maps?q=$venuePackage->map_lat, $venuePackage->map_long";
							$ticketUpdateData['type_venue_id'] = $venuePackage->venueType ? $venuePackage->type_venue_id : null;
							$ticketUpdateData['venue_address'] = $venue->name . ", " . $venuePackage->full_address;
						}
					}
				}

				//  save checkout field value in database
				$this->saveCheckoutFieldValue($ticket, $typeTicketBookingId, Input::all());

				$ticket->update($ticketUpdateData);

				// ticket action update
				$ticketUpdate = [
					'statusId' => config('evibe.status.progress'),
					'ticket'   => $ticket,
					'comments' => config('evibe.ticket_status_message.booking_finalize')
				];
				$this->updateTicketAction($ticketUpdate);
				$ticketMapping->update(['finalized_at' => Carbon::now()]);
			}
			else
			{
				$res['success'] = false;
				$res['error'] = "Error occurred while creating ticket booking. Please try again later. If this issue persits, report to admin";
			}
		}

		return response()->json($res);
	}

	public function deleteTicketMapping($ticketMappingId)
	{
		$ticketMapping = \TicketMapping::find($ticketMappingId);

		if ($ticketMapping && $ticketMapping->delete())
		{
			$res = ['success' => true];

			// delete all mapping field value
			TicketMappingFieldValue::where('ticket_mapping_id', $ticketMappingId)->delete();
		}
		else
		{
			$res = [
				'success' => false,
				'error'   => "Could'nt delete ticket mapping. Please try again later. If this issue persists, report to admin."
			];
		}

		return response()->json($res);
	}

	public function getOptionsByTypeTicket($ticketId)
	{
		$ticket = \Ticket::find($ticketId);
		if ($ticket)
		{
			$mapTypeId = request("mapTypeId");

			$cityId = $ticket->city_id;
			$ticketEvent = $ticket->event_id ?: false;

			$mappingValues = $this->getOptionsByType($mapTypeId, $cityId, $ticketEvent);

			return view("tickets.details.mapping.type-ticket-options", ["mappingValues" => $mappingValues]);
		}
	}
}
