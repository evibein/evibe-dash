<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;
use App\Jobs\Emails\Ticket\MailSyrowUpdateTicketToTeamJob;
use App\Models\TypeEvent;
use App\Models\Types\TypeCategoryTags;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CaptureRequirementsController extends BaseTicketController
{
	public function initializeTicket()
	{
		$showCRMOccasions = [
			config("evibe.event.kids_birthday"),
			config("evibe.event.special_experience"),
			config("evibe.event.bachelor_party"),
			config("evibe.event.house_warming"),
			config("evibe.event.pre_post"),
			config("evibe.event.corporate"),
			config("evibe.event.others")
		];
		$validVenueTypes = [1, 19, 21, 23, 24, 32];

		$cities = \City::orderBy('is_default', 'DESC')->get();
		$occasions = TypeEvent::select("id", "name")
		                      ->whereIn("id", $showCRMOccasions)
		                      ->get();
		$venueTypes = \TypeVenue::select("id", "name")
		                        ->whereIn("id", $validVenueTypes)
		                        ->get();
		$kidsBirthdayData = TypeEvent::select('id', 'name', 'parent_id')
		                             ->where('parent_id', config("evibe.event.kids_birthday"))
		                             ->get();

		$areas = \Area::orderBy('city_id', 'DESC')->orderBy('name')->get();

		$data = [
			'city'               => $cities,
			'area'               => $areas,
			"occasions"          => $occasions,
			"typeVenues"         => $venueTypes,
			"kidsBirthdayGender" => "",
			"eventId"            => "",
			"kidsBirthDayInfo"   => $kidsBirthdayData,
			"isEdit"             => 0
		];

		return view('tickets.capture-requirements.party_requirements', ["data" => $data]);
	}

	public function saveInitializedTicket()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/basic';
		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				"customerGender"     => request("customerGender"),
				"name"               => request("customerName"),
				"userId"             => $user->id,
				"cityId"             => request('city'),
				"eventId"            => request('occasion'),
				"eventDate"          => request('eventDate'),
				"zip_code"           => request('zip'),
				"area_id"            => request('area'),
				"typeVenueId"        => request('typeVenue'),
				"created_handler_id" => $user->id,
				"teamNotes"          => request('teamNotes'),
				"eventSlot"          => request('eventSlot'),
				"specificEvent"      => request("childEvents"),
				"babyGender"         => request("kidsBirthdayGender"),
				"email"              => request("email")
			]
		];

		$res = $this->makeApiCallWithUrl($options);
		if (!$res["success"])
		{
			return redirect()->back()->with('errorMsg', $res["error"])->withInput();
		}

		if ($res["ticketId"] > 0 && $user->role_id == config("evibe.roles.syrow"))
		{
			$ticket = \Ticket::find($res["ticketId"]);
			if ($ticket)
			{
				$ticket->update([
					                "enquiry_source_id" => config("evibe.ticket_enquiry_source.phone")
				                ]);
			}
		}

		return redirect(route("ticket.create.edit.services", [$res["ticketId"]]));
	}

	public function editPartyDetails($ticketId, $isEdit = "0")
	{
		$showCRMOccasions = [
			config("evibe.event.kids_birthday"),
			config("evibe.event.special_experience"),
			config("evibe.event.bachelor_party"),
			config("evibe.event.house_warming"),
			config("evibe.event.pre_post"),
			config("evibe.event.corporate"),
			config("evibe.event.others")
		];

		$kidsBirthdayData = TypeEvent::select('id', 'name', 'parent_id')
		                             ->where('parent_id', config("evibe.event.kids_birthday"))
		                             ->get();

		$cities = \City::orderBy('is_default', 'DESC')->get();

		$occasions = TypeEvent::select("id", "name")
		                      ->whereIn("id", $showCRMOccasions)
		                      ->get();

		$validVenueTypes = [1, 19, 21, 23, 24, 32];
		$venueTypes = \TypeVenue::select("id", "name")
		                        ->whereIn("id", $validVenueTypes)
		                        ->get();

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/basic/' . $ticketId;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		$parentEventId = $this->getParentEventId($res['eventId']);

		$ticket = \Ticket::find($ticketId);

		$areaOptions = null;

		// Fetching all the areas of all cities as to facilitate to change the city
		if ($ticket)
		{
			$areaOptions = \Area::orderBy('city_id', 'DESC')->orderBy('name')->get();
		}

		$data = [
			"city"             => $cities,
			"area"             => $areaOptions,
			"occasions"        => $occasions,
			"parentEventId"    => $parentEventId,
			"typeVenues"       => $venueTypes,
			"response"         => $res,
			"kidsBirthDayInfo" => $kidsBirthdayData,
			"eventId"          => $res['eventId'],
			"isEdit"           => ($isEdit == 0) ? 0 : 1,
		];

		return view('tickets.capture-requirements.party_requirements', ["data" => $data]);
	}

	public function savePartyDetails($ticketId, $isEdit = "0")
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/basic/' . $ticketId;

		$options = [
			'method'      => $isEdit == 1 ? "PUT" : "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				"customerGender"  => request("customerGender"),
				"name"            => request("customerName"),
				"userId"          => $user->id,
				"cityId"          => request('city'),
				"eventId"         => request('occasion'),
				"selectedEventId" => request('selectedEventId'),
				"budget"          => request('budget'),
				"eventDate"       => request('eventDate'),
				"typeVenueId"     => request('typeVenue'),
				//"flexEventDate"   => request('flexEventDate'),
				"zip_code"        => request('zip'),
				"area_id"         => request('area'),
				"teamNotes"       => request('teamNotes'),
				"eventSlot"       => request('eventSlot'),
				"specificEvent"   => request("childEvents"),
				"babyGender"      => request("kidsBirthdayGender"),
				"email"           => request('email')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (!$res["success"])
		{
			return redirect()->back()->with('errorMsg', $res["error"])->withInput();
		}

		if (request("isClose") == 1)
		{
			return redirect(route("ticket.details.info", [$res['ticketId']]));
		}
		elseif ($isEdit == 1)
		{
			return redirect(route("ticket.modify.edit.services", [$res['ticketId'], 1]));
		}
		else
		{
			return redirect(route("ticket.create.edit.services", $res['ticketId']));
		}
	}

	public function editAdditionalPartyDetails($ticketId, $isEdit = "0")
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/additional/' . $ticketId;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);
		$res["isEdit"] = ($isEdit == 0) ? 0 : 1;

		$parentEventId = $res['parentIdOfEventId'];

		if ($parentEventId == config("evibe.event.kids_birthday"))
		{
			return view('tickets.capture-requirements.additional_party_requirements', ["data" => $res]);
		}
		else
		{
			if ($isEdit == 1)
			{
				return redirect(route("ticket.modify.edit.services", [$ticketId, 1]));
			}
			else
			{
				return redirect(route("ticket.create.edit.services", [$ticketId]));
			}
		}
	}

	public function saveAdditionalPartyDetails($ticketId, $isEdit = "0")
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/additional/' . $ticketId;

		$options = [
			'method'      => $isEdit == 1 ? "PUT" : "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				"childEventId"       => request("childEventId"),
				"kidsBirthdayGender" => request("kidsBirthdayGender"),
				"teamNotes"          => request("teamNotes")
			]
		];
		$res = $this->makeApiCallWithUrl($options);

		if ($res['success'])
		{
			$redirectUrl = route("ticket.create.edit.services", [$ticketId]);
			if (request("isClose") == 1)
			{
				$redirectUrl = route("ticket.details.info", [$res['ticketId']]);
			}
			elseif ($isEdit == 1)
			{
				$redirectUrl = route("ticket.modify.edit.services", [$ticketId, 1]);
			}

			return response()->json([
				                        "success"     => true,
				                        "redirectUrl" => $redirectUrl
			                        ]);
		}
		else
		{
			$redirectUrl = route("ticket.modify.edit.additional.party.details", [$ticketId]);

			return response()->json([
				                        "success"     => false,
				                        "error"       => $res["error"],
				                        "redirectUrl" => $redirectUrl
			                        ]);
		}
	}

	public function editServices($ticketId, $isEdit = "0")
	{
		$ticket = \Ticket::find($ticketId);
		$typeTicket = \TypeTicket::select("id", "name")->get();
		$user = Auth::user();

		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'services/' . $ticketId;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		foreach ($res["services"] as $key => $service)
		{
			$serviceType = $typeTicket->where('id', $service["serviceType"])->first();

			if ($serviceType)
			{
				$res["categorizedServices"][$serviceType->name][] = [
					"id"           => $service["id"],
					"mapTypeId"    => $service["serviceType"],
					"name"         => $service["name"],
					"optionsCount" => $service["optionsCount"],
					"minPrice"     => $service["minPrice"],
					"maxPrice"     => $service["maxPrice"],
					"description"  => $service["description"],
					"isSelected"   => $service["isSelected"]
				];

				$res["selectedServices"][$serviceType->name] = (isset($res["selectedServices"][$serviceType->name]) && ($res["selectedServices"][$serviceType->name] == 1)) ? 1 : ($service["isSelected"] ? 1 : 0);
			}
		}

		$res["ticket"] = $ticket;
		$res["isEdit"] = ($isEdit == 0) ? 0 : 1;

		return view('tickets.capture-requirements.required_services', ["data" => $res]);
	}

	public function saveServices($ticketId, $isEdit = "0")
	{
		$data = request()->all();
		$serviceTags = [];

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'services/' . $ticketId;

		foreach ($data["serviceTags"] as $serviceTag)
		{
			$serviceTags[] = ["id" => $serviceTag];
		}

		$options = [
			'method'      => $isEdit == 1 ? "PUT" : "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				"teamNotes"         => request("teamNotes"),
				"selectedServices"  => $serviceTags,
				"requirementFields" => isset($data["checkoutInputs"]) ? $data["checkoutInputs"] : "",
				"minBudget"         => request("minBudget"),
				"maxBudget"         => request("maxBudget")
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res['success'])
		{
			$redirectUrl = route("ticket.create.edit.contact.info", [$ticketId]);
			if (request("redirectUrl"))
			{
				$redirectUrl = request("redirectUrl");
			}
			elseif (request("isClose") == 1)
			{
				$redirectUrl = route("ticket.details.info", [$res['ticketId']]);
			}
			elseif ($isEdit == 1)
			{
				$redirectUrl = route("ticket.modify.edit.contact.info", [$ticketId, 1]);
			}

			return response()->json([
				                        "success"     => true,
				                        "redirectUrl" => $redirectUrl
			                        ]);
		}
		else
		{
			return response()->json([
				                        "success" => false,
				                        "error"   => $res["error"]
			                        ]);
		}
	}

	public function editContactInfo($ticketId, $isEdit = "0")
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/contact/' . $ticketId;
		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);
		$res["isEdit"] = ($isEdit == 0) ? 0 : 1;
		$res['ticketId'] = $ticketId;

		return view("tickets.capture-requirements.contact_details", ["data" => $res]);
	}

	public function saveContactInfo($ticketId, $isEdit = "0")
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/contact/' . $ticketId;

		$options = [
			'method'      => $isEdit == 1 ? "PUT" : "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				"phone"                => request("phone"),
				"altPhone"             => request("altPhone"),
				"venueLandmark"        => request("landmark"),
				"teamNotes"            => request("teamNotes"),
				//"nextFollowUp"         => request("nextFollowUp"),
				//"typeFollowup"         => request('typeFollowup'),
				//"closureDate"          => request("closureDate"),
				"popUpEnquiryTimeSlot" => request('popUpEnquiryTimeSlot')
			]
		];

		$res = $this->makeApiCallWithUrl($options);
		if (!$res["success"])
		{
			return redirect()->back()->with('errorMsg', $res["error"])->withInput();
		}
		elseif ($user->role_id == config("evibe.roles.syrow"))
		{
			$this->dispatch(new MailSyrowUpdateTicketToTeamJob(["ticketId" => $ticketId]));

			$ticket = \Ticket::find($ticketId);

			if ($ticket && $ticket->status_id == config("evibe.status.followup"))
			{
				$ticket->update([
					                "status_id" => config("evibe.status.progress")
				                ]);
			}

			return redirect(route("syrow.tickets.list"));
		}
		else
		{
			if (request("isClose") == 1)
			{
				return redirect(route("ticket.details.info", [$res['ticketId']]));
			}
			else
			{
				return redirect(route('ticket.reco.selected.services', $ticketId));
			}
		}
	}

	public function editPartyStatusInfo($ticketId, $isEdit = "0")
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/status/' . $ticketId;

		$options = [

			'method'      => 'GET',
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		$res['isEdit'] = ($isEdit == 0) ? 0 : 1;
		$res['ticketId'] = $ticketId;

		return view('tickets.capture-requirements.status_details', ['data' => $res]);
	}

	public function savePartyStatusInfo($ticketId, $isEdit = "0")
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'details/status/' . $ticketId;

		$options = [
			'method'      => $isEdit == 1 ? "PUT" : "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				"status"               => request("status"),
				"nextFollowUp"         => request("nextFollowUp"),
				"ticketSource"         => request('ticketSource'),
				"sourceSpecific"       => request('sourceSpecific'),
				"teamNotes"            => request('teamNotes'),
				"altPhone"             => request('altPhone'),
				"altEmail"             => request('altEmail'),
				"decisionMaker"        => request('decisionMaker'),
				"popUpEnquiryTimeSlot" => request('popUpEnquiryTimeSlot'),
				"typeFollowup"         => request('typeFollowup')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (!$res["success"])
		{
			return redirect()->back()->with('errorMsg', $res["error"])->withInput();
		}
		elseif (request("isClose") == 1)
		{
			return redirect(route("ticket.details.info", [$res['ticketId']]));
		}
		else
		{
			return redirect(route('ticket.reco.selected.services', $ticketId));
		}
	}

	public function getTicketBookingCheckoutFields($ticketId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'services/requirements/' . $ticketId . '?bookingType=' . request("typeTicketBooking") . '&eventId=' . request("ticketEventId");

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		return view('tickets.util.ticket_booking_checkout_fields', ["data" => $res]);
	}

	public function makeApiCallWithUrl($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			return $res;

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;
			Log::error($res['error']);

			return false;
		}
	}
}