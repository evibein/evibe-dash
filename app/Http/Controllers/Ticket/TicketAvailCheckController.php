<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;
use App\Models\AvailCheck\AvailabilityCheckGallery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TicketAvailCheckController extends BaseTicketController
{
	public function getOptionsByTypeTicket($ticketId)
	{
		$ticket = \Ticket::find($ticketId);
		$ticketEvent = $ticket->event_id ?: false;
		$cityId = $ticket->city_id;

		if ($ticket)
		{
			$optionId = request("optionId");

			$user = Auth::user();
			$accessToken = $this->getAccessTokenFromUserId($user);

			$options = [
				"url"         => config("evibe.api.base_url") . "avail-checks/get-options",
				"method"      => "GET",
				"accessToken" => $accessToken,
				"jsonData"    => [
					"optionId" => $optionId,
					"eventId"  => $ticketEvent,
					"cityId"   => $cityId
				]
			];

			$mappingValues = $this->makeGuzzleRequest($options);

			return view("tickets.details.mapping.type-ticket-options", ["mappingValues" => $mappingValues]);
		}
	}

	public function fetchAvailCheckOptionInfo($ticketId)
	{

		$data = Request()->all();

		if ($data)
		{
			$user = Auth::user();
			$accessToken = $this->getAccessTokenFromUserId($user);

			$options = [
				"url"         => config("evibe.api.base_url") . "avail-checks/get-option-info",
				"method"      => "GET",
				"accessToken" => $accessToken,
				"jsonData"    => $data
			];

			$data = $this->makeGuzzleRequest($options);

			return $data;
		}

	}

	public function createAvailCheck($ticketId)
	{
		$data = Request()->all();

		if ($data)
		{
			$user = Auth::user();
			$accessToken = $this->getAccessTokenFromUserId($user);

			// validate uploaded images here
			// save uploaded images here after the availability check is created
			// validate image links in API
			// save image links after teh availability check is created

			$invalidImages = [];
			if (isset($data['photos']))
			{
				// validation for upload image count
				/*if (count($data['photos']) > 12)
				{
					Log::info("upload limit exceeded");
					$response = [
						'success'  => false,
						'errorMsg' => "uploaded images must be less than 13"
					];

					Log::info("upload limit exceeded");
					return $response;
				}
				*/

				$images = $data['photos'];

				if (count($images) > 0)
				{
					foreach ($images as $image)
					{
						//Log::info(" checking image");

						$imageRules = [
							'file' => 'required|mimes:png,jpeg,jpg,JPG|max:4096'
						];

						$imageMessages = [
							'file.mimes' => 'One of the images/video is not valid. (only .png, .jpeg, .jpg, are accepted)',
							'file.max'   => 'Image size cannot be more than 4 MB'
						];

						$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

						if ($imageValidator->fails())
						{
							$failedRules = $imageValidator->failed();

							//to be tested
							if (isset($failedRules['file']['mimes']))
							{
								$response = [
									'success'    => false,
									'imageError' => true,
									'imageName'  => $image->getClientOriginalName(),
									'errorMsg'   => $imageValidator->messages()->first()
								];

								return $response;
							}
							else
							{
								array_push($invalidImages, $image->getClientOriginalName());
							}
						}

					}

				}
			}

			if (count($invalidImages))
			{
				$response = [
					'success'    => false,
					'imageError' => true,
					'imageName'  => $invalidImages,
					'errorMsg'   => "Image size cannot be more than 4 MB"
				];

				return $response;
			}

			$options = [
				"url"         => config("evibe.api.base_url") . "avail-checks/post-avail-check",
				"method"      => "POST",
				"accessToken" => $accessToken,
				"jsonData"    => $data
			];

			$data = $this->makeGuzzleRequest($options);

			if ($data['success'])
			{
				if (isset($images))
				{
					$uploadedImages = $this->uploadImages($images, $data['availCheckId'], $ticketId);

					$imageUpload = AvailabilityCheckGallery::insert($uploadedImages);

					if (!$imageUpload)
					{
						$response = [
							'success' => false,
							'msg'     => "failed to upload local images"
						];

						return $response;
					}
				}
			}

			return $data;
		}
	}

	protected function uploadImages($images, $id, $ticketId)
	{
		$directPath = "/ticket/$ticketId/images/avail-check/$id/";
		$uploadedImageLInks = [];
		foreach ($images as $image)
		{
			$option = [
				'isWatermark' => false,
				'isOptimize'  => false,
			];

			$imageName = $this->uploadImageToServer($image, $directPath, $option);

			$imgInsertData = [
				'url'                   => $imageName,
				'title'                 => $this->getImageTitle($image),
				'type_id'               => config('evibe.gallery.type.image'),
				'availability_check_id' => $id
			];
			array_push($uploadedImageLInks, $imgInsertData);
		}

		return $uploadedImageLInks;

	}
}