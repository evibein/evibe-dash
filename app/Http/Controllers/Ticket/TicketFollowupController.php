<?php

namespace App\Http\Controllers\Ticket;

use App\Models\TicketFollowup;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TicketFollowupController extends TicketUtilController
{
	/**
	 * @author Vikash <vikash@evibe.in>
	 * @since  18th April 2016
	 *
	 * Ticket followup module
	 */
	public function saveFollowup($ticketId)
	{
		$followupDate = Input::get('followupDate');
		$followupComments = Input::get('followupComment');
		$userId = Auth::user()->id;
		$redirectUrl = route('ticket.details.updates', $ticketId) . '#followup-section';
		$closureDate = Input::get('updateClosureDate');

		$rules = [
			'followupDate'      => 'required|date|after:now',
			'updateClosureDate' => 'date|after:now'
		];
		$messages = [
			'followupDate.required' => 'Followup Date is required',
			'followupDate.date'     => 'Followup Date is invalid',
			'followupDate.after'    => 'Followup Date should be after current date',
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect($redirectUrl)->withErrors($validator, 'followupError')
			                             ->withInput();
		}
		$followupData = [
			'followup_date' => strtotime($followupDate),
			'comments'      => $followupComments,
			'ticket_id'     => $ticketId,
			'handler_id'    => $userId
		];

		$isFollowup = TicketFollowup::where('ticket_id', $ticketId)
		                            ->whereNull('is_complete')
		                            ->first();

		if ($isFollowup)
		{
			$saveFollowup = $isFollowup->update($followupData);
		}
		else
		{
			$saveFollowup = TicketFollowup::create($followupData);
		}

		if ($saveFollowup)
		{
			$ticket = \Ticket::find($ticketId);
			$ticketUpdateComment = $followupComments . ' at ' . date('d/m/Y', strtotime($followupDate));
			if (strtotime($closureDate) != $ticket->expected_closure_date && $closureDate)
			{
				$ticket->expected_closure_date = strtotime($closureDate);
				$ticket->update();
			}
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => ($ticket->status_id == config('evibe.status.no_response')) ? config('evibe.status.no_response') : config('evibe.status.followup'),
				'comments' => $ticketUpdateComment
			];

			$this->setCreatedHandlerId($ticket, $userId);
			$this->updateTicketAction($ticketUpdate);
		}

		return redirect($redirectUrl)->with('followupSuccess', 'Ticket followup scheduled successfully');
	}

	public function completeFollowup($followupId)
	{
		$res = ['success' => false];
		$followup = TicketFollowup::find($followupId);
		$comment = Input::get('doneComment');

		if ($followup)
		{
			$updateData = [
				'handler_id'   => Auth::user()->id,
				'is_complete'  => 1,
				'done_comment' => $comment
			];
			if ($followup->update($updateData))
			{

				$ticketUpdateComment = 'Followups marked as done';

				$ticketUpdate = [
					'ticket'   => $followup->ticket,
					'statusId' => config('evibe.status.followup'),
					'comments' => $ticketUpdateComment
				];
				$this->updateTicketAction($ticketUpdate);

				$res = ['success' => true];
			}
		}
		else
		{
			$res = [
				'success' => false,
				'error'   => 'Followup data not found'
			];
		}

		return response()->json($res);
	}

	public function updateFollowup($followupId)
	{
		$followup = TicketFollowup::find($followupId);
		$res['success'] = false;

		if ($followup)
		{
			$rules = [
				'date' => 'required|date|after:now',
			];
			$messages = [
				'date.required' => 'Followup Date is required',
				'date.date'     => 'Followup Date is invalid',
				'date.after'    => 'Followup Date should be after current date',
			];

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validator->messages()->first()
				                        ]);
			}

			$updateData = [
				'followup_date' => strtotime(Input::get('date')),
				'comments'      => Input::get('comment')
			];

			if ($followup->update($updateData))
			{
				$res['success'] = true;
			}
		}
		else
		{
			$res = [
				'success' => false,
				'error'   => "Followup data not found"
			];
		}

		return response()->json($res);
	}

	// saving the ticket followup from the modal after recommendation email,
	// custom quote, exchange contact,venue availability check

	public function savePopUpFollowUp($ticketId)
	{
		$res = ['success' => false];
		$allInput = Input::all();
		$followupDate = Input::get('date');
		$followupComments = Input::get('comment');
		$followupType = Input::get('followupType') ? trim(Input::get('followupType')) : null;
		$closureDate = Input::get('recommendationClosureDate');

		$rules = [
			'date'                      => 'required|date|after:now',
			'recommendationClosureDate' => 'sometimes|required|date|after:now'
		];
		$messages = [
			'date.required'                   => 'Followup Date is required',
			'date.date'                       => 'Followup Date is invalid',
			'date.after'                      => 'Followup Date should be after current date',
			'recommendationClosureDate.date'  => "Closure Date is invalid",
			'recommendationClosureDate.after' => 'Closure Date Should be after current date'
		];

		if ($followupType)
		{
			$parsedDate = Carbon::parse($followupDate)->format('G');
			$allInput['time'] = $parsedDate;
			$rules['time'] = 'integer|between:8,21';
			$messages['time.between'] = 'Followup time should be in between 8 AM to 10 PM.';
		}

		$validator = Validator::make($allInput, $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'error'   => $validator->messages()->first(),
				                        'success' => false
			                        ]);
		}

		$followupData = [
			'followup_date' => strtotime($followupDate),
			'comments'      => $followupType,
			'ticket_id'     => $ticketId,
			'handler_id'    => Auth::user()->id,
			'followup_type' => $followupType
		];

		$isFollowup = TicketFollowup::where('ticket_id', $ticketId)->whereNull('is_complete')->first();

		if ($isFollowup)
		{
			$saveFollowup = $isFollowup->update($followupData);
		}
		else
		{
			$saveFollowup = TicketFollowup::create($followupData);
		}

		if ($saveFollowup)
		{
			$ticketUpdateComment = "[" . $followupType . "] " . $followupComments . ' at ' . date('d/m/Y', strtotime($followupDate));

			$ticket = \Ticket::find($ticketId);

			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.followup'),
				'comments' => $ticketUpdateComment
			];
			if (strtotime($closureDate) != $ticket->expected_closure_date && $closureDate)
			{
				$ticket->expected_closure_date = strtotime($closureDate);
				$ticket->update();
			}
			$this->updateTicketAction($ticketUpdate);

			// make API call to initiate auto follow-ups
			$user = Auth::user();
			$accessToken = $this->getAccessTokenFromUserId($user);
			$url = config('evibe.api.base_url') . config('evibe.api.auto-followups.prefix') . $ticketId . '/create/' . config('evibe.type_reminder.followup');

			$options = [
				'method'      => "POST",
				'url'         => $url,
				'accessToken' => $accessToken,
				'jsonData'    => []
			];

			$res = $this->makeApiCallWithUrl($options);

			if (isset($res['success']) && !$res['success'])
			{
				$error = "Some error occurred while creating a auto followup reminder for ticket: $ticketId";
				if (isset($res['error']))
				{
					$error = $res['error'];
				}

				Log::error($error);
			}

			$res = ['success' => true];
		}

		return response()->json($res);
	}

	public function deleteFollowup($followupId)
	{
		$followup = TicketFollowup::find($followupId);
		$res['success'] = false;

		if ($followup)
		{
			if ($followup->delete())
			{
				$res['success'] = true;
			}

			return response()->json($res);
		}
		else
		{
			$res = [
				'success' => false,
				'error'   => 'followup data not found'
			];
		}

		return response()->json($res);
	}
}
