<?php

namespace App\Http\Controllers\Ticket;

use App\Jobs\Emails\Ticket\MailOrderProcessToCustomer;
use App\Jobs\Emails\Ticket\MailPreBookToVendor;
use App\Models\Util\CheckoutFieldValue;
use Carbon\Carbon;
use Evibe\Facades\AppUtilFacade as AppUtil;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;

class TicketOrderController extends TicketUtilController
{
	/**
	 * Send order process email / SMS
	 *
	 * If advance for all the ticket booking were paid
	 * then a booking email is sent other wise order process alerts
	 * will be sent
	 *
	 * @since 2 Feb 2015
	 * @todo
	 *    + generate PDF document
	 *    + update publicLink in AppUtil::fillMappingValues() method
	 */
	public function sendOrderProcessAlerts($ticketId)
	{
		$res = $this->sendOrderProcessNotifications($ticketId);

		return response()->json($res);
	}

	public function sendOrderProcessNotifications($ticketId)
	{
		$ticket = \Ticket::with('bookings.mapping', 'bookings.provider')->findOrFail($ticketId);
		$res = ['success' => true];
		$ccAddresses = [
			//Auth::user()->username,
			config('evibe.ops_alert_no_action_email')
		];

		if ($ticket->handler)
		{
			array_push($ccAddresses, $ticket->handler->username);
		}

		// get list of all bookings
		$ticketBookings = $ticket->bookings;

		if (!count($ticketBookings))
		{
			$res = [
				'success' => false,
				'error'   => "No orders finalized, cannot send order process email. Please first finalize an order."
			];
		}

		$validateData = [
			'Customer name'  => $ticket->name,
			'Customer email' => $ticket->email,
			'party date'     => $ticket->event_date,
			'location'       => $ticket->area,
			'pin code'       => $ticket->zip_code,
			//'landmark'       => $ticket->venue_landmark
		];

		foreach ($validateData as $fieldName => $fieldValue)
		{
			if (!$fieldValue)
			{
				$res = [
					'success' => false,
					'error'   => "$fieldName is required."
				];

				return $res;
			}
		}

		$partyDateTime = date("d/m/y H:i", $ticket->event_date);
		$partyDate = date("d/m/y", $ticket->event_date);

		// bookings available
		$data = [
			'ticketId'   => [$ticketId],
			'enquiryId'  => $ticket->enquiry_id,
			'customer'   => [
				'name'  => ucfirst($ticket->name),
				'email' => $ticket->email,
				'phone' => $ticket->phone,
			],
			'additional' => [
				'partyDateTime' => $partyDateTime,
				'eventId'       => $ticket->event_id,
				'eventName'     => $ticket->event_id ? $this->getEventName($ticket->event_id) : '',
				'babyName'      => $ticket->baby_name,
				'venueAddress'  => $ticket->venue_address,
				'venueLandmark' => $ticket->venue_landmark
			],
			'bookings'   => [
				'list' => []
			]
		];

		// adding dynamic checkout fields value
		$checkoutFields = $this->getCheckoutField($ticket);
		foreach ($checkoutFields as $checkoutField)
		{
			$checkoutValue = $checkoutField->ticketCheckoutValue($ticketId);

			$notApplicable = $checkoutField->is_crm && strtolower($checkoutValue) == "n/a" ? true : false;
			if ($checkoutValue && !$notApplicable)
			{
				$data['additional'][$checkoutField->name] = $checkoutValue;
			}
		}

		$data['ticketId'] = $ticketId;
		$totalBookingAmount = 0;
		$totalAdvanceToPay = 0;

		$data['additional']['partyDateTime'] = $partyDateTime;
		$data['additional']['partyDate'] = $partyDate;

		foreach ($ticketBookings as $booking)
		{
			$hasShowVenueName = false;
			$mapping = $booking->mapping;
			$mapTypeId = $mapping->map_type_id;
			$mapId = $mapping->map_id;
			$mappedValues = AppUtil::fillMappingValues([
				                                           'isCollection' => false,
				                                           'mapId'        => $mapId,
				                                           'mapTypeId'    => $mapTypeId
			                                           ]);

			if ($booking->is_venue_booking && ($mapTypeId == config('evibe.ticket_type.packages')) && $this->isVenueDeal($mapId))
			{
				$hasShowVenueName = true;
			}

			$data['bookings']['list'][] = [
				'bookingId'        => $booking->booking_id,
				'bookingInfo'      => $booking->booking_info,
				'bookingAmount'    => $booking->booking_amount,
				'advanceAmount'    => $booking->advance_amount,
				'advancePaid'      => $booking->is_advance_paid,
				'isVenue'          => $booking->is_venue_booking,
				'hasCustomization' => $booking->has_customization,
				'hasShowVenueName' => $hasShowVenueName,
				'prerequisites'    => $booking->prerequisites,
				'facts'            => $booking->facts,
				'partyDateTime'    => $booking->party_date_time,
				'partyEndTime'     => $booking->party_end_time,
				'type'             => $mappedValues['type'],
				'code'             => $mappedValues['code'],
				'name'             => $mappedValues['name'],
				'link'             => $mappedValues['publicLink'],
				'provider'         => (isset($mappedValues['provider']) && $mappedValues['provider']) ? $mappedValues['provider'] : null,
				'booking'          => $booking
			];

			$totalBookingAmount += $booking->booking_amount;
			$totalAdvanceToPay += ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;
		}

		$data['bookings']['minBookingDateTime'] = $ticketBookings->min('party_date_time');
		$data['bookings']['maxBookingDateTime'] = $ticketBookings->max('party_date_time');
		$partyDateTime = $this->getPartyDateTimeByBookingsPartyTime($data['bookings']['minBookingDateTime'], $data['bookings']['maxBookingDateTime']);
		$data['partyDateTime'] = $partyDateTime;

		$data['bookings']['totalBookingAmount'] = $totalBookingAmount;
		$data['bookings']['totalAdvanceToPay'] = $totalAdvanceToPay;
		$location = $ticket->area ? $ticket->area->name : null;
		$customDeadline = request("deadlineHours") ? request("deadlineHours") : Carbon::now()->addHour(3)->toDateTimeString();
		$customDeadline = date("Y/m/d H:i", strtotime($customDeadline));
		$customDeadline = Carbon::createFromFormat('Y/m/d H:i', $customDeadline)->timestamp;
		$deadline = $customDeadline;

		if ($totalAdvanceToPay == 0)
		{
			// all bookings have advance paid. Order in process in not the correct email
			$res = [
				'success' => false,
				'error'   => "Advance has been paid by customer, can't send order process email."
			];
		}
		elseif (is_null($location))
		{
			// issue with ticket location
			$res = [
				'success' => false,
				'error'   => 'Location for ticket not found. Pls add location to ticket.'
			];
		}
		elseif ($customDeadline < Carbon::now()->timestamp)
		{
			$res = [
				'success' => false,
				'error'   => "Entered Deadline time is less than Current Time or Wrong input format ."
			];
		}
		elseif ($customDeadline > $ticketBookings->min('party_date_time'))
		{
			$res = [
				'success' => false,
				'error'   => "Entered Deadline time is more than Party Time or Wrong input format ."
			];
		}
		else
		{

			// generate payment link
			$paymentLink = $this->getPaymentLink($ticket->id, $ticket->email);

			$partnerCcAddresses = [config('evibe.business_group_email')];

			$data['bookings']['paymentLink'] = $paymentLink['getFullUrl'];
			$data['ccAddresses'] = $ccAddresses; // get ccAddresses

			$res['paymentLink'] = $paymentLink['getShortUrl'];

			// send order under process email, SMS to all vendors

			foreach ($ticketBookings as $booking)
			{
				$vendor = $booking->provider;

				$vendorEmailData = [
					'ticketId'      => $ticketId,
					'to'            => $vendor->email,
					'ccAddresses'   => $partnerCcAddresses,
					'partyDateTime' => date("d-M-Y, h:i A \(D\)", $booking->party_date_time),
					'partyDate'     => date("d-M-Y", $booking->party_date_time),
					'vendorName'    => ucfirst($vendor->person),
					'bookingInfo'   => $booking->booking_info,
					'customerName'  => ucfirst($ticket->name),
					'prerequisites' => $booking->prerequisites,
					'facts'         => $booking->facts,
					'isVenue'       => $booking->is_venue_booking,
					'partyLocation' => $location,
					'landmark'      => $ticket->venue_landmark,
					'zipCode'       => $ticket->zip_code,
					'booking'       => $booking,
					'additional'    => $data['additional']
				];

				$smsData = [];
				$vendorSMSTpl = config('evibe.sms_tpl.pre_book.vendor');
				$vendorSMSReplaces = [
					'#field1#' => ucfirst($vendor->person),
					'#field2#' => date("d-M-y", $booking->party_date_time),
					'#field3#' => ucfirst($ticket->name),
					'#field4#' => $location,
					'#field5#' => AppUtil::formatPrice($booking->advance_amount)
				];

				$smsData[$vendor->phone] = str_replace(array_keys($vendorSMSReplaces), array_values($vendorSMSReplaces), $vendorSMSTpl);
				$this->dispatch(new MailPreBookToVendor($vendorEmailData));
				/*Queue::push('\Evibe\Utilities\SendEmail@mailPreBookToVendor', $vendorEmailData);*/
				Queue::push('\Evibe\Utilities\SendSMS@smsPreBookToVendor', $smsData);
			}

			$smsPartyDateTime = $data['additional']['partyDate'];
			if (isset($data['bookings']['minBookingDateTime']) && isset($data['bookings']['maxBookingDateTime']))
			{
				$smsPartyDateTime = date("d M Y", $data['bookings']['minBookingDateTime']);

				if ($data['bookings']['minBookingDateTime'] == $data['bookings']['maxBookingDateTime'])
				{
					$smsPartyDateTime = date("d M Y h:i A", $data['bookings']['minBookingDateTime']);
				}
				elseif (date("d m y", $data['bookings']['minBookingDateTime']) != date("d m y", $data['bookings']['maxBookingDateTime']))
				{
					$smsPartyDateTime = date("d/m/y (D)", $data['bookings']['minBookingDateTime']) . ' - ' . date("d/m/y (D)", $data['bookings']['maxBookingDateTime']);
				}
			}

			$customerSMSTpl = config('evibe.sms_tpl.pre_book.customer');
			$customerSMSReplaces = [
				'#field1#' => $data['customer']['name'],
				'#field2#' => $smsPartyDateTime,
				'#field3#' => 'Rs. ' . AppUtil::formatPrice($data['bookings']['totalAdvanceToPay']),
				'#field4#' => date("d/m/y h:i A", $deadline),
				'#field5#' => $paymentLink['getShortUrl']
			];

			$smsData = [
				'to'   => $data['customer']['phone'],
				'text' => str_replace(array_keys($customerSMSReplaces), array_values($customerSMSReplaces), $customerSMSTpl)
			];

			// notify customer
			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email); // alt email in CC
			}
			$data['ccAddresses'] = $ccAddresses;
			$data['bookings']['deadline'] = date("d/m/y h:i A", $deadline);

			$resEmail = $this->validateEmail($data['customer']['email'], $ticket, "Customer");

			if (!$resEmail['success'])
			{
				return $resEmail;
			}

			$resPhone = $this->validatePhone($smsData['to'], $ticket, "Customer");

			if (!$resPhone['success'])
			{
				return $resPhone;
			}

			/*Queue::push('\Evibe\Utilities\SendEmail@mailOrderProcessToCustomer', $data);*/
			$this->dispatch(new MailOrderProcessToCustomer($data));
			Queue::push('\Evibe\Utilities\SendSMS@smsPreBookToCustomer', $smsData);
			// update timestamp
			$ticket->update([
				                'is_auto_booked'         => 0, // nullify if previously initiated auto booking
				                'success_email_sent_at'  => date('Y-m-d H:i:s'),
				                'asked_order_details_at' => date('Y-m-d H:i:s'),
				                'payment_deadline'       => $deadline,
				                'success_email_type'     => config('evibe.success_type.confirmed'),
				                'updated_at'             => date('Y-m-d H:i:s')
			                ]);

			// ticket action updated
			$ticketUpdate = [
				'statusId' => config('evibe.status.confirmed'),
				'ticket'   => $ticket,
				'comments' => config('evibe.ticket_status_message.order_process')
			];
			$this->updateTicketAction($ticketUpdate);
		}

		return $res;
	}

	/**
	 * using sendBookedNotifications method of PaymentController
	 */
	public function sendReceipt($ticketId)
	{
		$response = ['success' => false];
		$ticket = \Ticket::find($ticketId);

		if ($ticket)
		{
			$validateData = [
				'name'       => $ticket->name,
				'email'      => $ticket->email,
				'party date' => $ticket->event_date,
				'location'   => $ticket->area,
				'pin code'   => $ticket->zip_code,
				//'landmark'   => $ticket->venue_landmark
			];

			foreach ($validateData as $fieldName => $fieldValue)
			{
				if (!$fieldValue)
				{
					$res = [
						'success' => false,
						'error'   => "$fieldName is required."
					];

					return response()->json($res);
				}
			}

			$handlerId = Auth::user()->id;
			$host = config('evibe.live.host');
			$link = "$host/pay/process/$ticketId?isFromDash=1&handlerId=$handlerId";

			$response = $this->sendCurlRequest($link);
		}

		return response()->json($response);
	}

	/**
	 * @author : Vikash <vikash@evibe.in>
	 * @since  : 1 September 2016
	 *
	 * @param      $ticketId
	 * send updated receipt to vendor
	 *
	 */
	public function sendUpdatedReceipt($ticketId)
	{
		$bookingIds = [];
		$response = ['success' => false];
		$bookingData = Input::get('bookingData');

		foreach ($bookingData as $booking)
		{
			if ($booking['id'] && $booking['message'])
			{
				$ticketBooking = \TicketBooking::findOrFail($booking['id']);
				if ($ticketBooking->update(['updated_message' => $booking['message']]))
				{
					$bookingIds[] = $booking['id'];
				}
			}
		}

		if (count($bookingIds) > 0)
		{
			$handlerId = Auth::user()->id;
			$host = config('evibe.live.host');
			$bookingIdsString = implode(",", $bookingIds);
			$link = "$host/pay/update-receipt/$ticketId?handlerId=$handlerId&bookingIds=$bookingIdsString";

			try
			{
				$ch = curl_init($link);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				$cUrlResponse = json_decode(curl_exec($ch), true);
				if ($cUrlResponse['success'])
				{
					$response['success'] = true;
				}
				else
				{
					$response['error'] = isset($cUrlResponse['error']) ? $cUrlResponse['error'] : '';
				}

				curl_close($ch);
			} catch (\Exception $e)
			{
				$this->sendErrorReport($e);
			}
		}
		else
		{
			$response['error'] = "No booking has selected to send the update (some glitch happened)";
		}

		return $response;
	}

	// for service auto booking
	public function confirmServiceAutoBooking($ticketId, $isResend = false)
	{
		$ticket = \Ticket::with('bookings.mapping')->find($ticketId);
		$handlerId = Auth::user()->id;
		$token = Hash::make($ticket->phone . $ticket->created_at);

		$ticketBooking = $ticket->bookings[0];

		if ($ticketBooking->map_id && $ticketBooking->map_type_id == config('evibe.ticket_type.planners'))
		{
			$apiNotificationBase = config('evibe.api.base_url') . config('evibe.api.pay_notification');
			$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.services') . '&typeTicketId=' . config('evibe.ticket_type.entertainments') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
			if ($isResend)
			{
				$url .= '&isResend=1';
			}
			$response = $this->sendCurlRequest($url);

			return response()->json($response);
		}
		else
		{
			return response()->json(['error' => 'Please map a planner and try again']);
		}
	}

	// making curl request on main for confirm booking
	public function confirmAutoBooking($ticketId, $isResend = false)
	{
		$response = $this->confirmAutoBookingRequest($ticketId, $isResend);

		return response()->json($response);
	}

	public function confirmAutoBookingRequest($ticketId, $isResend)
	{
		$ticket = \Ticket::find($ticketId);
		$handlerId = Auth::user()->id;

		$token = Hash::make($ticket->phone . $ticket->created_at);
		$type = $ticket->type_ticket_id;

		$apiNotificationBase = config('evibe.api.base_url') . config('evibe.api.pay_notification');

		$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.packages') . '&typeTicketId=' . config('evibe.ticket_type.generic-package') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;

		switch ($type)
		{
			case config('evibe.ticket_type.venue-deals'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.packages') . '&typeTicketId=' . config('evibe.ticket_type.venue-deals') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
			case config('evibe.ticket_type.couple-experiences'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.packages') . '&typeTicketId=' . config('evibe.ticket_type.couple-experiences') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
			case config('evibe.ticket_type.lounge'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.packages') . '&typeTicketId=' . config('evibe.ticket_type.lounge') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
			case config('evibe.ticket_type.villa'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.packages') . '&typeTicketId=' . config('evibe.ticket_type.villa') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
			case config('evibe.ticket_type.resort'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.packages') . '&typeTicketId=' . config('evibe.ticket_type.resort') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
			case config('evibe.ticket_type.generic-package'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.packages') . '&typeTicketId=' . config('evibe.ticket_type.generic-package') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
			case config('evibe.ticket_type.cakes'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.cakes') . '&typeTicketId=' . config('evibe.ticket_type.cakes') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
			case config('evibe.ticket_type.decors'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.decors') . '&typeTicketId=' . config('evibe.ticket_type.decors') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
			case config('evibe.ticket_type.services'):
			case config('evibe.ticket_type.entertainments'):
				$url = $apiNotificationBase . 'send/' . $ticketId . '?isFromDash=1&mapTypeId=' . config('evibe.ticket_type.services') . '&typeTicketId=' . config('evibe.ticket_type.entertainments') . '&type=1&handler_id=' . $handlerId . '&token=' . $token;
				break;
		}
		if ($isResend)
		{
			$url .= '&isResend=1';
		}

		$response = $this->sendCurlRequest($url);
		$this->setCreatedHandlerId($ticket, $handlerId);

		return $response;
	}

	public function cancelAutoBooking($ticketId)
	{
		$ticket = \Ticket::find($ticketId);
		$res = ['success' => false];
		$bookings = $ticket->bookings()->get();

		if (!count($bookings))
		{
			$res = [
				'success' => false,
				'error'   => 'No booking found for this ticket'
			];
		}

		$advanceAmount = $bookings->sum('advance_amount');
		$primaryBooking = null;
		$primaryMapping = null;
		$primaryProductData = [];

		foreach ($bookings as $booking)
		{
			$mapping = $booking->mapping;

			$productData = AppUtil::fillMappingValues([
				                                          'mapId'        => $mapping->map_id,
				                                          'mapTypeId'    => $mapping->map_type_id,
				                                          'isCollection' => false
			                                          ]);

			if (isset($productData['mapTypeId']) && ($productData['mapTypeId'] != config('evibe.ticket_type.add-on')))
			{
				$primaryBooking = $booking;
				$primaryMapping = $mapping;
				$primaryProductData = $productData;
				break;
			}
		}

		if (!$primaryBooking)
		{
			$primaryBooking = $bookings->first();
			$primaryMapping = $primaryBooking->mapping;
			$primaryProductData = AppUtil::fillMappingValues([
				                                                 'mapId'        => $primaryMapping->map_id,
				                                                 'mapTypeId'    => $primaryMapping->map_type_id,
				                                                 'isCollection' => false
			                                                 ]);
		}

		// send email to customer
		$subject = $ticket->name . ', booking for ' . date("d M Y", $ticket->event_date) . ' is cancelled';

		$ccAddress = [
			Auth::user()->username,
			config('evibe.enquiry_group_email'),
			config('evibe.business_group_email')
		];

		if ($ticket->handler)
		{
			array_push($ccAddress, $ticket->handler->username);
		}

		$emailData = [
			'to'            => [$ticket->email],
			'replyTo'       => [config('evibe.enquiry_group_email')],
			'ccAddresses'   => $ccAddress,
			'subject'       => $subject,
			'customerName'  => $ticket->name,
			'enqId'         => $ticket->enquiry_id,
			'itemInfo'      => $primaryBooking['booking_info'],
			'checkIn'       => date("d M Y", $ticket->event_date),
			'bookingDate'   => date("d M Y", strtotime($ticket->created_at)),
			'advanceAmount' => $advanceAmount
		];

		$smsTpl = config('evibe.sms_tpl.auto_cancel.normal');

		$smsTplData = [
			'#field1#' => $ticket->name,
			'#field2#' => $primaryProductData['name'],
			'#field3#' => date("d-M-y", $ticket->event_date),
			'#field4#' => AppUtil::formatPrice($advanceAmount),
			'#field5#' => $ticket->email,
		];

		$smsText = str_replace(array_keys($smsTplData), array_values($smsTplData), $smsTpl);

		$smsData = [
			'to'   => $ticket->phone,
			'text' => $smsText
		];

		$resEmail = $this->validateEmail($emailData['to'], $ticket, "Customer");

		if (!$resEmail['success'])
		{
			return response()->json($resEmail);
		}

		$resPhone = $this->validatePhone($smsData['to'], $ticket, "Customer");

		if (!$resPhone['success'])
		{
			return response()->json($resPhone);
		}

		Queue::push('\Evibe\Utilities\SendEmail@mailAutoBookCancellationCustomer', $emailData);
		Queue::push('\Evibe\Utilities\SendSMS@smsAutoBookCancelToCustomer', $smsData);

		if ($emailData && $smsData && $ticket->update(['is_auto_cancellation_mail' => 1]))
		{
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.auto_cancel'),
				'comments' => 'Ticket auto cancelled from dash by ' . Auth::user()->name
			];

			if ($ticket->status_id == config('evibe.status.service_auto_pay'))
			{
				$ticketUpdate['serviceAutoCancel'] = true;
			}

			$this->setCreatedHandlerId($ticket);
			$this->updateTicketAction($ticketUpdate);

			// trigger 'cancel ticket booking API' only after status update
			$user = Auth::user();
			$accessToken = $this->getAccessTokenFromUserId($user);

			foreach ($bookings as $booking)
			{
				$url = config('evibe.api.base_url') . config('evibe.api.finance.cancellations.prefix') . '/' . $booking->id . '/cancel';

				$options = [
					'method'      => "PUT",
					'url'         => $url,
					'accessToken' => $accessToken,
					'jsonData'    => [
						'comments' => 'Auto Booking Cancellation in DASH'
					]
				];

				$res = $this->makeApiCallWithUrl($options);

				if (isset($res['success']) && !$res['success'])
				{
					$error = 'Some error occurred while cancelling ticket booking';

					if (isset($res['error']) && $res['error'])
					{
						$error = $res['error'];
					}

					// log_site_error
					$this->saveError([
						                 'fullUrl' => request()->fullUrl(),
						                 'message' => 'Cancel Ticket Booking API - ' . $error,
						                 'code'    => 0,
						                 'details' => $error
					                 ]);
				}
			}

			$res['success'] = true;
		}

		return response()->json($res);
	}

	public function cancelVenueDealAutoBooking($ticketId)
	{
		$ticket = \Ticket::find($ticketId);
		$res = ['success' => false];

		$booking = $ticket->bookings->first();

		if (!$booking)
		{
			$res = [
				'success' => false,
				'error'   => 'No booking found for this ticket'
			];
		}

		//send email to customer
		$mapping = $booking->mapping;

		$autoBookedItem = AppUtil::fillMappingValues([
			                                             'mapId'        => $mapping->map_id,
			                                             'mapTypeId'    => $mapping->map_type_id,
			                                             'isCollection' => false
		                                             ]);

		$partyDate = date("d M Y", $booking->party_date_time);
		$partySlot = $ticket->slot->name;
		$venueName = $autoBookedItem['provider']['name'];
		$venueLocation = $autoBookedItem['provider']['location'];
		$tokenAmount = AppUtil::formatPrice($booking->token_amount);

		$subject = "[Evibe.in] $ticket->name, " . $venueName . " is not available for your party on $partyDate, $partySlot slot";

		$ccAddress = [
			Auth::user()->username,
			config('evibe.enquiry_group_email'),
			config('evibe.business_group_email')
		];

		if ($ticket->handler)
		{
			array_push($ccAddress, $ticket->handler->username);
		}

		if ($ticket->alt_email)
		{
			array_push($ccAddress, $ticket->alt_email);
		}

		$emailData = [
			'to'            => [$ticket->email],
			'replyTo'       => [config('evibe.enquiry_group_email')],
			'ccAddresses'   => $ccAddress,
			'subject'       => $subject,
			'customerName'  => $ticket->name,
			'bookingId'     => $booking->booking_id,
			'venueName'     => $venueName,
			'venueLocation' => $venueLocation,
			'partyDate'     => $partyDate,
			'partySlot'     => $partySlot,
			'tokenAmount'   => $tokenAmount,
		];

		$smsTpl = config('evibe.sms_tpl.auto_cancel.venue_deals');

		$smsTplData = [
			'#field1#' => $ticket->name,
			'#field2#' => str_limit($venueName, 20),
			'#field3#' => $partyDate,
			'#field4#' => $partySlot,
			'#field5#' => $tokenAmount,
			'#field6#' => $ticket->email
		];

		$smsText = str_replace(array_keys($smsTplData), array_values($smsTplData), $smsTpl);

		$smsData = [
			'to'   => $ticket->phone,
			'text' => $smsText
		];

		$resEmail = $this->validateEmail($emailData['to'], $ticket, "Customer");

		if (!$resEmail['success'])
		{
			return response()->json($resEmail);
		}

		$resPhone = $this->validatePhone($smsData['to'], $ticket, "Customer");

		if (!$resPhone['success'])
		{
			return response()->json($resPhone);
		}

		Queue::push('\Evibe\Utilities\SendEmail@mailAutoBookVdCancellationCustomer', $emailData);
		Queue::push('\Evibe\Utilities\SendSMS@smsAutoBookCancelToCustomer', $smsData);

		if ($emailData && $smsData && $ticket->update(['is_auto_cancellation_mail' => 1]))
		{
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.auto_cancel'),
				'comments' => 'Ticket auto cancelled (venue deal) from dash by ' . Auth::user()->name
			];

			$this->updateTicketAction($ticketUpdate);
			$res['success'] = true;
		}

		return response()->json($res);
	}

	public function cancelCakeAutoBooking($ticketId)
	{
		$ticket = \Ticket::find($ticketId);
		$res = ['success' => false];

		$booking = $ticket->bookings->first();
		$mapping = $booking->mapping;;

		$autoBookedItem = AppUtil::fillMappingValues([
			                                             'mapId'        => $mapping->map_id,
			                                             'mapTypeId'    => $mapping->map_type_id,
			                                             'isCollection' => false
		                                             ]);

		if (!$booking)
		{
			$res = [
				'success' => false,
				'error'   => 'No booking found for this ticket'
			];
		}

		//send email to customer
		$partyDate = date("d M Y", $ticket->event_date);
		$weight = $this->getCheckoutFieldByName($ticketId, config('evibe.checkout_field.cake.weight'));
		$slot = $this->getCheckoutFieldByName($ticketId, config('evibe.checkout_field.cake.delivery_slot'));
		$cakeName = $autoBookedItem['name'];

		$subject = "[Evibe.in] $ticket->name, $cakeName order for $partyDate is not available";

		$ccAddress = [
			Auth::user()->username,
			config('evibe.enquiry_group_email'),
			config('evibe.business_group_email')
		];

		if ($ticket->handler)
		{
			array_push($ccAddress, $ticket->handler->username);
		}

		if ($ticket->alt_email)
		{
			array_push($ccAddress, $ticket->alt_email);
		}

		$emailData = [
			'to'            => [$ticket->email],
			'replyTo'       => [config('evibe.enquiry_group_email')],
			'ccAddresses'   => $ccAddress,
			'subject'       => $subject,
			'customerName'  => $ticket->name,
			'bookingId'     => $booking->booking_id,
			'cakeName'      => $cakeName,
			'cakeWeight'    => $weight,
			'partyDate'     => $partyDate,
			'partySlot'     => $slot,
			'advanceAmount' => $booking->advance_amount,
		];

		$smsTpl = config('evibe.sms_tpl.auto_cancel.cake');

		$smsTplData = [
			'#field1#' => $ticket->name,
			'#field2#' => str_limit($cakeName, 20),
			'#field3#' => date('d/m/y', strtotime($partyDate)),
			'#field4#' => $slot,
			'#field5#' => $booking->advance_amount,
			'#field6#' => $ticket->email
		];

		$smsText = str_replace(array_keys($smsTplData), array_values($smsTplData), $smsTpl);

		$smsData = [
			'to'   => $ticket->phone,
			'text' => $smsText
		];

		$resEmail = $this->validateEmail($emailData['to'], $ticket, "Customer");

		if (!$resEmail['success'])
		{
			return response()->json($resEmail);
		}

		$resPhone = $this->validatePhone($smsData['to'], $ticket, "Customer");

		if (!$resPhone['success'])
		{
			return response()->json($resPhone);
		}

		Queue::push('\Evibe\Utilities\SendEmail@mailAutoBookCakeCancellationCustomer', $emailData);
		Queue::push('\Evibe\Utilities\SendSMS@smsAutoBookCancelToCustomer', $smsData);

		if ($emailData && $smsData && $ticket->update(['is_auto_cancellation_mail' => 1]))
		{
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.auto_cancel'),
				'comments' => 'Ticket auto cancelled (cake) from dash by ' . Auth::user()->name
			];

			$this->updateTicketAction($ticketUpdate);
			$res['success'] = true;
		}

		return response()->json($res);
	}

	public function resendAutoBookingReceipt($ticketId)
	{
		$ticket = \Ticket::find($ticketId);

		if ($ticket->type_ticket_id == config('evibe.ticket_type.entertainments'))
		{
			return $this->confirmServiceAutoBooking($ticketId, true);
		}
		else
		{
			return $this->confirmAutoBooking($ticketId, true);
		}
	}

	public function newAdvancePaid($ticketBookingId)
	{
		$ticketBooking = \TicketBooking::findOrFail($ticketBookingId);

		$res = ['success' => true];
		$rules = [
			'advancePaid' => 'required|integer|min:100|max:' . $ticketBooking->booking_amount,
			'paymentRef'  => 'required'
		];

		$messages = [
			'advancePaid.required' => 'Advance paid is required',
			'advancePaid.integer'  => 'Advance paid is not valid (only numbers allowed)',
			'advancePaid.min'      => 'Advance paid cannot be that low',
			'advancePaid.max'      => 'Advance paid cannot be more than booking amount',
			'paymentRef.required'  => 'Payment reference number is required'
		];

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}
		else
		{
			$advanceAmount = Input::get('advancePaid');
			$ticketBooking->update([
				                       'advance_amount'    => $advanceAmount,
				                       'payment_reference' => Input::get('paymentRef'),
				                       'is_advance_paid'   => 1
			                       ]);

			// ticket update action
			$ticketUpdate = [
				'ticket'   => $ticketBooking->ticket,
				'statusId' => $ticketBooking->ticket->status_id,
				'comments' => config('evibe.ticket_status_message.payment_received')
			];
			$this->updateTicketAction($ticketUpdate);
		}

		return response()->json($res);
	}

	private function sendCurlRequest($url)
	{
		$response = ['success' => false];

		try
		{
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$cUrlResponse = json_decode(curl_exec($ch), true);
			if ($cUrlResponse['success'])
			{
				$response['success'] = true;
			}
			else
			{
				$response['error'] = isset($cUrlResponse['error']) ? $cUrlResponse['error'] : '';
			}

			curl_close($ch);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);
		}

		return $response;
	}

	private function getCheckoutFieldByName($ticketId, $fieldId)
	{
		return CheckoutFieldValue::where('ticket_id', $ticketId)->where('checkout_field_id', $fieldId)->value('value');
	}
}
