<?php

namespace App\Http\Controllers\Ticket;

use App\Models\Planner\Planner;
use App\Models\ServiceGallery;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\TypeEvent;
use App\Models\Types\TypeBookingConcept;
use App\Models\Types\TypeTicketBooking;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class QuickBookController extends TicketOrderController
{
	public function fetchQuickBookRequiredData()
	{
		// fetch data based on form

		// probables: (any available value, pre-fill)
		// fetch tickets data [last 30 days]
		$limitDate = Carbon::createFromTimestamp(time())->subMonths(1)->startOfDay()->toDateTimeString();

		$tickets = \Ticket::where('created_at', '>=', $limitDate)
		                  ->whereIn('status_id', [
			                  config('evibe.status.initiated'),
			                  config('evibe.status.progress'),
			                  config('evibe.status.followup')
		                  ])
		                  ->whereNull('deleted_at')
		                  ->orderBy('created_at', 'DESC')
		                  ->get();

		$cities = \City::orderBy('is_default', 'DESC')->get();

		$events = TypeEvent::select("id", "name")->get();

		$typeVenues = \TypeVenue::select("id", "name")
		                        ->where('is_checkout_page', 1)
		                        ->get();

		$areas = \Area::orderBy('city_id', 'DESC')->orderBy('name')->get();

		$typeTicketSources = \TypeTicketSource::orderBy('created_at', 'DESC')->get();

		// fetch product types
		// @see: if planner, venue or artist selected, do not give products to proceed [in ajax request]
		$productTypes = \TypeTicket::all();
		// fetch products data - ajax
		// fetch partners data
		$planners = Planner::where('is_live', 1)->get();

		$venues = \Venue::where('is_live', 1)->get();
		// fetch required booking fields based on product type
		$ticketBookingTypes = TypeTicketBooking::where('for_auto_booking', 0)->get();

		$typeBookingConcepts = TypeBookingConcept::all();

		$data = [
			'ticket'             => $tickets,
			'event'              => $events,
			'city'               => $cities,
			'typeVenue'          => $typeVenues,
			'area'               => $areas,
			'ticketSource'       => $typeTicketSources,
			'productType'        => $productTypes,
			'planner'            => $planners,
			'venue'              => $venues,
			'typeTicketBooking'  => $ticketBookingTypes,
			'typeBookingConcept' => $typeBookingConcepts
		];

		return view('quick-book.booking', ['data' => $data]);
	}

	public function getQuickBookProducts()
	{
		$productTypeId = request('productTypeId');
		$cityId = request('cityId');
		$eventId = request('eventId');
		$parentEvent = TypeEvent::find($eventId);
		if ($parentEvent && $parentEvent->parent_id)
		{
			$eventId = $parentEvent->parent_id;
		}

		$products = $this->getOptionsByType($productTypeId, $cityId, $eventId);

		return response()->json([
			                        'success'  => true,
			                        'products' => $products
		                        ]);

	}

	public function getQuickBookProductImages()
	{
		$productTypeId = request('productTypeId');
		$productId = request('productId');

		$productImages = [];
		$productGallery = null;
		$typeImage = config('evibe.gallery.type.image');

		switch ($productTypeId)
		{
			case config('evibe.ticket_type.packages'):
			case config('evibe.ticket_type.priests'):
			case config('evibe.ticket_type.venue-deals'):
			case config('evibe.ticket_type.couple-experiences'):
			case config('evibe.ticket_type.food'):
			case config('evibe.ticket_type.lounge'):
			case config('evibe.ticket_type.villa'):
			case config('evibe.ticket_type.resort'):
				$productGallery = \PackageGallery::where('planner_package_id', $productId)->whereNull('deleted_at')->where('type', $typeImage)->get();
				if (count($productGallery))
				{
					foreach ($productGallery as $image)
					{
						array_push($productImages, $image->getGalleryUrl());
					}
				}
				break;
			case config('evibe.ticket_type.trends'):
				$productGallery = \TrendGallery::where('trending_id', $productId)->whereNull('deleted_at')->where('type', $typeImage)->get();
				if (count($productGallery))
				{
					foreach ($productGallery as $image)
					{
						array_push($productImages, $image->getLink("results"));
					}
				}
				break;
			case config('evibe.ticket_type.cakes'):
				$productGallery = \CakeGallery::where('cake_id', $productId)->whereNull('deleted_at')->where('type', $typeImage)->get();
				if (count($productGallery))
				{
					foreach ($productGallery as $image)
					{
						array_push($productImages, $image->getLink("results"));
					}
				}
				break;
			case config('evibe.ticket_type.decors'):
				$productGallery = \DecorGallery::where('decor_id', $productId)
				                               ->whereNull('deleted_at')
				                               ->where('type_id', $typeImage)
				                               ->get();

				if (count($productGallery))
				{
					foreach ($productGallery as $image)
					{
						array_push($productImages, $image->getLink("results"));
					}
				}
				break;
			case config('evibe.ticket_type.services'):
			case config('evibe.ticket_type.entertainments'):
				$productGallery = ServiceGallery::where('type_service_id', $productId)->whereNull('deleted_at')->where('type_id', $typeImage)->get();
				if (count($productGallery))
				{
					foreach ($productGallery as $image)
					{
						array_push($productImages, $image->getLink("results"));
					}
				}
				break;
		}

		if (!count($productImages))
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'There are no images for this product'
			                        ]);
		}

		return response()->json([
			                        'success'       => true,
			                        'productImages' => $productImages
		                        ]);
	}

	public function getQuickBookCheckoutFields()
	{
		$typeTicketBookingId = \request('typeTicketBookingId');
		$eventId = \request('eventId');

		$checkoutFieldArray = [];
		$checkoutFields = CheckoutField::where('type_ticket_booking_id', $typeTicketBookingId)
		                               ->where('is_auto_booking', 0)
		                               ->where(function ($query) use ($eventId) {
			                               $query->where('event_id', $eventId)
			                                     ->orWhere('event_id', null);
		                               })
		                               ->orderBy('type_field_id', 'ASC')
		                               ->get();

		foreach ($checkoutFields as $checkoutField)
		{
			array_push($checkoutFieldArray, [
				'id'                  => $checkoutField->id,
				'typeTicketBookingId' => $checkoutField->type_ticket_booking_id,
				'name'                => $checkoutField->name,
				'identifier'          => $checkoutField->identifier,
				'typeFieldId'         => $checkoutField->type_field_id
			]);
		}

		return response()->json([
			                        'success'        => true,
			                        'checkoutFields' => $checkoutFieldArray
		                        ]);
	}

	public function getQuickBookProductTypes()
	{
		$eventId = \request('eventId');

		$parentEvent = TypeEvent::find($eventId);
		if ($parentEvent && $parentEvent->parent_id)
		{
			$eventId = $parentEvent->parent_id;
		}

		$productTypeIds = $this->getMappingTypesOnOccasion($eventId);

		// remove planner, venue from default
		$pKeyId = array_search(config('evibe.ticket_type.planners'), $productTypeIds);
		if (is_int($pKeyId))
		{
			unset($productTypeIds[$pKeyId]);
		}
		$vKeyId = array_search(config('evibe.ticket_type.venues'), $productTypeIds);
		if (is_int($vKeyId))
		{
			unset($productTypeIds[$vKeyId]);
		}

		$productTypes = \TypeTicket::select('id', 'name')
		                           ->whereIn('id', $productTypeIds)
		                           ->get()
		                           ->toArray();

		return response()->json([
			                        'success'      => true,
			                        'productTypes' => $productTypes
		                        ]);
	}

	public function createQuickBooking()
	{
		try
		{
			$handlerId = Auth::user()->id;
			$oldTicketId = Input::get('ticketId');
			$eventId = Input::get('eventId');
			$bookingAmount = Input::get('bookingAmount');
			$productPrice = Input::get('productPrice');
			$typeTicketBookingId = Input::get('typeTicketBooking');

			$rules = [
				'name'               => 'required',
				'phone'              => 'required|digits:10',
				'email'              => 'required|email',
				'altPhone'           => 'sometimes|digits:10',
				'partyDateTime'      => 'required|date|after:now',
				'eventId'            => 'required|integer|min:1',
				'cityId'             => 'required|integer|min:1',
				'zipCode'            => 'required|integer|digits:6',
				'areaId'             => 'required',
				'venueLandmark'      => 'required',
				'ticketSource'       => 'sometimes|integer',
				'sourceSpecific'     => 'sometimes',
				'productId'          => 'required|integer|min:1',
				'productTypeId'      => 'required|integer|min:1',
				'partnerId'          => 'required|integer|min:1',
				'partnerTypeId'      => 'required|integer|min:1',
				'productPrice'       => 'required|integer|min:1',
				'discountAmount'     => 'required|integer|max:' . $productPrice,
				'bookingAmount'      => 'required|integer|min:1',
				'advanceAmount'      => 'required|integer|max:' . $bookingAmount,
				'typeTicketBooking'  => 'sometimes|required|integer|min:1',
				'bookingTypeDetails' => 'required_with:typeTicketBooking|max:255',
				'typeBookingConcept' => 'required|integer|min:1',
				'orderDetails'       => 'required|min:10'
			];

			$messages = [
				'eventId.required'                 => 'Kindly select an occasion',
				'eventId.min'                      => 'Kindly select a valid occasion',
				'cityId.required'                  => 'Kindly select an city',
				'cityId.min'                       => 'Kindly select a valid city',
				'zipCode.required'                 => 'Kindly provide customer location pincode',
				'zipCode.digits'                   => 'Kindly enter a valid pincode',
				'areaId.required'                  => 'Location is required',
				'venueLandmark'                    => 'Landmark is required',
				'bookingAmount.required'           => 'Booking amount is required',
				'bookingAmount.integer'            => 'Booking amount entered is not valid (use ONLY numbers)',
				'bookingAmount.min'                => 'Booking amount cannot be zero',
				'advanceAmount.required'           => 'Advance amount is required',
				'advanceAmount.integer'            => 'Advance amount entered is not valid (use ONLY numbers)',
				'advanceAmount.min'                => 'Advance amount cannot be that low',
				'advanceAmount.max'                => 'Advance amount cannot be more than booking amount',
				'partyDateTime.required'           => 'Party date & time is required',
				'partyDateTime.date'               => 'Party date & time is not valid',
				'reportBeforeTimeHrs.between'      => 'select the valid hours of party before time(Ex-4 hrs)',
				'reportBeforeTimeMin.between'      => 'select the valid minutes of party before time(15 minutes)',
				'additionalInfo.required'          => 'Order details is required',
				'additionalInfo.min'               => 'Order details should be minimum 10 characters',
				'partyEndTime.after'               => 'Party End date must be after Party Start date ',
				'typeTicketBooking.required'       => 'Ticket booking type is required',
				'typeTicketBooking.min'            => 'Please select a valid ticket booking type',
				'bookingTypeDetails.required_with' => 'Booking type details is required with ticket type booking',
				'bookingTypeDetails.min'           => 'Booking type details should be less than 255 characters',
				'typeBookingConcept.required'      => 'Booking concept type is required',
				'typeBookingConcept.integer'       => 'Booking concept type is invalid',
				'typeBookingConcept.min'           => 'Booking concept type is invalid'
			];

			$crmFields = CheckoutField::where('type_ticket_booking_id', $typeTicketBookingId)
			                          ->where('is_auto_booking', 0)
			                          ->where(function ($query) use ($eventId) {
				                          $query->where('event_id', $eventId)
				                                ->orWhere('event_id', null);
			                          })
			                          ->orderBy('type_field_id', 'ASC')
			                          ->get();

			foreach ($crmFields as $crmField)
			{
				$rules[$crmField->name] = "required";
				$messages[$crmField->name . '.required'] = "$crmField->identifier is required. if not applicable type <b>n/a</b>";
				if($crmField->type_field_id == config('evibe.input.checkbox'))
				{
					// @see: options are stored as text separated by commas
					$messages[$crmField->name . '.required'] = "$crmField->identifier are required. Kindly select at least one choice.";
				}
			}

			$validation = Validator::make(Input::all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validation->messages()->first()
				                        ]);
			}

			$partyDateTime = Input::get('partyDateTime');

			$ticketData = [
				'name'           => Input::get('name'),
				'phone'          => Input::get('phone'),
				'email'          => Input::get('email'),
				'alt_phone'      => Input::get('altPhone'),
				'event_date'     => strtotime($partyDateTime),
				'event_id'       => Input::get('eventId'),
				'city_id'        => Input::get('cityId'),
				'area_id'        => Input::get('areaId'),
				'zip_code'       => Input::get('zipCode'),
				'venue_landmark' => Input::get('venueLandmark'),
				'handler_id'     => $handlerId,
			];

			$validStatus = [
				config('evibe.status.initiated'),
				config('evibe.status.progress'),
				config('evibe.status.followup')
			];
			$ticket = \Ticket::where('id', $oldTicketId)
			                 ->whereIn('status_id', $validStatus)
			                 ->first();

			// todo: validate if ticket in in correct state and bookings exist
			if ($ticket && !$ticket->bookings->count())
			{
				$ticket->update($ticketData);
			}
			else
			{
				$ticket = \Ticket::create($ticketData);
			}

			// generate enq
			if (!$ticket->enquiry_id)
			{
				$ticket->enquiry_id = 'ENQ-QB-' . $ticket->id;
			}

			$ticketSource = Input::get('ticketSource');
			if ($ticketSource && $ticketSource > 0)
			{
				$ticket->source_id = $ticketSource;
			}

			if (Input::get('sourceSpecific'))
			{
				$ticket->source_specific = Input::get('sourceSpecific');
			}

			if (Input::get('partnerTypeId') == config('evibe.ticket_type.venues'))
			{
				$venue = \Venue::find(Input::get('partnerId'));
				if ($venue)
				{
					 // @todo: if ever re-used, correct this logic of getting from partner directly
					$ticket->venue_address = $venue->full_address;
					$ticket->venue_landmark = $venue->landmark;
					$ticket->area_id = $venue->area_id;
					$ticket->zip_code = $venue->zip;
					$ticket->type_venue_id = $venue->type_id;
					$ticket->lat = $venue->map_lat;
					$ticket->lng = $venue->map_long;
					$ticket->map_address = $venue->name . ", " . $venue->full_address;
					$ticket->map_link = "http://maps.google.com/maps?q=$venue->map_lat, $venue->map_long";
				}
			}

			$ticket->save();

			// ticket mapping
			$ticketMapping = \TicketMapping::create([
				                                        'ticket_id'       => $ticket->id,
				                                        'handler_id'      => $handlerId,
				                                        'party_date_time' => $partyDateTime,
				                                        'info'            => Input::get('orderDetails'),
				                                        'map_id'          => Input::get('productId'),
				                                        'map_type_id'     => Input::get('productTypeId')
			                                        ]);
			// ticket booking
			$ticketBooking = \TicketBooking::create([
				                                        'ticket_id'              => $ticket->id,
				                                        'booking_id'             => $ticket->enquiry_id,
				                                        'party_date_time'        => strtotime($partyDateTime),
				                                        'booking_info'           => Input::get('orderDetails'),
				                                        'product_price'          => $productPrice,
				                                        'booking_amount'         => $bookingAmount,
				                                        'advance_amount'         => Input::get('advanceAmount'),
				                                        'discount_amount'        => Input::get('discountAmount'),
				                                        'is_venue_booking'       => Input::get('partnerTypeId') == config('evibe.ticket_type.venues') ? 1 : 0,
				                                        'map_id'                 => Input::get('partnerId'),
				                                        'map_type_id'            => Input::get('partnerTypeId'),
				                                        'ticket_mapping_id'      => $ticketMapping->id,
				                                        'handler_id'             => $handlerId,
				                                        'type_ticket_booking_id' => $typeTicketBookingId,
				                                        'booking_type_details'   => Input::get('bookingTypeDetails'),
			                                        ]);
			// checkout fields
			foreach ($crmFields as $crmField)
			{
				if (Input::get($crmField->name))
				{
					CheckoutFieldValue::create([
						                           'ticket_id'         => $ticket->id,
						                           'checkout_field_id' => $crmField->id,
						                           'value'             => Input::get($crmField->name)
					                           ]);
				}
			}

			// product images
			$productImages = Input::get('productImages');

			if (count($productImages))
			{
				foreach ($productImages as $imageUrl)
				{
					$filename = pathinfo($imageUrl, PATHINFO_FILENAME);
					$galleryData = [
						'ticket_booking_id' => $ticketBooking->id,
						'url'               => $imageUrl,
						'type'              => 1, // link
						'title'             => $filename
					];

					TicketBookingGallery::create($galleryData);
				}
			}

			// ticket update
			$ticketUpdate = [
				'ticket'   => $ticket,
				'statusId' => config('evibe.status.initiated'),
				'comments' => config('evibe.ticket_status_message.quick_book')
			];
			$this->updateTicketAction($ticketUpdate);

			$alertRes = $this->sendOrderProcessNotifications($ticket->id);

			if (isset($alertRes['success']) && $alertRes['success'])
			{
				$paymentLink = isset($alertRes['paymentLink']) ? $alertRes['paymentLink'] : null;

				return response()->json([
					                        'success'       => true,
					                        'paymentLink'   => $paymentLink,
					                        'ticketId'      => $ticket->id,
					                        'customerName'  => $ticket->name,
					                        'customerPhone' => $ticket->phone,
					                        'customerEmail' => $ticket->email,
					                        'partyDateTime' => Carbon::createFromTimestamp($ticket->event_date)->toDateTimeString()
				                        ]);
			}
			else
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to send order process alert to customer'
				                        ]);
			}

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false,
				                        'error'   => $exception->getMessage()
			                        ]);
		}
	}
}