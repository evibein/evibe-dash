<?php

namespace App\Http\Controllers\Ticket;

use App\Jobs\Emails\Issues\ReportTimeMismatchAlertToTeam;
use App\Jobs\Emails\Partner\SendRejectionMailToPartner;
use App\Jobs\Emails\RefundAmountMail;
use App\Jobs\Finance\DeleteScheduleSettlementBooking;
use App\Jobs\Finance\UpdateScheduleSettlementBooking;
use App\Models\Ticket\RefundGallery;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Util\CheckoutFieldValue;
use Carbon\Carbon;
use Evibe\Facades\AppUtilFacade as AppUtil;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Mockery\CountValidator\Exception;

class TicketBookingController extends TicketOrderController
{
	public function showEditTicketBooking($ticketBookingId)
	{
		$ticketBooking = \TicketBooking::with('mapping')
		                               ->findOrFail($ticketBookingId);

		$mapValues = $ticketBooking->mapping->getMappedValues();
		$beforeTime = $ticketBooking->reports_before;
		$ticketBookingDetails = '';

		if ($ticketBooking->booking_type_details)
		{
			$ticketBookingDetails = $ticketBooking->booking_type_details;
		}
		elseif ($ticketBooking->ticketBookingType)
		{
			$ticketBookingDetails = $ticketBooking->ticketBookingType->name;
		}
		else
		{
			// do nothing
		}

		$res = [
			'success'             => true,
			'ticket_booking_id'   => $ticketBookingId,
			'name'                => $mapValues['name'],
			'code'                => $mapValues['code'],
			'url'                 => $mapValues['link'],
			'party_end_time'      => $ticketBooking->party_end_time ? date('Y/m/d H:i', $ticketBooking->party_end_time) : date('Y/m/d H:i', $ticketBooking->party_date_time),
			'mapTypeId'           => $ticketBooking->map_type_id,
			'price'               => $ticketBooking->booking_amount,
			'advance'             => $ticketBooking->advance_amount,
			'customized'          => $ticketBooking->has_customization,
			'info'                => $ticketBooking->booking_info,
			'map_id'              => $ticketBooking->map_id,
			'map_type_id'         => $ticketBooking->map_type_id,
			'party_date_time'     => date('Y/m/d H:i', $ticketBooking->party_date_time),
			'before_time_hrs'     => ($beforeTime / 60),
			'before_time_min'     => $beforeTime % 60,
			'prerequisites'       => $ticketBooking->prerequisites,
			'facts'               => $ticketBooking->facts,
			'typeTicketBookingId' => $ticketBooking->type_ticket_booking_id,
			'bookingTypeDetails'  => $ticketBookingDetails
		];

		return response()->json($res);
	}

	public function updateTicketBookingDetails()
	{
		$ticketBookingId = Input::get('ticketBookingId');
		$ticketBooking = \TicketBooking::with('ticket')->findOrFail($ticketBookingId);
		$typeTicketBookingId = Input::get('typeTicketBooking');
		$typeBookingConceptId = Input::get('typeBookingConcept');
		$ticket = $ticketBooking->ticket;
		$userId = Auth::user()->id;

		$venueHall = null;
		$res = ['success' => true];
		$rules = [
			'bookingAmount'       => 'required',
			'advanceAmount'       => 'required|integer|max:' . Input::get('bookingAmount'),
			'partyDateTime'       => 'required|date',
			'reportBeforeTimeHrs' => 'integer|between:0,23',
			'reportBeforeTimeMin' => 'integer|between:0,59',
			'typeTicketBooking'   => 'sometimes|required|integer|min:1',
			'bookingTypeDetails'  => 'required_with:typeTicketBooking|max:255',
			'typeBookingConcept'  => 'required|integer|min:1'
		];

		$messages = [
			'bookingAmount.required'           => 'Booking amount is required',
			'bookingAmount.integer'            => 'Booking amount entered is not valid (use ONLY numbers)',
			'bookingAmount.min'                => 'Booking amount cannot be that low (min: 1000)',
			'advanceAmount.required'           => 'Advance amount is required',
			'advanceAmount.integer'            => 'Advance amount entered is not valid (use ONLY numbers)',
			'advanceAmount.min'                => 'Advance amount cannot be that low',
			'advanceAmount.max'                => 'Advance amount cannot be more than booking amount',
			'partyDateTime.required'           => 'Party date & time is required',
			'partyDateTime.date'               => 'Party date & time is not valid',
			'reportBeforeTimeHrs.between'      => 'select the valid hours of party before time(Ex-4 hrs)',
			'reportBeforeTimeMin.between'      => 'select the valid minutes of party before time(15 minutes)',
			'additionalInfo.required'          => 'Order details is required',
			'additionalInfo.min'               => 'Order Details should be min 10 characters',
			'partyEndTime.after'               => 'Party End date must be after Party Start date ',
			'typeTicketBooking.required'       => 'Ticket booking type is required',
			'typeTicketBooking.min'            => 'Please select a valid ticket booking type',
			'bookingTypeDetails.required_with' => 'Booking Type details is required with ticket type booking',
			'bookingTypeDetails.min'           => 'Booking type details should be less than 255 characters',
			'typeBookingConcept.required'      => 'Booking concept type is required',
			'typeBookingConcept.integer'       => 'Booking concept type is invalid',
			'typeBookingConcept.min'           => 'Booking concept type is invalid'
		];

		$crmFields = $this->getCheckoutField($ticket, $typeTicketBookingId, 1);
		foreach ($crmFields as $crmField)
		{
			$rules['checkout-' . $crmField->name] = "required";
			$messages['checkout-' . $crmField->name . '.required'] = "$crmField->identifier is required. if not applicable type <b>n/a</b>";
			if ($crmField->type_field_id == config('evibe.input.checkbox'))
			{
				// @see: options are stored as text separated by commas
				$messages['checkout-' . $crmField->name . '.required'] = "$crmField->identifier are required. Kindly select at least one choice.";
			}
		}

		$rules['additionalInfo'] = 'required|min:10';
		$rules['partyEndTime'] = 'date|after:partyDateTime';

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}
		else
		{
			// calculate the before time
			$reportsBeforeTimeHrs = Input::get('reportBeforeTimeHrs');
			$reportsBeforeTimeMinutes = Input::get('reportBeforeTimeMin');
			$reportsBefore = AppUtil::getTimeInMinutesFormat($reportsBeforeTimeHrs, $reportsBeforeTimeMinutes);

			$partyDateTime = strtotime(Input::get('partyDateTime'));
			$partyEndDateTime = strtotime(Input::get('partyEndTime'));

			$oldCheckInTime = $ticketBooking->party_date_time;
			$checkInTime = date("g:i A", $partyDateTime);
			$timeUpdate = "";

			if ($oldCheckInTime)
			{
				if ($oldCheckInTime != $checkInTime)
				{
					$timeUpdate = ". Check in time will be updated to $checkInTime from $oldCheckInTime. ";
				}
			}
			else
			{
				$timeUpdate = ". Check in time ($checkInTime) will be created for the first time.";
			}

			$checkOutTime = $partyEndDateTime ? date("g:i A", $partyEndDateTime) : null;

			$reportsAtTimestamp = $partyDateTime - (($reportsBefore) * 60);
			$reportsAt = date("Y-m-d H:i:s", $reportsAtTimestamp);

			$bookingAmount = Input::get('bookingAmount');
			$advanceAmount = Input::get('advanceAmount');

			// update `ticket_bookings`
			$ticketBooking->update([
				                       'party_date_time'         => $partyDateTime,
				                       //'check_in'                => $checkInTime,
				                       'booking_info'            => Input::get('additionalInfo'),
				                       'booking_amount'          => $bookingAmount,
				                       'advance_amount'          => $advanceAmount,
				                       'has_customization'       => Input::get('hasCustomization'),
				                       'prerequisites'           => Input::get('prerequisites'),
				                       'facts'                   => Input::get('facts'),
				                       'party_end_time'          => $partyEndDateTime,
				                       'check_out'               => $checkOutTime,
				                       'reports_before'          => $reportsBefore,
				                       'reports_at'              => $reportsAt,
				                       'handler_id'              => $userId,
				                       'type_ticket_booking_id'  => $typeTicketBookingId,
				                       'type_booking_concept_id' => $typeBookingConceptId,
				                       'updated_at'              => date('Y-m-d H:i:s'),
				                       'booking_type_details'    => trim(Input::get('bookingTypeDetails'))
			                       ]);

			//  save checkout field value in database
			$this->saveCheckoutFieldValue($ticket, $typeTicketBookingId, Input::all());

			$updatedPartyDateTime = $ticketBooking->party_date_time;
			//$updatedCheckInTime = $ticketBooking->check_in;
			if ($updatedPartyDateTime != $partyDateTime)
			{
				$timeUpdate .= "Party time is wrongly updated.";
				$bookingLink = config('evibe.host') . "tickets/" . $ticketBooking->ticket_id . "/bookings";
				$timeUpdateData = [
					'bookingId'             => $ticketBooking->booking_id,
					'bookingLink'           => $bookingLink,
					'observedPartyDateTime' => date("g:i A", $updatedPartyDateTime),
					'actualPartyDateTime'   => $partyDateTime,
					'partyDate'             => date("d/m/y", $updatedPartyDateTime)
				];
				$this->dispatch(new ReportTimeMismatchAlertToTeam($timeUpdateData));

				$res = [
					'success' => false,
					'error'   => "Party time has been updated wrongly. Kindly check your email for more info."
				];
			}
			// update ticket data
			// @see: nullify only `updated_receipt_sent_at` to identify if new updated booking available
			$bookingTime = \TicketBooking::select(DB::raw("MIN(party_date_time) AS min_book_time"))->where('ticket_id', $ticketBooking->ticket_id)->groupBy('ticket_id')->first();

			$ticketData = [
				'updated_receipt_sent_at' => null,
				'handler_id'              => $userId,
				'created_handler_id'      => $ticket->created_handler_id ?: $userId,
				'event_date'              => $bookingTime->min_book_time,
				'updated_at'              => date('Y-m-d H:i:s'),
			];

			$ticket->update($ticketData);

			// Ticket update action
			$ticketUpdate = [
				'statusId' => config('evibe.status.progress'),
				'ticket'   => $ticket,
				'comments' => config('evibe.ticket_status_message.booking_edit') . $timeUpdate
			];
			$this->updateTicketAction($ticketUpdate);
		}

		return response()->json($res);
	}

	public function changeTicketBookingPartner()
	{
		$ticketBookingId = Input::get('ticketBookingId');
		$ticketBooking = \TicketBooking::with('ticket')->findOrFail($ticketBookingId);
		$typeTicketBookingId = Input::get('typeTicketBooking');
		$typeBookingConceptId = Input::get('typeBookingConcept');
		$ticket = $ticketBooking->ticket;
		$userId = Auth::user()->id;

		$venueHall = null;
		$res = ['success' => true];
		$rules = [
			'bookingAmount'       => 'required',
			'advanceAmount'       => 'required|integer|max:' . Input::get('bookingAmount'),
			'partyDateTime'       => 'required|date',
			'reportBeforeTimeHrs' => 'integer|between:0,23',
			'reportBeforeTimeMin' => 'integer|between:0,59',
			'typeTicketBooking'   => 'sometimes|required|integer|min:1',
			'bookingTypeDetails'  => 'required_with:typeTicketBooking|max:255',
			'typeBookingConcept'  => 'required|integer|min:1',
			'partnerId'           => 'required|integer|min:1',
			'partnerTypeId'       => 'required|integer|min:1'
		];

		$messages = [
			'bookingAmount.required'           => 'Booking amount is required',
			'bookingAmount.integer'            => 'Booking amount entered is not valid (use ONLY numbers)',
			'bookingAmount.min'                => 'Booking amount cannot be that low (min: 1000)',
			'advanceAmount.required'           => 'Advance amount is required',
			'advanceAmount.integer'            => 'Advance amount entered is not valid (use ONLY numbers)',
			'advanceAmount.min'                => 'Advance amount cannot be that low',
			'advanceAmount.max'                => 'Advance amount cannot be more than booking amount',
			'partyDateTime.required'           => 'Party date & time is required',
			'partyDateTime.date'               => 'Party date & time is not valid',
			'reportBeforeTimeHrs.between'      => 'select the valid hours of party before time(Ex-4 hrs)',
			'reportBeforeTimeMin.between'      => 'select the valid minutes of party before time(15 minutes)',
			'additionalInfo.required'          => 'Order details is required',
			'additionalInfo.min'               => 'Order Details should be min 10 characters',
			'partyEndTime.after'               => 'Party End date must be after Party Start date ',
			'typeTicketBooking.required'       => 'Ticket booking type is required',
			'typeTicketBooking.min'            => 'Please select a valid ticket booking type',
			'bookingTypeDetails.required_with' => 'Booking Type details is required with ticket type booking',
			'bookingTypeDetails.min'           => 'Booking type details should be less than 255 characters',
			'typeBookingConcept.required'      => 'Booking concept type is required',
			'typeBookingConcept.integer'       => 'Booking concept type is invalid',
			'typeBookingConcept.min'           => 'Booking concept type is invalid',
			'partnerId.required'               => 'Kindly select a partner',
			'partnerId.min'                    => 'Kindly select a valid partner',
		];

		$crmFields = $this->getCheckoutField($ticket, $typeTicketBookingId, 1);
		foreach ($crmFields as $crmField)
		{
			$rules['checkout-' . $crmField->name] = "required";
			$messages['checkout-' . $crmField->name . '.required'] = "$crmField->identifier is required. if not applicable type <b>n/a</b>";
			if ($crmField->type_field_id == config('evibe.input.checkbox'))
			{
				// @see: options are stored as text separated by commas
				$messages['checkout-' . $crmField->name . '.required'] = "$crmField->identifier are required. Kindly select at least one choice.";
			}
		}

		$rules['additionalInfo'] = 'required|min:10';
		$rules['partyEndTime'] = 'date|after:partyDateTime';

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}
		else
		{
			// calculate the before time
			$reportsBeforeTimeHrs = Input::get('reportBeforeTimeHrs');
			$reportsBeforeTimeMinutes = Input::get('reportBeforeTimeMin');
			$reportsBefore = AppUtil::getTimeInMinutesFormat($reportsBeforeTimeHrs, $reportsBeforeTimeMinutes);

			$partyDateTime = strtotime(Input::get('partyDateTime'));
			$partyEndDateTime = strtotime(Input::get('partyEndTime'));

			$oldCheckInTime = $ticketBooking->party_date_time;
			$checkInTime = date("g:i A", $partyDateTime);
			$timeUpdate = "";

			if ($oldCheckInTime)
			{
				if ($oldCheckInTime != $checkInTime)
				{
					$timeUpdate = ". Check in time will be updated to $checkInTime from $oldCheckInTime. ";
				}
			}
			else
			{
				$timeUpdate = ". Check in time ($checkInTime) will be created for the first time.";
			}

			$checkOutTime = $partyEndDateTime ? date("g:i A", $partyEndDateTime) : null;

			$reportsAtTimestamp = $partyDateTime - (($reportsBefore) * 60);
			$reportsAt = date("Y-m-d H:i:s", $reportsAtTimestamp);

			$bookingAmount = Input::get('bookingAmount');
			$advanceAmount = Input::get('advanceAmount');

			$oldPartnerId = $ticketBooking->map_id;
			$oldPartnerTypeId = $ticketBooking->map_type_id;

			$newPartnerId = Input::get('partnerId');
			$newPartnerTypeId = Input::get('partnerTypeId');

			//if ($oldPartnerTypeId != $newPartnerTypeId)
			//{
			//	return response()->json([
			//		                        'success' => false,
			//		                        'error'   => 'Kindly select same type of partner for the booking. If unable to do so, report this to tech team'
			//	                        ]);
			//}

			if (($oldPartnerId == $newPartnerId) && ($oldPartnerTypeId == $newPartnerTypeId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly select a different partner'
				                        ]);
			}

			// update `ticket_bookings`
			$ticketBooking->update([
				                       'party_date_time'         => $partyDateTime,
				                       //'check_in'                => $checkInTime,
				                       'booking_info'            => Input::get('additionalInfo'),
				                       'booking_amount'          => $bookingAmount,
				                       'advance_amount'          => $advanceAmount,
				                       'has_customization'       => Input::get('hasCustomization'),
				                       'prerequisites'           => Input::get('prerequisites'),
				                       'facts'                   => Input::get('facts'),
				                       'party_end_time'          => $partyEndDateTime,
				                       'check_out'               => $checkOutTime,
				                       'reports_before'          => $reportsBefore,
				                       'reports_at'              => $reportsAt,
				                       'handler_id'              => $userId,
				                       'type_ticket_booking_id'  => $typeTicketBookingId,
				                       'type_booking_concept_id' => $typeBookingConceptId,
				                       'updated_at'              => date('Y-m-d H:i:s'),
				                       'booking_type_details'    => trim(Input::get('bookingTypeDetails')),
				                       'map_id'                  => $newPartnerId,
				                       'map_type_id'             => $newPartnerTypeId,
				                       'is_partner_changed'      => 1,
				                       'old_partner_id'          => $oldPartnerId,
				                       'old_partner_type_id'     => $oldPartnerTypeId,
				                       'updated_message'         => 'Partner has been changes for this booking'
			                       ]);

			$oldPartner = $ticketBooking->oldProvider;

			// dispatch job
			if ($oldPartner)
			{
				$mapping = $ticketBooking->mapping;
				$option = AppUtil::fillMappingValues([
					                                     'isCollection' => false,
					                                     'mapTypeId'    => $mapping->map_type_id,
					                                     'mapId'        => $mapping->map_id
				                                     ]);

				$data = [
					'partnerName'        => $oldPartner->person,
					'partnerCompanyName' => $oldPartner->name,
					'partnerMobile'      => $oldPartner->phone,
					'partnerEmail'       => $oldPartner->email,
					'bookingId'          => $ticketBooking->booking_id,
					'bookingAmount'      => $ticketBooking->booking_amount,
					'partyDateTime'      => $ticketBooking->party_date_time ? date("d M Y", $ticketBooking->party_date_time) : null,
					'optionName'         => isset($option['name']) && $option['name'] ? $option['name'] : null,
					'ticketId'           => $ticket->id
				];

				// send cancellation email to old partner
				$this->dispatch(new SendRejectionMailToPartner($data));
			}
			else
			{
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.partner_update'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketBookingController.php - ' . __LINE__ . '] Unable to fetch old partner to send cancellation email.',
					                                   'exception' => '[TicketBookingController.php - ' . __LINE__ . '] Unable to fetch old partner to send cancellation email.',
					                                   'trace'     => '[Ticket Id: ' . $ticket->id . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
					                                   'details'   => '[Ticket Id: ' . $ticket->id . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
				                                   ]);
			}

			//  save checkout field value in database
			$this->saveCheckoutFieldValue($ticket, $typeTicketBookingId, Input::all());

			$updatedPartyDateTime = $ticketBooking->party_date_time;
			//$updatedCheckInTime = $ticketBooking->check_in;
			if ($updatedPartyDateTime != $partyDateTime)
			{
				$timeUpdate .= "Party time is wrongly updated.";
				$bookingLink = config('evibe.host') . "tickets/" . $ticketBooking->ticket_id . "/bookings";
				$timeUpdateData = [
					'bookingId'             => $ticketBooking->booking_id,
					'bookingLink'           => $bookingLink,
					'observedPartyDateTime' => date("g:i A", $updatedPartyDateTime),
					'actualPartyDateTime'   => $partyDateTime,
					'partyDate'             => date("d/m/y", $updatedPartyDateTime)
				];
				$this->dispatch(new ReportTimeMismatchAlertToTeam($timeUpdateData));

				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.partner_update'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketBookingController.php - ' . __LINE__ . '] Party time has been updated wrongly. Kindly check your email for more info.',
					                                   'exception' => '[TicketBookingController.php - ' . __LINE__ . '] Party time has been updated wrongly. Kindly check your email for more info.',
					                                   'trace'     => '[Ticket Id: ' . $ticket->id . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
					                                   'details'   => '[Ticket Id: ' . $ticket->id . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
				                                   ]);
			}

			// update ticket data
			// @see: nullify only `updated_receipt_sent_at` to identify if new updated booking available
			// can be enabled after fixing issues in MAIN
			$bookingTime = \TicketBooking::select(DB::raw("MIN(party_date_time) AS min_book_time"))->where('ticket_id', $ticketBooking->ticket_id)->groupBy('ticket_id')->first();

			$ticketData = [
				'updated_receipt_sent_at' => null,
				'handler_id'              => $userId,
				'created_handler_id'      => $ticket->created_handler_id ?: $userId,
				'event_date'              => $bookingTime->min_book_time,
				'updated_at'              => date('Y-m-d H:i:s'),
			];

			$ticket->update($ticketData);

			// Ticket update action
			$ticketUpdate = [
				'statusId' => config('evibe.status.booked'),
				'ticket'   => $ticket,
				'comments' => config('evibe.ticket_status_message.partner_change') . $timeUpdate
			];
			$this->updateTicketAction($ticketUpdate);

			$this->dispatch(new UpdateScheduleSettlementBooking([
				                                                    'ticketBookingId' => $ticketBooking->id,
				                                                    'handlerId'       => $userId
			                                                    ]));

			// updated receipts will be sent in MAIN

			$partnerChange = true;
			$ticketId = $ticket->id;
			$bookingIds = [$ticketBooking->id];
			$handlerId = $userId;
			$host = config('evibe.live.host');
			$bookingIdsString = implode(",", $bookingIds);
			$link = "$host/pay/update-receipt/$ticketId?handlerId=$handlerId&bookingIds=$bookingIdsString&partnerChange=$partnerChange";

			try
			{
				$ch = curl_init($link);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				$cUrlResponse = json_decode(curl_exec($ch), true);
				if ($cUrlResponse['success'])
				{
					curl_close($ch);

					$ticket->update(['updated_receipt_sent_at' => date('Y-m-d H:i:s')]);

					return response()->json([
						                        'success'    => true,
						                        'successMsg' => 'Partner changed successfully and updated receipts has been send to customer, new partner and old partner'
					                        ]);
				}
				else
				{
					$this->sendNonExceptionErrorReport([
						                                   'code'      => config('evibe.error_code.partner_update'),
						                                   'url'       => request()->fullUrl(),
						                                   'method'    => request()->method(),
						                                   'message'   => '[TicketBookingController.php - ' . __LINE__ . '] Error occurred while sending partner change updated receipts to customer and new partner',
						                                   'exception' => '[TicketBookingController.php - ' . __LINE__ . '] Unable to send partner change receipts',
						                                   'trace'     => '[Ticket Id: ' . $ticket->id . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
						                                   'details'   => '[Ticket Id: ' . $ticket->id . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
					                                   ]);
				}
				curl_close($ch);
			} catch (\Exception $exception)
			{
				$this->sendErrorReport($exception);
			}

			return response()->json([
				                        'success'    => true,
				                        'successMsg' => 'Partner changed successfully. Cancellation receipt sent to old partner.'
			                        ]);
		}

		return response()->json($res);
	}

	public function showEditTicketBookingBeforeTime($ticketBookingId)
	{
		$ticketBooking = \TicketBooking::findOrFail($ticketBookingId);
		$beforeTime = $ticketBooking->reports_before;

		$res = [
			'before_time_hrs' => ($beforeTime / 60),
			'before_time_min' => $beforeTime % 60,
			'bookingId'       => $ticketBookingId,
			'success'         => true
		];

		return response()->json($res);
	}

	/**
	 * @author Vikash<vikash@evibe.in>
	 * @since  4 Jan 2016
	 *
	 * Editing and saving the before time for missed call technology
	 *
	 */
	public function editTicketBookingBeforeTime($ticketBookingId)
	{

		$rules = [
			'reportBeforeTimeHrs' => 'integer|between:0,23',
			'reportBeforeTimeMin' => 'integer|between:0,59',
		];

		$messages = [
			'reportBeforeTimeHrs.between' => 'select the valid hours of party before time(Ex-4 hrs)',
			'reportBeforeTimeMin.between' => 'select the valid minutes of party before time(15 minutes)'
		];

		$validation = Validator::make(Input::all(), $rules, $messages);

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];

		}
		else
		{

			$res = ['success' => true];
			$ticketBooking = \TicketBooking::findOrFail($ticketBookingId);

			//calculate the before time
			$reportsBeforeTimeHrs = Input::get('reportBeforeTimeHrs');
			$reportsBeforeTimeMinutes = Input::get('reportBeforeTimeMin');
			$reportsBefore = AppUtil::getTimeInMinutesFormat($reportsBeforeTimeHrs, $reportsBeforeTimeMinutes);

			$partyDateTime = strtotime(Input::get('partyDateTime'));
			$reportsAtTimestamp = $partyDateTime - (($reportsBefore) * 60);
			$reportsAt = date("Y-m-d H:i:s", $reportsAtTimestamp);

			$ticketBooking->update([
				                       'party_date_time' => $partyDateTime,
				                       'reports_before'  => $reportsBefore,
				                       'reports_at'      => $reportsAt,
			                       ]);

			$bookingTime = \TicketBooking::select(DB::raw("MIN(party_date_time) AS min_book_time"))->where('ticket_id', $ticketBooking->ticket_id)->groupBy('ticket_id')->first();
			\Ticket::find($ticketBooking->ticket_id)->update([
				                                                 'event_date' => $bookingTime->min_book_time,
			                                                 ]);

		}

		return response()->json($res);
	}

	public function deleteTicketBooking($ticketBookingId)
	{
		$ticketBooking = \TicketBooking::findOrFail($ticketBookingId);
		$isVenueBooking = $ticketBooking->is_venue_booking;
		$typeTicketBookingId = $ticketBooking->type_ticket_booking_id;
		$ticket = $ticketBooking->ticket;
		$mappingId = $ticketBooking->ticket_mapping_id;
		$userId = Auth::user()->id;

		if ($ticketBooking->delete())
		{
			$isNoBooking = ($ticketBooking->ticket->bookings->count() == 0) ? true : false;

			// update the ticket
			$ticketUpdate = [
				'ticket'      => $ticketBooking->ticket,
				'statusId'    => config('evibe.status.progress'),
				'comments'    => config('evibe.ticket_status_message.booking_deleted'),
				'isNoBooking' => $isNoBooking
			];
			$this->updateTicketAction($ticketUpdate);

			$ticketData = [];

			// if ticket bookings exits, update nearest booking time for the ticket
			if (!$isNoBooking)
			{
				$bookingTime = \TicketBooking::select(DB::raw("MIN(party_date_time) AS min_book_time"))
				                             ->where('ticket_id', $ticketBooking->ticket->id)
				                             ->groupBy('ticket_id')
				                             ->first();

				$ticketData['event_date'] = $bookingTime->min_book_time;
			}
			else
			{
				if ($ticket->is_auto_booked)
				{
					$ticket->is_auto_booked = 0;
					$ticket->save();
				}
			}

			// null-ify venue details if ticket booking is of type venue.
			/* Disabled by @Jeevan on 13-02-2019 */
			//if ($isVenueBooking == 1)
			//{
			//	$data = [
			//		'lat'            => '',
			//		'lng'            => '',
			//		'map_address'    => '',
			//		'venue_address'  => '',
			//		'venue_landmark' => '',
			//		'zip_code'       => '',
			//		'type_venue_id'  => null,
			//		'area_id'        => null,
			//		'city_id'        => null,
			//	];
			//	$ticketData = array_merge($ticketData, $data);
			//}

			if ($ticketData)
			{
				$ticketBooking->ticket->update($ticketData);
			}

			$ticketMapping = \TicketMapping::find($mappingId);
			if ($ticketMapping)
			{
				// delete the mapping also - to avoid errors
				//$ticketMapping->update(['finalized_at' => null]);
				$ticketMapping->update(['deleted_at' => Carbon::createFromTimestamp(time())->toDateTimeString()]);
			}

			// reset all the checkout field value of this booking type to null
			// @see if there is two more booking with same type then don't reset.
			$typeCount = \TicketBooking::where('type_ticket_booking_id', $typeTicketBookingId)
			                           ->where('ticket_id', $ticket->id)
			                           ->count();
			if (!$typeCount)
			{
				CheckoutFieldValue::where('ticket_id', $ticket->id)->delete();
			}

			// delete all ticket booking image
			if ($ticketBooking->gallery->count())
			{
				TicketBookingGallery::where('id', $ticketBookingId)->forceDelete();
			}

			$this->setCreatedHandlerId($ticket, $userId);

			$this->dispatch(new DeleteScheduleSettlementBooking(['ticketBookingId' => $ticketBooking->id]));

			$res = ['success' => true];
		}
		else
		{
			$res = [
				'success' => false,
				'error'   => "Couldn't delete ticket map booking details. Please try again later. If this issue persists, report to admin."
			];
		}

		return response()->json($res);
	}

	public function fetchCancellationCharges($ticketBookingId)
	{
		if (!$ticketBooking = \TicketBooking::find($ticketBookingId))
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Unable to find ticket booking to cancel'
			                        ]);
		}

		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.cancellations.prefix') . '/' . $ticketBookingId . '/calculate';

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'isCustomer' => request('isCustomer')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function cancelTicketBooking($ticketBookingId)
	{
		if (!$ticketBooking = \TicketBooking::find($ticketBookingId))
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Unable to find ticket booking to cancel'
			                        ]);
		}

		// make API call to fetch partner adjustments list
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.cancellations.prefix') . '/' . $ticketBookingId . '/cancel';

		$options = [
			'method'      => "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'isCustomer'   => request('isCustomer'),
				'comments'     => request('comments'),
				'sendReceipts' => true
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (isset($res['success']) && !$res['success'])
		{
			$error = 'Some error occurred cancelling ticket booking';

			if (isset($res['error']) && $res['error'])
			{
				$error = $res['error'];
			}

			// log_site_error
			$this->saveError([
				                 'fullUrl' => request()->fullUrl(),
				                 'message' => 'Ticket Booking Cancel APT API - ' . $error,
				                 'code'    => 0,
				                 'details' => $error
			                 ]);
		}

		return response()->json($res);
	}

	/*
	 * Gallery section
	 * @author: vikash <vikash@evibe.in>
	 * @Since: 11 August 2016
	 */
	public function uploadImages($bookingId)
	{
		$type = Input::get('type');

		// upload from a link
		if ($type && $type == 1)
		{
			$res = ['success' => false];
			$imageUrl = trim(Input::get('imageLink'));
			$rules = ['imageLink' => 'required|active_url'];
			$messages = [
				'imageLink.required'   => 'Please enter an url to upload image.',
				'imageLink.active_url' => 'This is an invalid url.'
			];

			$validator = validator(['imageLink' => $imageUrl], $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validator->messages()->first()
				                        ]);

			}

			$validImage = getimagesize($imageUrl);
			if ($validImage)
			{
				$filename = pathinfo($imageUrl, PATHINFO_FILENAME);
				$saveGallery = TicketBookingGallery::create([
					                                            'ticket_booking_id' => $bookingId,
					                                            'url'               => $imageUrl,
					                                            'type'              => $type,
					                                            'title'             => $filename
				                                            ]);

				if ($saveGallery)
				{
					$ticket = \TicketBooking::find($bookingId)->ticket;
					$this->setCreatedHandlerId($ticket);

					return response()->json(['success' => true]);
				}
			}
			else
			{
				return response()->json(['success' => false, 'error' => 'This is an invalid image url, Please try with a new one']);
			}

			return response()->json($res);
		}
		else
		{
			// upload from computer
			$images = Input::file('images');
			$ticket = \TicketBooking::find($bookingId)->ticket;
			$redirectErrorUrl = route('ticket.details.bookings', $ticket->id) . '#' . $bookingId . '?error=true';
			$redirectUrl = route('ticket.details.bookings', $ticket->id) . '#' . $bookingId;
			$uploadPath = '/ticket/' . $ticket->id . '/' . $bookingId . '/';

			foreach ($images as $image)
			{
				//validation
				$rules = [
					'image' => 'required|mimes:png,jpeg,jpg,JPG|max:1024'
				];
				$messages = [
					'image.required' => 'Please choose at-least one image to upload.',
					'image.mimes'    => 'Please upload a valid image file.only png, jpeg, jpg is allowed.',
					'image.max'      => 'Image size cannot be more than 1 MB. please compress the image and upload again.'
				];

				$validator = validator(['image' => $image], $rules, $messages);

				if ($validator->fails())
				{
					return redirect($redirectErrorUrl)->withErrors($validator, 'bookingGalleryError')
					                                  ->withInput(['bookingId' => $bookingId]);
				}

				//upload image
				$imageUrl = $this->uploadImageToServer($image, $uploadPath);

				$saveGallery = TicketBookingGallery::create([
					                                            'ticket_booking_id' => $bookingId,
					                                            'url'               => $imageUrl,
					                                            'type'              => 2,
					                                            'title'             => pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
				                                            ]);

				if (!$saveGallery)
				{
					return redirect($redirectErrorUrl)->with('bookingGalleryError', 'Error while saving images name in database')
					                                  ->withInput(['bookingId' => $bookingId]);
				}
			}

			$this->setCreatedHandlerId($ticket);

			return redirect($redirectUrl)->with('successMessage' . $bookingId, 'Image Uploaded successfully');
		}
	}

	public function deleteImage($id)
	{
		$gallery = TicketBookingGallery::find($id);

		if ($gallery)
		{
			$ticketBookingId = $gallery->ticket_booking_id;
			$ticket = \TicketBooking::find($ticketBookingId)->ticket;
			$redirectUrl = route('ticket.details.bookings', $ticket->id) . '#' . $ticketBookingId;

			// if type is not 1, then its just a link
			if ($gallery->type !== 1)
			{
				$allImages = $gallery->getAllImageFilePath();

				//delete all image from server
				foreach ($allImages as $image)
				{
					File::delete($image);
				}
			}

			//delete image name from database
			$gallery->forceDelete();

			$this->setCreatedHandlerId($ticket);

			return redirect($redirectUrl)->with('successMessage' . $ticketBookingId, 'Image Deleted successfully');
		}
	}

	public function saveRefundAmount($bookingId)
	{
		$booking = \TicketBooking::find($bookingId);
		if ($booking)
		{
			$images = Input::file('refundImages');
			$handler = Auth::user();

			$booking->update([
				                 'refund_amount'       => request('inputRefundAmt'),
				                 'refund_comments'     => request('inputRefundComments'),
				                 'refund_mail_sent_at' => Carbon::now(),
				                 'refund_handler_id'   => $handler->id,
			                 ]);

			if (count($images) && !is_null($images[0]))
			{
				foreach ($images as $image)
				{
					$imageRules = [
						'file' => 'required|mimes:png,jpeg,jpg,JPG|max:1024'
					];

					$imageMessages = [
						'file.required' => 'Please upload atleast one image or video',
						'file.mimes'    => 'One of the images/video is not valid. (only .png, .jpeg, .jpg, .mp4, .x-flv, .x-mpegURL, .MP2T, .3gpp, .quicktime, .x-msvideo, .x-ms-wmv are accepted)',
						'file.max'      => 'Image size cannot br more than 1 MB'
					];

					$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

					if ($imageValidator->fails())
					{
						return redirect()->back()->with(['errorMsgRefund' => $imageValidator->messages()->first()]);
					}
				}

				$directPath = '/ticket/' . $booking->ticket_id . '/' . $bookingId . '/refund/';

				$count = 0;
				foreach ($images as $image)
				{
					$count++;
					$option = ['isWatermark' => false, 'isOptimize' => false];

					$imageName = $this->uploadImageToServer($image, $directPath, $option);

					$renameImage = $booking->booking_id . "_" . $count;
					rename(config("evibe.gallery.root") . '/ticket/' . $booking->ticket_id . '/' . $bookingId . '/refund/' . $imageName, config("evibe.gallery.root") . '/ticket/' . $booking->ticket_id . '/' . $bookingId . '/refund/' . $renameImage);

					$imgInsertData = [
						'url'               => $renameImage,
						'title'             => $this->getImageTitle($image),
						'ticket_booking_id' => $bookingId
					];

					RefundGallery::create($imgInsertData);
				}
			}

			$emailData = [
				'booking_id'      => $bookingId,
				'refund_amount'   => request('inputRefundAmt'),
				'refund_comments' => request('inputRefundComments'),
				'handler'         => $handler
			];

			$this->dispatch(new RefundAmountMail($emailData));
			$this->setCreatedHandlerId($booking->ticket, $handler->id);

			return redirect()->back()
			                 ->with(['successMsgRefund' => "Refund amount has been updated successfully."]);
		}

		return redirect()->back()->with(['errorMsgRefund' => "Some error occurred, please try again."]);
	}

	public function updatePlanner($bookingId)
	{
		try
		{
			$booking = \TicketBooking::find($bookingId);

			if ($booking)
			{
				$mapId = request('mapId');
				$mapTypeId = request("is_venue") != 1 ? config('evibe.ticket_type.planners') : config('evibe.ticket_type.venues');

				$booking->update([
					                 'map_id'             => $mapId,
					                 'map_type_id'        => $mapTypeId,
					                 'is_partner_changed' => 1
				                 ]);

				$ticket = $booking->ticket;
				$this->setCreatedHandlerId($ticket);

				if ($ticket->status_id == config('evibe.status.auto_cancel'))
				{
					$ticket->status_id = config('evibe.status.auto_pay');
					$ticket->save();
				}

				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          'comments' => 'Partner added to the booking ' . $booking->booking_id,
				                          ]);

				return response()->json([
					                        'success'    => true,
					                        'successMsg' => 'Partner updated successfully'
				                        ]);
			}
			else
			{
				// error that cannot find booking
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.partner_update'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketBookingController.php - ' . __LINE__ . '] Unable to find ticket booking to update partner',
					                                   'exception' => '[TicketBookingController.php - ' . __LINE__ . '] Unable to find ticket booking to update partner',
					                                   'trace'     => '[Ticket Booking Id: ' . $bookingId . ']',
					                                   'details'   => '[Ticket Booking Id: ' . $bookingId . ']'
				                                   ]);

				return response()->json(['success' => false]);
			}

		} catch (Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json(['error' => true]);
		}
	}

	public function resendBookingCancellationReceipts($ticketBookingId)
	{
		try
		{
			$user = Auth::user();
			$accessToken = $this->getAccessTokenFromUserId($user);
			$url = config('evibe.api.base_url') . config('evibe.api.finance.cancellations.prefix') . '/' . $ticketBookingId . '/resend-cancel-receipts';

			$options = [
				'method'      => "GET",
				'url'         => $url,
				'accessToken' => $accessToken,
				'jsonData'    => []
			];

			$res = $this->makeApiCallWithUrl($options);

			if (isset($res['success']) && !$res['success'])
			{
				$error = 'Some error occurred while sending cancellation receipts';

				if (isset($res['error']) && $res['error'])
				{
					$error = $res['error'];
				}

				$this->sendNonExceptionErrorReport([
					                                   'code'      => 0,
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketBookingController.php - ' . __LINE__ . ']' . $error,
					                                   'exception' => '[TicketBookingController.php - ' . __LINE__ . ']' . $error,
					                                   'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
					                                   'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
				                                   ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => $error
				                        ]);
			}

			return response()->json([
				                        'success'    => true,
				                        'successMsg' => isset($res['successMsg']) && $res['successMsg'] ? $res['successMsg'] : 'Successfully sent cancellation receipts'
			                        ]);
		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	public function updateBookingPartner($ticketBookingId)
	{
		// @see: along with partner, product and its details will also be changed
		try
		{

			$user = Auth::user();

			// get ticket, booking and mapping
			$ticketBooking = \TicketBooking::find($ticketBookingId);
			if (!$ticketBooking)
			{
				Log::error("Unable to fetch booking data from ticketBookingId: $ticketBookingId");

				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Unable to fetch booking data'
				                        ]);
			}

			$ticket = \Ticket::find($ticketBooking->ticket_id);
			if (!$ticket)
			{
				Log::error("Unable to fetch ticket data from ticketBookingId: $ticketBookingId");

				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Unable to fetch ticket data'
				                        ]);
			}

			$ticketMapping = \TicketMapping::find($ticketBooking->ticket_mapping_id);
			if (!$ticketMapping)
			{
				Log::error("Unable to fetch ticket mapping data from ticketBookingId: $ticketBookingId");

				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Unable to fetch ticket mapping data'
				                        ]);
			}

			$newProductId = request('newProductId');
			$newProductTypeId = request('newProductTypeId');
			$newPartnerId = request('newPartnerId');
			$newPartnerTypeId = request('newPartnerTypeId');
			$newBookingAmount = request('newBookingAmount');
			$newAdvanceAmount = request('newAdvanceAmount');
			$timeUpdate = "";

			if (!$newPartnerId || !$newPartnerTypeId)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly select a partner to proceed'
				                        ]);
			}

			if (!$newBookingAmount || !$newAdvanceAmount)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly enter booking and advance amounts'
				                        ]);
			}

			// @see: booking amount of new product can be less than the previous advance paid
			//if ($newAdvanceAmount > $newBookingAmount)
			//{
			//	return response()->json([
			//		                        'success'  => false,
			//		                        'errorMsg' => 'Advance amount cannot be greater than booking amount'
			//	                        ]);
			//}

			$productData = null;
			// update product and product related details
			if ($newProductId && $newProductTypeId && (($ticketMapping->map_id != $newProductId) || ($ticketMapping->map_type_id != $newProductTypeId)))
			{
				$ticketMapping->map_id = $newProductId;
				$ticketMapping->map_type_id = $newProductTypeId;
				$ticketMapping->type_id = null;
				$ticketMapping->type_details = null;

				// remove checkout fields
				$ticketBooking->type_ticket_booking_id = null;
				$ticketBooking->booking_type_details = null;

				// update product related details like info, pre-pre, facts
				$productData = AppUtil::fillMappingValues([
					                                          'isCollection' => false,
					                                          'mapTypeId'    => $newProductTypeId,
					                                          'mapId'        => $newProductId
				                                          ]);
				$ticketBooking->booking_info = isset($productData['info']) ? $productData['info'] : null;
				$ticketBooking->prerequisites = isset($productData['prerequisites']) ? $productData['prerequisites'] : null;
				$ticketBooking->facts = isset($productData['facts']) ? $productData['facts'] : null;

				$timeUpdate .= "Product has been changed. ";
			}

			// update partner
			$oldPartnerId = $ticketBooking->map_id;
			$oldPartnerTypeId = $ticketBooking->map_type_id;
			$ticketBooking->map_id = $newPartnerId;
			$ticketBooking->map_type_id = $newPartnerTypeId;
			$ticketBooking->old_partner_id = $oldPartnerId;
			$ticketBooking->old_partner_type_id = $oldPartnerTypeId;

			if ($newPartnerId && $newPartnerTypeId && $oldPartnerId && $oldPartnerTypeId && (($newPartnerId != $oldPartnerId) || ($newPartnerTypeId != $oldPartnerTypeId)))
			{
				$ticketBooking->is_partner_changed = 1;
				$ticketBooking->partner_changed_at = Carbon::now();
				$ticketBooking->partner_confirmed_at = Carbon::now();
			}

			// @see: package provided by the same partner might be changed
			//if (($oldPartnerId == $newPartnerId) && ($oldPartnerTypeId == $newPartnerTypeId))
			//{
			//	return response()->json([
			//		                        'success' => false,
			//		                        'error'   => 'Kindly select a different partner'
			//	                        ]);
			//}

			if ($newPartnerTypeId == config('evibe.ticket_type.venues'))
			{
				$ticketBooking->is_venue_booking = 1;
			}

			// update price details
			if ($newBookingAmount && $newAdvanceAmount && (($ticketBooking->booking_amount != $newBookingAmount) || ($ticketBooking->advance_amount != $newAdvanceAmount)))
			{

				// check if advance amount accommodates to 30% of booking amount
				$minAdvanceAmount = round($newBookingAmount * 0.3);
				if ($ticketBooking->advance_amount < $minAdvanceAmount)
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => 'Advance paid should accommodate to at least 30% of booking amount'
					                        ]);
				}

				// create refund if advance amount is more than booking amount
				if ($ticketBooking->advance_amount > $newBookingAmount)
				{
					$refundableAmount = $ticketBooking->advance_amount - $newBookingAmount;

					if ($refundableAmount && $ticketBooking->is_advance_paid)
					{
						// create refund
						// make API call to create customer refund
						$accessToken = $this->getAccessTokenFromUserId($user);
						$url = config('evibe.api.base_url') . config('evibe.api.finance.refunds.prefix') . '/' . $ticketBooking->id . '/create';

						$options = [
							'method'      => "POST",
							'url'         => $url,
							'accessToken' => $accessToken,
							'jsonData'    => [
								'refundAmount' => $refundableAmount,
								'evibeBear'    => 0,
								'partnerBear'  => 0,
								'comments'     => 'Refund created due to change in option where customer previously paid more than the updated option',
							]
						];

						$res = $this->makeApiCallWithUrl($options);

						if (isset($res['success']) && !$res['success'])
						{
							$this->sendNonExceptionErrorReport([
								                                   'code'      => config('evibe.error_code.partner_update'),
								                                   'url'       => request()->fullUrl(),
								                                   'method'    => request()->method(),
								                                   'message'   => '[TicketBookingController.php - ' . __LINE__ . '] Some error occurred while creating refund.',
								                                   'exception' => '[TicketBookingController.php - ' . __LINE__ . '] Some error occurred while creating refund.',
								                                   'trace'     => '[Ticket Id: ' . $ticket->id . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
								                                   'details'   => '[Ticket Id: ' . $ticket->id . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
							                                   ]);
						}
					}

					$newAdvanceAmount = $ticketBooking->advance_amount - $refundableAmount;
				}

				$ticketBooking->product_price = $newBookingAmount;
				$ticketBooking->booking_amount = $newBookingAmount;
				$ticketBooking->advance_amount = $newAdvanceAmount;

				// @see: should check how transportation charges are affecting

				$timeUpdate .= 'Price details has been changed.';
			}

			$ticketBooking->save();
			$ticketMapping->save();

			// get all the bookings and update the address
			$ticketBookings = \TicketBooking::where('ticket_id', $ticket->id)->get();
			if (count($ticketBookings))
			{
				foreach ($ticketBookings as $booking)
				{
					if ($booking->map_type_id == config('evibe.ticket_type.venues'))
					{
						// update venue details of the first venue partner
						$partner = $booking->provider;

						// map link
						$mapLat = $partner->map_lat ?: 0;
						$mapLong = $partner->map_long ?: 0;
						$mapLink = "https://www.google.co.in/maps/@$mapLat,$mapLong,15z";

						$zipCode = $partner->zip;
						$venueAddress = $partner->full_address . ", Pin Code: " . $zipCode;
						$areaId = $partner->area_id;
						$venueLandmark = $partner->landmark;
						$mapAddress = $partner->name . ", " . $venueLandmark;
						$typeVenueId = $partner->type_id;

						$venueMapping = $booking->mapping;

						// if the mapping is a venue package, use its address rather than direct venue itself
						if (in_array($venueMapping->map_type_id, [
							config('evibe.ticket_type.packages'),
							config('evibe.ticket_type.villa'),
							config('evibe.ticket_type.food'),
							config('evibe.ticket_type.couple-experiences'),
							config('evibe.ticket_type.priests'),
							config('evibe.ticket_type.tents'),
							config('evibe.ticket_type.generic-package')
						]))
						{
							$venuePackage = \VendorPackage::find($venueMapping->map_id);
							if ($venuePackage && ($venuePackage->map_type_id == config('evibe.ticket_type.venues')))
							{
								// map link
								$mapLat = $venuePackage->map_lat ?: 0;
								$mapLong = $venuePackage->map_long ?: 0;
								$mapLink = "https://www.google.co.in/maps/@$mapLat,$mapLong,15z";

								$zipCode = $venuePackage->zip;
								$venueAddress = $venuePackage->full_address . ", Pin Code: " . $zipCode;
								$areaId = $venuePackage->area_id;
								$venueLandmark = $venuePackage->landmark;
								$mapAddress = $partner->name . ", " . $venueAddress;
								$typeVenueId = $venuePackage->type_venue_id;
							}
						}

						$ticket->area_id = $areaId ?: null;
						$ticket->zip_code = $zipCode ?: null;
						$ticket->venue_address = $venueAddress ?: null;
						$ticket->venue_landmark = $venueLandmark ?: null;
						$ticket->map_link = $mapLink;
						$ticket->map_address = $mapAddress;
						$ticket->type_venue_id = $typeVenueId;

						break;
					}
				}
			}

			// make this null to ensure that team sends updated receipt after changing partner or product
			$ticket->updated_receipt_sent_at = null;

			$ticket->save();

			// ticket update action
			$ticketUpdate = [
				'statusId' => $ticket->status_id,
				'ticket'   => $ticket,
				'comments' => config('evibe.ticket_status_message.partner_change') . $timeUpdate
			];
			$this->updateTicketAction($ticketUpdate);

			// manage settlements
			// either partner might be changed or amount details might have changed
			// since party is not yet done, settlement won't be created
			if ($ticketBooking->is_advance_paid)
			{
				$this->dispatch(new UpdateScheduleSettlementBooking([
					                                                    'ticketBookingId' => $ticketBooking->id,
					                                                    'handlerId'       => $user->id
				                                                    ]));
			}

			$successMsg = 'Booking details have been successfully updated.';

			// send communication if partner has been changed and if ticket is in booked status
			// dispatch job
			$oldPartner = $ticketBooking->oldProvider;
			if (($ticket->status_id == config('evibe.status.booked')) && $oldPartner && (($newPartnerId != $oldPartnerId) || ($newPartnerTypeId != $oldPartnerTypeId)))
			{
				/*
				 * Clearly mention regarding the cancelled booking info
				 * Show a table with all the bookings mapped to the partner for that ticket and show individual booking status
				 * Show (booking for a particular partner for a ticket): currently cancelled + remaining live (or) [all the cancelled + pending live]
				 * */

				$partnerBookingsData = [];
				$livePartnerBookingsCount = 0;
				$bookedBookings = \TicketBooking::where('ticket_id', $ticket->id)
				                                ->where('is_advance_paid', 1)
				                                ->whereNull('deleted_at')
				                                ->get();
				$partnerBookings = $bookedBookings->where('map_id', $oldPartnerId)
				                                  ->where('map_type_id', $oldPartnerTypeId);

				// @see: add immediately changed old partner bookings to show in the table
				$changedPartnerBookings = $bookedBookings->where('old_partner_id', $oldPartnerId)
				                                         ->where('old_partner_type_id', $oldPartnerTypeId);

				if (count($partnerBookings))
				{
					foreach ($partnerBookings as $partnerBooking)
					{
						$partnerBookingOption = AppUtil::fillMappingValues([
							                                                   'isCollection' => false,
							                                                   'mapId'        => $partnerBooking->mapping->map_id,
							                                                   'mapTypeId'    => $partnerBooking->mapping->map_type_id
						                                                   ]);

						$partnerBookingOptionType = $partnerBooking->booking_type_details;
						if (in_array($partnerBookingOptionType, [
							'Venue',
							'Provider',
							'Planner'
						]))
						{
							$partnerBookingOptionType = null;
						}

						array_push($partnerBookingsData, [
							'bookingOption' => isset($partnerBookingOption['name']) && $partnerBookingOption['name'] ? $partnerBookingOption['name'] : ($partnerBookingOptionType ? $partnerBookingOptionType : null),
							'bookingId'     => $partnerBooking->booking_id,
							'bookingAmount' => $partnerBooking->booking_amount,
							'partyDateTime' => $partnerBooking->party_date_time ? date('d M Y, h:i A', $partnerBooking->party_date_time) : null,
							'bookingStatus' => ($partnerBooking->cancelled_at == null) ? 1 : 0
						]);
					}

					$livePartnerBookings = $partnerBookings->filter(function ($item) {
						return $item->cancelled_at == null;
					});
					$livePartnerBookingsCount = $livePartnerBookings ? $livePartnerBookings->count() : 0;
				}

				if (count($changedPartnerBookings))
				{
					foreach ($changedPartnerBookings as $changedPartnerBooking)
					{
						$changedPartnerBookingOption = AppUtil::fillMappingValues([
							                                                          'isCollection' => false,
							                                                          'mapId'        => $changedPartnerBooking->mapping->map_id,
							                                                          'mapTypeId'    => $changedPartnerBooking->mapping->map_type_id
						                                                          ]);

						$changedPartnerBookingOptionType = $changedPartnerBooking->booking_type_details;
						if (in_array($changedPartnerBookingOptionType, [
							'Venue',
							'Provider',
							'Planner'
						]))
						{
							$changedPartnerBookingOptionType = null;
						}

						array_push($partnerBookingsData, [
							'bookingOption' => isset($changedPartnerBookingOption['name']) && $changedPartnerBookingOption['name'] ? $changedPartnerBookingOption['name'] : ($changedPartnerBookingOptionType ? $changedPartnerBookingOptionType : null),
							'bookingId'     => $changedPartnerBooking->booking_id,
							'bookingAmount' => $changedPartnerBooking->booking_amount,
							'partyDateTime' => $changedPartnerBooking->party_date_time ? date('d M Y, h:i A', $changedPartnerBooking->party_date_time) : null,
							'bookingStatus' => 0 // as the booking has been changed to some other partner
						]);
					}
				}

				// @todo: old partner rejection
				if (!$productData)
				{
					$productData = AppUtil::fillMappingValues([
						                                          'isCollection' => false,
						                                          'mapTypeId'    => $ticketMapping->map_type_id,
						                                          'mapId'        => $ticketMapping->map_id
					                                          ]);
				}

				// will be helpful in-case option name can't be fetched
				// do not consider if the booking type is generic partner types
				$optionType = $ticketBooking->booking_type_details;
				if (in_array($optionType, [
					'Venue',
					'Provider',
					'Planner'
				]))
				{
					$optionType = null;
				}

				$data = [
					'partnerName'              => $oldPartner->person,
					'partnerCompanyName'       => $oldPartner->name,
					'partnerMobile'            => $oldPartner->phone,
					'partnerEmail'             => $oldPartner->email,
					'bookingId'                => $ticketBooking->booking_id,
					'bookingAmount'            => $ticketBooking->booking_amount,
					'partyDateTime'            => $ticketBooking->party_date_time ? date("d M Y", $ticketBooking->party_date_time) : null,
					'optionName'               => isset($productData['name']) && $productData['name'] ? $productData['name'] : ($optionType ? $optionType : null),
					'customerName'             => $ticket->name,
					'partnerBookings'          => $partnerBookingsData,
					'livePartnerBookingsCount' => $livePartnerBookingsCount,
					'ticketId'                 => $ticket->id
				];

				// send cancellation email to old partner
				$this->dispatch(new SendRejectionMailToPartner($data));

				$successMsg .= ' Booking cancellation receipt sent to the old partner';

				// ticket update action
				$ticketUpdate = [
					'statusId' => $ticket->status_id,
					'ticket'   => $ticket,
					'comments' => config('evibe.ticket_status_message.old_partner_cancellation')
				];
				$this->updateTicketAction($ticketUpdate);
			}

			return response()->json([
				                        'success'    => true,
				                        'successMsg' => $successMsg
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function markStarOrder($ticketBookingId)
	{
		try
		{

			$handler = Auth::user();
			// manual star order - there is no star data for this option
			$partnerAmount = request('partnerAmount');
			if (!$partnerAmount)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly enter partner amount to mark this booking as star order'
				                        ]);
			}

			$ticketBooking = \TicketBooking::find($ticketBookingId);
			if (!$ticketBooking)
			{
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.update_function'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketBookingController.php - ' . __LINE__ . ']. Unable to find ticket booking from id.',
					                                   'exception' => '[TicketBookingController.php - ' . __LINE__ . ']. Unable to find ticket booking from id.',
					                                   'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
					                                   'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
				                                   ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$ticket = \Ticket::find($ticketBooking->ticket_id);
			if (!$ticket)
			{
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.update_function'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketBookingController.php - ' . __LINE__ . ']. Unable to find ticket from id.',
					                                   'exception' => '[TicketBookingController.php - ' . __LINE__ . ']. Unable to find ticket from id.',
					                                   'trace'     => '[Ticket Id: ' . $ticketBooking->ticket_id . ']',
					                                   'details'   => '[Ticket Id: ' . $ticketBooking->ticket_id . ']'
				                                   ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$ticketBooking->is_star_order = 1;
			$ticketBooking->star_partner_amount = $partnerAmount;
			$ticketBooking->updated_at = Carbon::now();
			$ticketBooking->save();

			$ticket->updated_at = Carbon::now();
			$ticket->save();

			// Ticket update action
			$ticketUpdate = [
				'statusId' => config('evibe.status.booked'),
				'ticket'   => $ticket,
				'comments' => config('evibe.ticket_status_message.mark_star_order') . ' [' . $ticketBooking->booking_id . '].'
			];
			$this->updateTicketAction($ticketUpdate);

			$this->dispatch(new UpdateScheduleSettlementBooking([
				                                                    'ticketBookingId' => $ticketBookingId,
				                                                    'handlerId'       => $handler->id
			                                                    ]));

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}
}