<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;
use App\Jobs\Emails\Ticket\TicketEInviteAlert;
use App\Models\Ticket\TicketInvite;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;

class TicketBenefitServicesController extends BaseTicketController
{
	public function generateRSVPLink($ticketId)
	{
		try
		{

			$ticket = \Ticket::find($ticketId);
			if (!$ticketId)
			{
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.create_function'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => 'GET',
					                                   'message'   => '[TicketBenefitServicesController.php - ' . __LINE__ . '] ' . 'Unable to find ticket data for ticket',
					                                   'exception' => '[TicketBenefitServicesController.php - ' . __LINE__ . '] ' . 'Unable to find ticket data for ticket',
					                                   'trace'     => '[Ticket Id: ' . $ticket->id . ']',
					                                   'details'   => '[Ticket Id: ' . $ticket->id . ']'
				                                   ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket data'
				                        ]);
			}

			// generate RSVP link
			// it will be saved along with invites
			$RSVPLink = $this->getRSVPLink($ticket->id, $ticket->name);

			return response()->json([
				                        'success'  => true,
				                        'RSVPLink' => $RSVPLink
			                        ]);

		} catch (\Exception $exception)
		{
			return response()->json(['success' => false]);
		}
	}

	public function resendEInvite($ticketId)
	{
		try
		{
			$res = $this->sendEInvitationToCustomer($ticketId);
			if (isset($res['success']) && !$res['success'])
			{
				$error = 'Some error occurred while re-sending notifications to customer';

				if (isset($res['error']) && $res['error'])
				{
					$error = $res['error'];
				}

				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.update_function'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketBenefitServicesController.php - ' . __LINE__ . ']' . $error,
					                                   'exception' => '[TicketBenefitServicesController.php - ' . __LINE__ . ']' . $error,
					                                   'trace'     => '[Ticket Id: ' . $ticketId . ']',
					                                   'details'   => '[Ticket Id: ' . $ticketId . ']'
				                                   ]);
			}

			return response()->json([
				                        'success' => true
			                        ]);
		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function sendEInvite($ticketId)
	{
		try
		{

			$ticket = \Ticket::find($ticketId);
			if (!$ticketId)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket'
				                        ]);
			}

			$images = Input::file('images');

			if (!count($images) || (count($images) && is_null($images[0])))
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly choose an image to upload'
				                        ]);
			}

			if (count($images) > 1)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Upload only one invitation image'
				                        ]);
			}

			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:4096'
				];

				$imageMessages = [
					'file.required' => 'Please upload at least one image',
					'file.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
					'file.max'      => 'Image size cannot be more than 4 MB'
				];

				$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

				if ($imageValidator->fails())
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => $imageValidator->messages()->first()
					                        ]);
				}
			}

			$directPath = '/ticket/' . $ticketId . '/invites/';
			$RSVPLink = $this->getRSVPLink($ticket->id, $ticket->name);

			$image = $images[0];

			$option = [
				'isWatermark' => false,
				'isOptimize'  => false
			];

			$imageName = $this->uploadImageToServer($image, $directPath, $option);

			$ticketInvite = TicketInvite::create([
				                                     'ticket_id'  => $ticketId,
				                                     'title'      => $this->getImageTitle($image),
				                                     'url'        => $imageName,
				                                     'rsvp_link'  => $RSVPLink,
				                                     'created_at' => Carbon::now(),
				                                     'updated_at' => Carbon::now(),
			                                     ]);

			$res = $this->sendEInvitationToCustomer($ticketId, $ticketInvite);
			if (isset($res['success']) && !$res['success'])
			{
				$error = 'Some error occurred while sending notifications to customer';

				if (isset($res['error']) && $res['error'])
				{
					$error = $res['error'];
				}

				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.update_function'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[TicketBenefitServicesController.php - ' . __LINE__ . ']' . $error,
					                                   'exception' => '[TicketBenefitServicesController.php - ' . __LINE__ . ']' . $error,
					                                   'trace'     => '[Ticket Id: ' . $ticketId . ']',
					                                   'details'   => '[Ticket Id: ' . $ticketId . ']'
				                                   ]);
			}

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function sendEInvitationToCustomer($ticketId, $ticketInvite = null)
	{
		$ticket = \Ticket::find($ticketId);
		if (!$ticket)
		{
			return $res = [
				'success' => false,
				'error'   => 'Unable to find ticket data for ticket id: ' . $ticketId
			];
		}

		if (!$ticketInvite)
		{
			$ticketInvite = TicketInvite::where('ticket_id', $ticketId)
			                            ->whereNull('deleted_at')
			                            ->orderBy('invite_sent_at', 'DESC')
			                            ->orderBy('updated_at', 'DESC')
			                            ->first();

			if (!$ticketInvite)
			{
				return $res = [
					'success' => false,
					'error'   => 'Unable to find invitation for ticket with id: ' . $ticketId
				];
			}

			if (!$ticketInvite->rsvp_link)
			{
				$ticketInvite->rsvp_link = $this->getRSVPLink($ticket->id, $ticket->name);
				$ticketInvite->updated_at = Carbon::now();
				$ticketInvite->save();
			}
		}

		// send communication
		$directPath = '/ticket/' . $ticketId . '/invites/';
		$mailData = [
			'customerName'     => ucwords($ticket->name),
			'customerEmail'    => $ticket->email,
			'customerAltEmail' => $ticket->alt_email,
			'RSVPLink'         => $ticketInvite->rsvp_link,
			'RSVPImage'        => config('evibe.gallery.host') . $directPath . $ticketInvite->url
		];

		$this->dispatch(new TicketEInviteAlert($mailData));

		$customerTemplate = config('evibe.sms_tpl.invite.notify-card');
		$customerReplaces = [
			'#customerName#' => ucfirst($ticket->name),
			'#inviteLink#'   => $ticketInvite->rsvp_link,
		];
		$customerSMS = str_replace(array_keys($customerReplaces), array_values($customerReplaces), $customerTemplate);

		if ($customerSMS && $ticket->phone)
		{
			$customerSMSData = [
				'to'   => $ticket->phone,
				'text' => $customerSMS
			];
			Queue::push('Evibe\Utilities\SendSMS@smsInviteCardToCustomer', $customerSMSData);
		}

		// update ticket
		$ticketInvite->invite_sent_at = Carbon::now();
		$ticketInvite->updated_at = Carbon::now();
		$ticketInvite->save();

		return $res = [
			'success' => true,
		];
	}

	public function getRSVPLink($ticketId, $ticketName)
	{
		// @see: because of architecture mishap
		$ticketInvite = TicketInvite::where('ticket_id', $ticketId)
		                            ->whereNull('deleted_at')
		                            ->whereNotNull('rsvp_link')
		                            ->orderBy('invite_sent_at')
		                            ->first();
		if ($ticketInvite)
		{
			// if invitation exists, take that link
			$RSVPLink = $ticketInvite->rsvp_link;
		}
		else
		{
			// else calculate
			// @see: not much validation required
			$ticketName = $ticketName ? ucfirst(substr($ticketName, 0, 3)) : 'Cus';
			$url = $ticketName . $ticketId;

			$RSVPLink = config('evibe.live.host') . '/iv/' . $url;
			//$RSVPLink .= '&utm_campaign=RSVP&utm_source=' . $ticketId; // utm tagging (medium not included in-order to get unique link
			//$RSVPLink = $this->getShortUrl($RSVPLink);
		}

		return $RSVPLink;
	}
}