<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;

use App\Models\TicketFollowup;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Evibe\Facades\AppUtilFacade as AppUtil;

class TicketUpdateController extends BaseTicketController
{

	/**
	 * Save status update
	 *
	 * @author Anji <anji@evibe.in>
	 * @since  21 Oct 2015
	 */
	public function saveStatusUpdate($ticketId)
	{
		$userId = Auth::user()->id;
		$ticket = \Ticket::find($ticketId);
		$isAjaxRequest = Input::get('isAjaxRequest');
		$comments = null;
		$thisTicketStatus = null;
		$isFollowup = null;

		if ($ticket)
		{
			$thisTicketStatus = $ticket->status_id;

			if ($thisTicketStatus && ((($thisTicketStatus == config("evibe.status.confirmed") ||
							$thisTicketStatus == config("evibe.status.auto_pay")) ||
						($thisTicketStatus == config("evibe.status.service_auto_pay"))) ||
					($thisTicketStatus == config("evibe.status.booked"))))
			{
				if ($isAjaxRequest && $isAjaxRequest == 1)
				{
					$res = [
						"success" => false,
						"error"   => "You cannot update this ticket"
					];

					return response()->json($res);
				}

				return redirect()->back()->withInput();
			}

			$rules = [
				'tuDate'     => 'required|date',
				'tuStatus'   => 'required',
				'tuType'     => 'required',
				'tuComments' => 'required|min:4'
			];

			$messages = [
				'tuStatus.required'   => 'Please select status',
				'tuDate.required'     => 'Update date is required',
				'tuDate.date'         => 'Update date is invalid',
				'tuType.required'     => 'Update source is required',
				'tuComments.required' => 'Update comment is required',
				'tuComments.min'      => 'Comment should be at least 4 characters long'
			];

			// In case of followup status
			if ((Input::get('tuStatus') == config('evibe.status.no_response')) || (Input::get('tuStatus') == config('evibe.status.followup')))
			{
				$rules['followUpDate'] = 'required|date|after:now';

				$messages['followUpDate.required'] = 'Followup Date is required';
				$messages['followUpDate.date'] = 'Followup Date is invalid';
				$messages['followUpDate.after'] = 'Followup Date should be after current date';

				if (Input::get('tuStatus') == config('evibe.status.followup'))
				{
					$rules['followUpType'] = 'required';
					$rules['closureDate'] = 'required|date|after:now';
					$messages['followUpType.required'] = 'Followup Type is required';
					$messages['closureDate.required'] = 'Closure Date is required';
					$messages['closureDate.date'] = 'Closure Date is invalid';
					$messages['closureDate.after'] = 'Closure Date should be after current date';
				}

				$validation = Validator::make(Input::all(), $rules, $messages);
				$redirectUrl = route('ticket.details.updates', $ticketId);

				if ($validation->fails())
				{
					if ($isAjaxRequest && $isAjaxRequest == 1)
					{
						$res = [
							"success" => false,
							"error"   => $validation->messages()->first()
						];

						return response()->json($res);
					}

					return redirect($redirectUrl)
						->withErrors($validation)
						->withInput();
				}

				$followupType = Input::get('followUpType');

				$followupData = [
					'followup_date' => strtotime(Input::get('followUpDate')),
					'comments'      => $followupType,
					'ticket_id'     => $ticketId,
					'handler_id'    => $userId,
					'followup_type' => $followupType
				];

				$isFollowup = TicketFollowup::where('ticket_id', $ticketId)
				                            ->whereNull('is_complete')
				                            ->first();
				if ($isFollowup)
				{
					$isFollowup->update($followupData);
				}
				else
				{
					$isFollowup = TicketFollowup::create($followupData);
				}

				$ticket->expected_closure_date = strtotime(Input::get('closureDate'));
				$ticket->save();
			}

			$validation = Validator::make(Input::all(), $rules, $messages);
			$redirectUrl = route('ticket.details.updates', $ticketId);

			if ($validation->fails())
			{
				if ($isAjaxRequest && $isAjaxRequest == 1)
				{
					$res = [
						"success" => false,
						"error"   => $validation->messages()->first()
					];

					return response()->json($res);
				}

				return redirect($redirectUrl)
					->withErrors($validation)
					->withInput();
			}

			// Checking whether that ticket can be marked as duplicate
			if ((Input::get('tuStatus') && (Input::get('tuStatus')) == config("evibe.status.duplicate")))
			{
				$phoneNumber = $ticket->phone;

				if ($ticket->status_id == Input::get('tuStatus'))
				{
					if ($isAjaxRequest && $isAjaxRequest == 1)
					{
						$res = [
							"success" => false,
							"error"   => "Ticket is already in duplicate status"
						];

						return response()->json($res);
					}

					return redirect($redirectUrl)
						->withErrors('Status is already Duplicate')
						->withInput();
				}

				if ($phoneNumber)
				{
					$today = Carbon::now();
					$startOfMonth = Carbon::now()->subMonth(1);

					$isPhoneNumberExistsInLast30Days = \Ticket::where('created_at', '>=', $startOfMonth)
					                                          ->where('created_at', '<=', $today)
					                                          ->where('id', '!=', $ticket->id)
					                                          ->where('phone', $phoneNumber)
					                                          ->whereNull('deleted_at')
					                                          ->first();

					if (!$isPhoneNumberExistsInLast30Days)
					{
						// Error message showing that we cannot made it as duplicate
						if ($isAjaxRequest && $isAjaxRequest == 1)
						{
							$res = [
								"success" => false,
								"error"   => "You cannot mark it as Duplicate/Related"
							];

							return response()->json($res);
						}

						return redirect($redirectUrl)
							->withErrors('You cannot mark it as Duplicate/Related')
							->withInput();
					}
				}
			}

			// Checking whether that ticket can be marked as related
			if ((Input::get('tuStatus') && (Input::get('tuStatus')) == config("evibe.status.related")))
			{
				$relatedTicketId = Input::get('relatedTicket');

				if (\Ticket::find($relatedTicketId))
				{
					if ($ticket->related_ticket_id == $relatedTicketId)
					{
						if ($isAjaxRequest && $isAjaxRequest == 1)
						{
							$res = [
								"success" => false,
								"error"   => "The selected ticket has been marked as related"
							];

							return response()->json($res);
						}

						return redirect($redirectUrl)
							->withErrors('The selected ticket has been marked as related')
							->withInput();
					}

					$ticket->related_ticket_id = $relatedTicketId;
					$ticket->save();
				}
				else
				{
					if ($isAjaxRequest && $isAjaxRequest == 1)
					{
						$res = [
							"success" => false,
							"error"   => "Please select valid return ticket id"
						];

						return response()->json($res);
					}

					return redirect($redirectUrl)
						->withErrors('Please enter valid return ticket id')
						->withInput();
				}
			}

			// Checking whether that ticket can be marked as irrelevant
			if ((Input::get('tuStatus')) && (Input::get('tuStatus')) == config('evibe.status.irrelevant'))
			{
				if ($ticket && $ticket->status_id == config('evibe.status.irrelevant'))
				{
					if ($isAjaxRequest && $isAjaxRequest == 1)
					{
						$res = [
							"success" => false,
							"error"   => "This ticket has been marked as irrelevant"
						];

						return response()->json($res);
					}

					return redirect($redirectUrl)
						->withErrors('This ticket has been marked as irrelevant')
						->withInput();
				}
			}

			// Removing the followup if ticket is updated to any end state
			$invalidStatusArray = [
				config("evibe.status.enquiry"),
				config("evibe.status.cancelled"),
				config("evibe.status.irrelevant"),
				config("evibe.status.duplicate"),
				config("evibe.status.related"),
				config("evibe.status.service_auto_pay"),
				config("evibe.status.not_interested"),
				config("evibe.status.drop_off")
			];

			if (in_array(Input::get('tuStatus'), $invalidStatusArray))
			{
				$followUp = TicketFollowup::where('ticket_id', $ticketId)
				                          ->whereNull('is_complete')
				                          ->first();
				$comment = Input::get('tuComments');

				if ($followUp)
				{
					$updateData = [
						'handler_id'   => Auth::user()->id,
						'is_complete'  => 1,
						'done_comment' => $comment
					];
					if ($followUp->update($updateData))
					{
						$ticketUpdate = [
							'ticket'   => $ticket,
							'statusId' => Input::get('tuStatus'),
							'comments' => "Followups marked as done"
						];
						$this->updateTicketAction($ticketUpdate);
					}
				}
			}

			$followupType = Input::get('followUpType') ? "[" . Input::get('followUpType') . "]" : "";

			$ticketUpdateData = [
				'ticket'     => $ticket,
				'statusId'   => Input::get('tuStatus'),
				'comments'   => $followupType . Input::get('tuComments'),
				'statusAt'   => strtotime(Input::get('tuDate')),
				'typeUpdate' => Input::get('tuType')
			];

			$this->setCreatedHandlerId($ticket);
			$this->updateTicketAction($ticketUpdateData);

			if ($isAjaxRequest)
			{
				$updatedStatus = \TypeTicketStatus::find($ticket->status_id);
				$currentDate = date('[y/m/d H:i:s]', Carbon::now()->timestamp);
				$lastUpdateTemplate = '<div class="text-white font-1">' . $currentDate . '</div>' .
					'<div class="text-muted font-12">' . $currentDate . '</div>' . $followupType . Input::get('tuComments');
				$followUpTemplate = !is_null($isFollowup) && $isFollowup->followup_date ?
					'<div class="text-white font-1">' . date('y/m/d H:i:s', $isFollowup->followup_date) . '</div>' . date("d M y, g:i a", $isFollowup->followup_date) : "---";
				$closureDateTemplate = $ticket->expected_closure_date != "" ?
					'<div class="text-white font-1">' . date('y/m/d H:i:s', $ticket->expected_closure_date) . '</div>' . date("d M y", $ticket->expected_closure_date) : "---";

				$ticketData = [
					"status"       => $updatedStatus ? $updatedStatus->name : "Not Set",
					"lastUpdate"   => $lastUpdateTemplate,
					"followupDate" => $followUpTemplate,
					"closureDate"  => $closureDateTemplate,
					"phone"        => $ticket->phone
				];

				$res = [
					"success"    => true,
					"error"      => "Successfully updated the ticket",
					"ticketData" => $ticketData
				];

				return response()->json($res);
			}

			return redirect($redirectUrl);
		}
		else
		{
			if ($isAjaxRequest)
			{
				$res = [
					"success" => false,
					"error"   => "Ticket not found "
				];

				return response()->json($res);
			}

			return redirect()->back();
		}
	}

	//load ticket update dynamically for ticket list page
	public function loadTicketUpdate($ticketId)
	{
		$ticketUpdates = \TicketUpdate::where('ticket_id', $ticketId)
		                              ->orderBy('created_at', 'DESC')
		                              ->get();

		$updateData = [];

		foreach ($ticketUpdates as $ticketUpdate)
		{
			array_push($updateData, [
				'date'     => AppUtil::formatDateTimeForBooking($ticketUpdate->status_at),
				'status'   => $ticketUpdate->status->name,
				'handler'  => $ticketUpdate->handler ? $ticketUpdate->handler->name : 'Auto',
				'source'   => $ticketUpdate->type_update,
				'comments' => $ticketUpdate->comments
			]);
		}

		$res = [
			'success'       => true,
			'ticketUpdates' => $updateData
		];

		return json_encode($res);
	}
}
