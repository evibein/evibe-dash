<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;
use App\Models\Util\AccessToken;
use App\Models\Util\AuthClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class TicketUtilController extends BaseTicketController
{
	public function getAccessTokenFromUserId($user)
	{
		// default client id
		$clientId = config('evibe.api.client_id');

		$token = AccessToken::where('user_id', $user->id)
		                    ->where('auth_client_id', $clientId)
		                    ->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			if ($client)
			{
				$accessToken = $this->issueAccessToken($user, $client);

				return $accessToken;
			}
			else
			{
				return redirect(route("ticket.list"));
			}
		}
		else
		{
			return $token->access_token;
		}
	}

	public function makeApiCallWithUrl($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			return $res;

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;
			Log::error($res['error']);

			return false;
		}
	}
}