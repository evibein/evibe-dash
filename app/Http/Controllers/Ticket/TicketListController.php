<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;
use App\Models\TicketFollowup;
use App\Models\TypeEvent;

use App\Models\Types\TypeEnquirySource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TicketListController extends BaseTicketController
{
	public function showNewTicketsList(Request $request)
	{
		$showFilterReset = false;

		// Search results
		if ($request->has('query') && $searchQuery = $request->input('query'))
		{
			$showFilterReset = true;
		}

		// Filter results - by booking
		if ($request->has('status'))
		{
			$filterStatusId = -1;

			switch ($request->input('status'))
			{
				case 'initiated':
					$filterStatusId = 1;
					break;
				case 'in-progress':
					$filterStatusId = 2;
					break;
				case 'confirmed':
					$filterStatusId = 3;
					break;
				case 'booked':
					$filterStatusId = 4;
					break;
				case 'cancelled':
					$filterStatusId = 5;
					break;
				case 'just-enquiry':
					$filterStatusId = 6;
					break;
				case 'followup':
					$filterStatusId = 7;
					break;
				case 'auto-pay':
					$filterStatusId = 8;
					break;
				case 'auto-cancel':
					$filterStatusId = 9;
					break;
				case 'service-auto-pay':
					$filterStatusId = 12;
					break;
			}

			if ($filterStatusId != -1)
			{
				$showFilterReset = true;
			}
		}

		// Filter by party date start & end
		if ($request->input('pds') && $filterPDStart = strtotime($request->input('pds')))
		{
			$showFilterReset = true;
		}

		if ($request->input('pde') && $filterPDEnd = strtotime($request->input('pde')))
		{
			$showFilterReset = true;
		}

		$data = [
			'filters'    => [
				'reset' => $showFilterReset,
			],
			'typeStatus' => \TypeTicketStatus::all()
		];

		return view('tickets/new-list', ['data' => $data]);
	}

	public function showTicketsList(Request $request)
	{
		$showFilterReset = false;
		$isFilter = false;
		$defaultPerPage = 30;
		$tickets = \Ticket::select('ticket.id', 'name', 'phone', 'city_id', 'status_id', 'event_id', 'enquiry_id', 'lead_status_id', 'booking_likeliness_id', 'event_date', 'enquiry_source_id');
		$leftJoin = true;
		$needBookings = false;
		$validRoleIds = [
			config('evibe.roles.super_admin'),
			config('evibe.roles.admin'),
			config('evibe.roles.customer_delight'),
			config('evibe.roles.operations'),
			config('evibe.roles.sr_crm'),
			config('evibe.roles.sr_bd'),
			config('evibe.roles.syrow'),
			config('evibe.roles.ops_agent'),
			config('evibe.roles.ops_tl'),
			config('evibe.roles.bd_agent'),
			config('evibe.roles.bd_tl')
		];
		$ticketStatus = \TypeTicketStatus::select("id", "name")->get();
		$handlerIds = User::select("id", "name")->whereIn("role_id", $validRoleIds)->whereNull('deleted_at')->get()->toArray();
		$leadStatus = \TypeLeadStatus::select("id", "name", "color")->get();
		$enquirySources = TypeEnquirySource::select("id", "name")->whereNull("deleted_at")->get();
		$customerSources = \TypeTicketSource::select("id", "name")->whereNull("deleted_at")->get();
		$bookingLikelyOptions = config('evibe.bookingLikeliness');
		$typeEvent = TypeEvent::select("id", "name")->whereNull("deleted_at")->get();

		// Search results
		if ($request->has('query') && $searchQuery = $request->input('query'))
		{
			if ($searchQuery == "7908954573")
			{
				return view("errors.404");
			}

			$isFilter = true;
			$padQuery = '%' . $searchQuery . '%';

			/**
			 * @see select is required as certain fields are ambiguous and are chosen randomly
			 * (ex: ticket.area_id, ticket_bookings.area_id)
			 */
			$tickets->leftJoin('ticket_bookings', 'ticket_bookings.ticket_id', '=', 'ticket.id')
			        ->where(function ($query) use ($padQuery, $searchQuery) {
				        $query->where('ticket.name', 'LIKE', $padQuery)
				              ->orWhere('ticket.email', 'LIKE', $padQuery)
				              ->orWhere('ticket.phone', 'LIKE', $padQuery)
				              ->orWhere('ticket.enquiry_id', 'LIKE', $padQuery)
				              ->orWhere('ticket_bookings.booking_id', 'LIKE', $padQuery);
			        });
			$leftJoin = true;
		}

		// Search by handler
		if ($request->has('handler') && $request->input('handler') > 0)
		{
			$isFilter = true;
			$showFilterReset = true;

			$tickets->where('ticket.handler_id', $request->input('handler'));
		}

		// Search by enq source
		if ($request->has('enqSource') && $request->input('enqSource') > 0)
		{
			$isFilter = true;
			$showFilterReset = true;

			$tickets->where('ticket.enquiry_source_id', $request->input('enqSource'));
		}

		// Search by customer source
		if ($request->has('customerSource') && $request->input('customerSource') > 0)
		{
			$isFilter = true;
			$showFilterReset = true;

			$tickets->where('ticket.source_id', $request->input('customerSource'));
		}

		// Search by booking likeliness
		if ($request->has('bookingLikelyNes') && $request->input('bookingLikelyNes') > 0)
		{
			$isFilter = true;
			$showFilterReset = true;

			$tickets->where('ticket.booking_likeliness_id', $request->input('bookingLikelyNes'));
		}

		// Filter results - by booking
		if ($request->has('status'))
		{
			$isFilter = true;
			$filterStatusId = -1;

			//dd($request->input('status'));
			switch ($request->input('status'))
			{
				case 'initiated':
					$filterStatusId = 1;
					break;
				case 'in-progress':
					$filterStatusId = 2;
					break;
				case 'confirmed':
					$filterStatusId = 3;
					break;
				case 'booked':
					$filterStatusId = 4;
					break;
				case 'cancelled':
					$filterStatusId = 5;
					break;
				case 'just-enquiry':
					$filterStatusId = 6;
					break;
				case 'followup':
					$filterStatusId = 7;
					break;
				case 'auto-pay':
					$filterStatusId = 8;
					break;
				case 'auto-cancel':
					$filterStatusId = 9;
					break;
				case 'service-auto-pay':
					$filterStatusId = 12;
					break;
				case 'not-interested':
					$filterStatusId = 13;
					break;
				case 'duplicate':
					$filterStatusId = 14;
					break;
				case 'no-response':
					$filterStatusId = 17;
					break;
				case 'invalid-email':
					$filterStatusId = 18;
					break;
				case 'drop-off':
					$filterStatusId = 19;
					break;
			}

			if ($filterStatusId != -1)
			{
				$showFilterReset = true;
				$tickets->where('ticket.status_id', $filterStatusId);
			}
		}

		if ($request->has('occasion'))
		{
			$isFilter = true;
			$eventId = $request->input('occasion');
			if ($eventId > 0)
			{
				$showFilterReset = true;
				$tickets->where('event_id', $eventId);
			}
		}

		if ($request->has('leadStatus'))
		{
			$isFilter = true;
			$leadStatusId = -1;

			switch ($request->input('leadStatus'))
			{
				case 'hot':
					$leadStatusId = 1;
					break;
				case 'medium':
					$leadStatusId = 2;
					break;
				case 'cold':
					$leadStatusId = 3;
					break;
			}

			if ($leadStatusId != -1)
			{
				$showFilterReset = true;
				$tickets->where('lead_status_id', $leadStatusId);
			}
		}

		//By querying from data base
		if (($request->input('rds')) && ($request->input('rde')))
		{
			$rds = date("Y-m-d H:i:s", strtotime($request->input('rds')));
			$rde = date("Y-m-d H:i:s", strtotime($request->input('rde')) + 23 * 59 * 59);

			$recommendationFilterTicketIds = \TicketMapping::select("recommended_at", "ticket_id")
			                                               ->whereNotNull('recommended_at')
			                                               ->where('recommended_at', '>=', $rds)
			                                               ->where('recommended_at', '<=', $rde)
			                                               ->pluck('ticket_id')
			                                               ->unique()
			                                               ->toArray();

			$tickets->whereIn('ticket.id', $recommendationFilterTicketIds);
		}

		// Filter by party date start & end
		if ($request->input('pds') && $filterPDStart = strtotime($request->input('pds')))
		{
			$isFilter = true;
			$showFilterReset = true;
			$tickets->where('ticket.event_date', '>=', $filterPDStart);
		}

		if ($request->input('pde') && $filterPDEnd = strtotime($request->input('pde')))
		{
			$isFilter = true;
			$showFilterReset = true;
			$filterPDEnd += 24 * 60 * 60 - 1; // till day end
			$tickets->where('ticket.event_date', '<=', $filterPDEnd);
		}

		// Filter by updated date start and end
		if ($request->input('uds') && $filterUDStart = strtotime($request->input('uds')))
		{
			$isFilter = true;
			$showFilterReset = true;
			$tickets->where('ticket.updated_at', '>=', date('Y-m-d H:i:s', $filterUDStart));
		}

		if ($request->input('ude') && $filterUDEnd = strtotime($request->input('ude')))
		{
			$isFilter = true;
			$showFilterReset = true;
			$filterUDEnd += 24 * 60 * 60 - 1; // till day end
			$tickets->where('ticket.updated_at', '<=', date('Y-m-d H:i:s', $filterUDEnd));
		}

		// Filter by created date start & end
		if ($request->input('cds') && $filterCDStart = strtotime($request->input('cds')))
		{
			$isFilter = true;
			$showFilterReset = true;
			$tickets->where('ticket.created_at', '>=', date('Y-m-d H:i:s', $filterCDStart));
		}

		if ($request->input('cde') && $filterCDEnd = strtotime($request->input('cde')))
		{
			$isFilter = true;
			$showFilterReset = true;
			$filterCDEnd += 60 - 1; // till minute end
			$tickets->where('ticket.created_at', '<=', date('Y-m-d H:i:s', $filterCDEnd));
		}

		// Filter by payment done date start & end
		if ($request->input('pdds') && $filterPDDStart = strtotime($request->input('pdds')))
		{
			$isFilter = true;
			$showFilterReset = true;
			$tickets->where('ticket.paid_at', '>=', $filterPDDStart);
		}

		if ($request->input('pdde') && $filterPDDEnd = strtotime($request->input('pdde')))
		{
			$isFilter = true;
			$showFilterReset = true;
			$filterPDDEnd += 60 - 1; // till minute end
			$tickets->where('ticket.created_at', '<=', date('Y-m-d H:i:s', $filterPDDEnd));
		}

		// Sort results
		$sortType = 'DESC';
		$sortOrder = 'ticket.updated_at';

		if ($request->has('sort'))
		{
			switch ($request->input('sort'))
			{
				case 'updated-first':
					$sortType = 'DESC';
					$sortOrder = 'ticket.updated_at';
					break;
				case 'updated-last':
					$sortType = 'ASC';
					$sortOrder = 'ticket.updated_at';
					break;
				case 'date-desc':
					$sortType = 'DESC';
					$sortOrder = 'ticket.event_date';
					break;
				case 'date-asc':
					$sortType = 'ASC';
					$sortOrder = 'ticket.event_date';
					break;
				case 'created-last':
					$sortType = 'ASC';
					$sortOrder = 'ticket.created_at';
					break;
				case 'created-first':
					$sortType = 'DESC';
					$sortOrder = 'ticket.created_at';
					break;
				case 'booking-asc':
					$needBookings = true;
					$sortType = 'ASC';
					$sortOrder = 'ticket_bookings.booking_amount';
					break;
				case 'booking-desc':
					$needBookings = true;
					$sortType = 'DESC';
					$sortOrder = 'ticket_bookings.booking_amount';
					break;
			}
		}

		$defaultPerPage = $request->has('count') ? (int)$request->input('count') : $defaultPerPage;
		if (!$leftJoin && $needBookings)
		{
			$tickets->leftJoin('ticket_bookings', 'ticket_bookings.ticket_id', '=', 'ticket.id');
		}

		$notRequiredStatus = [config("evibe.status.booked"), config("evibe.status.cancelled"), config("evibe.status.enquiry")];
		if (!$isFilter)
		{
			$tickets = $tickets->whereNotIn('status_id', $notRequiredStatus);
		}

		$tickets = $tickets->orderBy($sortOrder, $sortType)
		                   ->groupBy('ticket.id')// fix for multiples
		                   ->paginate($defaultPerPage);

		$ticketBookings = null;
		if ($tickets->count() > 0)
		{
			$ticketBookings = \TicketBooking::select('party_date_time', 'ticket_id')
			                                ->whereIn('ticket_id', $tickets->pluck('id')->toArray())
			                                ->get();
		}

		$activeFollowups = TicketFollowup::whereIn("ticket_id", $tickets->pluck("id"))->whereNull('is_complete')->get();

		$data = [
			'tickets'                 => $tickets,
			'ticketBookings'          => $ticketBookings,
			'filters'                 => [
				'reset' => $showFilterReset
			],
			'ticketStatus'            => $ticketStatus,
			'leadStatus'              => $leadStatus,
			'handlerIds'              => $handlerIds,
			'activeFollowUps'         => $activeFollowups,
			'enquirySources'          => $enquirySources,
			'customerSources'         => $customerSources,
			'bookingLikelyNesOptions' => $bookingLikelyOptions,
			'events'                  => $typeEvent
		];

		return view('tickets/list', ['data' => $data]);
	}

	//showing ticket details
	public function showTicketDetails($ticketId)
	{
		return redirect()->route('ticket.details.bookings', $ticketId);
	}

	/**
	 * @since 20 Jan 2015
	 */
	public function showCreateTicket()
	{
		$data = [
			'source'     => \TypeTicketSource::all(),
			'leadStatus' => \TypeLeadStatus::all(),
			'city'       => \City::orderBy('is_default', 'DESC')->get(),
			'area'       => \Area::orderBy('city_id', 'DESC')->orderBy('name')->get(),
			'vendors'    => \Vendor::all(),
			'occasion'   => TypeEvent::all()
		];

		return view('tickets/create', ['data' => $data]);
	}

	// create new ticket
	public function saveTicket()
	{
		// validate rules
		$rules = [
			'customerName' => 'required',
			'phone'        => 'required_without:email|digits:10',
			'email'        => 'required_without:phone|email',
			'eventDate'    => 'required|date',
			'altEmail'     => 'email',
			'altPhone'     => 'digits:10',
			'city'         => 'required|exists:city,id',
			'area'         => 'required|exists:area,id',
			'budget'       => 'numeric',
			'occasion'     => 'required|exists:type_event,id',
			'landmark'     => 'max:100',
			'zip'          => 'integer|digits:6',
			'source'       => 'required|integer|min:1',
			'leadStatus'   => 'required|integer|min:1'
		];

		$messages = [
			'customerName.required'  => 'Please enter customer name',
			'phone.required'         => 'Please enter customer phone number (10 digits)',
			'phone.required_without' => 'Either email / phone number is required',
			'email.required'         => 'Please enter the email address',
			'email.required_without' => 'Either email / phone number is required',
			'email.email'            => 'Please enter a valid email address',
			'eventDate.required'     => 'Party date is required',
			'eventDate.date'         => 'Please select a valid party date',
			'phone.phone'            => 'Phone number is not valid',
			'budget.required'        => 'Please enter the event budget',
			'budget.number'          => 'Please enter a valid event budget',
			'altEmail.email'         => 'Alternate email is invalid',
			'altPhone.phone'         => 'Alternate phone number is invalid',
			'occasion.required'      => 'Please select a valid occasion type',
			'occasion.exists'        => 'Please select a valid occasion type',
			'area.required'          => 'Please select valid location',
			'area.exists'            => 'Please select valid location',
			'landmark.max'           => 'Landmark cannot be more than 100 characters',
			'zip.integer'            => 'Please enter a valid zip code.',
			'zip.digits'             => 'Zip code should be 6 digits.',
			'source.required'        => 'Please select a ticket source',
			'source.min'             => 'Please select a ticket source',
			'leadStatus.required'    => 'please select the lead status',
			'leadStatus.min'         => 'please select the lead status'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect()->route('ticket.new')
			                 ->withErrors($validator)
			                 ->withInput();
		}
		else
		{
			$statusId = config('evibe.status.initiated'); // initialize

			$newTicket = \Ticket::create([
				                             'name'              => Input::get('customerName'),
				                             'email'             => Input::get('email'),
				                             'phone'             => Input::get('phone'),
				                             'event_id'          => Input::get('occasion'),
				                             'comments'          => Input::get('comments'),
				                             'event_date'        => strtotime(Input::get('eventDate')),
				                             'budget'            => Input::get('budget'),
				                             'area_id'           => Input::get('area'),
				                             'status_id'         => $statusId,
				                             'source_id'         => Input::get('source'),
				                             'source_specific'   => Input::get('sourceSpecific'),
				                             'lead_status_id'    => Input::get('leadStatus'),
				                             'handler_id'        => Auth::user()->id,
				                             'city_id'           => Input::get('city'),
				                             'venue_landmark'    => Input::get('landmark'),
				                             'zip_code'          => Input::get('zip'),
				                             'alt_phone'         => Input::get('altPhone'),
				                             'alt_email'         => Input::get('altEmail'),
				                             'enquiry_source_id' => config("evibe.ticket_enquiry_source.direct"),
				                             'created_at'        => date('Y-m-d H:i:s'),
				                             'updated_at'        => date('Y-m-d H:i:s')
			                             ]);

			// update enquiry id
			$newTicket->enquiry_id = 'ENQ-BLR-DI-' . $newTicket->id;
			$newTicket->save();

			$this->updateTicketAction([
				                          'comments'   => "New ticket created",
				                          'typeUpdate' => config('evibe.ticket_type_update.manual'),
				                          'ticket'     => $newTicket
			                          ]);

			return redirect()->route('ticket.list');
		}
	}

	/**
	 * Edit ticket details
	 *
	 * @since 9 Feb 2015
	 */
	public function editTicket($ticketId)
	{
		// validate rules
		$rules = [
			'name'       => 'required',
			'phone'      => 'required_without:email|digits:10',
			'email'      => 'required_without:phone|email',
			'altPhone'   => 'phone',
			'altEmail'   => 'email',
			'eventDate'  => 'required|date',
			'cityId'     => 'required',
			'areaId'     => 'required|integer|min:1',
			'budget'     => 'numeric',
			'zipCode'    => 'integer|digits:6',
			'occasion'   => 'required|min:1',
			'sourceId'   => 'required|integer|min:1',
			'leadStatus' => 'required',
		];

		$messages = [
			'name.required'          => 'Customer name is required',
			'phone.required'         => 'Customer phone number (10 digits) is required',
			'phone.required_without' => 'Either email / phone number is required',
			'email.required'         => 'Please enter the email address',
			'email.required_without' => 'Either email / phone number is required',
			'email.email'            => 'Please enter a valid email address',
			'eventDate.required'     => 'Please select a valid party date',
			'eventDate.date'         => 'Please select a valid party date',
			'phone.phone'            => 'Phone number is not valid',
			'altPhone.phone'         => 'Alt. phone number is invalid',
			'altEmail.email'         => 'Alt. email is invalid',
			'budget.required'        => 'Please enter the event budget',
			'budget.number'          => 'Please enter a valid event budget',
			'occasion.required'      => 'Please select an occasion type',
			'occasion.min'           => 'Please select a valid occasion',
			'zipCode.integer'        => 'Please enter a valid pin code',
			'zipCode.digits'         => 'Please enter a valid 6 digit pin code',
			'cityId.required'        => 'Please select a city',
			'areaId.required'        => 'Please select a location',
			'areaId.integer'         => 'Please select a valid location',
			'areaId.min'             => 'Please select a valid location',
			'sourceId.required'      => 'Please select a ticket source',
			'sourceId.min'           => 'Please select a ticket source',
			'leadStatus.required'    => 'Please select a lead status',
		];

		$validation = Validator::make(Input::all(), $rules, $messages);
		$res = ['success' => true];

		if ($validation->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}
		else
		{
			$ticket = \Ticket::findOrFail($ticketId);
			$oldAreaId = $ticket->area_id;
			$oldPartyDateTime = $ticket->event_date;
			$newAreaId = Input::get('areaId');
			$newPartyDateTime = strtotime(Input::get('eventDate'));

			$ticket->update([
				                'name'              => Input::get('name'),
				                'phone'             => Input::get('phone'),
				                'email'             => Input::get('email'),
				                'alt_phone'         => Input::get('altPhone'),
				                'alt_email'         => Input::get('altEmail'),
				                'budget'            => Input::get('budget'),
				                'event_date'        => $newPartyDateTime,
				                'comments'          => Input::get('comments'),
				                'city_id'           => Input::get('cityId'),
				                'area_id'           => $newAreaId,
				                'event_id'          => Input::get('occasion'),
				                'food_type'         => Input::get('food') ? Input::get('food') : null,
				                'guests_count'      => Input::get('guests') ? Input::get('guests') : null,
				                'updated_at'        => date('Y-m-d H:i:s'),
				                'handler_id'        => Auth::user()->id,
				                'zip_code'          => Input::get('zipCode'),
				                'venue_landmark'    => Input::get('landmark'),
				                'source_id'         => Input::get('sourceId'),
				                'source_specific'   => Input::get('sourceSpecific'),
				                'lead_status_id'    => Input::get('leadStatus'),
				                'food_service_type' => Input::get('foodServiceType'),
				                'budget_per_person' => Input::get('budgetPerPerson'),
				                'venue_min_guests'  => Input::get('foodGuestCount')
			                ]);

			// update all ticket bookings event date & time, area (cannot be updated for confirmed, booked, auto pay tickets)
			if ($ticket->bookings)
			{
				$ticket->bookings()->update([
					                            'area_id'         => $newAreaId,
					                            'party_date_time' => $newPartyDateTime,
					                            'updated_at'      => date('Y-m-d H:i:s')
				                            ]);
			}

			// if change in area / party_date_time & status confirmed / booked,
			// then update status to in-progress & nullify previous success emails sent
			$isDataChanged = ($oldAreaId != $newAreaId || $oldPartyDateTime != $newPartyDateTime);
			$isTicketProcessed = ($ticket->status_id == config('evibe.status.confirmed') || $ticket->status_id == config('evibe.status.booked'));
			if ($isDataChanged && $isTicketProcessed)
			{
				$ticket->update([
					                'success_email_sent_at' => null,
					                'success_email_type'    => null
				                ]);
			}

			// ticket action updated
			$ticketUpdate = [
				'ticket'   => $ticket,
				'comments' => config('evibe.ticket_status_message.edit_ticket')
			];
			$this->updateTicketAction($ticketUpdate);
		}

		return response()->json($res);
	}

	/**
	 * Cancel a ticket
	 *
	 * @since  6 Apr 2015
	 */
	public function cancelTicket($ticketId)
	{
		$ticket = \Ticket::findOrFail($ticketId);

		$user = Auth::user();
		//validation to check weather the created handler is set or not

		$createdHandlerId = $ticket->created_handler_id;
		if (!$createdHandlerId)
		{
			$ticket->created_handler_id = $user->id;
			$ticket->save();
		}
		$res = ['success' => false, 'error' => 'Could not find ticket'];

		if ($ticket)
		{
			// set status = cancel
			if ($ticket->status_id != config('evibe.status.booked') && $ticket->status_id != config('evibe.status.confirmed'))
			{
				$ticket->status_id = config('evibe.status.cancelled');
			}
			$ticket->updated_at = date('Y-m-d H:i:s');
			$ticket->save();

			// delete mappings, bookings
			\TicketMapping::where('ticket_id', $ticketId)->delete();
			\TicketBooking::where('ticket_id', $ticketId)->delete();

			$res['success'] = true;
		}

		// update ticket action
		$ticketUpdate = [
			'ticket'   => $ticket,
			'comments' => config('evibe.ticket_status_message.cancelled'),
			'statusId' => config('evibe.status.cancelled')
		];
		$this->updateTicketAction($ticketUpdate);

		return response()->json($res);
	}

	public function reOpenTicket($ticketId)
	{
		$ticket = \Ticket::findOrFail($ticketId);
		$res = ['success' => false, 'error' => 'Could not find ticket'];

		if ($ticket)
		{
			// set status based on mappings / bookings
			$ticket->updated_at = date('Y-m-d H:i:s');
			$ticket->save();

			if (in_array($ticket->status_id, [config("evibe.status.cancelled"), config("evibe.status.auto_cancel")]))
			{
				$ticketBookings = $ticket->bookings;

				if ($ticketBookings->count() > 0)
				{
					foreach ($ticketBookings as $ticketBooking)
					{
						$ticketBooking->update([
							                       "cancelled_at"         => null,
							                       "handler_cancelled_at" => null
						                       ]);
					}
				}
			}

			// update ticket action
			$ticketUpdate = [
				'statusId' => config('evibe.status.initiated'),
				'ticket'   => $ticket,
				'comments' => config('evibe.ticket_status_message.reopen')
			];
			$this->updateTicketAction($ticketUpdate);

			$res['success'] = true;
		}

		return response()->json($res);
	}

	// Delete a ticket
	public function deleteTicket($ticketId)
	{
		$ticket = \Ticket::find($ticketId);

		$user = Auth::user();

		//validation to check weather the created handler is set or not
		$createdHandlerId = $ticket->created_handler_id;
		if ($createdHandlerId)
		{
			$ticket->created_handler_id = $user->id;
			$ticket->save();
		}

		if ($ticket && $ticket->delete())
		{
			// delete ticket mapping, bookings
			\TicketMapping::where('ticket_id', $ticketId)->delete();
			\TicketBooking::where('ticket_id', $ticketId)->delete();

			$res = response()->json(['success' => true]);
		}
		else
		{
			$res = response()->json([
				                        'success' => false,
				                        'error'   => ['Could not delete the ticket, please try again later.']
			                        ]);
		}

		return $res;
	}

	public function loadExtraData(Request $request)
	{
		$data = [];
		$lastStatus = null;
		$ticketIds = $request->input('ticketIds');

		$tickets = \Ticket::select("id", "name")
		                  ->whereIn('id', $ticketIds)
		                  ->get();

		$ticketStatus = \TicketUpdate::select("id", "status_at", "ticket_id", "status_id", "handler_id", "type_update", "comments")
		                             ->whereIn("ticket_id", $ticketIds)
		                             ->get()
		                             ->keyBy("ticket_id")
		                             ->toArray();

		$handlers = User::whereIn("role_id", [
			config("evibe.roles.admin"), config("evibe.roles.super_admin"), config("evibe.roles.sr_bd"),
			config("evibe.roles.sr_crm"), config("evibe.roles.customer_delight"), config("evibe.roles.bd"), config("evibe.roles.tech")])
		                ->get()
		                ->pluck("name", "id");

		$typeTicketStatus = \TypeTicketStatus::all()
		                                     ->pluck("name", "id");

		foreach ($tickets as $ticket)
		{
			$data[$ticket->id] = [
				'status' => 0,
			];

			if (isset($ticketStatus[$ticket->id]))
			{
				$lastStatus = $ticketStatus[$ticket->id];

				$data[$ticket->id] = [
					'status'     => 1,
					'statusAt'   => $this->bookingFormatDate($lastStatus["status_at"]),
					'statusName' => isset($typeTicketStatus[$lastStatus["status_id"]]) ? $typeTicketStatus[$lastStatus["status_id"]] : "--",
					'handler'    => isset($handlers[$lastStatus["handler_id"]]) ? $handlers[$lastStatus["handler_id"]] : "Auto",
					'source'     => $lastStatus["type_update"],
					'comments'   => $lastStatus["comments"],
					'name'       => $ticket->name,
					'url'        => route('ticket.update.load', $ticket->id)
				];
			}
		}

		return response()->json(['success' => true, 'extraData' => $data]);
	}

	public function saveCopyTicket($ticketId)
	{
		$ticket = \Ticket::find($ticketId);
		if (!$ticketId)
		{
			return redirect()->back();
		}

		$copyTicket = $ticket->replicate();
		$copyTicket->is_auto_booked = 0;
		$copyTicket->status_id = config("evibe.status.initiated");
		$copyTicket->feedback_sms_sent_at = null;
		$copyTicket->feedback_email_sent_at = null;
		$copyTicket->feedback_received_at = null;
		$copyTicket->feedback_url = '';
		$copyTicket->success_email_type = null;
		$copyTicket->success_email_sent_at = null;
		$copyTicket->payment_deadline = null;
		$copyTicket->payment_reminder_sent_at = null;
		$copyTicket->auto_book_deadline_date = null;
		$copyTicket->paid_at = null;
		$copyTicket->updated_receipt_sent_at = null;
		$copyTicket->is_copied = 1;
		$copyTicket->enquiry_source_id = config('evibe.ticket_enquiry_source.direct_copy');
		$copyTicket->save();

		$copyTicket->enquiry_id = 'ENQ-BLR-DI-' . $copyTicket->id;
		$copyTicket->save();

		$this->updateTicketAction([
			                          'comments'   => "Copied ticket",
			                          'typeUpdate' => config('evibe.ticket_type_update.manual'),
			                          'ticket'     => $copyTicket
		                          ]);

		return redirect(route("ticket.copy", $copyTicket->id));
	}

	public function CopyTicket($ticketId)
	{
		$ticketDetailsController = new TicketDetailsController();

		return $ticketDetailsController->showTicketInformation($ticketId, 1);
	}

	private function bookingFormatDate($timestamp)
	{
		return date("d M Y, h:i A \(D\)", $timestamp);
	}

	public function LoadTicketListAjaxData($statusId, Request $request)
	{
		$showFilterReset = false;
		$defaultPerPage = 50;
		if ($statusId == config("evibe.lead_status.hot"))
		{
			$defaultPerPage = 100;
		}

		$ticketStatus = \TypeTicketStatus::select("id", "name")->get();
		$handlerIds = User::select("id", "name")->get();

		$tickets = \Ticket::with('activeFollowup')
		                  ->select('ticket.*');

		if (!$statusId)
		{
			$tickets = $tickets->whereNull('lead_status_id');
		}
		else
		{
			$tickets = $tickets->where('lead_status_id', $statusId);
		}

		// Search results
		if ($request->has('query') && $searchQuery = $request->input('query'))
		{
			$showFilterReset = true;
			$padQuery = '%' . $searchQuery . '%';

			/**
			 * @see select is required as certain fields are ambiguous and are chosen randomly
			 * (ex: ticket.area_id, ticket_bookings.area_id)
			 */
			$tickets->leftJoin('ticket_bookings', 'ticket_bookings.ticket_id', '=', 'ticket.id')
			        ->where(function ($query) use ($padQuery, $searchQuery) {
				        $query->where('ticket.name', 'LIKE', $padQuery)
				              ->orWhere('ticket.email', 'LIKE', $padQuery)
				              ->orWhere('ticket.phone', 'LIKE', $padQuery)
				              ->orWhere('ticket.enquiry_id', 'LIKE', $padQuery)
				              ->orWhere('ticket_bookings.booking_id', 'LIKE', $padQuery);
			        });
		}

		// Filter results - by booking
		if ($request->has('status'))
		{
			$filterStatusId = -1;

			switch ($request->input('status'))
			{
				case 'initiated':
					$filterStatusId = 1;
					break;
				case 'in-progress':
					$filterStatusId = 2;
					break;
				case 'confirmed':
					$filterStatusId = 3;
					break;
				case 'booked':
					$filterStatusId = 4;
					break;
				case 'cancelled':
					$filterStatusId = 5;
					break;
				case 'just-enquiry':
					$filterStatusId = 6;
					break;
				case 'followup':
					$filterStatusId = 7;
					break;
				case 'auto-pay':
					$filterStatusId = 8;
					break;
				case 'auto-cancel':
					$filterStatusId = 9;
					break;
				case 'service-auto-pay':
					$filterStatusId = 12;
					break;
			}

			if ($filterStatusId != -1)
			{
				$showFilterReset = true;
				$tickets->where('ticket.status_id', $filterStatusId);
			}
		}

		// Filter by party date start & end
		if ($request->input('pds') && $filterPDStart = strtotime($request->input('pds')))
		{
			$showFilterReset = true;
			$tickets->where('ticket.event_date', '>=', $filterPDStart);
		}

		if ($request->input('pde') && $filterPDEnd = strtotime($request->input('pde')))
		{
			$showFilterReset = true;
			$filterPDEnd += 24 * 60 * 60 - 1; // till day end
			$tickets->where('ticket.event_date', '<=', $filterPDEnd);
		}

		if (!$showFilterReset)
		{
			$tickets = $tickets->whereNotIn('status_id', [
				config("evibe.status.booked"),
				config("evibe.status.cancelled")
			]);
		}

		$handlerId = Auth::user()->id;

		$tickets = $tickets->groupBy('ticket.id')// fix for multiples
		                   ->take($defaultPerPage)
		                   ->orderBy("ticket.updated_at", "DESC")
		                   ->get();

		$areas = \Area::select("id", "name")
		              ->whereIn('id', $tickets->pluck("area_id"))
		              ->get();

		$now = Carbon::now()->timestamp;
		$todayStart = Carbon::today()->startOfDay()->timestamp;
		$todayEnd = Carbon::today()->endOfDay()->timestamp;
		$tomorrowStart = Carbon::tomorrow()->startOfDay()->timestamp;
		$tomorrowEnd = Carbon::tomorrow()->endOfDay()->timestamp;

		$p1Status = [config("evibe.status.followup")];

		$handlerTodayFollowups = $tickets->filter(function ($item) use ($handlerId, $p1Status, $now, $todayEnd) {
			$followup = $item->activeFollowup ? $item->activeFollowup->followup_date : false;

			return $item->handler_id == $handlerId &&
				in_array($item->status_id, $p1Status) &&
				($followup && $followup > $now && $followup < $todayEnd);
		});

		$handlerLapsedFollowups = $tickets->filter(function ($item) use ($handlerId, $p1Status, $now) {
			$followup = $item->activeFollowup ? $item->activeFollowup->followup_date : false;

			return $item->handler_id == $handlerId &&
				in_array($item->status_id, $p1Status) &&
				($followup && $followup < $now);
		});

		$othersTodayFollowups = $tickets->filter(function ($item) use ($handlerId, $p1Status, $now, $todayEnd) {
			$followup = $item->activeFollowup ? $item->activeFollowup->followup_date : false;

			return $item->handler_id != $handlerId &&
				in_array($item->status_id, $p1Status) &&
				($followup && $followup > $now && $followup < $todayEnd);
		});

		$othersLapsedFollowups = $tickets->filter(function ($item) use ($handlerId, $p1Status, $now) {
			$followup = $item->activeFollowup ? $item->activeFollowup->followup_date : false;

			return $item->handler_id != $handlerId &&
				in_array($item->status_id, $p1Status) &&
				($followup && $followup < $now);
		});

		$allTomorrowFollowups = $tickets->filter(function ($item) use ($handlerId, $p1Status, $tomorrowStart, $tomorrowEnd) {
			$followup = $item->activeFollowup ? $item->activeFollowup->followup_date : false;

			return in_array($item->status_id, $p1Status) &&
				($followup && $followup > $tomorrowStart && $followup < $tomorrowEnd);
		});

		$allRemainingFollowUps = $tickets->filter(function ($item) use ($p1Status, $tomorrowEnd) {
			$followup = $item->activeFollowup ? $item->activeFollowup->followup_date : false;

			return in_array($item->status_id, $p1Status) &&
				($followup && $followup > $tomorrowEnd);
		});

		$allUnMappedFollowUps = $tickets->filter(function ($item) use ($p1Status, $tomorrowEnd) {
			$followup = $item->activeFollowup ? $item->activeFollowup->followup_date : false;

			return in_array($item->status_id, $p1Status) && !$followup;
		});

		$allRemainingTickets = $tickets->filter(function ($item) use ($p1Status) {
			return !in_array($item->status_id, $p1Status);
		});

		$results = collect();
		$results = $results->merge($handlerTodayFollowups->sortByDesc("event_date"));
		$results = $results->merge($handlerLapsedFollowups->sortByDesc("event_date"));
		$results = $results->merge($othersTodayFollowups->sortByDesc("event_date"));
		$results = $results->merge($othersLapsedFollowups->sortByDesc("event_date"));
		$results = $results->merge($allTomorrowFollowups->sortByDesc("event_date"));
		$results = $results->merge($allRemainingFollowUps->sortByDesc("event_date"));
		$results = $results->merge($allUnMappedFollowUps->sortByDesc("event_date"));
		$results = $results->merge($allRemainingTickets->sortByDesc("event_date"));

		$data = [
			'tickets'      => $results->toArray(),
			'filters'      => [
				'reset' => $showFilterReset
			],
			'ticketStatus' => $ticketStatus,
			'handlerIds'   => $handlerIds,
			'statusId'     => $statusId,
			'areas'        => $areas
		];

		return view('tickets/list/list-data', ['data' => $data]);
	}
}
