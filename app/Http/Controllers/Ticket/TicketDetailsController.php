<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Base\BaseTicketController;
use App\Models\AvailCheck\AvailabilityCheckGallery;
use App\Models\AddOn\AddOn;
use App\Models\Email\EmailTrack;
use App\Models\Ticket\LogTicketReminders;
use App\Models\Ticket\TicketInvite;
use App\Models\Ticket\TicketServicesTags;
use App\Models\Ticket\TicketSiteVisit;
use App\Models\Ticket\TicketThankYouCard;
use App\Models\TicketFollowup;
use App\Models\Ticket\TicketFollowupsType;
use App\Models\TypeEvent;
use App\Models\Types\TypeBookingConcept;
use App\Models\Types\TypeCancellationReason;
use App\Models\Types\TypeCategoryTags;
use App\Models\Types\TypeTicketBooking;
use App\Models\TypeServices;
use App\Models\Util\CheckoutField;
use App\Models\Util\StarOption;
use App\Models\Util\TicketReviewQuestion;
use Carbon\Carbon;
use Evibe\Facades\AppUtilFacade as AppUtil;
use GuzzleHttp\Psr7\LazyOpenStream;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class TicketDetailsController extends BaseTicketController
{
	public function showTicketBenefitServiceDetails($ticketId)
	{
		$ticket = \Ticket::with('bookings')->find($ticketId);

		$eInvite = TicketInvite::where('ticket_id', $ticketId)
		                       ->whereNull('deleted_at')
		                       ->orderBy('invite_sent_at', 'DESC')
		                       ->orderBy('updated_at', 'DESC')
		                       ->first();

		$eTYC = TicketThankYouCard::where('ticket_id', $ticketId)
		                          ->whereNull('deleted_at')
		                          ->orderBy('updated_at')
		                          ->first();

		$falloutFeedback = $this->getFalloutFeedback($ticketId, $ticket->status_id);

		$data = [
			'ticket'          => $ticket,
			'eInvite'         => $eInvite,
			'eTYC'            => $eTYC,
			'inviteUrlPath'   => config('evibe.gallery.host') . '/ticket/' . $ticketId . '/invites/',
			'TYCUrlPath'      => config('evibe.gallery.host') . '/ticket/' . $ticketId . '/thank-you-cards/',
			'falloutFeedback' => $falloutFeedback
		];

		return view('tickets.details.benefit-service.info')->with($data);
	}

	public function showTicketFinanceDetails($ticketId)
	{
		$ticket = \Ticket::with('bookings')->find($ticketId);

		// check and redirecting to other details page based on the condition fo the checkForRedirect()
		if (!$ticket->isTicketBookingFinanceVisible())
		{
			return redirect($this->getRedirectRoute($ticket));
		}

		$eventId = $ticket->event_id ? $ticket->event_id : 1;

		$falloutFeedback = $this->getFalloutFeedback($ticketId, $ticket->status_id);

		$data = [
			'ticket'          => $ticket,
			'falloutFeedback' => $falloutFeedback
		];

		return view('tickets.details.finance.info')->with($data);
	}

	public function showTicketBookingDetails($ticketId)
	{
		$ticket = \Ticket::with('bookings')->findOrFail($ticketId);
		$bookings = $ticket->bookings;
		if (count($bookings))
		{
			$minPartyDate = $bookings->min('party_date_time');
		}
		else
		{
			$minPartyDate = $ticket->event_date;
		}
		// check and redirecting to other details page based on the condition fo the checkForRedirect()
		if (!$ticket->isTicketBookingVisible())
		{
			return redirect($this->getRedirectRoute($ticket));
		}

		$updatedReceipt = $this->checkUpdateReceipt($ticket);
		$typeTicketBookings = TypeTicketBooking::all();
		$typeBookingConcept = TypeBookingConcept::all();
		$eventId = $ticket->event_id ? $ticket->event_id : 1;
		$parentEventId = $this->getParentEventId($eventId);

		$checkoutFields = CheckoutField::where(function ($query) use ($parentEventId) {
			$query->where('event_id', $parentEventId)
			      ->orWhereNull('event_id');
		})->get();

		if ($ticket->is_auto_booked == 1)
		{
			$checkoutFields = $checkoutFields->where('is_auto_booking', 1);
		}
		else
		{
			$checkoutFields = $checkoutFields->where('is_auto_booking', 0);
		}

		$planners = \Vendor::where('is_live', 1)->where('city_id', $ticket->city_id)->get();
		$venues = \Venue::where('is_live', 1)->where('city_id', $ticket->city_id)->get();

		$falloutFeedback = $this->getFalloutFeedback($ticketId, $ticket->status_id);

		$eventProducts = [];
		// @todo: try to use cache

		$decors = \Decor::join('planner', 'planner.id', '=', 'decor.provider_id')
		                ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
		                ->select('decor.*')
		                ->where('decor.is_live', 1)
		                ->where('decor_event.event_id', $parentEventId)
		                ->where('planner.city_id', $ticket->city_id)
		                ->whereNull('decor.deleted_at')
		                ->whereNull('planner.deleted_at')
		                ->whereNull('decor_event.deleted_at')
		                ->get();

		$cakes = \Cake::join('planner', 'planner.id', '=', 'cake.provider_id')
		              ->join('cake_event', 'cake_event.event_id', '=', 'cake.id')
		              ->select('cake.*')
		              ->where('cake.is_live', 1)
		              ->where('cake_event.event_id', $parentEventId)
		              ->where('planner.city_id', $ticket->city_id)
		              ->whereNull('cake.deleted_at')
		              ->whereNull('planner.deleted_at')
		              ->whereNull('cake_event.deleted_at')
		              ->get();

		$entServices = TypeServices::join('service_event', 'service_event.type_service_id', '=', 'type_service.id')
		                           ->select('type_service.*')
		                           ->where('type_service.is_live', 1)
		                           ->where('service_event.type_event_id', $parentEventId)
		                           ->where('type_service.city_id', $ticket->city_id)
		                           ->whereNull('type_service.deleted_at')
		                           ->whereNull('service_event.deleted_at')
		                           ->get();

		$packages = \VendorPackage::select('planner_package.*')
		                          ->where('planner_package.is_live', 1)
		                          ->where('planner_package.event_id', $parentEventId)
		                          ->where('planner_package.city_id', $ticket->city_id)
		                          ->whereNull('planner_package.deleted_at')
		                          ->get();

		$addOns = AddOn::join('add_on_event', 'add_on_event.add_on_id', '=', 'add_on.id')
		               ->select('add_on.*')
		               ->where('add_on.is_live', 1)
		               ->where('add_on_event.event_id', $parentEventId)
		               ->where('add_on.city_id', $ticket->city_id)
		               ->whereNull('add_on.deleted_at')
		               ->whereNull('add_on_event.deleted_at')
		               ->get();

		if ($decors)
		{
			foreach ($decors as $decor)
			{
				array_push($eventProducts, [
					'id'            => $decor->id,
					'productTypeId' => config('evibe.ticket_type.decors'),
					'name'          => $decor->name,
					'code'          => $decor->code,
					'partnerId'     => $decor->provider_id,
					'partnerTypeId' => config('evibe.ticket_type.planners'),
					'price'         => $decor->min_price,
					'priceWorth'    => $decor->worth
				]);
			}
		}

		if ($cakes)
		{
			foreach ($cakes as $cake)
			{
				array_push($eventProducts, [
					'id'            => $cake->id,
					'productTypeId' => config('evibe.ticket_type.cakes'),
					'name'          => $cake->title,
					'code'          => $cake->code,
					'partnerId'     => $cake->provider_id,
					'partnerTypeId' => config('evibe.ticket_type.planners'),
					'price'         => $cake->price,
					'priceWorth'    => $cake->price_worth
				]);
			}
		}

		if ($entServices)
		{
			foreach ($entServices as $entService)
			{
				array_push($eventProducts, [
					'id'            => $entService->id,
					'productTypeId' => config('evibe.ticket_type.services'),
					'name'          => $entService->name,
					'code'          => $entService->code,
					'partnerId'     => null,
					'partnerTypeId' => null,
					'price'         => $entService->min_price,
					'priceWorth'    => $entService->worth_price
				]);
			}
		}

		if ($packages)
		{
			foreach ($packages as $package)
			{
				array_push($eventProducts, [
					'id'            => $package->id,
					'productTypeId' => $package->type_ticket_id,
					'name'          => $package->name,
					'code'          => $package->code,
					'partnerId'     => $package->map_id,
					'partnerTypeId' => $package->map_type_id,
					'price'         => $package->price,
					'priceWorth'    => $package->price_worth
				]);
			}
		}

		if ($addOns)
		{
			foreach ($addOns as $addOn)
			{
				array_push($eventProducts, [
					'id'            => $addOn->id,
					'productTypeId' => config('evibe.ticket_type.add-on'),
					'name'          => $addOn->name,
					'code'          => $addOn->code,
					'partnerId'     => null,
					'partnerTypeId' => null,
					'price'         => $addOn->price_per_unit,
					'priceWorth'    => $addOn->price_worth_per_unti
				]);
			}
		}

		$data = [
			'ticket'              => $ticket,
			'updatedReceipt'      => $updatedReceipt,
			'checkoutFields'      => $checkoutFields,
			'typeTicketBookings'  => $typeTicketBookings,
			'typeBookingConcepts' => $typeBookingConcept,
			'planners'            => $planners,
			'venues'              => $venues,
			'falloutFeedback'     => $falloutFeedback,
			'minPartyDateTime'    => $minPartyDate,
			'eventProducts'       => $eventProducts,
		];

		return view('tickets.details.booking.info')->with($data);
	}

	public function showTicketMappingDetails($ticketId)
	{
		$ticket = \Ticket::with('mappings', 'enquiries')->findOrFail($ticketId);
		$typeTickets = \TypeTicket::select("name", "id")->get();
		$mappingMultipleQuote = [];
		$cities = \City::all();

		$mapTypeIdsOccasion = $this->getMappingTypesOnOccasion($ticket->event_id);
		$quoteMappings = \TicketMapping::where('ticket_id', $ticketId)->whereNull('enquiry_created_at')->orderBy('map_type_id')->get();
		$typeTicketBookings = TypeTicketBooking::where("for_auto_booking", 0)->get();
		$typeBookingConcepts = TypeBookingConcept::all();

		$eventId = $ticket->event_id ? $ticket->event_id : 1;
		$parentEventId = $this->getParentEventId($eventId);
		$event = TypeEvent::where('id', $ticket->event_id)->first();

		$lastRecommendedAt = $quoteMappings->max('recommended_at');

		foreach ($quoteMappings as $quoteMapping)
		{
			$typeTkt = $typeTickets->find($quoteMapping->map_type_id) ? $typeTickets->find($quoteMapping->map_type_id)->name : null;
			$mappingMultipleQuote[$typeTkt][] = $quoteMapping;
		}

		$checkoutFields = CheckoutField::where(function ($query) use ($parentEventId) {
			$query->where('event_id', $parentEventId)
			      ->orWhereNull('event_id');
		})->get();

		$availCheclCheckoutFileds = $checkoutFields;

		if ($ticket->is_auto_booked == 1)
		{
			$checkoutFields = $checkoutFields->where('is_auto_booking', 1);
		}
		else
		{
			$checkoutFields = $checkoutFields->where('is_auto_booking', 0);
		}

		$falloutFeedback = $this->getFalloutFeedback($ticketId, $ticket->status_id);

		$typeFollowUp = TicketFollowupsType::select('name')
		                                   ->get();
		$planners = \Vendor::where('is_live', 1)->where('city_id', $ticket->city_id)->get();
		$venues = \Venue::where('is_live', 1)->where('city_id', $ticket->city_id)->get();

		$venuePackages = \VendorPackage::where('map_type_id', config('evibe.ticket_type.venues'))
		                               ->whereNull('deleted_at')
		                               ->where('is_live', 1)
		                               ->get();

		$ticketMappings = $ticket->mappings;
		$nonVenueMappingIds = [];
		foreach ($ticketMappings as $mapping)
		{
			$optionId = $mapping->map_id;
			$optionTypeId = $mapping->map_type_id;
			if (($optionTypeId == config('evibe.ticket_type.villa')) || ($optionTypeId == config('evibe.ticket_type.couple-experiences')))
			{
				$option = \VendorPackage::find($optionId);
				if (!$option)
				{
					// skip
					continue;
				}

				if ($option->map_type_id == config('evibe.ticket_type.venues'))
				{
					// skip
				}
				else
				{
					array_push($nonVenueMappingIds, $mapping->id);
				}
			}
			else
			{
				array_push($nonVenueMappingIds, $mapping->id);
			}
		}

		$nonVenueTicketMappings = $ticketMappings->filter(function ($query) use ($nonVenueMappingIds) {
			return in_array($query->id, $nonVenueMappingIds);
		});

		// form avail check notification data
		$availCheckMappings = \TicketMapping::where('ticket_id', $ticketId)
		                                    ->where('enquiry_type_id', config('evibe.enquiry.avl_check'))
		                                    ->whereNull('deleted_at')
		                                    ->get();

		$data = [
			'ticket'                   => $ticket,
			'mapTypeIdsOccasion'       => $mapTypeIdsOccasion,
			'typeTickets'              => $typeTickets,
			'multipleQuoteMappings'    => $mappingMultipleQuote,
			'typeTicketBookings'       => $typeTicketBookings,
			'typeBookingConcepts'      => $typeBookingConcepts,
			'checkoutFields'           => $checkoutFields,
			'availCheckCheckoutFields' => $availCheclCheckoutFileds,
			'eventName'                => $event ? $event->name : false,
			'cities'                   => $cities,
			'falloutFeedback'          => $falloutFeedback,
			'lastRecommendedAt'        => $lastRecommendedAt ?: null,
			'typeFollowUps'            => $typeFollowUp,
			'planners'                 => $planners,
			'venues'                   => $venues,
			'venueOptions'             => $venuePackages,
			'nonVenueTicketMappings'   => $nonVenueTicketMappings,
			'availCheckMappings'       => $availCheckMappings
		];

		return view('tickets.details.mapping.info')->with($data);
	}

	public function showAvailChecksDetails($ticketId)
	{
		$ticket = \Ticket::findOrFail($ticketId);

		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);

		$options = [
			"url"         => config("evibe.api.base_url") . "avail-checks/info",
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => [
				"ticketId" => $ticketId
			]
		];

		$data = $this->makeGuzzleRequest($options);

		$options = $data['options'];

		$data = [
			'ticket'           => $ticket,
			'options'          => $options,
			'availCheckData'   => $data['availCheckData'],
			'availCheckImages' => $data['availCheckImages']
		];

		return view('tickets.details.avail-checks.info')->with($data);

	}

	public function showTicketUpdateDetails($ticketId)
	{
		$ticket = \Ticket::find($ticketId);

		if (!$ticket)
		{
			return view("errors.404");
		}

		$mappingMultipleQuote = [];
		$statuses = \TypeTicketStatus::pluck('name', 'id')
		                             ->toArray();

		$falloutFeedback = $this->getFalloutFeedback($ticketId, $ticket->status_id);

		// marge ticket updates and log reminders and sort them
		$ticketUpdates = \TicketUpdate::select("id", "handler_id", "created_at", "status_at", "status_id", "type_update", "comments")
		                              ->where('ticket_id', $ticketId)
		                              ->get()
		                              ->keyBy("created_at")
		                              ->toArray();

		$ticketReminders = LogTicketReminders::select("id", "comments", "created_at", "type_reminder_group_id")
		                                     ->where('ticket_id', $ticketId)
		                                     ->whereNull('deleted_at')
		                                     ->get()
		                                     ->keyBy("created_at")
		                                     ->toArray();

		$handlers = \App\Models\User::whereIn("id", array_column($ticketUpdates, "handler_id"))
		                            ->pluck("name", "id")
		                            ->toArray();

		$ticketUpdates = array_merge($ticketUpdates, $ticketReminders);
		krsort($ticketUpdates);

		$typeFollowUp = TicketFollowupsType::select('name')
		                                   ->get();

		$data = [
			'ticket'                => $ticket,
			'multipleQuoteMappings' => $mappingMultipleQuote,
			'statuses'              => $statuses,
			'handlers'              => $handlers,
			'ticketUpdates'         => $ticketUpdates,
			'falloutFeedback'       => $falloutFeedback,
			'typeFollowUps'         => $typeFollowUp
		];

		return view('tickets.details.update.info')->with($data);
	}

	public function showTicketInformation($ticketId, $copy = null)
	{
		$ticket = \Ticket::find($ticketId);
		if ($ticket)
		{
			$cancellationMailMsg = "";
			$cities = \City::all();
			$areas = \Area::all();
			$occasions = TypeEvent::all();
			$sources = \TypeTicketSource::all();
			$leadStatus = \TypeLeadStatus::all();
			$typeTicket = \TypeTicket::select("id", "name")->get();
			$ticketSiteVisit = TicketSiteVisit::where('ticket_id', $ticketId)->first();
			$typeVenue = \TypeVenue::find($ticket->type_venue_id);

			$eventId = $ticket->event_id;
			$parentEventId = $this->getParentEventId($eventId);
			$ticketServiceTags = TicketServicesTags::where('ticket_id', $ticketId)->get();

			$typeCategoryTags = TypeCategoryTags::whereIn('id', $ticketServiceTags->pluck('type_category_tag_id')->toArray())
			                                    ->select('type_tag_id', 'id', 'count', 'min_price', 'max_price')
			                                    ->get();

			$typeTags = \Tags::whereIn("id", $typeCategoryTags->pluck('type_tag_id')->toArray())
			                 ->select("identifier", "map_type_id", "id")
			                 ->where(function ($query) use ($parentEventId) {
				                 $query->where('type_tags.type_event', $parentEventId)
				                       ->orWhere('type_tags.type_event', null);
			                 })
			                 ->whereNotNull('map_type_id')
			                 ->get();

			$typeServiceNames = \TypeTicket::whereIn("id", $typeTags->pluck("map_type_id")->toArray())
			                               ->pluck("name", "id");

			$selectedServices = [];
			foreach ($ticketServiceTags as $key => $value)
			{
				$categoryTag = $typeCategoryTags->where("id", $value->type_category_tag_id)->first();

				if ($categoryTag)
				{
					$tag = $typeTags->where("id", $categoryTag->type_tag_id)->first();
					if ($tag && $tag->map_type_id && isset($typeServiceNames[$tag->map_type_id]))
					{
						$selectedServices[$typeServiceNames[$tag->map_type_id]][] = [
							"name"         => $tag->identifier,
							"optionsCount" => $categoryTag->count,
							"minPrice"     => $categoryTag->min_price,
							"maxPrice"     => $categoryTag->max_price
						];
					}
				}
			}

			$ticketCancelLog = \LogTicketActions::where('ticket_id', $ticketId)
			                                    ->where('is_success', 1)
			                                    ->where('action', 'LIKE', 'ManualCancellationMail')
			                                    ->first();

			if ($ticketCancelLog)
			{
				$cancellationMailMsg = "A cancellation mail has been sent to this customer manually on " . date("d-M-Y H:i", strtotime($ticketCancelLog->updated_at));
				if ($ticket->cancellationReason())
				{
					$cancellationMailMsg .= '. Reason :: <b>' . $ticket->cancellationReason() . '</b>';
				}
			}

			$emailSig = config('evibe.email_salutation');
			$emailSig .= AppUtil::getHandlerName() . ",<br/>";
			$emailSig .= "Customer Delight Advisor,<br/>";
			$emailSig .= "www.evibe.in";

			$cancellationReason = TypeCancellationReason::all();
			$ticketReviewQuestions = TicketReviewQuestion::all();

			$falloutFeedback = $this->getFalloutFeedback($ticketId, $ticket->status_id);

			$currentTicketFollowUpDetails = TicketFollowup::where('ticket_id', $ticketId)
			                                              ->whereNull('is_complete')
			                                              ->first();

			if ($currentTicketFollowUpDetails)
			{
				$currentTicketFollowUpDetails = $currentTicketFollowUpDetails->followup_date;
			}

			$typeFollowUp = TicketFollowupsType::select('name')
			                                   ->get();

			$data = [
				'ticket'                => $ticket,
				'cancellationMailMsg'   => $cancellationMailMsg,
				'cities'                => $cities,
				'areas'                 => $areas,
				'occasions'             => $occasions,
				'sources'               => $sources,
				'leadStatus'            => $leadStatus,
				'emailSig'              => $emailSig,
				'cancellationReason'    => $cancellationReason,
				'ticketReviewQuestions' => $ticketReviewQuestions,
				'copy'                  => $copy,
				'selectedServices'      => $selectedServices,
				'falloutFeedback'       => $falloutFeedback,
				'ticketSiteVisit'       => $ticketSiteVisit,
				'ticketFollowUpDetails' => $currentTicketFollowUpDetails,
				'typeVenue'             => $typeVenue,
				'parentEventId'         => $this->getParentEventId($ticket->event_id),
				'typeFollowUps'         => $typeFollowUp
			];

			return view('tickets.details.basicInfo.info')->with($data);
		}
		else
		{
			return redirect()->back();
		}
	}

	public function getEmailTrackerData()
	{
		$id = Input::get('ticketId');
		$emailsSent = EmailTrack::where('headers', 'like', '%' . $id . '%')->get();

		return response()->json(['success' => true, 'emailSentData' => $emailsSent]);
	}

	public function fetchTicketPartyTimings($ticketId)
	{
		$ticket = \Ticket::with('bookings')->find($ticketId);
		$partyTiming = $ticket->event_date ? date("d M Y h:i A", $ticket->event_date) : '--';
		if ($ticket->bookings)
		{
			$bookings = $ticket->bookings;
			$minBookTime = $bookings->min('party_date_time');
			$maxBookTime = $bookings->max('party_date_time');

			if ($minBookTime != 0)
			{
				$partyTiming = date("d M Y h:i A", $minBookTime);
			}

			// @see: did not change the display structure because of its usage by CRM team for internal purposes
			if ($minBookTime != $maxBookTime)
			{
				$partyTiming = date("d M Y h:i A", $minBookTime) . ' - ' . date("d M Y h:i A", $maxBookTime);
			}
		}

		return response()->json(['success' => true, 'partyTime' => $partyTiming]);
	}

	/** Edit lead status details */
	public function editLeadStatus($ticketId)
	{
		$ticket = \Ticket::find($ticketId);
		$leadStatusId = request('leadStatus', null);
		$validLeadStatusIds = \TypeLeadStatus::all()->pluck("id")->toArray();
		$errorMessage = null;

		if (!$ticket)
		{
			$errorMessage = "Invalid ticket id";
		}
		elseif (is_null($leadStatusId))
		{
			$errorMessage = "Lead status id cannot be null";
		}
		elseif (!in_array($leadStatusId, $validLeadStatusIds))
		{
			$errorMessage = "Invalid lead status id";
		}
		else
		{
			// do nothing
		}

		if (!is_null($errorMessage))
		{
			return response()->json([
				                        "success" => false,
				                        "error"   => $errorMessage
			                        ]);
		}

		$ticket->update(['lead_status_id' => $leadStatusId]);

		$res = [
			'success' => true,
			'value'   => $ticket->leadStatus->name
		];

		return response()->json($res);
	}

	public function getTicketUpdateDetails()
	{
		$last30DaysTickets = \Ticket::select('id', 'name', 'phone', 'email')
		                            ->where('created_at', '>=', Carbon::today()->subDay(1))
		                            ->where('created_at', '<=', Carbon::now())
		                            ->get();

		$statuses = \TypeTicketStatus::whereNull('change_internal')->get();

		$data = [
			"ticketsData"     => $last30DaysTickets,
			"ticketsStatuses" => $statuses
		];

		return response()->json($data);
	}

	private function getRedirectRoute($ticket)
	{
		$route = '';

		if ($ticket->status_id == config('evibe.status.cancelled'))
		{
			$route = route('ticket.details.info', $ticket->id);
		}
		elseif (!$ticket->bookings->count())
		{
			$route = route('ticket.details.mappings', $ticket->id);
		}

		return $route;
	}

	public function setFollowUpForNoResponseAfterMail($ticketId)
	{
		$ticket = \Ticket::find($ticketId);
		$comment = Input::get('noResponseFollowUpComment');
		$rules = [
			'noResponseFollowUpDate' => 'required|date|after:now',
			'noResponseClosureDate'  => 'date|after:now'
		];
		$messages = [
			'noResponseFollowUpDate.required' => "Date is required",
			'noResponseFollowUpDate.date'     => 'Followup Date is invalid',
			'noResponseFollowUpDate.after'    => 'Followup Date should be after current date',
			'noResponseClosureDate.date'      => 'Closure Date is invalid',
			'noResponseClosureDate.after'     => 'Closure Date should be after current date'
		];

		$validator = Validator::make(Input::all(), $rules, $messages);
		if ($validator->fails())
		{
			$res = [
				"success" => false,
				"msg"     => $validator->messages()->first()
			];

			return response()->json($res);
		}
		if ($ticket)
		{
			$isFollowup = TicketFollowup::where('ticket_id', $ticketId)
			                            ->whereNull('is_complete')
			                            ->first();

			if ($isFollowup)
			{
				$isFollowup->update(
					[
						"followup_date" => strtotime(Input::get('noResponseFollowUpDate')),
						'comments'      => $comment,
						'followup_type' => $comment
					]
				);
			}
			else
			{
				TicketFollowup::create([
					                       "followup_date" => strtotime(Input::get('noResponseFollowUpDate')),
					                       "ticket_id"     => $ticketId,
					                       "comments"      => $comment,
					                       'followup_type' => $comment
				                       ]);
			}
			$closureDate = strtotime(request()->input('noResponseClosureDate'));
			if ($ticket->expected_closure_date != $closureDate && $closureDate)
			{
				$ticket->expected_closure_date = $closureDate;
				$ticket->update();
			}
			$res = [
				"success" => true,
				"error"   => "Successfully Updated the followUp"
			];

			return response()->json($res);
		}
		else
		{
			$res = [
				"success" => false,
				"msg"     => "Some error occurred"
			];

			return response()->json($res);
		}
	}

	public function updateCustomerInfo()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$options = [
			"url"         => config("evibe.api.base_url") . config('evibe.api.tickets.update'),
			"method"      => "GET",
			"accessToken" => $accessToken,
			"jsonData"    => request()->all()
		];
		$data = $this->makeGuzzleRequest($options);

		return response()->json($data);

	}
}
