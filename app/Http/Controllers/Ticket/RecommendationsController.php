<?php

namespace App\Http\Controllers\Ticket;

use App\Models\Ticket\TicketFollowupsType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class RecommendationsController extends TicketUtilController
{
	public function getSelectedServices($ticketId)
	{
		$typeTicket = \TypeTicket::select("id", "name")->get();
		$typeFollowUp = TicketFollowupsType::select('name')
		                                   ->get();
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'suggestions/' . $ticketId;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);
		$res['typeFollowUps'] = $typeFollowUp;
		if (!isset($res["response"]) || !$res["response"])
		{
			$response = [
				"ticketId"      => $ticketId,
				'typeFollowUps' => $typeFollowUp
			];

			return view('tickets.recommendations.recommendations_error', ["data" => $response]);
		}
		elseif (count($res["serviceTags"]) == 0)
		{
			return redirect(route("ticket.modify.edit.services", [$ticketId, 1]));
		}

		foreach ($res["serviceTags"] as $service)
		{
			$serviceType = $typeTicket->where('id', $service["mapTypeId"])->first();
			if ($serviceType)
			{
				$res["categorizedServices"][$serviceType->name][] = [
					"id"           => $service["id"],
					"name"         => $service["name"],
					"optionsCount" => $service["optionsCount"],
					"minPrice"     => $service["minPrice"],
					"maxPrice"     => $service["maxPrice"],
					"description"  => $service["description"],
				];

				$res["selectedServiceTagIds"][] = $service["mapTypeId"];
			}
			$res["selectedServiceTagIds"] = array_unique($res["selectedServiceTagIds"]);
		}
		$res["typeTicket"] = $typeTicket;

		return view('tickets.recommendations.selected_services', ["data" => $res]);
	}

	public function getSelectedServiceOptions($ticketId, $serviceId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'suggestions/' . $ticketId . '/' . $serviceId;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				"search"     => request("query"),
				"price_min"  => request("priceMin"),
				"price_max"  => request("priceMax"),
				"category"   => request("category"),
				"price_sort" => request('priceSort'),
				"area"       => request('area')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (!isset($res["success"]) && !$res["success"])
		{
			return view('tickets.recommendations.recommendations_error');
		}

		$res["ticketId"] = $ticketId;
		$res["serviceId"] = $serviceId;

		return view('tickets.recommendations.service_options', ["data" => $res]);
	}

	public function editServiceOption($ticketId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'suggestions/' . $ticketId;

		$options = [
			'method'      => (request("editValue") == 1) ? "POST" : "DELETE",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				"optionId"     => request("id"),
				"optionTypeId" => request("serviceId")
			]
		];

		$res = $this->makeApiCallWithUrl($options);
		$res["ticketId"] = $ticketId;

		if ($res["success"])
		{
			return response()->json([
				                        "success" => true,
			                        ]);
		}

		return response()->json([
			                        "success" => false,
			                        "error"   => $res["error"]
		                        ]);
	}

	public function getSelectedOptions($ticketId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'recos/' . $ticketId . '/preview';

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);
		$res["ticketId"] = $ticketId;

		if (!isset($res["success"]) && !$res["success"])
		{
			return view('tickets.recommendations.recommendations_error');
		}

		return view('tickets.recommendations.selected_options', ["data" => $res]);
	}

	public function sendRecommendations($ticketId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.dashBoard.prefix') . 'recos/' . $ticketId . '/send';

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);
		if ($res["success"])
		{

			return response()->json([
				                        "success"     => true,
				                        "followUp"    => $res['followUp'],
				                        "name"        => $res['name'],
				                        "phone"       => $res['phone'],
				                        "callingCode" => (isset($res['callingCode']) && $res['callingCode']) ? $res['callingCode'] : "+91",
				                        "recoUrl"     => $res['recoUrl']
			                        ]);
		}

		return response()->json([
			                        "success" => false,
			                        "error"   => $res["error"]
		                        ]);
	}
}