<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Evibe\Facades\AppUtilFacade as AppUtil;

class TrendsAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if (!AppUtil::isTabVisible('trends'))
            {
                return App::abort(405);
            }
        return $next($request);
    }
}
