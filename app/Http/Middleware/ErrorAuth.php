<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Closure;
use Evibe\Facades\AppUtilFacade as AppUtil;

class ErrorAuth
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  Request $request
	 * @param Closure $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!AppUtil::isTabVisible('errors'))
		{
			return App::abort(405);
		}

		return $next($request);
	}
}
