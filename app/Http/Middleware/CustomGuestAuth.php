<?php

namespace App\Http\Middleware;

use Closure;
use Evibe\Facades\AppUtilFacade as AppUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CustomGuestAuth
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  Request $request
	 * @param Closure $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		if (Auth::check())
		{
			//Redirect::route('tickets');
			$roleId = Auth::user()->role_id;
			AppUtil::setSessions();

			// Role ids are as per database
			// ensure it is in sync with 'role' table in database
			// 1: super admin
			// 2: admin
			// 6: customer delight team
			if (in_array($roleId, [1, 2, 6, 9]))
			{
				return Redirect::to('/tickets');
			}

			// 5: business developer
			elseif (in_array($roleId, [5]))
			{
				return Redirect::to('/venues');
			}

			elseif (in_array($roleId, [config("evibe.roles.syrow")]))
			{
				return Redirect::to('/s/tickets/list');
			}

			elseif (in_array($roleId, [config("evibe.roles.tech")]))
			{
				return redirect(route("monitor.payments"));
			}

			// restricted access
			else
			{
				return Redirect::route('login')->with('auth_error', 'Not an authorised user')->withInput();
			}

		}

		return $next($request);
	}
}