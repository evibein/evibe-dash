<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthBlockedUsers
{
	/**
	 * Handle an incoming request.
	 *
	 * @param Request     $request
	 * @param Closure     $next
	 * @param string|null $guard
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null)
	{
		if (strpos($request->getUri(), "query=7908954573") !== false)
		{
			Log::info($request->ip() . " - " . $_SERVER['HTTP_USER_AGENT'] . " - " . $_SERVER['REQUEST_TIME_FLOAT']);
			return "Un-Authorized";
		}

		return $next($request);
	}
}