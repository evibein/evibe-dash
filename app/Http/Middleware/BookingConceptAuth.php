<?php

namespace App\Http\Middleware;

use Closure;
use Evibe\Facades\AppUtilFacade as AppUtil;
use Illuminate\Http\Request;

class BookingConceptAuth
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  Request  $request
	 * @param Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!AppUtil::isTabVisible('booking_concept'))
		{
			return App::abort(405);
		}

		return $next($request);
	}
}
