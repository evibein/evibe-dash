<?php

namespace App\Http;

use App\Http\Middleware\AnalyticsAuth;
use App\Http\Middleware\AuthBlockedUsers;
use App\Http\Middleware\AuthPartnerProfile;
use App\Http\Middleware\AuthUserManagement;
use App\Http\Middleware\BookingConceptAuth;
use App\Http\Middleware\CakeAuth;
use App\Http\Middleware\CustomGuestAuth;
use App\Http\Middleware\DeliveryImageAuth;
use App\Http\Middleware\EncryptCookies;
use App\Http\Middleware\ErrorAuth;
use App\Http\Middleware\ImageAuth;
use App\Http\Middleware\MonitoringAuth;
use App\Http\Middleware\NearByAreaAuth;
use App\Http\Middleware\OldDashboard;
use App\Http\Middleware\PackageAuth;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Middleware\ReviewAuth;
use App\Http\Middleware\TicketsAuth;
use App\Http\Middleware\TrackingAuth;
use App\Http\Middleware\TrendsAuth;
use App\Http\Middleware\VendorAuth;
use App\Http\Middleware\VenueAuth;
use App\Http\Middleware\VenueHallsAuth;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class Kernel extends HttpKernel
{
	/**
	 * The application's global HTTP middleware stack.
	 *
	 * These middleware are run during every request to your application.
	 *
	 * @var array
	 */
	protected $middleware = [
		CheckForMaintenanceMode::class,
	];

	/**
	 * The application's route middleware groups.
	 *
	 * @var array
	 */
	protected $middlewareGroups = [
		'web' => [
			AuthBlockedUsers::class,
			EncryptCookies::class,
			AddQueuedCookiesToResponse::class,
			StartSession::class,
			ShareErrorsFromSession::class,
			/*\App\Http\Middleware\VerifyCsrfToken::class,*/
			SubstituteBindings::class,
		],

		'api' => [
			'throttle:60,1',
			'bindings',
		],
	];

	/**
	 * The application's route middleware.
	 *
	 * These middleware may be assigned to groups or used individually.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth'                      => Authenticate::class,
		'auth.basic'                => AuthenticateWithBasicAuth::class,
		'guest'                     => RedirectIfAuthenticated::class,
		'throttle'                  => ThrottleRequests::class,
		'authorize.vendors'         => VendorAuth::class,
		'authorize.tickets'         => TicketsAuth::class,
		'authorize.quick-book'      => TicketsAuth::class,
		'authorize.venues'          => VenueAuth::class,
		'authorize.packages'        => PackageAuth::class,
		'authorize.trends'          => TrendsAuth::class,
		'authorize.venue-halls'     => VenueHallsAuth::class,
		'authorize.images'          => ImageAuth::class,
		'authorize.reviews'         => ReviewAuth::class,
		'authorize.errors'          => ErrorAuth::class,
		'authorize.nearby-areas'    => NearByAreaAuth::class,
		'authorize.cakes'           => CakeAuth::class,
		'authorize.analytics'       => AnalyticsAuth::class,
		'guest.custom'              => CustomGuestAuth::class,
		'authorize.user_management' => AuthUserManagement::class,
		'authorize.partner_profile' => AuthPartnerProfile::class,
		'authorize.booking_concept' => BookingConceptAuth::class,
		'authorize.tracking'        => TrackingAuth::class,
		'authorize.delivery_images' => DeliveryImageAuth::class,
		'authorize.monitor'         => MonitoringAuth::class,
		'authorize.old-dashboard'   => OldDashboard::class,
	];
}
