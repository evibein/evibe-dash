<?php

namespace App\Models\Tracking;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderTracking extends Model
{
	use SoftDeletes;

	protected $table = 'order_tracking';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}