<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class DecorReview extends Eloquent {

	use SoftDeletes;
	
	protected $table = 'decor_review';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];
	
	public function decor()
	{
		return $this->belongsTo('Decor', 'decor_id');
	}
}