<?php

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\DecorEvent;

class Decor extends Model
{
	use SoftDeletes;

	protected $table = 'decor';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function provider()
	{
		return $this->belongsTo('Vendor', 'provider_id');
	}

	public function gallery()
	{
		return $this->hasMany('DecorGallery', 'decor_id');
	}

	public function events()
	{
		return $this->hasMany(DecorEvent::class, 'decor_id');
	}

	public function tags()
	{
		return $this->hasMany(DecorTags::class, 'decor_id');
	}

	public function scopeForCity($query, $cityId)
	{
		return $query->join('planner', 'decor.provider_id', '=', 'planner.id')
		             ->where('planner.city_id', $cityId);
	}

	public function getProfilePic()
	{
		$host = config('evibe.gallery.host');
		$defaultHost = config('evibe.gallery.default_profile_pic.root');
		$vendorDefault = config('evibe.gallery.default_profile_pic.decor');
		$profilePic = $host . '/' . $defaultHost . '/' . $vendorDefault;

		// if none of them has is_profile = 1, get last uploaded
		$profileImgObj = DecorGallery::where('decor_id', $this->id)
		                             ->orderBy('is_profile', 'DESC')
		                             ->orderBy('updated_at', 'DESC')
		                             ->first();
		if ($profileImgObj)
		{
			$profilePic = $profileImgObj->getLink();
		}

		return $profilePic;
	}

	public function getTags()
	{
		$tags = [];
		$decorTagsList = $this->tags;

		// available tags
		$availableTags = [];
		$allTags = Tags::all();

		foreach ($allTags as $tag)
		{
			$availableTags[$tag->id] = [
				'name'      => $tag->name,
				'parent_id' => $tag->parent_id,
			];
		}

		foreach ($decorTagsList as $decorTag)
		{
			$currentTag = $availableTags[$decorTag->tag_id];
			$tagString = "";

			if ($currentTag['parent_id'])
			{
				$parentId = $currentTag['parent_id'];
				$parentTag = $availableTags[$parentId];
				if ($parentTag)
				{
					$tagString .= $parentTag['name'] . " :: ";
				}
			}

			$tagString .= $currentTag['name'];

			if ($tagString)
			{
				$tags[$decorTag->tag_id] = $tagString;
			}
		}

		return $tags;
	}

	public function getTagsString($type = 0)
	{
		$tagsStr = '';
		$tagList = [];
		foreach ($this->tags as $decorTag)
		{
			if (!in_array($decorTag->tags->name, $tagList))
			{
				array_push($tagList, $decorTag->tags->name);
				$tagsStr .= $decorTag->tags->name . ', ';
			}

		}
		$tagsStr = substr($tagsStr, 0, -2); // remove trailing ", "
		return $tagsStr;
	}

	public function getLiveLink()
	{
		$liveHost = config('evibe.live.host');
		$profileUrl = config('evibe.live.profile_url.decors');
		$cityUrl = $this->provider->city->url;

		return $liveHost . '/' . $cityUrl . '/' . $profileUrl . '/' . $this->url;
	}

	public function optimizeImages()
	{
		// @todo bad practice, find a fix
		ini_set('memory_limit', '128M');
		$profileWidth = 640;
		$resultWidth = 394;
		$thumbsWidth = 80;

		// optimize decor images
		foreach ($this->gallery as $decorImage)
		{
			$base = '../../gallery/decors/' . $this->id . '/images/';
			$imgPath = $base . $decorImage->url;
			$profileImgPath = $base . 'profile/';
			$resultsImgPath = $base . 'results/';
			$thumbsImgPath = $base . 'thumbs/';

			$img = Image::make($imgPath);

			// profile
			AppUtil::resizeImg($img, "Profile", $profileImgPath, $decorImage->url, $profileWidth);

			// results
			AppUtil::resizeImg($img, "Results", $resultsImgPath, $decorImage->url, $resultWidth);

			// thumbs
			AppUtil::resizeImg($img, "Thumbs", $thumbsImgPath, $decorImage->url, $thumbsWidth);
		}
	}

	public function getPriceWorth()
	{
		return $this->worth;
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
		             ->whereNull('decor_event.deleted_at')
		             ->where('decor_event.event_id', $eventId);
	}

	// Mutators
	public function setNameAttribute($value)
	{
		$this->attributes['name'] = ucwords(strtolower(trim($value)));
	}

	public function getNameAttribute($value)
	{
		return ucwords($value);
	}

	public function getLink()
	{
		return route('decor.info.view', $this->id);
	}

	public function handler()
	{
		return $this->belongsTo(\App\Models\User::class, 'handler_id');
	}
}