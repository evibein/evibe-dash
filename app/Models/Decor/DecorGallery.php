<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class DecorGallery extends Eloquent
{

	use SoftDeletes;

	protected $table = 'decor_gallery';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function decor()
	{
		return $this->belongsTo('Decor', 'decor_id');
	}

	public function scopeTypeImages($query)
	{
		return $query->where('type_id', Config::get('evibe.gallery.type.image'));
	}

	public function getLink($type = null)
	{
		$decor = $this->decor;
		if ($decor)
		{
			$imgLink = AppUtil::getGalleryBaseUrl();
			$imgLink .= '/decors/' . $decor->id . '/images/';
			$validTypes = ["profile", "results", "thumbs"];

			if (!is_null($type) && in_array($type, $validTypes))
			{
				$imgLink .= "/$type/";
			}

			$imgLink .= $this->url;
		}
		else
		{
			$imgLink = '';//@todo Default id
		}

		return $imgLink;
	}
}