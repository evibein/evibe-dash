<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DecorEvent extends Model
{
	use SoftDeletes;

	protected $table = 'decor_event';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];

	public function event()
	{
		return $this->belongsTo('App\Models\TypeEvent','event_id');
	}

}
