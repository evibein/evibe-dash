<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class DecorTags extends Eloquent {

	use SoftDeletes;
	
	protected $table = 'decor_tags';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];
	
	public function tags()
	{
		return $this->belongsTo('Tags', 'tag_id');
	}
}