<?php

namespace App\Models\AddOn;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddOn extends Model
{
	use SoftDeletes;

	protected $table = 'add_on';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('add_on_event', 'add_on_event.add_on_id', '=', 'add_on.id')
		             ->whereNull('add_on_event.deleted_at')
		             ->where('add_on_event.event_id', $eventId);
	}

	public function scopeIsLive($query)
	{
		return $query->where('add_on.is_live', 1);
	}

	public function scopeForCity($query, $cityId)
	{
		return $query->where('add_on.city_id', $cityId);
	}

	public function city()
	{
		return $this->belongsTo('City', 'city_id');
	}

	public function getProfilePic()
	{
		$host = config('evibe.gallery.host');
		$profilePic = $host . '/addons/' . $this->id . '/' . $this->image_url;

		return $profilePic;
	}
}