<?php

namespace App\Models\AddOn;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddOnTags extends Model
{
	use SoftDeletes;

	protected $table = 'add_on_tags';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}