<?php

namespace App\Models\AddOn;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddOnEvent extends Model
{
	use SoftDeletes;

	protected $table = 'add_on_event';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}