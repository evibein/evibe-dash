<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModel extends Model
{
	protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

	use SoftDeletes;
}