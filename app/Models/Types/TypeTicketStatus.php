<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeTicketStatus extends \Eloquent {

	use SoftDeletes;
	
	protected $table = 'type_ticket_status';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];

}