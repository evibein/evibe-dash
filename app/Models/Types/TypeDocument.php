<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeDocument extends Model
{
	use SoftDeletes;

	protected $table = 'type_document';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}