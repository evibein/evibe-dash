<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeCakeCategoryValues extends Model
{
	use SoftDeletes;

	protected $table = 'type_cake_category_value';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

}
