<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class Tags extends \Eloquent
{

	use SoftDeletes;

	protected $table = 'type_tags';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function packages()
	{
		return $this->belongsToMany('VendorPackage', 'planner_package_tags', 'tile_tag_id', 'planner_package_id');
	}

	public function event()
	{
		return $this->belongsTo(\App\Models\TypeEvent::class, 'type_event');
	}

	public function mapType()
	{
		return $this->belongsTo(TypeTicket::class, 'map_type_id');
	}

	public function parent()
	{
		return $this->belongsTo(Tags::class, 'parent_id');
	}

	public function typeBooking()
	{
		return $this->belongsTo(\App\Models\Types\TypeTicketBooking::class, 'type_booking_id');
	}
}