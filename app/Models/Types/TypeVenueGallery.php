<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeVenueGallery extends Model
{
	use SoftDeletes;

	protected $table = 'type_venue_gallery';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];
}
