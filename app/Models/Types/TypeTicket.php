<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeTicket extends \Eloquent
{

	use SoftDeletes;

	protected $table = 'type_ticket';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function getNameAttribute($value)
	{
		return ucfirst($value);
	}

}