<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePartnerOptionsFields extends Model
{
	use SoftDeletes;

	protected $table = 'type_partner_options_fields';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}