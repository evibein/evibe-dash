<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeVenueMenu extends Model
{
	use SoftDeletes;

	protected $table = 'type_venue_menu';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];
}
