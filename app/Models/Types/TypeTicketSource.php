<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeTicketSource extends Eloquent {

	use SoftDeletes;
	
	protected $table = 'type_ticket_source';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];

}