<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePackageGallery extends Model
{
	use SoftDeletes;

	protected $table = 'type_package_gallery';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];
}
