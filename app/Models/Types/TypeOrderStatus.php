<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeOrderStatus extends Model
{
	use SoftDeletes;

	protected $table = 'type_order_status';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}