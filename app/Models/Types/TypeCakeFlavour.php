<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeCakeFlavour extends Model
{
	use SoftDeletes;

	protected $table = 'type_cake_flavour';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
}
