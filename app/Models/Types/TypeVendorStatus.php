<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeVendorStatus extends Model
{
	use SoftDeletes;
	protected $table = 'type_vendor_status';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
