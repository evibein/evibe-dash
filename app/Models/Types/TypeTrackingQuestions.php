<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeTrackingQuestions extends Model
{
	use SoftDeletes;

	protected $table = 'type_tracking_questions';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
