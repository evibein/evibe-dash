<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeEnquirySource extends Model
{
	use SoftDeletes;

	protected $table = 'type_enquiry_source';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}