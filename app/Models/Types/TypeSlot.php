<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeSlot extends Model
{
	use SoftDeletes;

	protected $table = 'type_slot';
	protected $dates = ['deleted_at'];
	protected $guarded = [];
	public static $rules = [];
}
