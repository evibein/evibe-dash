<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeServices extends Model
{
	use SoftDeletes;

	protected $table = 'type_service';
	protected $dates = ['deleted_at'];
	protected $guarded = [];
	public static $rules = [];

	public function gallery()
	{
		return $this->hasMany(ServiceGallery::class, 'type_service_id');
	}

	public function city()
	{
		return $this->belongsTo(\City::class, 'city_id');
	}

	public function events()
	{
		return $this->hasMany(ServiceEvent::class, 'type_service_id');
	}

	public function getProfilePic()
	{
		$host = config('evibe.gallery.host');
		$defaultHost = config('evibe.gallery.default_profile_pic.root');
		$vendorDefault = config('evibe.gallery.default_profile_pic.service');
		$profilePic = $host . '/' . $defaultHost . '/' . $vendorDefault;

		// if none of them has is_profile = 1, get last uploaded
		$profileImgObj = ServiceGallery::where('type_service_id', $this->id)
		                               ->orderBy('is_profile', 'DESC')
		                               ->orderBy('updated_at', 'DESC')
		                               ->first();
		if ($profileImgObj)
		{
			$profilePic = $profileImgObj->getLink();
		}

		return $profilePic;
	}

	public function getEventString()
	{
		$serviceEvents = $this->events;
		$eventsList = '';
		foreach ($serviceEvents as $serviceEvent)
		{
			$eventsList .= $serviceEvent->event->name . ', ';
		}
		$allEvents = substr($eventsList, 0, -2);

		return $allEvents;
	}

	public function scopeSelectCols($query)
	{
		return $query->select('type_service.id', 'type_service.name', 'type_service.min_price', 'type_service.max_price', 'type_service.worth_price',
		                      'type_service.created_at', 'type_service.updated_at', 'type_service.deleted_at', 'type_service.is_live');
	}

	// Mutators
	public function setNameAttribute($value)
	{
		$this->attributes['name'] = ucwords(strtolower(trim($value)));
	}

	public function getNameAttribute($value)
	{
		return ucwords($value);
	}

	public function getLiveLink()
	{
		$liveHost = config('evibe.live.host');
		$profileUrl = config('evibe.live.profile_url.entertainment');
		$cityUrl = $this->city ? $this->city->url : "bangalore";

		return $liveHost . '/' . $cityUrl . '/' . $profileUrl . '/' . $this->url;
	}

	public function getLink()
	{
		return route('services.details', $this->id);
	}
}
