<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeField extends Model
{
	use SoftDeletes;
	protected $table = 'type_field';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
