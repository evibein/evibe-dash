<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class BookingTerms extends \Eloquent {
	
	use SoftDeletes;
	
	protected $table = 'type_terms';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

}