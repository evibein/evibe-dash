<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeVenue extends Eloquent {

	use SoftDeletes;
	
	protected $table = 'type_venue';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];

}