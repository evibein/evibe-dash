<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeCakeCategory extends Model
{
	use SoftDeletes;

	protected $table = 'type_cake_category';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

	public function values()
	{
		return $this->hasMany(TypeCakeCategoryValues::class, 'type_cake_cat_id');
	}

	public function getFilterValue($cakeId)
	{
		$categoryValues = $this->values;
		$cake = \Cake::findOrFail($cakeId);
		$data = [];

		foreach ($categoryValues as $categoryValue)
		{
			if ($categoryValue->type_cake_cat_id == config('evibe.cake_cat_value.weight'))
			{
				if ($categoryValue->value > (float)$cake->min_order)
				{
					$data[] = $this->getCategoryValue($categoryValue);
				}
			}
			else
			{
				$data[] = $this->getCategoryValue($categoryValue);
			}
		}

		return $data;
	}

	private function getCategoryValue($categoryValue)
	{
		return [
			'id'   => $categoryValue->id,
			'name' => $categoryValue->name
		];
	}
}
