<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeCancellationReason extends Model
{
	use SoftDeletes;

	protected $table = 'type_cancellation_reason';
	protected $dates = ['deleted_at'];
	protected $guarded = [];
	public static $rules = [];
}
