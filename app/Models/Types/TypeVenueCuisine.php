<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeVenueCuisine extends \Eloquent
{

	use SoftDeletes;

	protected $table = 'type_venue_cuisine';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];

}