<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeNotification extends Eloquent
{

	use SoftDeletes;

	protected $table = 'type_notification';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

}