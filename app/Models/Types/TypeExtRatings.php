<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeExtRatings extends \Eloquent {

	use SoftDeletes;
	
	protected $table = 'type_ratings_website';
	protected $dates = ['deleted_at'];
	protected $guarded = array();
	public static $rules = array();

}
