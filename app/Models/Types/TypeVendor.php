<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\Role;

class TypeVendor extends \Eloquent
{
	use SoftDeletes;

	protected $table = 'type_vendors';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function role()
	{
		return $this->belongsTo(Role::class, 'role_id');
	}
}