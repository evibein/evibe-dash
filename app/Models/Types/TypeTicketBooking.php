<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeTicketBooking extends Model
{
	use SoftDeletes;

	protected $table = 'type_ticket_booking';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function checkoutFields()
	{
		return $this->hasMany('App\Models\Util\CheckoutField', 'type_ticket_booking_id');
	}
}
