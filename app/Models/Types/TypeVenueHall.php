<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeVenueHall extends Eloquent {

	use SoftDeletes;
	
	protected $table = 'type_venue_hall';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];

}