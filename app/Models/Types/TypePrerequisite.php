<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePrerequisite extends Model
{
	use SoftDeletes;

	protected $table = 'type_prerequisites';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
