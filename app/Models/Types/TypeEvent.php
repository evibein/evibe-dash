<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeEvent extends Model
{

	use SoftDeletes;

	protected $table = 'type_event';
	protected $dates = ['deleted_at'];
	protected $guarded = [];
	public static $rules = [];


}