<?php

namespace App\Models\Coupon;

/**
 * Created by PhpStorm.
 * User: BlackOut
 * Date: 19-07-2018
 * Time: 10:35
 */
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model;

class LogReferralFriendActions extends Model
{
	use SoftDeletes;

	protected $table = 'log_referral_friend_actions';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}