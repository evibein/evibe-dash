<?php

namespace App\Models\Coupon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogCouponUsage extends Model
{
	use SoftDeletes;

	protected $table = 'log_coupon_usage';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}