<?php

namespace App\Models\Coupon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponRedeemed extends Model
{
	use SoftDeletes;

	protected $table = 'coupon_redeemed';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}