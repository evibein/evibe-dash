<?php

namespace App\Models\Coupon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponMappings extends Model
{
	use SoftDeletes;

	protected $table = 'coupon_mappings';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}