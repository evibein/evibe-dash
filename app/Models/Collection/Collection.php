<?php

namespace App\Models\Collection;

use App\Models\CollectionOption;
use App\Models\TypeEvent;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collection extends Model
{
	use SoftDeletes;

	protected $table = 'collection';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function options()
	{
		return $this->hasMany(CollectionOption::class, 'collection_id')->orderBy('priority', 'asc');
	}

	public function city()
	{
		return $this->belongsTo(\City::class, 'city_id');
	}

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'event_id');
	}

	public function createdBy()
	{
		return $this->belongsTo(User::class, 'created_by');
	}

	public function approvedBy()
	{
		return $this->belongsTo(User::class, 'approved_by');
	}

	private function getBasePath()
	{
		return config('evibe.gallery.host') . '/collection/' . $this->id . '/images/';
	}

	public function getProfilePic()
	{
		return $this->getBasePath() . $this->profile_image;
	}

	public function getCoverPic()
	{
		return $this->getBasePath() . $this->cover_image;
	}

	public function getLiveLink($noEvent = null)
	{
		if ($this->city)
		{
			$liveLink = config('evibe.live.host') . '/' . $this->city->url;

			if ($this->event && is_null($noEvent))
			{
				$liveLink .= '/' . $this->event->url;
			}

			return $liveLink . '/collections/' . $this->url;
		}

		return "#";
	}
}
