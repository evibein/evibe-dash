<?php

namespace App\Models\AvailCheck;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AvailabilityCheckGallery extends Model
{
	use SoftDeletes;

	protected $table = 'availability_check_gallery';
}