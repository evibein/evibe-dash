<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CollectionOption extends Model
{
	use SoftDeletes;

	protected $table = 'collection_option';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
