<?php

namespace App\Models\Partner;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerPackageOptionsValues extends Model
{
	use SoftDeletes;

	protected $table = 'partner_package_options_values';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}