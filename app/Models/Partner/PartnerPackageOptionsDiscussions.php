<?php

namespace App\Models\Partner;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerPackageOptionsDiscussions extends Model
{
	use SoftDeletes;

	protected $table = 'partner_package_options_messages';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}