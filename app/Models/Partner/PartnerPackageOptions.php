<?php

namespace App\Models\Partner;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerPackageOptions extends Model
{
	use SoftDeletes;

	protected $table = 'partner_package_options';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_id');
	}
}