<?php

namespace App\Models\Partner;

use App\Models\Types\TypeDocument;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerDocument extends Model
{
	use SoftDeletes;

	protected $table = 'partner_documents';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function getPath()
	{
		if ($this->partner_type_id == config('evibe.ticket_type.planners'))
		{
			$documentPath = config("evibe.gallery.host") . "/planner/" . $this->partner_id . "/documents/" . $this->original_file_name;
		}
		else
		{
			$documentPath = config("evibe.gallery.host") . "/venue/" . $this->partner_id . "/documents/" . "$this->original_file_name";
		}

		return $documentPath;
	}

	public function documentType()
	{
		return $this->belongsTo(TypeDocument::class, 'type_document_id');
	}

	public function getName()
	{
		$name = TypeDocument::where('id', '=', $this->type_document_id)
		                    ->value('name');

		return $name;
	}
}