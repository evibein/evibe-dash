<?php

namespace App\Models\Partner;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerPackageOptionsGallery extends Model
{
	use SoftDeletes;

	protected $table = 'partner_package_option_gallery';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}