<?php

namespace App\Models\Settlement;

use App\Models\BaseModel;

class ScheduleSettlementBooking extends BaseModel
{
	protected $table = 'schedule_settlement_bookings';
}