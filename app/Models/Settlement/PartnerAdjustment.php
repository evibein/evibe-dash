<?php

namespace App\Models\Settlement;

use App\Models\BaseModel;

class PartnerAdjustment extends BaseModel
{
	protected $table = 'partner_adjustments';
}