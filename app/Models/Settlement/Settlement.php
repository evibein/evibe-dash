<?php

namespace App\Models\Settlement;

use App\Models\BaseModel;

class Settlement extends BaseModel
{
	protected $table = 'settlements';
}