<?php

namespace App\Models\Settlement;

use App\Models\BaseModel;

class SettlementDetails extends BaseModel
{
	protected $table = 'settlement_details';
}