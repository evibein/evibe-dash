<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorFollowup extends Model
{
	use SoftDeletes;

	protected $table = 'vendor_followup';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo(User::class, 'handler_id');
	}
}
