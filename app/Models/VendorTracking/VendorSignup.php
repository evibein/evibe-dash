<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorSignup extends Model
{
	use SoftDeletes;

	protected $table = 'vendor_signup';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

	public function type()
	{
		if ($this->is_venue == 0)
		{
			return $this->belongsTo(\TypeVendor::class, 'type_id');
		}
		else
		{
			return $this->belongsTo(\TypeVenue::class, 'type_id');
		}

	}

	public function area()
	{
		return $this->belongsTo(\Area::class, 'area_id');
	}

	public function city()
	{
		return $this->belongsTo(\City::class, 'city_id');
	}

	public function handler()
	{
		return $this->belongsTo(User::class, 'handler_id');
	}

	public function status()
	{
		return $this->belongsTo(TypeVendorStatus::class, 'status_id');
	}

	public function source()
	{
		return $this->belongsTo('TypeTicketSource', 'source_id');
	}

	public function leadStatus()
	{
		return $this->belongsTo('TypeLeadStatus', 'lead_status_id');
	}

	// Mutators
	public function setPersonAttribute($value)
	{
		$this->attributes['person'] = ucwords(strtolower(trim($value)));
	}

	public function getPersonAttribute($value)
	{
		return ucwords($value);
	}
}
