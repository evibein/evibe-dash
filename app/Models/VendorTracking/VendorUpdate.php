<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorUpdate extends Model
{
	protected $table = 'vendor_update';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function status()
	{
		return $this->belongsTo(TypeVendorStatus::class,'status_id');
	}

	public function handler()
	{
		return $this->belongsTo(User::class,'handler_id');
	}
}
