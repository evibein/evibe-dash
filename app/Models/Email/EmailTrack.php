<?php

namespace App\Models\Email;

use \Illuminate\Database\Eloquent\Model;

class EmailTrack extends Model
{
	protected $table = 'sent_emails';
	protected $guarded = ['id'];
	public static $rules = [];
}