<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\VenueHallEvent;

class VenueHall extends Model
{
	use SoftDeletes;

	protected $table = 'venue_hall';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function venue()
	{
		return $this->belongsTo('Venue', 'venue_id');
	}

	public function provider()
	{
		return $this->belongsTo('Venue', 'venue_id');
	}

	public function type()
	{
		return $this->belongsTo('TypeVenueHall', 'type_id');
	}

	public function gallery()
	{
		return $this->hasMany(VenueHallGallery::class, 'venue_hall_id');
	}

	public function events()
	{
		return $this->hasMany(VenueHallEvent::class, 'venue_hall_id');
	}

	public function getProfilePic()
	{
		$host = config('evibe.gallery.host');
		$defaultHost = config('evibe.gallery.default_profile_pic.root');
		$vendorDefault = config('evibe.gallery.default_profile_pic.venue');

		$profilePic = $host . '/' . $defaultHost . '/' . $vendorDefault;

		// type_id = 4 = inside view
		$profilePicObj = VenueHallGallery::where('venue_hall_id', $this->id)
		                                 ->where('type_id', 4)
		                                 ->orderBy('updated_at', 'DESC')
		                                 ->first();

		if ($profilePicObj)
		{
			$profilePic = $profilePicObj->getPath();
		}
		else
		{
			// get from venue
			$profilePicObj = VenueGallery::where('venue_id', $this->venue->id)
			                             ->orderBy('updated_at', 'DESC')
			                             ->first();

			if ($profilePicObj)
			{
				$profilePic = $profilePicObj->getPath();
			}

		}

		return $profilePic;
	}

	public function getPriceWorth()
	{
		return 0;
	}

	public function getAvgExtRating()
	{
		$extRating = VenueExtRating::where('venue_id', $this->venue->id)->avg('rating_value');

		return round($extRating, 2);
	}

	public function getLiveLink()
	{
		$host = config('evibe.live.host');
		$cityUrl = $this->venue->city->url;
		$profileUrl = config('evibe.live.profile_url.venues');

		return $host . '/' . $cityUrl . '/' . $profileUrl . '/' . $this->url;
	}

}