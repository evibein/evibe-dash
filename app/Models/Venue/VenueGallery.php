<?php

/**
 * @author Anji <anji@evibe.in>
 * @since 10 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueGallery extends Eloquent {

	use SoftDeletes;

	protected $table = 'venue_gallery';
	protected $guarded = array('id');
	public static $rules = array();
	protected $dates = ['deleted_at'];
	
	public function venue()
	{
		return $this->belongsTo('Venue', 'venue_id');
	}

	public function getPath()
	{
		$host = Config::get('evibe.gallery.host');
		$venueId = $this->venue_id;

		$path = $host .'/venues/'. $venueId;
		$path .= '/images/'. $this->url;

		return $path;
	}

	public function getGalleryPic($id)
	{
		$imgUrl = array();
		
		// Venue picture
		$venueImgObj = VenueGallery::where('venue_id', $id)
							->orderBy('updated_at', 'DESC')
							->get();
		$venue_id=$id;
		
		$i=0;
		$venueArrayImg=array();
		if($venueImgObj){
			if(!empty($venueImgObj)){
				foreach($venueImgObj as $value)
				{
					$imgUrl[$i]['type'] = 'Venue';
					$imgUrl[$i]['url'] = AppUtil::getGalleryBaseUrl().'/venues/'.$venue_id.'/images/'.$value->url;
					$imgUrl[$i]['title'] = $value->title;
					$i++;
				}
			}
		}
		
		
		$venueHall = VenueHall::select('id')->where('venue_id', $id)
							->orderBy('updated_at', 'DESC')
							->get();
							
		
		$HallArrayImg=array();
		if($venueHall){
			if(!empty($venueHall)){
				foreach($venueHall as $value){
					$HallArrayImg[] = VenueHallGallery::select('venue_hall_id','url','id','title')->where('venue_hall_id', $value->id)
									->orderBy('updated_at', 'DESC')
									->get();

				}
			}
		}
		
		
		if($i==0){
			$j=0;
		}else{
			$j=$i;
		}
		
		if(!empty($HallArrayImg)){
			foreach($HallArrayImg as $valueArr){
				if(!empty($valueArr)){
					foreach($valueArr as $value){
						if(!empty($value)){
							if($value->id && $value->url){
								
								$imgUrl[$j]['type'] = 'hall';
								
								$imgUrl[$j]['url'] = AppUtil::getGalleryBaseUrl().'/venues/'.$venue_id.'/halls/'.$value->venue_hall_id.'/images/'.$value->url;
								
								$imgUrl[$j]['title'] = $value->title;
								$j++;
							}
							
						}
					}
					
					
				}
			}
		}

		return $imgUrl;
	}
}