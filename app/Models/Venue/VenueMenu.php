<?php

/**
 * @author Anji <anji@evibe.in>
 * @since 10 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueMenu extends Eloquent {

	use SoftDeletes;

	protected $table = 'venue_menu';
	protected $guarded = array('id');
	public static $rules = array();
	protected $dates = ['deleted_at'];
	
				
	public function getVenueMenuPic($venueId, $menuId)
	{
		$imgUrl ='';
		$imgUrl = AppUtil::getGalleryBaseUrl() .'/venues/hall_default/default.png';

		// profile picture
		$profileImgObj = VenueMenu::where('id', $menuId)->first();

		if ($profileImgObj)
		{
			$imgUrl = AppUtil::getGalleryBaseUrl() .'/venues/'. $venueId .'/menus/' .$profileImgObj->url;
		}

		return $imgUrl;
	}
}