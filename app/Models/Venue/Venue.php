<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
	use SoftDeletes;

	protected $table = 'venue';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function packages()
	{
		return $this->morphMany(VendorPackage::class, null, 'map_type_id', 'map_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_id');
	}

	public function city()
	{
		return $this->belongsTo('City', 'city_id');
	}

	public function area()
	{
		return $this->belongsTo('Area', 'area_id');
	}

	public function venueType()
	{
		return $this->belongsTo('TypeVenue', 'type_id');
	}

	public function venueHall()
	{
		return $this->hasMany('VenueHall', 'venue_id');
	}

	public function venueCuisine()
	{
		return $this->hasMany('venueCuisine', 'venue_id');
	}

	public function source()
	{
		return $this->belongsTo(\TypeTicketSource::class, 'source_id');
	}

	public function leadStatus()
	{
		return $this->belongsTo(\TypeLeadStatus::class, 'lead_status_id');
	}

	public function getAvgExtRating()
	{
		$extRating = VenueExtRating::where('venue_id', $this->id)->avg('rating_value');

		return round($extRating, 2);
	}

	public function getLocalLink()
	{
		return '/venues/view/' . $this->id;
	}

	public function bookings()
	{
		return $this->morphMany(TicketBooking::class, null, 'map_type_id', 'map_id');
	}

	public function scheduledBookings()
	{
		return $this->morphMany(\App\Models\Finance\ScheduleSettlementBookings::class, null, 'partner_type_id', 'partner_id');
	}
}