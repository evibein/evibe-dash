<?php

/**
 * @author Anji <anji@evibe.in>
 * @since 10 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class MenuGallery extends Eloquent {

	use SoftDeletes;

	protected $table = 'venue_gallery';
	protected $guarded = array('id');
	public static $rules = array();
	protected $dates = ['deleted_at'];
	
				
	public function getProfilePic($id)
	{
		$imgUrl ='';
		$imgUrl = AppUtil::getGalleryBaseUrl() .'/venues/menu/default/venue.png';
		
		// profile picture
		$profileImgObj = VenueGallery::where('venue_id', $id)
							->orderBy('updated_at', 'DESC')
							->first();

		if ($profileImgObj)
		{
			$imgUrl = AppUtil::getGalleryBaseUrl().'/venues/'.$id.'/menu/images/'.$profileImgObj->url;
		}
		
		return $imgUrl;
	}
}