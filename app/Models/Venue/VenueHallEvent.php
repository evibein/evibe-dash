<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VenueHallEvent extends Model
{
    use SoftDeletes;
    protected $table = 'venue_hall_event';
    protected $dates = ['deleted_at'];
    protected $guarded = array();
    public static $rules = array();

    public function Hall()
    {
        return $this->belongsTo(\VenueHall::class, 'venue_hall_id');
    }

    public function event()
    {
        return $this->belongsTo(TypeEvent::class, 'event_id');
    }
}