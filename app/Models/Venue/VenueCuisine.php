<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueCuisine extends \Eloquent {

	use SoftDeletes;
	
	protected $table = 'venue_cuisine';
	protected $dates = ['deleted_at'];
	protected $guarded = array('id');
	public static $rules = array();

	public function type()
	{
		return $this->belongsTo('TypeVenueCuisine', 'type_id');
	}

}