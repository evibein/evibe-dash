<?php

/**
 * @author Anji <anji@evibe.in>
 * @since 10 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueExtRating extends Eloquent {

	use SoftDeletes;

	protected $table = 'venue_ext_ratings';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];
	
	public function type()
	{
		return $this->belongsTo('TypeExtRating', 'type_ratings_website_id');
	}

}
