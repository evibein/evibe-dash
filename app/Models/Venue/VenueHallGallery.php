<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  10 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueHallGallery extends Eloquent
{

	use SoftDeletes;

	protected $table = 'venue_hall_gallery';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function hall()
	{
		return $this->belongsTo('VenueHall', 'venue_hall_id');
	}

	public function type()
	{
		return $this->belongsTo(\App\Models\TypeVenueGallery::class, 'type_id');
	}

	public function getPath()
	{
		$host = config('evibe.gallery.host');
		$hallId = $this->venue_hall_id;
		if(VenueHall::find($hallId))
		{
			$venueId = $this->hall->venue_id;

			$path = $host . '/venues/' . $venueId;
			$path .= '/halls/' . $hallId . '/images/' . $this->url;

			return $path;
		}
		else
		{
			return $path = '';//@todo No image found
		}
	}

	public function getHallGalleryPic($id, $venue_id)
	{
		$imgUrl = [];
		$venueArrayImg = [];

		// Venue picture
		$venueImgObj = VenueGallery::where('venue_id', $venue_id)
		                           ->orderBy('updated_at', 'DESC')
		                           ->get();

		$i = 0;
		if ($venueImgObj)
		{
			if (!empty($venueImgObj))
			{
				foreach ($venueImgObj as $value)
				{
					$imgUrl[$i]['type'] = 'venue';
					$imgUrl[$i]['url'] = AppUtil::getGalleryBaseUrl() . '/venues/' . $venue_id . '/images/' . $value->url;
					$imgUrl[$i]['title'] = $value->title;
					$i++;
				}
			}
		}

		if ($i == 0)
		{
			$j = 0;
		}
		else
		{
			$j = $i;
		}
		// Hall picture
		$HallArrayImg = VenueHallGallery::select('url', 'id', 'title')->where('venue_hall_id', $id)
		                                ->orderBy('updated_at', 'DESC')
		                                ->get();

		if (!empty($HallArrayImg))
		{
			foreach ($HallArrayImg as $valueArr)
			{
				if (!empty($valueArr))
				{
					if ($valueArr->id && $valueArr->url)
					{
						$imgUrl[$j]['type'] = 'hall';
						$imgUrl[$j]['url'] = AppUtil::getGalleryBaseUrl() . '/venues/' . $venue_id . '/halls/' . $id . '/images/' . $valueArr->url;
						$imgUrl[$j]['title'] = $valueArr->title;
					}

				}
				$j++;
			}
		}

		return $imgUrl;
	}
}