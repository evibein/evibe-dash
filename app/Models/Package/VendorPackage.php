<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class VendorPackage extends Model
{
	use SoftDeletes;

	protected $table = 'planner_package';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function provider()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}

	public function city()
	{
		return $this->belongsTo('City', 'city_id');
	}

	public function area()
	{
		return $this->belongsTo('Area', 'area_id');
	}

	public function venueType()
	{
		return $this->belongsTo('TypeVenue', 'type_venue_id');
	}

	public function gallery()
	{
		return $this->hasMany('PackageGallery', 'planner_package_id');
	}

	public function services()
	{
		return $this->hasMany('PackageService', 'planner_package_id');
	}

	public function faqs()
	{
		return $this->hasMany('PlannerPackageFaqs', 'planner_package_id');
	}

	public function tags()
	{
		return $this->belongsToMany('Tags', 'planner_package_tags', 'planner_package_id', 'tile_tag_id')
		            ->wherePivot('deleted_at', null);
	}

	public function property()
	{
		return $this->belongsTo(TypeVenueHall::class, 'hall_type_id');
	}

	public function reviews()
	{
		return $this->hasMany('PlannerPackageReviews', 'planner_package_id');
	}

	public function event()
	{
		return $this->belongsTo(\App\Models\TypeEvent::class, 'event_id');
	}

	public function getTagsString($type = 0)
	{
		$tagsStr = '';
		$tagList = [];

		foreach ($this->tags as $packageTag)
		{
			if (!in_array($packageTag->name, $tagList))
			{
				array_push($tagList, $packageTag->name);
				$tagsStr .= $packageTag->name . ', ';
			}
		}

		$tagsStr = substr($tagsStr, 0, -2); // remove trailing ", "

		return $tagsStr;
	}

	public function getTags()
	{
		$tags = [];
		$packageTagsList = $this->tags;

		// available tags
		$availableTags = [];
		$allTags = Tags::all();

		foreach ($allTags as $tag)
		{
			$availableTags[$tag->id] = [
				'name'      => $tag->name,
				'parent_id' => $tag->parent_id,
			];
		}

		foreach ($packageTagsList as $packageTag)
		{
			$currentTag = $availableTags[$packageTag->pivot->tile_tag_id];
			$tagString = "";

			if ($currentTag['parent_id'])
			{
				$parentId = $currentTag['parent_id'];
				$parentTag = $availableTags[$parentId];
				if ($parentTag)
				{
					$tagString .= $parentTag['name'] . " :: ";
				}
			}

			$tagString .= $currentTag['name'];

			if ($tagString)
			{
				$tags[$packageTag->pivot->tile_tag_id] = $tagString;
			}
		}

		return $tags;
	}

	public function itinerary()
	{
		return $this->hasMany('PlannerPackageItinerary', 'planner_package_id');
	}

	public function getPriceWorth()
	{
		$worth = $this->price_worth;
		if ($this->price_worth <= $this->price)
		{
			$worth = 0;
		}

		return $worth;
	}

	public function getProfilePic()
	{
		$host = config('evibe.gallery.host');
		$defaultHost = config('evibe.gallery.default_profile_pic.root');
		$packageDefault = config('evibe.gallery.default_profile_pic.package');

		$profilePic = $host . '/' . $defaultHost . '/' . $packageDefault;

		$profilePicObj = PackageGallery::where('planner_package_id', $this->id)
		                               ->orderBy('is_profile', 'DESC')
		                               ->orderBy('updated_at', 'DESC')
		                               ->first();

		if ($profilePicObj)
		{
			if ($this->map_type_id == config('evibe.ticket_type.venues'))
			{
				$profilePic = $host . '/venues/' . $this->provider->id . '/images/packages/' . $this->id . '/' . $profilePicObj->url;
			}
			else
			{
				$profilePic = $host . '/planner/' . $this->provider->id . '/images';
				$profilePic .= '/packages/' . $this->id . '/' . str_replace(' ', '', $profilePicObj->url);
			}
		}

		return $profilePic;
	}

	public function getLiveLink()
	{
		$liveHost = config('evibe.live.host');
		$profileUrl = config('evibe.live.profile_url.packages');
		$cityUrl = $this->provider->city->url;

		return $liveHost . '/' . $cityUrl . '/' . $profileUrl . '/' . $this->url;
	}

	public function firstEvent()
	{
		$event = '';
		$packageEvent = \App\Models\VendorPackageEvent::where('package_id', $this->id)->first();
		if ($packageEvent)
		{
			$event = \App\Models\TypeEvent::find($packageEvent->event_id);

		}

		return $event;
	}

	public function getLink()
	{
		$route = route('package.view', $this->id);

		return $route;
	}

}