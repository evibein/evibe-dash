<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorPackageEvent extends Model
{
    use SoftDeletes;

    protected $table = 'planner_package_event';
    protected $dates = ['deleted_at'];
    protected $guarded = ['id'];
    public static $rules = [];

    public function event()
    {
        return $this->belongsTo(TypeEvent::class, 'event_id');
    }

    public function package()
    {
        return $this->belongsTo(\VendorPackage::class, 'package_id');
    }
}
