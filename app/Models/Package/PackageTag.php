<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageTag extends Model
{
	use SoftDeletes;

	protected $table = 'planner_package_tags';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function tag()
	{
		return $this->belongsTo('Tags', 'tile_tag_id');
	}
}