<?php
/**
 * @author Harish Annavajjala <harish.annavajjala@evibe.in>
 * @since 05-Jan-2015
 */
use Illuminate\Database\Eloquent\SoftDeletes;

class PlannerPackageItinerary extends \Eloquent {
	
	use SoftDeletes;
	protected $table = 'planner_package_itinerary';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];
	protected $fillable = [];
}


