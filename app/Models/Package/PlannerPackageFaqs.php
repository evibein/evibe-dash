<?php
/**
 * @author Harish Annavajjala <harish.annavajjala@evibe.in>
 * @since 03-Jan-2015
 */
use Illuminate\Database\Eloquent\SoftDeletes;

class PlannerPackageFaqs extends \Eloquent {

	use SoftDeletes;

	protected $table = 'planner_package_faqs';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];
	
}
