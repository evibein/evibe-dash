<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class PackageGallery extends Eloquent
{
	use SoftDeletes;

	protected $table = 'planner_package_gallery';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function package()
	{
		return $this->belongsTo(VendorPackage::class, 'planner_package_id');
	}

	public function getGalleryPath()
	{
		$url = config('evibe.gallery.root');
		$package = $this->package;

		if ($package && $package->provider)
		{
			if ($package->map_type_id == config('evibe.ticket_type.venues'))
			{
				$url .= '/venues/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			elseif ($package->map_type_id == config('evibe.ticket_type.planners'))
			{
				$url .= '/planner/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			elseif ($package->map_type_id == config('evibe.ticket_type.artists'))
			{
				$url .= '/planner/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			else
			{
				$url = false;
			}

			if ($url === false)
			{
				return "";
			}
			else
			{
				return $url . '/' . $this->url;
			}
		}
		else
		{
			return null;
		}
	}

	public function getGalleryUrl()
	{
		$url = config('evibe.gallery.host');
		$package = $this->package;

		if ($package && $package->provider)
		{
			if ($package->map_type_id == config('evibe.ticket_type.venues'))
			{
				$url .= '/venues/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			elseif ($package->map_type_id == config('evibe.ticket_type.planners'))
			{
				$url .= '/planner/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			elseif ($package->map_type_id == config('evibe.ticket_type.artists'))
			{
				$url .= '/planner/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			else
			{
				$url = false;
			}

			if ($url === false)
			{
				return "";
			}
			else
			{
				return $url . '/' . $this->url;
			}
		}
		else
		{
			return null;
		}
	}
}