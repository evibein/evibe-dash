<?php

/**
 * @author Harish Annavajjala <harish.annavajjala@evibe.in>
 * @since 02-Jan-2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class PlannerPackageReviews extends \Eloquent {

	use SoftDeletes;
	protected $table = 'planner_package_reviews';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];
	protected $fillable = [];

	public function reviewReplies()
	{
		return $this->hasOne('PlannerPackageReviewReply', 'planner_package_reviews_id');
	}

	public function tempReviewReplies()
	{
		return $this->hasOne('TempPlannerPackageReviewReply', 'planner_package_reviews_id');
	}

}