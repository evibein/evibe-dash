<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PackageService extends Model
{
	use SoftDeletes;

	protected $table = 'planner_package_service';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function service()
	{
		return $this->belongsTo('Service', 'service_id');
	}
}