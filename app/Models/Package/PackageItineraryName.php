<?php

namespace App\Models\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageItineraryName extends Model
{
	use SoftDeletes;
	protected $table = 'package_itinerary_name';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function itineraries()
	{
		return $this->hasMany(\PlannerPackageItinerary::class, 'itinerary_id');
	}
}
