<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Eloquent {

	use SoftDeletes;

	protected $table = 'services';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

}