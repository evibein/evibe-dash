<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceEvent extends Model
{
	use SoftDeletes;

	protected $table = 'service_event';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function event()
	{
		return $this->belongsTo(TypeEvent::class,'type_event_id');
	}

	public function services()
	{
		return $this->hasMany(TypeServices::class,'id');
	}

}
