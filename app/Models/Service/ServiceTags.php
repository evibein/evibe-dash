<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceTags extends Model
{
	use SoftDeletes;

	protected $table = 'service_tags';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function tags()
	{
		return $this->belongsTo('Tags', 'tag_id');
	}
}