<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Evibe\Facades\AppUtilFacade as AppUtil;

class ServiceGallery extends Model
{
	use SoftDeletes;

	protected $table = 'service_gallery';
	protected $dates = ['deleted_at'];
	protected $guarded = [];
	public static $rules = [];

	public function service()
	{
		return $this->belongsTo(TypeServices::class, 'type_service_id');
	}

	public function getLink($type = null)
	{
		$service = $this->service;
		if ($service)
		{
			$imgLink = AppUtil::getGalleryBaseUrl();
			$imgLink .= '/services/' . $service->id . '/images/';
			$validTypes = ["profile", "results", "thumbs"];

			if (!is_null($type) && in_array($type, $validTypes))
			{
				$imgLink .= "/$type/";
			}

			$imgLink .= $this->url;
		}
		else
		{
			$imgLink = ''; // @todo Default id
		}

		return $imgLink;
	}
}
