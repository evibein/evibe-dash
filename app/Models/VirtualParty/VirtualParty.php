<?php

namespace App\Models\VirtualParty;

use App\Models\BaseModel;

class VirtualParty extends BaseModel
{
	protected $table = 'virtual_party';
}