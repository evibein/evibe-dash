<?php

namespace App\Models\VirtualParty;

use App\Models\BaseModel;

class VirtualPartyGuestWish extends BaseModel
{
	protected $table = 'virtual_party_guest_wish';
}