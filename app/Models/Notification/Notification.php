<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends \Eloquent
{

	use SoftDeletes;

	protected $table = 'notifications';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function ticketMapping()
	{
		return $this->belongsTo('TicketMapping', 'ticket_mapping_id');
	}

	public function handler()
	{
		return $this->belongsTo('App\Models\User', 'created_by');
	}

	public function attachments()
	{
		return $this->hasMany('NotificationAttachment', 'notification_id');
	}

	public function replyAttachments()
	{
		$attachments = NotificationAttachment::where('notification_id', $this->id)
		                                     ->where('is_reply', 1)
		                                     ->get();

		return $attachments;
	}

	public function notificationType()
	{
		return $this->belongsTo('TypeNotofication', 'type_id');
	}

	public function getPartner()
	{
		$partner = Vendor::where('user_id', $this->created_for)->first();
		if(!$partner)
		{
			$partner = Venue::where('user_id', $this->created_for)->first();
		}

		return $partner;
	}
}