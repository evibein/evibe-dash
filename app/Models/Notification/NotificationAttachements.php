<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationAttachements extends Eloquent {

	use SoftDeletes;
	
	protected $table = 'notification_attachments';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];

	public function notification()
	{
		return $this->belongsTo('Notification', 'notification_id');
	}

}