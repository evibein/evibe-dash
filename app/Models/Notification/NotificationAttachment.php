<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationAttachment extends \Eloquent {
	
	use SoftDeletes;
	
	protected $table = 'notification_attachments';
	protected $guarded = array('id');
	public static $rules = array();
	protected $dates = ['deleted_at'];

	public function scopeForReply($query)
	{
		return $query->where('is_reply', 1);
	}

}