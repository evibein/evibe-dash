<?php

namespace App\Models\Ticket;

use App\Models\User;
use App\Models\Util\TicketReviewQuestionAnswer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketReview extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_review';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo(User::class, 'accepted_by');
	}

	public function extraAnswers()
	{
		return $this->hasMany(TicketReviewQuestionAnswer::class, 'review_id');
	}

	public function answerIds()
	{
		return \GuzzleHttp\json_encode($this->extraAnswers->pluck('option_id')->toArray());
	}
}
