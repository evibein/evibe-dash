<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CheckAvailability;
use App\Models\Ticket\TicketReview;
use \App\Models\TypeEvent;
use \App\Models\TypeSlot;
use \App\Models\Ticket\TicketGallery;
use \App\Models\Types\TypeCancellationReason;
use \App\Models\TicketFollowup;

class Ticket extends Model
{
	use SoftDeletes;

	protected $table = 'ticket';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo('App\Models\User', 'handler_id');
	}

	public function status()
	{
		return $this->belongsTo('TypeTicketStatus', 'status_id');
	}

	public function source()
	{
		return $this->belongsTo('TypeTicketSource', 'source_id');
	}

	public function enquirySource()
	{
		return $this->belongsTo(\App\Models\Types\TypeEnquirySource::class, "enquiry_source_id");
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function area()
	{
		return $this->belongsTo('Area', 'area_id');
	}

	public function mapType()
	{
		return $this->belongsTo('TypeTicket', 'type_ticket_id');
	}

	public function bookings()
	{
		return $this->hasMany('TicketBooking', 'ticket_id');
	}

	public function updates()
	{
		return $this->hasMany(TicketUpdate::class, 'ticket_id');
	}

	public function activeFollowup()
	{
		return $this->hasOne(TicketFollowup::class, 'ticket_id')->whereNull('is_complete');
	}

	public function archiveFollowups()
	{
		return $this->hasMany(TicketFollowup::class, 'ticket_id')->where('is_complete', 1);
	}

	public function mappings()
	{
		return $this->hasMany('TicketMapping', 'ticket_id');
	}

	public function enquiries()
	{
		return $this->hasMany('TicketMapping', 'ticket_id')->whereNotNull('enquiry_created_at');
	}

	public function ticketReview()
	{
		return $this->hasOne(TicketReview::class, 'ticket_id');
	}

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'event_id');
	}

	public function slot()
	{
		return $this->belongsTo(TypeSlot::class, 'type_slot_id');
	}

	public function galleries()
	{
		return $this->hasMany(TicketGallery::class, 'ticket_id');
	}

	public function leadStatus()
	{
		return $this->belongsTo('TypeLeadStatus', 'lead_status_id');
	}

	public function isSuccessful()
	{
		$validStatuses = [config('evibe.status.confirmed'), config('evibe.status.booked')];

		return in_array($this->status_id, $validStatuses);
	}

	public function ticketRating()
	{
		$review = $this->ticketReview;
		$rating = $review->customer_care + $review->ease_of_booking + $review->price_for_quality;
		$rating = round($rating / 3, 2);

		return $rating;
	}

	public function getSuccessEmailType()
	{
		$res = "Success";

		// 1: confirmed; 2: booked
		if (!is_null($this->success_email_type))
		{
			$res = "Order under process";

			if ($this->success_email_type == config('evibe.success_type.booked'))
			{
				$res = "Booked receipt";
			}
		}

		return $res;
	}

	public function quotes()
	{
		return $this->hasMany('TicketQuote', 'ticket_id');
	}

	public function statusUpdates()
	{
		return $this->hasMany('TicketUpdate', 'ticket_id');
	}

	public function checkAvailability()
	{
		return $this->hasMany(CheckAvailability::class, 'ticket_id');
	}

	public function isVenueMappings()
	{
		$count = false;
		$venueMapCount = TicketMapping::where(['map_type_id' => config('evibe.ticket_type.venues'), 'ticket_id' => $this->id])->count();

		if ($venueMapCount > 0)
		{
			$count = true;
		}

		return $count;

	}

	public function lastAvailability()
	{
		return $this->hasMany(CheckAvailability::class, 'ticket_id')
		            ->orderBy('created_at', 'DESC')
		            ->first();
	}

	public function isExchangeContactNonAdmin()
	{
		$count = false;
		$venueMapCount = TicketMapping::where(['ticket_id' => $this->id])
		                              ->whereNull('contact_exchanged_at')->count();

		if ($venueMapCount > 0)
		{
			$count = true;
		}

		return $count;
	}

	public function ticketUpdate()
	{
		return $this->hasMany(TicketUpdate::class, "ticket_id");
	}

	public function lastTicketUpdate()
	{
		return $this->hasMany(TicketUpdate::class, 'ticket_id')->orderBy('updated_at', 'DESC')->first();
	}

	// Mutators

	public function setNameAttribute($value)
	{
		$this->attributes['name'] = ucwords(strtolower(trim($value)));
	}

	public function getNameAttribute($value)
	{
		return ucwords($value);
	}

	public function showEnquiry()
	{
		return $this->hasMany(TicketMapping::class, 'ticket_id')->whereNotNull('enquiry_created_at');
	}

	public function typeCancellation()
	{
		return $this->belongsTo(TypeCancellationReason::class, 'type_cancellation_id');
	}

	public function cancellationReason()
	{
		$reason = "";

		if ($this->typeCancellation)
		{
			$reason = $this->typeCancellation->message;
		}
		elseif ($this->cancellation_reason)
		{
			$reason = $this->cancellation_reason;
		}

		return $reason;
	}

	public function deliveryCount($mapId, $mapTypeId)
	{
		$count = 0;
		$bookings = $this->paidBookings($mapId, $mapTypeId);
		foreach ($bookings as $booking)
		{
			if ($booking->delivery_created_at)
			{
				$count += $booking->deliveryGallery->count();
			}
		}

		return $count;
	}

	public function paidBookings($mapId = null, $mapTypeId = null)
	{
		$bookings = $this->hasMany(TicketBooking::class, 'ticket_id')->whereNotNull('delivery_created_at')->where('is_advance_paid', 1);
		if ($mapId && $mapTypeId)
		{
			$bookings = $bookings->where('map_id', $mapId)->where('map_type_id', $mapTypeId);
		}

		return $bookings->get();
	}

	public function isTicketBookingVisible()
	{
		$isVisible = true;

		if ($this->status_id == config('evibe.status.cancelled'))
		{
			$isVisible = false;
		}
		elseif (!$this->bookings->count())
		{
			$isVisible = false;
		}

		return $isVisible;
	}

	public function isTicketBookingFinanceVisible()
	{
		$isVisible = true;

		if (!$this->bookings->count())
		{
			$isVisible = false;
		}

		return $isVisible;
	}

	public function isTicketBenefitServicesVisible()
	{
		$isVisible = false;

		if ($this->e_invite || $this->e_thank_you_card)
		{
			$isVisible = true;
		}

		return $isVisible;
	}

	public function isTicketRecommendationsVisible()
	{
		$isVisible = true;

		$user = \Illuminate\Support\Facades\Auth::user();
		if ($user->role_id == config('evibe.roles.accounts'))
		{
			$isVisible = false;
		}

		return $isVisible;
	}

	public function isTicketAvailChecksVisible()
	{
		$isVisible = true;

		$user = \Illuminate\Support\Facades\Auth::user();
		if ($user->role_id == config('evibe.roles.accounts'))
		{
			$isVisible = false;
		}

		return $isVisible;
	}

	public function isTicketMappingsVisible()
	{
		$isVisible = true;

		$user = \Illuminate\Support\Facades\Auth::user();
		if ($user->role_id == config('evibe.roles.accounts'))
		{
			$isVisible = false;
		}

		return $isVisible;
	}
}