<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketFalloutFeedback extends Model
{
	use SoftDeletes;
	protected $table = 'ticket_fallout_feedback';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
