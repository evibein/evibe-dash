<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketUnique extends \Eloquent
{
	use SoftDeletes;

	protected $table = 'ticket_unique';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}