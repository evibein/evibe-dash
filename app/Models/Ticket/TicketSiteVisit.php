<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketSiteVisit extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_site_visit';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function ticket()
	{
		return $this->belongsTo(\Ticket::class, 'ticket_id');
	}
}