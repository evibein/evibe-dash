<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;

class RefundGallery extends Model
{
	protected $table = 'refund_gallery';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}
