<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\SoftDeletes;

class LogTicketReminders extends \Eloquent
{
	use SoftDeletes;

	protected $table = 'log_ticket_reminders';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}