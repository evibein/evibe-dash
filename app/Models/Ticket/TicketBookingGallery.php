<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketBookingGallery extends Model
{
	use SoftDeletes;
	protected $table = 'ticket_booking_gallery';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function ticketBooking()
	{
		return $this->belongsTo(\TicketBooking::class, 'ticket_booking_id');
	}

	public function getImageBasePath()
	{
		$ticketBookingId = $this->ticketBooking->id;
		$ticketId = $this->ticketBooking->ticket->id;

		$imagePath = config('evibe.gallery.host') . '/ticket/' . $ticketId . '/' . $ticketBookingId . '/';

		return $imagePath;
	}

	public function getOriginalImagePath()
	{
		if ($this->type == 1)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . $this->url;
	}

	public function getResultsImagePath()
	{
		if ($this->type == 1)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . 'results/' . $this->url;
	}

	public function getProfileImagePath()
	{
		if ($this->type == 1)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . 'profile/' . $this->url;
	}

	public function getThumbsImagePath()
	{
		if ($this->type == 1)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . 'thumbs/' . $this->url;
	}

	public function getAllImageFilePath()
	{
		$parentDir = config('evibe.gallery.root');
		$basePath = $parentDir . '/ticket/' . $this->ticketBooking->ticket->id . '/' . $this->ticketBooking->id . '/';

		$allPaths = [
			$basePath . $this->url,
			$basePath . 'profile/' . $this->url,
			$basePath . 'results/' . $this->url,
			$basePath . 'thumbs/' . $this->url,
		];

		return $allPaths;
	}

}
