<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CheckAvailability;
use Evibe\Facades\AppUtilFacade as AppUtil;
use \Illuminate\Database\Eloquent\Model;
use \App\Models\Util\CheckoutField;
use \App\Models\Types\TypeTicketBooking;

class TicketMapping extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_mapping';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo('App\Models\User', 'handler_id');
	}

	public function mapType()
	{
		return $this->belongsTo('TypeTicket', 'map_type_id');
	}

	public function ticket()
	{
		return $this->belongsTo('Ticket', 'ticket_id');
	}

	public function booking()
	{
		return $this->hasOne('TicketBooking', 'ticket_mapping_id');
	}

	public function typeEnquiry()
	{
		return $this->belongsTo(TypeNotification::class, 'enquiry_type_id');
	}

	public function type()
	{
		return $this->belongsTo(TypeTicketBooking::class,'type_id');
	}

	/**
	 * @since 15 Jan 2015
	 */
	public function getMappedValues()
	{
		return AppUtil::fillMappingValues([
			                                  'isCollection' => false,
			                                  'mapTypeId'    => $this->map_type_id,
			                                  'mapId'        => $this->map_id
		                                  ]);
	}

	public function getMappedNotifications($mapping_id)
	{
		$notification = Notification::where('ticket_mapping_id', $mapping_id)
		                            ->orderBy('created_at', 'DESC')
		                            ->get();

		foreach ($notification as $value)
		{
			$attachments = NotificationAttachements::where('notification_id', $value->id)->get();
			$value->attachements = $attachments;
		}

		return $notification;
	}

	public function notifications()
	{
		return $this->hasMany(Notification::class, 'ticket_mapping_id')
		            ->orderBy('created_at', 'DESC');
	}

	public function lastAvailability()
	{
		return $this->hasMany(CheckAvailability::class, 'ticket_mapping_id')
		            ->orderBy('created_at', 'DESC')
		            ->first();
	}

	public function checkoutFields()
	{
		$eventId = $this->ticket->event_id ? $this->ticket->event_id : 1;

		return $this->hasMany(CheckoutField::class, 'type_ticket_booking_id', 'type_id')
		            ->where(function ($query) use ($eventId)
		            {
			            $query->where('event_id', $eventId)->orWhereNull('event_id');
		            });
	}

	public function gallery()
	{
		return $this->hasMany(\App\Models\Ticket\TicketMappingGallery::class, 'ticket_mapping_id');
	}

	public function enquiry()
	{
		return $this->hasOne(Notification::class, 'ticket_mapping_id')->orderBy('created_at', 'DESC');
	}

}