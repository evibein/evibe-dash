<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogBookingFinance extends Model
{
	use SoftDeletes;

	protected $table = 'log_booking_finance';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo(\App\Models\User::class, 'handler_id');
	}
}