<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketThankYouCard extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_thankyou_cards';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
}