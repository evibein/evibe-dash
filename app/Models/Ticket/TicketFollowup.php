<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketFollowup extends Model
{
	use SoftDeletes;
	protected $table = 'ticket_followups';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo(User::class, 'handler_id');
	}

	public function ticket()
	{
		return $this->belongsTo(\Ticket::class, 'ticket_id');
	}
}
