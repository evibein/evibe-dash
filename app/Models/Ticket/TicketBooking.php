<?php

use \App\Models\Types\TypeTicketBooking;
use \App\Models\Types\TypeBookingConcept;
use \App\Models\Ticket\TicketBookingGallery;
use \App\Models\Util\CheckoutField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\Vendor\DeliveryGallery;

class TicketBooking extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_bookings';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function mapping()
	{
		return $this->belongsTo('TicketMapping', 'ticket_mapping_id');
	}

	public function ticket()
	{
		return $this->belongsTo('Ticket', 'ticket_id');
	}

	public function provider()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}

	public function oldProvider()
	{
		return $this->morphTo(null, 'old_partner_type_id', 'old_partner_id');
	}

	public function vendorReview()
	{
		return $this->hasOne(Review::class, 'ticket_booking_id');
	}

	public function ticketBookingType()
	{
		return $this->belongsTo(TypeTicketBooking::class, 'type_ticket_booking_id');
	}

	public function gallery()
	{
		return $this->hasMany(TicketBookingGallery::class, 'ticket_booking_id');
	}

	public function deliveryGallery()
	{
		return $this->hasMany(DeliveryGallery::class, 'ticket_booking_id');
	}

	public function bookingConcept()
	{
		return $this->belongsTo(TypeBookingConcept::class, 'type_booking_concept_id');
	}

	public function checkoutFields()
	{
		$eventId = $this->ticket->event_id ? $this->ticket->event_id : 1;

		$parentEvent = \App\Models\TypeEvent::select('parent_id')
		                                    ->where('id', $eventId)
		                                    ->first();

		if ($parentEvent && $parentEvent->parent_id)
		{
			$eventId = $parentEvent->parent_id;
		}

		return $this->hasMany(CheckoutField::class, 'type_ticket_booking_id', 'type_ticket_booking_id')
		            ->where(function ($query) use ($eventId) {
			            $query->where('event_id', $eventId)->orWhereNull('event_id');
		            });
	}

	public function checkVendorReview()
	{
		$isAddReview = false;
		$bookingIds = $this->ticket->bookings->pluck('id');

		if ($this->is_advance_paid == 1)
		{
			$vendorReview = Review::where('map_type_id', $this->map_type_id)
			                      ->where('map_id', $this->map_id)
			                      ->whereIn('ticket_booking_id', $bookingIds)
			                      ->whereNull('deleted_at')
			                      ->first();
			if (!$vendorReview)
			{
				$isAddReview = true;
			}
		}

		return $isAddReview;
	}

	public function checkReviewType()
	{
		if ($this->mapping->map_type_id == config('evibe.ticket_type.venues') ||
			$this->mapping->map_type_id == config('evibe.ticket_type.cakes') ||
			($this->mapping->map_type_id == config('evibe.ticket_type.packages') && $this->map_type_id == config('evibe.ticket_type.venues'))
		)
		{
			$type = 'venueCake';

		}
		//for non-venue & non-cake
		else
		{
			$type = 'vendor';
		}

		return $type;
	}

	public function orderTrackingHandler()
	{
		return $this->belongsTo(\App\Models\User::class, 'order_tracking_handler_id');
	}

	public function orderTrackingUpdatedTime()
	{
		return $this->hasMany(\App\Models\Tracking\OrderTracking::class, 'booking_id');
	}

	public function refundHandler()
	{
		return $this->belongsTo(\App\Models\User::class, 'refund_handler_id');
	}

	public function refundGallery()
	{
		return $this->hasMany(\App\Models\Ticket\RefundGallery::class, 'ticket_booking_id');
	}

	public function logBookingFinance()
	{
		return $this->hasMany(\App\Models\Ticket\LogBookingFinance::class, 'ticket_booking_id');
	}
}