<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketPostBookingRequests extends Model
{
	use SoftDeletes;

	protected $table = 'post_booking_requests';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}