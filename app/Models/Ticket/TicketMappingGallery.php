<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketMappingGallery extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_mapping_gallery';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function ticketMapping()
	{
		return $this->belongsTo(\TicketMapping::class, 'ticket_mapping_id');
	}

	public function getImageBasePath()
	{
		$ticketMappingId = $this->ticketMapping->id;
		$ticketId = $this->ticketMapping->ticket->id;

		$imagePath = config('evibe.gallery.host') . '/ticket/' . $ticketId . '/mapping/' . $ticketMappingId . '/images/';

		return $imagePath;
	}

	public function getOriginalImagePath()
	{
		if ($this->type_id == 2)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . $this->url;
	}

	public function getResultsImagePath()
	{
		if ($this->type_id == 2)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . 'results/' . $this->url;
	}

	public function getProfileImagePath()
	{
		if ($this->type_id == 2)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . 'profile/' . $this->url;
	}

	public function getThumbsImagePath()
	{
		if ($this->type_id == 2)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . 'thumbs/' . $this->url;
	}

	public function getAllImageFilePath()
	{
		$parentDir = config('evibe.gallery.root');
		$basePath = $parentDir . '/ticket/' . $this->ticketMapping->ticket->id . '/' . $this->ticketMapping->id . '/';

		$allPaths = [
			$basePath . $this->url,
			$basePath . 'profile/' . $this->url,
			$basePath . 'results/' . $this->url,
			$basePath . 'thumbs/' . $this->url,
		];

		return $allPaths;
	}
}
