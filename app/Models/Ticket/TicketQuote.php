<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketQuote extends \Eloquent {

	protected $fillable = [];

	protected $table = 'ticket_quote';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo('App\Models\User', 'handler_id');
	}

	public function mapType()
	{
		return $this->belongsTo('TypeTicket', 'map_type_id');
	}

	public function ticket()
	{
		return $this->belongsTo('Ticket', 'ticket_id');
	}
}