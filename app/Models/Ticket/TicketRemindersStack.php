<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketRemindersStack extends \Eloquent
{
	use SoftDeletes;

	protected $table = 'ticket_reminders_stack';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}