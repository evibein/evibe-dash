<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketVendorExchange extends \Eloquent {

	use SoftDeletes;
	
	protected $table = 'ticket_vendor_exchange';
	protected $guarded = array();
	public static $rules = array();
	protected $dates = ['deleted_at'];

	public function vendor()
	{
		return $this->belongsTo('Vendor', 'vendor_id');
	}
}