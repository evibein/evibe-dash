<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketUpdate extends Model
{

	use SoftDeletes;

	protected $table = 'ticket_update';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo('App\Models\User', 'handler_id');
	}

	public function status()
	{
		return $this->belongsTo('TypeTicketStatus', 'status_id');
	}

	public function ticket()
	{
		return $this->belongsTo('Ticket', 'ticket_id');
	}
}