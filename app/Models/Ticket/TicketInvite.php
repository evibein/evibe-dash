<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketInvite extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_invites';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
}