<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketMappingFieldValue extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_mapping_field_value';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
