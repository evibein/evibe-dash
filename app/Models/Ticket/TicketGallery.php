<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketGallery extends Model
{
	use SoftDeletes;
	protected $table = 'ticket_gallery';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function imageLink()
	{
		return config('evibe.gallery.host') . '/ticket/' . $this->ticket_id . '/images/' . $this->url;
	}
}