<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketReviewQuestion extends Model
{
	use SoftDeletes;

	protected $table = 'evibe_review_question';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function options()
	{
		return $this->hasMany(TicketReviewQuestionOption::class, 'question_id');
	}

	public function stringifyAnswer($reviewId)
	{
		$options = $this->options;
		$string = "";
		foreach ($options as $option)
		{
			$answer = TicketReviewQuestionAnswer::where('review_id', $reviewId)->where('option_id', $option->id)->count();
			if ($answer)
			{
				$string .= $option->text . ', ';
			}
		}

		return rtrim($string, ', ');
	}
}
