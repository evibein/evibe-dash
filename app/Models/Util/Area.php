<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Eloquent {

	use SoftDeletes;

	protected $table = 'area';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function getNearbyAreas()
	{
		$nearByAreaIds = array();
		$nearbyAreas = AreaNearby::where('area1', $this->id)
						->orWhere('area2', $this->id)
						->select('area1', 'area2')
						->get();

		foreach ($nearbyAreas as $nearbyArea)
		{
			$area1 = $nearbyArea->area1;
			$area2 = $nearbyArea->area2;

			if ($area1 != $this->id) array_push($nearByAreaIds, $area1);
			if ($area2 != $this->id) array_push($nearByAreaIds, $area2);
		}

		return array_unique($nearByAreaIds);
	}

	public function city()
	{
		return $this->belongsTo('City', 'city_id');
	}
	
}