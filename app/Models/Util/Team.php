<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
	use SoftDeletes;

	protected $table = 'team';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

	public function imagePath()
	{
		return config('evibe.gallery.host') . '/img/team/' . $this->image_url;
	}
}
