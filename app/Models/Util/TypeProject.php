<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeProject extends Model
{
	use SoftDeletes;

	protected $table = 'type_project';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
}
