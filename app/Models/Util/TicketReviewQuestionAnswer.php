<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketReviewQuestionAnswer extends Model
{
	use SoftDeletes;

	protected $table = 'evibe_review_question_answer';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function option()
	{
		return $this->belongsTo(TicketReviewQuestionOption::class, 'option_id');
	}

}
