<?php

namespace App\Models\Util;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Approval extends Model
{
	use SoftDeletes;

	protected $table = 'approval';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function liveBy()
	{
		return $this->belongsTo(User::class, 'live_by');
	}

	public function rejectBy()
	{
		return $this->belongsTo(User::class, 'rejected_by');
	}

	public function approveAskedBy()
	{
		return $this->belongsTo(User::class, 'approve_asked_by');
	}
}
