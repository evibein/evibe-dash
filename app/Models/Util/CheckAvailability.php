<?php

namespace App\Models;

use \TicketMapping;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CheckAvailability extends Model
{
	use SoftDeletes;

	protected $table = 'availability_check';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];

	public function ticket()
	{
		return $this->belongsTo(\Ticket::class, 'ticket_id');
	}

	public function hall()
	{
		return $this->belongsTo(\VenueHall::class, 'map_id');
	}

	public function enquiredPerson()
	{
		return $this->belongsTo(User::class, 'enquired_by');
	}

	public function checkedBy()
	{
		return $this->belongsTo(User::class, 'checked_by');
	}
	public function mapping()
	{
		return $this->belongsTo(TicketMapping::class, 'ticket_mapping_id');
	}

}
