<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignUser extends \Eloquent
{

    use SOftDeletes;

    protected $table = 'campaign_user';
    protected $guarded = array();
    public static $rules = array();
    protected $dates = ['deleted_at'];

    public function getPosterUrl()
    {
        $takenDate = "dec" . (int) date("d", strtotime($this->taken_at));

        return Config::get('evibe.gallery.host') . '/santa-posters/posters/' . $takenDate . '/' . $this->poster_url;
        // return 'http://gallery.evibe.in/planner/96/images/1393589031.JPG';
    }

    public function getSharePageUrl()
    {
        return Config::get('evibe.live.host') . '/bangalore/c/xmas-poster/share?id=' . $this->id . '&token=' . Hash::make($this->id);
    }

    public function getCouponUrl()
    {
        $takenDate = (int) date("d", strtotime($this->taken_at)) . "dec";

        return Config::get('evibe.gallery.host') . '/santa-posters/coupons/' . $takenDate . '.png';
        // return 'http://gallery.evibe.in/santa-posters/coupons/5dec.png';
    }

}