<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogListingActivity extends Model
{
	use SoftDeletes;

	protected $table = 'log_listing_activity';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
