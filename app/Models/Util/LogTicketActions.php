<?php

/**
 * @author Harish <harish.annavajjala@evibe.in>
 * @since  4 Mar 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class LogTicketActions extends \Eloquent
{

	use SoftDeletes;

	protected $table = 'log_ticket_actions';
	protected $dates = ['deleted_at'];
	protected $guarded = [];
	public static $rules = [];

}