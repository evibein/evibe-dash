<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CheckoutFieldOptions extends Model
{
	use SoftDeletes;

	protected $table = 'checkout_field_options';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}