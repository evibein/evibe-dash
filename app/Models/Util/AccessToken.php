<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessToken extends Model
{
	protected $table = 'access_tokens';
	protected $primaryKey = 'access_token';
	public $incrementing = false;
	protected $guarded = [];
}
