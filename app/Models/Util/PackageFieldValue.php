<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageFieldValue extends Model
{
	use SoftDeletes;
	protected $table = 'package_field_value';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
