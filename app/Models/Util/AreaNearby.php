<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class AreaNearby extends \Eloquent {

	use SoftDeletes;

	protected $table = 'area_nearby';
	protected $guarded = array('id');
	public static $rules = array();
	protected $dates = ['deleted_at'];

}