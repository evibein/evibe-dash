<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class SiteErrors extends \Eloquent {

	use SoftDeletes;
	
	protected $table = 'log_site_errors';
	protected $guarded = array('id');
	public static $rules = array();
	protected $dates = ['deleted_at'];
	
}