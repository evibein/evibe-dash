<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SellingStatus extends Model
{
	use SoftDeletes;

	protected $table = 'selling_status';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}