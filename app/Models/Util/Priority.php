<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Priority extends Model
{
	use SoftDeletes;

	protected $table = 'priority';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
}
