<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;

class TicketAnalytics extends Model
{
	protected $table = 'view_ticket_data';
}