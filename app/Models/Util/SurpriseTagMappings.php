<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurpriseTagMappings extends Model
{
	use SoftDeletes;
	protected $table = 'surprise_tag_mappings';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}