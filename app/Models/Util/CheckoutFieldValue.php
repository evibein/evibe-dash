<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CheckoutFieldValue extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_checkout_field_value';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
