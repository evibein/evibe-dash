<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreviousProductURLs extends Model
{
	use SoftDeletes;

	protected $table = 'prev_product_urls';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}