<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeSellingStatus extends Model
{
	use SoftDeletes;

	protected $table = 'type_selling_status';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}