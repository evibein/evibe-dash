<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketReviewQuestionOption extends Model
{
	use SoftDeletes;

	protected $table = 'evibe_review_question_option';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function question()
	{
		return $this->belongsTo(TicketReviewQuestion::class, 'question_id');
	}
}
