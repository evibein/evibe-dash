<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankDetails extends Model
{
	use SoftDeletes;

	protected $table = 'bank_details';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
