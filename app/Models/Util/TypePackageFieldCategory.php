<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePackageFieldCategory extends Model
{
	use SoftDeletes;
	protected $table = 'type_package_field_cat';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
