<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class InternalQuestion extends Model
{
	use SoftDeletes;

	protected $table = 'internal_questions';

	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id');
	}

	public function deletedBy()
	{
		return $this->belongsTo('App\Models\User', 'deleted_by');
	}
}
