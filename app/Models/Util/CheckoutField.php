<?php

namespace App\Models\Util;

use App\Models\Ticket\TicketMappingFieldValue;
use App\Models\TypeEvent;
use App\Models\Types\TypeField;
use App\Models\Types\TypeTicketBooking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CheckoutField extends Model
{
	use SoftDeletes;

	protected $table = 'checkout_field';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function ticketCheckoutValue($ticketId)
	{
		$checkoutField = CheckoutFieldValue::where('ticket_id', $ticketId)
		                                   ->where('checkout_field_id', $this->id)
		                                   ->first();
		if ($checkoutField)
		{
			return $checkoutField->value;
		}
	}

	public function enquiryCheckoutValue($mappingId)
	{
		$checkoutField = TicketMappingFieldValue::where('ticket_mapping_id', $mappingId)
		                                        ->where('field_id', $this->id)
		                                        ->first();
		if ($checkoutField)
		{
			return $checkoutField->value;
		}
	}

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'event_id');
	}

	public function typeTicketBooking()
	{
		return $this->belongsTo(TypeTicketBooking::class, 'type_ticket_booking_id');
	}

	public function typeField()
	{
		return $this->belongsTo(TypeField::class, 'type_field_id');
	}
}
