<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;

class AuthClient extends Model
{
	protected $table = 'auth_clients';
	protected $primaryKey = 'auth_client_id';
	public $incrementing = false;
	protected $guarded = [];
}
