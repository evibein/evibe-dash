<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
	use SoftDeletes;

	protected $table = 'project';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
}
