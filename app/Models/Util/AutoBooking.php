<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;

class AutoBooking extends Model
{

	protected $table = 'auto_booking';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
