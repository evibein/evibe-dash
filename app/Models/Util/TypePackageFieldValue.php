<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePackageFieldValue extends Model
{
	use SoftDeletes;
	protected $table = 'type_package_field_value';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
