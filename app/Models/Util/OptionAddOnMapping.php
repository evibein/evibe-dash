<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class OptionAddOnMapping extends BaseModel
{
	protected $table = 'option_add_on_mapping';
}