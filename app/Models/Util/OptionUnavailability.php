<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class OptionUnavailability extends BaseModel
{
	protected $table = "option_unavailability";
}