<?php

namespace App\Models\Util;

use App\Models\TypeEvent;
use App\Models\Types\TypeField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePackageField extends Model
{
	use SoftDeletes;
	protected $table = 'type_package_field';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function field()
	{
		return $this->belongsTo(TypeField::class, 'type_field_id');
	}

	public function typeField()
	{
		return $this->hasMany(TypePackageFieldValue::class, 'type_package_field_id');
	}

	public function occasion()
	{
		return $this->belongsTo(TypeEvent::class, 'occasion_id');
	}

	public function fieldType()
	{
		return $this->belongsTo(TypePackageFieldCategory::class,'type_cat_id');
	}

	public function filterValidation()
	{
		$existingRule = [];
		$combinedValidation = $this->validation;
		$validationArray = explode('|', $combinedValidation);

		if (count($validationArray))
		{
			if (in_array('required', $validationArray))
			{
				$existingRule['required'] = true;
			}
			if (in_array('numeric', $validationArray))
			{
				$existingRule['numeric'] = true;
			}
			if (in_array('integer', $validationArray))
			{
				$existingRule['integer'] = true;
			}
			$min = preg_grep("/min/", $validationArray);
			$minArray = array_values($min);
			if (count($minArray) > 0)
			{
				$existingRule['min'] = substr($minArray[0], strpos($minArray[0], ':') + 1);
			}
			$max = preg_grep("/max/", $validationArray);
			$maxArray = array_values($max);
			if (count($maxArray) > 0)
			{
				$existingRule['max'] = substr($maxArray[0], strpos($maxArray[0], ':') + 1);
			}

			if (in_array('email', $validationArray))
			{
				$existingRule['email'] = true;
			}
			if (in_array('phone', $validationArray))
			{
				$existingRule['phone'] = true;
			}

			return $existingRule;
		}
	}
}
