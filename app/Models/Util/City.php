<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Eloquent
{
	use SoftDeletes;

	protected $table = 'city';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function areas()
	{
		return $this->hasMany('Area', 'city_id');
	}
}