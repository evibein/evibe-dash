<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  1 June 2015
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CakePrice extends Model
{
	use SoftDeletes;

	protected $table = 'cake_price';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function cake()
	{
		return $this->belongsTo(Cake::class, 'cake_id');
	}
}