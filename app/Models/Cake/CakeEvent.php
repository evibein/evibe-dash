<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CakeEvent extends Model
{
	use SoftDeletes;

	protected $table = 'cake_event';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function cake()
	{
		return $this->belongsTo(Cake::class, 'cake_id');
	}
}