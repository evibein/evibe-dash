<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  1 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class CakeGallery extends \Eloquent
{

	use SoftDeletes;

	protected $table = 'cake_gallery';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function cake()
	{
		return $this->belongsTo('Cake', 'cake_id');
	}

	public function getLink($type = null)
	{
		$imgLink = '';
		$cake = $this->cake;
		$validTypes = ["profile", "results", "thumbs"];

		if ($cake)
		{
			$imgLink = AppUtil::getGalleryBaseUrl();
			$imgLink .= "/cakes/" . $cake->id . "/";

			if (!is_null($type) && in_array($type, $validTypes))
			{
				$imgLink .= "/$type/";
			}

			$imgLink .= $this->url;
		}

		return $imgLink;
	}

	public function getProfileLink()
	{
		$imgLink = '';
		$cake = $this->cake;
		if ($cake)
		{
			$imgLink = AppUtil::getGalleryBaseUrl();
			$imgLink .= '/cakes/' . $cake->id . '/profile/' . $this->url;
		}

		return $imgLink;
	}
}