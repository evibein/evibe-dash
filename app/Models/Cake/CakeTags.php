<?php

/**
 * @author Anji <anji@evibe.in>
 * @since 2 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class CakeTags extends \Eloquent
{

    use SoftDeletes;

    protected $table = 'cake_tags';
    protected $guarded = array();
    public static $rules = array();
    protected $dates = ['deleted_at'];

    public function cake()
    {
        return $this->belongsTo('Cake', 'cake_id');
    }

    public function tag()
    {
        return $this->belongsTo('TileTag', 'tag_id');
    }

}