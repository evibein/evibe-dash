<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  1 June 2015
 */

use App\Models\TypeEvent;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Types\TypeCakeFlavour;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cake extends Model
{
	use SoftDeletes;

	protected $table = 'cake';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function tags()
	{
		return $this->belongsToMany('Tags', 'cake_tags', 'cake_id', 'tag_id')
		            ->wherePivot('deleted_at', null);
	}

	public function gallery()
	{
		return $this->hasMany('CakeGallery', 'cake_id');
	}

	public function prices()
	{
		return $this->hasMany('CakePrice', 'cake_id');
	}

	public function recommendedFlavour()
	{
		return $this->belongsTo(TypeCakeFlavour::class, 'flavour_id');
	}

	public function events()
	{
		return $this->belongsToMany(TypeEvent::class, 'cake_event', 'cake_id', 'event_id')
		            ->wherePivot('deleted_at', null);
	}

	public function scopeForCity($query, $cityId)
	{
		return $query->join('planner', 'cake.provider_id', '=', 'planner.id')
		             ->where('planner.city_id', $cityId);
	}

	public function getPriceWorth()
	{
		$worth = $this->price;
		$cakePrices = CakePrice::where('cake_id', $this->id)
		                       ->orderBy('price', 'ASC')
		                       ->first();

		if ($cakePrices)
		{
			$worth = $cakePrices->worth;
		}

		return $worth;
	}

	public function provider()
	{
		return $this->belongsTo('Vendor', 'provider_id');
	}

	public function getProfilePic()
	{
		$imgUrl = AppUtil::getGalleryBaseUrl() . '/img/app/default_cake.png';

		// profile picture
		$profileImgObj = CakeGallery::where('cake_id', $this->id)
		                            ->where('type', config('evibe.gallery.type.image'))
		                            ->orderBy('is_profile', 'DESC')
		                            ->orderBy('updated_at', 'DESC')
		                            ->first();
		if ($profileImgObj)
		{
			$imgUrl = $profileImgObj->getProfileLink();
		}

		return $imgUrl;
	}

	// Mutators
	public function setTitleAttribute($value)
	{
		$this->attributes['title'] = ucwords(strtolower(trim($value)));
	}

	public function getTitleAttribute($value)
	{
		return ucwords($value);
	}

	public function getTags()
	{
		$tags = [];
		$cakeTagsList = $this->tags;

		// available tags
		$availableTags = [];
		$allTags = Tags::all();

		foreach ($allTags as $tag)
		{
			$availableTags[$tag->id] = [
				'name'      => $tag->name,
				'parent_id' => $tag->parent_id,
			];
		}

		foreach ($cakeTagsList as $cakeTag)
		{
			$currentTag = $availableTags[$cakeTag->pivot->tag_id];
			$tagString = "";

			if ($currentTag['parent_id'])
			{
				$parentId = $currentTag['parent_id'];
				$parentTag = $availableTags[$parentId];
				if ($parentTag)
				{
					$tagString .= $parentTag['name'] . " :: ";
				}
			}

			$tagString .= $currentTag['name'];

			if ($tagString)
			{
				$tags[$cakeTag->pivot->tag_id] = $tagString;
			}
		}

		return $tags;
	}

	public function getLink()
	{
		$url = "cakes/$this->id/info";

		$currentUrl = url()->to($url);

		return $currentUrl;
	}
}