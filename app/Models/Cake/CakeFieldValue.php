<?php

namespace App\Models\Cake;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CakeFieldValue extends Model
{
	protected $table = 'cake_field_values';
	protected $guarded = ['id'];
}
