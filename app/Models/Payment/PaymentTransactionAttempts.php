<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTransactionAttempts extends Model
{
	use SoftDeletes;

	protected $table = 'payment_transaction_attempts';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}