<?php

namespace App\Models\Planner;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Planner extends Model
{
	use SoftDeletes;

	protected $table = 'planner';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}