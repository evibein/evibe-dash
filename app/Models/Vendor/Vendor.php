<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
	use SoftDeletes;

	protected $table = 'planner';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function packages()
	{
		return $this->morphMany(VendorPackage::class, null, 'map_type_id', 'map_id');
	}

	public function type()
	{
		return $this->belongsTo('TypeVendor', 'type_id');
	}

	public function city()
	{
		return $this->belongsTo('City', 'city_id');
	}

	public function area()
	{
		return $this->belongsTo('Area', 'area_id');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id');
	}

	public function status()
	{
		return $this->belongsTo(\App\Models\TypeVendorStatus::class, 'status_id');
	}

	public function gallery()
	{
		return $this->hasMany('VendorGallery', 'planner_id');
	}

	public function handler()
	{
		return $this->belongsTo(\App\Models\User::class, 'handler_id');
	}

	public function source()
	{
		return $this->belongsTo(\TypeTicketSource::class, 'source_id');
	}

	public function leadStatus()
	{
		return $this->belongsTo(\TypeLeadStatus::class, 'lead_status_id');
	}

	public function getProfileUrl()
	{
		$cityUrl = $this->city->url;
		$serverRoot = AppUtil::getServerUrl();
		$type = $this->type->url;

		return $serverRoot . '/' . $cityUrl . '/' . $type . '/' . $this->url;
	}

	public function getPriceWorth()
	{
		return 0;
	}

	public function getProfilePic()
	{
		$host = config('evibe.gallery.host');
		$defaultHost = config('evibe.gallery.default_profile_pic.root');
		$vendorDefault = config('evibe.gallery.default_profile_pic.planner');

		$profilePic = $host . '/' . $defaultHost . '/' . $vendorDefault;

		$profilePicObj = VendorGallery::where('planner_id', $this->id)
		                              ->orderBy('is_profile', 'DESC')
		                              ->orderBy('updated_at', 'DESC')
		                              ->first();

		if ($profilePicObj)
		{
			$profilePic = $host . '/planner/' . $this->id . '/images/' . $profilePicObj->url;
		}

		return $profilePic;
	}

	public function getLocalLink()
	{
		return '/vendors/view/' . $this->id;
	}

	public function bookings()
	{
		return $this->morphMany(TicketBooking::class, null, 'map_type_id', 'map_id');
	}

	public function scheduledBookings()
	{
		return $this->morphMany(\App\Models\Finance\ScheduleSettlementBookings::class, null, 'partner_type_id', 'partner_id');
	}
}