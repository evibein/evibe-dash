<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Model;

class DeliveryGallery extends Model
{
	protected $table = 'delivery_gallery';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];

	public function delivery()
	{
		return $this->belongsTo(\TicketBooking::class, 'ticket_booking_id');
	}

	public function getPath($delivery = null)
	{
		$delivery = !is_null($delivery) ? $delivery : $this->delivery;

		return config('evibe.gallery.host') . "/ticket/$delivery->ticket_id/$delivery->id/partner/$this->url";
	}
}
