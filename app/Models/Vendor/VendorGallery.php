<?php

/**
 * @author Anji <anji@evibe.in>
 * @since 10 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class VendorGallery extends Eloquent {

	use SoftDeletes;

	protected $table = 'planner_gallery';
	protected $guarded = array('id');
	public static $rules = array();
	protected $dates = ['deleted_at'];
}