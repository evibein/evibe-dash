<?php

namespace App\Models\Finance;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleSettlementBookings extends Model {

	use SoftDeletes;

	protected $table = 'schedule_settlement_bookings';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function bookings()
	{
		return $this->belongsTo(\TicketBooking::class, 'ticket_booking_id');
	}

	public function provider()
	{
		return $this->morphTo(null, 'partner_type_id', 'partner_id');
	}
}