<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrendEvents extends Model
{
	use SoftDeletes;

	protected $table = 'trend_event';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'type_event_id');
	}
}
