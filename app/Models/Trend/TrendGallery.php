<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class TrendGallery extends Eloquent
{

	use SoftDeletes;

	protected $table = 'trending_gallery';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function trend()
	{
		return $this->belongsTo(Trend::class, 'trending_id');
	}

	public function getLink($type = null)
	{
		$url = config('evibe.gallery.host');
		$trend = $this->trend;
		$validTypes = ["profile", "results", "thumbs"];
		$imgLink = $url . '/trends/' . $trend->id . '/images/';

		if (!is_null($type) && in_array($type, $validTypes))
		{
			$imgLink .= "/$type/";
		}

		return $imgLink . $this->url;
	}
}