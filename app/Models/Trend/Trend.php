<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class Trend extends \Eloquent
{

	use SoftDeletes;

	protected $table = 'trending';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function provider()
	{
		return $this->belongsTo('Vendor', 'planner_id');
	}

	public function city()
	{
		return $this->belongsTo('City', 'city_id');
	}

	public function gallery()
	{
		return $this->hasMany('TrendGallery', 'trending_id');
	}

	public function getPriceWorth()
	{
		return 0;
	}

	public function events()
	{
		return $this->hasMany(\App\Models\TrendEvents::class, 'trend_id');
	}

	public function getProfilePic()
	{
		$host = config('evibe.gallery.host');
		$defaultHost = config('evibe.gallery.default_profile_pic.root');
		$trendDefault = config('evibe.gallery.default_profile_pic.trend');

		$profilePic = $host . '/' . $defaultHost . '/' . $trendDefault;

		$profilePicObj = TrendGallery::where('trending_id', $this->id)
		                             ->orderBy('is_profile', 'DESC')
		                             ->orderBy('updated_at', 'DESC')
		                             ->first();

		if ($profilePicObj)
		{
			$profilePic = $host . '/trends/' . $this->id . '/images/' . $profilePicObj->url;
		}

		return $profilePic;
	}

	public function scopeForCity($query, $cityId)
	{
		return $query->join('planner', 'trending.planner_id', '=', 'planner.id')
		             ->where('planner.city_id', $cityId);
	}

	public function getInfoString()
	{
		return substr($this->info, 0, 140) . '...';
	}

}