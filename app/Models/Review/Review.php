<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\Review\PartnerReviewAnswer;
use \Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	use SoftDeletes;

	protected $table = 'planner_review';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function vendor()
	{
		return $this->belongsTo('Vendor', 'planner_id');
	}

	public function reply()
	{
		return $this->hasMany('ReviewReply', 'review_id');
	}

	public function booking()
	{
		return $this->belongsTo(TicketBooking::class, 'ticket_booking_id');
	}

	public function individualAnswers()
	{
		return $this->hasMany(PartnerReviewAnswer::class, 'review_id');
	}

	public function provider()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}
}