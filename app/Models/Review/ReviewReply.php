<?php

use Illuminate\Database\Eloquent\SoftDeletes;

class ReviewReply extends Eloquent {

	use SoftDeletes;
	
	protected $table = 'review_reply';
	protected $guarded = array('id');
	public static $rules = array();
	protected $dates = ['deleted_at'];

	public function review()
	{
		return $this->belongsTo('Review', 'review_id');
	}

}