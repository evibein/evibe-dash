<?php

namespace App\Models\Story;

use App\Models\TypeEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerStory extends Model
{
	use SoftDeletes;

	protected $table = 'stories_customer';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function getGalleryLink()
	{
		$galleryLink = '';
		$type = $this->type_id;

		//@see: each story has only one video/image
		if ($type == config('evibe.gallery.type.image'))
		{
			$host = config('evibe.gallery.host');
			$galleryLink = $host . '/story/customer/' . $this->url;
		}
		elseif ($type == config('evibe.gallery.type.video'))
		{
			$host = config('evibe.gallery.youtube_host');
			$galleryLink = $host . trim($this->url);
		}

		return $galleryLink;
	}

	public function event()
	{
		return $this->belongsTo(TypeEvent::class,'event_id');
	}
}
