<?php

namespace App\Models\Story;

use App\Models\TypeEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class VendorStory extends Model
{
	use SoftDeletes;

	protected $table = 'stories_vendor';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function vendor()
	{
		if ($this->map_type_id == config('evibe.ticket_type.venues'))
		{
			return $this->belongsTo(\Venue::class, 'map_id');
		}
		else
		{
			return $this->belongsTo(\Vendor::class, 'map_id');
		}
	}

	public function getGalleryLink()
	{
		$galleryLink = '';
		$type = $this->type_id;

		//@see: each story has only one video/image
		if ($type == config('evibe.gallery.type.image'))
		{
			$host = config('evibe.gallery.host');
			$galleryLink = $host . '/story/vendor/' . $this->url;
		}
		elseif ($type == config('evibe.gallery.type.video'))
		{
			$host = config('evibe.gallery.youtube_host');
			$galleryLink = $host . trim($this->url);
		}

		return $galleryLink;
	}

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'event_id');
	}
}
