<?php

namespace App\Models\Story;

use App\Models\Util\City;
use Illuminate\Database\Eloquent\Model;

class SearchableProduct extends Model
{
	protected $table = "searchable_product";

	public function city()
	{
		return $this->belongsTo(\City::class, "city_id");
	}

	public function decorGallery()
	{
		return $this->hasMany(\DecorGallery::class, "decor_id");
	}
}
