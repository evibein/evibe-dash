<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostalPinCode extends Model
{
	use SoftDeletes;

	protected $table = 'postal_pin_code';
}