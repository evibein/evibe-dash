<?php

namespace Evibe\Utilities;

/**
 * Added namespace
 *
 * @author Anji <anji@evibe.in>
 * @since  9 Jan 2015
 */

use App;
use Cake;
use Decor;
use App\Models\TypeServices;
use App\Models\Util\CheckoutFieldValue;
use App\Models\Util\CheckoutFieldOptions;
use App\Models\Ticket\TicketMappingFieldValue;
use Illuminate\Support\Facades\Log;
use Trend;
use Vendor;
use VenueHall;
use VendorPackage;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\TypeSlot;

class AppUtil
{
	public function watermarkImage($path, $args = null)
	{
		/**
		 * @todo bad practise, need to update this.
		 */
		ini_set('memory_limit', '128M');
		$imageRotate = is_array($args) && array_key_exists('isRotate', $args) ? $args['isRotate'] : null;
		$path = urldecode($path);
		$path = '../../' . $path;
		$pathInfo = pathinfo($path);
		$extension = isset($pathInfo["extension"]) ? $pathInfo["extension"] : false;
		/**
		 * Removed resizing images
		 *
		 * Image are not shown as a carousel instead are listed.
		 *
		 * @since 10 Feb 2015
		 */
		/*
		$img->resize(700, null, function ($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});
		*/
		$image = false;
		if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'JPG' || $extension == 'JPEG')
		{
			$image = imagecreatefromjpeg($path);
		}
		elseif ($extension == 'png' || $extension == 'PNG')
		{
			$image = imagecreatefrompng($path);
		}
		elseif ($extension == 'gif' || $extension == 'GIF')
		{
			$image = imagecreatefromgif($path);
		}

		if ($imageRotate)
		{
			$image = imagerotate($image, $imageRotate, 0);
		}

		$imageDimensions = getimagesize($path);
		$stampData = $this->getWatermarkImagePosition($imageDimensions);
		if (isset($stampData["stamp"]))
		{
			$stamp = imagecreatefrompng($stampData["stamp"]);

			// Set the margins for the stamp and get the height/width of the stamp image
			$imageWidth = isset($imageDimensions[0]) ? $imageDimensions[0] : 0;
			$imageHeight = isset($imageDimensions[1]) ? $imageDimensions[1] : 0;
			$sx = imagesx($stamp);
			$sy = imagesy($stamp);

			// Copy the stamp image onto our photo using the margin offsets and the photo
			// width to calculate positioning of the stamp.
			imagecopy(
				$image,
				$stamp,
				(2 * $imageWidth) / 25,
				imagesy($image) - $sy - ($imageHeight - $sy) / 2,
				0,
				0,
				imagesx($stamp),
				imagesy($stamp)
			);

			// Remove the original file
			unlink($path);

			// rename the copied watermark image to original image
			if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'JPG' || $extension == 'JPEG')
			{
				imagejpeg($image, $path);
			}
			if ($extension == 'png' || $extension == 'PNG')
			{
				imagepng($image, $path);
			}
			if ($extension == 'gif' || $extension == 'GIF')
			{
				imagegif($image, $path);
			}

			chmod($path, 0777);
			imagedestroy($image);
		}
		else
		{
			Log::error("error while getting watermark image for file(path)" . $path);
		}

	}

	public function getWatermarkImagePosition($originalImageDimensions)
	{
		$positionRight = $positionBottom = $calculatedWidth = 0;
		$stamp = null;

		$imageWidth = isset($originalImageDimensions[0]) ? $originalImageDimensions[0] : 0;
		$imageHeight = isset($originalImageDimensions[1]) ? $originalImageDimensions[1] : 0;
		$stamp = config('evibe.root.gallery_queue') . "/watermarks/new/";
		if ($imageHeight > $imageWidth)
		{
			$stamp = config('evibe.root.gallery_queue') . "/watermarks/vertical/";
		}

		if (($imageWidth != 0) && ($imageHeight != 0))
		{
			if ($imageWidth >= 4000)
			{
				$calculatedWidth = 530;
			}
			elseif ($imageWidth >= 3500)
			{
				$calculatedWidth = 470;
			}
			elseif ($imageWidth >= 3000)
			{
				$calculatedWidth = 400;
			}
			elseif ($imageWidth >= 2500)
			{
				$calculatedWidth = 330;
			}
			elseif ($imageWidth >= 2000)
			{
				$calculatedWidth = 260;
			}
			elseif ($imageWidth >= 1400)
			{
				$calculatedWidth = 190;
			}
			elseif ($imageWidth >= 1200)
			{
				$calculatedWidth = 160;
			}
			elseif ($imageWidth >= 1000)
			{
				$calculatedWidth = 135;
			}
			elseif ($imageWidth >= 900)
			{
				$calculatedWidth = 120;
			}
			elseif ($imageWidth >= 800)
			{
				$calculatedWidth = 110;
			}
			elseif ($imageWidth >= 700)
			{
				$calculatedWidth = 100;
			}
			elseif ($imageWidth >= 500)
			{
				$calculatedWidth = 70;
			}
			elseif ($imageWidth >= 300)
			{
				$calculatedWidth = 40;
			}
			elseif ($imageWidth < 300)
			{
				$calculatedWidth = 40;
			}

			$stamp .= "$calculatedWidth.png";
		}

		return [
			"stamp" => $stamp
		];
	}

	/**
	 * ResizeImages
	 *
	 * @author Anji <anji@evibe.in>
	 * @since  21 Oct 2015
	 */
	public function resizeImg($img, $type, $newImgPath, $imgUrl, $width, $height = null)
	{
		if (!file_exists($newImgPath))
		{
			mkdir($newImgPath, 0777, true);
		}

		$img->resize($width, $height, function ($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});
		$img->save($newImgPath . $imgUrl);
	}

	public function setSessions()
	{
		Session::put('username', Auth::user()->username);
		Session::put('user_id', Auth::user()->id);
	}

	public function isFilterChecked($type, $value, $key)
	{
		if ($type == "priceRange")
		{
			return is_int(strpos($value, $key)) ? "checked" : "";
		}
		else
		{
			if ($type == "tags")
			{
				return in_array($key, $value) ? "checked" : "";
			}
			else
			{
				return "";
			}
		}
	}

	public function getGalleryBaseUrl()
	{
		return config('evibe.gallery.host');
	}

	/**
	 * This method returns the total path of package gallery images
	 */
	public function getPackageGalleryUrl($gallery, $isNew = false)
	{
		$url = config('evibe.gallery.host');

		$package = $gallery->package;

		if ($package->provider)
		{
			if ($package->map_type_id == config('evibe.ticket_type.venues'))
			{
				$url .= '/venues/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			else
			{
				$url .= '/planner/' . $package->provider->id . '/images/packages/' . $package->id;
			}
		}

		$url .= '/' . $gallery->url;

		return $url;
	}

	public function getServerUrl()
	{
		return config('evibe.live.host');
	}

	public function formatPrice($price)
	{
		return $this->moneyFormatIndia($price);
	}

	public function formatHandler($email)
	{
		$split = explode('@', $email);
		$split = str_replace(".", " ", $split[0]);

		return ucwords($split);
	}

	public function formatDateForTicket($timestamp)
	{
		return date("d M Y \(D\)", $timestamp);
	}

	public function formatDateTimeForBooking($timestamp)
	{
		return date("d M Y, h:i A \(D\)", $timestamp);
	}

	public function formatDate($date)
	{
		return date("D, M j \'y", strtotime($date));
	}

	public function formatUnixDate($date)
	{
		return date("D, M j \'y", $date);
	}

	public function formatBookingEmailDate($timestamp)
	{
		return date("D, M j Y", $timestamp);
	}

	public function formatTimestamp($date)
	{
		return date('h:i a\, F d, Y', strtotime($date));
	}

	public function formatJSDate($date)
	{
		$dateArr = explode('-', $date);

		return $dateArr[1] . '/' . $dateArr[2] . '/' . $dateArr[0];
	}

	public function formatJSDateTime($time)
	{
		$dateTime = '';

		if ($time)
		{
			$dateTime = date('m/d/Y H:i', $time);
		}

		return $dateTime;
	}

	public function filterGalleryItems($collection, $type)
	{
		$filteredCollection = $collection->filter(function ($item) use ($type) {
			if ($item->type == $type)
			{
				return true;
			}
		});

		return $filteredCollection;
	}

	public function filterVendors($collection, $key, $value, $existing)
	{
		$filteredCollection = $collection->filter(function ($item) use ($key, $value, $existing) {
			if ($item->$key == $value && !$this->isExisting($item->id, $existing))
			{
				return true;
			}
		});

		return $filteredCollection;
	}

	public function filterCollection($collection, $key, $typeId = 2)
	{
		$filteredCollection = $collection->filter(function ($item) use ($key, $typeId) {
			if ($item->$key->id == $typeId)
			{
				return true;
			}
		});

		return $filteredCollection;
	}

	public function getTicketStatusIcon($statusId)
	{
		$iconClass = "";

		switch ($statusId)
		{
			case 3:
				$iconClass = "ok";
				break;
			case 4:
				$iconClass = "ok-sign";
				break;
			case 5:
				$iconClass = "remove-sign";
				break;
			case 6:
				$iconClass = "remove";
				break;
			case 8:
				$iconClass = "ok-sign";
				break;
			case 9:
				$iconClass = "remove-sign";
				break;
			case 10:
				$iconClass = "ok-sign";
				break;
			case 12:
				$iconClass = "ok-sign";
				break;
			case 13:
				$iconClass = "exclamation-sign";
				break;
			case 17:
				$iconClass = "minus-sign";
				break;
			case 18:
				$iconClass = "exclamation-sign";
				break;
			case 19:
				$iconClass = "ban-circle";
				break;
			default:
				$iconClass = "expand";
				break;
		}

		return "glyphicon-" . $iconClass;
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  9 Jan 2015
	 *
	 * @see    Role Id's must be in Sync with database table 'roles'
	 */
	public function isTabVisible($type)
	{
		$result = false;
		$validRoles = [config('evibe.roles.super_admin'), config('evibe.roles.admin')];
		$roleId = Auth::user()->role_id;

		switch ($type)
		{
			// DashBoards
			case 'crmDashBoard':
				array_push($validRoles,
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'opsDashboard':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_tl')
				);
				break;

			// Finance related
			case 'finance':
				array_push($validRoles,
				           config('evibe.roles.accounts')
				);
				break;

			// Tagging reviews to particular options which are mapped to partners
			case 'reviews_mapping':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			// Tickets info
			case 'tickets':
				array_push($validRoles,
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm'),
				           config("evibe.roles.syrow"),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_tl'),
				           config('evibe.roles.accounts'),
				           config('evibe.roles.bd_agent')
				);
				break;

			// Order tracking for upcoming bookings
			case 'tracking':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_tl'),
				           config('evibe.roles.marketing')
				);
				break;

			case 'customer-feedback':
				array_push($validRoles,
				           config('evibe.roles.marketing')
				);
				break;

			// Info of all the bookings in a calendar view
			case 'booking-calendar':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_tl'),
				           config('evibe.roles.bd_agent')
				);
				break;

			case 'stories':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.marketing'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'availability':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config("evibe.roles.tech"),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'collection':
			case 'add-ons':
			case 'venue-halls':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config("evibe.roles.tech"),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'partner_bank_details':
			case 'review_edit':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config("evibe.roles.tech"),
				           config("evibe.roles.bd_agent")
				);
				break;

			case 'packages':
			case 'decors':
			case 'venues':
			case 'services':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'trends':
			case 'cakes':
			case 'images':
			case 'vendors':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.marketing'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'reviews':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.marketing'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_tl'),
				           config('evibe.roles.marketing')
				);
				break;

			case 'edit_venues':
			case 'activation':
			case 'nearby-areas':
			case 'new-package':
			case 'new-decor':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.bd_agent')
				);
				break;

			case 'content':
			case 'revamp_tags':
			case 'audit':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'delivery_images':
				array_push($validRoles,
				           config('evibe.roles.marketing'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm')
				);
				break;

			case 'feedback::pending':
			case 'ticket_followup::edit':
				array_push($validRoles,
				           config('evibe.roles.operations'),
				           config('evibe.roles.customer_delight'),
				           config('evibe.roles.sr_crm')
				);
				break;

			case 'vendor_followup::edit':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations')
				);
				break;

			case 'booking_concept';
				array_push($validRoles,
				           config('evibe.roles.operations'),
				           config('evibe.roles.marketing')
				);
				break;

			case 'user_management':
			case 'analytics':
			case 'coupon':
			case 'virtual-party':
				// only for super admins
				unset($validRoles[array_search(config('evibe.roles.admin'), $validRoles)]);
				break;

			case 'errors':
			case 'automation':
			case 'downloads':
			case 'monitor':
				array_push($validRoles,
				           config('evibe.roles.tech')
				);
				break;

			case 'delivery':
				array_push($validRoles,
				           config('evibe.roles.marketing'),
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_agent'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'live_profile':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd')
				);
				break;

			case 'reorder':
			case 'option-availability':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'decors::actions':
			case 'decors::makeactive':
				array_push($validRoles,
				           config('evibe.roles.sr_bd')
				);
				break;

			case 'decors::status':
			case 'cakes::status':
			case 'decors::edit_gallery':
			case 'decors::view_provider':
			case 'decors::edit_tags':
			case 'decors::nonlive':
			case 'packages::edit_tags':
			case 'packages::edit_gallery':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.sr_crm'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'trends::edit_info':
			case 'decors::edit_info':
			case 'packages::edit_info':
			case 'services::edit-services':
			case 'cake::edit_info':
				array_push($validRoles,
				           config('evibe.roles.bd'),
				           config('evibe.roles.sr_bd'),
				           config('evibe.roles.operations'),
				           config('evibe.roles.ops_tl'),
				           config('evibe.roles.bd_agent'),
				           config('evibe.roles.bd_tl')
				);
				break;

			case 'vendors::actions':
				array_push($validRoles,
				           config('evibe.roles.bd_agent')
				);
				break;

			case 'activate_options':
				array_push($validRoles,
				           config('evibe.roles.bd_agent')
				);
				break;

			case 'stories::delete':
			case 'trends::delete':
			case 'cakes::delete':
			case 'checkout_field::show':
			case 'checkout_field::add':
			case 'checkout_field::delete':
			case 'dashboard':
			case 'package_field':
			case 'delete_venues':
			case 'delete_ticket':
			case 'activate_venue':
			case 'received_advance':
			case 'cancel_booking':
				// do nothing
				break;

			default:
				return abort(405, "Unauthorized access");
		}

		if (in_array($roleId, $validRoles))
		{
			$result = true;
		}

		return $result;
	}

	public function getVendorStatusIcon($statusId)
	{
		$iconClass = '';
		switch ($statusId)
		{
			case 1:
				$iconClass = "glyphicon-expand";
				break;
			case 2:
				$iconClass = "glyphicon-arrow-up";
				break;
			case 3:
				$iconClass = "glyphicon-ok-sign";
				break;
			case 4:
				$iconClass = "glyphicon-cloud-download";
				break;
			case 5:
				$iconClass = "glyphicon-thumbs-down";
				break;
			case 6:
				$iconClass = "glyphicon glyphicon-remove-sign";
				break;
			case 7:
				$iconClass = "glyphicon-thumbs-up";
				break;
			case 8;
				$iconClass = "glyphicon-thumbs-down";
		}

		return $iconClass;
	}

	public function isSuperAdmin($roleId = null)
	{
		$isSuperAdmin = false;

		if (is_null($roleId))
		{
			$roleId = Auth::user()->role_id;
		}

		// only super admin
		// 1 - super admin
		if ($roleId == 1)
		{
			$isSuperAdmin = true;
		}

		return $isSuperAdmin;
	}

	/**
	 * @since 20 Jan 2015
	 */
	public function isAdmin($roleId = null)
	{
		$isAdmin = false;

		if (is_null($roleId))
		{
			$roleId = Auth::user()->role_id;
		}

		// 1 - super admin, 2 - admin
		if ($roleId <= 2)
		{
			$isAdmin = true;
		}

		return $isAdmin;
	}

	public function isSrCrm($roleId = null)
	{
		$isSrCrm = false;

		if (is_null($roleId))
		{
			$roleId = Auth::user()->role_id;
		}

		// 9 - senior CRM
		if ($roleId == 9)
		{
			$isSrCrm = true;
		}

		return $isSrCrm;
	}

	public function isSrBd($roleId = null)
	{
		$isSrBd = false;

		if (is_null($roleId))
		{
			$roleId = Auth::user()->role_id;
		}

		// 14 - senior BD
		if ($roleId == config("evibe.roles.sr_bd"))
		{
			$isSrBd = true;
		}

		return $isSrBd;
	}

	public function isUpdateTicket($roleId = null)
	{
		$isUpdateTicket = true;

		if (is_null($roleId))
		{
			$roleId = Auth::user()->role_id;
		}

		// 20 - Accounts
		if ($roleId == config("evibe.roles.accounts"))
		{
			$isUpdateTicket = false;
		}

		return $isUpdateTicket;
	}

	public function fillMappingValues($options)
	{
		$mappingValues = $mappers = [];
		$hasProvider = false;
		$mapTypeId = $options['mapTypeId'];
		$typeId = $options['mapTypeId'];
		$mapId = isset($options['mapId']) ? $options['mapId'] : false;
		$ticketEvent = isset($options['ticketEventId']) ? $options['ticketEventId'] : false;
		$cityId = isset($options['cityId']) ? $options['cityId'] : null;
		$mapType = "";

		/**
		 * @see whereNull is required as using $someVariable = Vendor; results
		 * @see `mapTypeId` is matched against `type_ticket`  in "evibe.php" config file
		 *      which is in sync with database table type_ticket
		 */
		$selectableVendors = Vendor::withTrashed();

		switch ($mapTypeId)
		{
			// packages * related
			case config('evibe.ticket_type.packages'):
			case config('evibe.ticket_type.food'):
			case config('evibe.ticket_type.villa'):
			case config('evibe.ticket_type.resort'):
			case config('evibe.ticket_type.priests'):
			case config('evibe.ticket_type.lounge'):
			case config('evibe.ticket_type.tents'):
			case config('evibe.ticket_type.couple-experiences'):
			case config('evibe.ticket_type.generic-package'):
			case config('evibe.ticket_type.venue-deals'):
				$mappers = $mapId ? VendorPackage::withTrashed()->with('provider')->find($mapId) : VendorPackage::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "packages";
				break;

			// planners
			case config('evibe.ticket_type.planners'):
				$mappers = $mapId ? $selectableVendors->find($mapId) : Vendor::where('is_live', 1)->get();
				$mapType = "vendors";
				break;

			// venues (aka halls)
			case config('evibe.ticket_type.venues'):
				$venueHalls = VenueHall::withTrashed()->with('venue');
				$mappers = $mapId ? $venueHalls->find($mapId) : VenueHall::with('venue')->get();
				$mapType = "venues";
				break;

			// services
			case config('evibe.ticket_type.services'):
				$mappers = $mapId ? TypeServices::withTrashed()->find($mapId) : TypeServices::where('is_live', 1)->get();
				$hasProvider = false;
				$mapType = "services";
				break;

			// trends
			case config('evibe.ticket_type.trends'):
				$mappers = $mapId ? Trend::withTrashed()->with('provider')->find($mapId) : Trend::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "trends";
				break;

			// cakes
			case config('evibe.ticket_type.cakes'):
				$mappers = $mapId ? Cake::withTrashed()->with('provider')->find($mapId) : Cake::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "cakes";
				break;

			// decors
			case config('evibe.ticket_type.decors'):
				$mappers = $mapId ? Decor::withTrashed()->with('provider')->find($mapId) : Decor::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "decors";
				break;

			// add-ons
			case config('evibe.ticket_type.add-on'):
				// @todo: need to correct for add-ons
				$mappers = $mapId ? App\Models\AddOn\AddOn::withTrashed()->find($mapId) : App\Models\AddOn\AddOn::where('is_live', 1)->get();
				$hasProvider = false;
				$mapType = "add-ons";
		}

		if ($options['isCollection'])
		{
			foreach ($mappers as $mapper)
			{
				if (!$mapper)
				{
					continue;
				}

				$basicVal = [
					'code'    => $mapper->code,
					'name'    => $mapTypeId == config('evibe.ticket_type.cakes') ? $mapper->title : ($mapper->name ? $mapper->name : ""),
					'type'    => $mapType,
					'deleted' => $mapper->deleted_at,
					'info'    => $mapper->info
				];

				// push hall only if venues exists
				if ($mapTypeId == config('evibe.ticket_type.venues') && $mapper->venue && $mapper->venue->is_live == 1)
				{
					if (!is_null($cityId) && $mapper->venue->city_id != $cityId)
					{
						continue;
					}
					array_set($basicVal, "name", $mapper->venue->name . " - " . $mapper->name);
					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'  => $mapper->venue->price_min_veg,
						'person' => $mapper->venue->person,
						'phone'  => $mapper->venue->phone,
						'userId' => $mapper->venue->user_id
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket_type.services'))
				{
					if (!is_null($cityId) && $mapper->city_id != $cityId)
					{
						continue;
					}
					$servicePrice = $mapper->min_price;
					//$servicePrice .= $mapper->max_price ? ' - Rs. ' . $this->formatPrice($mapper->max_price) : '';

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'  => $servicePrice,
						'person' => 'Evibe',
						'phone'  => config('evibe.phone_plain'),
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket_type.decors'))
				{
					if (!is_null($cityId) && $mapper->provider->city_id != $cityId)
					{
						continue;
					}
					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'  => $mapper->min_price,
						'person' => $mapper->provider->person,
						'phone'  => $mapper->provider->phone,
						'userId' => $mapper->provider->user_id
					]);
				}
				else
				{
					// packages exists, but vendors is not active
					$isValid = $hasProvider ? $mapper->provider : $mapper->person;
					if (!$isValid)
					{
						continue;
					}

					if ($hasProvider && !is_null($cityId) && $mapper->provider->city_id != $cityId)
					{
						continue;
					}
					elseif (!$hasProvider && !is_null($cityId) && $mapper->city_id != $cityId)
					{
						continue;
					}

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'  => $mapper->price,
						'person' => $hasProvider ? $mapper->provider->person : $mapper->person,
						'phone'  => $hasProvider ? $mapper->provider->phone : $mapper->phone,
						'userId' => $hasProvider ? $mapper->provider->user_id : $mapper->user_id,
					]);
				}
			}
		}
		else
		{
			/**
			 * @todo generate public link based on mapType
			 * @todo venues and vendors should be separated for viewing
			 */
			$mapper = $mappers;
			$forLink = $mapType;
			$liveHost = config('evibe.live.host');
			// load default values
			$mappingValues = [
				'id'         => '',
				'code'       => '',
				'name'       => '',
				'price'      => '',
				'info'       => '',
				'worth'      => '',
				'pic'        => '',
				'deleted'    => '',
				'provider'   => [
					'name'     => '',
					'person'   => '',
					'phone'    => '',
					'email'    => '',
					'location' => '',
					'userId'   => ''
				],
				'link'       => '',
				'url'        => '',
				'cityUrl'    => '',
				'publicLink' => '',
				'type'       => '',
				'self'       => ''
			];

			$packageTypes = [
				config('evibe.ticket_type.packages'),
				config('evibe.ticket_type.venue-deals'),
				config('evibe.ticket_type.priests'),
				config('evibe.ticket_type.tents'),
				config('evibe.ticket_type.food'),
				config('evibe.ticket_type.couple-experiences'),
				config('evibe.ticket_type.resort'),
				config('evibe.ticket_type.villa'),
				config('evibe.ticket_type.lounge'),
				config('evibe.ticket_type.generic-package')
			];
			if (in_array($mapTypeId, $packageTypes))
			{
				$mapTypeId = config('evibe.ticket_type.packages');
			}

			if ($mapper)
			{
				// special case for tickets raised for services
				if ($mapTypeId == config('evibe.ticket_type.services'))
				{
					$cityUrl = \City::find(1)->url;
					$servicePrice = $mapper->min_price;
					//$servicePrice .= $mapper->max_price ? ' - Rs. ' . $this->formatPrice($mapper->max_price) : '';

					$mappingValues = [
						'id'         => $mapper->id,
						'name'       => $mapper->name ? $mapper->name : "",
						'code'       => $mapper->code ? $mapper->code : $mapper->name,
						'price'      => $servicePrice,
						'worth'      => $mapper->worth_price,
						'range_info' => $mapper->range_info,
						'info'       => $mapper->info,
						'max_price'  => $mapper->max_price,
						'deleted'    => $mapper->deleted_at,
						'link'       => '/' . $forLink . '/details/' . $mapper->id,
						'type'       => $mapType,
						'publicLink' => config('evibe.live.host') . '/' . $cityUrl . '/' . config('evibe.live.profile_url.entertainment') . '/' . $mapper->url,
						'pic'        => $mapper->getProfilePic()
					];
				}
				elseif ($mapTypeId == config('evibe.ticket_type.venues'))
				{
					if (!$mapper->venue)
					{
						return $mappingValues;
					}

					// venues (aka halls)
					$cityUrl = $mapper->venue->city ? $mapper->venue->city->url : "bangalore"; // default city
					$typeProfileUrl = config('evibe.live.profile_url.venues');
					$publicLink = $liveHost . '/' . $cityUrl . '/' . $typeProfileUrl . '/' . $mapper->url;

					$mappingValues = [
						'id'         => $mapper->id,
						'code'       => $mapper->code,
						'name'       => $mapper->name ? $mapper->name : "",
						'info'       => $mapper->venue->info,
						'tax'        => $mapper->venue->tax_percent,
						'worth'      => $mapper->getPriceWorth(),
						'price'      => $mapper->venue->price_min_veg,
						'pic'        => $mapper->getProfilePic(),
						'deleted'    => $mapper->deleted_at,
						'provider'   => [
							'name'     => $mapper->venue->name,
							'person'   => $mapper->venue->person,
							'phone'    => $mapper->venue->phone,
							'email'    => $mapper->venue->email,
							'location' => $mapper->venue->area->name,
							'userId'   => $mapper->venue->user_id,
							'altPhone' => $mapper->venue->alt_phone
						],
						'link'       => '/' . $forLink . '/hall/' . $mapper->id,
						'url'        => $mapper->url,
						'cityUrl'    => $cityUrl,
						'publicLink' => $publicLink,
						'type'       => ucfirst($mapType),
						'self'       => $mapper
					];
				}
				elseif($mapTypeId == config('evibe.ticket_type.add-on'))
				{
					$cityUrl = $mapper->city->url;
					//$typeProfileUrl = config('evibe.live.profile_url.' . $mapType);
					//$publicLink = $liveHost . '/' . $cityUrl . '/' . $typeProfileUrl . '/' . $mapper->url;
					$publicLink = null; // no profile page for add-ons
					$linkForDash = '/add-ons'; // todo: need to revamp

					$mappingValues = [
						'id'            => $mapper->id,
						'code'          => $mapper->code,
						'name'          => $mapper->name,
						'price'         => $mapper->price_per_unit,
						'info'          => $mapper->info,
						'prerequisites' => null,
						'facts'         => null,
						'worth'         => $mapper->price_worth_per_unit,
						'pic'           => $mapper->getProfilePic(),
						'deleted'       => $mapper->deleted_at,
						'link'          => $linkForDash,
						'url'           => $mapper->url,
						'cityUrl'       => $cityUrl,
						'publicLink'    => $publicLink,
						'type'          => ucfirst($mapType),
						'self'          => $mapper
					];
				}
				else
				{
					// for decors / cakes / trends / packages / planners
					$mapName = ($mapTypeId == config('evibe.ticket_type.cakes')) ? $mapper->title : ($mapper->name ? $mapper->name : "");
					$cityUrl = $hasProvider ? $mapper->provider ? $mapper->provider->city->url : 'Not Available' : $mapper->city->url;
					$typeProfileUrl = config('evibe.live.profile_url.' . $mapType);
					$publicLink = $liveHost . '/' . $cityUrl . '/' . $typeProfileUrl . '/' . $mapper->url;
					$linkForDash = '/' . $forLink . '/view/' . $mapper->id;

					if ($mapTypeId == config('evibe.ticket_type.decors'))
					{
						$price = $mapper->min_price;
						$prerequisites = $mapper->more_info;
					}
					else
					{
						$price = $mapper->price;
						$prerequisites = $mapper->prerequisites;
					}

					if ($mapTypeId == config('evibe.ticket_type.cakes') || $mapTypeId == config('evibe.ticket_type.decors'))
					{
						$linkForDash = '/' . $forLink . '/' . $mapper->id . '/info';
					}
					//@see changed the local dash link due to the categories in packages type.
					if ($mapTypeId == config('evibe.ticket_type.packages'))
					{
						$linkForDash = route('package.view', $mapper->id);
					}

					$mappingValues = [
						'id'            => $mapper->id,
						'code'          => $mapper->code,
						'name'          => $mapName,
						'price'         => $price,
						'info'          => $mapper->info,
						'prerequisites' => $prerequisites,
						'facts'         => $mapper->facts,
						'worth'         => $mapper->getPriceWorth(),
						'pic'           => $mapper->provider ? $mapper->getProfilePic() : null,
						'deleted'       => $mapper->deleted_at,
						'link'          => $linkForDash,
						'provider'      => [
							'name'     => $hasProvider ? $mapper->provider ? $mapper->provider->name : '' : $mapper->name,
							'person'   => $hasProvider ? $mapper->provider ? $mapper->provider->person : '' : $mapper->person,
							'phone'    => $hasProvider ? $mapper->provider ? $mapper->provider->phone : '' : $mapper->phone,
							'email'    => $hasProvider ? $mapper->provider ? $mapper->provider->email : '' : $mapper->email,
							'location' => $hasProvider ? $mapper->provider ? $mapper->provider->area->name : '' : $mapper->area->name,
							'userId'   => $hasProvider ? $mapper->provider ? $mapper->provider->user_id : '' : $mapper->user_id,
							'altPhone' => $hasProvider ? $mapper->provider ? $mapper->provider->alt_phone : '' : $mapper->alt_phone
						],
						'url'           => $mapper->url,
						'cityUrl'       => $cityUrl,
						'publicLink'    => $publicLink,
						'type'          => ucfirst($mapType),
						'self'          => $mapper
					];

				}

				if ($ticketEvent)
				{
					$mappingValues['publicLink'] = $this->getWebsiteLiveLink($ticketEvent, $mapper, $mapTypeId, $cityUrl);
				}
			}
		}

		/* Adding map_type_id, whether product or partner */
		$mappingValues['mapTypeId'] = $typeId;

		return $mappingValues;
	}

	public function getVendorTypeUrl($vendorTypeId)
	{
		$url = "packages";

		switch ($vendorTypeId)
		{
			case 1:
			case 2:
				$url = "vendors/view";
				break;
			case 3:
			case 5:
			case 6:
				$url = "venues/hall";
				break;
		}

		return $url;
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  2 June 2015
	 */
	public function getLiveUrl($typeId, $item)
	{
		$cityUrl = null;
		if (($item->provider) && ($item->provider->city->url))
		{
			$cityUrl = $item->provider->city->url;
		}

		$cityDetails = \City::select('id', 'url')->where('is_active', 1)->get()->toArray();

		$liveUrl = config('evibe.live.host');

		if ($typeId == config('evibe.ticket_type.cakes'))
		{
			$liveUrl .= '/liveUrl/' . $cityUrl . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}
		elseif ($typeId == config('evibe.ticket_type.decors'))
		{
			$liveUrl .= '/liveUrl/' . $cityUrl . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}
		elseif ($typeId == config('evibe.ticket_type.services'))
		{
			if (is_null($cityUrl))
			{
				$cityDetails = \City::select('id', 'url')->where('is_active', 1)->get()->toArray();
				$key = array_search($item->city_id, array_column($cityDetails, 'id'));
			}

			$liveUrl .= '/liveUrl/' . $cityDetails[$key]['url'] . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}
		elseif ($typeId == config('evibe.ticket_type.packages'))
		{
			$liveUrl .= '/liveUrl/' . $cityUrl . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}
		elseif ($typeId == config('evibe.ticket_type.trends'))
		{
			$liveUrl .= '/liveUrl/' . $cityUrl . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}

		return $liveUrl;
	}

	public function filterVenueExtRatings($collection, $needle)
	{
		$filteredCollection = $collection->filter(function ($item) use ($needle) {
			if ($item->type_ratings_website_id == $needle)
			{
				return true;
			}
		});

		return $filteredCollection->first();
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  25 Aug 2015
	 */
	public function getHandlerName($name = null, $id = null)
	{
		if (is_null($name))
		{
			$name = Auth::user()->username;
		}
		if ($id)
		{
			$name = App\Models\User::find($id)->username;
		}

		if ($pos = strpos($name, '@'))
		{
			$name = substr($name, 0, $pos);
		}
		if ($pos = strpos($name, '.'))
		{
			$name = substr($name, 0, $pos);
		}

		return ucfirst($name);
	}

	/**
	 * @author Vikash <vikash@evibe.in>
	 * @since  4 Jan 2016
	 */
	public function getTimeInMinutesFormat($hrs, $min)
	{
		$time = $hrs * 60 + $min;

		return $time;
	}

	public function getTimeFormatForBeforeBooking($time)
	{
		$timeHrs = (integer)($time / 60);
		$timeMins = $time % 60;

		return $timeHrs . 'h ' . $timeMins . 'm';
	}

	/*
	 * Vikash <vikash@evibe.in>
	 * @since 31 March 2016
	 */
	public function getVenueType($type)
	{
		$venueType = '';
		if ($type == config('evibe.venue.type.veg'))
		{
			$venueType = "veg";
		}
		elseif ($type == config('evibe.venue.type.non_veg'))
		{
			$venueType = "Non veg";
		}
		elseif ($type == config('evibe.venue.type.both'))
		{
			$venueType = 'both ( veg & non-veg)';
		}
		elseif ($type == config('evibe.venue.type.hall'))
		{
			$venueType = 'hall only';
		}

		return ucfirst($venueType);
	}

	public function getSlot($slot)
	{
		$slot = TypeSlot::find($slot);
		if ($slot)
		{
			$venueSlot = $slot->name;
		}
		else
		{
			$venueSlot = 'Any Slot';
		}

		return ucfirst($venueSlot);
	}

	public function formatPriceRange($priceRange)
	{

		if (is_numeric($priceRange))
		{
			$formattedMoney = $this->formatPrice($priceRange);
		}
		else
		{
			$formattedMoney = $priceRange;
		}

		return $formattedMoney;
	}

	private function moneyFormatIndia($num)
	{
		$explrestunits = "";
		$paise = "";
		$nSign = "";

		if ($num < 0)
		{
			$numArray = explode('-', $num);
			$nSign = '-';
			$num = $numArray[1];
		}

		if (strpos($num, '.'))
		{
			$numArray = explode('.', $num);
			$paise = $numArray[1];
			$num = $numArray[0];
		}

		if (strlen($num) > 3)
		{

			$lastthree = substr($num, strlen($num) - 3, strlen($num));
			$restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);

			for ($i = 0; $i < sizeof($expunit); $i++)
			{
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0)
				{
					$explrestunits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				}
				else
				{
					$explrestunits .= $expunit[$i] . ",";
				}
			}

			$thecash = $explrestunits . $lastthree;
		}
		else
		{
			$thecash = $num;
		}

		if ($paise)
		{
			$thecash .= '.' . $paise;
		}

		if ($nSign)
		{
			$thecash = $nSign . $thecash;
		}

		return $thecash; // writes the final format where $currency is the currency symbol.
	}

	private function isExisting($vendorId, $existing)
	{
		$returnVal = false;

		foreach ($existing as $reco)
		{
			if ($reco->vendor_id == $vendorId)
			{
				$returnVal = true;
				break;
			}
		}

		return $returnVal;
	}

	// generate checkout field markup
	public function getCheckoutFieldMarkup($checkoutField, $ticket, $value = null)
	{
		$markup = "Input Markup Not Available";
		$type = $checkoutField->type_field_id;

		if (!$value)
		{
			// fetch the value of checkout field
			$checkoutFieldValue = CheckoutFieldValue::where([
				                                                'ticket_id'         => $ticket->id,
				                                                'checkout_field_id' => $checkoutField->id
			                                                ])->first();
			if ($checkoutFieldValue)
			{
				$value = $checkoutFieldValue->value;
			}
		}

		switch ($type)
		{
			case config('evibe.input.text'):
				$markup = '<input type="text"
											placeholder="Type ' . strtolower($checkoutField->identifier) . '"
											class="form-control checkout-' . $checkoutField->name . '"
											name="' . $checkoutField->name . '"
											value="' . $value . '">';
				break;
			case config('evibe.input.textarea'):
				$markup = '<textarea rows="5" placeholder="Type ' . strtolower($checkoutField->identifier) . '" class="form-control editor-plugin checkout-' . $checkoutField->name . '" name="' . $checkoutField->name . '">' . $value . '</textarea>';
				break;

			case config('evibe.input.checkbox'):
				$checkoutOptions = CheckoutFieldOptions::where('checkout_field_id', $checkoutField->id)->get();
				$valueOptionsArray = [];
				if ($value)
				{
					$value = trim($value);
					$valueOptionsArray = explode(', ', $value);
				}

				if (count($checkoutOptions))
				{
					$markup = '';
					foreach ($checkoutOptions as $checkoutOption)
					{
						$checkedText = (count($valueOptionsArray) && in_array($checkoutOption->name, $valueOptionsArray)) ? 'checked' : '';

						$markup .= '<div class="in-blk pad-r-20">';
						$markup .= '<input type="checkbox" id="' . $checkoutOption->id . '" name="' . $checkoutField->name . '" value="' . $checkoutOption->name . '" ' . $checkedText . '>';
						$markup .= '<label for="' . $checkoutOption->id . '" class="mar-l-5">' . ucwords($checkoutOption->name) . '</label>';
						$markup .= '</div>';
					}
				}
				break;
			case config('evibe.input.option'):
				$checkoutOptions = CheckoutFieldOptions::where('checkout_field_id', $checkoutField->id)->get();
				$markup = '<select name="' . $checkoutField->name . '" class="form-control checkout-' . $checkoutField->name . '">';
				foreach ($checkoutOptions as $checkoutOption)
				{
					$selectedText = $value == $checkoutOption->name ? 'selected' : '';
					$markup .= '<option value="' . $checkoutOption->name . '" ' . $selectedText . '>' . $checkoutOption->name . '</option>';
				}
				$markup .= '</select>';
				break;
		}

		return $markup;
	}

	// generate checkout field markup
	public function getMappingFieldMarkup($checkoutField, $value = null)
	{
		$markup = "Input Markup Not Available";
		$type = $checkoutField->type_field_id;

		if (!$value && 1)
		{
			// fetch the value of checkout field
			$mappingFieldValue = TicketMappingFieldValue::where([
				                                                    'ticket_mapping_id' => 1,
				                                                    'field_id'          => $checkoutField->id
			                                                    ])->first();
			if ($mappingFieldValue)
			{
				$value = $mappingFieldValue->value;
			}
		}

		switch ($type)
		{
			case config('evibe.input.text'):
				$markup = '<input type="text"
											placeholder="Type ' . strtolower($checkoutField->identifier) . '"
											class="form-control checkout-' . $checkoutField->name . '"
											name="' . $checkoutField->name . '"
											value="' . $value . '">';
				break;
			case config('evibe.input.textarea'):
				$markup = '<textarea rows="5" placeholder="Type ' . strtolower($checkoutField->identifier) . '" class="form-control editor-plugin checkout-' . $checkoutField->name . '" name="' . $checkoutField->name . '">' . $value . '</textarea>';
				break;

			case config('evibe.input.checkbox'):
				$checkoutOptions = CheckoutFieldOptions::where('checkout_field_id', $checkoutField->id)->get();
				$valueOptionsArray = [];
				if ($value)
				{
					$value = trim($value);
					$valueOptionsArray = explode(', ', $value);
				}

				if (count($checkoutOptions))
				{
					$markup = '';
					foreach ($checkoutOptions as $checkoutOption)
					{
						$checkedText = (count($valueOptionsArray) && in_array($checkoutOption->name, $valueOptionsArray)) ? 'checked' : '';

						$markup .= '<div class="in-blk pad-r-20">';
						$markup .= '<input type="checkbox" id="' . $checkoutOption->id . '" name="' . $checkoutField->name . '" value="' . $checkoutOption->name . '" ' . $checkedText . '>';
						$markup .= '<label for="' . $checkoutOption->id . '" class="mar-l-5">' . ucwords($checkoutOption->name) . '</label>';
						$markup .= '</div>';
					}
				}
				break;
			case config('evibe.input.option'):
				$checkoutOptions = CheckoutFieldOptions::where('checkout_field_id', $checkoutField->id)->get();
				$markup = '<select name="' . $checkoutField->name . '" class="form-control checkout-' . $checkoutField->name . '">';
				foreach ($checkoutOptions as $checkoutOption)
				{
					$selectedText = $value == $checkoutOption->name ? 'selected' : '';
					$markup .= '<option value="' . $checkoutOption->name . '" ' . $selectedText . '>' . $checkoutOption->name . '</option>';
				}
				$markup .= '</select>';
				break;
		}

		return $markup;
	}

	/**
	 * @return string
	 * @author : vikash <vikash@evibe.in>
	 * @Since  : 21 july 2016
	 *         this function is for getting the website link,
	 *         if new occasion or type launched on website, then add those link in this function
	 *
	 */
	public function getWebsiteLiveLink($eventId, $mapper, $mapTypeId, $cityUrl)
	{
		$fullUrl = '';

		if ($mapTypeId == config('evibe.ticket_type.packages'))
		{
			$fullUrl = $this->getPackageLiveLink($eventId, $mapper, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.venues'))
		{
			$fullUrl = $this->getVenueLiveLink($eventId, $mapper->url, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.services'))
		{
			$fullUrl = $this->getEntertainmentLiveLInk($eventId, $mapper->url, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.decors'))
		{
			$fullUrl = $this->getDecorLiveLink($eventId, $mapper->url, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.cakes'))
		{
			$fullUrl = $this->getCakeLiveLink($eventId, $mapper->url, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.trends'))
		{
			$fullUrl = $this->getTrendLiveLink($eventId, $mapper->url, $cityUrl);
		}

		return $fullUrl;
	}

	private function getEntertainmentLiveLInk($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.live.host') . '/' . $cityUrl . '/';

		if ($eventId == config('evibe.event.bachelor_party'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.bachelor.entertainment');
		}
		elseif ($eventId == config('evibe.event.pre_post'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.pre-post.entertainment');
		}
		else
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.entertainment');
		}

		return $liveUrl . '/' . $profileUrl;
	}

	private function getDecorLiveLink($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.live.host') . '/' . $cityUrl . '/';

		if ($eventId == config('evibe.event.pre_post'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.pre-post.decors');
		}
		else
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.decors');
		}

		return $liveUrl . '/' . $profileUrl;
	}

	private function getVenueLiveLink($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.live.host') . '/' . $cityUrl . '/';

		if ($eventId == config('evibe.event.pre_post'))
		{
			$liveLink = $liveHost . config('evibe.live.profile_url.pre-post.venues');
		}
		else
		{
			$liveLink = $liveHost . config('evibe.live.profile_url.venues');
		}

		return $liveLink . '/' . $profileUrl;
	}

	private function getCakeLiveLink($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.live.host') . '/' . $cityUrl . '/';

		if ($eventId == config('evibe.event.bachelor_party'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.bachelor.cakes');
		}
		elseif ($eventId == config('evibe.event.pre_post'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.pre-post.cakes');
		}
		else
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.cakes');
		}

		return $liveUrl . '/' . $profileUrl;
	}

	private function getTrendLiveLink($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.live.host') . '/' . $cityUrl . '/';
		$liveUrl = $liveHost . config('evibe.live.profile_url.trends');

		return $liveUrl . '/' . $profileUrl;
	}

	private function getPackageLiveLink($eventId, $package, $cityUrl)
	{
		$packageUrl = $package->url;
		$liveHost = config('evibe.live.host') . '/' . $cityUrl . '/';
		$url = $liveHost . config('evibe.live.profile_url.packages');

		if ($eventId == config('evibe.event.kids_birthday'))
		{
			if ($this->checkPackageType($package, config('evibe.ticket_type.venue-deals')))
			{
				$url = $liveHost . config('evibe.live.profile_url.venue-deals');
			}
		}
		elseif ($eventId == config('evibe.event.bachelor_party'))
		{
			//resort
			if ($this->checkPackageType($package, config('evibe.ticket_type.resort')))
			{
				$url = $liveHost . config('evibe.live.profile_url.bachelor.resorts');
			}
			//villa
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.villa')))
			{
				$url = $liveHost . config('evibe.live.profile_url.bachelor.villas');
			}
			//lounge
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.lounge')))
			{
				$url = $liveHost . config('evibe.live.profile_url.bachelor.lounges');
			}
			//food
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.food')))
			{
				$url = $liveHost . config('evibe.live.profile_url.bachelor.food');
			}
		}
		elseif ($eventId == config('evibe.event.special_experience'))
		{
			$url = $liveHost . config('evibe.live.profile_url.couple_experience.packages');
		}

		return $url . '/' . $packageUrl;
	}

	public function checkPackageType($package, $checkForType)
	{
		$isType = false;
		$eventId = $package->event_id;

		$tagIds = \Tags::where('type_event', $eventId)
		               ->where('map_type_id', $checkForType)
		               ->pluck('id');

		$packageTagObj = \PackageTag::whereIn('tile_tag_id', $tagIds)
		                            ->where('planner_package_id', $package->id)
		                            ->get();

		if ($packageTagObj->count() > 0)
		{
			$isType = true;
		}

		return $isType;
	}

	function renderHtmlWithUl($text)
	{
		if ((strpos($text, "<li>") >= 0) && (strpos($text, "<ul>") == false))
		{
			return "<ul>" . $text . "</ul>";
		}
		else
		{
			return $text;
		}
	}
}