<?php

namespace Evibe\Utilities;

/**
 * @author Anji <anji@evibe.in>
 * @since  2 Feb 2014
 */

use Illuminate\Support\Facades\Queue;

class SendSMS
{
	public function smsExchangeContacts($job, $data)
	{
		$this->sendViaSMSCGateway($job, $data);
	}

	public function smsPreBookToVendor($job, $data)
	{
		if (count($data))
		{
			foreach ($data as $phone => $text)
			{
				$smsData = [
					'to'   => $phone,
					'text' => $text
				];

				$this->sendViaSMSCGateway($job, $smsData);
			}
		}
	}

	public function smsPreBookToCustomer($job, $data)
	{
		$this->sendViaSMSCGateway($job, $data);
	}

	public function smsAutoBookCancelToCustomer($job, $data)
	{
		$this->sendViaSMSCGateway($job, $data);
	}

	public function smsCancellationToCustomer($job, $data)
	{
		if (!$data)
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.cancel.customer');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['partyDate'],
			'#field3#' => $data['reason'],
			'#field4#' => config('evibe.phone')
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['phone'],
			'text' => $text
		];

		$this->sendViaSMSCGateway($job, $smsData);
	}

	public function smsCancellationToVendor($job, $data)
	{
		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.cancel.vendor');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['partyDate'],
			'#field3#' => $data['customer'],
			'#field4#' => $data['reason']
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['phone'],
			'text' => $text
		];

		$this->sendViaSMSCGateway($job, $smsData);
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  27 Aug 2015
	 */
	public function smsOTPToProUser($job, $data)
	{
		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.pro_app.otp');
		$replaces = [
			'#field1#' => $data['otp'],
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);
		$smsData = [
			'to'   => $data['to'],
			'text' => $text
		];

		$this->sendViaSMSCGateway($job, $smsData);
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  17 Nov 2015
	 */
	public function smsInvibeSignupOTP($job, $data)
	{
		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe_admin.sms_tpl.invibe_app.otp');
		$replaces = [
			'#field1#' => $data['otp'],
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);
		$smsData = [
			'to'   => $data['to'],
			'text' => $text
		];

		$this->sendViaSMSCGateway($job, $smsData);
	}

	public function smsAppNotificationToVendor($job, $data)
	{
		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.pro_app.notification');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['type'],
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['to'],
			'text' => $text
		];

		$this->sendViaSMSCGateway($job, $smsData);
	}

	/**
	 * Send Custom quote & Reco notification
	 *
	 * @author Anji <anji@evibe.in>
	 * @since  20 Oct 2015
	 */
	public function smsCustomQuote($job, $data)
	{
		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.quote.customer');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['email'],
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['to'],
			'text' => $text
		];

		$this->sendViaSMSCGateway($job, $smsData);
	}

	public function smsRecoAlert($job, $data)
	{
		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.reco.customer');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['recoLink'],
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['to'],
			'text' => $text
		];

		$this->sendViaSMSCGateway($job, $smsData);
	}

	public function smsNoResponse($job, $data)
	{
		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.no_response.customer');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['to'],
			'#field3#' => $data['handlerPhone']
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['to'],
			'text' => $text
		];

		$this->sendViaSMSCGateway($job, $smsData);
	}

	/**
	 * Send Xmas Poster 2015
	 */
	public function smsXmasPosterDelivery($job, $data)
	{
		if (!count($data))
		{
			return;
		}
		$tpl = config('evibe.sms_tpl.campaigns.xmas2015');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['to']
		];
		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);
		$smsData = [
			'to'   => $data['phone'],
			'text' => $text
		];
		$this->sendViaSMSCGateway($job, $smsData);
	}

	/**
	 * Send invalid email to users
	 */
	public function smsInvalidEmail($job, $data)
	{
		if (!count($data))
		{
			return;
		}
		$tpl = config('evibe.sms_tpl.campaigns.invalid_email');

		foreach ($data as $key => $user)
		{
			$replaces = ['#field1#' => $user['name']];
			$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);
			$smsData = [
				'to'   => $user['phone'],
				'text' => $text
			];

			$this->sendViaSMSCGateway($job, $smsData, true);
			$userObj = \CampaignUser::find($user['id']);
			if ($userObj)
			{
				$userObj->update([
					                 'invalid_email_sms_sent_at' => date('Y-m-d H:i:s'),
					                 'updated_at'                => date('Y-m-d H:i:s')
				                 ]);
			}
		}

		$job->delete();
	}

	/**
	 * Send FB contest notification to users with posters
	 */
	public function smsFBContestToExistingCUsers($job, $data)
	{
		if (!count($data))
		{
			return;
		}
		$tpl = config('evibe.sms_tpl.campaigns.fb_contest');

		foreach ($data as $key => $user)
		{
			$replaces = ['#field1#' => $user['name']];
			$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);
			$smsData = [
				'to'   => $user['phone'],
				'text' => $text
			];

			$this->sendViaSMSCGateway($job, $smsData, true);
			$userObj = \CampaignUser::find($user['id']);
			if ($userObj)
			{
				$userObj->update([
					                 'contest_notified_at' => date('Y-m-d H:i:s'),
					                 'updated_at'          => date('Y-m-d H:i:s')
				                 ]);
			}
		}

		$job->delete();
	}

	/**
	 * Notify Santa Campaign to Tickets
	 */
	public function smsXmasCampaignToTickets($job, $data)
	{
		if (!count($data))
		{
			return;
		}
		$tpl = config('evibe.sms_tpl.campaigns.notify_tickets');

		foreach ($data as $key => $user)
		{
			$replaces = [
				'#field1#' => $user['name'],
				'#field2#' => $user['email']
			];
			$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);
			$smsData = [
				'to'   => $user['phone'],
				'text' => $text
			];

			$this->sendViaSMSCGateway($job, $smsData, true);
		}

		$ids = array_map(function ($ar) {
			return $ar['id'];
		}, $data);
		\TicketUnique::whereIn('id', $ids)
		             ->update([
			                      'notified_at' => date('Y-m-d H:i:s'),
			                      'updated_at'  => date('Y-m-d H:i:s')
		                      ]);

		$job->delete();
	}

	public function smsInviteCardToCustomer($job, $data)
	{
		$this->sendViaSMSCGateway($job, $data);
	}

	/**
	 * Inner Methods
	 */
	private function sendViaSMSCGateway($job, $data, $isDontDelete = false)
	{
		$username = config('smsc.username');
		$password = config('smsc.password');
		$senderId = config('smsc.sender_id');
		$smsType = config('smsc.route');
		$text = rawurlencode($data['text']);
		$to = $data['to'];

		if ($to && strlen($to) == 10)
		{
			$smsGatewayApi = "http://smsc.biz/httpapi/send?username=" . $username .
				"&password=" . $password .
				"&sender_id=" . $senderId .
				"&route=" . $smsType .
				"&phonenumber=" . $to .
				"&message=" . $text;

			$ch = curl_init($smsGatewayApi);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			curl_close($ch);

			// sms sending error occurred
			if ($result < 0)
			{
				$this->triggerErrorEmail(['to'         => $to,
				                          'text'       => $data['text'],
				                          'error_code' => $result
				                         ]);
			}
		}
		else
		{
			$this->triggerErrorEmail(['to'         => $to,
			                          'text'       => $data['text'],
			                          'error_code' => 'Invalid'
			                         ]);
		}

		if (!$isDontDelete)
		{
			$job->delete();
		}
	}

	private function triggerErrorEmail($data, $fromAppGateway = false)
	{
		$errorType = '[SMS Error]';
		if ($fromAppGateway)
		{
			$errorType = '[SMS App Error]';
		}

		$errorData = [
			'to'      => config('evibe.tech_admin_email'),
			'subject' => $errorType . ' SMS sending failed - ' . $data['to'] . " - " . $data['error_code'],
			'data'    => $data
		];

		Queue::push('\Evibe\Utilities\SendEmail@mailSMSErrorToAdmin', $errorData);
	}

}