<?php

namespace Evibe\Utilities;

/**
 * Added namespace
 *
 * @author Anji <anji@evibe.in>
 * @since  9 Jan 2015
 */

use Vendor;
use TicketUnique;
use Evibe\Facades\AppUtilFacade as AppUtilFacade;

use Aws\Laravel\AwsFacade as Aws;

class SendEmail
{
	public function mailGenericErrorToAdmin($job, $data)
	{
		$sub = '[Dash Error] Code: ' . $data['code'] . ' - ' . date('F j, Y, g:i a');

		$mailData = [
			'to'      => [config('evibe.tech_group_email')],
			'from'    => 'Evibe Errors <ping@evibe.in>',
			'replyTo' => [config('evibe.tech_group_email')],
			'cc'      => [],
			'sub'     => $sub,
			'body'    => $this->getErrorReportMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function mailTypeEmail($job, $data)
	{
		$from = 'Team Evibe.in <enquiry@evibe.in>';
		$sub = 'Your enquiry with Evibe.in';

		$mailData = [
			'to'      => [$data['to']],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'from'    => $from,
			'sub'     => $sub,
			'body'    => $this->getTypeEmailMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function mailReviewToVendor($job, $data)
	{
		$from = 'Team Evibe.in <business@evibe.in>';
		$sub = $data['subject'];

		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'from'    => $from,
			'sub'     => $sub,
			'body'    => $this->getReviewEmailMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function mailAutoBookCancellationCustomer($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'from'    => 'Team Evibe.in <enquiry@evibe.in>',
			'sub'     => $data['subject'],
			'body'    => $this->getAutoBookCancellationMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function mailAutoBookVdCancellationCustomer($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'from'    => 'Team Evibe.in <enquiry@evibe.in>',
			'sub'     => $data['subject'],
			'body'    => $this->getAutoBookVdCancellationMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function mailAutoBookCakeCancellationCustomer($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'from'    => 'Team Evibe.in <enquiry@evibe.in>',
			'sub'     => $data['subject'],
			'body'    => $this->getAutoBookCakeCancellationMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function mailMappingsReco($job, $data)
	{
		$from = ucfirst($data['handlerName']) . '<enquiry@evibe.in>';
		$to = $data['to'];
		$sub = "Regarding your event on " . $data["partyDate"] . " - #" . $data["enquiryId"];
		$subjectList = [];

		$mappings = $data['mappings'];
		$packages = $this->filterTicketMappings($mappings, config('evibe.ticket_type.packages'));
		$planners = $this->filterTicketMappings($mappings, config('evibe.ticket_type.planners'));
		$venues = $this->filterTicketMappings($mappings, config('evibe.ticket_type.venues'));
		$trends = $this->filterTicketMappings($mappings, config('evibe.ticket_type.trends'));
		$cakes = $this->filterTicketMappings($mappings, config('evibe.ticket_type.cakes'));
		$decors = $this->filterTicketMappings($mappings, config('evibe.ticket_type.decors'));
		$services = $this->filterTicketMappings($mappings, config('evibe.ticket_type.services'));

		/**
		 * @see maintain this order
		 */
		$iterate = [
			"venues"       => count($venues),
			"packages"     => count($packages) || count($planners) || count($trends),
			"decor styles" => count($decors),
			"cakes"        => count($cakes),
			"services"     => count($services)
		];

		foreach ($iterate as $key => $count) {
			if ($count) {
				array_push($subjectList, $key);
			}
		}

		$mailData = [
			'to'      => $to,
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'from'    => $from,
			'sub'     => $sub,
			'body'    => $this->getRecoEmailMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * Send confirmation email
	 *
	 * @todo
	 *  + Generate pdf with complete details and attach to email
	 */
	public function mailOrderProcessToCustomer($job, $data)
	{
		$partyDateTime = $data['additional']['partyDateTime'];
		if (isset($data['bookings']['minBookingDateTime']) && isset($data['bookings']['maxBookingDateTime'])) {
			$partyDateTime = date("d M Y", $data['bookings']['minBookingDateTime']);

			if ($data['bookings']['minBookingDateTime'] == $data['bookings']['maxBookingDateTime']) {
				$partyDateTime = date("d M Y h:i A", $data['bookings']['minBookingDateTime']);
			} elseif (date("d m y", $data['bookings']['minBookingDateTime']) != date("d m y", $data['bookings']['maxBookingDateTime'])) {
				$partyDateTime = date("d/m/y (D)", $data['bookings']['minBookingDateTime']) . ' - ' . date("d/m/y (D)", $data['bookings']['maxBookingDateTime']);
			}
		}

		$from = "Team Evibe.in <enquiry@evibe.in>";
		$sub = "[Evibe.in] " . ucfirst($data['customer']['name']) . ", advance payment link & order details for ";
		$sub .= $partyDateTime;

		$mailData = [
			'to'      => [$data['customer']['email']],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['ccAddresses'],
			'from'    => $from,
			'sub'     => $sub,
			'body'    => $this->getOrderProcessEmailToCustomerMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * Send confirmation email to Vendor
	 *
	 * @author Harish <harish.annavajjal@evibe.in>
	 * @since  3 Mar 2015
	 */
	public function mailPreBookToVendor($job, $data)
	{
		$from = "Team Evibe.in <business@evibe.in>";
		$sub = "[Evibe.in] Booking order for " . $data['partyDate'] . " by " . $data['customerName'] . " is under process";

		$mailData = [
			'to'      => [$data['to']],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['ccAddresses'],
			'from'    => $from,
			'sub'     => $sub,
			'body'    => $this->getOrderProcessEmailToVendorMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Harish <harish.annavajjala@evibe.in>
	 * @since  4 Mar 2015
	 */
	public function sendCancellationMailToCustomer($job, $data)
	{
		$mailData = [
			'to'      => [$data['email']],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyToAddresses'],
			'from'    => 'Team Evibe.in <enquiry@evibe.in>',
			'sub'     => "[Evibe.in] " . ucfirst($data['name']) . ", order cancelled for " . $data['partyDate'],
			'body'    => $this->getCancellationMailToCustomerMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function sendCancellationMailToVendor($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['ccAddresses'],
			'bcc'     => $data['bccAddresses'],
			'replyTo' => $data['replyToAddresses'],
			'from'    => 'Team Evibe.in <enquiry@evibe.in>',
			'sub'     => $data['subject'],
			'body'    => $this->getCancellationMailToVendorMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function mailSMSErrorToAdmin($job, $data)
	{
		$mailData = [
			'to'      => [$data['to']],
			'from'    => "SMS Error Alert <ping@evibe.in>",
			'replyTo' => ['ping@evibe.in'],
			'sub'     => $data['subject'],
			'body'    => isset($data['text']) ? $data['text'] : 'SMS Sending Failed, check and fix at the earliest. Data unavailable. Number will be in the subject'
		];

		$this->send($job, $mailData);
	}

	/**
	 * Send custom quotation to customer with handler, admin in cc
	 *
	 * @author Anji <anji@evibe.in>
	 * @since  25 Aug 2015
	 */
	public function mailCustomQuote($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'from'    => 'Team Evibe.in <enquiry@evibe.in>',
			'sub'     => $data['subject'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['ccAddresses'],
			'body'    => $this->getCustomQuoteMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * Send any custom email from dash
	 *
	 * @author Anji <anji@evibe.in>
	 * @since  19 Oct 2015
	 */
	public function mailAnyCustomEmail($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'from'    => AppUtilFacade::getHandlerName() . ' <ping@evibe.in>',
			'sub'     => $data['sub'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['ccAddresses'],
			'body'    => $this->getAnyCustomEmailMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @since 27 Aug 2015
	 */
	public function mailNotificationReplyToTeam($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'from'    => 'Evibe Alert <ping@evibe.in>',
			'sub'     => $data['subject'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => [],
			'body'    => $this->getNotificationReplyMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @since 21 Oct 2015
	 */
	public function mailNoResponse($job, $data)
	{
		$sub = ucfirst($data['name']) . ", regarding your enquiry for party on " . $data['partyDate'];

		$mailData = [
			'to'      => $data['to'],
			'from'    => $data["handlerFirstName"] . " from Evibe.in <enquiry@evibe.in>",
			'sub'     => $sub,
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'body'    => $this->getNoResponseMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Vikash<vikash@evibe.in>
	 * @Since  25th March 2016
	 * venue availability check status email
	 */
	public function mailVenueAvailabilityConfirm($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'from'    => 'Available<ping@evibe.in>',
			'sub'     => $data['sub'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'body'    => $this->venueAvailabilityConfirmCheckMarkup($data),
		];
		$this->send($job, $mailData);
	}

	public function mailVenueAvailabilityDeny($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'from'    => 'Unavailable<ping@evibe.in>',
			'sub'     => $data['sub'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'body'    => $this->venueAvailabilityDenyMarkup($data),
		];
		$this->send($job, $mailData);
	}

	public function mailAvailabilityAsked($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'from'    => 'Check Avail<ping@evibe.in>',
			'sub'     => $data['sub'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyTo'],
			'body'    => $this->venueAvailabilityAskMarkup($data),
		];
		$this->send($job, $mailData);
	}

	public function mailSantaPoster($job, $data)
	{
		$mailData = [
			'to'      => [$data['to']],
			'from'    => 'Evibe<ping@evibe.in>',
			'sub'     => 'Your X-Mas poster with Santa',
			'cc'      => $data['ccAddresses'],
			'bcc'     => ['swathi@evibe.in'],
			'replyTo' => $data['replyTo'],
			'body'    => $this->getSantaPosterMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function notifyXmasCampaignToTickets($job, $data)
	{

		foreach ($data as $key => $user) {
			$mailData = [
				'to'      => [$user['email']],
				'from'    => 'Evibe<ping@evibe.in>',
				'cc'      => $user['altEmail'] ? [$user['altEmail']] : [],
				'sub'     => '[Evibe.in] Meet & snap with our Live SANTA #MeWithSanta',
				'replyTo' => ['ping@evibe.in'],
				'body'    => $this->getSantaCampaignEmailMarkup($user)
			];

			$this->send($job, $mailData);
		}

		$ids = array_map(function ($ar) {
			return $ar['id'];
		}, $data);
		TicketUnique::whereIn('id', $ids)->update([
			'notified_at' => date('Y-m-d H:i:s'),
			'updated_at'  => date('Y-m-d H:i:s')
		]);
	}

	/**
	 * Inner methods
	 */
	private static function send($job, $data)
	{
		$ses = Aws::createClient('Ses');
		$ses->sendEmail([
			'Source'           => $data['from'],
			'ReplyToAddresses' => $data['replyTo'],
			'Destination'      => [
				'ToAddresses'  => $data['to'],
				'CcAddresses'  => array_key_exists('cc', $data) ? $data['cc'] : [],
				'BccAddresses' => array_key_exists('bcc', $data) ? $data['bcc'] : []
			],
			'Message'          => [
				'Subject' => [
					'Data' => $data['sub']
				],
				'Body'    => [
					'Html' => [
						'Data' => $data['body']
					]
				]
			]
		]);
		$job->delete();
	}

	private static function getOrderProcessEmailToCustomerMarkup($data)
	{
		$termsMarkup = file_get_contents(base_path('resources/views/emails/orders/terms.html'));
		$cancellationMarkup = file_get_contents(base_path('resources/views/emails/orders/cancellation.html'));
		$extraInfo = "";

		$card1Markup = file_get_contents(base_path('resources/views/emails/orders/op_card1.html'));
		$card1Replaces = [
			'@date@'       => date('d/m/y h:i A'),
			'@evibePhone@' => config('evibe.phone'),
			'@custName@'   => $data['customer']['name'],
			'@advanceAmt@' => AppUtilFacade::formatPrice($data['bookings']['totalAdvanceToPay']),
			'@deadline@'   => $data['bookings']['deadline'],
			'@pgLink@'     => $data['bookings']['paymentLink'],
			'@thanks@'     => self::getThanksMarkup()
		];
		$card1Markup = str_replace(array_keys($card1Replaces), array_values($card1Replaces), $card1Markup);

		$card2Markup = file_get_contents(base_path('resources/views/emails/orders/op_card2.html'));
		$card2Replaces = [
			'@custName@'  => $data['customer']['name'],
			'@custPhone@' => $data['customer']['phone'],
			//'@partyDate@' => $data['additional']['partyDateTime']
		];
		//if (isset($data['bookings']['minBookingDateTime']) && isset($data['bookings']['maxBookingDateTime']))
		//{
		//	if ($data['bookings']['minBookingDateTime'] < $data['bookings']['maxBookingDateTime'])
		//	{
		//		$card2Replaces['@partyTimings@'] = date("d/m/y H:i", $data['bookings']['minBookingDateTime']) . " - " . date("d/m/y H:i",$data['bookings']['maxBookingDateTime']);
		//	}
		//	else
		//	{
		//		$card2Replaces['@partyTimings@'] = date("d/m/y H:i", $data['bookings']['minBookingDateTime']);
		//	}
		//}
		//else
		//{
		//	$card2Replaces['@partyTimings@'] = $data['additional']['partyDateTime'];
		//}
		$card2Markup = str_replace(array_keys($card2Replaces), array_values($card2Replaces), $card2Markup);
		$card3Markup = "";
		foreach ($data['bookings']['list'] as $booking) {
			$advTxt = '<div><b>Advance Amount</b></div>';
			if ($booking['advancePaid'] == 1) {
				$advTxt = '<div style="color:#00A800"><b>Advance Paid</b></div>';
			}
			$advancePaidMarkup = '<li style="display:table-cell;">' . $advTxt;
			$advancePaidMarkup .= '<div>Rs. ' . AppUtilFacade::formatPrice($booking['advanceAmount']) . '</div></li>';
			$bookingInfo = $booking['bookingInfo'];
			$bookingPreReq = $booking['prerequisites'];
			$bookingFacts = $booking['facts'];
			//Add some extra field when venue booking payment link email sent
			if ($booking['isVenue'] == 1) {
				$card3RepeatWithVenue = file_get_contents(base_path('resources/views/emails/orders/op_card3_with_venue.html'));
				$venueNameMarkup = "";
				if ($booking['hasShowVenueName']) {
					$venueNameMarkup = '<li style="display:table-cell;">
	                                         <div><b>Venue Name</b></div>
				                             <div>' . $booking['provider']['name'] . '</div>
	             	                   </li>';
				}

				$card3WithVenueReplace = [
					'@orderId@'         => $booking['bookingId'],
					'@venueNameMarkup@' => $venueNameMarkup,
					'@venuePerson@'     => $booking['provider']['person'],
					'@partyDateTime@'   => date("d/m/y g:i A", $booking['partyDateTime']),
					'@advAmount@'       => AppUtilFacade::formatPrice($booking['advanceAmount']),
					'@orderAmount@'     => AppUtilFacade::formatPrice($booking['bookingAmount']),
					'@bookingInfo@'     => $bookingInfo,
				];

				$card3Markup .= str_replace(array_keys($card3WithVenueReplace), array_values($card3WithVenueReplace), $card3RepeatWithVenue);
			} else {
				$card3Repeat = file_get_contents(base_path('resources/views/emails/orders/op_card3.html'));
				$card3Replaces = [
					'@orderId@'           => $booking['bookingId'],
					'@evtMng@'            => $booking['provider']['person'],
					'@ordAmt@'            => AppUtilFacade::formatPrice($booking['bookingAmount']),
					'@partyDateTime@'     => date("d/m/y g:i A", $booking['partyDateTime']),
					'@advancePaidMarkup@' => $advancePaidMarkup,
					'@bookingInfo@'       => $bookingInfo
				];
				$card3Markup .= str_replace(array_keys($card3Replaces), array_values($card3Replaces), $card3Repeat);
			}
			// add pre-reqs / facts
			if ($bookingFacts || $bookingPreReq || $booking['booking']->gallery->count() || $booking['booking']->checkoutFields->count()) {
				$extraInfo = '<div style="background-color:#FFFFFF; padding: 0 20px 20px 20px">';

				if ($booking['booking']->gallery->count()) {
					$extraInfo .= "<div style='margin-bottom:15px'>
                                    <div><b>Reference Image(s)</b></div>";
					foreach ($booking['booking']->gallery as $image) {
						$extraInfo .= '<a href="' . $image->getOriginalImagePath() . '" style="text-decoration:none">
										 <img src="' . $image->getResultsImagePath() . '" alt="' . $image->title . '" style="max-width:160px; max-height:100px; margin:10px 10px 0 0; border:1px solid #d9d9d9">
									   </a>';
					}

					$extraInfo .= "</div>";
				}

				if ($booking['booking']->checkoutFields->count()) {

					foreach ($booking['booking']->checkoutFields as $checkoutField) {
						if (!empty($data['additional'][$checkoutField->name])) {
							$extraInfo .= "<div style='margin-bottom:15px'>";
							$extraInfo .= "<div><b>" . ucwords($checkoutField->identifier) . "</b></div>
										<div>" . $data['additional'][$checkoutField->name] . "</div>";
							$extraInfo .= "</div>";
						}
					}
				}

				if ($bookingPreReq) {
					$extraInfo .= "<div style='margin-bottom:15px'>
									<div><b>Prerequisites</b></div>
                                    <div>" . $bookingPreReq . "</div>
								</div>";
				}

				if ($bookingFacts) {
					$extraInfo .= "<div style='margin-bottom:20px;'>
										<div><b>Facts</b></div>
										<div>" . $bookingFacts . "</div>
									</div>";
				}

				$extraInfo .= "</div>";
				$card3Markup = $card3Markup . $extraInfo;
			}
		}

		$markup = "<div style='background-color:#d9d9d9;padding:30px 20px;width:700px;font-size:14px;line-height:22px;'>";
		$markup .= $card1Markup . $card2Markup . $card3Markup;
		$markup .= $cancellationMarkup . $termsMarkup;
		$markup .= "</div>"; // close
		$markup .= "<div style='padding-top:10px;font-size:12px;color:#999'>If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>";

		return $markup;
	}

	private static function getOrderProcessEmailToVendorMarkup($data)
	{
		$body = "
			<div style='background-color:#F5F5F5;padding:10px;max-width:700px;'>
				<div style='padding: 20px 30px 10px 30px;background-color: #FFFFFF;'>
					<div>
						<div style='float:left;'>
							<div>
								<a href='http://evibe.in'>
									<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
								</a>
								<div style='padding-top:10px;'>Phone: " . config('evibe.phone') . "</div>
							</div>
						</div>
						<div style='float:right;'>
							<div style='padding-top:10px;'>
								<span>Booking Date: </span>
								<span>" . date('d/m/y h:i A') . "</span>
							</div>
						</div>
						<div style='clear:both;'></div>
					</div>
					<hr>
					<div style='padding-top: 10px;'>
						<p>Dear " . $data['vendorName'] . ",</p>
						<p>We are pleased to inform you that booking for " . $data['partyDate'] . " is in process. Please check below for more details.</p>";
		$fullLocation = $data['partyLocation'];
		if ($data['zipCode']) {
			$fullLocation .= ", Pin code: " . $data['zipCode'];
		}
		if ($data['landmark']) {
			$fullLocation .= ", Landmark: " . $data['landmark'];
		}
		$body .= "<div style='padding-top: 10px;'>
					<div style='color:#EF3E75;'><b>Customer Details: </b></div>
					<div style='padding-top:10px;'>
						<table style='max-width:600px;border:1px solid #AAA;border-collapse:collapse;'>
							<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Name: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $data['customerName'] . "</td>
							</tr>
							<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Location: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $fullLocation . "</td>
							</tr>
							<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Party time: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $data['partyDateTime'] . "</td>
							</tr>";

		$body .= "<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Order Details: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $data['bookingInfo'] . "</td>
							</tr>";

		if ($data['booking']->gallery->count() > 0) {
			$body .= "<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Reference Image(s)</b></td>
								<td style='border:1px solid #AAA;padding:4px 4px 4px 7px;'>";
			foreach ($data['booking']->gallery as $gallery) {
				$body .= '<a href="' . $gallery->getOriginalImagePath() . '" alt="' . $gallery->name . '" style="text-decoration:none">
								<img src="' . $gallery->getResultsImagePath() . '" style="max-width:160px;max-height:100px;margin:0 5px 5px 0; border:1px solid #d9d9d9">
						 </a>';
			}

			$body .= "</td>
						</tr>";
		}

		if ($data['booking']->checkoutFields->count()) {
			foreach ($data['booking']->checkoutFields as $checkoutField) {
				if (!empty($data['additional'][$checkoutField->name])) {
					$body .= "<tr>
						<td style='border:1px solid #AAA;padding:4px;'><b>" . ucwords($checkoutField->identifier) . " </b></td>
						<td style='border:1px solid #AAA;padding:4px;'>" . $data['additional'][$checkoutField->name] . "</td>
						</tr>";
				}
			}
		}

		if ($data['prerequisites']) {
			$body .= "<tr>
						<td style='border:1px solid #AAA;padding:4px;'><b>Prerequisites: </b></td>
						<td style='border:1px solid #AAA;padding:4px;'>" . $data['prerequisites'] . "</td>
					</tr>";
		}

		if ($data['facts']) {
			$body .= "<tr>
						<td style = 'border:1px solid #AAA;padding:4px;'><b>Facts: </b></td>
						<td style = 'border:1px solid #AAA;padding:4px;'> " . $data['facts'] . " </td>
					</tr>";
		}

		$body .= "</table></div></div>";

		$body .= "<div style='padding-top: 10px;'>
					<p>Once the advance has been paid by the customer, you will be notified with complete details.</p>
					<p>Please reply back if you are not able to meet any of the above mentioned details.</p>
					<p>" . self::getPartnerSalutationMarkup() . "</p>
				</div>";

		return $body;
	}

	private static function getCancellationMailToCustomerMarkup($data)
	{
		$body = "
			<div style='background-color:#F5F5F5;padding:10px;max-width:700px;'>
				<div style='padding: 20px 30px 10px 30px;background-color: #FFFFFF;'>
					<div>
						<div style='float:left;'>
							<div>
								<a href='http://evibe.in'>
									<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
								</a>
								<div style='padding-top:10px;'>Phone: +91 9640204000</div>
							</div>
						</div>
						<div style='clear:both;'></div>
					</div>
					<hr>
					<div style='padding-top: 10px;'>
						<p>Dear " . $data['name'] . ",</p>
						<p>We regret to inform you that we had to cancel your party order of " . $data['partyDate'] . " because <b>" . $data['reason'] . "</b>";
		$body .= "<p>In case you have any queries, please feel free to contact us back.</p>";
		$body .= "<p>Phone Number: +91 9640204000<br>E-mail: <u>ping@evibe.in</u></p>";
		$body .= "<p>" . self::getThanksMarkup() . "Team Evibe.in</p>";

		return $body;
	}

	private function getCancellationMailToVendorMarkup($data)
	{
		$body = "
			<div style='background-color:#F5F5F5;padding:10px;max-width:700px;'>
				<div style='padding: 20px 30px 10px 30px;background-color: #FFFFFF;'>
					<div>
						<div style='float:left;'>
							<div>
								<a href='http://evibe.in'>
									<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
								</a>
								<div style='padding-top:10px;'>Phone: +91 9640204000</div>
							</div>
						</div>
						<div style='clear:both;'></div>
					</div>
					<hr>
					<div style='padding-top: 10px;'>
						<p>Dear " . $data['vendorName'] . ",</p>
						<p>We regret to inform you that we had to cancel your party order for " . $data['partyDate'] . '. Please check below for more details.
						<table border="1" style="border-collapse:collapse;margin-top:15px">
							<tr>
								<td style="padding:8px"><b>Customer Name</b></td>
								<td style="padding:8px">' . $data['customerName'] . '</td>
							</tr>
							<tr>
								<td style="padding:8px"><b>Party Date</b></td>
								<td style="padding:8px">' . $data['partyDate'] . '</td>
							</tr>
							<tr>
								<td style="padding:8px"><b>Party Location</b></td>
								<td style="padding:8px">' . $data['location'] . '</td>
							</tr>
							<tr>
								<td style="padding:8px"><b>Reason</b></td>
								<td style="padding:8px">' . $data['reason'] . '</td>
							</tr>
						</table>';

		$body .= "<p>In case you have any queries, please feel free to contact us back.</p>";
		$body .= "<p>Phone Number: +91 9640204000<br>E-mail: <u>ping@evibe.in</u></p>";
		$body .= "<p>" . $this->getPartnerSalutationMarkup() . "</p>";

		return $body;
	}

	private static function getTypeEmailMarkup($data)
	{
		$body = '
			<div style="background-color:#F5F5F5;padding:10px;max-width:700px;">
				<div style="padding: 20px 30px 10px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="http://evibe.in">
									<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Phone: </span>
								<span>' . config('evibe.phone') . '</span>
							</div>
							<div style="padding-top:10px;">Website: <a href="http://evibe.in">www.evibe.in</a></div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>
					<div style="padding-top: 10px;">
						<p>Dear ' . $data["name"] . ',</p>';

		switch ($data['type']) {
			case 1:
				$body .= '<p>This is regarding your party enquiry. We tried calling you and there was no answer.</p><p>If your request is still open, you can give us a call on ' . config('evibe.phone') . ' to take it further.</p>';
				break;

			case 2:
				$body .= '<p>This is regarding your party enquiry. We tried calling you on the contact number submitted on our website and it seems to be an incorrect number.</p><p>If your request is still open, you can give us a call on ' . config('evibe.phone') . ' to take it further.</p>';
				break;

			case 3:
				$body .= '<p>This is regarding your party enquiry. We are very sorry to let you know that there are no vendor services that match your expectations / request details.</p><p> We wish you a great party!';
				break;
		}

		$body .= '		<p>
							<div>' . self::getThanksMarkup() . '</div>
							<div>Team Evibe.in</div>
						</p>
					</div>
				</div>
				<div style="padding:0 30px 10px 30px;color:#999;">
					<div style="padding-top:10px;">Like us on Facebook: <a href="http://facebook.com/evibe.in">http://facebook.com/evibe.in</a> and show us your love :-)</div>
				</div>
			</div>
			<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as "not spam" and add senders id to contact list or safe list.</div>
			<div style="padding-top:10px;font-size:10px;color:#333">
				<div>Disclaimer</div>
				<div>The information contained in this electronic message and any attachments to this message are intended for the exclusive use of the addressee(s) and may contain proprietary, confidential or privileged information. If you are not the intended recipient, you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately and destroy all copies of this message and any attachments.</div>
			</div>
		';

		return $body;
	}

	private static function getReviewEmailMarkup($data)
	{
		$body = '
			<div style="background-color:#F5F5F5;padding:10px;max-width:700px;">
				<div style="padding: 20px 30px 10px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="http://evibe.in">
									<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Phone: </span>
								<span>' . config('evibe.phone') . '</span>
							</div>
							<div style="padding-top:10px;">Website: <a href="http://evibe.in">www.evibe.in</a></div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>
					<div style="padding-top: 10px;">
						<p>Namaste ' . $data["vendorName"] . ',</p>
						<p>Greetings from Evibe.in. Trust you are doing great.</p>
						<p>We have received a new review for your party services. Please check below for more details.</p>
						<p>
							<table style="max-width:600px;border:1px solid #AAA;border-collapse:collapse;">
								<tr>
									<td style="border:1px solid #AAA;padding:4px;"><b>Name</b></td>
									<td style="border:1px solid #AAA;padding:4px;">' . $data["clientName"] . '</td>
								</tr>
								<tr>
									<td style="border:1px solid #AAA;padding:4px;"><b>Party Date</b></td>
									<td style="border:1px solid #AAA;padding:4px;">' . $data["partyDate"] . '</td>
								</tr>';
		if (array_key_exists('additionalRatings', $data) && $data['additionalRatings']) {
			foreach ($data['additionalRatings'] as $additionalRating) {
				$body .= '<tr>
							<td style="border:1px solid #AAA;padding:4px;"><b>' . $additionalRating['question'] . '</b></td>
							<td style="border:1px solid #AAA;padding:4px;">' . $additionalRating['rating'] . ' / ' . $additionalRating['maxRating'] . '</td>
						 </tr>';
			}
		}
		$body .= '<tr>
									<td style="border:1px solid #AAA;padding:4px;"><b>Review</b></td>
									<td style="border:1px solid #AAA;padding:4px;">' . $data["reviewData"] . '</td>
								</tr>
							</table>
						</p>
						<p>If you would like to thank the client (or) have something to tell, please reply to this email, it will be added to your profile.</p>
						<p>' . self::getPartnerSalutationMarkup() . '</p>
					</div>
				</div>
				<div style="padding:0 30px 10px 30px;color:#999;">
					<div style="padding-top:10px;">Like us on Facebook: <a href="http://facebook.com/evibe.in">http://facebook.com/evibe.in</a> for all the latest updates.</div>
				</div>
			</div>
			<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as "not spam" and add senders id to contact list or safe list.</div>
			<div style="padding-top:10px;font-size:10px;color:#333">
				<div>Disclaimer</div>
				<div>The information contained in this electronic message and any attachments to this message are intended for the exclusive use of the addressee(s) and may contain proprietary, confidential or privileged information. If you are not the intended recipient, you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately and destroy all copies of this message and any attachments.</div>
			</div>
		';

		return $body;
	}

	private function getRecoEmailMarkup($data)
	{
		$greetings = 'Based on your preferences, we\'ve sent you the best recommendations for your party. Please click on the button below to check and shortlist the options as per your choice.';
		$recommendationButton = "<div style='text-align:center;margin:35px auto;'>
									<a href = " . $data['recommendationUrl'] . " style=' text-decoration:none; border-radius: 4px;text-align: center; border:0;font-weight:500; background-color:#fa4052;color:#fffff1;
                                         font-size:20px;padding:10px 12px;'>Click To Shortlist Recommendations</a>
								</div>";
		$questions = "If you have any trouble in shortlisting the options from the above link, please reply to this email. We will respond to you within 1 Business hour.";

		$nextSteps = file_get_contents(base_path('resources/views/emails/tickets/process.html'));
		$whyUs = file_get_contents(base_path('resources/views/emails/tickets/why-us.blade.php'));

		$body = '<p>Namaste ' . $data["name"] . ',</p>' .
			$greetings .
			$recommendationButton .
			'<div style="padding:15px 0;">' . $questions . '</div>' .
			$nextSteps . $whyUs .
			'<p style="padding-top:10px;"><b><u>Note:</u> Everything is subjected to availability.</b></p>
						<p style="padding-top:3px;">
							<div>' . self::getThanksMarkup() . '</div>
							<div>' . $data["handlerName"] . '</div>
							<div>' . config('evibe.phone') . '</div>
						</p> 			
		';

		return $body;
	}

	private function filterTicketMappings($mappings, $mapTypeId)
	{
		return array_filter($mappings, function ($item) use ($mapTypeId) {
			if ($item['map_type_id'] == $mapTypeId) {
				return true;
			}
		});
	}

	private function getCustomQuoteMarkup($data)
	{
		// greetings
		$markup = 'Namaste ' . $data['customerName'] . ', <br/><br/>';

		// opening
		if ($data['mailOpening']) {
			$markup .= $data['mailOpening'];
		} else {
			$markup .= 'It was great talking to you. As per our conversation, here are the details: <br/><br/>';
		}

		// quote mappings
		if (count($data['quotes'])) {
			$markup .= '<ul>';

			foreach ($data['quotes'] as $quoteMapping) {
				$markup .= '<li style="padding-bottom: 6px;">';

				if ($quoteMapping['publicLink']) {
					$markup .= '<a href="' . $quoteMapping['publicLink'] . '" target="_blank" style="text-decoration:none;margin-right:5px;">' .
						'<span style="
                        color: #fff;
                        font-size:12px;
                        font-weight:normal;
                        background-color: #ed3e72;
                        box-sizing: border-box;
                        border-radius: 3px;
                        line-height: 1.5;
                        padding: 3px 6px;">'
						. 'View Photos' .
						'</span>' . '</a>';

					$markup .= '<a href="' . $quoteMapping['publicLink'] . '" target="_blank">' . $quoteMapping['name'] . ' - ';
					$markup .= AppUtilFacade::formatPriceRange($quoteMapping['price']) . '</a>';
				} else {
					$markup .= '<b>' . $quoteMapping['name'] . ' - ' . $quoteMapping['price'] . '</b>';
				}

				$markup .= '<ul><li>' . $quoteMapping['info'] . '</li></ul>';
				$markup .= '</li>';
			}

			$markup .= '</ul>';
		}

		// quote suggestions
		if (count($data['suggestions'])) {
			$markup .= 'Relevant packages at best prices:<br/>';
			$markup .= '<ul>';

			foreach ($data['suggestions'] as $quoteSuggestion) {
				if ($quoteSuggestion['publicLink']) {
					$markup .= '<li style="padding-bottom: 6px;">';
					$markup .= '<a href="' . $quoteSuggestion['publicLink'] . '" target="_blank" style="text-decoration:none;margin-right:5px;">' .
						'<span style="
                        color: #fff;
                        font-size:12px;
                        font-weight:normal;
                        background-color: #ed3e72;
                        box-sizing: border-box;
                        border-radius: 3px;
                        line-height: 1.5;
                        padding: 3px 6px;">'
						. 'View Photos' .
						'</span>' . '</a>';
					$markup .= '<a href="' . $quoteSuggestion['publicLink'] . '" target="_blank">' . $quoteSuggestion['name'] . ' - ';
					$markup .= AppUtilFacade::formatPriceRange($quoteSuggestion['price']) . '</a>';
					$markup .= '</li>';
				}
			}

			$markup .= '</ul>';
		}

		// ending
		if ($data['mailEnding']) {
			$markup .= $data['mailEnding'];
		}

		// next steps
		$markup .= file_get_contents(base_path('resources/views/emails/tickets/process.html'));

		// ps statement
		$markup .= '<br/><br/>';
		$markup .= '<u>PS:</u> Everything is subjected to availability and transport charges might be applicable based on your location.';

		// signature
		$markup .= '<br/><br/>';
		$markup .= $this->getThanksMarkup();
		$markup .= $data['handlerName'];

		return $markup;
	}

	private function getAnyCustomEmailMarkup($data)
	{
		$markup = 'Namaste ' . $data['customerName'] . ', <br/><br/>'; // greetings
		$markup .= $data['opening'] . '<br/><br/>';
		$markup .= $data['body'];
		$markup .= $data['ending'] . '<br/><br/>';
		$markup .= $data['sig'];

		return $markup;
	}

	private function getNotificationReplyMarkup($data)
	{
		// greetings
		$markup = 'Hi ' . $data['handlerName'] . ', <br/><br/>';

		// body
		$markup .= 'You have received reply from a vendor. Please check below for more details. <br/><br/>';

		$markup .= '<table style="max-width:600px;border:1px solid #AAA;border-collapse:collapse;">
						<tr>
							<td style="border:1px solid #AAA;padding:4px;"><b>Reply Text<b></td>
							<td style="border:1px solid #AAA;padding:4px;">' . $data['replyText'] . '</td>
						</tr>
						<tr>
							<td style="border:1px solid #AAA;padding:4px;"><b>Reply Decision<b></td>
							<td style="border:1px solid #AAA;padding:4px;">' . $data['replyDecision'] . '</td>
						</tr>
						<tr>
							<td style="border:1px solid #AAA;padding:4px;"><b>Replied At<b></td>
							<td style="border:1px solid #AAA;padding:4px;">' . $data['repliedAt'] . '</td>
						</tr>
						<tr>
							<td style="border:1px solid #AAA;padding:4px;"><b>Ticket Name<b></td>
							<td style="border:1px solid #AAA;padding:4px;">' . $data['ticketName'] . '</td>
						</tr>
						<tr>
							<td style="border:1px solid #AAA;padding:4px;"><b>Party Date<b></td>
							<td style="border:1px solid #AAA;padding:4px;">' . $data['ticketPartyDate'] . '</td>
						</tr>
						<tr>
							<td style="border:1px solid #AAA;padding:4px;"><b>Ticket Id<b></td>
							<td style="border:1px solid #AAA;padding:4px;">' . $data['ticketId'] . '</td>
						</tr>
					</table>';

		$markup .= '<br/>Please do the needful at the earliest.';
		$markup .= '<br><br>' . $this->getThanksMarkup() . 'Admin';

		return $markup;
	}

	private function getNoResponseMarkup($data)
	{
		$markup = 'Namaste ' . $data['name'] . ',<br/><br/>';
		$markup .= 'I am ' . ucfirst($data['handlerName']) . ' from Evibe.in: your favourite party planner. ';
		if (isset($data['partyDate']) && $data['partyDate']) {
			$markup .= 'This email is regarding your enquiry for party on <b>' . $data['partyDate'] . '</b>. ';
		}
		$markup .= 'I tried calling you on <b>' . $data['phone'] . '</b>, but could not connect. ';
		$markup .= 'I hope your enquiry is still open. Please call me back on ' . $data['handlerPhone'] . ' (or) ' . config('evibe.operations.phone') . ' to discuss complete details. You can also reply to this email as per your convenience.<br/><br/>';

		$markup .= $this->getThanksMarkup();
		$markup .= $data['handlerName'] . '<br/>';
		$markup .= $data['handlerPhone'] . ', ' . config('evibe.operations.phone') . '<br/>';
		$markup .= 'www.evibe.in';

		return $markup;
	}

	public function venueAvailabilityAskMarkup($data)
	{
		$markup = ' <div style = "border:16px solid #f5f5f5;font-size:12px;padding:20px">';
		$markup .= 'Hi Business Team, <br ><br > ';
		$markup .= 'Please check availability of the following venue(s) for <b>' . $data['customerName'] . '\'s </b> party and revert at the earliest.<br><br>';

		$markup .= '<div style="margin-top: 13px font-size:15px"><b>Customer Requirements</b>';
		$markup .= '<table border="1"  style="border-collapse: collapse; border: 1px solid black;width:60%;margin-top:10px;">
					<tr>
						<td style="padding:5px;width:170px;"><b>Customer Name</b></td>
						<td style="padding:5px">' . $data['customerName'] . '</td>
					</tr>
					<tr>
						<td style="padding:5px;width:170px;"><b>Enquiry Id</b></td>
                        <td style = "padding:5px"><a href=' . $data['ticketLink'] . '>' . $data['enquiryId'] . '</a></td>
					</tr> ';
		$markup .= '<tr>
						<td style="padding:5px;width:170px;"><b>Party Date ( pref 1 )</b>
						<div>' . $data['date1'] . '</div>
						</td>
						<td style="padding:5px"><b style ="color:#ed3e72">' . $data['slot1'] . '</b></td>
					</tr>';
		if ($data['date2']) {
			$markup .= '<tr>
							<td style="padding:5px;width:170px;"><b>Party Date ( pref 2 )</b>
							<div>' . $data['date2'] . '</div>
							</td>
							<td style="padding:5px"><b style ="color:#ed3e72">' . $data['slot2'] . '</b></td>
						</tr>';
		}
		if ($data['date3']) {
			$markup .= '<tr>
							<td style="padding:5px;width:170px;"><b>Party Date ( pref 3 )</b>
							<div>' . $data['date3'] . '</div>
							</td>
							<td style="padding:5px"><b style ="color:#ed3e72">' . $data['slot3'] . '</b></td>
						</tr>';
		}
		$markup .= '<tr>
						<td style="padding:5px;width:170px;"><b>Budget</b></td>
						<td style="padding:5px">' . $data['budget'] . '</td>
					</tr>
					<tr>
						<td style="padding:5px;width:170px;"><b>Guests </b></td>
						<td style="padding:5px">' . $data['guests'] . '</td>
					</tr>
			        <tr>
						<td style="padding:5px;width:170px;"><b>Type</b> </td>
						<td style="padding:5px">' . $data['type'] . '</td>
					</tr>';

		if ($data['comments']) {
			$markup .= '<tr>
							<td style="padding:5px;width:170px;"><b>Comments</b></td>
							<td style="padding:5px">' . $data['comments'] . '</td>
						</tr>';
		}
		$markup .= '<tr></table></div>';

		$markup .= '<div style="margin-top: 15px;font-size:13px"><b>Venue Details</b>';

		$markup .= '<table border="1"  style="border-collapse: collapse; border: 1px solid black;width:60%;margin-top:10px;">
						<tr>
							<th style="padding:5px">Venue</th><th>Hall</th><th>Code</th><th>Person</th><th>Phone</th>
						</tr>';
		foreach ($data['availabilityData'] as $venueAsked) {
			$markup .= '<tr>
							<td style="padding:5px">' . $venueAsked['venue'] . '</td>
							<td style="padding:5px">' . $venueAsked['hall'] . '</td>
							<td style="padding:5px"><a href=' . $venueAsked['link'] . '>' . $venueAsked['code'] . '</td>
							<td style="padding:5px">' . $venueAsked['person'] . '</td>
							<td style="padding:5px">' . $venueAsked['phone'] . '</td>
						</tr>';
		}

		$markup .= '</table></div>';

		$markup .= '</table><br>Please inform us about the availability as soon as possible.<br><br>';
		$markup .= $this->getThanksMarkup();
		$markup .= 'CRM Team<br></div>';

		return $markup;
	}

	private function venueAvailabilityConfirmCheckMarkup($data)
	{
		$markup = '<div style="border: 22px solid #f5f5f5; padding:20px;">';
		$markup .= 'Hi CRM Team, <br><br>';
		$markup .= 'The following venues are <b> available </b> for ' . $data['name'] . '\'s party.';
		$markup .= '<div style="margin-top:10px;" ><b>Customer Details</b></div>';

		$markup .= '<table border="1" style="border-collapse: collapse;margin-top:5px;width:70%">
						 <tr>
                                <td style="padding:5px;width:170px;"><b>Customer Name</b></td>
                                <td style="padding:5px">' . $data['name'] . '</td>

                            </tr>
							<tr>
								<td style="padding:5px;width:170px;"><b>Enquiry Id</b></td>
		                        <td style = "padding:5px"><a href=' . $data['ticketLink'] . '>' . $data['enquiryId'] . '</a></td>
							</tr>
                            <tr>
                                <td style="padding:5px;width:170px;"><b>Customer Phone</b></td>
                                <td style="padding:5px">' . $data['phone'] . '</td>
                            </tr>
                             <tr>
                                <td style="padding:5px;width:170px;"><b>Email</b></td>
                                <td style="padding:5px">' . $data['email'] . '</td>
                            </tr>

					</table>';

		$markup .= '<div style=" margin-top:10px; margin-bottom:5px" ><b>Venue Details</b></div>';

		foreach ($data['venueData'] as $venueData) {

			$markup .= '<table border="1" style="border-collapse: collapse;margin-bottom:10px;width:70%">
							<tr>
								<td colspan="2" style="padding:5px"><div style="padding:5px;font-weight: bold"  >' . $venueData['venue'] . ' [ <a href=' . $venueData['link'] . '>' . $venueData['code'] . ' </a>]</div></td>
                            </tr>';

			if ($venueData['date1']) {
				$markup .= '<tr>
								<td style="padding:5px;width:180px;"><b>Slots Available (pref 1)</b><div>' . $venueData['date1'] . '</td>
								<td style="padding:5px"><b style="color:#ed3e72">' . $venueData['slot1'] . '</b></td>
							</tr>';
			}
			if ($venueData['date2']) {
				$markup .= '<tr>
								<td style="padding:5px;width:180px;"><b>Slots Available (pref 2)</b><div>' . $venueData['date2'] . '</td>
								<td style="padding:5px"><b style="color:#ed3e72">' . $venueData['slot2'] . '</b></td>
							</tr>';
			}
			if ($venueData['date3']) {

				$markup .= '<tr>
								<td style="padding:5px;width:180px;"><b>Slots Available (pref 3)</b><div>' . $venueData['date3'] . '</td>
								<td style="padding:5px"><b style="color:#ed3e72">' . $venueData['slot3'] . '</b></td>
							</tr>';
			}
			$markup .= '</td></tr>
						<tr>
							<td style="padding:5px;width:180px;"><b> Tax % </b></td>
							<td style="padding:5px">' . $venueData['tax'] . '</td>
						</tr>';
			if ($venueData['pricePerPerson']) {
				$markup .= '<tr>
								<td style="padding:5px;width:180px;"><b>Price / Person</b></td>
								<td style="padding:5px">' . $venueData['pricePerPerson'] . '</td>
							</tr>';
			}
			$markup .= '<tr><td style="padding:5px;width:180px;"><b>Contact Exchanged ?</b></td>';
			if ($venueData['contactExchanged'] == 1) {
				$markup .= '<td style="padding:5px"><span style="color:#32CD32"><b> YES </b></span></td>';
			} elseif ($venueData['contactExchanged'] == 0) {
				$markup .= '<td style="padding:5px"> <span style="color:#FF2400"><b> NO </b></span></td>';
			}
			$markup .= '</tr>';
			if ($venueData['comments']) {
				$markup .= '<tr>
								<td style="padding:5px;width:180px;"><b>Comments</b></td>
								<td style="padding:5px" >' . $venueData['comments'] . '</td>
							</tr>';
			}

			$markup .= '</table ><br>';
		}

		$markup .= $this->getThanksMarkup() . 'Team Business</div>';

		return $markup;
	}

	public function venueAvailabilityDenyMarkup($data)
	{
		$markup = '<div style="border: 22px solid #f5f5f5; padding:20px;">';
		$markup .= 'Hi CRM Team, <br><br>';
		if ($data['venueCount'] == 1) {
			$markup .= '<a href=' . $data['venueData'][0]['link'] . '>' . $data['venueData'][0]['venue'] . '</a> is not available for ' . $data['name'] . '\'s party';
			if ($data['venueData'][0]['comments']) {
				$markup .= ' <br> <b> comments : </b>' . $data['venueData'][0]['comments'];
			}
			$markup .= '<div style=" margin-top:10px;" ><b>Customer Details</b></div>';
			$markup .= '<table border="1" style="border-collapse: collapse;margin-top:5px;width:70%">
						 <tr>
                                <td style="padding:5px"><b>Name</b></td>
                                <td style="padding:5px">' . $data['name'] . '</td>
                            </tr>
                            <tr>
								<td style="padding:5px"><b>Enquiry Id</b></td>
		                        <td style = "padding:5px"><a href=' . $data['ticketLink'] . '>' . $data['enquiryId'] . '</a></td>
							</tr>
                             <tr>
                                <td style="padding:5px"><b>Email</b></td>
                                <td style="padding:5px">' . $data['email'] . '</td>
                            </tr>
                            <tr>
                                <td style="padding:5px"><b>Phone</b></td>
                                <td style="padding:5px">' . $data['phone'] . '</td>
                            </tr>
					</table>';
		} else {
			$markup .= 'The following venues are not available for ' . $data['name'] . '\'s party.';
			$markup .= '<div style=" margin-top:10px;" ><b>Customer Details</b></div>';

			$markup .= '<table border="1" style="border-collapse: collapse;margin-top:5px;width:70%">
						 <tr>
                                <td style="padding:5px"><b>Customer Name</b></td>
                                <td style="padding:5px">' . $data['name'] . '</td>
                            </tr>
                            <tr>
								<td style="padding:5px"><b>Enquiry Id</b></td>
		                        <td style = "padding:5px"><a href=' . $data['ticketLink'] . '>' . $data['enquiryId'] . '</a></td>
							</tr>
                            <tr>
                                <td style="padding:5px"><b>Customer Phone</b></td>
                                <td style="padding:5px">' . $data['phone'] . '</td>
                            </tr>
					</table>';

			$markup .= '<div style="font-size:18px; margin-top:10px; margin-bottom:5px" ><b>Venue Details</b></div>';

			foreach ($data['venueData'] as $venueData) {

				$markup .= '<table border="1" style="border-collapse: collapse;margin-bottom:10px;width:70%">
							<tr>
								<td colspan="2" style="padding:5px">' . $venueData['venue'] . ' [ <a href=' . $venueData['link'] . '>' . $venueData['code'] . '</a> ]</td>
                            </tr>';

				if ($venueData['comments']) {
					$markup .= '<tr>
								<td style="padding:5px"><b>Comments</b></td>
								<td style="padding:5px">' . $venueData['comments'] . '</td>
							</tr>';
				}

				$markup .= '</table ><br>';
			}
		}

		$markup .= '<br><br>' . $this->getThanksMarkup() . '<br>Team Business</div>';

		return $markup;
	}

	public function getAutoBookCancellationMarkup($data)
	{
		$markup = '<div style="background-color:#F5F5F5;padding:10px 20px;max-width:700px; font-size:14px;line-height: 22px">
							<div style="padding:20px 10px;">
								<div>
									<div style="float:left;">
										<div><a href="http://evibe.in"><img src="http://gallery.evibe.in/img/logo/logo_evibe.png"
										alt="evibe.in"></a></div>
									</div>
									<div style="float:right;">
										<div><i>Date: ' . date("d/m/Y h:i A", time()) . '</i> </div>
										<div style="padding-top:4px;"><i>Phone:</i> ' . config('evibe.phone') . '</div>
									</div>
								<div style="clear:both;"></div>
							</div>
							<div style="padding:4px;margin-top:15px; background-color:#800000;text-align:center">
								<h2 style="color:#ffffff">
									<img src=' . config('evibe.gallery.host') . '/main/img/icons/cancel.png> Booking cancelled (#' . $data['enqId'] . ')
								</h2>
							</div>
							<div style="background-color:#ffffff;padding:10px 20px;">
								<div style="margin-top:30px;text-align:center">
									<div style="font-size:18px;margin:15px 0 0 0">Dear ' . $data['customerName'] . ',</div>
									<div style="margin-top:10px">We are sorry :(.</div>
									<div style="font-size:15px;width:90%;margin:15px 0">
									Unfortunately, your order for ' . $data['itemInfo'] . ' for ' . $data['checkIn'] . ' that you have booked on
									' . $data['bookingDate'] . ' is not available. We had to cancel your order.</p><p>
									We will initiate the full refund of Rs. ' . AppUtilFacade::formatPrice($data['advanceAmount']) . ' paid by you as advance within 2 business days. You will receive
									the money in 7-10 working days depending on your bank.
									</div>
								</div>
								<div style="margin:15px auto">
									Should you have any queries, please reply to this email. We will
									respond to you at the earliest.
									<div style=margin-top:15px>
									' . $this->getThanksMarkup() . '
									<div>Team Evibe.in</div>
								</div>
								</div>
							</div>

				</div>';

		return $markup;
	}

	public function getAutoBookVdCancellationMarkup($data)
	{
		$markup = '<div style="background-color:#F5F5F5;padding:10px 20px;max-width:700px; font-size:14px;line-height: 22px">
							<div style="padding:20px 10px;">
								<div>
									<div style="float:left;">
										<div><a href="http://evibe.in"><img src="http://gallery.evibe.in/img/logo/logo_evibe.png"
										alt="evibe.in"></a></div>
									</div>
									<div style="float:right;">
										<div><i>Date: ' . date("d/m/Y h:i A", time()) . '</i> </div>
										<div style="padding-top:4px;"><i>Phone:</i> ' . config('evibe.phone') . '</div>
									</div>
								<div style="clear:both;"></div>
							</div>
							<div style="padding:4px;margin-top:15px; background-color:#800000;text-align:center">
								<h2 style="color:#ffffff">
									<img src=' . config('evibe.gallery.host') . '/main/img/icons/cancel.png> Venue Not Available - #' . $data['bookingId'] . '
								</h2>
							</div>
							<div style="background-color:#ffffff;padding:10px 20px;">
								<div style="margin-top:30px">
									<div style="margin:15px 0 0 0">Namaste ' . $data['customerName'] . ',</div>
										<p>We regret to inform you that ' . $data['venueName'] . ' at ' . $data['venueLocation'] . ' is not available for your party date on
										  ' . $data['partyDate'] . ' (' . $data['partySlot'] . ' slot).
										</p>
										<p>We will initiate the full refund of token amount of token amount of Rs. ' . AppUtilFacade::formatPrice($data['tokenAmount']) . ' within 2 business days. You will receive the money within 7 - 10 working days depending on your bank.</p>
										<p>Should you have any queries, please reply to this email. We will respond to you at the earliest.</p>
								</div>
								<div style="margin:15px auto">

									<div style=margin-top:15px>
									' . $this->getThanksMarkup() . '
									<div>Team Evibe.in</div>
								</div>
								</div>
							</div>

				</div>';

		return $markup;
	}

	public function getAutoBookCakeCancellationMarkup($data)
	{
		$markup = '<div style="background-color:#F5F5F5;padding:10px 20px;max-width:700px; font-size:14px;line-height: 22px">
							<div style="padding:20px 10px;">
								<div>
									<div style="float:left;">
										<div><a href="http://evibe.in"><img src="http://gallery.evibe.in/img/logo/logo_evibe.png"
										alt="evibe.in"></a></div>
									</div>
									<div style="float:right;">
										<div><i>Date: ' . date("d/m/Y H:i A", time()) . '</i> </div>
										<div style="padding-top:4px;"><i>Phone:</i> ' . config('evibe.phone') . '</div>
									</div>
								<div style="clear:both;"></div>
							</div>
							<div style="padding:4px;margin-top:15px; background-color:#800000;text-align:center">
								<h2 style="color:#ffffff">
									<img src=' . config('evibe.gallery.host') . '/main/img/icons/cancel.png> Cake Not Available - #' . $data['bookingId'] . '
								</h2>
							</div>
							<div style="background-color:#ffffff;padding:10px 20px;">
								<div style="margin-top:30px">
									<div style="margin:15px 0 0 0">Namaste ' . $data['customerName'] . ',</div>
										<p>We regret to inform you that your order of ' . $data['cakeName'] . ' (' . $data['cakeWeight'] . ') for ' . $data['partyDate'] . ' (' . $data['partySlot'] . ') is not available.
										</p>
										<p>We will initiate the full refund of Rs. ' . AppUtilFacade::formatPrice($data['advanceAmount']) . ' within 2 business days. You will receive the money within 7 - 10 working days depending on your bank.</p>
										<p>Should you have any queries, please reply to this email. We will respond to you at the earliest.</p>
								</div>
								<div style="margin:15px auto">

									<div style=margin-top:15px>
									' . $this->getThanksMarkup() . '
									<div>Team Evibe.in</div>
								</div>
								</div>
							</div>

				</div>';

		return $markup;
	}

	private function getErrorReportMarkup($data)
	{
		$markup = "<div style='padding:10px'>
                    <p>Hi Team,</p>
                    <p>please go through the details and fix it immediately.</p>
	                    <table border='1' style='border-collapse: collapse; margin-top:10px;width:100%'>
		                    <tr>
		                      <td style='padding:8px'>Url </td>
		                      <td style='padding:8px'>" . $data['url'] . "</td>
		                    </tr>
		                    <tr>
		                      <td style='padding:8px'>Code </td>
		                      <td style='padding:8px'>" . $data['code'] . "</td>
		                    </tr>
		                    <tr>
		                      <td style='padding:8px'>Method </td>
		                      <td style='padding:8px'>" . $data['method'] . "</td>
		                    </tr>
		                    <tr>
		                      <td style='padding:8px'>Message </td>
		                      <td style='padding:8px'>" . $data['message'] . "</td>
		                    </tr>
		                    <tr>
		                      <td style='padding:8px'>Trace </td>
		                      <td style='padding:8px'>" . $data['trace'] . "</td>
		                    </tr>
	                    </table>
	                    <p>
	                    <div>" . $this->getThanksMarkup() . "</div>
	                    <div>Admin</div>
	                    </p>
                   </div>";

		return $markup;
	}

	public static function getPartnerSalutationMarkup()
	{
		$markup = "<div>" . config('evibe.email_salutation') . "</div>
				   <div>Your wellwishers at <a href='" . config('evibe.live.host') . "' style='color:#333'>Evibe.in</div>";

		return $markup;
	}

	public static function getThanksMarkup()
	{
		$markup = "<div>" . config('evibe.email_salutation') . "</div>";

		return $markup;
	}
}
