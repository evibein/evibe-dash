<?php namespace Evibe\Facades;

/**
 * @author Anji <anji@evibe.in>
 * @since  9 Jan 2015
 */

use Evibe\Utilities\AppUtil;
use Illuminate\Support\Facades\Facade;

class AppUtilFacade extends Facade
{

	protected static function getFacadeAccessor()
	{
		return new AppUtil();
	}

}