<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Request;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		AuthorizationException::class,
		ModelNotFoundException::class,
		ValidationException::class,
		TokenMismatchException::class,
		HttpException::class
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception $e
	 *
	 * @return void
	 */
	public function report(Exception $e)
	{
		$code = $e->getCode();
		// $dontEmail = [0, 404, 401, 405];
		$dontEmail = [0, 404];

		if (config('evibe.email_error') &&
			!in_array($code, $dontEmail)
		)
		{
			$data = [
				'fullUrl' => Request::fullUrl(),
				'code'    => $code,
				'message' => $e->getMessage(),
				'details' => $e->getTraceAsString()
			];

			$emailData = [
				'code'    => $code,
				'url'     => $data['fullUrl'],
				'method'  => Request::method(),
				'message' => $data['message'],
				'trace'   => $data['details']
			];

			Queue::push('\Evibe\Utilities\SendEmail@mailGenericErrorToAdmin', $emailData);
			$this->saveError($data);
		}
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Exception               $e
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		return parent::render($request, $e);
	}

	private function saveError($data)
	{
		\SiteErrors::create([
			                    'project_id' => config('evibe.project_id'),
			                    'url'        => $data['fullUrl'],
			                    'exception'  => $data['message'],
			                    'code'       => $data['code'],
			                    'details'    => $data['details']
		                    ]);
	}

	protected function unauthenticated($request, AuthenticationException $exception)
	{
		if ($request->expectsJson())
		{
			return response()->json(['error' => 'Unauthenticated.'], 401);
		}

		return redirect()->guest('login');
	}
}
