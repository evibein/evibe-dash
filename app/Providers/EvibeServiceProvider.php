<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class EvibeServiceProvider extends ServiceProvider
{
	public function boot()
	{
		// morphTo relation model comparison for provider() of package category & booking details
		Relation::morphMap([
			                   config('evibe.ticket_type.planners') => \Vendor::class,
			                   config('evibe.ticket_type.artists')  => \Vendor::class,
			                   config('evibe.ticket_type.venues')   => \Venue::class,
			                   config('evibe.ticket_type.cakes')    => \Vendor::class,
			                   config('evibe.ticket_type.decors')   => \Vendor::class
		                   ], false);
	}

	public function register()
	{

	}
}