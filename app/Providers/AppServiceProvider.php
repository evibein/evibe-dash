<?php

namespace App\Providers;

use App\Evibe\EvibeValidator;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Validator::resolver(function ($translator, $data, $rules, $messages)
		{
			return new EvibeValidator($translator, $data, $rules, $messages);
		});

		// Blade directives
		Blade::directive('price', function ($expression)
		{
			return "<span class='rupee-font'>&#8377;</span> <?php echo AppUtil::formatPrice(e($expression)); ?>";
		});

		Blade::directive('priceW', function ($expression)
		{
			return "<?php echo AppUtil::formatPrice(e($expression)); ?>";
		});
		
		// DB::listen(function($query) {
		//	File::append(
		//		storage_path('/logs/query.log'),
		//		microtime() . " - " . $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
		//	);
		// });
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
	}
}
