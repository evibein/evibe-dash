<?php namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{

	/**
	 * This namespace is applied to the controller routes in your routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Http\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router $router
	 *
	 * @return void
	 */
	public function boot()
	{
		//

		parent::boot();

		/*
		|--------------------------------------------------------------------------
		| Application & Route Filters
		|--------------------------------------------------------------------------
		|
		| Below you will find the "before" and "after" events for the application
		| which may be used to do any work before or after a request into your
		| application. Here you may also register your custom route filters.
		|
		*/
		/*
			\App::before(function($request)
			{
				//
			});


			App::after(function($request, $response)
			{
				//
			});
		*/
		/*
		|--------------------------------------------------------------------------
		| Authentication Filters
		|--------------------------------------------------------------------------
		|
		| The following filters are used to verify that the user of the current
		| session is logged into this application. The "basic" filter easily
		| integrates HTTP Basic authentication for quick, simple checking.
		|
		*/

		//\Illuminate\Support\Facades\Route::filter('auth', function()
		//{
		//    if (Auth::guest()) return Redirect::guest('login');
		//});
		//
		//
		// Route::filter('auth.basic', function()
		// {
		//    return Auth::basic();
		// });

		//Route::filter('authorize.tickets', function()                   //done
		//{
		//    if (!AppUtil::isTabVisible('tickets'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.venues', function()             //done
		//{
		//    if (!AppUtil::isTabVisible('venues'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.packages', function()          //done
		//{
		//    if (!AppUtil::isTabVisible('packages'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.vendors', function()                   //done
		//{
		//    if (!AppUtil::isTabVisible('vendors'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.images', function()                //done
		//{
		//    if (!AppUtil::isTabVisible('images'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.trends', function()               //done
		//{
		//    if (!AppUtil::isTabVisible('trends'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.cakes', function()              //done
		//{
		//    if (!AppUtil::isTabVisible('cakes'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.reviews', function()                //done
		//{
		//    if (!AppUtil::isTabVisible('reviews'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.errors', function()                //done
		//{
		//    if (!AppUtil::isTabVisible('errors'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//Route::filter('authorize.errors', function()              //done
		//{
		//    if (!AppUtil::isTabVisible('nearby-areas'))
		//    {
		//        return App::abort(405);
		//    }
		//});
		//
		//
		///*
		//|--------------------------------------------------------------------------
		//| Guest Filter
		//|--------------------------------------------------------------------------
		//|
		//| The "guest" filter is the counterpart of the authentication filters as
		//| it simply checks that the current user is not logged in. A redirect
		//| response will be issued if they are, which you may freely change.
		//|
		//*/
		//
		//Route::filter('guest', function()
		//{
		//    if (Auth::check())
		//    {
		//        //Redirect::route('tickets');
		//        $roleId = Auth::user()->role_id;
		//        AppUtil::setSessions();
		//
		//        // Role ids are as per database
		//        // ensure it is in sync with 'role' table in database
		//        // 1: super admin
		//        // 2: admin
		//        // 6: customer delight team
		//        if (in_array($roleId, array(1, 2, 6)))
		//        {
		//            return Redirect::to('/tickets');
		//        }
		//
		//        // 5: business developer
		//        else if(in_array($roleId, array(5)))
		//        {
		//            return Redirect::to('/venues');
		//        }
		//
		//        // restricted access
		//        else
		//        {
		//            return Redirect::route('login')->with('auth_error', 'Not an authorised user')->withInput();
		//        }
		//
		//    }
		//});
		//
		///*
		//|--------------------------------------------------------------------------
		//| CSRF Protection Filter
		//|--------------------------------------------------------------------------
		//|
		//| The CSRF filter is responsible for protecting your application against
		//| cross-site request forgery attacks. If this special token in a user
		//| session does not match the one given in this request, we'll bail.
		//|
		//*/
		//Route::filter('csrf', function()
		//{
		//    if (Session::token() != Input::get('_token'))
		//    {
		//        throw new Illuminate\Session\TokenMismatchException;
		//    }
		//});
		//    }
		//
		//    /**
		//     * Define the routes for the application.
		//     *
		//     * @param  \Illuminate\Routing\Router  $router
		//     * @return void
		//     */
	}

	public function map()
	{
		$this->mapWebRoutes();
	}

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapWebRoutes()
	{
		Route::group([
			             'middleware' => 'web',
			             'namespace'  => $this->namespace,
		             ], function ($router) {
			require base_path('routes/web.php');
		});
	}
}