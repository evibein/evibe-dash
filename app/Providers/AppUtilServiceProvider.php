<?php

namespace App\Providers;

use Evibe\Utilities\AppUtil;
use Illuminate\Support\ServiceProvider;

class AppUtilServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//$this->app['evibe.apputil'] = $this->app->share(function ($app)
		//{
		//	return new AppUtil;
		//});
	}
}
