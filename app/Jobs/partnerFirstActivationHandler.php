<?php

namespace App\Jobs;

use App\Jobs\Emails\sendErrorAlertToTechTeam;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Hash;

class partnerFirstActivationHandler extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$res = ['success' => false];
		$partner = $this->data['partner'];

		$token = Hash::make($partner->id . $partner->created_at);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'first-booking/' . $partner->id . '?token=' . $token;
		$data = [
			'partnerId' => $partner->id,
			'mapTypeId' => $this->data['mapTypeId'],
			'liveLink'  => $this->data['liveLink'],
			'itemType'  => $this->data['itemType']
		];
		try
		{
			$client = new Client();
			$client->request('POST', $url, [
				'json' => $data
			]);

			$res['success'] = true;
		} catch (ClientException $e)
		{
			$this->sendReportToTechTeam($e, $partner);
		}
	}

	private function sendReportToTechTeam($e, $provider)
	{
		$sub = '[URGENT] Error occurred while sending the first activation email to ' . $provider->person . ' (' . $provider->name . ')';

		$data = [
			'sub'        => $sub,
			'errorTitle' => $e->getMessage(),
			'code'       => $e->getCode(),
			'details'    => $e->getTraceAsString()
		];

		dispatch(new sendErrorAlertToTechTeam($data));
	}
}
