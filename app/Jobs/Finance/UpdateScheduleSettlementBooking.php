<?php

namespace App\Jobs\Finance;

use App\Jobs\BaseJob;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Support\Facades\Auth;
use Exception;

class UpdateScheduleSettlementBooking extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$ticketBookingId = isset($data['ticketBookingId']) && $data['ticketBookingId'] ? $data['ticketBookingId'] : null;
		$handlerId = isset($data['handlerId']) && $data['handlerId'] ? $data['handlerId'] : null;

		if ($ticketBookingId)
		{
			$user = User::find($handlerId);
			if(!$user)
			{
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.update_settlement_booking'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . ']' . 'Unable to find handler for the ticket booking. So, updating with default handler.',
					                                   'exception' => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . ']' . 'Unable to find handler for the ticket booking. So, updating with default handler.',
					                                   'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
					                                   'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
				                                   ]);

				// default handler
				$user = User::find(config("evibe.default.access_token_handler"));
			}

			$accessToken = $this->getAccessTokenFromUserId($user);
			$url = config('evibe.api.base_url') . config('evibe.api.finance.booking-settlements.prefix') . '/' . $ticketBookingId . '/update';

			$options = [
				'method'      => "PUT",
				'url'         => $url,
				'accessToken' => $accessToken,
				'jsonData'    => []
			];

			$res = $this->makeApiCallWithUrl($options);

			if (isset($res['success']) && !$res['success'])
			{
				$error = 'Some error occurred while updating schedule settlement booking';

				if (isset($res['error']) && $res['error'])
				{
					$error = $res['error'];
				}

				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.update_settlement_booking'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . ']' . $error,
					                                   'exception' => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . ']' . $error,
					                                   'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
					                                   'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
				                                   ]);
			}
		}
		else
		{
			// error reporting that ticket booking not found
			$this->sendNonExceptionErrorReport([
				                                   'code'      => config('evibe.error_code.update_settlement_booking'),
				                                   'url'       => request()->fullUrl(),
				                                   'method'    => request()->method(),
				                                   'message'   => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . '] Unable to find ticket booking to update schedule settlement booking',
				                                   'exception' => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . '] Unable to find ticket booking to update schedule settlement booking',
				                                   'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
				                                   'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
			                                   ]);
		}

	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}

