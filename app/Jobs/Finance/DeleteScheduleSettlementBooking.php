<?php

namespace App\Jobs\Finance;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Mockery\CountValidator\Exception;

class DeleteScheduleSettlementBooking extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$ticketBookingId = isset($data['ticketBookingId']) && $data['ticketBookingId'] ? $data['ticketBookingId'] : null;
		if ($ticketBookingId)
		{
			$user = Auth::user();
			$accessToken = $this->getAccessTokenFromUserId($user);
			$url = config('evibe.api.base_url') . config('evibe.api.finance.booking-settlements.prefix') . '/' . $ticketBookingId . '/delete';

			$options = [
				'method'      => "PUT",
				'url'         => $url,
				'accessToken' => $accessToken,
				'jsonData'    => []
			];

			$res = $this->makeApiCallWithUrl($options);

			if (isset($res['success']) && !$res['success'])
			{
				$error = 'Some error occurred while deleting schedule settlement booking';

				if (isset($res['error']) && $res['error'])
				{
					$error = $res['error'];
				}

				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.delete_settlement_booking'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[DeleteScheduleSettlementBooking.php - ' . __LINE__ . ']' . $error,
					                                   'exception' => '[DeleteScheduleSettlementBooking.php - ' . __LINE__ . ']' . $error,
					                                   'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
					                                   'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
				                                   ]);
			}
		}
		else
		{
			// error reporting that ticket booking not found
			$this->sendNonExceptionErrorReport([
				                                   'code'      => config('evibe.error_code.update_settlement_booking'),
				                                   'url'       => request()->fullUrl(),
				                                   'method'    => request()->method(),
				                                   'message'   => '[DeleteScheduleSettlementBooking.php - ' . __LINE__ . '] Unable to find ticket booking to delete schedule settlement booking',
				                                   'exception' => '[DeleteScheduleSettlementBooking.php - ' . __LINE__ . '] Unable to find ticket booking to delete schedule settlement booking',
				                                   'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
				                                   'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
			                                   ]);
		}

	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
