<?php

namespace App\Jobs;

use App\Models\Util\Priority;
use Illuminate\Contracts\Queue\ShouldQueue;

class SavePriority extends Job implements ShouldQueue
{
	protected $updateRecordsArray;
	private $cityId;
	private $occasionId;
	private $pageId;
	private $tagId;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($args)
	{
		$this->updateRecordsArray = $args['order'];
		$this->cityId = $args['cityId'];
		$this->occasionId = $args['occasionId'];
		$this->pageId = $args['pageId'];
		$this->tagId = $args['tagId'];
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$cityId = $this->cityId;
		$occasionId = $this->occasionId;
		$pageTypeId = $this->pageId;
		$tagTypeId = $this->tagId;
		$tagTypeId = ($tagTypeId > 0) ? $tagTypeId : null;
		$priority = 1;
		$itemIdsList = (array)$this->updateRecordsArray;

		//update or create the priority of newly prioritise elements.
		foreach ($itemIdsList as $itemId)
		{
			//first search this item exists in priority table or not
			$priorityObj = Priority::where(['occasion_id' => $occasionId,
			                                'map_type_id' => $pageTypeId,
			                                'map_id'      => $itemId,
			                                'city_id'     => $cityId
			                               ]);

			if (is_null($tagTypeId))
			{
				$priorityObj->whereNull('type_tag_id');
			}
			else
			{
				$priorityObj->where('type_tag_id', $tagTypeId);
			}

			$priorityObj = $priorityObj->first();

			// if item found in priority table, then update
			if ($priorityObj)
			{
				$priorityObj->update(['priority' => $priority]);
			}
			else
			{
				//if item not found in priority table, then create a new one
				Priority::create([
					                 'occasion_id' => $occasionId,
					                 'map_type_id' => $pageTypeId,
					                 'map_id'      => $itemId,
					                 'city_id'     => $cityId,
					                 'priority'    => $priority,
					                 'type_tag_id' => $tagTypeId

				                 ]);
			}

			$priority++;
		}

		// now reset the priority of other elements
		$resetPriorities = Priority::where(['occasion_id' => $occasionId, 'map_type_id' => $pageTypeId])
		                           ->where('city_id', $cityId)
		                           ->whereNotIn('map_id', $itemIdsList);
		if (is_null($tagTypeId))
		{
			$resetPriorities->whereNull('type_tag_id')
			                ->orderBy('priority', 'asc')
			                ->chunk(100, function ($existingItems) use ($priority)
			                {
				                foreach ($existingItems as $item)
				                {
					                $item->update(['priority' => $priority]);
					                $priority++;
				                }
			                });;
		}
		else
		{
			$resetPriorities->where('type_tag_id', $tagTypeId)
			                ->forceDelete();
		}
	}
}