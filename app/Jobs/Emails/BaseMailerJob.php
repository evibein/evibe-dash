<?php

namespace App\Jobs\Emails;

use App\Jobs\BaseJob;
use Evibe\Utilities\SendEmail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class BaseMailerJob extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		//
	}

	protected function getPartnerCc($partner, $addCc = [], $businessCC = false)
	{
		$fields = ['alt_email', 'alt_email_1', 'alt_email_2', 'alt_email_3'];
		$emails = [];

		foreach ($fields as $field)
		{
			if (!empty($partner->{$field}))
			{
				array_push($emails, $partner->{$field});
			}
		}

		if (count($addCc))
		{
			$emails = array_merge($emails, $addCc);
		}

		if ($businessCC)
		{
			array_push($emails, config('evibe.business_group_email'));
		}

		return $emails;
	}

	protected function getPartnerSalutation()
	{
		$emailFile = new SendEmail();

		return $emailFile->getPartnerSalutationMarkup();
	}
}
