<?php

namespace App\Jobs\Emails;

use App\Jobs\BaseJob;
use App\Models\Ticket\RefundGallery;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class RefundAmountMail extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$booking = \TicketBooking::find($data['booking_id']);
		$refundGallery = RefundGallery::where('ticket_booking_id', $data['booking_id'])->get();
		if ($booking)
		{
			$ticket = $booking->ticket;
			$data['booking'] = $booking;
			$data['ticket'] = $ticket;
			$data['gallery'] = $refundGallery;

			$sub = "Refund processed for " . $booking->booking_id;
			$to = $ticket->email;
			$cc = config("evibe.enquiry_group_email");
			$handlerName = $data['handler']->name;
			$handlerMail = $data['handler']->username;

			if ($to)
			{
				Mail::send('emails.refund.refund_amount', ['data' => $data], function ($m) use ($sub, $to, $cc, $handlerMail, $handlerName) {
					$m->from($handlerMail, $handlerName);
					$m->to($to);
					$m->cc($cc);
					$m->subject($sub);
				});
			}
			else
			{
				Mail::send('emails.refund.refund_amount', ['data' => $data], function ($m) use ($sub, $to, $cc, $handlerMail, $handlerName) {
					$m->from($handlerMail, $handlerName);
					$m->to(config('evibe.accounts_group_email'));
					$m->subject('[Invalid Email Address]. Sub: ' . $sub);
				});
			}
		}
	}
}
