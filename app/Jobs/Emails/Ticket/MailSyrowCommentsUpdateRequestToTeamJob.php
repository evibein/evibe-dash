<?php

namespace App\Jobs\Emails\Ticket;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class MailSyrowCommentsUpdateRequestToTeamJob extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if (isset($data["ticketId"]))
		{
			$subject = "New Request Update - #" . $data["ticketId"];
		}
		elseif (isset($data["phone"]))
		{
			$subject = "New Request Update - Ticket Not Found [Phone: " . $data["phone"] . "]";
		}
		else
		{
			$subject = "New Request Update";
		}

		Mail::send('emails.tickets.comment_request_update_alert_team', ['data' => $data], function ($m) use ($subject, $data) {
			$m->from(config('evibe.email'), "Syrow")
			  ->to($data["to"])
			  ->subject($subject);
		});
	}
}