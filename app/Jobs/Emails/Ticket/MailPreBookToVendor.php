<?php

namespace App\Jobs\Emails\Ticket;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class MailPreBookToVendor extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$sub = "[Evibe.in] Booking order for " . $data['partyDate'] . " by " . $data['customerName'] . " is under process";
		$body = self::getOrderProcessEmailToVendorMarkup($data);

		// todo: write failed job

		Mail::send('emails.partner.mail-pre-book-to-vendor', ['data' => $body], function ($m) use ($data,$sub) {
			$m->from('business@evibe.in', 'Team Evibe.in');
			$m->to($data['to']);
			$m->cc($data['ccAddresses']);
			$m->subject($sub);
			$m->getHeaders()->addTextHeader('TicketId:', $data['ticketId']);
		});
	}
	public static function getOrderProcessEmailToVendorMarkup($data)
	{
		$body = "
			<div style='background-color:#F5F5F5;padding:10px;max-width:700px;'>
				<div style='padding: 20px 30px 10px 30px;background-color: #FFFFFF;'>
					<div>
						<div style='float:left;'>
							<div>
								<a href='http://evibe.in'>
									<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
								</a>
								<div style='padding-top:10px;'>Phone: " . config('evibe.phone') . "</div>
							</div>
						</div>
						<div style='float:right;'>
							<div style='padding-top:10px;'>
								<span>Booking Date: </span>
								<span>" . date('d/m/y h:i A') . "</span>
							</div>
						</div>
						<div style='clear:both;'></div>
					</div>
					<hr>
					<div style='padding-top: 10px;'>
						<p>Dear " . $data['vendorName'] . ",</p>
						<p>We are pleased to inform you that booking for " . $data['partyDate'] . " is in process. Please check below for more details.</p>";
		$fullLocation = $data['partyLocation'];
		if ($data['zipCode'])
		{
			$fullLocation .= ", Pin code: " . $data['zipCode'];
		}
		if ($data['landmark'])
		{
			$fullLocation .= ", Landmark: " . $data['landmark'];
		}
		$body .= "<div style='padding-top: 10px;'>
					<div style='color:#EF3E75;'><b>Customer Details: </b></div>
					<div style='padding-top:10px;'>
						<table style='max-width:600px;border:1px solid #AAA;border-collapse:collapse;'>
							<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Name: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $data['customerName'] . "</td>
							</tr>
							<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Location: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $fullLocation . "</td>
							</tr>
							<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Occasion: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $data['additional']['eventName'] . "</td>
							</tr>
							<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Party time: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $data['partyDateTime'] . "</td>
							</tr>";

		$body .= "<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Order Details: </b></td>
								<td style='border:1px solid #AAA;padding:4px;'>" . $data['bookingInfo'] . "</td>
							</tr>";

		if ($data['booking']->gallery->count() > 0)
		{
			$body .= "<tr>
								<td style='border:1px solid #AAA;padding:4px;'><b>Reference Image(s)</b></td>
								<td style='border:1px solid #AAA;padding:4px 4px 4px 7px;'>";
			foreach ($data['booking']->gallery as $gallery)
			{
				$body .= '<a href="' . $gallery->getOriginalImagePath() . '" alt="' . $gallery->name . '" style="text-decoration:none">
								<img src="' . $gallery->getResultsImagePath() . '" style="max-width:160px;max-height:100px;margin:0 5px 5px 0; border:1px solid #d9d9d9">
						 </a>';
			}

			$body .= "</td>
						</tr>";
		}

		if ($data['booking']->checkoutFields->count())
		{
			foreach ($data['booking']->checkoutFields as $checkoutField)
			{
				if (!empty($data['additional'][$checkoutField->name]))
				{
					$body .= "<tr>
						<td style='border:1px solid #AAA;padding:4px;'><b>" . ucwords($checkoutField->identifier) . " </b></td>
						<td style='border:1px solid #AAA;padding:4px;'>" . $data['additional'][$checkoutField->name] . "</td>
						</tr>";
				}
			}
		}

		if ($data['prerequisites'])
		{
			$body .= "<tr>
						<td style='border:1px solid #AAA;padding:4px;'><b>Prerequisites: </b></td>
						<td style='border:1px solid #AAA;padding:4px;'>" . $data['prerequisites'] . "</td>
					</tr>";
		}

		if ($data['facts'])
		{
			$body .= "<tr>
						<td style = 'border:1px solid #AAA;padding:4px;'><b>Facts: </b></td>
						<td style = 'border:1px solid #AAA;padding:4px;'> " . $data['facts'] . " </td>
					</tr>";
		}

		$body .= "</table></div></div>";

		$body .= "<div style='padding-top: 10px;'>
					<p>Once the advance has been paid by the customer, you will be notified with complete details.</p>
					<p>Please reply back if you are not able to meet any of the above mentioned details.</p>
					<p>" . self::getPartnerSalutationMarkup() . "</p>
				</div>";

		return $body;
	}
	public static function getPartnerSalutationMarkup()
	{
		$markup = "<div>" . config('evibe.email_salutation') . "</div>
				   <div>Your wellwishers at <a href='" . config('evibe.live.host') . "' style='color:#333'>Evibe.in</div>";

		return $markup;
	}
}
