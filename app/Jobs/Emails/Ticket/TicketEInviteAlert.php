<?php

namespace App\Jobs\Emails\Ticket;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class TicketEInviteAlert extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['sub'] = "Invite your guest using this cool e-invitation & unique invite link";

		$data['cc'] = isset($data['customerAltEmail']) ? [$data['customerAltEmail']] : [];

		// todo: write failed job

		if (isset($data['customerEmail']) && $data['customerEmail'])
		{
			Mail::send('emails.tickets.e-invite', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.invitations_group_email'), 'Evibe Invites');
				$m->to($data['customerEmail']);
				$m->cc($data['cc']);
				$m->replyTo(config('evibe.invitations_group_email'));
				$m->subject($data['sub']);
			});
		}
		else
		{
			// todo: modify error reporting email
			Mail::send('emails.tickets.e-invite', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.invitations_group_email'), 'Evibe Invites');
				$m->to(config('evibe.invitations_group_email'));
				$m->subject("[Invalid customer email]. Sub: " . $data['sub']);
			});
		}
	}
}
