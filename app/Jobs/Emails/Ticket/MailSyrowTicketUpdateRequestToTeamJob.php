<?php

namespace App\Jobs\Emails\Ticket;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class MailSyrowTicketUpdateRequestToTeamJob extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$subject = "[POST BOOKING] Order Update Request - #" . $data["ticketId"];

		Mail::send('emails.tickets.post_booking_customer_alert_team', ['data' => $data], function ($m) use ($subject) {
			$m->from(config('evibe.email'), "Syrow")
			  ->to(config('evibe.enquiry_group_email'))
			  ->subject($subject);
		});
	}
}