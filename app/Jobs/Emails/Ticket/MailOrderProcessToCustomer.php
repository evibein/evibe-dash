<?php

namespace App\Jobs\Emails\Ticket;

use App\Jobs\BaseJob;
use Evibe\Facades\AppUtilFacade;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class MailOrderProcessToCustomer extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$sub = "Please complete payment to book your event";
		$sub .= isset($data["enquiryId"]) ? " - #" . $data["enquiryId"] : "";

		$body = $this->getOrderProcessEmailToCustomerMarkup($data);

		// todo: write failed job

		Mail::send('emails.orders.mail-order-process-to-customer', ['data' => $body], function ($m) use ($data, $sub) {
			$m->from(config('evibe.enquiry_group_email'), 'Evibe Payments');
			$m->to($data['customer']['email']);
			$m->cc($data['ccAddresses']);
			$m->subject($sub);
			$m->getHeaders()->addTextHeader('TicketId:', $data['ticketId']);
		});
	}

	private static function getOrderProcessEmailToCustomerMarkup($data)
	{
		$termsMarkup = file_get_contents(base_path('resources/views/emails/orders/terms.html'));
		$cancellationMarkup = file_get_contents(base_path('resources/views/emails/orders/cancellation.html'));
		$extraInfo = "";

		$card1Markup = file_get_contents(base_path('resources/views/emails/orders/op_card1.html'));
		$card1Replaces = [
			'@date@'       => date('d/m/y h:i A'),
			'@evibePhone@' => config('evibe.phone'),
			'@custName@'   => $data['customer']['name'],
			'@advanceAmt@' => AppUtilFacade::formatPrice($data['bookings']['totalAdvanceToPay']),
			'@deadline@'   => $data['bookings']['deadline'],
			'@pgLink@'     => $data['bookings']['paymentLink'],
			'@thanks@'     => self::getThanksMarkup(),
			'@eventName@' => $data['additional']['eventName'],
			'@partyDateTime@' => $data['partyDateTime']
		];
		$card1Markup = str_replace(array_keys($card1Replaces), array_values($card1Replaces), $card1Markup);

		$card2Markup = file_get_contents(base_path('resources/views/emails/orders/op_card2.html'));
		$card2Replaces = [
			'@custName@'  => $data['customer']['name'],
			'@custPhone@' => $data['customer']['phone'],
			//'@partyDate@' => $data['additional']['partyDateTime']
		];
		//if (isset($data['bookings']['minBookingDateTime']) && isset($data['bookings']['maxBookingDateTime']))
		//{
		//	if ($data['bookings']['minBookingDateTime'] < $data['bookings']['maxBookingDateTime'])
		//	{
		//		$card2Replaces['@partyTimings@'] = date("d/m/y H:i", $data['bookings']['minBookingDateTime']) . " - " . date("d/m/y H:i",$data['bookings']['maxBookingDateTime']);
		//	}
		//	else
		//	{
		//		$card2Replaces['@partyTimings@'] = date("d/m/y H:i", $data['bookings']['minBookingDateTime']);
		//	}
		//}
		//else
		//{
		//	$card2Replaces['@partyTimings@'] = $data['additional']['partyDateTime'];
		//}
		$card2Markup = str_replace(array_keys($card2Replaces), array_values($card2Replaces), $card2Markup);
		$card3Markup = "";
		foreach ($data['bookings']['list'] as $booking)
		{
			$advTxt = '<div><b>Advance Amount</b></div>';
			if ($booking['advancePaid'] == 1)
			{
				$advTxt = '<div style="color:#00A800"><b>Advance Paid</b></div>';
			}
			$advancePaidMarkup = '<li style="display:table-cell;">' . $advTxt;
			$advancePaidMarkup .= '<div>Rs. ' . AppUtilFacade::formatPrice($booking['advanceAmount']) . '</div></li>';
			$bookingInfo = $booking['bookingInfo'];
			$bookingPreReq = $booking['prerequisites'];
			$bookingFacts = $booking['facts'];
			//Add some extra field when venue booking payment link email sent
			if ($booking['isVenue'] == 1)
			{
				$card3RepeatWithVenue = file_get_contents(base_path('resources/views/emails/orders/op_card3_with_venue.html'));
				$venueNameMarkup = "";
				if ($booking['hasShowVenueName'])
				{
					$venueNameMarkup = '<li style="display:table-cell;">
	                                         <div><b>Venue Name</b></div>
				                             <div>' . $booking['provider']['name'] . '</div>
	             	                   </li>';
				}

				$card3WithVenueReplace = [
					'@orderId@'         => $booking['bookingId'],
					'@venueNameMarkup@' => $venueNameMarkup,
					'@venuePerson@'     => $booking['provider']['person'],
					'@partyDateTime@'   => date("d/m/y g:i A", $booking['partyDateTime']),
					'@advAmount@'       => AppUtilFacade::formatPrice($booking['advanceAmount']),
					'@orderAmount@'     => AppUtilFacade::formatPrice($booking['bookingAmount']),
					'@bookingInfo@'     => $bookingInfo,
				];

				$card3Markup .= str_replace(array_keys($card3WithVenueReplace), array_values($card3WithVenueReplace), $card3RepeatWithVenue);
			}
			else
			{
				$card3Repeat = file_get_contents(base_path('resources/views/emails/orders/op_card3.html'));
				$card3Replaces = [
					'@orderId@'           => $booking['bookingId'],
					'@evtMng@'            => $booking['provider']['person'],
					'@ordAmt@'            => AppUtilFacade::formatPrice($booking['bookingAmount']),
					'@partyDateTime@'     => date("d/m/y g:i A", $booking['partyDateTime']),
					'@advancePaidMarkup@' => $advancePaidMarkup,
					'@bookingInfo@'       => $bookingInfo
				];
				$card3Markup .= str_replace(array_keys($card3Replaces), array_values($card3Replaces), $card3Repeat);
			}
			// add pre-reqs / facts
			if ($bookingFacts || $bookingPreReq || $booking['booking']->gallery->count() || $booking['booking']->checkoutFields->count())
			{
				$extraInfo = '<div style="background-color:#FFFFFF; padding: 0 20px 20px 20px">';

				if ($booking['booking']->gallery->count())
				{
					$extraInfo .= "<div style='margin-bottom:15px'>
                                    <div><b>Reference Image(s)</b></div>";
					foreach ($booking['booking']->gallery as $image)
					{
						$extraInfo .= '<a href="' . $image->getOriginalImagePath() . '" style="text-decoration:none">
										 <img src="' . $image->getResultsImagePath() . '" alt="' . $image->title . '" style="max-width:160px; max-height:100px; margin:10px 10px 0 0; border:1px solid #d9d9d9">
									   </a>';
					}

					$extraInfo .= "</div>";
				}

				if ($booking['booking']->checkoutFields->count())
				{

					foreach ($booking['booking']->checkoutFields as $checkoutField)
					{
						if (!empty($data['additional'][$checkoutField->name]))
						{
							$extraInfo .= "<div style='margin-bottom:15px'>";
							$extraInfo .= "<div><b>" . ucwords($checkoutField->identifier) . "</b></div>
										<div>" . $data['additional'][$checkoutField->name] . "</div>";
							$extraInfo .= "</div>";
						}
					}
				}

				if ($bookingPreReq)
				{
					$extraInfo .= "<div style='margin-bottom:15px'>
									<div><b>Prerequisites</b></div>
                                    <div>" . $bookingPreReq . "</div>
								</div>";
				}

				if ($bookingFacts)
				{
					$extraInfo .= "<div style='margin-bottom:20px;'>
										<div><b>Facts</b></div>
										<div>" . $bookingFacts . "</div>
									</div>";
				}

				$extraInfo .= "</div>";
				$card3Markup = $card3Markup . $extraInfo;
			}
		}

		$markup = "<div style='background-color:#d9d9d9;padding:30px 20px;width:700px;font-size:14px;line-height:22px;'>";
		$markup .= $card1Markup . $card2Markup . $card3Markup;
		$markup .= $cancellationMarkup . $termsMarkup;
		$markup .= "</div>"; // close
		$markup .= "<div style='padding-top:10px;font-size:12px;color:#999'>If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>";

		return $markup;
	}

	public static function getThanksMarkup()
	{
		$markup = "<div>" . config('evibe.email_salutation') . "</div>";

		return $markup;
	}
}
