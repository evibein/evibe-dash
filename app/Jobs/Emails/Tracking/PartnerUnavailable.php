<?php

namespace App\Jobs\Emails\Tracking;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class PartnerUnavailable extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$sub = "[Escalation] For party on " . date("d M Y (D), H:i:s", $data["booking"]->party_date_time);
		$to = [config("evibe.business_group_email"), config("evibe.enquiry_group_email")];

		Mail::send('emails.tracking.partner_unavailable', ['data' => $data], function ($m) use ($sub, $to) {
			$m->from(config('evibe.email'), 'Order Tracking');
			$m->to($to);
			$m->subject($sub);
		});
	}
}
