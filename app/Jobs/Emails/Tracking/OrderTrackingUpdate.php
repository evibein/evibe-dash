<?php

namespace App\Jobs\Emails\Tracking;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class OrderTrackingUpdate extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$sub = "[Customer Update] Party on " . date("d M Y \(D\), H:i:s", $data["booking"]->party_date_time);
		$to = config("evibe.business_group_email");
		$cc = config("evibe.enquiry_group_email");

		if ($data["isCustomer"] == 2)
		{
			$sub = "[Partner Update] Party on " . date("d M Y \(D\), H:i:s", $data["booking"]->party_date_time);
			$to = config("evibe.enquiry_group_email");
			$cc = config("evibe.business_group_email");
		}

		Mail::send('emails.tracking.order_update', ['data' => $data], function ($m) use ($sub, $to, $cc) {
			$m->from(config('evibe.email'), 'Order Tracking');
			$m->to($to);
			$m->cc($cc);
			$m->subject($sub);
		});
	}
}
