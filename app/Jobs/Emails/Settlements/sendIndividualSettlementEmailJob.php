<?php

namespace App\Jobs\Emails\Settlements;

use App\Jobs\Emails\BaseMailerJob;
use Illuminate\Support\Facades\Mail;

class sendIndividualSettlementEmailJob extends BaseMailerJob
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$fromDate = date('d/m/Y', $data['fromDate']);
		$toDate = !empty($data['toDate']) ? $data['toDate'] : time();
		$toDate = date('d/m/Y', $toDate);
		$subject = "[Evibe.in] " . $data['partner']->person . ", settlement for parties happened from $fromDate to $toDate";

		$emailData = [
			'to'          => $data['partner']->email,
			'cc'          => $this->getPartnerCc($data['partner'], [config('evibe.accounts_group_email')], false),
			'sub'         => $subject,
			'total'       => $data['total'],
			'partner'     => $data['partner'],
			'settlements' => $data['settlements'],
			'salutation'  => $this->getPartnerSalutation(),
			'fromDate'    => $fromDate,
			'toDate'      => $toDate
		];

		if ($emailData['to'])
		{
			Mail::send('emails.settlement.partner', $emailData, function ($m) use ($emailData) {
				$m->from(config('evibe.accounts_group_email'), 'Evibe.in Accounts');
				$m->to($emailData['to'], "Partner")->subject($emailData['sub']);

				if (isset($emailData['cc']) && count($emailData['cc']))
				{
					$m->cc($emailData['cc']);
				}
			});
		}
		else
		{
			Mail::send('emails.settlement.partner', $emailData, function ($m) use ($emailData) {
				$m->from(config('evibe.accounts_group_email'), 'Evibe.in Accounts');
				$m->to(config('evibe.accounts_group_email'));
				$m->subject('[Null Email Address]. Sub: ' . $emailData['sub']);
			});
		}
	}
}
