<?php

namespace App\Jobs\Emails;

use App\Jobs\BaseJob;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailNoResponseToCust extends BaseJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$sub = "Regarding your event on " . $data["partyDate"] . " - #" . $data["enquiryId"];

		$data['from'] = "enquiry@evibe.in";
		$data['sub'] = $sub;

		Mail::send('emails.response.cust-no-response', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.enquiry_group_email'), 'Team Evibe')
			  ->to($data['to'])
			  ->subject($data['sub']);
			$m->getHeaders()->addTextHeader('TicketId:', $data['ticketId']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}