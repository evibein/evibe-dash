<?php

namespace App\Jobs\Emails\Partner;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Mockery\CountValidator\Exception;

class SendRejectionMailToPartner extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$genericMailData = [
			'date'       => date("d/m/Y h:i A", time()),
			'evibePhone' => config('phone'),
			'evibeLink'  => config('evibe.live.host')
		];

		$data = array_merge($data, $genericMailData);

		//$data['sub'] = $data['partnerName'] . ', ' . (isset($data['optionName']) && $data['optionName'] ? $data['optionName'] : 'booking') . ' UNAVAILABLE confirmation for ' . $data['partyDateTime'];
		$data['sub'] = '[#' . $data['ticketId'] . '] Order Rejected - ' . $data['partyDateTime'] . ' - ' . (isset($data['optionName']) && $data['optionName'] ? $data['optionName'] : 'booking');

		if ($data['partnerEmail'])
		{
			Mail::send('emails.partner.reject', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.enquiry_group_email'), 'Team Evibe.in')
				  ->to($data['partnerEmail'])
				  ->replyTo(config('evibe.enquiry_group_email'))
					//->cc([config('evibe.business_group_email'), config('evibe.enquiry_group_email')])
                  ->bcc(config('evibe.ops_alert_no_action_email'))
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.partner.reject', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.enquiry_group_email'), 'Team Evibe.in')
				  ->to(config('evibe.enquiry_group_email'))
				  ->replyTo(config('evibe.enquiry_group_email'))
					//->cc(config('evibe.enquiry_group_email'))
                  ->bcc(config('evibe.ops_alert_no_action_email'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
