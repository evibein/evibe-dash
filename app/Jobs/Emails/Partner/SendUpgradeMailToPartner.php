<?php

namespace App\Jobs\Emails\Partner;

use App\Jobs\BaseJob;
use App\Models\Partner\PartnerPackageOptions;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class SendUpgradeMailToPartner extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$partner = PartnerPackageOptions::find($data['packageId']);
		if ($partner)
		{
			$sub = "We need some enhancements in the package you had provided";
			$to = config("evibe.business_group_email");
			$cc = config("evibe.business_group_email");

			//Mail::send('emails.tracking.order_update', ['data' => $data], function ($m) use ($sub, $to, $cc)
			//{
			//	$m->from(config('evibe.email'), 'Order Tracking');
			//	$m->to($to);
			//	$m->cc($cc);
			//	$m->subject($sub);
			//});
		}
	}
}
