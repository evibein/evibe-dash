<?php

namespace App\Jobs\Emails\Approval;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class RejectApprovalJob extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$date = date("d/m/Y, h:i A", time());
		$title = $data['title'];
		$userName = $data['userName'];

		$sub = "[Evibe.in] $userName rejected the approval of $title at $date.";

		Mail::send('emails.approval.reject', ['data' => $data], function ($m) use ($sub) {
			$m->from(config('evibe.email'), 'Evibe.in');
			$m->to(config('evibe.business_group_email'), "Business")->subject($sub);
		});
	}
}
