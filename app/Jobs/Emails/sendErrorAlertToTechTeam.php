<?php

namespace App\Jobs\Emails;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class sendErrorAlertToTechTeam extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		Mail::send('emails.report.generic_error', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.email'), 'Evibe.in');
			$m->to(config('evibe.tech_group_email'), "Evibe")->subject($data['sub']);

			if (isset($data['cc']) && count($data['cc']))
			{
				$m->cc($data['cc']);
			}
		});
	}
}
