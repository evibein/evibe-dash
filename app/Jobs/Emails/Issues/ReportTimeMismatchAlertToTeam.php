<?php

namespace App\Jobs\Emails\Issues;

use App\Jobs\BaseJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class ReportTimeMismatchAlertToTeam extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = "Party time for a booking is not correct.";

		Mail::send('emails.issues.time-mismatch', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.email'), 'Party Time Mismatch')
			  ->to(config('evibe.enquiry_group_email'))
			  ->cc(config('evibe.tech_group_email'))
			  ->subject($data['sub']);
		});
	}
}
