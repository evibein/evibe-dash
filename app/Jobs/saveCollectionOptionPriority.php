<?php

namespace App\Jobs;

use App\Models\CollectionOption;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class saveCollectionOptionPriority extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $sortableIds;
	private $collectionId;

	public function __construct($itemIds, $collectionId)
	{
		$this->sortableIds = $itemIds;
		$this->collectionId = $collectionId;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$itemIds = $this->sortableIds;
		$priority = 1;

		foreach ($itemIds as $optionId)
		{
			$option = CollectionOption::findOrFail($optionId);

			$option->update(['priority' => $priority]);
			$priority++;
		}
	}
}
