<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Models\Util\AccessToken;
use App\Models\Util\AuthClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class BaseJob extends Job implements ShouldQueue
{
	public function sendFailedJobsToTeam($exception)
	{
		$data = [
			"from"       => config('evibe.email'),
			"to"         => config('evibe.tech_group_email'),
			"errorTitle" => "Default Error Title",
			"code"       => "Default Code",
			"details"    => "Default Details",
			"file"       => "Default File",
			"line"       => "Default Line"
		];

		if (!is_null($exception) && $exception instanceof \Exception)
		{
			$data['errorTitle'] = $exception->getMessage();
			$data['code'] = $exception->getCode();
			$data['details'] = $exception->getTraceAsString();
			$data['file'] = $exception->getFile();
			$data['line'] = $exception->getLine();
		}

		$data['project'] = 'DASH';
		$data['sub'] = '[Failed Job] - ' . date('F j, Y, g:i a');

		Mail::send('app.failed-jobs', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.email'), 'Evibe Errors')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});

		return true;
	}

	public function getAccessTokenFromUserId($user)
	{
		// default client id
		$clientId = config('evibe.api.client_id');

		$token = AccessToken::where('user_id', $user->id)
		                    ->where('auth_client_id', $clientId)
		                    ->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			if ($client)
			{
				$accessToken = $this->issueAccessToken($user, $client);

				return $accessToken;
			}
			else
			{
				return redirect(route("ticket.list"));
			}
		}
		else
		{
			return $token->access_token;
		}
	}

	public function issueAccessToken($user, $client)
	{
		$accessToken = new AccessToken();
		$accessToken->access_token = $this->getNewAccessToken();
		$accessToken->auth_client_id = $client->auth_client_id;
		$accessToken->user_id = $user->id;

		$accessToken->save();

		return $accessToken->access_token;
	}

	private function getNewAccessToken($len = 40)
	{
		$stripped = '';
		do
		{
			$bytes = openssl_random_pseudo_bytes($len, $strong);

			// We want to stop execution if the key fails because, well, that is bad.
			if ($bytes === false || $strong === false)
			{
				// @codeCoverageIgnoreStart
				throw new \Exception('Error Generating Key');
				// @codeCoverageIgnoreEnd
			}
			$stripped .= str_replace(['/', '+', '='], '', base64_encode($bytes));
		} while (strlen($stripped) < $len);

		return substr($stripped, 0, $len);
	}

	public function makeApiCallWithUrl($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			return $res;

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;
			Log::error($res['error']);

			return false;
		}
	}

	protected function sendErrorReport($e, $sendEmail = true)
	{
		$code = 'Error';
		if ($e instanceof HttpException)
		{
			$code = $e->getStatusCode();
		}

		if (config('evibe.email_error'))
		{
			$data = [
				'fullUrl' => request()->fullUrl(),
				'code'    => $code,
				'message' => $e->getMessage(),
				'details' => $e->getTraceAsString()
			];

			if ($sendEmail)
			{
				$emailData = [
					'code'    => $code,
					'url'     => $data['fullUrl'],
					'method'  => request()->method(),
					'message' => $data['message'],
					'trace'   => $data['details']
				];

				Queue::push('\Evibe\Utilities\SendEmail@mailGenericErrorToAdmin', $emailData);
			}
			$this->saveError($data);
		}
	}

	protected function saveError($data)
	{
		\SiteErrors::create([
			                    'project_id' => config('evibe.project_id'),
			                    'url'        => isset($data['fullUrl']) && $data['fullUrl'] ? $data['fullUrl'] : $data['url'],
			                    'exception'  => $data['message'],
			                    'code'       => $data['code'],
			                    'details'    => $data['details']
		                    ]);
	}

	protected function sendNonExceptionErrorReport($data, $sendEmail = true)
	{
		$errorData = [
			'code'       => isset($data['code']) && $data['code'] ? $data['code'] : 0,
			'url'        => isset($data['url']) && $data['url'] ? $data['url'] : config('evibe.host'),
			'method'     => isset($data['method']) && $data['method'] ? $data['method'] : request()->method(),
			'message'    => isset($data['message']) && $data['message'] ? $data['message'] : 'Some error occurred',
			'trace'      => isset($data['trace']) && $data['trace'] ? $data['trace'] : null,
			'project_id' => isset($data['projectId']) && $data['projectId'] ? $data['projectId'] : config('evibe.project_id'),
			'exception'  => isset($data['exception']) && $data['exception'] ? $data['exception'] : 'Unknown exception',
			'details'    => isset($data['details']) && $data['details'] ? $data['details'] : 'No clear details'
		];
		if ($sendEmail)
		{
			Queue::push('\Evibe\Utilities\SendEmail@mailGenericErrorToAdmin', $errorData);
		}
		$this->saveError($errorData);
	}
}