<?php

namespace App\Jobs;

use App\Models\Collection\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class saveCollectionPriority extends BaseJob
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $sortableIds;
	private $cityId;

	public function __construct($itemIds, $cityId)
	{
		$this->sortableIds = $itemIds;
		$this->cityId = $cityId;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$itemIds = $this->sortableIds;
		$priority = 1;

		foreach ($itemIds as $collectionId)
		{
			$collection = Collection::findOrFail($collectionId);

			$collection->update(['priority' => $priority]);
			$priority++;
		}
	}
}
