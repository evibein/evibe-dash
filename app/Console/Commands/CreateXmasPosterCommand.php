<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use Intervention\Image\Facades\Image;
use Symfony\Component\Console\Input\InputOption;

class CreateXmasPosterCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'evibe:xmas-poster';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create X-Mas poster for Santa 2015 campaign.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $galleryRoot = "../gallery";
        $directPath = '/santa-posters/';
        $uploadPath = $galleryRoot . $directPath;
        $isForce = $this->option('force');
        $pIds = $this->option('pids');
        $start = $this->option('start');
        $end = $this->option('end');
        $spid = $this->option('spid');
        $epid = $this->option('epid');
        $frames = $this->option('frames');
        $date = $this->option('date');
        $needsBg = $this->option('bg');

        $users = \CampaignUser::orderBy('pic_id');

        if ($pIds)
        {
            $users->whereIn('pic_id', explode(",", $pIds));
        }

        if ($start)
        {
            $users->where('id', '>=', $start);
        }

        if ($end)
        {
            $users->where('id', '<=', $end);
        }

        if ($frames)
        {
            $users->whereIn('frame_id', explode(",", $frames));
        }

        if ($date)
        {
            $timestamp = strtotime("midnight " . $date . " December 2015");
            $startDate = date('Y-m-d H:i:s', $timestamp);
            $endDate = date('Y-m-d H:i:s', $timestamp + (24 * 60 * 60) - 1);

            if ($startDate && $endDate)
            {
                $users->whereBetween('taken_at', [$startDate, $endDate]);
            }

        }

        if ($spid)
        {
            $users->where('pic_id', '>=', $spid);
        }

        if ($epid)
        {
            $users->where('pic_id', '<=', $epid);
        }

        $users = $users->get();
        ini_set('memory_limit', '512M');

        if (count($users))
        {
            $this->info("Found " . count($users) . " matching users. Creating posters...");
            $this->info("********");
            foreach ($users as $user)
            {
                $frameId = $user->frame_id;
                $takenDate = (int) date("d", strtotime($user->taken_at));
                $this->info("Making poster for " . $user->pic_id);
                $currentPath = $uploadPath . "pics/dec$takenDate/" . $user->poster_url;
                $posterPath = $uploadPath . "posters/dec$takenDate/" . $user->poster_url;
                $baseValue = 'straight';
                if ($takenDate < 12) $baseValue = 'cross';
                $configFrameValues = Config::get('evibe.frameValues');
                $baseFrameValues = $configFrameValues[$baseValue];

                if (!$isForce && file_exists($posterPath))
                {
                    $this->comment("    - poster available, ignoring");
                    continue;
                }

                // resize image to w:1600, h:auto
                $resizeValues = $baseFrameValues[$frameId]['pic']['dim'];
                $pic = Image::make($currentPath);
                $pic->resize($resizeValues['w'], $resizeValues['h'], function ($constraint)
                {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $this->info("   - resize done");

                // crop image
                $frameCrop = $baseFrameValues[$frameId]['pic']['crop'];
                $pic->crop($frameCrop['w'], $frameCrop['h'], $frameCrop['x'], $frameCrop['y']);
                $this->info("   - crop done");

                // apply filter
                $filterValues = $configFrameValues['filters'];
                $brightness = isset($filterValues[$takenDate]['brightness']) ? $filterValues[$takenDate]['brightness'] : 5;
                $contrast = isset($filterValues[$takenDate]['contrast']) ? $filterValues[$takenDate]['contrast'] : 10;
                $pic->brightness($brightness);
                $pic->contrast($contrast);
                $this->info("   - filter applied");

                // overlay
                $frameDims = $baseFrameValues[$frameId]['template']['dim'];
                $overlayValues = $baseFrameValues[$frameId]['pic']['overlay'];
                $theme = Image::make($uploadPath . "themes/$baseValue/$frameId.png");
                $poster = Image::canvas($frameDims['w'], $frameDims['h']);

                if ($needsBg)
                {
                    $bgImage = Image::make($uploadPath . "themes/bg.png");
                    $poster->insert($bgImage, "top-left", 0, 0);
                }

                $poster->insert($pic, "top-left", $overlayValues['x'], $overlayValues['y']);
                $poster->insert($theme, "top-left", 0, 0);
                $this->info("   - overlay done");

                // watermark only for Dec 5, 6 pictures
                if ($baseValue != 'straight')
                {
                    $branding = Image::make($uploadPath . "themes/$baseValue/" . $frameId . "_logo.png");
                    $poster->insert($branding, "top-left", 0, 0);
                    $this->info("   - watermark done");
                }

                // save poster
                $poster->save($posterPath);
                $this->info("   - poster created");
                $this->info("---------");
            }

            $this->comment("All posters created successfully");
        }
        else
        {
            $this->info("No users found, aborting...");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('force', 'f', InputOption::VALUE_NONE, 'Force creating poster.', null),
            array('pids', null, InputOption::VALUE_REQUIRED, 'Create posters for only these picture IDs. (comma separated)', null),
            array('start', null, InputOption::VALUE_REQUIRED, 'Start from this id (included)', null),
            array('end', null, InputOption::VALUE_REQUIRED, 'End with this id (included)', null),
            array('spid', null, InputOption::VALUE_REQUIRED, 'Start from this pic id (included)', null),
            array('epid', null, InputOption::VALUE_REQUIRED, 'End with this pic id (included)', null),
            array('frames', null, InputOption::VALUE_REQUIRED, 'Only for these frames (comma separated)', null),
            array('date', null, InputOption::VALUE_REQUIRED, 'Only for this date', null),
            array('bg', null, InputOption::VALUE_NONE, 'Apply background', null),
        );
    }
}
