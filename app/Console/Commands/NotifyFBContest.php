<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Queue;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class NotifyFBContest extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'evibe:notify-fb-contest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify FB contest to users to whom posters were taken on Dec 5, 6';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $opts = $this->option();
        $smsData = [];
        $users = \CampaignUser::where('status', 1)
            ->where('taken_at', '>=', '2015-12-05 00:00:00')
            ->where('taken_at', '<=', '2015-12-07 00:00:00')
            ->whereNull('contest_notified_at');

        if (isset($opts['start'])) $users->where('id', '>=', $opts['start']);
        if (isset($opts['end'])) $users->where('id', '<=', $opts['end']);
        if (isset($opts['count'])) $users->take($opts['count']);
        $users = $users->get();

        $this->info("Found " . count($users) . " users");
        foreach ($users as $user)
        {
            if ($user->phone)
            {
                $smsData[] = [
                    'id'    => $user->id,
                    'name'  => $user->name ? $user->name : 'Friend',
                    'phone' => $user->phone
                ];
            }
        }

        Queue::push('Evibe\Utilities\SendSMS@smsFBContestToExistingCUsers', $smsData);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('start', null, InputOption::VALUE_REQUIRED, 'Starting with this ID (included)', null),
            array('end', null, InputOption::VALUE_REQUIRED, 'Ending from this ID (included)', null),
            array('count', null, InputOption::VALUE_REQUIRED, 'Total count', null),
        );
    }
}
