<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Queue;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class NotifyLiveSantaToClients extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'evibe:notify-tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify all clients about Live SANTA campaign.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $opts = $this->option();
        $emailData = [];
        $smsData = [];
        $tickets = \TicketUnique::whereNull('deleted_at');

        if ($opts['start']) $tickets->where('id', '>=', $opts['start']);
        if ($opts['end']) $tickets->where('id', '<=', $opts['end']);
        if ($opts['count']) $tickets->take($opts['count']);
        if (!$opts['force']) $tickets->whereNull('notified_at');
        if ($opts['ignore'])
        {
            $ignores = explode(",", $opts['ignore']);
            $tickets->whereNotIn('email', $ignores)
                ->whereNotIn('phone', $ignores)
                ->whereNotIn('name', $ignores)
                ->whereNotIn('alt_email', $ignores)
                ->whereNotIn('alt_phone', $ignores);
        }

        $tickets = $tickets->get();
        if (count($tickets)) $this->info("Found " . count($tickets) . " users. Sending notifications ...");
        else $this->error("No users found, aborting...");

        foreach ($tickets as $ticket)
        {
            $partialData = [
                'id'    => $ticket->id,
                'name'  => ucfirst($ticket->name),
                'email' => strtolower($ticket->email)
            ];

            if ($ticket->email)
            {
                $emailData[] = array_merge($partialData, ['altEmail' => $ticket->alt_email ? $ticket->alt_email : '']);
                if ($ticket->phone)
                {
                    $smsData[] = array_merge($partialData, ['phone' => $ticket->phone]);
                    if ($ticket->alt_phone) $smsData[] = array_merge($partialData, ['phone' => $ticket->alt_phone]);
                }
            }
        }

        // send Email & SMS
        Queue::push('Evibe\Utilities\SendEmail@notifyXmasCampaignToTickets', $emailData);
        Queue::push('Evibe\Utilities\SendSMS@smsXmasCampaignToTickets', $smsData);

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('start', null, InputOption::VALUE_REQUIRED, 'Ticket start ID.'),
            array('end', null, InputOption::VALUE_REQUIRED, 'Ticket end ID'),
            array('count', null, InputOption::VALUE_REQUIRED, 'Number of tickets to consider'),
            array('ignore', null, InputOption::VALUE_REQUIRED, 'Comma separated list to ignore', null),
            array('force', null, InputOption::VALUE_NONE, 'Force send', null),
        );
    }
}
