@extends('layout.base')

@section('content')
<div class="container-fluid">
	<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
		<div class="app-tabs">
			@include('app/tabs')
		</div>
		<div class="page-content trends-wrap">
			<div class="trends-panel-cnt">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Nearby Areas</h2>
					</div>
					<div class="panel-body">
						<div class="col-md-6">
							<div>
								<div class="col-md-8">
									<div class="form-group">
										<label for="selectCity">City</label>
										<select id="selectCity" class="form-control">
											<option class="default" value="-1" selected="selected">--select--</option>
											@foreach ($data['cities'] as $city)
												<option value="{{ $city->id }}">{{ $city->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<div>
											<label for="selectArea" class="in-blk valign-mid">Area</label>
											<a id="addNewArea" data-redirectto="/nearby-areas" class="in-blk valign-mid pad-l-10">
												<i class="glyphicon glyphicon-plus"></i>
												Add New Area
											</a>
										</div>
										<select name="selectArea" id="selectArea" class="form-control">
											<option class="default" value="-1">--select--</option>
											@foreach ($data['areas'] as $area)
												<option value="{{ $area->id }}" data-cityid="{{ $area->city_id }}" class="hide">{{ $area->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div>
								<div class="col-md-12">
									<div class="areas-tags-wrap hide">
										<div class="form-group">
											<label>Existing Nearby Areas</label>
											<ul class="areas-tags ls-none no-mar no-pad"></ul>
											<div class="pad-b-4 no-areas text-danger hide">No nearby areas found. Add nearby areas using control below.</div>
										</div>
										<div class="form-group">
											<label for="nearbyAreas">Add Nearby Areas</label>
											<ul id="nearbyAreas"></ul>
											<a id="updateAreasBtn" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i> Update</a>
											<input id="nearbyAreasInput" type="text" class="form-control hide">
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
	
	$(document).ready(function() {

		var NearbyMethods = {};

		NearbyMethods.onSelectCity  = function() {
			var th = $('#selectCity');
			$('#selectArea option:not(.default)').addClass('hide');
			if (th.find('option:selected').length && th.find('option:selected').val() != -1) {
				var cityId = $('#selectCity').val();
				$('#selectArea option[data-cityid='+ cityId +']').removeClass('hide'); // show only relevant options
			}
			else {
				$('#selectArea').val(-1);
				NearbyMethods.hideTagsWrap(); // disable stuff
			}
		};

		NearbyMethods.onSelectArea = function() {
			var th = $('#selectArea');
			if (th.find('option:selected').length && th.find('option:selected').val() != -1) {
				var selected = th.find('option:selected');
				var cityId = selected.data('cityid');
				var areaId = th.val();
				var url = '';

				$.ajax({
					url: '/nearby-areas/'+ cityId +'/'+ areaId,
					type: 'GET',
					dataType: 'json',
					data: {},
					success: function (data, textStatus, jqXHR) {
						if (data.success) {
							// show taggable input
							var allAreas = [];
							if (data.nearbyAreas.length == 0) {
								NearbyMethods.noNearbyAreas();
								$('#selectArea option:not(:selected)').each(function(key, el) {
									if ($(this).attr('value') != -1) {
										allAreas.push({
											id: $(this).attr('value'),
											value: $(this).text()
										});
									}
								});
							}
							else {
								NearbyMethods.hasNearbyAreas();
								for (key in data.nearbyAreas) {
									var currentArea = data.nearbyAreas[key];
									var markup = '<li data-cityid="'+ currentArea['city_id'] +'" data-id="'+ currentArea['id'] +'">'+
													currentArea['name'] +
													'<a class="del-nearby-area pad-l-3 btn btn-xs"><i class="glyphicon glyphicon-trash"></i></a>' +
												'</li>';
									$('.areas-tags').append(markup);
								}

								$('#selectArea option:not(:selected)').each(function(key, el) {
									var cVal = $(this).attr('value');
									if (cVal != -1 && !NearbyMethods.isExistingArea(cVal, data.nearbyAreas)) {
										allAreas.push({
											id: $(this).attr('value'),
											value: $(this).text()
										});
									}
								});
							}

							// category tags
							$('#nearbyAreas').tagit({
								singleField: true,
								singleFieldNode: $('#nearbyAreasInput'),
								showAutocompleteOnFocus: true,
								removeConfirmation: true,
								allowSpaces: true,
								placeholderText: 'Start typing...',
								autocomplete: {
									source: allAreas
								}
							});
						} else  {
							window.showNotyError();
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError();
					}
				});
			}
		};

		NearbyMethods.updateAreas = function(event) {
			event.preventDefault();
			var areaId = $('#selectArea').val();
			$.ajax({
				url: '/nearby-areas/'+ areaId +'/save',
				type: 'POST',
				dataType: 'JSON',
				data: { 'areas': $('#nearbyAreasInput').val() },
				success: function (data, textStatus, jqXHR) {
					if (data.success) {
						window.location.reload();
					} else {
						window.showNotyError();
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					window.showNotyError();
				}
			})

		};

		NearbyMethods.isExistingArea = function (needle, haystack) {
			for (key in haystack) {
				if (haystack[key]['id'] == needle) return true;
			}
			return false;
		};

		NearbyMethods.deleteNearbyArea = function (event) {
			if (!confirm('Are you sure?')) return false;
			var area1 = $('#selectArea').val();
			$.ajax({
				url: '/nearby-areas/delete' ,
				type: 'GET',
				dataType: 'json',
				data: {
					area1: area1,
					area2: $(this).parent().data('id')
				},
				success: function (data, textStatus, jqXHR) {
					if (data.success) window.location.reload();
					else window.showNotyError();
				},
				error: function (jqXHR, textStatus, errorThrown) {
					window.showNotyError();
				}
			});
		};

		NearbyMethods.noNearbyAreas = function() {
			$('.areas-tags-wrap').removeClass('hide');
			$('.areas-tags-wrap .no-areas').removeClass('hide');
			$('.areas-tags').empty();
			$('.areas-tags').addClass('hide');
		};

		NearbyMethods.hasNearbyAreas = function() {
			$('.areas-tags-wrap').removeClass('hide');
			$('.areas-tags-wrap .no-areas').addClass('hide');
			$('.areas-tags').empty();
			$('.areas-tags').removeClass('hide');
		};

		NearbyMethods.hideTagsWrap = function() {
			$('.areas-tags-wrap').addClass('hide');
			$('.areas-tags').empty();
		};

		// add new location
		$('#addNewArea').click(function (event) {

			event.preventDefault();

			$('#addNewAreaModal').data('redirectto', $(this).data('redirectto'));
			$('#addNewAreaModal').modal();

		});

		// init
		NearbyMethods.onSelectCity();
		NearbyMethods.onSelectArea();
		NearbyMethods.hideTagsWrap();

		// event handlers
		$('#selectCity').on('change', NearbyMethods.onSelectCity);
		$('#selectArea').on('change', NearbyMethods.onSelectArea);
		$('#updateAreasBtn').on('click', NearbyMethods.updateAreas);
		$('.areas-tags').on('click', '.del-nearby-area', NearbyMethods.deleteNearbyArea);

	});

</script>
@stop
