@extends('layout.base')

@section('content')
	<div class="text-center">
		<h3 class="in-blk mar-t-5">Revamp Tags for</h3>
		<div class="in-blk form-group pad-l-10">
			<select class="form-control tags-revamp-categories">
				@foreach($data['typeTicket'] as $type)
					<option value="{{ $type->id }}"
							@if($type->id == request("typeTicketId")) selected @endif>{{ $type->name }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="in-blk form-group pad-l-10">
			<select class="form-control tags-revamp-events">
				@foreach($data['typeEvent'] as $event)
					<option value="{{ $event->id }}"
							@if($event->id == request("eventId")) selected @endif>{{ $event->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="in-blk form-group pad-l-10">
			<select class="form-control tags-revamp-cities">
				@foreach($data['cities'] as $city)
					<option value="{{ $city->id }}"
							@if($city->id == request("cityId")) selected @endif>{{ $city->name }}
					</option>
				@endforeach
			</select>
		</div>
		@if(request('eventId') || request('cityId') || request('typeTicketId'))
			<div class="in-blk pad-l-10">
				<a class="btn btn-warning btn-xs" href="{{ route("revamp.tags.list") }}">Clear Filter</a>
			</div>
		@endif
		<div class="container text-center">
			<div class="alert alert-warning">
				<i class="glyphicon glyphicon-warning-sign"></i> Tags Revamp will only work for decors all the cities.
			</div>
		</div>
	</div>
	<div class="pad-l-10 pad-r-10 tags-revamp-section">
		<ul class="list-unstyled">
			@if(count($data['productsData']) > 0)
				@if($data['productTypeId'] == config('evibe.ticket_type.decors'))
					@foreach($data['productsData']['products'] as $decor)
						<li data-id="{{ $decor->id }}">
							<div class="panel panel-default mar-b-10">
								<div class="panel-heading">
									<div class="pull-left font-16">
										<span class="badge-up col-story hide">
											<i class="glyphicon glyphicon-chevron-down"></i>
										</span>
										<span class="badge-down col-story">
											<i class="glyphicon glyphicon-chevron-up"></i>
										</span>
										{{ "[".$decor->code ."] ". ucfirst($decor->name)}}
										<a target="_blank" class="btn btn-xs btn-link" href="{{ route("decor.info.view", $decor->id) }}">View Profile</a>
									</div>
									@if($latestDecorTag = $data['productsData']['tags']->where('decor_id', $decor->id)->sortByDesc('updated_at')->first())
										<div class="pull-right">
											<span>Tags last updated at :</span>
											<span class="story-event-badge">{{ date("d M y, h:i A", strtotime($latestDecorTag->updated_at)) }}</span>
										</div>
									@endif
									<div class="clearfix"></div>
								</div>
								<div class="panel-body">
									<div class="col-sm-2 no-pad">
										@php
											$imageUrl = $data['productsData']['gallery']->where('decor_id', $decor->id)->first();
											$url = $imageUrl ? $imageUrl->url : "";
										@endphp
										<img width="200" src="{{ config("evibe.gallery.host")."/decors/".$decor->id."/images/".$url }}">
										<div class="pad-t-10 font-16 text-bold text-center">
											<span>Price: </span>
											<span class="rupee-font">R</span> {{ \Evibe\Facades\AppUtilFacade::formatPrice($decor->min_price) }}
										</div>
									</div>
									<div class="col-sm-10 no-pad-r panel-body-{{ $decor->id }}">
										@if ($data['relevantEvents']->count())
											<div class="events-form-container pad-b-10">
												<label for="">Event Supported</label>
												<form class="form-add-event" action="{{ route('revamp.tags.add.event', $decor->id) }}" method="post">
													<div class="form-group  in-blk">
														<select class="form-control event-tag" name="eventId" data-decor-id="{{ $decor->id }}">
															<option value="-1" selected="selected">-- Select Event --</option>
															@foreach ($data['relevantEvents'] as $event)
																<option value="{{ $event->id }}">{{ $event->name }}</option>
															@endforeach
														</select>
													</div>
													<button type="submit" class="btn btn-info">Add Event</button>
												</form>
												<div class="event-tags-container-{{ $decor->id }}">
													@php $decorEvents = $data['productsData']['events']->where('decor_id', $decor->id); @endphp
													@foreach ($decorEvents as $decorEvent)
														<div class="alert-danger mar-r-5 mar-b-5 pad-5 in-blk revamp-option-event-{{ $decorEvent->event_id."-".$decor->id }}">
															{{ $decorEvent->name }}
															@if ($decorEvent->event_id != $data['currentEventId'])
																<i data-value="{{ $decorEvent->event_id }}" data-decor-id="{{ $decor->id }}" class="glyphicon glyphicon-trash delete-revamp-tag-event"></i>
															@endif
														</div>
													@endforeach
												</div>
											</div>
											<hr class="mar-t-10">
										@endif
										<div class="tags-container">
											<form class="form-add-tags" action="{{ route("revamp.tags.add.tag", $decor->id) }}" data-decor-id="{{ $decor->id }}" method="post">
												<div class="form-group in-blk">
													<label for="parentTagId">Select Parent Tag</label>
													<select class="form-control parent-tag" name="parentTagId" data-decor-id="{{ $decor->id }}">
														<option value="-1" selected="selected">-- Select Parent Tag --</option>
														@foreach($data['typeTags']->where('parent_id', null) as $tag)
															<option value="{{ $tag->id }}">{{ $tag->name }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group in-blk">
													<label for="childTagId">Select Child Tag</label>
													<select class="form-control child-tag child-tag-{{ $decor->id }}" name="childTagId">
														<option value="-1">-- Select Child Tag --</option>
														@foreach($data['typeTags']->where('parent_id', '<>' , null) as $tag)
															<option value="{{ $tag->id }}">{{ $tag->name }}</option>
														@endforeach
													</select>
												</div>
												<button type="submit" class="btn btn-primary">Add Tag</button>
											</form>
											<div>
												@foreach ($data['relevantParentTags'] as $rptName => $rptId)
													@php $filteredTags = $data['productsData']['tags']->where('decor_id', $decor->id)->where('parent_id', $rptId); @endphp
													<div class="col-sm-4 mar-b-15">
														<label for="">{{ $rptName }}</label>
														<div class="tag-category-{{ $rptId }}__{{ $decor->id }}__{{ config('evibe.ticket_type.decors') }}">
															@foreach($filteredTags as $currentTag)
																<div class="alert-info mar-r-5 mar-b-5 pad-5 in-blk revamp-option-tag-{{ $currentTag->tag_id."-".$decor->id }}">{{ $currentTag->name }}
																	<i data-value="{{ $currentTag->tag_id }}" data-decor-id="{{ $decor->id }}" class="glyphicon glyphicon-trash delete-revamp-tag"></i>
																</div>
															@endforeach
														</div>
													</div>
												@endforeach
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
					@endforeach
					<div class="text-center">
						{{ $data['productsData']['products']->links() }}
					</div>
				@endif
			@endif
			@if(!count($data['productsData']) || !count($data['productsData']['products']))
				<div class="container text-center">
					<div class="alert alert-danger mar-t-30">
						<i class="glyphicon glyphicon-warning-sign"></i> No options found. (Please select a valid combination).
					</div>
				</div>
			@endif
		</ul>
	</div>
	<input type="hidden" value="{{ route("revamp.tags.delete.tag") }}" id="tagDeleteUrl">
	<input type="hidden" value="{{ route("revamp.tags.delete.event") }}" id="eventDeleteUrl">
	<div class="hide">
		<select id="duplicateChildTags">
			@foreach($data['typeTags'] as $tag)
				@if(!is_null($tag->parent_id))
					<option value="{{ $tag->id }}" data-parentid="{{ $tag->parent_id }}">{{ $tag->name }}</option>
				@endif
			@endforeach
		</select>
	</div>
@endsection

@section('javascript')
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			// On change tag get tags according to that event
			//@todo: Move the change URL function from ticket list page to base & use that function to change the URL
			$('.tags-revamp-events, .tags-revamp-categories, .tags-revamp-cities').on('change', function () {
				if ($('.tags-revamp-events').val() >= 1 || $('.tags-revamp-categories').val() >= 1 || $('.tags-revamp-cities').val() >= 1) {

					var url = $(location).attr('protocol') + "//" + $(location).attr('hostname') + $(location).attr('pathname')
						+ "?eventId=" + $('.tags-revamp-events').val()
						+ "&cityId=" + $('.tags-revamp-cities').val()
						+ "&typeTicketId=" + $('.tags-revamp-categories').val();

					window.location.replace(url);
				} else {
					showNotyError("Please select a valid event Id");
				}
			});

			$('.panel-heading').on('click', function (event) {
				event.preventDefault();
				$(this).parents('.panel-default').find('.panel-body').toggleClass('hide');
				$(this).parents('.panel-default').find('.badge-down').toggleClass('hide');
				$(this).parents('.panel-default').find('.badge-up').toggleClass('hide');
			});

			$('.btn-decor-profile-link').on('click', function (e) {
				e.preventDefault();

				var url = $(this).attr('href');
				var win = window.open(url, '_blank');
				win.focus()
			});

			// add tags
			$('.parent-tag').on('change', function () {
				var options = $('<option value="-1" data-parentid="-1">-- Select Child Tag--</option>');
				var childTags = '';
				if ($(this).val() !== -1) {
					var val = $(this).val(); // add child tags
					childTags = $('#duplicateChildTags option[data-parentid="' + val + '"]').clone();
				}

				var decorId = $(this).data('decor-id');
				options = $.merge(options, childTags);
				$('.child-tag-' + decorId).html(options);
			});

			$('.form-add-tags').submit(function (e) {
				e.preventDefault();
				var formUrl = $(this).attr('action');
				var childTagId = parseInt($(this).find('.child-tag').val(), 0);

				if (childTagId === -1 || !childTagId) {
					alert("Please select child tag");
					return false;
				}

				$.ajax({
					url: formUrl,
					type: 'post',
					dataType: 'json',
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function (data) {
						if (data.success) {
							showNotySuccess("Tag added successfully");
							var parentTagId = $(data.tagTemplate).attr('data-parenttagid');
							var productId = data.productId;
							var categoryId = data.categoryId;
							var key = '.tag-category-' + parentTagId + '__' + productId + '__' + categoryId;
							$(key).append(data.tagTemplate);
						}
						else if (data.error) {
							showNotyError(data.error);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showNotyError('Error occurred while deleting tag, Please try again.')
					}
				});
			});

			$('.form-add-event').submit(function (e) {
				e.preventDefault();
				var formUrl = $(this).attr('action');
				var eventId = parseInt($(this).find('.event-tag').val(), 0);

				if (eventId === -1 || !eventId) {
					alert("Please select an event");
					return false;
				}

				$.ajax({
					url: formUrl,
					type: 'post',
					dataType: 'json',
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function (data) {
						if (data.success) {
							showNotySuccess("Event added successfully");
							$('.event-tags-container-' + data.decorId).append(data.eventTemplate);
						}
						else if (data.error) {
							showNotyError(data.error);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showNotyError('Error occurred while deleting tag, Please try again.')
					}
				});
			});

			$("body").on("click", ".delete-revamp-tag", function (e) {
				e.preventDefault();
				var tagId = $(this).data("value");
				var decorId = $(this).data("decor-id");
				if (confirm("Are you sure to delete this tag?")) {
					$.ajax({
						url: $('#tagDeleteUrl').val() + "/" + tagId + "/" + decorId,
						type: 'post',
						dataType: 'json',
						success: function (data) {
							$('.revamp-option-tag-' + tagId + '-' + decorId).remove();
							showNotySuccess("Tag deleted successfully");
						},
						error: function (jqXHR, textStatus, errorThrown) {
							showNotyError('Error occurred while deleting tag, Please try again.')
						}
					});
				}
			});

			$("body").on("click", '.delete-revamp-tag-event', function (e) {
				e.preventDefault();
				var eventId = $(this).data("value");
				var decorId = $(this).data("decor-id");
				if (confirm("Are you sure to delete this event?")) {
					$.ajax({
						url: $('#eventDeleteUrl').val() + "/" + eventId + "/" + decorId,
						type: 'post',
						dataType: 'json',
						success: function () {
							$('.revamp-option-event-' + eventId + '-' + decorId).remove();
							showNotySuccess("Event deleted successfully");
						},
						error: function (jqXHR, textStatus, errorThrown) {
							showNotyError('Error occurred while deleting tag, Please try again.')
						}
					});
				}
			});

		});
	</script>
@endsection