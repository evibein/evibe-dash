@extends('layout.base')

@section('custom-head')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.css"/>
@endsection

@section('content')
	@php $filterTicketsQueryString = request('dfilter') ? request('dfilter') : "new-tickets"; @endphp
	<div class="sales-dashboard-wrap" style="margin-top: -10px">
		<div class="col-xs-12 no-pad-l no-pad-r">
			@if (AppUtil::isAdmin())
				<div class="col-sm-6 text-center font-10 show-admin-metrics-data">
					<span class="pull-left font-14">This Month</span>
					<table class="table table-responsive table-bordered text-center table-fixed no-mar-b sales-metrics-data" style="border: none">
						<tr>
							<td class="">
								<div class="text-bold font-20 valid-tickets-month"></div>
								<div class="in-blk font-13 pad-t-3">Valid Tickets</div>
							</td>
							<td class="">
								<div class="text-bold font-20 orders-count-month"></div>
								<div class="in-blk font-13 pad-t-3">Number of Orders</div>
							</td>
							<td class="">
								<div class="text-bold font-20 orders-aov-month"></div>
								<div class="in-blk font-13 pad-t-3">Average Order Value</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-6 text-center font-10 no-pad-l">
					<span class="pull-left font-14">This Week</span>
					<table class="table table-responsive table-bordered text-center table-fixed no-mar-b sales-metrics-data" style="border: none">
						<tr>
							<td class="">
								<div class="text-bold font-20 valid-tickets-week"></div>
								<div class="in-blk font-13 pad-t-3">Valid Tickets</div>
							</td>
							<td class="">
								<div class="text-bold font-20 orders-count-week"></div>
								<div class="in-blk font-13 pad-t-3">Number of Orders</div>
							</td>
							<td class="">
								<div class="text-bold font-20 orders-aov-week"></div>
								<div class="in-blk font-13 pad-t-3">Average Order Value</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="clearfix"></div>
			@endif
			<div class="mar-t-20 mar-b-20">
				<div class="col-sm-12 mar-b-10">
					<div class="font-18 pad-t-8 in-blk">My Metrics</div>
					<div class="pull-right in-blk mar-b-10">
						<div class="col-sm-5 no-pad">
							<input type="text" class="form-control in-blk filter-anlytics-date filter-anlytics-creation-start-time text-center font-13-imp " value="" id="startTimeOutSideCollapse">
						</div>
						<div class="col-sm-1 no-pad text-center">
							<div class="text-center mar-t-10 text-bold filter-title">To</div>
						</div>
						<div class="col-sm-5 no-pad">
							<input type="text" class="form-control font-13-imp filter-anlytics-date filter-anlytics-creation-end-time text-center" id="endTimeOutSideCollapse" value="">
						</div>
						<div class="col-sm-1 no-pad text-center">
							<div class="text-center filter-date-btn-wrap">
								<a class="btn btn-xs btn-primary btn-filter-by-cd">GO</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					@if (AppUtil::isAdmin())
						<a href="{{ route('crmDashBoard.new.tickets.list') }}" class="btn btn-link btn-xs text-center pull-right">Switch to old version</a>
					@endif
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-11 no-pad-l no-pad-r">
					<div class="col-sm-6 text-center font-10">
						<table class="table table-responsive table-bordered text-center table-fixed no-mar-b sales-metrics-data" style="border: none">
							<tr>
								<td class="ticket-status-metric-wrap alert-success show-filter-data" data-ticket-status-id="{{ config("evibe.status.booked") }}" data-query-phrase='booked'>
									<div class="text-bold font-20 showBookedTicketsByHandler"></div>
									<div class="in-blk pad-t-3 filter-val">TOTAL BOOKED</div>
								</td>
								<td class="ticket-status-metric-wrap show-filter-data" data-ticket-status-id="{{ config("evibe.status.enquiry") }}" data-query-phrase='just enquiry'>
									<div class="text-bold font-20 showEnquiriesByHandler non-new-tickets"></div>
									<div class="text-bold font-18 non-new-tickets-NA hide">-</div>
									<div class="in-blk pad-t-3 filter-val">JUST ENQUIRY</div>
								</td>
								<td class="ticket-status-metric-wrap show-filter-data" data-ticket-status-id="{{ config("evibe.status.no_response") }}" data-query-phrase='no response'>
									<div class="text-bold font-20 showNoResponseByHandler non-new-tickets"></div>
									<div class="text-bold font-18 non-new-tickets-NA hide">-</div>
									<div class="in-blk pad-t-3 filter-val">NO RESPONSE</div>
								</td>
								<td class="ticket-status-metric-wrap show-filter-data @if($filterTicketsQueryString != "new-tickets") hide @endif filter-btns new-tickets" data-ticket-status-id="{{ config("evibe.status.initiated") }}" data-query-phrase='initiated'>
									<div class="text-bold font-20 showInitiativesByHandler non-new-tickets"></div>
									<div class="text-bold font-18 non-new-tickets-NA hide">-</div>
									<div class="in-blk pad-t-3 filter-val">INITIATED</div>
								</td>
								<td class="ticket-status-metric-wrap show-filter-data" data-ticket-status-id="{{ config("evibe.status.followup") }}" data-query-phrase='followup'>
									<div class="text-bold font-20 showFollowupsByHandler non-new-tickets"></div>
									<div class="text-bold font-18 non-new-tickets-NA hide">-</div>
									<div class="in-blk pad-t-3 filter-val">NEW FOLLOWUP</div>
								</td>
								<td class="ticket-status-metric-wrap filter-btns @if($filterTicketsQueryString != "new-tickets") hide @endif active new-tickets">
									<div class="text-bold font-20 showTotalCountByHandler"></div>
									<div class="in-blk pad-t-3">NEW TICKETS</div>
								</td>
								<td class="ticket-status-metric-wrap filter-btns @if($filterTicketsQueryString != "followup") hide @endif active followup">
									<div class="text-bold font-20 showReturnFollowupsByHandler"></div>
									<div class="in-blk pad-t-3">RETURN FOLLOWUP</div>
								</td>
								<td class="ticket-status-metric-wrap filter-btns @if($filterTicketsQueryString != "progress") hide @endif active progress">
									<div class="text-bold font-20 showInProgressByHandler"></div>
									<div class="in-blk pad-t-3">IN PROGRESS</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-sm-6 text-center font-10 no-pad-l">
						<table class="table table-responsive table-bordered text-center table-fixed no-mar-b sales-metrics-data" style="border: none">
							<tr>
								<td class="ticket-status-metric-wrap alert-success show-filter-data" data-query-phrase='booked' data-ticket-status-id="{{ config("evibe.status.booked") }}">
									<div class="text-bold font-20 showBookedTickets"></div>
									<div class="in-blk pad-t-3 filter-val">TOTAL BOOKED</div>
								</td>
								<td class="ticket-status-metric-wrap show-filter-data" data-query-phrase='just enquiry' data-ticket-status-id="{{ config("evibe.status.enquiry") }}">
									<div class="text-bold font-20 showEnquiries non-new-tickets"></div>
									<div class="text-bold font-18 non-new-tickets-NA hide">-</div>
									<div class="in-blk pad-t-3 filter-val">JUST ENQUIRY</div>
								</td>
								<td class="ticket-status-metric-wrap show-filter-data" data-query-phrase='no response' data-ticket-status-id="{{ config("evibe.status.no_response") }}">
									<div class="text-bold font-20 showNoResponse non-new-tickets"></div>
									<div class="text-bold font-18 non-new-tickets-NA hide">-</div>
									<div class="in-blk pad-t-3 filter-val">NO RESPONSE</div>
								</td>
								<td class="ticket-status-metric-wrap show-filter-data @if($filterTicketsQueryString != "new-tickets") hide @endif filter-btns new-tickets" data-query-phrase='initiated' data-ticket-status-id="{{ config("evibe.status.initiated") }}">
									<div class="text-bold font-20 showInitiatives non-new-tickets"></div>
									<div class="text-bold font-18 non-new-tickets-NA hide">-</div>
									<div class="in-blk pad-t-3 filter-val">INITIATED</div>
								</td>
								<td class="ticket-status-metric-wrap show-filter-data" data-query-phrase='followup' data-ticket-status-id="{{ config("evibe.status.followup") }}">
									<div class="text-bold font-20 showFollowups non-new-tickets"></div>
									<div class="text-bold font-18 non-new-tickets-NA hide">-</div>
									<div class="in-blk pad-t-3 filter-val">NEW FOLLOWUP</div>
								</td>
								<td class="ticket-status-metric-wrap updateDataBtn filter-btns @if($filterTicketsQueryString != "new-tickets") hide @endif active new-tickets" data-id="new-tickets">
									<div class="text-bold font-20 showTotalCount"></div>
									<div class="in-blk pad-t-3">NEW TICKETS</div>
								</td>
								<td class="ticket-status-metric-wrap updateDataBtn filter-btns active @if($filterTicketsQueryString != "followup") hide @endif followup" data-id="followup">
									<div class="text-bold font-20 showReturnFollowups"></div>
									<div class="in-blk pad-t-3">FOLLOWUPS</div>
								</td>
								<td class="ticket-status-metric-wrap updateDataBtn filter-btns active @if($filterTicketsQueryString != "progress") hide @endif progress" data-id="progress">
									<div class="text-bold font-20 showTotalCount"></div>
									<div class="in-blk pad-t-3">IN PROGRESS</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-1 no-pad-l">
					<div class="new-tickets filter-btns @if($filterTicketsQueryString != "new-tickets") hide @endif">
						<div class="btn btn-xs btn-default full-width updateDataBtn" data-id="followup">Followups</div>
						<div class="btn btn-xs mar-t-5 btn-default full-width updateDataBtn" data-id="progress">In Progress</div>
					</div>
					<div class="followup filter-btns @if($filterTicketsQueryString != "followup") hide @endif">
						<div class="btn btn-xs btn-default full-width updateDataBtn" data-id="new-tickets">New Tickets</div>
						<div class="btn btn-xs mar-t-5 btn-default full-width updateDataBtn" data-id="progress">In Progress</div>
					</div>
					<div class="progress filter-btns @if($filterTicketsQueryString != "progress") hide @endif" style="height: auto;">
						<div class="btn btn-xs btn-default full-width updateDataBtn" data-id="new-tickets">New Tickets</div>
						<div class="btn btn-xs mar-t-5 btn-default full-width updateDataBtn" data-id="followup">Followups</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="col-xs-7" style="border-right: 1px solid #e1e1e1">
			<div>
				<div class="tickets-list-wrap">
					<div class="tickets-loading-wrap" style="min-height: 100px">
						<div class="loader mar-l-20" style="margin-top: 100px;">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
					</div>
					<div class="tickets-data"></div>
				</div>
			</div>
		</div>
		<div class="col-xs-5">
			<div>
				<div class="text-center">
					<div class="col-sm-10">
						<input id="searchQuery" type="text" name="query" class="form-control" placeholder="Search tickets by phone" value="{{ request('query') }}"/>
					</div>
					<div class="col-sm-2 no-pad-l pull-left">
						<a class="btn btn-info" id="salesNumberSearch">Search</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr class="hr-text no-pad no-mar-t mar-b-5">
				<div id="searchTicketData">
					@if (request()->has('query') && request('query'))
						@include("utilities.sales.search-data")
					@endif
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div id="modalShowTicketUpdate" class="modal" tabindex="-1" role="dialog" data-url="">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<div class="pull-left">
						<h4 class="no-mar">Showing All Updates</h4>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="modal-body">
					<div class="form-group ticket-update-body">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal"><i
								class="glyphicon glyphicon-thumbs-up"></i> Got it
					</button>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="customerPhone" value="{{ request('query') }}">
	<input type="hidden" id="dFilter" value="new-tickets">
	<input type="hidden" id="filterUrl" value="{{ route('sales.tickets.list', "new-tickets") }}">
	<input type="hidden" id="ticketUpdateModalId" value="">
	<input type="hidden" id="returnBookedTicketCount" value="{{ $data["returnTicketBookingCount"] }}">
	<input type="hidden" id="returnBookedTicketCountByHandler" value="{{ $data["returnTicketBookingCountByHandler"] }}">
@stop

@section('app-modals')
	@parent
	@include('tickets.list.modals')
@endsection

@section('javascript')(
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		//Load initial tickets with url
		$(window).load(function () {
			loadTicketsData();
			loadUpdateTicketStatusModal();
		});

		(function initFunctions() {
			$("body").on("submit", "form", function () {
				$(this).submit(function () {
					return false;
				});
				return true;
			});

			$("#salesNumberSearch").on("click", function () {
				updateTicketListData($("#searchQuery").val());
			});

			$("#searchQuery").on('keypress', function (e) {
				if (e.which === 13) {
					updateTicketListData($(this).val());
				}
			});

			$('.sales-dashboard-wrap').on("click", ".apply-filters-btn", function () {
				updateUrlByFilters(null);
			}).on("click", ".ticket-search-phone", function () {
				updateTicketListData($(this).data("id"), $(this).data("email"), $(this).data("ticket-id"));
			}).on("click", ".sales-clear-filters-btn", function () {
				location.href = $("#filterUrl").val() + "?dfilter=" + $("#dFilter").val();
			});
		})();

		(function metricsDataClickTriggers() {
			$(".updateDataBtn").on("click", function () {
				var dFilterVal = $(this).data("id");
				if (dFilterVal !== $("#dFilter").val()) {
					$("#dFilter").val(dFilterVal);

					$(".tickets-loading-wrap").show();
					$(".tickets-data").empty();

					$(".filter-btns").addClass("hide");
					$(".updateDataBtn").addClass("disabled");
					$("." + dFilterVal).removeClass("hide");

					// Load Tickets
					loadTicketsData();
				} else {
					$(".sales-datatable-status-filter").val("").trigger($.Event("keyup", {keyCode: 13}));
				}
			});

			$(".show-filter-data").on("click", function () {
				var status = $(this).data('query-phrase');
				$(".sales-datatable-status-filter").val(status).trigger($.Event("keyup", {keyCode: 13}));
			});
		})();

		(function dateClickFilters() {
			$('.btn-filter-by-cd').on('click', function (event) { // filter by party date
				event.preventDefault();
				var startDate = $('.filter-anlytics-creation-start-time').val();
				var endDate = $('.filter-anlytics-creation-end-time').val();

				if (!startDate || !endDate) {
					showNotyError("Please select both start date & end date.")
				} else {
					var url = '';
					var params = new window.URLSearchParams(window.location.search);
					var dFilter = params.get('dfilter');
					if (dFilter) {
						url = url.concat('?dfilter=', dFilter, '&filterChosenStartingTime=', startDate, "&filterChosenEndingTime=", endDate);
					} else {
						url = url.concat('?filterChosenStartingTime=', startDate, "&filterChosenEndingTime=", endDate);
					}

					window.location.href = url;
				}
			});
		})();

		(function btnSeeAllUpdates() {
			$('.sales-dashboard-wrap').on('click', '.ticket-update-info', function () {
				window.showLoading();
				var url = $(this).data('url');
				var $modal = $('#modalShowTicketUpdate');
				$modal.modal('hide');

				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					success: function (data) {
						window.hideLoading();
						if (data.success) {
							$modal.find('.ticket-update-body').empty();

							$.each(data.ticketUpdates, function (key, value) {
								var markup = '<div class="status-updates-wrap pad-t-8">[' + value['date'] + '] ==> Status updated to <b>' + value['status'] + '</b> by ' +
									'<u>' + value['handler'] + '</u>. Update happened through ' + value['source'] + ' source ';
								if (value['comments']) {
									markup += 'with <b>comments: </b>' + value['comments'];
								}
								markup += '</div>';

								$modal.find('.ticket-update-body').append(markup);
							});

							$modal.modal('show');
						}
					},
					fail: function () {
						window.showNotyError("Error while fetching updates, Please try again.");
					}
				})
			});
		})();

		(function showAdminMetricsData(){
			if($(".show-admin-metrics-data").length > 0) {
				$.ajax({
					url: "/sales/tickets/admin/data",
					type: 'POST',
					dataType: 'json',
					success: function (data) {
						window.hideLoading();
						if (data.success) {
							$(".valid-tickets-month").append(data.month.tickets.count);
							$(".orders-count-month").append(data.month.bookings.count);
							$(".orders-aov-month").append(data.month.bookings.aov);
							$(".valid-tickets-week").append(data.week.tickets.count);
							$(".orders-count-week").append(data.week.bookings.count);
							$(".orders-aov-week").append(data.week.bookings.aov);
						}
					},
					fail: function () {
						window.showNotyError("Error while fetching admin metrics data, Please try again.");
					}
				})
			}
		})();

		var table = "";

		function loadUpdateTicketStatusModal() {
			$.ajax({
				url: '/tickets/status-update/details',
				type: 'GET',
				dataType: 'json',
				success: function (data, textStatus, jqXHR) {
					var optionHtml = '';
					var relatedTicket = '';

					if (data.ticketsStatuses) {
						$.each(data.ticketsStatuses, function (key, statusOptions) {
							optionHtml = optionHtml + "<option value='" + statusOptions.id + "'>" + statusOptions.name + "</option>";
						});

						$(optionHtml).appendTo('.ticket-statuses-through-ajax');
					}
					if (data.ticketsData) {
						$.each(data.ticketsData, function (count, relatedTicketDetails) {
							relatedTicket = relatedTicket + "<option value='" + relatedTicketDetails.id + "'>"
								+ relatedTicketDetails.id + " / " + relatedTicketDetails.name + " / " + relatedTicketDetails.email
								+ " / " + relatedTicketDetails.phone + "</option>";
						});

						$(relatedTicket).appendTo('.related-ticket-data-through-ajax');
					}

					$('#tuDate, #followUpDateFromModal, #updateClosureDate').datetimepicker({
						step: 30,
						minDate: 0
					});
					$('#relatedTicket').selectize();

					$('#tuStatus').on('change', function () {
						var updatedStatus = $('#tuStatus :selected').val();
						if (updatedStatus == 15) {
							$('.followup-date_wrap').addClass('hide');
							$('.no-response-date-wrap').addClass('hide');
							$('.related_ticket_wrap').removeClass('hide');
						} else if (updatedStatus == 7) {
							$('.related_ticket_wrap').addClass('hide');
							$('.no-response-date-wrap').addClass('hide');
							$('.followup-date_wrap').removeClass('hide');
						} else if (updatedStatus == 17) {
							$('.related_ticket_wrap').addClass('hide');
							$('.followup-date_wrap').addClass('hide');
							$('.no-response-date-wrap').removeClass('hide');
						} else {
							$('.related_ticket_wrap').addClass('hide');
							$('.followup-date_wrap').addClass('hide');
							$('.no-response-date-wrap').addClass('hide');
						}

						if (updatedStatus == $('#tsDuplicate').val()) {
							$('#tuType').val('other');
							$('#tuComments').val('Another valid ticket already exists!');
						}
					});

					$("#followUpDateFromModal").blur(function () {
						$("#updateClosureDate").val($(this).val());
					});

					$('#btnUpdateStatusAccept').on('click', function () {

						$('.ticket-update-modal-submit').addClass('hide');
						$('.ticket-update-modal-submitting').removeClass('hide');

						var ticketUpdateDateAndTime = $('#tuDate').val();
						var ticketUpdateStatusId = $('#tuStatus').val();
						var ticketUpdateComments = $('#tuComments').val();
						var ticketUpdateType = $('#tuType').val();
						var followUpDate = $('#followUpDateFromModal').val();
						var followUpType = $('#followupComment').val();
						var closureDate = $('#updateClosureDate').val();
						var isAjaxRequest = 1;

						var ticketUpdateRelatedTicketId = '';
						if (parseInt(ticketUpdateStatusId) == 15) {
							ticketUpdateRelatedTicketId = $('#relatedTicket').val();
						}

						$.ajax({
							url: '/tickets/' + $('#ticketUpdateModalId').val() + '/status-update',
							type: 'POST',
							dataType: 'json',
							data: {
								tuDate: ticketUpdateDateAndTime,
								tuType: ticketUpdateType,
								tuStatus: ticketUpdateStatusId,
								tuComments: ticketUpdateComments,
								relatedTicket: ticketUpdateRelatedTicketId,
								isAjaxRequest: isAjaxRequest,
								followUpDate: followUpDate,
								followUpType: followUpType,
								closureDate: closureDate
							},

							success: function (data, textStatus, jqXHR) {
								if (data.success) {
									$('#modalStatusUpdate').modal('hide');
									$('.ticket-update-modal-submit').removeClass('hide');
									$('.ticket-update-modal-submitting').addClass('hide');

									window.showNotySuccess("Ticket status updated successfully");

									if (data.ticketData.phone == $("#searchQuery").val()) {
										updateTicketListData(data.ticketData.phone);
									}

									if (typeof table != 'undefined' && table != "") {
										var parentTR = $(".test-remove-tr-" + $('#ticketUpdateModalId').val()).parents('tr');

										var d = table.row(parentTR).data();

										if (typeof d != 'undefined') {
											d["2"] = data.ticketData.status;
											d["3"] = data.ticketData.lastUpdate;

											table.row(parentTR)
												.data(d);

											var statusFilter = $(".sales-datatable-status-filter");
											var filter = statusFilter.val();
											statusFilter.val("").trigger("change");
											statusFilter.val(filter).trigger("change");
										}
									}
								} else {
									$('.ticket-update-modal-submit').removeClass('hide');
									$('.ticket-update-modal-submitting').addClass('hide');

									window.showNotyError(data.error);
								}
							},
							error: function (data, textStatus, jqXHR) {
								$('.ticket-update-modal-submit').removeClass('hide');
								$('.ticket-update-modal-submitting').addClass('hide');

								window.showNotyError(data.error);
							}
						});
					});
				},
				error: function (data, textStatus, jqXHR) {
					window.showNotyError("Some Error occured while getting the data, Please reload the page.");
				}
			});

			$('.sales-dashboard-wrap').on('click', '.btn-ticket-update', function () {
				$('#modalStatusUpdateForm')[0].reset();
				$('.related_ticket_wrap').addClass('hide');
				$('.followup-date_wrap').addClass('hide');
				$('.no-response-date-wrap').addClass('hide');

				var ticketId = $(this).data('ticketId');
				$('#ticketUpdateModalId').val(ticketId);
				$('#modalStatusUpdate').modal('show');
			});
		}

		function loadTicketsData() {
			$data = '<?php echo json_encode(request()->all()); ?>';

			$.ajax({
				url: "/sales/tickets/list/data?dfilter=" + $("#dFilter").val(),
				type: "POST",
				async: true,
				tryCount: 0,
				retryLimit: 1,
				data: JSON.parse($data),
				success: function (data) {
					if (typeof data !== undefined && data) {
						$(".updateDataBtn").removeClass("disabled");
						$(".tickets-loading-wrap").hide();
						$(".tickets-data").append(data);

						if ($("#dFilter").val() == "progress") {
							$(".non-new-tickets").addClass("hide");
							$(".non-new-tickets-NA").removeClass("hide");
						} else {
							$(".non-new-tickets").removeClass("hide");
							$(".non-new-tickets-NA").addClass("hide");
						}

						$(".ticket-status-metric-wrap").find(".font-20").empty();

						$(".showBookedTickets").append($("#countOfBookedTickets").val());
						$(".showEnquiries").append($("#countOfJustEnquiryTickets").val());
						$(".showNoResponse").append($("#countOfNoResponseTickets").val());
						$(".showInitiatives").append($("#countOfInitiatedTickets").val());
						$(".showFollowups").append($("#countOfFollowUpTickets").val());
						$(".showTotalCount").append($("#countOfTotalTickets").val());
						$(".showReturnFollowups").append($("#countOfFollowUpReturnTickets").val());

						$(".showBookedTicketsByHandler").append($("#countOfBookedTicketsByHandler").val());
						$(".showEnquiriesByHandler").append($("#countOfJustEnquiryTicketsByHandler").val());
						$(".showNoResponseByHandler").append($("#countOfNoResponseTicketsByHandler").val());
						$(".showInitiativesByHandler").append($("#countOfInitiatedTicketsByHandler").val());
						$(".showFollowupsByHandler").append($("#countOfFollowUpTicketsByHandler").val());
						$(".showTotalCountByHandler").append($("#countOfTotalTicketsByHandler").val());
						$(".showReturnFollowupsByHandler").append($("#countOfFollowUpReturnTicketsByHandler").val());
						$(".showInProgressByHandler").append($("#countOfInProgressTicketsByHandler").val());

						$("#startTimeOutSideCollapse").val($("#startTime").val());
						$("#endTimeOutSideCollapse").val($("#endTime").val());

						$('#ticketsSummary tfoot th').each(function () {
							var title = $(this).text();
							if (title === "Status") {
								$(this).html('<input class="sales-datatable-status-filter" type="text" style="width: 30px;" placeholder="Search ' + title + '" />');
							}
						});

						table = $('#ticketsSummary').DataTable({
							"order": [[0, "desc"]],
							"fixedHeader": true,
							"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]]
						});

						// Apply the search
						table.columns().every(function () {
							var that = this;

							$('input', this.footer()).on('keyup change', function () {
								if (that.search() !== this.value) {
									that.search(this.value)
										.draw();
								}
							});
						});

						$('.filter-anlytics-creation-start-time, .filter-anlytics-creation-end-time, .filter-followup-end-time, .filter-followup-start-time').datetimepicker({
							timepicker: true,
							scrollInput: false,
							scrollMonth: false,
							closeOnDateSelect: false,
							format: "d-m-Y H:i",
							formatTime: "H:i"
						});
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					this.tryCount++;
					if (this.tryCount <= this.retryLimit) {
						$.ajax(this);
					} else {
						showNotyError("Error occurred while fetching the data, Please reload the page & tryagain.	")
					}
				}
			});
		}

		function updateUrlByFilters($ticketStatus) {
			// filter options
			var filterChosenStartingTime = $('#startTime').val();
			var filterChosenEndingTime = $('#endTime').val();
			var occasion = $('#occasion').val();
			var handler = $('#handler').val();
			var createdByHandler = $('#createdByHandler').val();
			var enquirySource = $('#enquirySource').val();
			var customerSource = $('#customerSource').val();
			var bookingLikeliness = $('#booingLikeliness').val();
			var partyStartTime = $('#partyStartTime').val();
			var partyEndTime = $('#partyEndTime').val();
			var followupStartTime = $('#followupStartTime').val();
			var followupEndTime = $('#followupEndTime').val();
			var followupType = $('#followUpType').val();
			var closureStartTime = $('#closureStartTime').val();
			var closureEndTime = $('#closureEndTime').val();
			var createdAtStartTime = $('#createdAtStartTime').val();
			var createdAtEndTime = $('#createdAtEndTime').val();
			var ticketStatus = $ticketStatus ? $ticketStatus : '';
			var currentUrl = $("#filterUrl").val() + "?dfilter=" + $("#dFilter").val();
			var url = refreshUrlWithUpdatedUrls("query", $("#searchQuery").val(), currentUrl);

			var isUrlChanged = false;
			if (filterChosenStartingTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls("filterChosenStartingTime", filterChosenStartingTime, url);
			}

			if (filterChosenEndingTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('filterChosenEndingTime', filterChosenEndingTime, url);
			}

			if (occasion) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('occasion', occasion, url);
			}

			if (handler) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('handler', handler, url);
			}

			if (createdByHandler) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('createdByHandler', createdByHandler, url);
			}

			if (bookingLikeliness) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('bookingLikeliness', bookingLikeliness, url);
			}

			if (enquirySource) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('enquirySource', enquirySource, url);
			}
			if (customerSource) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('customerSource', customerSource, url);
			}
			if (partyStartTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('partyStartTime', partyStartTime, url);
			}

			if (partyEndTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('partyEndTime', partyEndTime, url);
			}
			if (followupStartTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('followupStartTime', followupStartTime, url);
			}

			if (followupEndTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('followupEndTime', followupEndTime, url);
			}

			if (followupType) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('followUpType', followupType, url);
			}

			if (closureStartTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('closureStartTime', closureStartTime, url);
			}

			if (closureEndTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('closureEndTime', closureEndTime, url);
			}

			if (createdAtStartTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('createdAtStartTime', createdAtStartTime, url);
			}

			if (ticketStatus) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('ticketStatus', ticketStatus, url);
			}

			if (createdAtEndTime) {
				isUrlChanged = true;
				url = refreshUrlWithUpdatedUrls('createdAtEndTime', createdAtEndTime, url);
			}

			if (isUrlChanged) {
				location.href = url;
			}
		}

		function refreshUrlWithUpdatedUrls(inputName, inputValue, currentUrl) {
			var redirectUrl = '';
			if (currentUrl.indexOf(inputName + '=') == -1) {
				redirectUrl = currentUrl + '&' + inputName + '=' + inputValue;
			} else {
				var res = currentUrl.split(inputName + '=')[1].split('&')[0];
				redirectUrl = currentUrl.replace(inputName + '=' + res, inputName + '=' + inputValue);
			}
			return redirectUrl;
		}

		function updateTicketListData(phone, email, ticketId) {
			if (typeof email === "undefined" || email === null) {
				email = "";
			}

			if (typeof ticketId === "undefined" || ticketId === null) {
				ticketId = "";
			}

			$("#searchTicketData").empty().html("<div class='text-center mar-t-10'><a class='alert alert-info'>Fetching ticket info.</a></div>");

			$("#searchQuery").val(phone);
			$.ajax({
				url: "/sales/tickets/search/data?query=" + phone + "&email=" + email + "&ticketId=" + ticketId,
				type: "POST",
				async: true,
				data: {},
				success: function (data) {
					if (typeof data !== undefined && data) {
						$("#searchTicketData").empty().append(data);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					showNotyError("Error occurred while fetching the data, Please reload the page & tryagain.")
				}
			});
		}
	});
</script>
@stop