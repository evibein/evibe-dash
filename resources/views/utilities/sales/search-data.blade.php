@if($data["searchTicket"] && ($data["searchTicket"]->count() > 0))
	@foreach($data["searchTicket"] as $ticket)
		<div>
			<table class="table table-bordered mar-b-5">
				<tr>
					<td colspan="2">
						<div class="pull-left">
							<i class="glyphicon glyphicon-user"></i>
							@if ($ticket->name)
								{{ $ticket->name }}
							@else
								---
							@endif
						</div>
						<div class="pull-right">
							<i class="glyphicon glyphicon-calendar"></i>
							@if($ticket->event_date)
								{{ date("F j Y, g:i a", $ticket->event_date)}}
							@else
								---
							@endif
						</div>
						<div class="clearfix"></div>
						<div class="pull-left mar-t-5">
							<i class="glyphicon glyphicon-map-marker"></i>
							@if ($ticket->city_id)
								{{ $ticket->city->name}}
							@else
								---
							@endif
						</div>
						<div class="pull-right mar-t-5">
							<i class="glyphicon glyphicon-gift"></i>
							@if ($ticket->event_id && isset($data["occasionNames"][$ticket->event_id]))
								{{ $data["occasionNames"][$ticket->event_id] }}
							@else
								---
							@endif
						</div>
					</td>
				</tr>
				<tr>
					<td>Current Status</td>
					<td>
						<div class="st-{{ $ticket->status_id }}">
							<i class="glyphicon {{ AppUtil::getTicketStatusIcon($ticket->status_id) }}"></i>
							@foreach($data['ticketStatus'] as $ticketState)
								@if($ticket->status_id == $ticketState->id)
									{{ $ticketState->name }}
								@endif
							@endforeach
						</div>
					</td>
				</tr>
				<tr>
					@php
						$handlerDetailsOfTicket = isset($data["handlerDetails"][$ticket->handler_id])? $data["handlerDetails"][$ticket->handler_id] : '';
						$createdHandlerDetailsOfTicket = isset($data["handlerDetails"][$ticket->created_handler_id])? $data["handlerDetails"][$ticket->created_handler_id] : '';
					@endphp
					<td>Handler <br>
						<small>(Current/Created)</small>
					</td>
					<td>
						@if($handlerDetailsOfTicket)
							{{ucfirst($handlerDetailsOfTicket)}}
						@else
							---
						@endif
						/
						@if($createdHandlerDetailsOfTicket)
							{{ucfirst($createdHandlerDetailsOfTicket)}}
						@else
							---
						@endif
					</td>
				</tr>
				@if($ticket->expected_closure_date)
					<tr>
						<td>
							Closure Date
						</td>
						<td>
							{{ date("d M y h:i A", $ticket->expected_closure_date) }}
						</td>
					</tr>
				@endif
				@if(isset($data["ticketUpdates"]) && isset($data["ticketUpdates"][$ticket->id]))
					<tr>
						<td>
							Last Updated Comment
						</td>
						<td>
							{{ $data["ticketUpdates"][$ticket->id]["comments"] }}
							<small>[Updated on: {{ date("d M y h:i A", strtotime($data["ticketUpdates"][$ticket->id]["created_at"])) }}]</small>
							<a class="btn btn-link btn-xs ticket-update-info" data-id="{{ $ticket->id }}" data-url={{ route('ticket.update.load', $ticket->id) }}>[See All Updates]</a>
						</td>
					</tr>
				@endif
				@php $currentFollowup = $ticket->activeFollowup; @endphp
				@if($currentFollowup)
					<tr>
						<td>
							FollowUp Date
						</td>
						<td>
							{{ date("d M y h:i A", $currentFollowup->followup_date) }}
						</td>
					</tr>
				@endif
				@if($ticket->status_id == config("evibe.status.booked"))
					<tr>
						<td>
							Partner Info
						</td>
						<td>
							@php $ticketBookings = $ticket->bookings; @endphp
							@foreach($ticketBookings as $ticketBooking)
								@php $provider = $ticketBooking->provider; @endphp
								{{ ucwords($ticketBooking->booking_type_details) }}
								<div class="pad-l-10">
									<b>Name:</b> {{ $provider->person }}<br>
									<b>Phone:</b> {{ $provider->phone }}<br>
									<b>Alt Phone:</b> {{ $provider->alt_phone }}
								</div>
							@endforeach
						</td>
					</tr>
					<tr>
						<td>
							Order Details
						</td>
						<td>
							@foreach($ticketBookings as $ticketBooking)
								{{ ucwords($ticketBooking->booking_type_details) }}
								<div class="pad-l-10">
									{!! $ticketBooking->booking_info !!}
								</div>
							@endforeach
						</td>
					</tr>
				@endif
				<tr>
					<td colspan="2" class="no-pad-t">
						<a href="#" class="btn btn-danger btn-xs mar-t-10 btn-ticket-update" data-ticket-id="{{ $ticket->id }}">New Update</a>
						@if($ticket->status_id == config("evibe.status.booked"))
							<a href="{{ route("ticket.details.bookings", $ticket->id) }}" target="_blank" class="btn btn-primary btn-xs mar-t-10">Resend Receipt</a>
							<a href="{{ route("ticket.details.bookings", $ticket->id) }}" target="_blank" class="btn btn-primary btn-xs mar-t-10">Update Order</a>
							<a href="{{ route("ticket.details.bookings", $ticket->id) }}" target="_blank" class="btn btn-primary btn-xs mar-t-10">Cancel Order</a>
						@elseif($ticket->status_id == config("evibe.status.confirmed"))
							<a href="{{ route("ticket.details.bookings", $ticket->id) }}" target="_blank" class="btn btn-primary btn-xs mar-t-10">Resend Payment Link</a>
						@endif
						<a href="{{ route("ticket.details.mappings", $ticket->id) }}" target="_blank" class="btn btn-primary btn-xs mar-t-10">Mappings</a>
						<a href="{{ route('ticket.reco.selected.services',$ticket->id) }}" target="_blank" class="btn btn-primary btn-xs mar-t-10">Recommendations</a>
						<a href="{{ route("ticket.details.info", $ticket->id) }}" target="_blank" class="btn btn-primary btn-xs mar-t-10">Ticket Info</a>
					</td>
				</tr>
			</table>
		</div>
	@endforeach
@else
	<div>
		<div class="alert alert-danger no-mar-b">
			<span>Sorry, no tickets were found.</span>
		</div>
	</div>
	<div class="text-center">
		<div class="mar-t-10 in-blk">
			<a href="{{route('ticket.create.initialise.details')}}" class="btn btn-primary">Create New Enquiry</a>
		</div>
	</div>
@endif