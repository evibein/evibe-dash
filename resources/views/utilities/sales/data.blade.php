<div class="mar-b-10">
	<a class="btn btn-primary in-blk" role="button" data-toggle="collapse" href="#collapseTags" aria-expanded="false" aria-controls="collapseTags">More Filters</a>
	@if(isset($data['filterReset']) && $data['filterReset'])
		<a class="btn btn-warning in-blk btn-xs mar-b-5 sales-clear-filters-btn">Reset Filters</a>
	@endif
</div>

<!-- Hidden Data -->
<input type="hidden" value="@if(isset($data['countOfBookedTickets'])) {{ $data['countOfBookedTickets'] }} @endif" id='countOfBookedTickets'>
<input type="hidden" value="@if(isset($data["countOfJustEnquiryTickets"])) {{ $data["countOfJustEnquiryTickets"] }} @endif" id='countOfJustEnquiryTickets'>
<input type="hidden" value="@if(isset($data["countOfNoResponseTickets"])) {{ $data["countOfNoResponseTickets"] }} @endif" id='countOfNoResponseTickets'>
<input type="hidden" value="@if(isset($data["countOfInitiatedTickets"])) {{ $data["countOfInitiatedTickets"] }} @endif" id='countOfInitiatedTickets'>
<input type="hidden" value="@if(isset($data["countOfFollowUpTickets"])) {{ $data["countOfFollowUpTickets"] }} @endif" id='countOfFollowUpTickets'>
<input type="hidden" value="@if(isset($data["countOfNewTickets"])) {{ $data["countOfNewTickets"]  }} @endif" id='countOfTotalTickets'>
<input type="hidden" value="@if(isset($data['countOfBookedTicketsByHandler'])) {{ $data['countOfBookedTicketsByHandler'] }} @endif" id='countOfBookedTicketsByHandler'>
<input type="hidden" value="@if(isset($data["countOfJustEnquiryTicketsByHandler"])) {{ $data["countOfJustEnquiryTicketsByHandler"] }} @endif" id='countOfJustEnquiryTicketsByHandler'>
<input type="hidden" value="@if(isset($data["countOfNoResponseTicketsByHandler"])) {{ $data["countOfNoResponseTicketsByHandler"] }} @endif" id='countOfNoResponseTicketsByHandler'>
<input type="hidden" value="@if(isset($data["countOfInitiatedTicketsByHandler"])) {{ $data["countOfInitiatedTicketsByHandler"] }} @endif" id='countOfInitiatedTicketsByHandler'>
<input type="hidden" value="@if(isset($data["countOfFollowUpTicketsByHandler"])) {{ $data["countOfFollowUpTicketsByHandler"] }} @endif" id='countOfFollowUpTicketsByHandler'>
<input type="hidden" value="@if(isset($data["countOfNewTicketsByHandler"])) {{ $data["countOfNewTicketsByHandler"]  }} @endif" id='countOfTotalTicketsByHandler'>
<input type="hidden" value="@if(isset($data["countOfFollowUpReturnTickets"])) {{ $data["countOfFollowUpReturnTickets"] }} @endif" id='countOfFollowUpReturnTickets'>
<input type="hidden" value="@if(isset($data["countOfFollowUpReturnTicketsByHandler"])) {{ $data["countOfFollowUpReturnTicketsByHandler"] }} @endif" id='countOfFollowUpReturnTicketsByHandler'>
<input type="hidden" value="@if(isset($data["countOfInProgressTickets"])) {{ $data["countOfInProgressTickets"] }} @endif" id='countOfInProgressTickets'>
<input type="hidden" value="@if(isset($data["countOfInProgressTicketsByHandler"])) {{ $data["countOfInProgressTicketsByHandler"] }} @endif" id='countOfInProgressTicketsByHandler'>

<div class="collapse mar-b-10" id="collapseTags">
	<div class="filter-group-options ls-none">
		<div class="col-sm-4 no-pad-l">
			<div class="text-center">
				<div class="filter-title text-left">By Date</div>
				<div class="col-sm-5 no-pad">
					<input type="text"
							class="form-control in-blk filter-followup-date filter-followup-start-time"
							id="startTime"
							value="{{date('d-m-Y H:i',$data['startDate'])}}">
				</div>
				<div class="col-sm-2 no-pad text-center">
					<div class="text-center mar-t-10 text-bold filter-title">To</div>
				</div>
				<div class="col-sm-5 no-pad">
					<input type="text"
							class="form-control filter-followup-date filter-followup-end-time"
							value="{{date('d-m-Y H:i',$data['endDate'])}}" id="endTime">
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="text-center">
				<div class="filter-title pull-left">By Booking Likeliness</div>
				<select class="form-control" name="booingLikeliness" id="booingLikeliness">
					<option @if(request()->input('bookingLikeliness') == "all")selected
							@endif value="all">All
					</option>
					@foreach($data['bookingLikeliness'] as $key => $enquirySource)
						<option value="{{$key}}"
								@if(request()->input('bookingLikeliness') == $key)selected @endif>{{$enquirySource}}
						</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-sm-4 no-pad-r">
			<div class="text-center">
				<div class="filter-title pull-left">By Occasion</div>
				<select class="form-control" name="occasion" id="occasion">
					<option value="all">All Occasions</option>
					@foreach($data['typeEvents'] as $key => $event)
						<option value="{{$key}}"
								@if(request()->input('occasion')== $key)selected @endif>{{$event}}
						</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-sm-4 no-pad-l mar-t-10">
			<div class="text-center">
				<div class="filter-title pull-left">By Updated handler</div>
				<select class="form-control" name="handler" id="handler">
					<option value="all">All Handlers</option>
					@foreach($data['handlerDetails'] as $key => $handlerDetail)
						<option value="{{$key}}"
								@if(request()->input('handler')== $key)selected @endif>{{$handlerDetail}}
						</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-sm-4 mar-t-10">
			<div class="text-center">
				<div class="filter-title pull-left">By Enquiry Source</div>
				<select class="form-control" name="enquirySource" id="enquirySource">
					<option value="all" @if(request()->input('enquirySource') == "all")selected
							@endif>All Sources
					</option>
					<option value="scheduled"
							@if(request()->input('enquirySource') == "scheduled")selected @endif>Schedule(Non-Phone)
					</option>
					@foreach($data['typeEnquirySource'] as $key => $enquirySource)
						<option value="{{$key}}"
								@if(request()->input('enquirySource')== $key)selected @endif>{{$enquirySource}}
						</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-sm-4 no-pad-r mar-t-10">
			<div class="text-center">
				<div class="filter-title pull-left">By Customer Source</div>
				<select class="form-control" name="customerSource" id="customerSource">
					<option value="all" @if(request()->input('customerSource') == "all")selected
							@endif>All Sources
					</option>
					@foreach($data['typeTicketSource'] as $key => $customerSource)
						<option value="{{$key}}"
								@if(request()->input('customerSource')== $key)selected @endif>{{$customerSource}}
						</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-sm-4 no-pad-l mar-t-10">
			<div class="text-center">
				<div class="filter-title text-left">By FollowUp Date</div>
				<div class="col-lg-5 no-pad">
					<input type="text"
							class="form-control in-blk filter-followup-date filter-followup-start-time"
							id="followupStartTime"
							value="{{$data['followupStartTime'] ? date('d-m-Y H:i',$data['followupStartTime']): null}}">
				</div>
				<div class="col-sm-2 no-pad text-center">
					<div class="text-center mar-t-10 text-bold filter-title">To</div>
				</div>
				<div class="col-lg-5 no-pad">
					<input type="text"
							class="form-control filter-followup-date filter-followup-end-time"
							id="followupEndTime"
							value="{{$data['followupEndTime'] ? date('d-m-Y H:i',$data['followupEndTime']): null}}">
				</div>
			</div>
		</div>
		<div class="col-sm-4 mar-t-10">
			<div class="text-center">
				<div class="filter-title text-left">By Closure Date</div>
				<div class="col-lg-5 no-pad">
					<input type="text"
							class="form-control in-blk filter-followup-date filter-followup-start-time"
							id="closureStartTime"
							value="{{$data['closureStartTime'] ? date('d-m-Y H:i',$data['closureStartTime']): null}}">
				</div>
				<div class="col-sm-2 no-pad text-center">
					<div class="text-center mar-t-10 text-bold filter-title">To</div>
				</div>
				<div class="col-lg-5 no-pad">
					<input type="text"
							class="form-control filter-followup-date filter-followup-end-time"
							id="closureEndTime"
							value="{{$data['closureEndTime'] ? date('d-m-Y H:i',$data['closureEndTime']): null}}">
				</div>
			</div>
		</div>
		<div class="col-sm-4 mar-t-10">
			<div class="text-center">
				<div class="filter-title text-left">By Created At Date</div>
				<div class="col-lg-5 no-pad">
					<input type="text"
							class="form-control in-blk filter-followup-date filter-followup-start-time"
							id="createdAtStartTime"
							value="{{$data['createdAtStartTime'] ? date('d-m-Y H:i',strtotime($data['createdAtStartTime'])): null}}">
				</div>
				<div class="col-sm-2 no-pad text-center">
					<div class="text-center mar-t-10 text-bold filter-title">To</div>
				</div>
				<div class="col-lg-5 no-pad">
					<input type="text"
							class="form-control filter-followup-date filter-followup-end-time"
							id="createdAtEndTime"
							value="{{$data['createdAtEndTime'] ? date('d-m-Y H:i',strtotime($data['createdAtEndTime'])): null}}">
				</div>
			</div>
		</div>
		<div class="col-sm-4 no-pad-l mar-t-10">
			<div class="text-center">
				<div class="filter-title pull-left">By Followup Type</div>
				<select class="form-control" name="followUpType" id="followUpType">
					<option value="all" @if(request()->input('followUpType') == "all") selected @endif>All Sources
					</option>
					@foreach($data['typeFollowUp'] as $key => $followupType)
						<option value="{{$followupType}}"
								@if(request()->input('followUpType')== $followupType) selected @endif>{{$followupType}}
						</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-sm-4 mar-t-10">
			<div class="text-center">
				<div class="filter-title text-left">By Party Date</div>
				<div class="no-pad col-md-5">
					<input type="text"
							class="form-control in-blk filter-followup-date filter-followup-start-time"
							id="partyStartTime"
							value="{{$data['startPartyTime'] ? date('d-m-Y H:i',$data['startPartyTime']): null}}">
				</div>
				<div class="col-sm-2 no-pad text-center">
					<div class="text-center mar-t-10 text-bold filter-title">To</div>
				</div>
				<div class=" no-pad col-md-5">
					<input type="text"
							class="form-control filter-followup-date filter-followup-end-time"
							id="partyEndTime"
							value="{{$data['endPartyTime'] ? date('d-m-Y H:i',$data['endPartyTime']): null}}">
				</div>
			</div>
		</div>
		<div class="col-sm-1">
			<a class="btn btn-primary mar-t-15 apply-filters-btn">Apply</a>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
@if (count($data['tickets']) == 0)
	<div class="alert alert-danger">
		<span>Sorry, no tickets were found.</span>
	</div>
@else
	@php $isReturnTickets = in_array($data["dfilter"], ["prospect", "followup", "missed", "return"]) @endphp
	<table id="ticketsSummary" class="table table-bordered table-responsive table-hover">
		<thead class="text-info">
		<tr>
			<th>Name (Phone)</th>
			<th>Party Date</th>
			<th>Status</th>
			<th>Handler(Current/ Created)</th>
			@if($isReturnTickets)
				<th>Enquiry Source</th>
			@endif
			<th></th>
		</tr>
		</thead>
		<tbody>
		@foreach($data['tickets'] as $ticket)
			@php
				$ticketUpdateComment = isset($data["ticketUpdate"][$ticket["id"]]) ? $data["ticketUpdate"][$ticket["id"]] : '';
				$typeTicketStatusOfTicket = isset($data["typeTicketStatus"][$ticket["status_id"]])? $data["typeTicketStatus"][$ticket["status_id"]] : '';
				$typeEnquirySourceOfTicket = isset($data["typeEnquirySource"][$ticket["enquiry_source_id"]])? $data["typeEnquirySource"][$ticket["enquiry_source_id"]] : '';
				$handlerDetailsOfTicket = isset($data["handlerDetails"][$ticket["handler_id"]])? $data["handlerDetails"][$ticket["handler_id"]] : '';
				$createdHandlerDetailsOfTicket = isset($data["handlerDetails"][$ticket["created_handler_id"]])? $data["handlerDetails"][$ticket["created_handler_id"]] : '';
			@endphp
			<tr>
				<td>
					<div class="text-white font-1">[{{ date('y/m/d H:i', strtotime($ticket["created_at"])) }}]</div>
					<span class="{{ "test-remove-tr-" . $ticket["id"] }}"></span>
					@if ($ticket["name"])
						{{ $ticket["name"]}}
					@else
						---
					@endif
					<div>
						(@if($ticket["calling_code"])<span class="mar-r-2">{{ $ticket["calling_code"]}}</span>@endif<span>{{ $ticket["phone"]}}</span>)
					</div>
					<div class="text-muted font-10">[{{ date('d M y, g:i a', strtotime($ticket["created_at"]))}}]</div>
					@if (isset($ticket["enquiry_source_id"]) && $ticket["enquiry_source_id"])
						@if ($ticket["enquiry_source_id"] == config('evibe.ticket_enquiry_source.vday_landing_page'))
							<br><span class="text-danger text font-14">[VDAY 2020]</span>
						@elseif($ticket["enquiry_source_id"] == config('evibe.ticket_enquiry_source.inspiration_page'))
							<br><span class="text-danger text font-14">[Inspiration Page]</span>
						@elseif($ticket["enquiry_source_id"] == config('evibe.ticket_enquiry_source.navigation_modal'))
							<br><span class="text-danger text font-14">[Navigation Modal]</span>
						@endif
					@endif
				</td>
				<td>
					<div class="text-white font-1">[{{ date('y/m/d H:i', $ticket["event_date"]) }}]</div>
					@if($ticket["event_date"])
						{{ date('d M y', $ticket["event_date"]) }}<br>
						{{ date('h:i A', $ticket["event_date"]) }}
					@else
						---
					@endif
				</td>
				<td>
					<div class="st-{{ $ticket["status_id"]}}">
						@if($typeTicketStatusOfTicket)
							{!! $typeTicketStatusOfTicket !!}
						@endif
					</div>
				</td>
				<td @if($isReturnTickets) style="max-width: 120px;" @endif>
					@if($handlerDetailsOfTicket)
						{{ucfirst($handlerDetailsOfTicket)}}
					@else
						---
					@endif
					/
					@if($createdHandlerDetailsOfTicket)
						{{ucfirst($createdHandlerDetailsOfTicket)}}
					@else
						---
					@endif
				</td>
				@if($isReturnTickets)
					<td>
						@if($typeEnquirySourceOfTicket)
							<div>{!! $typeEnquirySourceOfTicket !!}</div>
						@else
							---
						@endif
					</td>
				@endif
				<td class="text-center">
					<a class="btn btn-link btn-xs ticket-search-phone" data-id="{{ $ticket["phone"] }}" data-email="{{ $ticket["phone"] }}" data-ticket-id="{{ $ticket["id"] }}"><i class="glyphicon glyphicon-search"></i></a>
					<div class="whatsapp-button btn no-pad in-blk" data-id="{{$ticket["id"]}}">
						<img src="http://gallery.evibe.in/img/icons/whatsapp-logo.png">
					</div>
					<div>
						<a class="btn btn-link btn-xs ticket-update-info mar-t-5" data-id="{{ $ticket["id"] }}" data-url="{{ route('ticket.update.load', $ticket['id']) }}">
							<i class="glyphicon glyphicon-eye-open"></i> See All Updates
						</a>
					</div>
				</td>
			</tr>
		@endforeach
		</tbody>
		<tfoot>
		<tr>
			<th></th>
			<th></th>
			<th>Status</th>
			<th></th>
			@if($isReturnTickets)
				<th></th>
			@endif
			<th></th>
		</tr>
		</tfoot>
	</table>
@endif