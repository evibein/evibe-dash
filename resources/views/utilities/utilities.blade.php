@extends('layout.base')

@section('content')
<div class="container-fluid">
	<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
		<div class="app-tabs">
			@include('app/tabs')
		</div>
		<div class="page-content trends-wrap">
			<div class="trends-panel-cnt">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Utilities</h2>
					</div>
					<div class="panel-body">
						<div class="col-md-3 col-sm-3">
							<!-- NAV TABS OUTER -->
							<ul class="nav nav-pills nav-stacked" role="tablist">
								<li class="hide"><a href="#tabExchangeC" role="tab" data-toggle="tab">Exchange Contacts</a></li>
								<li class="active"><a href="#tabSendE" role="tab" data-toggle="tab">Send Emails</a></li>
							</ul>
						</div>
						<div class="col-md-9 col-sm-9">
							<!-- TAB PANES -->	
							<div class="tab-content well well-sm">
								<div id="tabExchangeC" class="tab-pane hide">
									<div>
										<ul class="nav nav-tabs" role="tablist">
											<li class="active"><a href="#tabEVenues" role="tab" data-toggle="tab">Venues</a></li>
											<li><a href="#tabEOrganisers" role="tab" data-toggle="tab">Organisers</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="tabEVenues" class="tab-pane active pad-t-10">
											<div class="col-md-4 col-sm-4">
												<button id="eAddVenueBtn" class="btn btn-md btn-info">Add Another Venue</button>
												<div class="form-group pad-t-10">
													<div class="selected-venue-list">
														<select name="selectVenue[]" class="form-control">
															@foreach($data['venues'] as $venue)
																<option data-phone="{{ $venue->phone }}" data-name="{{ $venue->person }}" 
																		data-venue="{{ $venue->name }}" value="{{ $venue->id }}">
																	{{ $venue->name }} ({{ $venue->person }})
																</option>
															@endforeach
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-8 col-sm-8">
												<form action="#">
													<div>
														<div class="col-md-6 col-sm-6">
															<div class="form-group">
																<label for="customerName">Customer Name</label>
																<input name="customerName" id="customerName" type="text" class="form-control"
																		placeholder="Enter customer name">
															</div>
														</div>
														<div class="col-md-6 col-sm-6">
															<div class="form-group">
																<label for="customerNumber">Customer Number</label>
																<input name="customerNumber" id="customerNumber" type="text" class="form-control"
																		placeholder="Enter customer number">
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div>
														<div class="col-md-4 col-sm-4">
															<div class="form-group">
																<label>Party Date</label>
																<input id="vPartyDate" type="text" class="form-control" placeholder="Enter Date">
															</div>
														</div>
														<div class="col-md-4 col-sm-4">
															<div class="form-group">
																<label>Guests Count</label>
																<input id="guestsCount" type="text" class="form-control" placeholder="Enter head count">
															</div>
														</div>
														<div class="col-md-4 col-sm-4">
															<div class="form-group">
																<label>Food Type</label>
																<select id="foodType" class="form-control">
																	<option value="Veg">Veg</option>
																	<option value="Non-Veg">Non-Veg</option>
																</select>
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div>
														<div class="col-md-12 col-sm-12">
															<div class="form-group">
																<label for="details">More Details (optional)</label>
																<textarea name="details" id="details" type="text" class="form-control"
																		placeholder="Enter more details (if any)"></textarea>
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div>
														<div class="col-md-12 col-sm-12">
															<button id="getVenuesSMSText" class="btn btn-danger">Get SMS Texts</button>
														</div>
													</div>
												</form>
											</div>
											<div class="clearfix"></div>
										</div>
										<div id="tabEOrganisers" class="tab-pane pad-t-10">
											<div class="col-md-4 col-sm-4">
												<button id="eAddOrganiserBtn" class="btn btn-md btn-info">Add Another Organiser</button>
												<div class="form-group pad-t-10">
													<div class="selected-organiser-list">
														<select name="selectOrganiser[]" class="form-control">
															@foreach($data['organisers'] as $organiser)
																<option data-phone="{{ $organiser->phone }}" data-name="{{ $organiser->person }}" 
																		value="{{ $organiser->id }}">
																	{{ $organiser->name }} ({{ $organiser->person }})
																</option>
															@endforeach
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-8 col-sm-8">
												<form action="#">
													<div>
														<div class="col-md-6 col-sm-6">
															<div class="form-group">
																<label for="oCustomerName">Customer Name</label>
																<input name="oCustomerName" id="oCustomerName" type="text" class="form-control"
																		placeholder="Enter customer name">
															</div>
														</div>
														<div class="col-md-6 col-sm-6">
															<div class="form-group">
																<label for="oCustomerNumber">Customer Number</label>
																<input name="oCustomerNumber" id="oCustomerNumber" type="text" class="form-control"
																		placeholder="Enter customer number">
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div>
														<div class="col-md-4 col-sm-4">
															<div class="form-group">
																<label>Party Date</label>
																<input id="oPartyDate" type="text" class="form-control" placeholder="Enter Date">
															</div>
														</div>
														<div class="col-md-4 col-sm-4">
															<div class="form-group">
																<label>Guests Count</label>
																<input id="oGuestsCount" type="text" class="form-control" placeholder="Enter head count">
															</div>
														</div>
														<div class="col-md-4 col-sm-4">
															<div class="form-group">
																<label>Location</label>
																<input id="oLocation" type="text" class="form-control" placeholder="Enter location">
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div>
														<div class="col-md-12 col-sm-12">
															<div class="form-group">
																<label for="details">Services Requested</label>
																<textarea name="oDetails" id="oDetails" type="text" class="form-control"
																		placeholder="Enter services requested details"></textarea>
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div>
														<div class="col-md-12 col-sm-12">
															<button id="getOrganisersSMSText" class="btn btn-danger">Get SMS Texts</button>
														</div>
													</div>
												</form>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								<div id="tabSendE" class="tab-pane active">
									<div class="pad-t-10">
										<button id="excEmailsBtn" class="btn btn-warning" data-toggle="modal" data-target="#selectEmailModal">Send Emails</button>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="modals-wrap">
	<!-- SELECT EMAIL TYPE MODAL -->
	<div class="modal fade" id="selectEmailModal" tabindex="-1" role="dialog" aria-labelledby="selectEmailModalLable" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="selectEmailModalLable">Send Email</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Customer Name:</label>
						<input type="text" id="sCustomerName" name="sCustomerName"
								class="form-control" placeholder="Enter customer name">
					</div>
					<div class="form-group">
						<label>Customer Email Address:</label>
						<input type="text" id="sCustomerEmail" name="sCustomerEmail"
								class="form-control" placeholder="Enter customer email">
					</div>
					<div class="form-group">
						<label>Select Email Type</label>
						<select name="sEmailType" id="sEmailType" class="form-control">
							<option value="1">No Response Email</option>
							<option value="2">Wrong Phone Number Email</option>
							<option value="3">No Relevant Vendors Email</option>
						</select>
					</div>
					<div class="form-group">
						<button id="sSendEmailBtn" class="btn btn-primary">Send Email</button>
						<button class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- SENDING MESSAGE MODAL -->
	<div class="modal fade" id="sendingModal" tabindex="-1" role="dialog" aria-labelledby="senidngModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="senidngModalLabel">Sending Email</h4>
				</div>
				<div class="modal-body">
					<div>Sending email...</div>
				</div>
			</div>
		</div>
	</div>
	<!-- SMS EXCHANGE MODAL -->
	<div class="modal fade" id="exchangeModal" tabindex="-1" role="dialog" aria-labelledby="exchangeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="exchangeModalLabel">Exchange Contacts SMS Text</h4>
				</div>
				<div class="modal-body">
					<div class="well pad-t-10">
						<p>Send this to <b>Customer (<span id="cNumberHolder"></span>)</b></p>
						<div id="cSmsTextHolder"></div>
					</div>
					<div class="well pad-t-10">
						<div id="vSmsesTextHolder"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
	
	$(document).ready(function() {

		$('#vPartyDate, #oPartyDate').datepicker({
			'changeMonth': true,
			'changeYear': true,
			'dateFormat': 'dd M yy'
		});

		$('#eAddVenueBtn').click(function(e) {

			var options = $('.selected-venue-list').find('select:first').html();
			var sel = '<div class="pad-t-10 pos-rel">' +
					'<div class="pull-left">' +
						'<select name="selectVenue[]" class="form-control">' + options + '</select>' +
					'</div>' +
					'<div class="pull-right"> ' +
						'<button class="option-clear btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove-sign"></i></button>' +
					'</div>' +
					'<div class="clearfix"></div>' +
				'</div>';

			$('.selected-venue-list').append(sel);
		});

		$('#tabExchangeC').on('click', '.option-clear', function(e) {
			var $th = $(this).parent().parent().remove();
		});

		$('#eAddOrganiserBtn').click(function(e) {

			var options = $('.selected-organiser-list').find('select:first').html();
			var sel = '<div class="pad-t-10 pos-rel">' +
					'<div class="pull-left">' +
						'<select name="selectOrganiser[]" class="form-control">' + options + '</select>' +
					'</div>' +
					'<div class="pull-right"> ' +
						'<button class="option-clear btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove-sign"></i></button>' +
					'</div>' +
					'<div class="clearfix"></div>' +
				'</div>';
				
			$('.selected-organiser-list').append(sel);
		});

		// EXCHANGE VENUE CONTACTS
		$('#getVenuesSMSText').click(function(e) {

			e.preventDefault();

			var cName = $('#customerName').val();
			var cNumber = $('#customerNumber').val();
			var cPartyDate = $('#vPartyDate').val();
			var cGuestsCount = $('#guestsCount').val();
			var cFoodType = $('#foodType').val();
			var cDetails = $('#details').val();
			var vSMSMarkup = '';
			var cSMSMarkup = '';
			var cVendorsListMarkup = '';

			// VENDOR SMS
			$('select[name="selectVenue[]"]').each(function(i, j) {

				var $option = $(j).find('option:selected');
				var vName = $option.data('name');
				var vNumber = $option.data('phone');
				var vHotel = $option.data('venue');
				
				vSMSMarkup += '<div class="pad-t-10">' +
					'<p>Send this to <b>' + vName + ' (' + vNumber + ')</b></p>' +
					'<p>Dear ' + vName + ', You have a new lead from Evibe. Details: ' +
						 cName + ', ' + cNumber + ', ' + cPartyDate +
						', ' + cGuestsCount + ' guests, ' + cFoodType + ', ' + cDetails +
						'. Please talk to the customer and let us know the update. Regards, Team Evibe' +
					'</p>' +
				'</div>';

				cVendorsListMarkup += ' (' + (i + 1) + ') ' + 'Venue: ' + vHotel + ', ' + vName + ', ' + vNumber;

			});

			$('#vSmsesTextHolder').html(vSMSMarkup);

			// CUSTOMER SMS
			cSMSMarkup += '<div class="pad-t-10">' +
					'<span>Dear ' + cName + ', Here are the details. </span>' +
					cVendorsListMarkup + 
					'. We have also sent your details to the managers. Regards, Team Evibe' +
				'</div>';

			$('#cNumberHolder').text(cNumber);
			$('#cSmsTextHolder').html(cSMSMarkup);
			
			// SHOW MODAL
			$('#exchangeModal').modal();
		});

		// EXCHANGE ORGANISERS CONTACTS
		$('#getOrganisersSMSText').click(function(e) {

			e.preventDefault();
			
			var cName = $('#oCustomerName').val();
			var cNumber = $('#oCustomerNumber').val();
			var cPartyDate = $('#oPartyDate').val();
			var cGuestsCount = $('#oGuestsCount').val();
			var cLocation = $('#oLocation').val();
			var cDetails = $('#oDetails').val();
			var vSMSMarkup = '';
			var cSMSMarkup = '';
			var cVendorsListMarkup = '';

			// VENDOR SMS
			$('select[name="selectOrganiser[]"]').each(function(i, j) {

				var $option = $(j).find('option:selected');
				var vName = $option.data('name');
				var vNumber = $option.data('phone');
				
				vSMSMarkup += '<div class="pad-t-10">' +
					'<p>Send this to <b>' + vName + ' (' + vNumber + ')</b></p>' +
					'<p>Dear ' + vName + ', You have a new lead from Evibe. Details: ' +
						 cName + ', ' + cNumber + ', ' + cPartyDate +
						', ' + cGuestsCount + ' guests, ' + cLocation + ', ' + cDetails +
						'. Please talk to the customer and let us know the update. Regards, Team Evibe' +
					'</p>' +
				'</div>';

				cVendorsListMarkup += ' (' + (i + 1) + ') ' + vName + ', ' + vNumber;

			});

			$('#vSmsesTextHolder').html(vSMSMarkup);

			// CUSTOMER SMS
			cSMSMarkup += '<div class="pad-t-10">' +
					'<span>Dear ' + cName + ', Here are the details. </span>' +
					cVendorsListMarkup + 
					'. We have also sent your details to the organisers. Regards, Team Evibe' +
				'</div>';

			$('#cNumberHolder').text(cNumber);
			$('#cSmsTextHolder').html(cSMSMarkup);
			
			// SHOW MODAL
			$('#exchangeModal').modal();
		});

		$('#sSendEmailBtn').click(function(e) {

			if (confirm('Are you sure?', 'Yes', 'No'))
			{
				$(this).text('Sending...');

				$.ajax({
					url: '/utilities/send-type-email',
					type: 'POST',
					dataType: 'JSON',
					data: {
						'name': $('#sCustomerName').val(),
						'email': $('#sCustomerEmail').val(),
						'type': $('#sEmailType').val()
					},
				})
				.done(function(data) {
					if (data.success) {
						$('#selectEmailModal').modal('hide');
						alert('SUCCESS! Email has been sent');
						location.reload();
					}
					else {
						alert('FAILURE! Email NOT sent');
					}
					$(this).text('Send Email');
				})
				.fail(function() {
					$('#selectEmailModal').modal('hide');
					alert('FAILURE! Email NOT sent');
					$(this).text('Send Email');
				});
			}

		});
	});
</script>
@stop
