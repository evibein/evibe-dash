@if (count($data['tickets']) == 0)
	<div class="alert alert-danger">
		<span>Sorry, no tickets were found.</span>
	</div>
@else
	<table id="ticketsSummary" class="table table-bordered table-responsive table-hover">
		<thead class="text-info">
		<tr>
			<th>Name (Phone)</th>
			<th>Status</th>
			<th>Comments</th>
			<th>Updated On</th>
			<th>Created On</th>
		</tr>
		</thead>
		<tbody>
		@foreach($data['tickets'] as $ticket)
			@php
				$ticketUpdate = $data["ticketUpdates"]->where("ticket_id", $ticket->id)->first();
			@endphp
			<tr>
				<td>
					@if ($ticket->name)
						{{ $ticket->name }}
					@else
						---
					@endif
					<div>
						(@if($ticket->calling_code)<span class="mar-r-2">{{ $ticket->calling_code }}</span>@endif<span>{{ $ticket->phone }}</span>)
					</div>
				</td>
				<td>
					<div class="st-{{ $ticket->status_id }}">
						@foreach($data['ticketStatus'] as $ticketState)
							@if($ticket->status_id == $ticketState->id)
								{{ $ticketState->name }}
							@endif
						@endforeach
					</div>
				</td>
				<td>
					@if($ticketUpdate)
						{!! $ticketUpdate->comments !!}
					@endif
				</td>
				<td>
					@if($ticketUpdate)
						{{ date("d M Y, g:i a", strtotime($ticketUpdate->created_at))}}
					@endif
				</td>
				<td>
					{{ date("d M Y, g:i a", strtotime($ticket->created_at))}}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
@endif