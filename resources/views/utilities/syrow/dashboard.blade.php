@extends('layout.base')

@section('custom-head')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.css"/>
@endsection

@section('content')
	<div class="mar-r-10 mar-l-10 page-content tickets-warp">
		<div class="col-xs-7">
			<div class="panel panel-default">
				<div class="panel-body">
					@if(request("dfilter") != "")
						<h3 class="text-center mar-t-10">List of tickets</h3>
					@else
						<h3 class="text-center mar-t-10">List of all tickets from past 3 days</h3>
					@endif
					<hr class="hr-text no-pad no-mar-t no-mar-b">
					<div class="text-center mar-b-15">
						<a class="btn @if(request("dfilter") == "") btn-primary @else btn-default @endif pad-5 in-blk day-filter" data-value="">All</a>
						<a class="btn @if(request("dfilter") == "today") btn-primary @else btn-default @endif pad-5 in-blk mar-l-10 day-filter" data-value="today">Today</a>
						<a class="btn @if(request("dfilter") == "yesterday") btn-primary @else btn-default @endif pad-5 mar-r-10 mar-l-10 in-blk day-filter" data-value="yesterday">Yesterday</a>
						<a class="btn @if(request("dfilter") == "dayBefore") btn-primary @else btn-default @endif pad-5 in-blk day-filter" data-value="dayBefore">Day Before Yesterday</a>
					</div>
					<div class="tickets-list-wrap">
						<div style="min-height: 80px">
							<div class="loader mar-t-30 mar-l-20">
								<div class="line"></div>
								<div class="line"></div>
								<div class="line"></div>
								<div class="line"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-5">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3 class="text-center mar-t-10">Search Tickets</h3>
					<hr class="hr-text no-pad no-mar-t">
					<div class="text-center">
						<div class="col-sm-12">
							<div class="mar-r-10 text-center">
								<input id="searchQuery" type="text" name="query" class="form-control" placeholder="Search tickets by phone" value="{{ request('query') }}"/>
							</div>
							<a class="btn btn-info mar-t-10" id="syrowNumberSearch">Search</a>
							@if (request()->has('query') && request('query'))
								<a href="{{ route("syrow.tickets.list") }}" id="resetSearch" class="pad-l-3 btn btn-link mar-t-10">Clear search</a>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
					@if (request()->has('query') && request('query'))
						@if (session()->has('error'))
							<div class="alert alert-danger alert-dismissible mar-t-10">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<i class="glyphicon glyphicon-remove-sign"></i> {{ session('error') }}
							</div>
						@endif
						@if (session()->has('successMsg'))
							<div class="alert alert-success alert-dismissible mar-t-10">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<i class="glyphicon glyphicon-remove-sign"></i> {{ session('successMsg') }}
							</div>
						@endif
						@if($data["searchTicket"] && ($data["searchTicket"]->count() > 0))
							@foreach($data["searchTicket"] as $ticket)
								<div class="panel-body mar-t-10" style="border: 1px solid #dbdbdb;">
									<table class="table table-bordered table-hover">
										<tr>
											<td>Name</td>
											<td>
												@if ($ticket->name)
													{{ $ticket->name }}
												@else
													---
												@endif
											</td>
										</tr>
										<tr>
											<td>Party Date</td>
											<td>
												@if($ticket->event_date)
													{{ date("F j Y, g:i a", $ticket->event_date)}}
												@else
													---
												@endif
											</td>
										</tr>
										<tr>
											<td>Current Status</td>
											<td>
												<div class="st-{{ $ticket->status_id }}">
													<i class="glyphicon {{ AppUtil::getTicketStatusIcon($ticket->status_id) }}"></i>
													@foreach($data['ticketStatus'] as $ticketState)
														@if($ticket->status_id == $ticketState->id)
															{{ $ticketState->name }}
														@endif
													@endforeach
												</div>
											</td>
										</tr>
										<tr>
											<td>City</td>
											<td>
												@if ($ticket->city_id)
													{{ $ticket->city->name}}
												@else
													---
												@endif
											</td>
										</tr>
										@if(isset($data["ticketUpdates"]) && isset($data["ticketUpdates"][$ticket->id]))
											<tr>
												<td>
													Last Updated Comment
												</td>
												<td>
													{{ $data["ticketUpdates"][$ticket->id]["comments"] }}
													<small>[Updated on: {{ date("d M y H:i", strtotime($data["ticketUpdates"][$ticket->id]["created_at"])) }}]</small>
												</td>
											</tr>
										@endif
										@if($ticket->status_id == config("evibe.status.booked"))
											<tr>
												<td>
													Partner Info
												</td>
												<td>
													@php $ticketBookings = $ticket->bookings; @endphp
													@foreach($ticketBookings as $ticketBooking)
														@php $provider = $ticketBooking->provider; @endphp
														{{ ucwords($ticketBooking->booking_type_details) }}
														<div class="pad-l-10">
															<b>Name:</b> {{ $provider->person }}<br>
															<b>Phone:</b> {{ $provider->phone }}<br>
															<b>Alt Phone:</b> {{ $provider->alt_phone }}
														</div>
													@endforeach
												</td>
											</tr>
											<tr>
												<td>
													Order Details
												</td>
												<td>
													@foreach($ticketBookings as $ticketBooking)
														{{ ucwords($ticketBooking->booking_type_details) }}
														<div class="pad-l-10">
															{!! $ticketBooking->booking_info !!}
														</div>
													@endforeach
												</td>
											</tr>
										@endif
									</table>
									@if($ticket->status_id == config("evibe.status.booked"))
										<div class="col-md-6 col-md-offset-3 text-center">
											<div class="mar-t-10">
												<a data-ticket-id="{{ $ticket->id }}" style="border-radius: 5px;" class="btn btn-primary full-width text-bold need-more-service">Add Other Services</a>
											</div>
											<div class="mar-t-10">
												<a data-ticket-id="{{ $ticket->id }}" style="border-radius: 5px;" class="btn btn-primary full-width text-bold update-party-venue">Update Order</a>
											</div>
											<div class="mar-t-10 mar-b-15">
												<a data-ticket-id="{{ $ticket->id }}" style="border-radius: 5px;" class="btn btn-primary full-width text-bold request-cancellation">Request Cancellation</a>
											</div>
										</div>
										<div class="clearfix"></div>
									@endif
									@if($ticket->status_id == config("evibe.status.initiated"))
										<div class="mar-t-10 text-center">
											<a href="{{ route("ticket.modify.edit.party.details", [$ticket->id, 1]) }}" class="btn btn-primary text-bold">Update Ticket Information</a>
											<a class="btn btn-primary not-valid-enquiry mar-l-10" data-ticket-id="{{ $ticket->id }}">Not Valid Enquiry</a>
										</div>
									@endif
									<form action="{{ route("syrow.ticket.save.comments", $ticket->id) }}" role="form" method="post" class="form form-horizontal">
										<div class="pad-l-15 pad-r-15">
											<div class="form-group no-mar-b">
												<div><label for="tuComments">Comments</label></div>
												<textarea id="tuComments" name="tuComments" cols="20" class="form-control"
														placeholder="Type your comments">{{ old('tuComments') }}</textarea>
											</div>
											<div class="text-center mar-t-10">
												<input type="submit" class="btn btn-primary" value="Add Comments">
											</div>
										</div>
									</form>
								</div>
							@endforeach
						@else
							<div class="pad-t-30">
								<div class="alert alert-danger no-mar-b">
									<span>Sorry, no tickets were found.</span>
								</div>
							</div>
							<div class="text-center">
								<div class="mar-t-10 in-blk">
									<a class="btn btn-primary not-valid-enquiry">Not Valid Enquiry</a>
								</div>
								<div class="mar-t-10 in-blk">
									<a href="{{route('ticket.create.initialise.details')}}" class="btn btn-primary">Create New Enquiry</a>
								</div>
							</div>
							<form action="{{ route("syrow.phone.save.comments", request('query')) }}" role="form" method="post" class="form form-horizontal">
								<div class="pad-l-15 pad-r-15">
									<div class="form-group no-mar-b">
										<label for="ticketNotFoundComments">Comments</label>
										<textarea id="ticketNotFoundComments" name="ticketNotFoundComments" cols="20" class="form-control" placeholder="Type your comments">{{ old('ticketNotFoundComments') }}</textarea>
									</div>
									<div class="text-center mar-t-10">
										<input type="submit" class="btn btn-primary" value="Add Comments">
									</div>
								</div>
							</form>
						@endif
					@endif
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<input type="hidden" id="customerPhone" value="{{ request('query') }}">
	<input type="hidden" id="dFilter" value="{{ request('dfilter') }}">
	<div id="modalNotValidEnquiry" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Not Valid Enquiry</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger mar-b-15 pad-10 hide"></div>
					<div class="pad-l-20 pad-r-20">
						<div class="col-md-12 no-pad-l">
							<div class="form-group">
								<div class="col-md-3 text-bold">
									Comments:
								</div>
								<div class="col-md-9">
									<textarea class="not-valid-comments form-control" rows="3"></textarea>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-not-valid-submit"><i
								class="glyphicon glyphicon-floppy-save"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
	<div id="modalNeedMoreServices" class="modal fade" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<h3 class="text-center">Need Other Services</h3>
				<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="tmoNeedMorePartyServicesComments" class="custom-mdl-label">Please Share Additional Requirements</label>
								<textarea id="tmoNeedMorePartyServicesComments" name="tmoNeedMorePartyServicesComments" class="form-control"></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center">
						<div class="btn btn-link font-16 mar-t-5 tmo-submit-option-btn" data-dismiss="modal">
							<span>Cancel</span>
						</div>
						<div class="btn btn-primary font-16 mar-t-5 tmo-need-more-services-submit">
							<span>Request Other Services</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalRequestCancellation" class="modal fade" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<h3 class="text-center">Are you sure to cancel this order?</h3>
				<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="tmoRequestCancellationComments" class="custom-mdl-label">Comments
									<small> (optional)</small>
								</label>
								<textarea id="tmoRequestCancellationComments" name="tmoRequestCancellationComments" class="form-control"></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center">
						<div class="btn btn-link font-16 mar-t-5 tmo-submit-option-btn" data-dismiss="modal">
							<span>Cancel</span>
						</div>
						<div class="btn btn-primary font-16 mar-t-5 tmo-request-cancellation-submit">
							<span>Yes</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalUpdatePartyVenue" class="modal fade" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<h3 class="text-center">Update Order</h3>
				<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="tmoUpdatePartyVenueComments" class="custom-mdl-label">Please enter requirements</label>
								<textarea id="tmoUpdatePartyVenueComments" name="tmoUpdatePartyVenueComments" class="form-control"></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center">
						<div class="btn btn-link font-16 mar-t-5 tmo-submit-option-btn" data-dismiss="modal">
							<span>Cancel</span>
						</div>
						<div class="btn btn-primary font-16 mar-t-5 tmo-update-party-venue-submit">
							<span>Request Update</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			(function initFunctions() {
				$("body").on("submit", "form", function () {
					$(this).submit(function () {
						return false;
					});
					return true;
				});

				$("#syrowNumberSearch").on("click", function () {
					refreshUrlWithExistingParam("query", $("#searchQuery").val());
				});
			})();

			(function loadTicketsData() {
				$(window).load(function () {
					$.ajax({
						url: "/s/tickets/list/data?dfilter=" + $("#dFilter").val(),
						type: "POST",
						async: true,
						tryCount: 0,
						retryLimit: 3,
						data: {},
						success: function (data) {
							if (typeof data !== undefined && data) {
								$(".tickets-list-wrap").empty().append(data);

								$('#ticketsSummary').DataTable({
									"order": [[0, "desc"]],
									"fixedHeader": true,
									"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]]
								});

								$(".day-filter").on("click", function () {
									refreshUrlWithExistingParam("dfilter", $(this).data("value"));
								});
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							this.tryCount++;
							if (this.tryCount <= this.retryLimit) {
								$.ajax(this);
							} else {
								showNotyError("Error occurred while fetching the data, Please reload the page & tryagain.	")
							}
						}
					});
				});
			})();

			var $ticketId = "";

			$(".not-valid-enquiry").on("click", function () {
				closeAllModals();

				$("#modalNotValidEnquiry").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$(".need-more-service").on("click", function () {
				closeAllModals();

				$("#modalNeedMoreServices").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$(".request-cancellation").on("click", function () {
				closeAllModals();

				$("#modalRequestCancellation").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$(".update-party-venue").on("click", function () {
				closeAllModals();

				$("#modalUpdatePartyVenue").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$(".tmo-need-more-services-submit").on("click", function () {
				var comments = $("#tmoNeedMorePartyServicesComments").val();
				if (comments == "") {
					showNotyError("Please enter your requirements.")
				} else {
					$requestType = "Add Other Services";
					$requestReason = comments;

					$("#modalNeedMoreServices").modal("hide");
					AjaxRequestTMO($requestType, $requestReason);
				}
			});

			$(".tmo-request-cancellation-submit").on("click", function () {
				var comments = $("#tmoRequestCancellationComments").val();
				$requestType = "Request Cancellation";
				$requestReason = comments;

				$("#modalRequestCancellation").modal("hide");
				AjaxRequestTMO($requestType, $requestReason);
			});

			$(".tmo-update-party-venue-submit").on("click", function () {
				var comments = $("#tmoUpdatePartyVenueComments").val();
				if (comments == "") {
					showNotyError("Please enter your requirements.")
				} else {
					$requestType = "Update My Order";
					$requestReason = comments;

					$("#modalUpdatePartyVenue").modal("hide");
					AjaxRequestTMO($requestType, $requestReason);
				}
			});

			$(".btn-not-valid-submit").on("click", function () {
				$("#modalNotValidEnquiry").modal("hide");
				showLoading();

				$.ajax({
					url: "/s/ticket/save/invalid",
					type: 'POST',
					data: {comments: $(".not-valid-comments").val(), phone: $("#customerPhone").val(), ticketId: $ticketId},
					success: function (data) {
						if (data.success) {
							showNotySuccess("Submitted Successfully.");

							setTimeout(function () {
								location.reload();
							}, 1500);
						} else {
							hideLoading();
							$(".modal-backdrop").remove();
							$("#modalNotValidEnquiry").modal("show");
							window.showNotyError(data.error);
						}
					},
					fail: function () {
						hideLoading();
						$(".modal-backdrop").remove();
						$("#modalNotValidEnquiry").modal("show");
						window.showNotyError("Error occurred while submitting your request, Please try again.");
					}

				})
			});

			function AjaxRequestTMO($requestType, $requestReason) {
				showLoading();
				$.ajax({
					url: "/s/ticket/request/save/" + $ticketId,
					type: "POST",
					data: {"requestType": $requestType, "requestReason": $requestReason},
					success: function (data) {
						if (data.success) {
							showNotySuccess("Submitted Successfully.");

							setTimeout(function () {
								location.reload();
							}, 1500);
						} else if (data.error) {
							hideLoading();
							showNotyError(data.error);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						hideLoading();
						window.showNotyError("An error occurred while submitting your request. Please try again later.");
						window.notifyTeam({
							"url": "track/request/save",
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}

			function closeAllModals() {
				$("#modalUpdatePartyDate").modal("hide");
				$("#modalNeedMoreServices").modal("hide");
				$("#modalRequestCancellation").modal("hide");
				$("#modalUpdatePartyVenue").modal("hide");
			}
		});
	</script>
@stop