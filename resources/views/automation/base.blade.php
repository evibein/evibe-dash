@extends('layout.base')

@section('content')

	<div class="col-md-12">
		<div class="col-md-4 col-md-offset-4 text-center">
			<div class="col-md-6">
				<a href="{{ route('automation.reminders') }}" class="btn btn-primary">Auto Followup Reminders</a>
			</div>
			<div class="col-md-6">
				<a href="{{ route('automation.test-followups') }}" class="btn btn-primary">Auto Followup Testing</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<hr>
		@yield('automation-data')
	</div>
	<div class="clearfix"></div>

@endsection