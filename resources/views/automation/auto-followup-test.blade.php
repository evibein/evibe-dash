@extends('automation.base')

@section('automation-data')

	<div class="heading">
		<div class="col-md-3 no-pad">
			Ticket Auto Followup Cases
		</div>
		<div class="col-md-9 no-pad">
			<form id="followupCases" method="GET" action="{{ route('automation.test-followups') }}">
				<input type="text" name="followupTime" id="followupTime" class="followup-date-time"
						placeholder="followup time"
						value="@if(isset($data['followupTime'])) {{ $data['followupTime'] }} @endif">
				<input type="text" name="startTime" id="startTime" class="followup-date-time"
						placeholder="T* = recommended time"
						value="@if(isset($data['startTime'])) {{ $data['startTime'] }} @endif"
						required>
				<input type="text" name="endTime" id="endTime" class="followup-date-time"
						placeholder="P* = party time" value="@if(isset($data['endTime'])) {{ $data['endTime'] }} @endif"
						required>
				<input type="text" name="switchTime" id="switchTime" class="switch-date-time"
						placeholder="switch time"
						value="@if(isset($data['switchTime'])) {{ $data['switchTime'] }} @endif">
				<button type="submit" class="btn-primary">Simulate</button>
			</form>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="text-danger text-center">
			{{ $errors->inputError->first() }}
		</div>
		@php $i = 1; @endphp
		@if(isset($data['reminderData']))
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th class="text-center">S.No</th>
					<th class="text-center">Reminder Sent At</th>
					<th width="10%" class="text-center">Reco Type</th>
					<th class="text-center">SMS Template</th>
				</tr>
				</thead>
				<tbody>
				@foreach($data['reminderData'] as $reminder)
					<tr>
						<td>{{ $i }}</td>
						<td>
							@if(isset($reminder['nextReminderAt']) && $reminder['nextReminderAt'])
								{{ date('Y-m-d H:i A', $reminder['nextReminderAt']) }}
							@endif
						</td>
						<td>@if(isset($reminder['typeReminderGroupId']) && $reminder['typeReminderGroupId'])
								[RR{{ $reminder['typeReminderGroupId'] }}]  :
								@if(isset($reminder['count']) && $reminder['count'])
									@if(($data['diff']) <= 7)
										<b>T+{{ $data['diff'] - $reminder['count'] + 1 }}</b>,
										@if(($reminder['count']) > 2)
											P>2
										@else
											P-{{ $reminder['count'] }}
										@endif
									@elseif($data['diff'] <= 14)
										@if(($reminder['count'] > 7))
											<b>T+{{ $data['diff'] - $reminder['count'] + 1 }}</b>, P>2
										@else
											<b>P-{{ $reminder['count'] }}</b>
										@endif
									@else
										@if(($data['diff'] - $reminder['count'] + 1 <= 7))
											<b>T+{{ $data['diff'] - $reminder['count'] + 1 }}</b>, P>2
										@else
											<b>P-{{ $reminder['count'] }}</b>
										@endif
									@endif
								@endif
							@endif
						</td>
						<td>
							@if(isset($reminder['typeReminderGroupId']) && $reminder['typeReminderGroupId'])
								{{ config('evibe.test.reminders.followup.rr'.$reminder['typeReminderGroupId']) }}
							@endif
						</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		@endif
	</div>

@endsection

@section('javascript')
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			$('.followup-date-time').datetimepicker({ // party date
				step: 30,
				minDate: false,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: false
			});

			$('.switch-date-time').datetimepicker({ // party date
				step: 30,
				minDate: false,
				maxDate: false,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: false
			});

		});
	</script>
@endsection