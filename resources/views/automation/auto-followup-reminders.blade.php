@extends('automation.base')

@section('automation-data')

	<div class="heading">
		<div class="col-md-4">
			Ticket Auto Followup Reminders
		</div>
		<div class="col-md-8">
			<form id="autoFollowupReminders" method="GET" action="{{ route('automation.reminders') }}">
				<select id="ticketId" name="ticketId" class="form-control" required="required">
					<option value="0"> -- Select ticket --</option>
					@foreach($tickets as $ticket)
						<option value="{{$ticket->id}}">
							{{ $ticket->id }} - [{{$ticket->name}}] - [{{$ticket->status_name}}] - [{{date('d M Y H:i A', $ticket->reco_sent_time)}}]
						</option>
					@endforeach
				</select>
				<input type="hidden" id="oldTicketId" value="@if(isset($ticketId) && $ticketId) {{ $ticketId }} @endif">
			</form>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="col-lg-12 col-md-12 pad-t-10 pad-b-20">
		@if(isset($customerDetails) && $customerDetails)
			<div class="col-md-10 col-md-offset-1">
				<div class="col-lg-6 col-md-6">
					<table class="table table-hover table-bordered">
						<tr>
							<th>Customer Name</th>
							<td>{{$customerDetails['customerName']}}</td>
						</tr>
						<tr>
							<th>Email</th>
							<td>{{$customerDetails['customerEmail']}}</td>
						</tr>
						<tr>
							<th>Phone</th>
							<td>{{$customerDetails['customerMobileNo']}}</td>
						</tr>
					</table>
				</div>
				<div class="col-lg-6 col-md-6">
					<table class="table table-hover table-bordered">
						<tr>
							<th>Status</th>
							<td>{{$customerDetails['statusOfParty']}}</td>
						</tr>
						<tr>
							<th>Recommendations sent time (T)</th>
							<td>{{$customerDetails['followupStartTime']}}</td>
						</tr>
						<tr>
							<th>Party date time (P)</th>
							<td>{{$customerDetails['followupEndTime']}}</td>
						</tr>
					</table>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		@endif

		@if(isset($reminders) && $reminders)
			<div class="pad-t-10">
				<table class="table table-hover table-bordered">
					<thead>
					<tr>
						<th>S.No</th>
						<th width="10%">Reminder Date</th>
						<th width="10%">Reminder Type</th>
						<th class="text-center">SMS Description</th>
						<th width="8%">SMS Sent ?</th>
					</tr>
					</thead>
					<?php $a = 1; ?>
					<tbody>
					@foreach($reminders as $reminder)
						<tr>
							<td>{{$a++}}</td>
							<td>{{$reminder['reminderDate']}}</td>
							<td>RR{{$reminder['typeReminderGroupId']}}</td>
							<td class="text-left">{{$reminder['smsTemplate']}}</td>
							<td>{{$reminder['isSMSSent']}}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		@elseif(isset($ticketId) && $ticketId)
			<div class="alert alert-danger text-center">There are no auto followup reminders for this ticket</div>
		@endif
	</div>

@endsection

@section('javascript')
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var $select = $('#ticketId').selectize({
				create: true
			});
			var selectize = $select[0].selectize;

			if($('#oldTicketId').val())
			{
				//				selectize.setValue($('#oldTicketId').val(), false);
			}

			$('#ticketId').on("change", function () {
				$('#autoFollowupReminders').submit();
			});

		});
	</script>
@endsection