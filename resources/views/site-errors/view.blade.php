@extends('layout.base')

<?php use Carbon\Carbon; ?>

@section('content')
	<div class="container">
		<div class="page-content">
			<div class="site-errors-wrap">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="upper">
							<div class="pull-left">
								<h2 class="panel-title">All Errors</h2>
							</div>
							<div class="pull-right">
								<button class="btn btn-danger hide" data-url=""
										@if(!$data['siteErrors']->count()) disabled @endif id="btnDelete">
									<i class="glyphicon glyphicon-trash"></i> @if(request()->has('startDate') || request()->has('endDate')) Delete Filtered Errors @else Delete All Errors  @endif
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="panel-body">
						<div>
							<div class="alert-danger pad-t-10 pad-l-10 pad-r-3 pad-b-10 @if($errors->first()) @else hide @endif" id="errorMsg"><a class="close" data-dismiss="alert">&times;</a>
								<i class="glyphicon glyphicon-info-sign mar-r-4"></i>
								{{ $errors->first() }}
							</div>
							@if(session()->has('SuccessMsg'))
								<div class="alert-success pad-t-10 pad-l-10 pad-r-3 pad-b-10">
									<a class="close" data-dismiss="alert">&times;</a>
									<i class="glyphicon glyphicon-ok-sign mar-r-4"></i> {{session('SuccessMsg')}}
								</div>
							@endif
							@if(session()->has('customError'))
								<div class="alert-danger pad-t-10 pad-l-10 pad-r-3 pad-b-10">
									<a class="close" data-dismiss="alert">&times;</a>
									<i class="glyphicon glyphicon-ok-sign mar-r-4"></i> {{session('customError')}}
								</div>
							@endif
						</div>
						<form method="get">
							<div class="mar-b-5">
								<div class="col-sm-3 col-md-3 col-lg-3 no-pad-l">
									<label for="project_id">Project</label>
									<select name="project_id" id="project_id" class="form-control">
										<option value="-1"> -- All --</option>
										@foreach($data['projects'] as $project)
											<option value="{{ $project->id }}" @if(request('project_id') == $project->id) selected @endif>{{ $project->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-lg-3 col-sm-3 col-md-3 no-pad-l">
									<label for="startDate">Start Date</label>
									<input type="text" class="form-control" name="startDate" id="startDate" value="@if(old('startDate')){{old('startDate')}}@else{{request()->get('startDate')}}@endif" placeholder="From Date">
								</div>
								<div class="col-lg-3 col-sm-3 col-md-3 no-pad-l">
									<label for="endDate">End Date</label>
									<input type="text" class="form-control" name="endDate" id="endDate" value="@if(old('endDate')){{old('endDate')}}@else{{request()->get('endDate')}}@endif" placeholder="To Date">
								</div>
								<div class="col-lg-2 col-sm-2 col-md-2 no-pad-l">
									<button class="btn btn-primary mar-t-10" id="deleteError" type="submit">
										<i class="glyphicon glyphicon-eye-open"></i> Filter Errors
									</button>
								</div>
								<div class="col-sm-1 col-md-1 col-lg-1 no-pad mar-t-5">
									@if(request()->has('startDate') || request()->has('endDate') || request()->has('project_id'))
										<a href="/errors" class="btn btn-xs btn-warning "> Reset Filter</a>
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
						</form>
						@if (count($data['siteErrors']) > 0)
							<div class="pad-t-10 alert alert-info alert-details mar-t-10">
								<div class="pull-left">
									<div class="default">
										<h5>Error details will be shown here.</h5>
									</div>
									<div class="data small-font hide">
										<b>
											<div class="head word-break"></div>
										</b>
										<div class="details pad-t-10 word-break"></div>
									</div>
								</div>
								<div class="pull-right">
									<a href="#" class="btn btn-danger btn-xs btn-hide-details hide">Hide Details</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<table class="table table-bordered table-striped table-condensed pad-t-10">
								<thead>
								<tr>
									<th>S.N</th>
									<th>URL</th>
									<th>Code/Message</th>
									<th>Exception</th>
									<th>Occurred</th>
								</tr>
								</thead>
								<tbody>
								<?php $i = 1 ?>
								@foreach($data['siteErrors'] as $siteError)
									<tr class="font-13">
										<td class="word-break">{{ $i++ }}</td>
										<td class="word-break">[{{ $siteError->method }}] <a href="{{ $siteError->url }}" target="_blank">{{ $siteError->url }}</a></td>
										<td class="word-break">[{{ $siteError->code }}] {{ $siteError->error_message }}</td>
										<td class="word-break">{{ $siteError->exception }}</td>
										<td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($siteError->created_at))->diffForHumans() }}</td>
										<td class="word-break">
											<div class="pull-left">
												<a href="#" class="btn btn-info btn-xs btn-show-details" data-id="{{ $siteError->id }}" data-url="{{ $siteError->url }}">Details</a>
											</div>
											<div class="pull-left mar-l-5">
												<a href="{{ route('error.individual.delete',$siteError->id) }}" onclick="return confirm('Are you sure? ','YES','No')" class="btn btn-xs btn-danger">
													<i class="glyphicon glyphicon-trash"></i>
												</a>
											</div>
											<div class="clearfix"></div>
										</td>
										<td class="error-details-{{ $siteError->id }} hide">
											<span>Ip Address: {{ $siteError->ip_address }}</span><br>
											<span>Previous Url: {{ $siteError->previous_url }}</span><br>
											<span>Error Trace: {!! $siteError->details !!}</span>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
							<div class="pad-t-10 text-center">{{ $data['siteErrors']->appends(Input::except('page'))->links() }}</div>
						@else
							<div class="alert alert-success mar-t-20">
								<h3>Hurray! No errors found</h3>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {

			//date time picker
			$('#startDate,#endDate').datetimepicker({
				step: 30,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				maxDate: new Date,
				timepicker: false,
				format: 'Y-m-d'
			});

			$('.site-errors-wrap').on('click', '.btn-show-details', function (event) {

				event.preventDefault();

				var id = $(this).data('id');
				var details = $('.error-details-' + id).html();
				var heading = 'Error details for: (' + id + ') ' + $(this).data('url');

				$('.alert-details .data .head').text(heading);
				$('.alert-details .data .details').append(details);
				$('.alert-details .default').addClass('hide');
				$('.alert-details .data').removeClass('hide');
				$('.btn-hide-details').removeClass('hide');

			})
				.on('click', '.btn-hide-details', function (event) {

					event.preventDefault();

					$(this).addClass('hide');
					$('.alert-details .default').removeClass('hide');
					$('.alert-details .data').addClass('hide');
				});

			$('#btnDelete').click(function (event) {
				event.preventDefault();

				if (confirm("Are you sure")) {
					var startDate = $('#startDate').val();
					var endDate = $('#endDate').val();
					var projectId = $('#project_id').val();

					$.ajax({
						type: 'GET',
						url: $(this).data('url'),
						dataType: 'json',
						data: {startDate: startDate, endDate: endDate, projectId: projectId},
						success: function (data) {
							if (data.success) {
								location.href = '/errors'
							}
						},
						error: function () {
							window.showNotyError();
						}
					});
				}
			})
		});

	</script>
@stop