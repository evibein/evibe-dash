<!-- OneSignal initiation -->
@if(auth()->check())
	<script type="text/javascript">
		var authUser = '<?php echo auth()->user()->toJson(); ?>';
		var appId = '<?php echo config("evibe.onesignal.appId"); ?>';
		var subdomainName = '<?php echo config("evibe.onesignal.subdomainName"); ?>';
		authUser = JSON.parse(authUser);

		function setupOneSignal(user) {

			var OneSignal = window.OneSignal || [];
			OneSignal.push(["init", {
				"appId": appId,
				"subdomainName": subdomainName,
				"autoRegister": true,
				"notifyButton": {
					"enable": false
				},
				"persistNotification": false
			}]);

			OneSignal.push(function () {

				var isPushSupported = OneSignal.isPushNotificationsSupported();
				if (!isPushSupported) {
					return false;
				}

				// user already subscribed
				OneSignal.isPushNotificationsEnabled(function (isEnabled) {
					if (isEnabled) {
						return false;
					}
				});

				// set defaults
				OneSignal.setDefaultTitle("New notification");

				// tags to be used on user subscription
				OneSignal.sendTags({
					"userId": user.hasOwnProperty('userId') ? user['userId'] : "",
					"name": user.hasOwnProperty('name') ? user['name'] : "",
					"email": user.hasOwnProperty('email') ? user['email'] : "",
					"role": user.hasOwnProperty('role') ? user['role'] : ""
				});
			});
		}

		setupOneSignal({
			name: authUser.name,
			userId: authUser.id,
			role: authUser.role_id,
			email: authUser.username
		});
	</script>
@endif