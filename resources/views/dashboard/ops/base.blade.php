@extends('layout.base')

@section('stylesheets')
	@parent
	<style type="text/css" rel="stylesheet" media="all">
		.star-order-highlight-wrap {
			padding: 2px 6px;
			font-size: 10px;
		}
	</style>
@endsection

@section('custom-head')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
@endsection

@section('content')
	<div class="panel panel-default page-content no-border">
		<div class="panel-body pad-b-10">
			<div class="title-sec">
				<div>
					<div class="col-sm-12 text-center">
						<div class="btn-group" role="group" aria-label="...">
							<a class="btn btn-default btn-tab-order-confirmations @if(request()->is('*/order-confirmation-tickets*')) active @endif"
									href="{{route('ops-dashboard.order-confirmation-tickets.list')}}">Order Confirmations
								<span class="ops-tab-count hide"></span></a>
							<a class="btn btn-default btn-tab-customer-updates @if(request()->is('*/customer-updates*')) active @endif"
									href="{{route('ops-dashboard.customer-updates.list')}}">Customer Updates
								<span class="ops-tab-count hide"></span></a>
							<a class="hide btn btn-default btn-tab-order-tracking @if(request()->is('*/order-tracking*')) active @endif"
									href="{{route('ops-dashboard.order-tracking.list')}}">Order Tracking
								<span class="ops-tab-count hide"></span></a>
							<a class="hide btn btn-default btn-tab-avail-checks @if(request()->is('*/avail-checks*')) active @endif"
									href="{{route('ops-dashboard.avail-checks.list')}}">Avail Checks
								<span class="ops-tab-count hide"></span></a>
						<!--<a class="btn btn-default btn-tab-customer-proofs @if(request()->is('*/customer-proofs*')) active @endif"
								   href="{{route('ops-dashboard.customer-proofs.list')}}">Customer Proofs
								<span class="ops-tab-count hide"></span></a> -->
							<a class="btn btn-default btn-tab-customer-reviews @if(request()->is('*/customer-reviews*')) active @endif"
									href="{{route('ops-dashboard.customer-reviews.list')}}">Customer Reviews
								<span class="ops-tab-count hide"></span></a>
							<a class="btn btn-default btn-tab-availability-checks @if(request()->is('*/avail-checks*')) active @endif"
									href="{{route('ops-dashboard.avail-checks.list')}}">Availability Checks
								<span class="ops-tab-count hide"></span></a>
							<a class="btn btn-default btn-tab-option-availability @if(request()->is('*/option-availability*')) active @endif"
									href="{{route('ops-dashboard.option-availability.info')}}">Option Availability
								<span class="ops-tab-count"></span></a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="content-sec">
				<div class="@if(request()->is('*/avail-checks*') || request()->is('*/option-availability*')) col-xs-12 @else col-xs-7 @endif">
					@yield("listData")
				</div>
				<div class="col-xs-5">
					<div class="mar-t-10 @if(request()->is('*/customer-updates*') || request()->is('*/customer-reviews*') )hide @endif">
						<div class="@if(request()->is('*/customer-proofs*') || request()->is('*/avail-checks*') || request()->is('*/option-availability*') ) hide @endif">
							<div class="text-center">
								<div class="col-sm-12">
									<div class="mar-r-10 text-center">
										<input id="opsSearchQuery" type="text" name="opsSearchQuery" class="form-control" placeholder="Search by phone" value=""/>
									</div>
									<a class="btn btn-info mar-t-10" id="opsSearchBtn">Search</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<hr class="hr-text no-pad no-mar-t mar-b-5">
						</div>
						<div id="opsSearchData">

						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			(function updateTabsCount() {
				$('.ops-tab-count').addClass('hide');
				var $ajaxUrl = '/ops-dashboard/tabs-count';
				$.ajax({
					url: $ajaxUrl,
					type: 'GET',
					dataType: 'json',
					data: {}
				}).done(function (data, textStatus, jqXHR) {
					if (data.success === true) {
						if (data.orderConfirmationsCount) {
							$('.btn-tab-order-confirmations .ops-tab-count').html("(" + data.orderConfirmationsCount + ")");
							$('.order-confirmations-pending-count').html("(" + data.orderConfirmationsCount + ")");
						}
						if (data.customerUpdatesCount) {
							$('.btn-tab-customer-updates .ops-tab-count').html("(" + data.customerUpdatesCount + ")");
						}
						if (data.customerProofsCount) {
							$('.btn-tab-customer-proofs .ops-tab-count').html("(" + data.customerProofsCount + ")");
						}
						if (data.customerReviewsCount) {
							$('.btn-tab-customer-reviews .ops-tab-count').html("(" + data.customerReviewsCount + ")");
						}
						$('.ops-tab-count').removeClass('hide');
					} else {
						var $errorMsg = "Some error occurred while fetching the pending todo count. Kindly refresh the page.";
						if (data.errorMsg) {
							$errorMsg = data.errorMsg;
						}
						window.showNotyError($errorMsg);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					window.showNotyError('Some error occurred while fetching the pending todo count. Kindly refresh the page.');
				});
			})();

		});
	</script>
@endsection