@extends("dashboard.ops.base")

@section("listData")
	<div class="form">
		<form method="GET">
			<div class="form-group">
				<div class="col-sm-3 mar-t-10">
					<div class="text-center">
						<div class="filter-title pull-left">Option</div>
						<div class=" no-pad">
							<select id="ouId" name="ouId" class="form-control in-blk">
								<option value="-1">-- select option --</option>
								@foreach($data['options'] as $option)
									<option value="{{ $option['ouId'] }}" @if(request()->input('ouId') == $option['ouId']) selected @endif>[{{ $option['code'] }}] {{ $option['name'] }}</option>
								@endforeach
							</select>
						</div>
						<input type="hidden" id="oaOptionsSlotsUrl" value="{{ route('ops-dashboard.option-availability.option-slots') }}">
					</div>
				</div>
				<div class="col-sm-3 mar-t-10">
					<div class="text-center">
						<div class="filter-title">Date Range</div>
						<div class="in-blk">
							<input type="text"
									class="form-control in-blk filter-date"
									id="startDate" name="startDate"
									@if(request()->input('startDate')) value="{{request()->input('startDate')}}" @elseif(isset($data['startDate']) && $data['startDate']) value="{{ $data['startDate'] }}" @endif>

						</div>
						<div class="in-blk mar-l-5 mar-r-5">
							<div class="text-center mar-t-10 text-bold filter-title">To</div>
						</div>
						<div class="in-blk">
							<input type="text"
									class="form-control filter-date"
									id="endDate" name="endDate"
									@if(request()->input('endDate')) value="{{request()->input('endDate')}}" @elseif(isset($data['endDate']) && $data['endDate']) value="{{ $data['endDate'] }}" @endif>
						</div>
					</div>
				</div>
				<div class="col-sm-3 mar-t-20 pad-t-8">
					<button type="submit" id='submitAvailabilityCheck' class="btn btn-warning"> Check Availability</button>
					<div class="btn btn-link reset-filters hide"
							data-url="{{route('ops-dashboard.option-availability.info')}}"> Reset Filters
					</div>
				</div>
				<div class="col-sm-3 mar-t-20 text-center pad-t-8" style="border-left: 1px solid #DDDDDD;">
					<div id="addUnavailabilityBtn" class="btn btn-primary">Add Slot Unavailability</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<input type="hidden" id="sort" name="" value=""/>
		</form>
	</div>

	<div class="bottom-sec mar-l-10 mar-r-10">
		@if(isset($data['optionSlotsAvailabilityData']) && count($data['optionSlotsAvailabilityData']))
			<div class="mar-t-20 font-18 " style="color: #919191;">Showing availability for {{ $data['optionName'] ? $data['optionName'] : 'the option' }}</div>
			<table style="border: 1px solid black" class="table table-bordered mar-t-10">
				<tr>
					<th>Party Date</th>
					<th>Slot Start</th>
					<th>Slot End</th>
					<th>Total Possible</th>
					<th>Bookings</th>
					<th>Marked Unavailable</th>
					<th>Available</th>
				</tr>
				@foreach($data['optionSlotsAvailabilityData'] as $i => $daySlotsData)
					<tr>
						<td rowspan="{{ count($daySlotsData) }}">{{ date('d M Y', $i) }}</td>
						@php $count = 1; @endphp
				<!-- Since the slots in a day remain the same -->
						@foreach($daySlotsData as $slotData)
							@if($count != 1) <tr> @endif
							@php $count++; @endphp
							<td>{{ $slotData['slotStartTime'] }}</td>
							<td>{{ $slotData['slotEndTime'] }}</td>
							<td>{{ $slotData['totalBookingsPossible'] }}</td>
							<td>{{ $slotData['existingBookings'] }}</td>
							<td>{{ $slotData['markedUnavailable'] }}</td>
							<td @if(($slotData['totalBookingsPossible'] - ($slotData['existingBookings'] + $slotData['markedUnavailable'])) <= 0) class="text-red" @endif>{{ $slotData['totalBookingsPossible'] - ($slotData['existingBookings'] + $slotData['markedUnavailable']) }}</td>
						</tr>
						@endforeach
				@endforeach
			</table>
		@elseif(isset($data['ouId']) && $data['ouId'])
			<div class="text-center font-20 text-red mar-t-10">Slots haven't been provided for this option</div>
		@endif
	</div>

	@include("dashboard.ops.option-availability.modals")


@endsection

@section('javascript')
	@parent

	<script type="text/javascript">
		$(document).ready(function () {

			$("#ouId").selectize();
			$("#oaOption").selectize();

			// check in & check out date popup
			$('#startDate,#endDate,#oaDate').datetimepicker({
				step: 30,
				timepicker: false,
				scrollInput: false,
				scrollMonth: false,
				closeOnDateSelect: true,
				format: 'Y-m-d'
			});

			$("#addUnavailabilityBtn").on('click', function (event) {
				event.preventDefault();
				$("#addOptionUnavailabilityModal").modal("show");
			});

			$("#oaOption").on('change', function () {
				var $oaOptionId = $("#oaOption").val();
				if (($oaOptionId !== "-1") && ($oaOptionId)) {
					/* @todo: remove slot selection element */
					$(".slot-selection-wrap").empty();
					/* make ajax request to fetch slots data */
					$.ajax({
						url: $("#oaOptionsSlotsUrl").val(),
						type: "GET",
						dataType: "JSON",
						data: {
							'ouId': $oaOptionId
						},
						success: function (data) {
							if (data.success) {
								/* create html based on the data */
								var $element = "";
								if (data.optionSlots) {
									$element += '<select id="oaSlotId" name="oaSlotId" class="form-control">';
									$.each(data.optionSlots, function (i, val) {
										$element += '<option value="' + val.id + '">' + val.slot_start_time + ' - ' + val.slot_end_time + ' (Total ' + val.bookings_available + ' slots)</option>';
									});
									$element += '</select>';
								} else {
									/* if slots are unavailable, show error and disable modal submit button */
									$element += '<div class="text-red">There are no slots provided for this options!</div>'
									window.showNotyError("There are no slots defined for this option");
								}
								$(".slot-selection-wrap").append($element);
							} else {
								/* $("#submitAddUnavailability").removeAttr('disabled'); */
								var $errorMsg = 'Some error occurred while fetching option slots. Kindly refresh the page and try again';
								if (data.errorMsg) {
									$errorMsg = data.errorMsg;
								}
								window.showNotyError($errorMsg);
							}
						},
						error: function (data) {
							window.showNotyError("Some error occurred, please try again");
						}
					});
				}
			});

			$("#submitAddUnavailability").on('click', function (event) {
				event.preventDefault();

				$.ajax({
					url: $(this).data('url'),
					type: "POST",
					dataType: "JSON",
					data: {
						"ouId": $("#oaOption").val(),
						"slotId": $("#oaSlotId").val(),
						"slotDate": $("#oaDate").val(),
						"oaUnavailableSlotsCount": $("#oaUnavailableSlotsCount").val(),
						"oaComments": $("#oaComments").val()
					},
					success: function (data) {
						if (data.success) {
							window.showNotySuccess("Successfully added slot unavailability");
							window.setTimeout(function () {
								location.reload()
							}, 1000);
						} else {
							/* $("#submitAddUnavailability").removeAttr('disabled'); */
							var $errorMsg = 'Some error occurred while adding option unavailability. Kindly refresh the page and try again';
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (data) {
						window.showNotyError("Some error occurred, please try again");
					}
				});
			})

		});
	</script>
@endsection