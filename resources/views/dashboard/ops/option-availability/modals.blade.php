<div id="addOptionUnavailabilityModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content mar-t-10">
			<form id=" " action="" method="" enctype="multipart/form-data">
				<div class="modal-header">
					<h4 class="modal-title">Option Unavailability</h4>
				</div>
				<div class="modal-body">
					<div class="hide alert-danger pad-6 mar-b-15 " id="errorBox"></div>
					<div class="">
						<div class="col-sm-4 no-pad-l pad-t-5">
							<label class="no-pad">Select Option</label>
						</div>
						<div class="col-sm-8 no-pad-l">
							<select name="oaOption" id="oaOption">
								<option value="-1">-- Select Option --</option>
								@foreach($data['options'] as $option)
									<option value="{{ $option['ouId'] }}">[{{ $option['code'] }}] {{ $option['name'] }}</option>
								@endforeach
							</select>
						</div>
						<div class="clearfix"></div>
						<div class="form-group mar-t-20">
							<div class="col-sm-4 no-pad-l pad-t-5">
								<label class="no-pad">Select Slot</label>
							</div>
							<div class="col-sm-8 no-pad-l slot-selection-wrap">

							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group mar-t-20">
							<div class="col-sm-4 no-pad-l no-pad-l pad-t-5">
								<label class="no-pad">Slot Date</label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<input type="text" id="oaDate" name="oaDate" class="form-control">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group mar-t-20">
							<div class="col-sm-4 no-pad-l no-pad-l pad-t-5">
								<label class="no-pad">Slots Unavailable</label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<input type="text" id="oaUnavailableSlotsCount" name="oaUnavailableSlotsCount" class="form-control">
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="form-group mar-t-20">
							<div class="col-sm-4 no-pad-l no-pad-l pad-t-5">
								<label class="no-pad">Reason</label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<input type="text" id="oaComments" name="oaComments" class="form-control">
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="modal-footer no-pad-b">
						<button type="button" class="btn btn-warning" data-dismiss="modal"><i
									class="glyphicon glyphicon-remove"></i> Close
						</button>
						<button type="button" id="submitAddUnavailability" class="btn btn-success" data-url="{{route("ops-dashboard.option-availability.add-unavailability")}}">
							<i class="glyphicon glyphicon-floppy-save"></i> Add Unavailability
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>