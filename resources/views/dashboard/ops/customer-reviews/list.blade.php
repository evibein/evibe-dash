@extends("dashboard.ops.base")
@section('listData')
	<div class="data-sec mar-t-10 pad-b-20">
		@if(isset($data['customerReviews']) && count($data['customerReviews']))

			<table id="customerReviews" class="table table-bordered table-responsive table-hover">
				<thead class="text-info">
				<tr>
					<th>Review Submitted</th>
					<th>Customer</th>
					<th>Average</th>
					<th>Recomend evibe</th>
					<th>Feedback</th>
					<th>Six Star</th>
				</tr>
				</thead>
				<tbody>
				@foreach($data['customerReviews'] as $review)

					<tr>
						<td>
							@if(isset($review['createdAt']) && $review['createdAt'])
								{{$review['createdAt']}}
							@else
								--
							@endif
						</td>
						<td>
							<a href="/tickets/{{ $review['ticketId'] }}/bookings" target="_blank">
								<div>
									@if(isset($review['customerName']) && $review['customerName'])
										{{ ucfirst($review['customerName']) }}
									@else
										--
									@endif
								</div>
								<div>
									@if(isset($review['customerPhone']) && $review['customerPhone'])
										{{ ucfirst($review['customerPhone']) }}
									@else
										--
									@endif
								</div>
								<div>
									@if(isset($review['customerEmail']) && $review['customerEmail'])
										{{ ucfirst($review['customerEmail']) }}
									@else
										--
									@endif
								</div>
							</a>
						</td>
						<td>
							@if(isset($review['averageRating']) && $review['averageRating'])
								{{round($review['averageRating'],2)}}
							@else
								--
							@endif
						</td>
						<td>
							@if(isset($review['isRecommendEvibe']) && ($review['isRecommendEvibe'] == 1))
								Yes
							@else
								No
							@endif
						</td>
						<td>
							@if(isset($review['customerReview']) && $review['customerReview'])
								{{$review['customerReview']}}
							@else
								--
							@endif
						</td>
						<td>
							@if(isset($review['sixStar']) && $review['sixStar'])
								{{$review['sixStar']}}
							@else
								--
							@endif
						</td>
					</tr>
				@endforeach

				</tbody>
			</table>
		@else
			<div class="alert-danger text-center text-bold pad-10">
				<i class="glyphicon glyphicon-eye-open"></i>
				No new reviews were found.
			</div>
		@endif
	</div>
	<div class="modal fade resolve-review-modal in hide" style="display: block;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Please enter comments for resolving the ticket</h4>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label for="resolveReviewComment">Enter Comments</label>
						<textarea name="" id="resolveReviewTextbox" name="resolveReviewTextbox" rows="5" placeholder="Type your text here(minimum length of 6 letters)" class="form-control resolveReviewTextbox" required=""></textarea>
					</div>
					<input type="hidden" value="" id="reviewId" name="reviewId">
					<div class="pull-right">
						<a class="btn btn-default btn-close" data-dismiss="modal">Close</a>

						<input class="btn btn-primary btn-resolve" data-id="" type="button" value="resolve"/>

					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
@endsection


@section('javascript')
	@parent
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#customerReviews').DataTable({
				"fixedHeader": true,
				"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]]
			});
			$('.btn-resolve').on('click', function () {
				var id = $(this).data("id");
				var comment = $('#' + id).val();
				if (comment == '') {
					showNotyError("Please enter some comment before you resolve the issue");

				} else {

					$.ajax({
						url: '/ops-dashboard/customer-reviews/resolve',
						type: 'GET',
						data: {
							ticketId: id,
							comment: comment
						},
						success: function (data) {
							if (data.success == true) {
								showNotySuccess("Review resolve successful");
								setTimeout(function () {
									window.location.reload();
								}, 1000);
							} else {
								showNotyError(data.errorMsg);
							}
						},

						error: function (jqXHR, textStatus, errorThrown) {
							showNotyError("Error occurred while resolving the review ");
						}

					})

				}

			});
			$('.close,.btn-close').on('click', function () {
				$('.resolve-review-modal').addClass('hide');
				$('.resolveReviewTextbox').val('');
			});
			$('.act-rej-review-btn').on('click', function () {
				var id = $(this).data("id");

				$('.resolveReviewTextbox').attr('id', id);
				$('.btn-resolve').attr('data-id', id);
				$('.resolve-review-modal').removeClass('hide');
			});

		});
	</script>
@endsection