@if($data['searchData'] && count($data['searchData']))
	@foreach($data['searchData'] as $data)
		<div class="mar-t-10">
			<table class="table table-bordered mar-b-5">
				<tr>
					<td colspan="2">
						<div class="pull-left">
							<i class="glyphicon glyphicon-user"></i>
							@if (isset($data['customerName']) && $data['customerName'])
								{{ $data['customerName'] }}
							@else
								---
							@endif
						</div>
						<div class="pull-right">
							<i class="glyphicon glyphicon-calendar"></i>
							@if(isset($data['partyDateTime']) && $data['partyDateTime'])
								{{ $data['partyDateTime'] }}
							@else
								--
							@endif
						</div>
						<div class="clearfix"></div>
						<div class="pull-left mar-t-5">
							<i class="glyphicon glyphicon-map-marker"></i>
							@if (isset($data['city']) && $data['city'])
								{{ $data['city'] }}
							@else
								---
							@endif
						</div>
						<div class="pull-right mar-t-5">
							<i class="glyphicon glyphicon-gift"></i>
							@if (isset($data['event']) && $data['event'])
								{{ $data['event'] }}
							@else
								---
							@endif
						</div>
					</td>
				</tr>
				<tr>
					<td>Partner</td>
					<td>
						@if(isset($data['partnerCompanyName']) && $data['partnerCompanyName'])
							<span class="mar-r-5">
								{{ $data['partnerCompanyName'] }}
							</span>
						@endif
						@if(isset($data['partnerCode']) && $data['partnerCode'])
							<span>
								{{ $data['partnerCode'] }}
							</span>
						@endif
					</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>
						@if(isset($data['status']) && $data['status'])
							{{ $data['status'] }}
						@else
							--
						@endif
					</td>
				</tr>
				<tr>
					<td>Booking Info</td>
					<td>
						@if(isset($data['productInfo']) && $data['productInfo'])
							{!! $data['productInfo'] !!}
						@else
							--
						@endif
					</td>
				</tr>
				<tr>
					<td>Booking Amount</td>
					<td>
						@if(isset($data['bookingAmount']) && $data['bookingAmount'])
							{{ $data['bookingAmount'] }}
						@else
							--
						@endif
					</td>
				</tr>
				<tr>
					<td>Advance Amount</td>
					<td>
						@if(isset($data['advanceAmount']) && $data['advanceAmount'])
							{{ $data['advanceAmount'] }}
						@else
							--
						@endif
					</td>
				</tr>
			</table>
		</div>
	@endforeach
@else
	<div class="pad-t-30">
		<div class="alert alert-danger no-mar-b">
			<span>Sorry, no order info found.</span>
		</div>
	</div>
@endif