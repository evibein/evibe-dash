@extends("dashboard.ops.base")

@section("listData")


	<div class="text-center mar-t-15">
		<a class="btn btn-default pad-b-10 @if( trim(Request::get('query'))=="pending" || trim(Request::get('query'))==null ) active @endif"
				href="{{route('ops-dashboard.order-confirmation-tickets.list')}}">Pending
			@if( isset($data['orderConfirmations']) &&(trim(Request::get('query'))=="pending" || trim(Request::get('query'))==null))
				<span>{{ "(". count($data['orderConfirmations']) .")" }}</span>
			@else
				<span class="order-confirmations-pending-count"></span>
			@endif
		</a>
		<a class="btn btn-default mar-l-10 pad-b-10 @if( trim(Request::get('query'))=="confirmed") active @endif"
				href="{{route('ops-dashboard.order-confirmation-tickets.list')}}?query=confirmed"> Confirmed
			@if( isset($data['orderConfirmations']) && trim(Request::get('query'))=="confirmed")
				<span>{{ "(". count($data['orderConfirmations']) .")" }}</span>
			@endif
		</a>
	</div>
	<div class="mar-t-15">
		<form action="{{route('ops-dashboard.order-confirmation-tickets.list')}}" method="get">
			<input type="hidden" name="query" @if( trim(Request::get('query'))=="confirmed") value="confirmed" @else value="pending" @endif/>
			<div class="row">
				<div class="col-sm-4">
					<div><label for="partyDateSearch">Search by Party Date</label></div>
					<input id="partyDateSearch" name="partyDate" type="text" class="form-control"
							placeholder="Party Date Search" @if(Request::get('partyDate')!=null) value="{{Request::get('partyDate')}}" @endif >
				</div>
				@if( trim(Request::get('query'))=="confirmed")
					<div class="col-sm-4">
						<div><label for="ticketConfirmDateSearch">Search by Confirm Date</label></div>
						<input id="ticketConfirmDateSearch" name="confirmDate" type="text" class="form-control"
								placeholder="Confirm Date Search" @if(Request::get('confirmDate')!=null) value="{{Request::get('confirmDate')}}" @endif >
					</div>
					<div class="col-sm-4">
						<div><label for="handlerSearch">Search by Handler</label></div>
						<select id="handlerSearch" name="handlerId" class="form-control"
								placeholder="Handler Name" @if(Request::get('handlerName')!=null) value="{{Request::get('handlerName')}}" @endif >
							<option value="" disabled selected> Select Handler</option>
							@if(isset($data['handlers']))
								@foreach($data['handlers'] as $handler)
									<option value="{{$handler['handlerId']}}" @if(Request::get('handlerId')==$handler['handlerId']) {{'selected'}}  @endif> {{$handler['handlerName']}} </option>
								@endforeach
							@endif
						</select>
					</div>
				@endif
				<div class="clearfix"></div>
			</div>

			<div class="mar-t-10 pad-b-10">
				<input type='submit' class="btn btn-info mar-t-10" id="pendingBookingsDateSearch" value="Search"/>

				<a class="btn btn-link mar-t-10 {{trim(Request::get('query'))}}" id="clearPartyDateSearch"
						@if( trim(trim(Request::get('query')))=="confirmed") href="{{route('ops-dashboard.order-confirmation-tickets.list')}}?query=confirmed"
						@else href="{{route('ops-dashboard.order-confirmation-tickets.list')}}"
						@endif>

					Clear Search </a>
			</div>
		</form>
	</div>

	<div class="data-sec mar-t-15 pad-b-20" id="pendingBookings">

		@if(isset($data['orderConfirmations']) && count($data['orderConfirmations']))
			<table id="orderConfirmationsData" class="table table-bordered table-responsive table-hover">
				<thead class="text-info">
				<tr>
					<th>Customer</th>
					<th>Party Date</th>
					<th>Partner Data</th>
					<th>Status</th>
					@if( Request::get("query")!=null && Request::get("query")==1)

						<th>Confirmation Details</th>

					@else

						<th>Action CTAs</th>

					@endif

				</tr>
				</thead>
				<tbody>
				@foreach($data['orderConfirmations'] as $order)
					<tr>
						<td>
							<a href="/tickets/{{ $order['ticketId'] }}/bookings" target="_blank">
								<div>
									@if(isset($order['customerName']) && $order['customerName'])
										{{ ucfirst($order['customerName']) }}
									@else
										--
									@endif
								</div>
								<div>
									@if(isset($order['customerPhone']) && $order['customerPhone'])
										{{ ucfirst($order['customerPhone']) }}
									@else
										--
									@endif
								</div>
								<div class="hide">
									@if(isset($order['customerEmail']) && $order['customerEmail'])
										{{ ucfirst($order['customerEmail']) }}
									@else
										--
									@endif
								</div>
							</a>
							@if(isset($order['isStarOrder']) && $order['isStarOrder'])
								<div class="star-order-highlight-wrap mar-t-5">
									<span class="glyphicon glyphicon-star"></span>
									<span>This is a star order</span>
								</div>
							@endif
						</td>
						<td>
							@if(isset($order['partyDateTime']) && $order['partyDateTime'])
								<div class="text-white font-1">[{{ date('y/m/d H:i', strtotime($order["partyDateTime"])) }}]</div>
								{{ $order['partyDateTime'] }}
							@else
								--
							@endif
						</td>
						<td>
							<div>
								@if(isset($order['partnerPersonName']) && $order['partnerPersonName'])
									{{ $order['partnerPersonName'] }}
								@else
									--
								@endif
							</div>
							<div>
								@if(isset($order['partnerCompanyName']) && $order['partnerCompanyName'])
									{{ $order['partnerCompanyName'] }}
								@else
									--
								@endif
							</div>
							<div>
								@if(isset($order['partnerPhone']) && $order['partnerPhone'])
									{{ $order['partnerPhone'] }}
								@else
									--
								@endif
							</div>
							<div class="hide">
								@if(isset($order['partnerEmail']) && $order['partnerEmail'])
									{{ $order['partnerEmail'] }}
								@else
									--
								@endif
							</div>
						</td>
						<td>
							@if(isset($order['status']) && $order['status'])
								{{ $order['status'] }}
							@else
								--
							@endif
						</td>
						<td class="text-center">
							@if(isset($order['partnerId']) && isset($order['partnerTypeId']) && $order['partnerId'] && $order['partnerTypeId'])
								@if(trim(Request::get('query'))!="confirmed")
									@if($order['statusId'] == config('evibe.status.auto_cancel'))
										<div class="font-11">[Kindly assign another partner]</div>
									@else
										<div>
											<a class="btn btn-primary confirm-partner-btn btn-xs mar-t-10"
													data-url="/ops-dashboard/order-confirmation-tickets/confirm-partner/{{ $order['ticketBookingId'] }}">
												<i class="glyphicon glyphicon-ok"></i> Confirm Partner
											</a>
										</div>
									@endif
									<div class="font-10 mar-t-10 text-italic">
										<a href="/tickets/{{ $order['ticketId'] }}/bookings" target="_blank">
											Change Partner / Cancel Order
										</a>
									</div>
								@endif
							@else
								<div>There is no partner for this order</div>
								<div class="font-10 mar-t-10 text-italic">
									<a href="/tickets/{{ $order['ticketId'] }}/bookings" target="_blank">(Go To
										Ticket)</a>
								</div>
							@endif
							@if(trim(Request::get('query'))=="confirmed")
								<div class="mar-t-10 text-italic">
									@if(isset($order['confirmedByHandler']) && $order['confirmedByHandler'])
										<div class="font-16">{{ $order['confirmedByHandler'] }}</div>
									@else
										--
									@endif
									<div>

										@if(isset($order['partnerConfirmedAt']) && $order['partnerConfirmedAt'])
											{{ $order['partnerConfirmedAt'] }}
										@else
											--
										@endif

									</div>
									<br>
									@endif

									<a href="#" class="order-confirmation-info-btn font-14" data-ticket-id="{{ $order["ticketId"] }}" data-ticket-booking-id="{{ $order['ticketBookingId'] }}">Order Info</a>
								</div>


						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		@elseif(isset($data["errorMsg"]) && $data["errorMsg"] )
			<br><br>
			<div class="alert-danger text-center text-bold pad-10" style="position: relative;top:30px;width:auto;">
				<i class="glyphicon glyphicon-warning-sign"></i>
				{{ $data["errorMsg"] }}
			</div>
		@else
			<br><br>
			<div class="alert-danger text-center text-bold pad-10" style="position: relative;width:auto;">
				<i class="glyphicon glyphicon-eye-open"></i>
				No new orders were found.
			</div>
		@endif
	</div>
@endsection

@section('javascript')
	@parent
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).ready(function () {
				var a = new Array();
				$("#handlerSearch").children("option").each(function (x) {
					test = false;
					b = a[x] = $(this).val();
					for (i = 0; i < a.length - 1; i++) {
						if (b == a[i]) test = true;
					}
					if (test) $(this).remove();
				})
			});

			$('#partyDateSearch,#ticketConfirmDateSearch').datetimepicker({ // party date
				timepicker: false,
				scrollInput: false,
				scrollMonth: false,
				closeOnDateSelect: true,
				format: "Y-m-d"

			});

			$('#orderConfirmationsData').DataTable({
				"fixedHeader": true,
				"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]],
				'order': [[1, 'asc']]
			});

			$('.confirm-partner-btn').on('click', function () {
				var confirmMessage = "Are you sure?";
				if (!confirm(confirmMessage)) {
					return false;
				}

				$.ajax({
					url: $(this).data('url'),
					type: 'POST',
					dataType: 'json',
					data: {}
				}).done(function (data, textStatus, jqXHR) {

					if (data.success === true) {
						var $successMsg = "Partner confirmed successfully";
						if (data.successMsg) {
							$successMsg = data.successMsg;
						}
						window.showNotySuccess($successMsg);
						setTimeout(function () {
							window.location.reload();
						}, 1000);
					} else {
						var $errorMsg = "Some issue occurred while confirming the partner. Kindly refresh the page and try again.";
						if (data.errorMsg) {
							$errorMsg = data.errorMsg;
						}
						window.showNotyError($errorMsg);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					window.showNotyError("Some issue occurred while confirming the partner. Kindly refresh the page and try again.");
				});
			});

			$('#opsSearchBtn').on('click', function () {
				var $searchQuery = $('#opsSearchQuery').val();
				var $ticketId = "";
				var $ticketBookingId = "";
				searchOrderConfirmations($searchQuery, $ticketId, $ticketBookingId);
			});

			$('#pendingBookingsDateSearch').on('click', function () {

				var $query = "0";
				var $partyDate = $('#partyDateSearch').val();
				var $confirmDate = "";

				filterByDate($query, $partyDate, $confirmDate);
			});

			$('.order-confirmation-info-btn').on('click', function (event) {
				event.preventDefault();

				var $searchQuery = "";
				var $ticketId = $(this).data('ticket-id');
				var $ticketBookingId = $(this).data('ticket-booking-id');
				searchOrderConfirmations($searchQuery, $ticketId, $ticketBookingId);
			});

			function searchOrderConfirmations($searchQuery, $ticketId, $ticketBookingId) {
				$.ajax({
					url: '/ops-dashboard/order-confirmation-tickets/search',
					type: 'GET',
					dataType: 'html',
					data: {
						query: $searchQuery,
						ticketId: $ticketId,
						ticketBookingId: $ticketBookingId
					},
					success: function (data) {
						if (typeof data !== undefined && data) {
							$("#opsSearchData").empty().append(data);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showNotyError("Error occurred while fetching the data, Please reload the page & try again.")
					}
				})
			}

		});
	</script>
@endsection