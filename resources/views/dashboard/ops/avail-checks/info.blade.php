@extends("dashboard.ops.base")

@section("listData")
	<div class="form">
		<form method="GET">
			<div class="form-group">
				<div class="col-sm-3 mar-t-10">
					<div class="text-center">
						<div class="filter-title pull-left">By Name, phone</div>
						<div class=" no-pad">
							<input type="text"
									class="form-control in-blk"
									id="basicSearch" name="query" placeholder="Name, Phone"
									@if(request()->input('query'))value="{{request()->input('query')}}" @endif>
						</div>
					</div>
				</div>
				<div class="col-sm-3 mar-t-10">
					<div class="text-center">
						<div class="filter-title">By Party Date</div>
						<div class="in-blk">
							<input type="text"
									class="form-control in-blk filter-date"
									id="partyStartTime" name="start_date"
									@if(request()->input('start_date'))value="{{request()->input('start_date')}}" @endif>

						</div>
						<div class="in-blk mar-l-5 mar-r-5">
							<div class="text-center mar-t-10 text-bold filter-title">To</div>
						</div>
						<div class="in-blk">
							<input type="text"
									class="form-control filter-date"
									id="partyEndTime" name="end_date"
									@if(request()->input('end_date'))value="{{request()->input('end_date')}}" @endif>
						</div>
					</div>
				</div>
				<div class="col-sm-3 mar-t-10">
					<div class="text-center">
						<div class="filter-title">By Requested Date</div>
						<div class="in-blk">
							<input type="text"
									class="form-control in-blk filter-date"
									id="partyStartTime" name="created_start_date"
									@if(request()->input('created_start_date'))value="{{request()->input('created_start_date')}}" @endif>

						</div>
						<div class="in-blk mar-l-5 mar-r-5">
							<div class="text-center mar-t-10 text-bold filter-title">To</div>
						</div>
						<div class="in-blk">
							<input type="text"
									class="form-control filter-date"
									id="partyEndTime" name="created_end_date"
									@if(request()->input('created_end_date'))value="{{request()->input('created_end_date')}}" @endif>
						</div>
					</div>
				</div>
				<div class="col-sm-3 mar-t-20 ">
					<button type="submit" id='submitFilter' class="btn btn-warning"> Filter</button>
					<div class="btn btn-link reset-filters hide" data-url="{{route('ops-dashboard.avail-checks.list')}}"> Reset Filters</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<input type="hidden" id="sort" name="" value=""/>
		</form>
	</div>

	<div class="bottom-sec mar-l-10 mar-r-10">

		<div class="new-tickets">
			<div class="data-sec mar-t-10 pad-b-20">
				<div id="callUrl" data-url="{{route("ops-dashboard.avail-checks.details")}}"></div>
				@if(count($data['availCheckData'] )>0)
					<table id="availCheckSummary" class="table table-bordered table-responsive table-hover">
						<thead class="text-info">
						<tr>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">CTA's</div>

							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Customer Name</div>
							</th>
							<th style="width: 146px;">
								<div class="in-blk" style="margin: auto;vertical-align: super;">Party Date</div>
								<div class="in-blk mar-l-20">
									<div id="partyDateAsc" class="list-sort-arrow">
										<i class="glyphicon glyphicon-circle-arrow-up @if(Request::get('partyDateAsc')) alert-danger @endif"></i>
									</div>
									<div id="partyDateDesc" class="list-sort-arrow">
										<i class="glyphicon glyphicon-circle-arrow-down @if(Request::get('partyDateDesc')) alert-danger @endif"></i>
									</div>
								</div>
							</th>
							<th style="width: 146px;">
								<div class="in-blk" style="margin: auto;vertical-align: super;">Created At</div>
								<div class="in-blk mar-l-20">
									<div id="createdAtAsc" class="mar-r-10 list-sort-arrow">
										<i class="glyphicon glyphicon-circle-arrow-up @if(Request::get('createdAtAsc')) alert-danger @endif"></i>
									</div>
									<div id="createdAtDesc" class="list-sort-arrow">
										<i class="glyphicon glyphicon-circle-arrow-down @if(Request::get('createdAtDesc') || ($data['isDescended'])) alert-danger @endif"></i>
									</div>
								</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Type</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Option</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Assigned Partners</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Budget By Ops</div>
							</th>
							<th width="30%">
								<div class="in-blk" style="margin: auto;vertical-align: super;">Avail Check Info</div>
							</th>
							<th width="8%">
								<div class="in-blk" style="margin: auto;vertical-align: super;">Partner Reply</div>
							</th>
						</tr>

						</thead>
						<tbody>
						@php $check=''; @endphp
						@foreach($data['availCheckData'] as $check)
							@php
								$span=1;
								if(count($check['partners'])>0)
								{
									$span=count($check['partners']);
								}


							@endphp
							<tr>
								<td rowspan="{{$span}}">
									<div>
										<div class="btn btn-info btn-post-avail-check" data-id="{{ $check['availCheckId'] }}">
											Ask Partner
										</div>
									</div>
								</td>

								<td rowspan="{{$span}}">

									<a href="{{ route("ticket.details.avail-checks", $check["ticketId"]) }}" target="_blank">
										<div>
											@if($check["name"])
												{{ $check["name"] }}
											@else
												--
											@endif
										</div>
										<div>
											@if($check["phoneNumber"])
												{{ $check["phoneNumber"] }}
											@else
												--
											@endif
										</div>
									</a>

								</td>

								<td rowspan="{{$span}}">
									<div>
										@if(isset($check["partyDate"]))

											{{ date("d M y, g:i a", $check["partyDate"])}}
										@else
											<div class="text-white font-1">9999</div>
											---
										@endif</div>
								</td>
								<td rowspan="{{$span}}">
									<div>
										@if(isset($check["createdDate"]))

											{{ date("d M y, g:i a", strtotime($check["createdDate"]["date"]))}}
										@else
											<div class="text-white font-1">9999</div>
											---
										@endif</div>
								</td>

								<td rowspan="{{$span}}">
									<div>
										@if(isset($check['optionType']))

											{{$check['optionType']}}

										@else
											{{"optionType not found"}}

										@endif
									</div>
								</td>

								<td rowspan="{{$span}}">
									<div>
										@if(isset($check['option']))

											{{$check['option']}}

										@else

											{{"option not found"}}

										@endif
									</div>
									@if(isset($check['isStarOption']) && $check['isStarOption'])
										<div class="star-order-highlight-wrap mar-t-5">
											<span class="glyphicon glyphicon-star"></span>
											<span>Star Option</span>
										</div>
									@endif
								</td>

								@if(count($check['partners'])>0)

									<td>
										{{$check['partners'][0]['name']}}
									</td>
									<td>
										{{$check['partners'][0]['budget']}}
									</td>
									<td>
										<div class="avail-check-info-cell">
											{{$check['partners'][0]['info']}}
										</div>
									</td>
									<td>
										@if($check['partners'][0]['isReplied'])
											<div><b>{{$check['partners'][0]['replyDecision']?'Yes':'No'}}</b></div>
											<div class="mar-t-5">{{$check['partners'][0]['replyText']}}</div>
											<div class="font-12 mar-t-5"><i>{{$check['partners'][0]['repliedAt']}}</i>
											</div>
										@else
											---
										@endif
									</td>
								@else
									<td rowspan="1">
										--
									</td>
									<td rowspan="1">
										--
									</td>
									<td rowspan="1">
										--
									</td>
									<td rowspan="1">
										--
									</td>

								@endif
							</tr>
							@foreach($check['partners'] as $key => $partner)
								@if($key!=0)
									<tr>
										<td>
											{{$check['partners'][$key]['name']}}
										</td>
										<td>
											{{$check['partners'][$key]['budget']}}
										</td>
										<td>
											<div class="avail-check-info-cell">
												{{ $check['partners'][$key]['info'] }}
											</div>
										</td>
										<td>
											@if($check['partners'][$key]['isReplied'])
												<div><b>{{$check['partners'][$key]['replyDecision']?'Yes':'No'}}</b>
												</div>
												<div class="mar-t-5">{{$check['partners'][$key]['replyText']}}</div>
												<div class="font-12 mar-t-5">
													<i>{{$check['partners'][$key]['repliedAt']}}</i></div>
											@else
												---
											@endif
										</td>
									</tr>
								@endif
							@endforeach


						@endforeach
						</tbody>
					</table>
				@else
					<div class="alert-danger text-center text-bold pad-10">
						<i class="glyphicon glyphicon-eye-open"></i>
						No avail checks were found.
					</div>
				@endif
			</div>
		</div>
	</div>

	@include("dashboard.ops.avail-checks.modals")


@endsection

@section('javascript')
	@parent
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>

	<script type="text/javascript">


		$(document).ready(function () {
			$('#partyDateAsc').click(function () {
				$('#sort').attr("name", "partyDateAsc");
				$('#sort').val(true);
				$('#submitFilter').click();
			});

			$('#partyDateDesc').click(function () {
				$('#sort').attr("name", "partyDateDesc");
				$('#sort').val(true);
				$('#submitFilter').click();
			});
			$('#createdAtAsc').click(function () {
				$('#sort').attr("name", "createdAtAsc");
				$('#sort').val(true);
				$('#submitFilter').click();
			});
			$('#createdAtDesc').click(function () {
				$('#sort').attr("name", "createdAtDesc");
				$('#sort').val(true);
				$('#submitFilter').click();
			});

			$('.reset-filters').on('click', function () {

				window.location = $(this).data('url');

			});
			$('.filter-date').datetimepicker({
				timepicker: false,
				scrollInput: false,
				scrollMonth: false,
				closeOnDateSelect: true,
				format: "d-m-Y"
			});

			(location.href.indexOf('?') > 0) && $('.reset-filters').removeClass('hide');

			$.getScript("/js/tickets/basicInfo.js");

			$('#getCheckInfo').wysihtml5({
				toolbar: {
					"image": false,
					'html': true,
					'blockquote': false,
					'size': 'xs'
				}
			});

		});

		//get the existing data and options

		var removedImages = [];
		var availCheckPartnersResults = [];

		var $availCheckPartners = $('#availCheckPartners');
		$selectavailCheckPartners = $availCheckPartners.selectize({
			//openOnFocus: false,
			//create: false,

			valueField: 'id',
			labelField: 'name',
			searchField: ['name', 'person', 'label'],
			render: {
				option: function (data, escape) {
					return "<div data-id='" + data.id + "'>" + data.label + "</div>"
				}
			}
		});

		var availCheckPartnersValue = $selectavailCheckPartners[0].selectize;

		$('#availCheckPartnersData').find('li').each(function (count) {

			availCheckPartnersResults.push({
				id: $(this).data('id'),
				name: $(this).data('name'),
				person: $(this).data('person'),
				label: $(this).data('name') + ' - ' + $(this).data('person')
			});

		});

		availCheckPartnersValue.disable();
		availCheckPartnersValue.addOption(availCheckPartnersResults);
		availCheckPartnersValue.enable();
		availCheckPartnersValue.refreshOptions();
		availCheckPartnersValue.blur();
		//availCheckPartnersValue.clear();

		$(".btn-post-avail-check").click(function () {

			var id = $(this).data('id');
			$('#submitAvailCheck').attr('data-id', id);
			var url = $('#callUrl').data('url');

			$.ajax({
				url: url,
				type: "POST",
				dataType: "JSON",
				data: {
					'availCheckId': id
				},
				success: function (data) {
					if (data.success) {
						var details = data.availCheckData;

						if (details.customerName) {
							$('#customerName').val(details.customerName);
						}
						if (details.option) {
							if (details.optionName) {
								$('#option').val(details.optionType + " - " + details.option + " - " + details.optionName);
							} else {
								$('#option').val(details.optionType + " - " + details.option);

							}
						}
						if (details.optionType) {
							$('#optionType').val(details.optionType);
						}
						if (details.partyDate) {
							$('#partyDateTime').val(details.partyDate);
						}
						if (details.budget) {
							$('#budget').val(details.budget);
						}
						if (details.info) {

							details.optionName && $('#getCheckInfo').data('wysihtml5').editor ? $('#getCheckInfo').data('wysihtml5').editor.setValue('<b>' + details.optionName + '</b><br>' + details.info) : "";
							!details.optionName && $('#getCheckInfo').data('wysihtml5').editor ? $('#getCheckInfo').data('wysihtml5').editor.setValue(details.info) : "";
						}

						if (data.planners) {
							//$('#availCheckPartnersData').empty();
							$('#venueData').empty();

							var $element = "";

							var list = data.planners;
							var venueList = [];

							if (list.length == 1) {
								$.each(data.planners, function (i, val) {
									$element += '<li data-id="' + val.plannerId + '" data-name="' + val.plannerName + '" data-person="' + val.plannerPerson + ' " data-code = "' + val.plannerCode + '" data-userid = "' + val.plannerUserId + '">' + val.plannerName + ', </li>'

									venueList.push({
										id: val.plannerId,
										name: val.plannerName,
										person: val.plannerPerson,
										label: val.plannerName + ' - ' + val.plannerPerson
									});
								});

								$('#availCheckPartnersData').append($element);
								$('#venueData').append($element);

								availCheckPartnersValue.removeOption(data.planners[0].plannerId);
								availCheckPartnersValue.addOption(venueList);
								availCheckPartnersValue.disable();
								//availCheckPartnersValue.refreshOptions();

							} else {
								availCheckPartnersValue.enable();
								availCheckPartnersValue.addOption(availCheckPartnersResults);
							}

							if (data.defaultPartnerId) {
								$('#defaultId').removeData('id');
								$('#defaultId').attr("data-id", data.defaultPartnerId);
								//availCheckPartnersValue.setValue(availCheckPartnersValue.search(id).items[0].id);
							}
							var id = $('#defaultId').data('id');
							if (id) {
								availCheckPartnersValue.setValue(id);
							}

						} else {
							$('#availCheckPartners').append("<div> Planners not found </div>");
						}

						$('#imageLinks').empty();
						if (data.imageUrls) {

							$.each(data.imageUrls, function (key, val) {

								var imgElement = "<div id='img" + val.imageId + "' class='in-blk slide-image'>" +
									"<a href='" + val.url + "' target='_blank' class='booking-gallery-link'>" +
									"<img src='" + val.url + "' class='no-pad-r no-mar-r gallery-item' style='vertical-align: text-top;'>" +
									"</a><a>" +
									"<div class='btn no-pad-l no-pad-b remove-img' id='" + val.imageId + "'>" +
									"<i class='glyphicon no-pad-l glyphicon-remove-circle'></i>" +
									"</div>	</a></div>";

								$('#imageLinks').append(imgElement);
							});
							$('.remove-img').click(function () {
								var id = $(this).attr('id');
								removedImages.push(id);
								$('#img' + id).remove();
							});
						}
						$("#opsAvailCheckDetails").modal("show");

					} else {
						var $errorMsg = 'Some error occurred while submitting data. Kindly refresh the page and try again';
						if (data.errorMsg) {
							$errorMsg = data.errorMsg;
						}
						window.showNotyError($errorMsg);
					}
				},
				error: function (data) {
					window.showNotyError("Some error occurred, please try again");
				}
			});

		});

		//image uploads

		var fileList = [];
		var fileName = [];

		var $modal = $('.check-upload-type');

		var $uploadType = $modal.find('input[name = uploadType]');

		$uploadType.click(function (event) {
			var value = $(this).val();

			// upload from link
			if (value == 1) {
				$modal.find('.pc-input').addClass('hide');
				$modal.find('.link-input').removeClass('hide');
			}

			// upload from pc
			else if (value == 2) {
				$modal.find('.link-input').addClass('hide');
				$modal.find('.pc-input').removeClass('hide');
			}
		});

		function showImageSection() {
			$('.new-option-images').removeClass('hide');
		}

		$('#uploadImageLink').on('keypress', function (e) {
			if (e.which == 13) {
				if (confirm(" add link")) {
					var link = $('#uploadImageLink').val();

					var imgElement = "<div class='in-blk remove-image slide-image'>" +
						"<a href='" + link + "' target='_blank' class='booking-gallery-link'>" +
						"<img src='" + link + "' class='no-pad-r no-mar-r gallery-item img-links' style='vertical-align: text-top;'>" +
						"</a>" +
						"<div class='btn no-pad-l no-pad-b delete-link'>" +
						"<i class='glyphicon no-pad-l glyphicon-remove-circle'></i>" +
						"</div></div>";

					$("#linkImages").append(imgElement);

					$('#uploadImageLink').val('');

					$(".delete-link").off('click').on('click', function () {
						$(this).parent(".remove-image").remove();
					});
					showImageSection();
				}
			}
		});

		$('.btn-add-upload-link').on('click', function () {

			var link = $('#uploadImageLink').val();

			var imgElement = "<div class='in-blk remove-image'>" +
				"<a href='" + link + "' target='_blank' class='booking-gallery-link'>" +
				"<img src='" + link + "' class='no-pad-r no-mar-r gallery-item img-links' style='vertical-align: text-top;'>" +
				"</a>" +
				"<div class='btn no-pad-l no-pad-b delete-link'>" +
				"<i class='glyphicon no-pad-l glyphicon-remove-circle'></i>" +
				"</div></div>";

			$("#linkImages").append(imgElement);

			$('#uploadImageLink').val('');

			$(".delete-link").off('click').on('click', function () {
				$(this).parent(".remove-image").remove();
			});
			showImageSection();

		});

		function removeFromList(ele, index) {

			for (var i = 0; i < fileList.length; i++) {
				if (fileName[i] === index) {
					$(ele).parent(".remove-image").remove();
					delete fileList[i];
					break;
				}
			}
		}

		$(document).ready(function () {

			if (window.File && window.FileList && window.FileReader) {
				$("#files").on("change", function (e) {
					var files = e.target.files,
						filesLength = files.length;

					for (var i = 0; i < filesLength; i++) {
						var f = files[i];
						fileList.push(f);
						fileName.push(f.name);

						var fileReader = new FileReader();
						fileReader.fileName = f.name;

						fileReader.onload = (function (e) {
							var file = e.target;

							var imgElement = "<div class='in-blk remove-image slide-image' style='max-height: 200px'>" +
								"<a href='" + e.target.result + "' target='_blank' class='booking-gallery-link'>" +
								"<img src='" + e.target.result + "' class='no-pad-r no-mar-r mar-l-10 gallery-item' style='vertical-align: text-top;'>" +
								"</a>" +
								"<div class='btn no-pad-l no-pad-b delete-src-image' name='" + file.fileName + "' >" +
								"<i class='glyphicon no-pad-l glyphicon-remove-circle'></i>" +
								"</div>" +
								"<div style='text-align: center;font-size: 12px;white-space: nowrap;width: 120px;overflow: hidden;text-overflow: ellipsis;'> " + file.fileName + "</div>" +
								"</div>";

							$("#uploadedImages").append(imgElement);

							$('.delete-src-image').off('click').on('click', function () {
								var name = $(this).attr('name');
								removeFromList(this, name);
							});

						});

						fileReader.readAsDataURL(f);
					}

					showImageSection();
				});

			} else {
				alert("Your browser doesn't support to File API")
			}

		});

		$("#opsAvailCheckDetails").on('keypress', function (e) {
			if (e.which == 13) {
				e.preventDefault();
			}
		});

		// submit the check

		$("#submitAvailCheck").click(function () {

			var id = $(this).data('id');
			var url = $(this).data('url');
			var $plannerOptionsData = [];
			var $plannerUserId = [];

			$(this).attr('disabled', true);

			var $data = new FormData();
			var $optionsData = [];

			$plannerString = $('#availCheckPartners').val();

			if ($plannerString) {
				var $plannerOptions = $plannerString.split(',');
				$.each($plannerOptions, function (i, val) {

					var listItems = $("#availCheckPartnersData li");
					var listItems1 = $("#venueData li");
					var found = 0;

					listItems1.each(function (idx, li) {
						var id = $(li).data('id');
						if (id == val) {
							$plannerOptionsData.push({
								'id': val,
								'userId': $(li).data("userid")
							});
							found = 1;
						}
					});
					if (found == 0) {
						listItems.each(function (idx, li) {
							var id = $(li).data('id');
							if (id == val) {
								$plannerOptionsData.push({
									'id': val,
									'userId': parseInt($(li).data("userid"))
								});
								found = 1;
							}
						});
					}
				});
			}

			$optionsData.push({
				'availCheckId': id,
				'selectedPartners': $plannerOptionsData,
				'info': $("#getCheckInfo").val(),
				'budget': $("#budget").val(),
				'OptionType': $("#optionType").val(),
				'partyTime': $("#partyDateTime").val()
			});

			var linkImages = [];

			$(".img-links").each(function () {
				imgsrc = this.src;
				linkImages.push(imgsrc);
			});

			fileList.forEach(function (item) {

				var file = item;
				$data.append('photos[]', file, file.name);

			});

			$data.append('optionsData', JSON.stringify($optionsData));
			//$data.append('imageLinks',JSON.stringify(images));
			$data.append('uploadedImageLinks', JSON.stringify(linkImages));

			$data.append('removedImageLinks', JSON.stringify(removedImages));

			$.ajax({
				url: url,
				type: "POST",
				dataType: "JSON",
				data: $data,
				processData: false,
				contentType: false,
				success: function (data) {
					if (data.success) {
						window.showNotySuccess("Availability Check created Successfully", 2000);
						setTimeout(function () {
							window.location.reload();
						}, 2000);
					} else {
						$("#submitAvailCheck").removeAttr('disabled');
						var $errorMsg = 'Some error occurred while submitting data. Kindly refresh the page and try again';
						if (data.errorMsg) {
							$errorMsg = data.errorMsg;
							if (data.imageError) {
								$.each(data.imageName, function (key, value) {
									$errorMsg = data.errorMsg;
									var img = $("div[name*='" + value + "']");
									img.parent('.remove-image').css({"opacity": "0.1", "border": "3px solid red"});

									$errorMsg = $errorMsg + " " + value;

									window.showNotyError($errorMsg);
								});
							} else {
								window.showNotyError($errorMsg);
							}
						} else {
							window.showNotyError($errorMsg);
						}
					}
				},
				error: function (data) {
					window.showNotyError("Some error occurred, please try again", 2000);
					$("#submitAvailCheck").removeAttr('disabled');
				}
			});

		});


	</script>

@endsection