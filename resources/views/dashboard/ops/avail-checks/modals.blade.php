<div id="opsAvailCheckDetails" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content mar-t-10">
			<form id=" " action="" method="" enctype="multipart/form-data">
				<div class="modal-header">
					<h4 class="modal-title">Fill the details to ask availability check</h4>
				</div>
				<div class="modal-body">
					<div class="hide alert-danger pad-6 mar-b-15 " id="errorBox"></div>
					<div class="">
						<div class="col-sm-4 no-pad-l">
							<label class="no-pad"> Select partner </label>
						</div>

						<div class="col-sm-8 no-pad-l">
							<input type="text" name="availCheckPartners" id="availCheckPartners" class="form-control">
						</div>

						<ul id="availCheckPartnersData" class="hide">
							@foreach ($data['allPartners'] as $planner)
								<li data-id="{{ $planner['plannerId'] }}"
										data-name="{{ $planner['plannerName'] }}"
										data-person="{{ $planner['plannerPerson'] }}"
										data-code="{{ $planner['plannerCode'] }}"
										data-userid= "{{$planner['plannerUserId']}}">
								</li>
							@endforeach
						</ul>

						<ul id="venueData" class="hide"></ul>

						<div id="defaultId" data-id="0"></div>
						<div class="clearfix"></div>

						<div class="form-group mar-t-20">
							<div class="col-sm-4 no-pad-l">
								<label class="no-pad">Customer Name</label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<input id="customerName" disabled class="form-control">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group mar-t-20">
							<div class="col-sm-4 no-pad-l no-pad-l">
								<label class="no-pad">Option</label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<input id="option" disabled class="form-control">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group mar-t-20 hide">
							<div class="col-sm-4 no-pad-l no-pad-l">
								<label class="no-pad">Option Type</label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<input id="optionType" disabled class="form-control">
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="form-group mar-t-20">
							<div class="col-sm-4 no-pad-l no-pad-l">
								<label class="no-pad">Budget</label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<input type="text" id="budget" class="form-control">
							</div>
							<div class="clearfix"></div>
						</div>


						<div class="form-group">
							<div class="col-sm-4 no-pad-l">
								<label>Party date time </label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<input type="text" id="partyDateTime" class="form-control" disabled>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="form-group">
							<div class="col-sm-4 no-pad-l">
								<label> Inclusions </label>
							</div>
							<div class="col-sm-8 no-pad-l">
								<textarea class="form-control" rows="4" name="check-info" id="getCheckInfo" placeholder="Enter information" style="width: 100%;"></textarea>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="new-option-images hide">
							<div class="mar-t-10 mar-l-10">
								<label> Uploaded Images from Links/Pc </label>
							</div>
							<div class="panel panel-default" style="width: 100%">

								<div id="linkImages" class="link-images">
								</div>
								<div id="uploadedImages" class="mar-l-20 mar-b-10">

								</div>
							</div>
						</div>


						<div class="panel panel-default" style="width:100%;">
							<div class="mar-l-20 mar-t-10">
								<h4 class="modal-title"> Upload New Images<span
											class="font-13 text-info"> (Choose one way to upload the images)</span>
								</h4>
							</div>
							<hr>
							<div class="check-upload-type">
								<div class="text-black text-center mar-b-10 mar-t-10 ">
									<div class="col-sm-6">
										<label class="mar-r-10"><input type="radio" name="uploadType" value="1" checked> Upload From Link</label>
									</div>
									<div class="col-sm-6">
										<label class="mar-r-10"><input type="radio" name="uploadType" value="2"> Upload From Image</label>
									</div>
									<div class="clearfix"></div>
								</div>

								<div class="form-group pad-10 link-input">
									<div class="alert alert-info no-pad">
										<div class="mar-t-10 pad-t-10">
											<div class="no-pad text-center pad-r-3 pad-l-3" style="width: auto">
												<input type="url" id="uploadImageLink" placeholder="Enter valid image url" class="form-control image-links">
											</div>
											<div class="mar-t-10 text-center pad-b-10">
												<div class="btn btn-info btn-add-upload-link">
													<i class="glyphicon glyphicon-cloud-upload"></i> Add Image Link
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group pad-10 hide pc-input">
									<div class="alert alert-info no-pad">
										<div class="form-group no-mar-b" id="imageCheck">
											<div class="gal-type-image">
												<div class="text-center pad-t-10 pad-b-10">
													<label for="files" class="btn btn-info">
														<i class="glyphicon glyphicon-cloud-upload"></i> Click To Add Images
													</label>
													<input type="file" id="files" accept="image/png, image/jpeg, image/jpg, image/JPG" class="hide" name="files[]" multiple/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="option-images">
							<div class="mar-t-10 mar-l-10">
								<label> Existing Option Images(from CRM) </label>
							</div>
							<div class="panel panel-default" style="width: 100%">
								<div class="existing-images" id="imageLinks">

								</div>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" id="btnBackPostAvailCheck" data-dismiss="modal"><i
									class="glyphicon glyphicon-remove"></i> Close
						</button>
						<button type="button" id="submitAvailCheck" class="btn btn-success" data-url="{{route("ops-dashboard.avail-checks.sendCheck")}}"><i
									class="glyphicon glyphicon-floppy-save"></i> Ask Avail Check
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>