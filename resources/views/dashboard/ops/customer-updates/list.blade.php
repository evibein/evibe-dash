@extends("dashboard.ops.base")

@section("listData")
	<div class="data-sec mar-t-10 pad-b-20">
		@if(isset($data['customerUpdates']) && count($data['customerUpdates']))
			<table id="customerUpdatesData" class="table table-bordered table-responsive table-hover">
				<thead class="text-info">
				<tr>
					<th>Customer</th>
					<th>Party Date</th>
					<th>Query</th>
					<th>Asked At</th>
					<th>Action CTAs</th>
				</tr>
				</thead>
				<tbody>
				@foreach($data['customerUpdates'] as $query)
					<tr>
						<td>
							<a href="/tickets/{{ $query['ticketId'] }}/bookings" target="_blank">
								<div>
									@if(isset($query['customerName']) && $query['customerName'])
										{{ ucfirst($query['customerName']) }}
									@else
										--
									@endif
								</div>
								<div>
									@if(isset($query['customerPhone']) && $query['customerPhone'])
										{{ ucfirst($query['customerPhone']) }}
									@else
										--
									@endif
								</div>
								<div>
									@if(isset($query['customerEmail']) && $query['customerEmail'])
										{{ ucfirst($query['customerEmail']) }}
									@else
										--
									@endif
								</div>
							</a>
						</td>
						<td>
							@if(isset($query['partyDateTime']) && $query['partyDateTime'])
								{{ $query['partyDateTime'] }}
							@else
								--
							@endif
						</td>
						<td>
							@if(isset($query['query']) && $query['query'])
								<div>{{ $query['query'] }}</div>
							@else
								--
							@endif
							@if(isset($query['queryComments']) && $query['queryComments'])
								<div class="font-12 text-italic mar-t-5">({!! $query['queryComments'] !!})</div>
							@endif
						</td>
						<td>
							@if(isset($query['queryAskedAt']) && $query['queryAskedAt'])
								{{ $query['queryAskedAt'] }}
							@else
								--
							@endif
						</td>
						<td class="text-center">
							<div>
								<a class="btn btn-primary query-resolve-btn btn-xs mar-t-10" data-url="/tickets/action/resolve-query/{{ $query['queryId'] }}">
									<i class="glyphicon glyphicon-thumbs-up"></i> Mark As Resolved
								</a>
							</div>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		@else
			<div class="alert-danger text-center text-bold pad-10">
				<i class="glyphicon glyphicon-eye-open"></i>
				No new orders updates were found.
			</div>
		@endif
	</div>
@endsection

@section('javascript')
	@parent
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {

			$('#customerUpdatesData').DataTable({
				"fixedHeader": true,
				"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]]
			});

			$('.query-resolve-btn').on('click', function () {
				var confirmMessage = "Are you sure?";
				if (!confirm(confirmMessage)) {
					return false;
				}

				$.ajax({
					url: $(this).data('url'),
					type: 'POST',
					dataType: 'json',
					data: {}
				}).done(function (data, textStatus, jqXHR) {
					if (data.success === true) {
						var $successMsg = "Query resolved successfully";
						if (data.successMsg) {
							$successMsg = data.successMsg;
						}
						window.showNotySuccess($successMsg);
						setTimeout(function () {
							window.location.reload();
						}, 1000);
					}
					else {
						var $errorMsg = "Some issue occurred while resolving the query. Kindly refresh the page and try again.";
						if (data.errorMsg) {
							$errorMsg = data.errorMsg;
						}
						window.showNotyError($errorMsg);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					window.showNotyError('Some error occurred while resolving the query. Kindly refresh the page.');
				});
			});

		});
	</script>
@endsection