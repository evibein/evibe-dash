@if($data['searchData'] && count($data['searchData']))
	@foreach($data['searchData'] as $data)
		<input type="hidden" id="hidTicketId" value="{{ $data['id'] }}">
		<div class="mar-t-10">
			<table class="table table-bordered no-mar-b">
				<tr>
					<td colspan="2">
						<div class="pull-left">
							<i class="glyphicon glyphicon-user"></i>
							@if (isset($data['name']) && $data['name'])
								{{ $data['name'] }}
							@else
								---
							@endif
						</div>
						<div class="pull-right">
							<i class="glyphicon glyphicon-calendar"></i>
							@if(isset($data['event_date']) && $data['event_date'])
								{{ date('d/m/Y H:i A', $data['event_date']) }}
							@else
								--
							@endif
						</div>
						<div class="clearfix"></div>
						<div class="pull-left mar-t-5">
							<i class="glyphicon glyphicon-map-marker"></i>
							@if (isset($data['city']) && $data['city'])
								{{ $data['city']['name'] }}
							@else
								---
							@endif
						</div>
						<div class="pull-right mar-t-5">
							<i class="glyphicon glyphicon-gift"></i>
							@if (isset($data['event']) && $data['event'])
								{{ $data['event']['name'] }}
							@else
								---
							@endif
						</div>
					</td>
				</tr>
			</table>
			@if(isset($data['customer_proofs']) && $data['customer_proofs'])
				<div class="customer-proofs-images-wrap pad-t-15 pad-b-15">
					@foreach($data['customer_proofs'] as $customerProof)
						@if($customerProof['deleted_at']  || $customerProof['rejected_at'] || $customerProof['approved_at'])
						@else
							<div class="col-sm-6">
								@if(isset($customerProof['type_proof']['name']))
									<div class="customer-proof-title">
										<span>{{ $customerProof['type_proof']['name'] }}</span>
										<span> - </span>
										@if(isset($customerProof['face']))
											<span>
											@if($customerProof['face'] == 1)
													Front Side
												@else
													Back Side
												@endif
											</span>
										@endif
									</div>
								@endif
								<div class="customer-proof-img-wrap mar-t-10">
									<img class="customer-proof-img" src="{{ config('evibe.gallery.host').'/ticket/'.$data['id'].'/customer-proofs/'.$customerProof['url'] }}">
								</div>
							</div>
							<div class="col-sm-8 hide">
								@if(isset($customerProof['type_proof']['name']))
									<div class="customer-proof-title">
										<span>{{ $customerProof['type_proof']['name'] }}</span>
										<span> - </span>
										@if(isset($customerProof['face']))
											<span>
											@if($customerProof['face'] == 1)
													Front Side
												@else
													Back Side
												@endif
											</span>
										@endif
									</div>
								@endif
								<div class="customer-proof-approval mar-t-5 hide">
									<div class="in-blk">
										<label><input type="radio" class="customer-proof-val" data-customer-proof-id="{{ $customerProof['id'] }}" name="customerProof-{{ $customerProof['id'] }}" value="1" checked> Accept</label>
									</div>
									<div class="in-blk mar-l-10">
										<label><input type="radio" class="customer-proof-val" data-customer-proof-id="{{ $customerProof['id'] }}" name="customerProof-{{ $customerProof['id'] }}" value="2"> Reject</label>
									</div>
								</div>
								<div class="customer-proof-comments mar-t-5 hide">
									<textarea class="form-control" name="customerProofComments-{{ $customerProof['id'] }}" placeholder="Kindly enter comments for rejection"></textarea>
								</div>
								<input type="hidden" id="customerProofType-{{ $customerProof['id'] }}" value="{{ $customerProof['type_proof_id'] }}">
								<input type="hidden" id="customerProofSide-{{ $customerProof['id'] }}" value="{{ $customerProof['face'] }}">
							</div>
						@endif
					@endforeach
					<div class="clearfix"></div>
				</div>
				<div class="customer-proof-validation-wrap pad-10">
					<div class="customer-proof-approval mar-t-5">
						<div class="in-blk">
							<label><input type="radio" class="customer-proof-val" name="customerProofStatus" value="1" checked> Accept</label>
						</div>
						<div class="in-blk mar-l-10">
							<label><input type="radio" class="customer-proof-val" name="customerProofStatus" value="2"> Reject</label>
						</div>
					</div>
					<div class="customer-proof-comments mar-t-5">
						<textarea class="form-control" name="customerProofComments" placeholder="Kindly enter comments for rejection"></textarea>
					</div>
					<div class="text-center">
						<div class="btn btn-primary mar-t-10 submit-customer-proofs-btn">Submit</div>
					</div>
				</div>
			@endif
		</div>
	@endforeach
@else
	<div class="pad-t-30">
		<div class="alert alert-danger no-mar-b">
			<span>Sorry, no order info found.</span>
		</div>
	</div>
@endif