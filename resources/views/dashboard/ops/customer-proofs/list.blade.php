@extends("dashboard.ops.base")

@section("listData")
	<div class="data-sec mar-t-10 pad-b-20">
		@if(isset($data['ticketsListData']) && count($data['ticketsListData']))
			<table id="customerProofsData" class="table table-bordered table-responsive table-hover">
				<thead class="text-info">
				<tr>
					<th>Customer</th>
					<th>Party Date</th>
					<th>Status</th>
					<th>Action CTAs</th>
				</tr>
				</thead>
				<tbody>
				@foreach($data['ticketsListData'] as $ticket)
					<tr>
						<td>
							<a href="/tickets/{{ $ticket['id'] }}/information" target="_blank">
								<div>
									@if(isset($ticket['name']) && $ticket['name'])
										{{ ucfirst($ticket['name']) }}
									@else
										--
									@endif
								</div>
								<div>
									@if(isset($ticket['phone']) && $ticket['phone'])
										{{ ucfirst($ticket['phone']) }}
									@else
										--
									@endif
								</div>
								<div>
									@if(isset($ticket['email']) && $ticket['email'])
										{{ ucfirst($ticket['email']) }}
									@else
										--
									@endif
								</div>
							</a>
						</td>
						<td>
							@if(isset($ticket['event_date']) && $ticket['event_date'])
								{{ date('d/m/Y H:i A', $ticket['event_date']) }}
							@else
								--
							@endif
						</td>
						<td>
							@if(isset($ticket['status']) && $ticket['status'])
								<div>{{ $ticket['status']['name'] }}</div>
							@else
								--
							@endif
						</td>
						<td class="text-center">
							<div>
								<div class="mar-t-10">
									<a href="#" class="customer-proofs-info-btn" data-ticket-id="{{ $ticket["id"] }}">
										<i class="glyphicon glyphicon-search mar-r-2"></i>View Customer Proofs
									</a>
								</div>
							</div>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		@else
			<div class="alert-danger text-center text-bold pad-10">
				<i class="glyphicon glyphicon-eye-open"></i>
				No new orders updates were found.
			</div>
		@endif
	</div>
@endsection

@section('javascript')
	@parent
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {

			$('#customerProofsData').DataTable({
				"fixedHeader": true,
				"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]]
			});

			$('#opsSearchBtn').on('click', function () {
				var $searchQuery = $('#opsSearchQuery').val();
				var $ticketId = "";
				searchCustomerProofs($searchQuery, $ticketId);
			});

			$('.customer-proofs-info-btn').on('click', function (event) {
				event.preventDefault();

				var $searchQuery = "";
				var $ticketId = $(this).data('ticket-id');
				searchCustomerProofs($searchQuery, $ticketId);
			});

			function searchCustomerProofs($searchQuery, $ticketId) {
				$.ajax({
					url: '/ops-dashboard/customer-proofs/search',
					type: 'GET',
					dataType: 'html',
					data: {
						query: $searchQuery,
						ticketId: $ticketId
					},
					success: function (data) {
						if (typeof data !== undefined && data) {
							$("#opsSearchData").empty().append(data);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showNotyError("Error occurred while fetching the data, Please reload the page & try again.")
					}
				})
			}

			$('#opsSearchData').on('click', '.submit-customer-proofs-btn', function () {

				var customerProofIds = [];
				var customerProofData = [];
				$('#opsSearchData .customer-proof-val').each(function (index) {
					customerProofIds.push($(this).data('customer-proof-id'));
				});
				$.unique(customerProofIds);

				$.each(customerProofIds, function (key, value) {
					var proofName = "customerProof-" + value;
					var proofCommentsName = "customerProofComments-" + value;
					var proofTypeId = "customerProofType-" + value;
					var proofSide = "customerProofSide-" + value;
					customerProofData.push({
						'id': value,
						'status': $("input[name='" + proofName + "']:checked").val(),
						'comments': $("textarea[name='" + proofCommentsName + "']").val(),
						'proofTypeId': $("#" + proofTypeId).val(),
						'proofSide': $("#" + proofSide).val()
					});
				});

				console.log(customerProofIds);
				console.log(customerProofData);

				$.ajax({
					url: "/ops-dashboard/customer-proofs/validate",
					type: 'POST',
					dataType: 'json',
					data: {
						ticketId: $('#hidTicketId').val(),
						customerProofData: customerProofData,
						customerProofStatus: $("input[name='customerProofStatus']:checked").val(),
						customerProofComments: $("textarea[name='customerProofComments']").val()
					},
					success: function (data) {
						if (data.success) {
							showNotySuccess("Validation status has been shared with the customer");
							setTimeout(function () {
								window.location.reload();
							}, 1000);
						}
						else {
							var $error = "Some Error occurred, please refresh the page";
							if(data.errorMsg) {
								$error = data.errorMsg;
							}
							showNotyError($error);
						}
					},
					error: function () {
						showNotyError("Some Error occurred, please refresh the page");
					}
				});
			});

		});
	</script>
@endsection