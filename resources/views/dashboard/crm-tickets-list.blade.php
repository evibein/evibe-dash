@if(count($data['tickets'])>0)
	<table id="newTicketsSummary" class="table table-bordered table-responsive table-hover">
		<thead class="text-info">
		<tr>
			<th>Customer Name</th>
			<th>Party Date</th>
			<th>Lead Status</th>
			<th>Source (Customer / Enquiry)</th>
			<th>Status</th>
			<th>Handler (Current / Created)</th>
			<th>City</th>
			<th>Occasion</th>
			<th>Last Update</th>
			<th>Pending Follow up</th>
		</tr>
		</thead>
		<tbody>
		@foreach($data['tickets'] as $ticket)
			@php
				$ticketUpdateComment = isset($data["ticketUpdate"][$ticket->id]) ? $data["ticketUpdate"][$ticket->id] : '';
				$activeFollowups = isset($data["activeFollowups"][$ticket->id])? $data["activeFollowups"][$ticket->id] : '';
				$typeLeadStatusOfTicket = $data['typeLeadStatus']->where('id',$ticket->lead_status_id)->first();
				$typeEnquirySourceOfTicket = $data['typeEnquirySource']->where('id',$ticket->enquiry_source_id)->first();
				$typeTicketSourceOfTicket = $data['typeTicketSource']->where('id',$ticket->source_id)->first();
				$typeTicketStatusOfTicket = $data['typeTicketStatus']->where('id',$ticket->status_id)->first();
				$handlerDetailsOfTicket = $data['handlerDetails']->where('id',$ticket->handler_id)->first();
				$createdHandlerDetailsOfTicket = $data['handlerDetails']->where('id',$ticket->created_handler_id)->first();
				$cityDetailsOfTicket = $data['cityDetails']->where('id',$ticket->city_id)->first();
				$eventTypeDetailsOfTicket = $data['eventTypeDetails']->where('id',$ticket->event_id)->first();
			@endphp
			<tr>
				<td>
					<a href="/tickets/{{ $ticket->id }}/information" target="_blank">
						<div class="text-muted font-11">[{{ date('d/m H:i', strtotime($ticket->created_at)) }}]</div>
						<div>
							@if($ticket->name)
								{{ $ticket->name }}
							@else
								Not Set
							@endif
						</div>
					</a>
					<span>
						(
						@if($ticket->calling_code)
							<span class="mar-r-2">{{ $ticket->calling_code }}</span>
						@endif
						<span>{{$ticket->phone}}</span>
						)
					</span>
					{{ $ticket->id }}
				</td>
				<td>
					@if($ticket->event_date)
						{{ date("d M, g:i a", $ticket->event_date) }}
					@else
						---
					@endif
				</td>
				<td>
					@if($typeLeadStatusOfTicket)
						{{ ucfirst($typeLeadStatusOfTicket->name) }}
					@else
						---
					@endif
				</td>
				<td>
					@if($typeEnquirySourceOfTicket)
						{{ ucfirst($typeEnquirySourceOfTicket->name) }}
					@else
						---
					@endif
					@if($typeTicketSourceOfTicket)
						/  {{ ucfirst($typeTicketSourceOfTicket->name) }}
					@else
						/ ---
					@endif
				</td>
				<td>
					@if($typeTicketStatusOfTicket)
						{{$typeTicketStatusOfTicket->name}}
					@else
						---
					@endif
				</td>
				<td>
					@if($handlerDetailsOfTicket)
						{{ucfirst($handlerDetailsOfTicket->name)}}
					@else
						---
					@endif
					/
					@if($createdHandlerDetailsOfTicket)
						{{ucfirst($createdHandlerDetailsOfTicket->name)}}
					@else
						---
					@endif

				</td>
				<td>
					@if($cityDetailsOfTicket)
						{{ucfirst($cityDetailsOfTicket->name)}}
					@else
						---
					@endif
				</td>
				<td>
					@if($eventTypeDetailsOfTicket)
						{{ucfirst($eventTypeDetailsOfTicket->name)}}
					@else
						---
					@endif
				</td>
				<td>
					@if($ticketUpdateComment)
						@php $comment = explode("~", $ticketUpdateComment); @endphp
						<div class="text-muted font-12">[{!! $comment[1] !!})]</div>
						{!! $comment[0] !!}
					@else
						---
					@endif
				</td>
				<td>
					@if($activeFollowups)
						<span class="@if ($activeFollowups < (\Carbon\Carbon::now()->timestamp)) text-danger @endif">
								{{ date("F j, Y, g:i a", $activeFollowups) }}
							</span>
					@else
						---
					@endif
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
@else
	<div class="alert-danger text-center text-bold pad-10 mar-r-10">
		<i class="glyphicon glyphicon-eye-open"></i>
		No new tickets were found.
	</div>
@endif
