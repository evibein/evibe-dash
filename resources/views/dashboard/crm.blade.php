<div class="col-md-8 col-lg-8 col-sm-8">
	<div class="text-center">
		<div>Based on recommendations (By Occasion)</div>
		<table class="table table-hover table-bordered">
			<thead class="thead-default">
			<tr>
				<td colspan="2"></td>
				<td>Sent</td>
				<td>Rated</td>
				<td>Didn't respond</td>
				<td>Avg rating</td>
			</tr>
			</thead>
			<tbody>
			<?php $thisEventDetails = null;?>
			@if($data['crmRecoData'])
				@foreach($data['crmRecoData'] as $key => $reco)
					<?php $tdCount = 1; $optionCount = 0; ?>
					@foreach($reco as $event => $item)
						<?php $tdCount++; $optionCount++; $thisEventDetails = $data['eventDetails']->where('id', $event)->first(); ?>
						<tr>
							@if($optionCount == 1)
								<td rowspan="{{ count($reco) }}">
									@foreach($data['handlers'] as $handler)
										@if($handler->id == $key)
											{{ $handler->name }}
										@endif
									@endforeach
								</td>
							@endif
							<td>
								@if($thisEventDetails)
									{{ $thisEventDetails->name }}
								@else
									{!! $event !!}
								@endif
							</td>
							<td>@if(isset($item['sent'])) {{ $item['sent'] }} @else 0 @endif</td>
							<td>@if(isset($item['rated'])) {{ $item['rated'] }} @else 0 @endif</td>
							<td>@if(isset($item['skipped'])) {{ $item['skipped'] }} @else 0 @endif</td>
							<td>@if(isset($item['rating']) && $item['rated'] != 0)
									<b>{{ $item['rating']/$item['rated'] }}</b> @else 0 @endif</td>
						</tr>
					@endforeach
				@endforeach
			@else
				<tr>
					<td colspan="6">
						N/A
					</td>
				</tr>
			@endif
			</tbody>
		</table>
	</div>
	<?php $count = 0; $thisEventDetails = null; $thisEventStatusDetails = null;?>
	@foreach($data['crmBookingData'] as $crmData)
		<div class="text-center">
			<?php $count++ ?>
			<div>
				Based on bookings (@if($count == 1) By Occasion) @else By Source) @endif
			</div>
			<table class="table table-bordered table-hover">
				<thead>
				<tr>
					<td colspan="2"></td>
					<td>Handled</td>
					<td>Booked</td>
					<td>Conversion Rate</td>
				</tr>
				</thead>
				<tbody>
				<?php $tdCount = 1; $thisStatusDetails = null; $thisSourceDetails = null;?>
				@if($crmData)
					@foreach($crmData as $key => $reco)
						<?php $optionCount = 0; ?>
						@foreach($reco as $event => $item)
							<?php $tdCount++; $optionCount++;
							$thisSourceDetails = $data['sourceDetails']->where('id', $event)->first();
							$thisStatusDetails = $data['statusDetails']->where('id', $event)->first();
							?>
							<tr>
								@if($optionCount == 1)
									<td rowspan="{{ count($reco) }}">
										@foreach($data['handlers'] as $handler)
											@if($handler->id == $key)
												{{ $handler->name }}
											@endif
										@endforeach
									</td>
								@endif
								<td>
									@if($thisSourceDetails)
										{{$thisSourceDetails->name}}
									@elseif($thisStatusDetails)
										{{$thisStatusDetails->name}}
									@endif
								</td>
								<td>@if(isset($item['started'])) {{ $item['started'] }} @else 0 @endif</td>
								<td>@if(isset($item['booked'])) {{ $item['booked'] }} @else 0 @endif</td>
								<td>@if(isset($item['started']) && isset($item['booked']) && $item['started'] != 0)
										<b>{{ ($item['booked']/$item['started'])*100 }}</b> @else 0 @endif</td>
							</tr>
						@endforeach
					@endforeach
				@else
					<tr>
						<td colspan="5">
							N/A
						</td>
					</tr>
				@endif
				</tbody>
			</table>
		</div>
	@endforeach
</div>