<div class="col-sm-6 col-sm-offset-3 text-center font-12">
	<table class="table table-responsive table-bordered text-center table-condensed table-fixed">
		<tr>
			<td class="alert-danger lead-status-metric-wrap" data-lead-status-id="{{ config("evibe.lead_status.hot") }}">
				<div class="text-bold font-22">
					@if($data['totalHotLeads'] > 0)
						{{$data['totalHotLeads']}}
					@else{{ "0" }}@endif
				</div>
				<div class="pad-t-3 in-blk">HOT</div>
			</td>
			<td class="alert-warning lead-status-metric-wrap" data-lead-status-id="{{ config("evibe.lead_status.medium") }}">
				<div class="text-bold font-22">
					@if($data["totalMediumLeads"] > 0)
						{{ $data["totalMediumLeads"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="pad-t-3 in-blk">MEDIUM</div>
			</td>
			<td class="alert-info lead-status-metric-wrap" data-lead-status-id="{{ config("evibe.lead_status.cold") }}">
				<div class="text-bold font-22">
					@if($data["totalColdLeads"] > 0)
						{{ $data["totalColdLeads"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="pad-t-3 in-blk">COLD</div>
			</td>
			<td class="lead-status-metric-wrap" data-lead-status-id="notSet">
				<div class="text-bold font-22">
					@if($data["totalNotSetLeads"] > 0)
						{{ $data["totalNotSetLeads"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="pad-t-3 in-blk">NOT SET</div>
			</td>
		</tr>
	</table>
</div>
<div class="col-sm-12 text-center font-12">
	<table class="table table-responsive table-bordered text-center table-condensed table-fixed">
		<tr>
			<td class="ticket-status-metric-wrap alert-success"
					data-ticket-status-id="{{ config("evibe.status.booked") }}">
				<div class="text-bold font-22">
					@if($data['countOfBookedTickets'] > 0)
						{{$data['countOfBookedTickets'] }}
						<small>
							{{"(". round(($data['countOfBookedTickets']/$data["countOfNewTickets"])*100 , 0) . "%)"}}
						</small>
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">
					BOOKED
					<span class="glyphicon glyphicon-info-sign" title="(Booked / Total New Tickets)"></span>
				</div>
			</td>
			<td class="ticket-status-metric-wrap"
					data-ticket-status-id="{{ config("evibe.status.enquiry") }}">
				<div class="text-bold font-22">
					@if($data["countOfJustEnquiryTickets"] > 0)
						{{ $data["countOfJustEnquiryTickets"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">JUST ENQUIRY</div>
			</td>
			<td class="ticket-status-metric-wrap"
					data-ticket-status-id="{{ config("evibe.status.no_response") }}">
				<div class="text-bold font-22">
					@if($data["countOfNoResponseTickets"] > 0)
						{{ $data["countOfNoResponseTickets"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">NO RESPONSE</div>
			</td>
			<td>
				<div class="text-bold font-22">
					@if($data['countOfOrderProcessedTickets'] != 0)
						{{ $data['totalProcessedBookingsCount'] }}
						<small>
							({{ round(($data['totalProcessedBookingsCount']/$data['countOfOrderProcessedTickets'])*100, 0) }}%)
						</small>
					@else
						--
					@endif
				</div>
				<div class="in-blk pad-t-3">
					OP RATIO
					<span class="glyphicon glyphicon-info-sign" title="(Processed / Booked)"></span>
				</div>
			</td>
			<td class="ticket-status-metric-wrap"
					data-ticket-status-id="{{ config("evibe.status.initiated") }}">
				<div class="text-bold font-22">
					@if($data["countOfInitiatedTickets"] > 0)
						{{ $data["countOfInitiatedTickets"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">INITIATED</div>
			</td>
			<td class="ticket-status-metric-wrap"
					data-ticket-status-id="{{ config("evibe.status.followup") }}">
				<div class="text-bold font-22">
					@if($data["countOfFollowUpTickets"])
						{{ $data["countOfFollowUpTickets"] }}
						<small>
							{{ "(" . round(($data["countOfFollowUpTickets"]/$data["countOfNewTickets"])*100, 0) ."%)" }}
						</small>
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">
					FOLLOWUP
					<span class="glyphicon glyphicon-info-sign"
							title="(Total Followup / Total New Tickets)"></span>
				</div>
			</td>
			<td>
				<div class="text-bold font-22">
					@if($data["countOfNewTickets"])
						{{ $data["countOfNewTickets"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">TOTAL NEW</div>
			</td>
		</tr>
	</table>
</div>
<div class="clearfix"></div>