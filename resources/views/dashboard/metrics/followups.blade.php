<div class="col-sm-6 col-sm-offset-3 text-center font-12">
	<table class="table table-responsive table-bordered text-center table-condensed table-fixed">
		<tr>
			<td class="alert-danger lead-status-metric-wrap"
					data-lead-status-id="{{ config("evibe.lead_status.hot") }}">
				<div class="text-bold font-22">
					@if($data['totalHotLeads'] > 0)
						{{ $data['totalHotLeads'] }}
					@else{{ "0" }}@endif
				</div>
				<div class="pad-t-3 in-blk">
					HOT
					<span class="glyphicon glyphicon-info-sign"
							title="(Pending / Total)"></span>
				</div>
			</td>
			<td class="alert-warning lead-status-metric-wrap"
					data-lead-status-id="{{ config("evibe.lead_status.medium") }}">
				<div class="text-bold font-22">
					@if($data["totalMediumLeads"] > 0)
						{{$data["totalMediumLeads"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="pad-t-3 in-blk">
					MEDIUM
					<span class="glyphicon glyphicon-info-sign"
							title="(Pending / Total)"></span>
				</div>
			</td>
			<td class="alert-info lead-status-metric-wrap"
					data-lead-status-id="{{ config("evibe.lead_status.cold") }}">
				<div class="text-bold font-22">
					@if($data["totalColdLeads"] > 0)
						{{ $data["totalColdLeads"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="pad-t-3 in-blk">COLD</div>
			</td>
			<td class="lead-status-metric-wrap" data-lead-status-id="notSet">
				<div class="text-bold font-22">
					@if($data["totalNotSetLeads"] > 0)
						{{ $data["totalNotSetLeads"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="pad-t-3 in-blk">NOT SET</div>
			</td>
		</tr>
	</table>
</div>
<div class="col-sm-12 text-center font-12">
	<table class="table table-responsive table-bordered text-center table-condensed table-fixed">
		<tr>
			<td class="ticket-status-metric-wrap alert-success"
					data-ticket-status-id="{{ config("evibe.status.booked") }}">
				<div class="text-bold font-22">
					@if($data['countOfBookedTickets'] > 0)
						{{ $data['countOfBookedTickets']}}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">BOOKED</div>
			</td>
			<td class="ticket-status-metric-wrap"
					data-ticket-status-id="{{ config("evibe.status.enquiry") }}">
				<div class="text-bold font-22">
					@if($data["countOfJustEnquiryTickets"] > 0)
						{{ $data["countOfJustEnquiryTickets"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">JUST ENQUIRY</div>
			</td>
			<td class="ticket-status-metric-wrap"
					data-ticket-status-id="{{ config("evibe.status.no_response") }}">
				<div class="text-bold font-22">
					@if($data["countOfNoResponseTickets"] > 0)
						{{ $data["countOfNoResponseTickets"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">NO RESPONSE</div>
			</td>
			<td class="ticket-status-metric-wrap"
					data-ticket-status-id="{{ config("evibe.status.confirmed") }}">
				<div class="text-bold font-22">
					@if($data['countOfOrderProcessedTickets'] != 0)
						{{ $data['countOfOrderProcessedTickets'] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">PROCESSED</div>
			</td>
			<td class="ticket-status-metric-wrap no-pad-l no-pad-r"
					data-ticket-status-id="{{ config("evibe.status.followup") }}">
				<div class="text-bold font-22">
					<span class="text-danger">{{ $data["countOfPendingFollowUps"] }}</span>
					{{ "/" }}
					{{ $data["countOfTodayFollowUps"] }}
				</div>
				<div class="in-blk pad-t-3">
					FOLLOWUP
					<span class="glyphicon glyphicon-info-sign"
							title="(Pending / Remaining)"></span>
				</div>
			</td>
			<td class="ticket-status-metric-wrap"
					data-ticket-status-id="{{ config("evibe.status.initiated") }}">
				<div class="text-bold font-22">
					@if($data["countOfInitiatedTickets"] > 0)
						{{ $data["countOfInitiatedTickets"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">INITIATED</div>
			</td>
			<td>
				<div class="text-bold font-22">
					@if($data["countOfReturnTickets"])
						{{ $data["countOfReturnTickets"] }}
					@else{{ "0" }}@endif
				</div>
				<div class="in-blk pad-t-3">TOTAL FOLLOWUPS</div>
			</td>
		</tr>
	</table>
</div>
<div class="clearfix"></div>