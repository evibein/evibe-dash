@extends("layout.base")

@section('content')
	<div class="mar-t-20">
		<h3 class="text-center">New Tickets</h3>
		<div class="new-tickets-section"></div>
	</div>
	<hr>
	<div class="mar-t-20">
		<h3 class="text-center">Followups</h3>
		<div class="followups-section"></div>
	</div>
	<hr>
	<div class="mar-t-20">
		<h3 class="text-center">In Progress</h3>
		<div class="inprogress-section"></div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			updateMetricsData();

			setInterval(function () {
				updateMetricsData();
			}, 120000);

			function updateMetricsData() {
				$.ajax({
					type: 'POST',
					url: '/metrics/data/new-tickets',
					dataType: 'html',
					success: function (data) {
						$('.new-tickets-section').empty().append(data)
					},
					fail: function () {
						window.showNotyError("Some error occurred while loading the metrics data of tickets");
					}
				});

				$.ajax({
					type: 'POST',
					url: '/metrics/data/followups',
					dataType: 'html',
					success: function (data) {
						$('.followups-section').empty().append(data)
					},
					fail: function () {
						window.showNotyError("Some error occurred while loading the metrics data of tickets");
					}
				});

				$.ajax({
					type: 'POST',
					url: '/metrics/data/in-progress',
					dataType: 'html',
					success: function (data) {
						$('.inprogress-section').empty().append(data)
					},
					fail: function () {
						window.showNotyError("Some error occurred while loading the metrics data of tickets");
					}
				});
			}
		});
	</script>
@endsection