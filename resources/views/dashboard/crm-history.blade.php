@extends('layout.base')

@section('content')
	<div class="panel panel-default page-content mar-l-10 mar-r-10">
		<div class="panel-body">
			<div class="title-sec no-pad">
				<div class="text-center">
					<div class="btn-group" role="group" aria-label="...">
						<a class="btn btn-default" href="{{route('crmDashBoard.list')}}">Summary</a>
						<a class="btn btn-default active" href="{{route('crmDashBoard.history')}}">History</a>
						<a class="btn btn-default hide" href="{{route('crmDashBoard.metrics')}}">Metrics</a>
					</div>
				</div>
				<div class="pull-left">
					<div class="font-20 pad-t-10">Showing history from {{$data['humanReadableStartDate']}} to {{$data['humanReadableEndDate']}}</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-2 col-md-2 col-xs-2 col-lg-2">

					<h4 class="in-blk pull-left pad-t-10">
						Filter Options
					</h4>
					<div class="in-blk filter-date-btn-wrap pad-t-10 pad-l-5">
						@if($data['filterReset'])
							<a class="btn btn-xs btn-warning" href="{{ route('crmDashBoard.history') }}">Reset</a>
						@endif
					</div>
					<div class="clearfix"></div>
					<div class="filter-group ls-none no-mar">

						<div>
							<div class="filter-title pull-left">Select Dates</div>
							<div class="text-center pad-t-10">
								<input type="text"
										class="form-control filter-anlytics-date filter-anlytics-creation-start-time text-center"
										id="startTime"
										value="{{date('d-m-Y H:i',$data['startDate'])}}">
							</div>
						</div>

						<div>
							<div class="text-center text-bold filter-title">To</div>
							<div class="text-center pad-b-10">
								<input type="text"
										class="form-control filter-anlytics-date filter-anlytics-creation-end-time text-center"
										value="{{date('d-m-Y H:i',$data['endDate'])}}" id="endTime">
							</div>
						</div>
						<div class="text-center pad-t-10 hide">
							<div class="filter-title pull-left">By Status</div>
							<select class="form-control" name="ticketStatusId" id="ticketStatusId">
								<option value="all">All</option>
								@foreach($data['typeTicketStatus'] as $key=>$value)
									<option value="{{$key}}"
											@if(request()->input('ticketStatusId')== $key)selected @endif>{{$value}}</option>
								@endforeach
							</select>
						</div>

						<div class="text-center pad-t-10 hide">
							<div class="filter-title pull-left">By handler</div>
							<select class="form-control" name="handlerId" id="handlerId">
								<option value="all">All Handlers</option>
								@foreach($data['crmAndAdminDetails'] as $key=>$value)
									<option value="{{$key}}"
											@if(request()->input('handlerId')== $key)selected @endif>{{$value}}</option>
								@endforeach
							</select>
						</div>

						<div class="text-center filter-date-btn-wrap">
							<a class="btn btn-xs btn-primary btn-filter-by-cd">
								<button class="btn btn-xs btn-primary">FILTER</button>
							</a>
						</div>
					</div>
					<div class="text-muted pad-t-3">
						<i class="glyphicon glyphicon-info-sign"></i>
						Difference of dates should not be more than a week
					</div>
				</div>
				<div class="col-sm-10 col-md-10 col-xs-10 col-lg-10 pad-t-10">
					@if($data['tickets'])
						<table class="table table-hover table-responsive table-bordered table-hover text text-center"
								id="crmDashhistoryTable">
							<thead class="text text-info text-center">
							<tr>
								<th>Ticket Details</th>
								<th>Last Updated At</th>
								<th>Status</th>
								<th>Handler</th>
								<th>Update Source</th>
								<th>Update Comment</th>
							</tr>
							</thead>
							<tbody>
							@foreach($data['tickets'] as $key => $ticket)
								@php
									$updateCount = 0; $totalUpdateCount = count($ticket);
									$keyForTicketDetails = array_search($key, array_column($data['ticketDetails'],'id'));
									$ticketDetails = $keyForTicketDetails ? $data['ticketDetails'][$keyForTicketDetails] : '';
								@endphp
								@if($ticketDetails)
									@foreach($ticket as $ticketUpdate)
										<tr>
											@php
												$typeTicketStatusName = isset($data['typeTicketStatus'][$ticketUpdate['status_id']])
												? $data['typeTicketStatus'][$ticketUpdate['status_id']] : '';
												$handlerName = isset($data['crmAndAdminDetails'][$ticketUpdate['ticket_updater']])
												?$data['crmAndAdminDetails'][$ticketUpdate['ticket_updater']]:'';
												$updateCount = $updateCount+1;
											@endphp
											@if($updateCount == 1)
												<td rowspan="{{ $totalUpdateCount }}">
													<div class="text text-center pad-t-10">
														<a href="/tickets/{{ $key }}/information" target="_blank">
															@if($ticketDetails["name"])
																<div>
																	<i class="glyphicon glyphicon-user"></i>
																	{{ $ticketDetails['name'] }}
																</div>
															@else
																Not Set
															@endif
														</a>
														<div class="pad-t-5">
															<i class="glyphicon glyphicon-time"></i>
															@if($ticketDetails['event_date'])
																{{date('j-M-y, g:i A',$ticketDetails['event_date'])}}
															@else
																---
															@endif
														</div>
														<div class="pad-t-5">
															<i class="glyphicon glyphicon-phone"></i>
															@if($ticketDetails['phone'])
																{{$ticketDetails['phone']}}
															@else
																---
															@endif
														</div>
														<div class="pad-t-5">
															<i class="glyphicon glyphicon-list"></i>
															@if($ticketDetails['status_id'])
																@if(isset($data['typeTicketStatus'][$ticketDetails['status_id']]))
																	{{ $data['typeTicketStatus'][$ticketDetails['status_id']] }}
																@else
																	---
																@endif
															@else
																Not Set
															@endif
														</div>
														<div class="pad-t-5">
															<i class="glyphicon glyphicon-eye-open"></i>
															@if($ticketDetails['handler_id'])
																@if(isset($data['crmAndAdminDetails'][$ticketDetails['handler_id']]))
																	@php
																		$fullNameOfHandler = explode(" ",$data['crmAndAdminDetails'][$ticketDetails['handler_id']]);
																	@endphp
																	{{$fullNameOfHandler[0]}}
																@else
																	Non CRM member
																@endif
															@else
																Not Set
															@endif
														</div>
													</div>
												</td>
											@endif
											<td>
												<span class="text-center">{{ date('j-M-y, g:i A',strtotime($ticketUpdate["updated_at"])) }}</span>
											</td>
											<td>
												{{ $typeTicketStatusName }}
											</td>
											<td>
												@if($handlerName)
													@php
														$fullNameOfHandler = explode(" ",$handlerName);
													@endphp
													{{ $fullNameOfHandler[0] }}
												@else
													{{'Auto'}}
												@endif</td>
											<td>
												@if($ticketUpdate["type_update"])
													{{ $ticketUpdate["type_update"] }}
												@else
													{{'---'}}
												@endif</td>
											<td>
												<div class="text-left">
													@if($ticketUpdate["comments"])
														{!! $ticketUpdate["comments"] !!}
													@else
														{{'---'}}
													@endif
												</div>
											</td>
										</tr>
									@endforeach
								@endif
								@if($updateCount == $totalUpdateCount)
									<tr>
										<td colspan="6" class="alert alert-info mar-t-10"></td>
									</tr>
								@endif
							@endforeach
							</tbody>
						</table>
					@else
						<div class="text text-info text-bold font-24 text-center">
							<i class="glyphicon glyphicon-search"></i>
							No tickets were there, you can apply filters.
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			initPlugins();

			function initPlugins() {
				$('.filter-anlytics-creation-start-time, ' +
					'.filter-anlytics-creation-end-time').datetimepicker({
					timepicker: true,
					scrollInput: false,
					scrollMonth: false,
					closeOnDateSelect: false,
					format: "d-m-Y H:i",
					formatTime: "H:i",
				});
			}

			$('.btn-filter-by-cd').click(function (event) { // filter by party date
				event.preventDefault();
				var startDate = $('.filter-anlytics-creation-start-time').val();
				var endDate = $('.filter-anlytics-creation-end-time').val();
				var parts = startDate.split("-");
				var d1 = new Date(parts[2], parts[1] - 1, parts[0]);

				if (!startDate || !endDate) {
					showNotyError("Please select both start date & end date.")
				}
				else {
					var url = '';
					if (startDate) url = formUrl(url, 'filterChosenStartingTime', startDate, false, ['page']);
					if (endDate) url = formUrl(url, 'filterChosenEndingTime', endDate, false, ['page']);
					refreshUrl(url);
				}
			});

			var filterChosenStartingTime = $('#startTime').val();
			var filterChosenEndingTime = $('#endTime').val();

			function getQueryParams(a) {
				if (a == "") return {};
				var b = {};
				for (var i = 0; i < a.length; ++i) {
					var p = a[i].split('=');
					if (p.length != 2) continue;
					b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
				}
				return b;
			}

			function formUrl(cUrl, key, value, isIgnore, ignores) {
				cUrl = cUrl ? cUrl.substr(cUrl.indexOf('?') + 1).split('&') : window.location.search.substr(1).split('&');
				var cParams = getQueryParams(cUrl), queryString = "", loc = window.location;
				var url = loc.protocol + '//' + loc.host + loc.pathname;
				if (isIgnore) { // remove from current object
					if (cParams.hasOwnProperty(key)) {
						try {
							delete cParams[key];
						}
						catch (e) {
						}
					}
				} else cParams[key] = value;
				for (key in cParams) { // form string
					if (ignores && ignores.constructor === Array && window.indexOf.call(ignores, key) >= 0) continue;
					queryString += key + "=" + cParams[key] + "&";
				}
				if (queryString) {
					url += '?' + queryString;
					url = url.slice(0, -1); // remove trailing '&'
				}
				return url;
			}

			function refreshUrl(url) {
				window.location.href = url;
			}

			function updateUrlByFilters(event) {
				event.preventDefault();

				var ticketStatusId = $('#ticketStatusId').val();
				var filterChosenStartingTime = $('#startTime').val();
				var filterChosenEndingTime = $('#endTime').val();
				var handlerId = $('#handlerId').val();
				var url = "history?";
				var isUrlChanged = false;

				if (filterChosenStartingTime) {
					isUrlChanged = true;
					url += '&filterChosenStartingTime=' + filterChosenStartingTime;
				}
				if (filterChosenEndingTime) {
					isUrlChanged = true;
					url += '&filterChosenEndingTime=' + filterChosenEndingTime;
				}
				if (ticketStatusId) {
					isUrlChanged = true;
					url += '&ticketStatusId=' + ticketStatusId;
				}
				if (handlerId) {
					isUrlChanged = true;
					url += '&handlerId=' + handlerId;
				}
				if (isUrlChanged) {
					location.href = url;
				}
			}

			$('#searchQuery').keyup(function (event) {
				if (event.which == 13 && $(this).val()) { // on enter submit search
					updateUrlByFilters(event);
				}
			});

			$('#ticketStatusId').change(updateUrlByFilters);
			$('#handlerId').change(updateUrlByFilters);
		});
	</script>

@endsection
