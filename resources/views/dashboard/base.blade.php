@extends('layout.base')

@section('content')
	@if(((isset($data['isCrm']) && $data['isCrm'] ==1) || (isset($data['admin']) && $data['admin'] ==1))
		 || (isset($data['isSrCrm']) && $data['isSrCrm'] ==1))
		<div class="text-center font-20 pad-b-20">
			<u>CRM Dashboard</u>
		</div>
		@include('dashboard.filters')
		@include('dashboard.crm')
	@else
		<div class="text-center font-20 text-danger">
			Dashboard coming soon !!!
		</div>
	@endif
@endsection
@section('javascript')
	<script type="text/javascript">
		$.getScript("/js/dashboard.js");
	</script>
@endsection