@extends("dashboard.crm.base")

@section('metrics')
	<div class="mar-t-15"></div>
@endsection


@section('innerContent')

	<div class="form">
		<form method="GET">
			<div class="form-group">
				<div class="col-sm-3 mar-t-10">
					<div class="text-center">
						<div class="filter-title pull-left">By Name, phone</div>
						<div class="clearfix"></div>
						<div class=" no-pad">
							<input type="text"
									class="form-control in-blk"
									id="basicSearch" name="query" placeholder="Name, Phone"
									@if(request()->input('query'))value="{{request()->input('query')}}" @endif>
						</div>
					</div>
				</div>
				<div class="col-sm-3 mar-t-10">
					<div class="text-center">
						<div class="filter-title">By Party Date</div>
						<div class="clearfix"></div>
						<div class=" no-pad col-md-5">
							<input type="text"
									class="form-control in-blk filter-date"
									id="partyStartTime" name="start_date"
									@if(request()->input('start_date'))value="{{request()->input('start_date')}}" @endif>

						</div>
						<div class="col-sm-1 no-pad text-center">
							<div class="text-center mar-t-10 text-bold filter-title">To</div>
						</div>
						<div class=" no-pad col-md-5">
							<input type="text"
									class="form-control filter-date"
									id="partyEndTime" name="end_date"
									@if(request()->input('end_date'))value="{{request()->input('end_date')}}" @endif>
						</div>
					</div>
				</div>
				<div class="col-sm-3 mar-t-10">
					<div class="text-center">
						<div class="filter-title">By Requested Date</div>
						<div class="clearfix"></div>
						<div class=" no-pad col-md-5">
							<input type="text"
									class="form-control in-blk filter-date"
									id="partyStartTime" name="created_start_date"
									@if(request()->input('created_start_date'))value="{{request()->input('created_start_date')}}" @endif>

						</div>
						<div class="col-sm-1 no-pad pad-l-3 pad-r-3 text-center">
							<div class="text-center mar-t-10 text-bold filter-title">To</div>
						</div>
						<div class=" no-pad col-md-5">
							<input type="text"
									class="form-control filter-date"
									id="partyEndTime" name="created_end_date"
									@if(request()->input('created_end_date'))value="{{request()->input('created_end_date')}}" @endif>
						</div>
					</div>
				</div>
				<div class="col-sm-3 mar-t-10">
					<button type="submit" id='submitFilter' class="btn btn-info"> Filter</button>
					<div class="btn btn-warning reset-filters hide" data-url="{{route('crmDashBoard.avail-check.list')}}"> Reset Filters</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<input type="hidden" id="sort" name="" value=""/>
		</form>
	</div>

	<div class="bottom-sec mar-l-10 mar-r-10">
		<div class="new-tickets">
			<div class="data-sec mar-t-10 pad-b-20">
				<div id="callUrl" data-url="{{route("ops-dashboard.avail-checks.details")}}"></div>
				@if(count($data['availCheckData'])>0)
					<table id="availCheckSummary" class="table table-bordered table-responsive table-hover">
						<thead class="text-info">
						<tr>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Customer Name</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Party Date</div>
								<div class="in-blk mar-l-20">
									<div id="partyDateAsc" class="">
										<i class="glyphicon glyphicon-circle-arrow-up @if(Request::get('partyDateAsc')) alert-danger @endif"></i>
									</div>
									<div id="partyDateDesc">
										<i class="glyphicon glyphicon-circle-arrow-down @if(Request::get('partyDateDesc')) alert-danger @endif"></i>
									</div>
								</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Created At</div>
								<div class="in-blk mar-l-20">
									<div id="createdAtAsc" class="mar-r-10">
										<i class="glyphicon glyphicon-circle-arrow-up @if(Request::get('createdAtAsc')) alert-danger @endif"></i>
									</div>
									<div id="createdAtDesc">
										<i class="glyphicon glyphicon-circle-arrow-down @if(Request::get('createdAtDesc') || ($data['isDescended'])) alert-danger @endif"></i>
									</div>
								</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Type</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Option</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Assigned Partners</div>
							</th>
							<th>
								<div class="in-blk" style="margin: auto;vertical-align: super;">Budget By Ops</div>
							</th>
							<th width="30%">
								<div class="in-blk" style="margin: auto;vertical-align: super;">Avail Check Info</div>
							</th>
							<th width="8%">
								<div class="in-blk" style="margin: auto;vertical-align: super;">Partner Reply</div>
							</th>
						</tr>
						</thead>
						<tbody>
						@php $check=""; @endphp
						@foreach($data['availCheckData'] as $check)
							@php
								$span=1;
								if(count($check['partners'])>0)
								{
									$span=count($check['partners']);
								}
							@endphp
							<tr>
								<td rowspan="{{$span}}">

									<a href="{{ route("ticket.details.avail-checks", $check["ticketId"]) }}" target="_blank">
										<div>
											@if($check["name"])
												{{ $check["name"] }}
											@else
												Not Set
											@endif
										</div>
										<div>
											@if($check["phoneNumber"])
												{{ $check["phoneNumber"] }}
											@else
												Not Set
											@endif
										</div>
									</a>

								</td>

								<td rowspan="{{$span}}">
									<div>
										@if(isset($check["partyDate"]))

											{{ date("d M y, g:i a", $check["partyDate"])}}
										@else
											<div class="text-white font-1">9999</div>
											---
										@endif</div>
								</td>
								<td rowspan="{{$span}}">
									<div>
										@if(isset($check["createdDate"]))

											{{ date("d M y, g:i a", strtotime($check["createdDate"]["date"]))}}
										@else
											<div class="text-white font-1">9999</div>
											---
										@endif</div>
								</td>

								<td rowspan="{{$span}}">
									<div>
										@if(isset($check['optionType']))

											{{$check['optionType']}}

										@else
											{{"optionType not found"}}

										@endif
									</div>
								</td>

								<td rowspan="{{$span}}">
									<div>
										@if(isset($check['option']))

											{{$check['option']}}

										@else

											{{"option not found"}}

										@endif
									</div>
								</td>

								@if(count($check['partners'])>0)

									<td>
										{{$check['partners'][0]['name']}}
									</td>
									<td>
										{{$check['partners'][0]['budget']}}
									</td>
									<td>
										<div class="avail-check-info-cell">
											{{$check['partners'][0]['info']}}
										</div>
									</td>
									<td>
										@if($check['partners'][0]['isReplied'])
											<div><b>{{$check['partners'][0]['replyDecision']?'Yes':'No'}}</b></div>
											<div class="mar-t-5">{{$check['partners'][0]['replyText']}}</div>
											<div class="font-12 mar-t-5"><i>{{$check['partners'][0]['repliedAt']}}</i>
											</div>
										@else
											---
										@endif
									</td>
								@else
									<td rowspan="1">
										No Partner Assigned
									</td>
									<td rowspan="1">
										--
									</td>
									<td rowspan="1">
										--
									</td>
									<td rowspan="1">
										--
									</td>

								@endif
							</tr>
							@foreach($check['partners'] as $key => $partner)
								@if($key!=0)
									<tr>
										<td>
											{{$check['partners'][$key]['name']}}
										</td>
										<td>
											{{$check['partners'][$key]['budget']}}
										</td>
										<td>
											<div class="avail-check-info-cell">
												{!! $check['partners'][$key]['info'] !!}
											</div>
										</td>
										<td>
											@if($check['partners'][$key]['isReplied'])
												<div><b>{{$check['partners'][$key]['replyDecision']?'Yes':'No'}}</b>
												</div>
												<div class="mar-t-5">{{$check['partners'][$key]['replyText']}}</div>
												<div class="font-12 mar-t-5">
													<i>{{$check['partners'][$key]['repliedAt']}}</i></div>
											@else
												---
											@endif
										</td>
									</tr>
								@endif
							@endforeach

						@endforeach
						</tbody>
					</table>
				@else
					<div class="alert-danger text-center text-bold pad-10">
						<i class="glyphicon glyphicon-eye-open"></i>
						No avail checks were found.
					</div>
				@endif
			</div>
		</div>
	</div>

@endsection

@section('childJavascript')
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#partyDateAsc').click(function () {
				$('#sort').attr("name", "partyDateAsc");
				$('#sort').val(true);
				$('#submitFilter').click();
			});

			$('#partyDateDesc').click(function () {
				$('#sort').attr("name", "partyDateDesc");
				$('#sort').val(true);
				$('#submitFilter').click();
			});
			$('#createdAtAsc').click(function () {
				$('#sort').attr("name", "createdAtAsc");
				$('#sort').val(true);
				$('#submitFilter').click();
			});
			$('#createdAtDesc').click(function () {
				$('#sort').attr("name", "createdAtDesc");
				$('#sort').val(true);
				$('#submitFilter').click();
			});
		});
		$('.reset-filters').on('click', function () {

			window.location = $(this).data('url');

		});
		$('.filter-date').datetimepicker({
			timepicker: false,
			scrollInput: false,
			scrollMonth: false,
			closeOnDateSelect: true,
			format: "d-m-Y"
		});

		(location.href.indexOf('?') > 0) && $('.reset-filters').removeClass('hide');

	</script>
@endsection