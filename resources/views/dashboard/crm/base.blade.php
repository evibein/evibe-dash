@extends('layout.base')

@section('custom-head')
	<link rel="stylesheet" type="text/css"
			href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.css"/>
@endsection

@section('content')
	<div class="panel panel-default page-content mar-l-10 mar-r-10">
		<div class="panel-body pad-b-10">
			<div class="title-sec no-pad">
				<div>
					<div class="col-sm-6 col-sm-offset-2 text-center">
						<div class="btn-group" role="group" aria-label="...">
							<a class="btn btn-default @if(request()->is('*/new-tickets*')) active @endif"
									href="{{route('crmDashBoard.new.tickets.list')}}">New Tickets</a>
							<a class="btn btn-default hide @if(request()->is('*/prospect*')) active @endif"
									href="{{route('crmDashBoard.prospect.tickets.list')}}">Prospect</a>
							<a class="btn btn-default hide" href="{{route('crmDashBoard.metrics')}}">Metrics</a>
							<a class="btn btn-default @if(request()->is('*/return-tickets*')) active @endif"
									href="{{route('crmDashBoard.return.tickets.list')}}">Followups</a>
							<a class="btn btn-default hide @if(request()->is('*/history*')) active @endif"
									href="{{route('crmDashBoard.history')}}">HISTORY</a>
							<a class="btn btn-default @if(request()->is('*/progress*')) active @endif"
									href="{{route('crmDashBoard.progress.tickets.list')}}">In Progress</a>
							<a class="btn btn-default @if(request()->is('*/avail-checks*')) active @endif"
									href="{{route('crmDashBoard.avail-check.list')}}">Avail Checks</a>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1 @if((request()->is('*/missed-tickets*'))|| (request()->is('*/avail-checks*')) ){{ "hide" }}@endif">
						<div class="col-sm-5 no-pad">
							<input type="text"
									class="form-control in-blk filter-anlytics-date filter-anlytics-creation-start-time text-center"
									@if(isset($data['startDate']))
										value="{{date('d-m-Y H:i',$data['startDate'])}}"
									@endif
									id="startTime">
						</div>
						<div class="col-sm-1 no-pad text-center">
							<div class="text-center mar-t-10 text-bold filter-title">To</div>
						</div>
						<div class="col-sm-5 no-pad">
							<input type="text"
									class="form-control filter-anlytics-date filter-anlytics-creation-end-time text-center"
									@if(isset($data['endDate']))
										value="{{date('d-m-Y H:i',$data['endDate'])}}"
									@endif
									id="endTime">
						</div>
						<div class="col-sm-1 no-pad text-center">
							<div class="text-center filter-date-btn-wrap">
								<a class="btn btn-xs btn-primary btn-filter-by-cd">GO</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				@yield("metrics")
			</div>
		</div>
		@yield("innerContent")
	</div>
	<input type="hidden"
			value="@if(request()->is('*/new-tickets*')){{ "1" }}@elseif(request()->is('*/return-tickets*')){{"2"}}@elseif(request()->is('*/missed-tickets*')){{"3"}}@elseif(request()->is('*/history*')){{"4"}} @else {{"5"}}@endif"
			id="isNewTickets">
	<input type="hidden" value="{{ request("leadStatus") }}" id="appliedLeadStatusId">
	<input type="hidden" value="{{ request("ticketStatus") }}" id="appliedTicketStatusId">
	<input type="hidden" id="ticketUpdateModalId" value="">
@endsection

@section('app-modals')
	@parent
	@include('tickets.list.modals')
@endsection

@section('javascript')
	@yield("childJavascript")
	<script type="text/javascript">
		$(document).ready(function () {
			(function initFunctions() {
				// date time picker for filters
				$('.filter-anlytics-creation-start-time, .filter-anlytics-creation-end-time, .filter-followup-end-time, .filter-followup-start-time').datetimepicker({
					timepicker: true,
					scrollInput: false,
					scrollMonth: false,
					closeOnDateSelect: false,
					format: "d-m-Y H:i",
					formatTime: "H:i"
				});
			})();

			$('.btn-filter-by-cd').click(function (event) { // filter by party date
				event.preventDefault();
				var startDate = $('.filter-anlytics-creation-start-time').val();
				var endDate = $('.filter-anlytics-creation-end-time').val();

				if (!startDate || !endDate) {
					showNotyError("Please select both start date & end date.")
				} else {
					var url = '';
					if (startDate) url = formUrl(url, 'filterChosenStartingTime', startDate, false, ['page']);
					if (endDate) url = formUrl(url, 'filterChosenEndingTime', endDate, false, ['page']);
					refreshUrl(url);
				}
			});

			(function loadUpdateTicketStatusModal() {
				$.ajax({
					url: '/tickets/status-update/details',
					type: 'GET',
					dataType: 'json',
					success: function (data, textStatus, jqXHR) {
						var optionHtml = '';
						var relatedTicket = '';

						if (data.ticketsStatuses) {
							$.each(data.ticketsStatuses, function (key, statusOptions) {
								optionHtml = optionHtml + "<option value='" + statusOptions.id + "'>" + statusOptions.name + "</option>";
							});

							$(optionHtml).appendTo('.ticket-statuses-through-ajax');
						}
						if (data.ticketsData) {
							$.each(data.ticketsData, function (count, relatedTicketDetails) {
								relatedTicket = relatedTicket + "<option value='" + relatedTicketDetails.id + "'>"
									+ relatedTicketDetails.id + " / " + relatedTicketDetails.name + " / " + relatedTicketDetails.email
									+ " / " + relatedTicketDetails.phone + "</option>";
							});

							$(relatedTicket).appendTo('.related-ticket-data-through-ajax');
						}

						triggerJSUpdateModal();
					},
					error: function (data, textStatus, jqXHR) {
						window.showNotyError("Some Error occured while getting the data, Please reload the page.");
					}
				});
			})();

			function triggerJSUpdateModal() {
				$('#tuDate, #followUpDateFromModal, #updateClosureDate').datetimepicker({
					step: 30,
					minDate: 0
				});
				$('#relatedTicket').selectize();

				$('#tuStatus').on('change', function () {
					var updatedStatus = $('#tuStatus :selected').val();
					if (updatedStatus == 15) {
						$('.followup-date_wrap').addClass('hide');
						$('.no-response-date-wrap').addClass('hide');
						$('.related_ticket_wrap').removeClass('hide');
					} else if (updatedStatus == 7) {
						$('.related_ticket_wrap').addClass('hide');
						$('.no-response-date-wrap').addClass('hide');
						$('.followup-date_wrap').removeClass('hide');
					} else if (updatedStatus == 17) {
						$('.related_ticket_wrap').addClass('hide');
						$('.followup-date_wrap').addClass('hide');
						$('.no-response-date-wrap').removeClass('hide');
					} else {
						$('.related_ticket_wrap').addClass('hide');
						$('.followup-date_wrap').addClass('hide');
						$('.no-response-date-wrap').addClass('hide');
					}

					if (updatedStatus == $('#tsDuplicate').val()) {
						$('#tuType').val('other');
						$('#tuComments').val('Another valid ticket already exists!');
					}
				});

				$('.application').on('click', '.btn-ticket-update', function (event) {
					event.preventDefault();

					$('#modalStatusUpdateForm')[0].reset();
					$('.related_ticket_wrap').addClass('hide');
					$('.followup-date_wrap').addClass('hide');
					$('.no-response-date-wrap').addClass('hide');

					var ticketId = $(this).data('ticketId');
					$('#ticketUpdateModalId').val(ticketId);
					$('#modalStatusUpdate').modal('show');
				});

				$("#followUpDateFromModal").blur(function () {
					$("#updateClosureDate").val($(this).val());
				});

				$('#btnUpdateStatusAccept').on('click', function () {

					$('.ticket-update-modal-submit').addClass('hide');
					$('.ticket-update-modal-submitting').removeClass('hide');

					var ticketUpdateDateAndTime = $('#tuDate').val();
					var ticketUpdateStatusId = $('#tuStatus').val();
					var ticketUpdateComments = $('#tuComments').val();
					var ticketUpdateType = $('#tuType').val();
					var followUpDate = $('#followUpDateFromModal').val();
					var followUpType = $('#followupComment').val();
					var closureDate = $('#updateClosureDate').val();
					var isAjaxRequest = 1;

					var ticketUpdateRelatedTicketId = '';
					if (parseInt(ticketUpdateStatusId) == 15) {
						ticketUpdateRelatedTicketId = $('#relatedTicket').val();
					}

					$.ajax({
						url: '/tickets/' + $('#ticketUpdateModalId').val() + '/status-update',
						type: 'POST',
						dataType: 'json',
						data: {
							tuDate: ticketUpdateDateAndTime,
							tuType: ticketUpdateType,
							tuStatus: ticketUpdateStatusId,
							tuComments: ticketUpdateComments,
							relatedTicket: ticketUpdateRelatedTicketId,
							isAjaxRequest: isAjaxRequest,
							followUpDate: followUpDate,
							followUpType: followUpType,
							closureDate: closureDate
						},

						success: function (data, textStatus, jqXHR) {
							if (data.success) {
								$('#modalStatusUpdate').modal('hide');
								$('.ticket-update-modal-submit').removeClass('hide');
								$('.ticket-update-modal-submitting').addClass('hide');

								window.showNotySuccess("Ticket status updated successfully");

								if (typeof updateDatatableData === 'function') {
									updateDatatableData(data.ticketData);
								}
							} else {
								$('.ticket-update-modal-submit').removeClass('hide');
								$('.ticket-update-modal-submitting').addClass('hide');

								window.showNotyError(data.error);
							}
						},
						error: function (data, textStatus, jqXHR) {
							$('.ticket-update-modal-submit').removeClass('hide');
							$('.ticket-update-modal-submitting').addClass('hide');

							window.showNotyError(data.error);
						}
					});
				});
			}

			function formUrl(cUrl, key, value, isIgnore, ignores) {
				cUrl = cUrl ? cUrl.substr(cUrl.indexOf('?') + 1).split('&') : window.location.search.substr(1).split('&');
				var cParams = getQueryParams(cUrl), queryString = "", loc = window.location;
				var url = loc.protocol + '//' + loc.host + loc.pathname;
				if (isIgnore) { // remove from current object
					if (cParams.hasOwnProperty(key)) {
						try {
							delete cParams[key];
						} catch (e) {
						}
					}
				} else cParams[key] = value;
				for (key in cParams) { // form string
					if (ignores && ignores.constructor === Array && window.indexOf.call(ignores, key) >= 0) continue;
					queryString += key + "=" + cParams[key] + "&";
				}
				if (queryString) {
					url += '?' + queryString;
					url = url.slice(0, -1); // remove trailing '&'
				}
				return url;
			}

			function getQueryParams(a) {
				if (a == "") return {};
				var b = {};
				for (var i = 0; i < a.length; ++i) {
					var p = a[i].split('=');
					if (p.length != 2) continue;
					b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
				}
				return b;
			}

			function refreshUrl(url) {
				window.location.href = url;
			}

			function updateUrlByFilters($leadStatus, $ticketStatus) {

				var filterChosenStartingTime = $('#startTime').val();
				var filterChosenEndingTime = $('#endTime').val();
				var occasion = $('#occasion').val();
				var handler = $('#handler').val();
				var createdByHandler = $('#createdByHandler').val();
				var enquirySource = $('#enquirySource').val();
				var customerSource = $('#customerSource').val();
				var ticketStatus = $ticketStatus ? $ticketStatus : '';
				var leadStatus = $leadStatus ? $leadStatus : '';
				var bookingLikeliness = $('#booingLikeliness').val();
				var url;
				var partyStartTime = $('#partyStartTime').val();
				var partyEndTime = $('#partyEndTime').val();
				var followupStartTime = $('#followupStartTime').val();
				var followupEndTime = $('#followupEndTime').val();
				var followupType = $('#followUpType').val();
				var closureStartTime = $('#closureStartTime').val();
				var closureEndTime = $('#closureEndTime').val();
				var createdAtStartTime = $('#createdAtStartTime').val();
				var createdAtEndTime = $('#createdAtEndTime').val();

				var isUrlChanged = false;
				var pageStatus = $('#isNewTickets').val();
				if (pageStatus == 1) {
					url = "new-tickets?";
				} else if (pageStatus == 2) {
					url = "return-tickets?";
				} else if (pageStatus == 3) {
					url = "missed-tickets?";
				} else if (pageStatus == 4) {
					url = "history?";
				} else {
					url = "prospect-tickets?";
				}
				if (filterChosenStartingTime) {
					isUrlChanged = true;
					url += '&filterChosenStartingTime=' + filterChosenStartingTime;
				}

				if (filterChosenEndingTime) {
					isUrlChanged = true;
					url += '&filterChosenEndingTime=' + filterChosenEndingTime;
				}

				if (occasion) {
					isUrlChanged = true;
					url += '&occasion=' + occasion;
				}

				if (handler) {
					isUrlChanged = true;
					url += '&handler=' + handler;
				}

				if (createdByHandler) {
					isUrlChanged = true;
					url += '&createdByHandler=' + createdByHandler;
				}

				if (bookingLikeliness) {
					isUrlChanged = true;
					url += '&bookingLikeliness=' + bookingLikeliness;
				}

				if (enquirySource) {
					isUrlChanged = true;
					url += '&enquirySource=' + enquirySource;
				}
				if (customerSource) {
					isUrlChanged = true;
					url += '&customerSource=' + customerSource;
				}
				if (leadStatus) {
					isUrlChanged = true;
					url += '&leadStatus=' + leadStatus;
				}

				if (ticketStatus) {
					isUrlChanged = true;
					url += '&ticketStatus=' + ticketStatus;
				}
				if (partyStartTime) {
					isUrlChanged = true;
					url += '&partyStartTime=' + partyStartTime;
				}

				if (partyEndTime) {
					isUrlChanged = true;
					url += '&partyEndTime=' + partyEndTime;
				}
				if (followupStartTime) {
					isUrlChanged = true;
					url += '&followupStartTime=' + followupStartTime;
				}

				if (followupEndTime) {
					isUrlChanged = true;
					url += '&followupEndTime=' + followupEndTime;
				}

				if (followupType) {
					isUrlChanged = true;
					url += '&followUpType=' + followupType;
				}

				if (closureStartTime) {
					isUrlChanged = true;
					url += '&closureStartTime=' + closureStartTime;
				}

				if (closureEndTime) {
					isUrlChanged = true;
					url += '&closureEndTime=' + closureEndTime;
				}

				if (createdAtStartTime) {
					isUrlChanged = true;
					url += '&createdAtStartTime=' + createdAtStartTime;
				}

				if (createdAtEndTime) {
					isUrlChanged = true;
					url += '&createdAtEndTime=' + createdAtEndTime;
				}

				if (isUrlChanged) {
					location.href = url;
				}

			}

			// filter options
			$('.apply-filters-btn').on("click", function () {
				var leadStatus = $('#appliedLeadStatusId').val();
				var ticketStatus = $('#appliedTicketStatusId').val();
				updateUrlByFilters(leadStatus, ticketStatus);
			});

			$('.lead-status-metric-wrap')
				.css('cursor', 'pointer')
				.hover(function () {
					$(this).css("text-decoration", "underline");
				}, function () {
					$(this).css("text-decoration", "none")
				})
				.on("click", function () {
					var leadStatus = $(this).data("leadStatusId");
					var ticketStatus = $('#appliedTicketStatusId').val();
					updateUrlByFilters(leadStatus, ticketStatus);
				});

			$('.ticket-status-metric-wrap')
				.css('cursor', 'pointer')
				.hover(function () {
					$(this).css("text-decoration", "underline");
				}, function () {
					$(this).css("text-decoration", "none")
				})
				.on("click", function () {
					var leadStatus = $('#appliedLeadStatusId').val();
					var ticketStatus = $(this).data("ticketStatusId");
					updateUrlByFilters(leadStatus, ticketStatus);
				});
		});
	</script>
@endsection