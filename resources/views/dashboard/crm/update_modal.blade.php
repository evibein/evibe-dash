@if(count($data["ticketUpdates"]) > 0)
	<table>
		@php $count = 0; @endphp
		@foreach($data["ticketUpdates"] as $ticketUpdate)
			@if($count > 0)
				<tr>
					<td colspan="2">
						<div class="text-center mar-t-5 mar-b-5">
							<span class="font-24 text-center glyphicon glyphicon-arrow-up"></span>
						</div>
					</td>
				</tr>
			@endif
			<tr class="@if(isset($data['startTime'])) @if(strtotime($ticketUpdate['updated_at']) < $data['startTime']) text-warning @endif @endif">
				<td class="col-sm-3 border-grey">
					<div>
						<span class="glyphicon glyphicon-user"> </span>
						<span>{{ $ticketUpdate["handler"] }}</span>
					</div>
					<div>
						<span class="glyphicon glyphicon-expand"></span>
						<span>{{ $ticketUpdate["status"] }}</span>
					</div>
				</td>
				<td class="col-sm-9 border-grey">
					<div>Update happened through
						<span class="text-bold">{{ $ticketUpdate["update_source"] }}</span>
						with comments
						<span class="text-bold">{{ $ticketUpdate["comments"] }}</span>
					</div>
					<div class="pull-right">{{ $ticketUpdate["updated_at"] }}</div>
				</td>
			</tr>
			@php $count++; @endphp
		@endforeach
	</table>
@else
	<div>Oops!! No ticket updates found for this ticket between those time Intervals.</div>
@endif