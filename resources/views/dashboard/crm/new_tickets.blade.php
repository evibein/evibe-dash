@extends("dashboard.crm.base")

@section('metrics')
	<div class="mar-t-20">
		<div class="col-sm-4 text-center font-11">
			<table class="table table-responsive table-bordered text-center table-condensed table-fixed">
				<tr>
					<td class="alert-danger lead-status-metric-wrap"
							data-lead-status-id="{{ config("evibe.lead_status.hot") }}">
						<div class="text-bold font-22">
							@if($data['totalHotLeads'] > 0)
								{{$data['totalHotLeads']}}
							@else{{ "0" }}@endif
						</div>
						<div class="pad-t-3 in-blk">HOT</div>
					</td>
					<td class="alert-warning lead-status-metric-wrap"
							data-lead-status-id="{{ config("evibe.lead_status.medium") }}">
						<div class="text-bold font-22">
							@if($data["totalMediumLeads"] > 0)
								{{ $data["totalMediumLeads"] }}
							@else{{ "0" }}@endif
						</div>
						<div class="pad-t-3 in-blk">MEDIUM</div>
					</td>
					<td class="alert-info lead-status-metric-wrap"
							data-lead-status-id="{{ config("evibe.lead_status.cold") }}">
						<div class="text-bold font-22">
							@if($data["totalColdLeads"] > 0)
								{{ $data["totalColdLeads"] }}
							@else{{ "0" }}@endif
						</div>
						<div class="pad-t-3 in-blk">COLD</div>
					</td>
					<td class="lead-status-metric-wrap" data-lead-status-id="notSet">
						<div class="text-bold font-22">
							@if($data["totalNotSetLeads"] > 0)
								{{ $data["totalNotSetLeads"] }}
							@else{{ "0" }}@endif
						</div>
						<div class="pad-t-3 in-blk">NOT SET</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="col-sm-8 text-center font-11">
			<table class="table table-responsive table-bordered text-center table-condensed table-fixed">
				<tr>
					<td class="ticket-status-metric-wrap alert-success"
							data-ticket-status-id="{{ config("evibe.status.booked") }}">
						<div class="text-bold font-22">
							@if($data['countOfBookedTickets'] > 0)
								{{$data['countOfBookedTickets'] }}
								<small>
									{{"(". round(($data['countOfBookedTickets']/$data["countOfNewTickets"])*100 , 0) . "%)"}}
								</small>
							@else{{ "0" }}@endif
						</div>
						<div class="in-blk pad-t-3">
							BOOKED
							<span class="glyphicon glyphicon-info-sign" title="(Booked / Total New Tickets)"></span>
						</div>
					</td>
					<td class="ticket-status-metric-wrap"
							data-ticket-status-id="{{ config("evibe.status.enquiry") }}">
						<div class="text-bold font-22">
							@if($data["countOfJustEnquiryTickets"] > 0)
								{{ $data["countOfJustEnquiryTickets"] }}
							@else{{ "0" }}@endif
						</div>
						<div class="in-blk pad-t-3">JUST ENQUIRY</div>
					</td>
					<td class="ticket-status-metric-wrap"
							data-ticket-status-id="{{ config("evibe.status.no_response") }}">
						<div class="text-bold font-22">
							@if($data["countOfNoResponseTickets"] > 0)
								{{ $data["countOfNoResponseTickets"] }}
							@else{{ "0" }}@endif
						</div>
						<div class="in-blk pad-t-3">NO RESPONSE</div>
					</td>
					<td>
						<div class="text-bold font-22">
							@if($data['countOfOrderProcessedTickets'] != 0)
								{{ $data['totalProcessedBookingsCount'] }}
								<small>
									({{ round(($data['totalProcessedBookingsCount']/$data['countOfOrderProcessedTickets'])*100, 0) }}%)
								</small>
							@else
								--
							@endif
						</div>
						<div class="in-blk pad-t-3">
							OP RATIO
							<span class="glyphicon glyphicon-info-sign" title="(Processed / Booked)"></span>
						</div>
					</td>
					<td class="ticket-status-metric-wrap"
							data-ticket-status-id="{{ config("evibe.status.initiated") }}">
						<div class="text-bold font-22">
							@if($data["countOfInitiatedTickets"] > 0)
								{{ $data["countOfInitiatedTickets"] }}
							@else{{ "0" }}@endif
						</div>
						<div class="in-blk pad-t-3">INITIATED</div>
					</td>
					<td class="ticket-status-metric-wrap"
							data-ticket-status-id="{{ config("evibe.status.followup") }}">
						<div class="text-bold font-22">
							@if($data["countOfFollowUpTickets"])
								{{ $data["countOfFollowUpTickets"] }}
								<small>
									{{ "(" . round(($data["countOfFollowUpTickets"]/$data["countOfNewTickets"])*100, 0) ."%)" }}
								</small>
							@else{{ "0" }}@endif
						</div>
						<div class="in-blk pad-t-3">
							FOLLOWUP
							<span class="glyphicon glyphicon-info-sign"
									title="(Total Followup / Total New Tickets)"></span>
						</div>
					</td>
					<td>
						<div class="text-bold font-22">
							@if($data["countOfNewTickets"])
								{{ $data["countOfNewTickets"] }}
							@else{{ "0" }}@endif
						</div>
						<div class="in-blk pad-t-3">TOTAL NEW</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section('innerContent')
	<div class="bottom-sec mar-l-10 mar-r-10">
		<div class="filter-group-options ls-none no-mar no-pad">
			<div class="sec-head">
				<a class="btn btn-warning text-bold font-16" role="button" data-toggle="collapse"
						href="#collapseTags"
						aria-expanded="false" aria-controls="collapseTaga">+ Filters
				</a>
				@if($data['filterReset'])
					<a class="btn btn-warning font-16"
							href="{{ route('crmDashBoard.new.tickets.list') }}">Reset Filters</a>
				@endif
			</div>
			<div class="collapse" id="collapseTags">
				<div class="filter-group-options ls-none">
					<div class="col-sm-2 no-pad-l">
						<div class="text-center">
							<div class="filter-title pull-left">By Booking Likeliness</div>
							<select class="form-control" name="booingLikeliness" id="booingLikeliness">
								<option @if(request()->input('bookingLikeliness') == "all")selected
										@endif value="all">All
								</option>
								@foreach($data['bookingLikeliness'] as $key => $enquirySource)
									<option value="{{$key}}"
											@if(request()->input('bookingLikeliness') == $key)selected @endif>{{$enquirySource}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="text-center">
							<div class="filter-title pull-left">By Occasion</div>
							<select class="form-control" name="occasion" id="occasion">
								<option value="all">All Occasions</option>
								@foreach($data['typeEvents'] as $key => $event)
									<option value="{{$key}}"
											@if(request()->input('occasion')== $key)selected @endif>{{$event}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="text-center">
							<div class="filter-title pull-left">By Updated handler</div>
							<select class="form-control" name="handler" id="handler">
								<option value="all">All Handlers</option>
								@foreach($data['handlerDetails'] as $key => $handlerDetail)
									<option value="{{$key}}"
											@if(request()->input('handler')== $key)selected @endif>{{$handlerDetail}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="text-center">
							<div class="filter-title pull-left">By Enquiry Source</div>
							<select class="form-control" name="enquirySource" id="enquirySource">
								<option value="all" @if(request()->input('enquirySource') == "all")selected
										@endif>All Sources
								</option>
								<option value="scheduled"
										@if(request()->input('enquirySource') == "scheduled")selected @endif>Schedule(Non-Phone)
								</option>
								@foreach($data['typeEnquirySource'] as $key => $enquirySource)
									<option value="{{$key}}"
											@if(request()->input('enquirySource')== $key)selected @endif>{{$enquirySource}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="text-center">
							<div class="filter-title pull-left">By Customer Source</div>
							<select class="form-control" name="customerSource" id="customerSource">
								<option value="all" @if(request()->input('customerSource') == "all")selected
										@endif>All Sources
								</option>
								@foreach($data['typeTicketSource'] as $key => $customerSource)
									<option value="{{$key}}"
											@if(request()->input('customerSource')== $key)selected @endif>{{$customerSource}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="text-center">
							<div class="filter-title pull-left">By Followup Type</div>
							<select class="form-control" name="followUpType" id="followUpType">
								<option value="all" @if(request()->input('followUpType') == "all")selected
										@endif>All Sources
								</option>
								@foreach($data['typeFollowUp'] as $key => $followupType)
									<option value="{{$followupType}}"
											@if(request()->input('followUpType')== $followupType)selected @endif>{{$followupType}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-2 no-pad-l mar-t-10">
						<div class="text-center">
							<div class="filter-title text-left">By Party Date</div>
							<div class="no-pad col-md-5">
								<input type="text"
										class="form-control in-blk filter-followup-date filter-followup-start-time"
										id="partyStartTime"
										value="{{$data['startPartyTime'] ? date('d-m-Y H:i',$data['startPartyTime']): null}}">
							</div>
							<div class="col-sm-1 no-pad text-center">
								<div class="text-center mar-t-10 text-bold filter-title">To</div>
							</div>
							<div class=" no-pad col-md-5">
								<input type="text"
										class="form-control filter-followup-date filter-followup-end-time"
										id="partyEndTime"
										value="{{$data['endPartyTime'] ? date('d-m-Y H:i',$data['endPartyTime']): null}}">
							</div>
						</div>
					</div>
					<div class="col-sm-2 mar-t-10">
						<div class="text-center">
							<div class="filter-title text-left">By FollowUp Date</div>
							<div class="col-lg-5 no-pad">
								<input type="text"
										class="form-control in-blk filter-followup-date filter-followup-start-time"
										id="followupStartTime"
										value="{{$data['followupStartTime'] ? date('d-m-Y H:i',$data['followupStartTime']): null}}">
							</div>
							<div class="col-sm-1 no-pad text-center">
								<div class="text-center mar-t-10 text-bold filter-title">To</div>
							</div>
							<div class="col-lg-5 no-pad">
								<input type="text"
										class="form-control filter-followup-date filter-followup-end-time"
										id="followupEndTime"
										value="{{$data['followupEndTime'] ? date('d-m-Y H:i',$data['followupEndTime']): null}}">
							</div>
						</div>
					</div>
					<div class="col-sm-2 mar-t-10">
						<div class="text-center">
							<div class="filter-title text-left">By Closure Date</div>
							<div class="col-lg-5 no-pad">
								<input type="text"
										class="form-control in-blk filter-followup-date filter-followup-start-time"
										id="closureStartTime"
										value="{{$data['closureStartTime'] ? date('d-m-Y H:i',$data['closureStartTime']): null}}">
							</div>
							<div class="col-sm-1 no-pad text-center">
								<div class="text-center mar-t-10 text-bold filter-title">To</div>
							</div>
							<div class="col-lg-5 no-pad">
								<input type="text"
										class="form-control filter-followup-date filter-followup-end-time"
										id="closureEndTime"
										value="{{$data['closureEndTime'] ? date('d-m-Y H:i',$data['closureEndTime']): null}}">
							</div>
						</div>
					</div>
					<div class="col-sm-2 mar-t-10">
						<div class="text-center">
							<div class="filter-title text-left">By Created At Date</div>
							<div class="col-lg-5 no-pad">
								<input type="text"
										class="form-control in-blk filter-followup-date filter-followup-start-time"
										id="createdAtStartTime"
										value="{{$data['createdAtStartTime'] ? date('d-m-Y H:i',strtotime($data['createdAtStartTime'])): null}}">
							</div>
							<div class="col-sm-1 no-pad text-center">
								<div class="text-center mar-t-10 text-bold filter-title">To</div>
							</div>
							<div class="col-lg-5 no-pad">
								<input type="text"
										class="form-control filter-followup-date filter-followup-end-time"
										id="createdAtEndTime"
										value="{{$data['createdAtEndTime'] ? date('d-m-Y H:i',strtotime($data['createdAtEndTime'])): null}}">
							</div>
						</div>
					</div>
					<div class="col-sm-1">
						<a class="btn btn-primary mar-t-15 apply-filters-btn">Apply</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="new-tickets">
			<div class="data-sec mar-t-10 pad-b-20">
				@if(count($data['tickets'])>0)
					<table id="newTicketsSummary" class="table table-bordered table-responsive table-hover">
						<thead class="text-info">
						<tr>
							<th></th>
							<th>Customer Name</th>
							<th>Party Date</th>
							<th>Lead Status</th>
							<th>Source (Customer / Enquiry)</th>
							<th>Status</th>
							<th>Handler (Current / Created)</th>
							<th>City</th>
							<th>Occasion</th>
							<th>Last Update</th>
							<th>Follow up</th>
							<th>Closure Date</th>
							<th>Preferred Slot</th>
						</tr>
						</thead>
						<tbody>
						@foreach($data['tickets'] as $ticket)
							@php
								$ticketUpdateComment = isset($data["ticketUpdate"][$ticket["id"]]) ? $data["ticketUpdate"][$ticket["id"]] : '';
								$activeFollowup = isset($data["activeFollowups"][$ticket["id"]])? $data["activeFollowups"][$ticket["id"]] : '';
								$typeLeadStatusOfTicket = isset($data["typeLeadStatus"][$ticket["lead_status_id"]])? $data["typeLeadStatus"][$ticket["lead_status_id"]] : '';
								$typeEnquirySourceOfTicket = isset($data["typeEnquirySource"][$ticket["enquiry_source_id"]])? $data["typeEnquirySource"][$ticket["enquiry_source_id"]] : '';
								$typeTicketSourceOfTicket = isset($data["typeTicketSource"][$ticket["source_id"]])? $data["typeTicketSource"][$ticket["source_id"]] : '';
								$typeTicketStatusOfTicket = isset($data["typeTicketStatus"][$ticket["status_id"]])? $data["typeTicketStatus"][$ticket["status_id"]] : '';
								$handlerDetailsOfTicket = isset($data["handlerDetails"][$ticket["handler_id"]])? $data["handlerDetails"][$ticket["handler_id"]] : '';
								$createdHandlerDetailsOfTicket = isset($data["handlerDetails"][$ticket["created_handler_id"]])? $data["handlerDetails"][$ticket["created_handler_id"]] : '';
								$cityDetailsOfTicket = isset($data["cityDetails"][$ticket["city_id"]])? $data["cityDetails"][$ticket["city_id"]] : '';
								$eventTypeDetailsOfTicket = isset($data["eventTypeDetails"][$ticket["event_id"]])? $data["eventTypeDetails"][$ticket["event_id"]] : '';
							@endphp
							<tr>
								<td class="text-center">
									<span class="{{ "test-remove-tr-" . $ticket["id"] }}"></span>
									<div class="whatsapp-button btn no-pad" data-id="{{$ticket["id"]}}">
										<img src="http://gallery.evibe.in/img/icons/whatsapp-logo.png">
									</div>
									<div>
										<a class="btn btn-primary btn-xs btn-ticket-update mar-t-10" data-ticket-id="{{ $ticket["id"] }}">
											<i class="glyphicon glyphicon-arrow-up"></i> Update
										</a>
									</div>
								</td>
								<td>
									<a href="/tickets/{{ $ticket["id"] }}/information" target="_blank">
										<div class="text-white font-1">[{{ date('y/m/d H:i', strtotime($ticket["created_at"])) }}]</div>
										<div class="text-muted font-10">[{{ date('d M y, g:i a', strtotime($ticket["created_at"]))}}]</div>
										<div>
											@if($ticket["name"])
												{{ $ticket["name"] }}
											@else
												Not Set
											@endif
										</div>
									</a>
									<div>
										(@if(isset($ticket["calling_code"]) && $ticket["calling_code"])<span class="mar-r-2">{{ $ticket["calling_code"] }}</span>@endif<span>{{$ticket["phone"]}}</span>)
									</div>
								</td>
								<td>
									@if($ticket["event_date"])
										<div class="text-white font-1">{{ date('y/m/d H:i:s', $ticket["event_date"])}}</div>
										{{ date("d M y, g:i a", $ticket["event_date"])}}
									@else
										<div class="text-white font-1">9999</div>
										---
									@endif
								</td>
								<td>
									@if($typeLeadStatusOfTicket)
										{{ ucfirst($typeLeadStatusOfTicket) }}
									@else
										---
									@endif
								</td>
								<td>
									@if($typeEnquirySourceOfTicket)
										{{ ucfirst($typeEnquirySourceOfTicket) }}
									@else
										---
									@endif
									@if($typeTicketSourceOfTicket)
										/  {{ ucfirst($typeTicketSourceOfTicket) }}
									@else
										/ ---
									@endif
								</td>
								<td>
									@if($typeTicketStatusOfTicket)
										{!! $typeTicketStatusOfTicket !!}
									@else
										---
									@endif
								</td>
								<td>
									@if($handlerDetailsOfTicket)
										{{ucfirst($handlerDetailsOfTicket)}}
									@else
										---
									@endif
									/
									@if($createdHandlerDetailsOfTicket)
										{{ucfirst($createdHandlerDetailsOfTicket)}}
									@else
										---
									@endif

								</td>
								<td>
									@if($cityDetailsOfTicket)
										{{ucfirst($cityDetailsOfTicket)}}
									@else
										---
									@endif
								</td>
								<td>
									@if($eventTypeDetailsOfTicket)
										{{ucfirst($eventTypeDetailsOfTicket)}}
									@else
										---
									@endif
								</td>
								<td>
									@if($ticketUpdateComment)
										@php $comment = explode("~", $ticketUpdateComment); @endphp
										<div class="text-white font-1">[{!! $comment[1] !!}]</div>
										<div class="text-muted font-12">[{!! $comment[1] !!}]</div>
										{!! $comment[0] !!}
									@else
										<div class="text-white font-1">9999</div>
										---
									@endif
								</td>
								<td>
									@if($activeFollowup)
										<div class="text-white font-1">{{ date('y/m/d H:i:s', $activeFollowup)}}</div>
										<span class="@if ($activeFollowup < (\Carbon\Carbon::now()->timestamp)) text-danger @endif">
								{{ date("d M y, g:i a", $activeFollowup) }}
							</span>
									@else
										<div class="text-white font-1">9999</div>
										---
									@endif
								</td>
								<td>
									@if($ticket["expected_closure_date"])
										<div class="text-white font-1">{{ date('y/m/d H:i:s', $ticket["expected_closure_date"])}}</div>
										<span class="@if ($ticket["expected_closure_date"] < (\Carbon\Carbon::now()->timestamp)) text-danger @endif">
										{{ date("d M y", $ticket["expected_closure_date"]) }}
										</span>
									@else
										<div class="text-white font-1">9999</div>
										---
									@endif
								</td>
								<td>
									@if($ticket["customer_preferred_slot"])
										{{ $ticket["customer_preferred_slot"] }}
									@else
										<div class="text-white font-1">9999</div>
										---
									@endif
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				@else
					<div class="alert-danger text-center text-bold pad-10">
						<i class="glyphicon glyphicon-eye-open"></i>
						No new tickets were found.
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection

@section('childJavascript')
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			var table = $('#newTicketsSummary').DataTable({
				"fixedHeader": true,
				"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]]
			});

			window.updateDatatableData = function (ticketData) {
				var parentTR = $(".test-remove-tr-" + $('#ticketUpdateModalId').val()).parents('tr');

				var d = table.row(parentTR).data();

				d["5"] = ticketData.status;
				d["9"] = ticketData.lastUpdate;
				d["10"] = ticketData.followupDate;
				d["11"] = ticketData.closureDate;

				table.row(parentTR)
					.data(d);
			}
		});
	</script>
@endsection