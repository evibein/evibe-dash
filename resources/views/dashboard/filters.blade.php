<div class="col-md-2 col-lg-2 col-sm-2 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
	<div>
		<h5 class="no-mar pull-left">Filter Options</h5>
		<div class="pull-right">
			@if($data['isShowReset'])
				<a class="btn btn-xs btn-warning mar-l-20"
						href="{{ route('dashBoard.list') }}">Reset</a>
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="filter-group mar-t-10">
		<div class="ls-none no-mar no-pad">
			<label class="filter-title">By Date</label>
			<div class="pad-b-20">
				<div class="text-center">
					<input type="text" class="form-control filter-pd-start"
							value="{{ request()->input('start_date') }}" placeholder="Start Date">
				</div>
				<div class="in-blk text-center mar-t-10">
					<input type="text" class="form-control filter-pd-end"
							value="{{ request()->input('end_date') }}" placeholder="End Date">
				</div>
			</div>
			<div class="pad-t-15 text-center">
				<button id="btnFilter" class="btn btn-info btn-filter-by-price">FILTER NOW</button>
			</div>
		</div>
	</div>
</div>