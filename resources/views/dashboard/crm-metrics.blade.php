@extends('layout.base')

@section('content')
	<div class="panel panel-default page-content mar-l-10 mar-r-10">
		<div class="panel-body">
			<div class="title-sec no-pad">
				<div class="text-center">
					<div class="btn-group" role="group" aria-label="...">
						<a class="btn btn-default"
								href="{{route('crmDashBoard.list')}}">Summary</a>
						<a class="btn btn-default"
								href="{{route('crmDashBoard.metrics')}}">Metrics</a>
						<a class="btn btn-default" href="{{route('crmDashBoard.history')}}">History</a>
					</div>
				</div>
				<div class="pull-left">
					<div class="font-20 pad-t-10">Showing metrics from {{$data['humanReadableStartDate']}} to {{$data['humanReadableEndDate']}}</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-2">

					<h4 class="in-blk pull-left pad-t-10">
						Filter Options
					</h4>
					<div class="in-blk filter-date-btn-wrap pad-t-10 pad-l-5">
						@if($data['filterReset'])
							<a class="btn btn-xs btn-warning" href="{{ route('crmDashBoard.metrics') }}">Reset</a>
						@endif
					</div>
					<div class="clearfix"></div>
					<div class="filter-group ls-none no-mar">

						<div>
							<div class="filter-title pull-left">SELECT DATES</div>
							<div class="text-center pad-t-10">
								<input type="text"
										class="form-control filter-anlytics-date filter-anlytics-creation-start-time text-center"
										id="startTime"
										value="{{date('d-m-Y H:i',$data['startDate'])}}">
							</div>
						</div>

						<div>
							<div class="text-center text-bold filter-title">To</div>
							<div class="text-center pad-b-10">
								<input type="text"
										class="form-control filter-anlytics-date filter-anlytics-creation-end-time text-center"
										value="{{date('d-m-Y H:i',$data['endDate'])}}" id="endTime">
							</div>
						</div>

						<div class="text-center pad-t-10">
							<div class="filter-title pull-left">By handler</div>
							<select class="form-control" name="handler" id="handler">
								<option value="all">All Handlers</option>
								@foreach($data['handlerDetails'] as $handlerDetail)
									<option value="{{$handlerDetail->id}}"
											@if(request()->input('handler')== $handlerDetail->id)selected @endif>{{$handlerDetail->name}}</option>
								@endforeach
							</select>
						</div>

						<div class="text-center filter-date-btn-wrap">
							<a class="btn btn-xs btn-primary btn-filter-by-cd">
								<button class="btn btn-xs btn-primary">FILTER</button>
							</a>
						</div>
					</div>
					<div class="text-muted pad-t-3">
						<i class="glyphicon glyphicon-info-sign"></i>
						Difference of dates should not be more than a week
					</div>
				</div>
				<div class="col-sm-10 pad-t-10">
					<div class="text-info font-16 text-center pad-b-10">Total metrics</div>
					<table class="table table-hover table-responsive table-bordered">
						<thead class="text-info">
						<th>Total Leads</th>
						<th>Pending Processed</th>
						<th>Total Bookings</th>
						<th>Conversion Rate</th>
						<th>Reco. sent</th>
						<th>Avg.reco.rating</th>
						<th>Reco dislikes</th>
						<th>Avg.customer rating</th>
						</thead>
						<tbody>
						@if($data)
							<td>
								@if($data['countOfNewTickets'])
									{{$data['countOfNewTickets']}}
								@else
									{{"---"}}
								@endif
							</td>

							<td>
								@if($data['countOfTotalOrderProcessed'])
									{{$data['countOfTotalOrderProcessed']}}
								@elseif($data['countOfTotalOrderProcessed']==0)
									{{"0"}}
								@else
									{{"---"}}
								@endif
							</td>

							<td>
								@if($data['totalBookings'])
									{{$data['totalBookings']}}
								@else
									{{"---"}}
								@endif
							</td>

							<td>
								@if($data['conversionRate'])
									{{$data['conversionRate']}} %
								@else
									{{"---"}}
								@endif

							</td>

							<td>
								@if($data['recommendationCount'])
									{{$data['recommendationCount']}}
								@else
									{{"---"}}
								@endif
							</td>

							<td>
								@if(($data['recommendationRating']) && ($data['countOfCustomersWhoGaveRatingForRecos']))
									{{$data['recommendationRating']}} ({{$data['countOfCustomersWhoGaveRatingForRecos']}})
								@else
									{{"---"}}
								@endif
							</td>
							<td>
								@if($data['recommendationDislikes'])
									{{$data['recommendationDislikes']}}
								@elseif($data['recommendationDislikes'] == 0)
									{{"0"}}
								@else
									{{"---"}}
								@endif
							</td>

							<td>
								@if(($data['plannerRating']) && ($data['countOfPlannerRating']))
									{{$data['plannerRating']}} ({{$data['countOfPlannerRating']}})
								@else
									{{"---"}}
								@endif
							</td>
						</tbody>
						@else
							{{"No data available"}}
						@endif
					</table>
				</div>
				<div class="col-sm-10">
					<div class="text-center">
						<div class="text-info pad-b-10 font-16">Based on recommendations (By Occasion)</div>
						<table class="table table-hover table-bordered">
							<thead class="thead-default">
							<tr>
								<td colspan="2"></td>
								<td>Sent</td>
								<td>Rated</td>
								<td>Didn't respond</td>
								<td>Avg rating</td>
							</tr>
							</thead>
							<tbody>
							<?php $thisEventDetails = null;?>
							@if($data['individualCRMMetrics']['crmRecoData'])
								@foreach($data['individualCRMMetrics']['crmRecoData'] as $key => $reco)
									<?php $tdCount = 1; $optionCount = 0; ?>
									@foreach($reco as $event => $item)
										<?php $tdCount++; $optionCount++; $thisEventDetails = $data['individualCRMMetrics']['eventDetails']->where('id', $event)->first(); ?>
										<tr>
											@if($optionCount == 1)
												<td rowspan="{{ count($reco) }}">
													@foreach($data['handlerDetails'] as $handler)
														@if($handler->id == $key)
															{{ $handler->name }}
														@endif
													@endforeach
												</td>
											@endif
											<td>
												@if($thisEventDetails)
													{{ $thisEventDetails->name }}
												@else
													{!! $event !!}
												@endif
											</td>
											<td>@if(isset($item['sent'])) {{ $item['sent'] }} @else 0 @endif</td>
											<td>@if(isset($item['rated'])) {{ $item['rated'] }} @else 0 @endif</td>
											<td>@if(isset($item['skipped'])) {{ $item['skipped'] }} @else 0 @endif</td>
											<td>@if(isset($item['rating']) && $item['rated'] != 0)
													{{ round($item['rating']/$item['rated'],2) }} @else 0 @endif</td>
										</tr>
									@endforeach
								@endforeach
							@else
								<tr>
									<td colspan="6">
										N/A
									</td>
								</tr>
							@endif
							</tbody>
						</table>
					</div>
					<?php $count = 0; $thisEventDetails = null; $thisEventStatusDetails = null;?>
					@foreach($data['individualCRMMetrics']['crmBookingData'] as $crmData)
						<div class="text-center">
							<?php $count++ ?>
							<div class="text-info font-16 pad-b-10">
								Based on bookings (@if($count == 1) By Occasion) @else By Source) @endif
							</div>
							<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<td colspan="2"></td>
									<td>Handled</td>
									<td>Booked</td>
									<td>Conversion Rate</td>
								</tr>
								</thead>
								<tbody>
								<?php $tdCount = 1; $thisStatusDetails = null; $thisSourceDetails = null;?>
								@if($crmData)
									@foreach($crmData as $key => $reco)
										<?php $optionCount = 0; ?>
										@foreach($reco as $event => $item)
											<?php $tdCount++; $optionCount++;
											$thisSourceDetails = $data['individualCRMMetrics']['sourceDetails']->where('id', $event)->first();
											$thisStatusDetails = $data['individualCRMMetrics']['statusDetails']->where('id', $event)->first();
											?>
											<tr>
												@if($optionCount == 1)
													<td rowspan="{{ count($reco) }}">
														@foreach($data['handlerDetails'] as $handler)
															@if($handler->id == $key)
																{{ $handler->name }}
															@endif
														@endforeach
													</td>
												@endif
												<td>
													@if($thisSourceDetails)
														{{$thisSourceDetails->name}}
													@elseif($thisStatusDetails)
														{{$thisStatusDetails->name}}
													@endif
												</td>
												<td>@if(isset($item['started'])) {{ $item['started'] }} @else 0 @endif</td>
												<td>@if(isset($item['booked'])) {{ $item['booked'] }} @else 0 @endif</td>
												<td>@if(isset($item['started']) && isset($item['booked']) && $item['started'] != 0)
														{{ round(($item['booked']/$item['started'])*100 ,2)}} @else 0 @endif
												</td>
											</tr>
										@endforeach
									@endforeach
								@else
									<tr>
										<td colspan="5">
											N/A
										</td>
									</tr>
								@endif
								</tbody>
							</table>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			initPlugins();

			function initPlugins() {
				$('.filter-anlytics-creation-start-time, ' +
					'.filter-anlytics-creation-end-time').datetimepicker({
					timepicker: true,
					scrollInput: false,
					scrollMonth: false,
					closeOnDateSelect: false,
					format: "d-m-Y H:i",
					formatTime: "H:i",
				});
			}

			$('.btn-filter-by-cd').click(function (event) { // filter by party date
				event.preventDefault();
				var startDate = $('.filter-anlytics-creation-start-time').val();
				var endDate = $('.filter-anlytics-creation-end-time').val();
				var parts = startDate.split("-");
				var d1 = new Date(parts[2], parts[1] - 1, parts[0]);

				if (!startDate || !endDate) {
					showNotyError("Please select both start date & end date.")
				}
				else {
					var url = '';
					if (startDate) url = formUrl(url, 'filterChosenStartingTime', startDate, false, ['page']);
					if (endDate) url = formUrl(url, 'filterChosenEndingTime', endDate, false, ['page']);
					refreshUrl(url);
				}
			});

			var filterChosenStartingTime = $('#startTime').val();
			var filterChosenEndingTime = $('#endTime').val();
			var handlerId = $('#handler').val();

			function getQueryParams(a) {
				if (a == "") return {};
				var b = {};
				for (var i = 0; i < a.length; ++i) {
					var p = a[i].split('=');
					if (p.length != 2) continue;
					b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
				}
				return b;
			}

			function formUrl(cUrl, key, value, isIgnore, ignores) {
				cUrl = cUrl ? cUrl.substr(cUrl.indexOf('?') + 1).split('&') : window.location.search.substr(1).split('&');
				var cParams = getQueryParams(cUrl), queryString = "", loc = window.location;
				var url = loc.protocol + '//' + loc.host + loc.pathname;
				if (isIgnore) { // remove from current object
					if (cParams.hasOwnProperty(key)) {
						try {
							delete cParams[key];
						}
						catch (e) {
						}
					}
				} else cParams[key] = value;
				for (key in cParams) { // form string
					if (ignores && ignores.constructor === Array && window.indexOf.call(ignores, key) >= 0) continue;
					queryString += key + "=" + cParams[key] + "&";
				}
				if (queryString) {
					url += '?' + queryString;
					url = url.slice(0, -1); // remove trailing '&'
				}
				return url;
			}

			function refreshUrl(url) {
				window.location.href = url;
			}

			function updateUrlByFilters(event) {
				event.preventDefault();

				var handler = $('#handler').val();
				var filterChosenStartingTime = $('#startTime').val();
				var filterChosenEndingTime = $('#endTime').val();
				var url = "metrics?";
				var isUrlChanged = false;

				if (handler) {
					isUrlChanged = true;
					url += '&handler=' + handler;
				}

				if (filterChosenStartingTime) {
					isUrlChanged = true;
					url += '&filterChosenStartingTime=' + filterChosenStartingTime;
				}
				if (filterChosenEndingTime) {
					isUrlChanged = true;
					url += '&filterChosenEndingTime=' + filterChosenEndingTime;
				}
				if (isUrlChanged) {
					location.href = url;
				}
			}

			$('#searchQuery').keyup(function (event) {
				if (event.which == 13 && $(this).val()) { // on enter submit search
					updateUrlByFilters(event);
				}
			});

			// filter options
			$('#handler').change(updateUrlByFilters);
		});
	</script>
@endsection
