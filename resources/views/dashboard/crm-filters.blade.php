<div class="filter-group-options ls-none no-mar no-pad">
	<h4 class="in-blk pull-left">
		Filter Options
	</h4>
	<div class="in-blk filter-date-btn-wrap pad-t-3 pad-l-5">
		@if($data['filterReset'])
			<a class="btn btn-xs btn-warning" href="{{ route('crmDashBoard.new.tickets.list') }}">Reset</a>
		@endif
	</div>
	<div class="clearfix"></div>

	<div class="filter-group">

		<div class="text-center">
			<div class="filter-title pull-left">By Occasion</div>
			<select class="form-control" name="occasion" id="occasion">
				<option value="all">All Occasions</option>
				@foreach($data['typeEvents'] as $key => $event)
					<option value="{{$key}}"
							@if(request()->input('occasion')== $key)selected @endif>{{$event}}</option>
				@endforeach
			</select>
		</div>

		<div class="text-center pad-t-10">
			<div class="filter-title pull-left">By lead Status</div>
			<select class="form-control pad-t-8" name="lead-status" id="ticketLeadStatusForAnalytics">
				<option value="all">All Lead status</option>
				<option value="notSet"
						@if(request()->input('leadStatus')== "notSet") selected @endif>Not set
				</option>
				@foreach($data['typeLeadStatus'] as $key => $leadStatus)
					<option value="{{$key}}"
							@if(request()->input('leadStatus')== $key)selected @endif>{{$leadStatus}}</option>
				@endforeach
			</select>
		</div>

		<div class="text-center pad-t-10">
			<div class="filter-title pull-left">By Ticket Status</div>
			<select class="form-control" name="ticket-status" id="ticketStatus">
				<option value="all">All Ticket status</option>
				@foreach($data['typeTicketStatus'] as $key => $typeTicketStatus)
					<option value="{{$key}}"
							@if(request()->input('ticketStatus')== $key)selected @endif>{{$typeTicketStatus}}</option>
				@endforeach
			</select>
		</div>

		<div class="text-center pad-t-10">
			<div class="filter-title pull-left">By handler</div>
			<select class="form-control" name="handler" id="handler">
				<option value="all">All Handlers</option>
				@foreach($data['handlerDetails'] as $key => $handlerDetail)
					<option value="{{$key}}"
							@if(request()->input('handler')== $key)selected @endif>{{$handlerDetail}}</option>
				@endforeach
			</select>
		</div>

		<div class="text-center pad-t-10">
			<div class="filter-title pull-left">By Created handler</div>
			<select class="form-control" name="createdByHandler" id="createdByHandler">
				<option value="all">All Handlers</option>
				@foreach($data['handlerDetails'] as $key => $handlerDetail)
					<option value="{{$key}}"
							@if(request()->input('createdByHandler')== $key)selected @endif>{{$handlerDetail}}</option>
				@endforeach
			</select>
		</div>


	</div>

</div>
<div class="text-muted pad-t-10 pad-b-10">
	<i class="glyphicon glyphicon-info-sign"></i> Difference of dates should not be more than 7 days
</div>