@extends("dashboard.piab.base")

@section('listData')

	<div class="data-sec mar-t-20 pad-b-20" id="productBookings">

		@if(isset($data['productBookingData']))
			<table id="productBookingData" class="table table-bordered table-responsive table-hover">
				<thead class="text-info">
				<tr>
					<th>Created At</th>
					<th>Customer Details</th>
					<th>Product Category</th>
					<th>Product Info</th>
					<th>Product Style</th>
					<th>Booking Amount</th>
					<th>City</th>
					<th>State</th>
					<th>Status</th>
					<th>Payment Done At</th>
				</tr>
				</thead>
				<tbody>
				@foreach($data['productBookingData'] as $productBooking)
					<tr>
						<td>
							<div class="text-white font-1">[{{ date('y/m/d H:i', strtotime($productBooking["createdAt"])) }}]</div>
							@if(isset($productBooking['createdAt']) && $productBooking['createdAt'])
								{{ $productBooking['createdAt']}}
							@else
								--
							@endif
							<div>
								<a class="btn btn-info mar-t-10" href="product-bookings/{{$productBooking['productBookingId']}}/info" target="_blank">
									<i class="glyphicon glyphicon-info-sign"></i> Order Info
								</a>
							</div>

						</td>
						<td>
							<a href="product-bookings/{{$productBooking['productBookingId']}}/info" target="_blank">
								<div>
									@if(isset($productBooking['customerName']) && $productBooking['customerName'])
										{{ ucfirst($productBooking['customerName']) }}
									@else
										--
									@endif
								</div>
								<div>
									@if(isset($productBooking['customerPhone']) && $productBooking['customerPhone'])
										{{ ucfirst($productBooking['customerPhone']) }}
									@else
										--
									@endif
								</div>
							</a>
						</td>
						<td>
							@if(isset($productBooking['productCategory']) && $productBooking['productCategory'])
								{{ ucfirst($productBooking['productCategory']) }}
							@else
								--
							@endif
						</td>
						<td>
							<div>
								@if(isset($productBooking['productName']) && $productBooking['productName'])
									{{ ucfirst($productBooking['productName']) }}
								@else
									--
								@endif
							</div>
						</td>
						<td>
							<div>
								@if(isset($productBooking['productStyle']) && $productBooking['productStyle'])
									{{ ucfirst($productBooking['productStyle']) }}
								@else
									--
								@endif
							</div>
						</td>

						<td>
							<div class="align-center">
								@if(isset($productBooking['productPrice']) && $productBooking['productPrice'])
									<div> Product Price : {{$productBooking['productPrice'] }}</div>
								@else
									--
								@endif
							</div>
							<div>
								@if(isset($productBooking['deliveryCharge']))
									<div> Delivery Charges :
										@if($productBooking['deliveryCharge']!=0)
											{{$productBooking['deliveryCharge'] }}
										@else
											{{" -- "}}
										@endif

									</div>
								@endif
							</div>
							<div>
								@if(isset($productBooking['couponDiscount']))
									<div> Coupon Discount :
										@if($productBooking['couponDiscount']!=0)
											{{$productBooking['couponDiscount'] }}
										@else
											{{ "--" }}
										@endif
									</div>
								@endif
							</div>
							<br>
							<div class="text-bold">
								<div> Total Amount : {{ $productBooking['productPrice']+$productBooking['deliveryCharge']-$productBooking['couponDiscount'] }}</div>
							</div>

						</td>
						<td>
							@if(isset($productBooking['city']) && $productBooking['city'])
								{{ ucfirst($productBooking['city']) }}
							@else
								--
							@endif
						</td>

						<td>
							<div>
								@if(isset($productBooking['state']) && $productBooking['state'])
									{{ ucfirst($productBooking['state']) }}
								@else
									--
								@endif
							</div>
						</td>

						<td>
							@if(isset($productBooking['status']) && $productBooking['status'])
								{{ ucfirst($productBooking['status']) }}
							@else
								--
							@endif
						</td>

						<td>
							@if(isset($productBooking['paymentDate']) && $productBooking['paymentDate'])
								<div> {{ $productBooking['paymentDate']}}</div>
							@else
								--
							@endif
						</td>

					</tr>
				@endforeach
				</tbody>
			</table>
		@elseif(isset($data["errorMsg"]) && $data["errorMsg"] )
			<br><br>
			<div class="alert-danger text-center text-bold pad-10" style="position: relative;top:30px;width:auto;">
				<i class="glyphicon glyphicon-warning-sign"></i>
				{!! $data["errorMsg"] !!}
			</div>
		@else
			<br><br>
			<div class="alert-danger text-center text-bold pad-10" style="position: relative;width:auto;">
				<i class="glyphicon glyphicon-eye-open"></i>
				No product Bookings were found.
			</div>
		@endif
	</div>
@endsection

@section('javascript')
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#productBookingData').DataTable({
				"fixedHeader": true,
				"order": [[ 0, "desc" ]],
				"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]]
			});
		});
	</script>

@endsection
