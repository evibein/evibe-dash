@extends('dashboard.piab.base')

@section('content')
	<div id="productBookingActions" class="product-booking-actions-wrap mar-t-25">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel-heading font-20 text-center">
				<div class="product-booking-heading">
					{{'# '.$data['productBookingInfo']['productBookingId']}}
				</div>
				<div class="product-booking-heading">
					<i class="glyphicon glyphicon-time"> </i>{{' '. $data['productBookingInfo']['createdAt']}}
				</div>
				<div class="product-booking-heading">
					<i class="glyphicon glyphicon-bullhorn"></i>{{' '.$data['productBookingInfo']['status']}}
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div id="ticketUpdates" class="ticket-actions-wrap mar-t-25">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div>
						<span class="font-20">Product Booking Updates</span>
						<button class="btn btn-primary product-booking-update-btn form-group mar-l-20">
							<i class="glyphicon glyphicon-plus mar-r-5"></i>Add New Update
						</button>
						<div class="pull-right">
						<span class="evibe-info text-normal font-12">
							<span class="badge font-12 evibe-badge">{{count($data['productBookingUpdates'])}}</span>
							<span class="mar-l-2">Updates</span>
						</span>
						</div>
					</div>
					<br>
					<div id="productBooking body " class="panel-body ">
						@if (count($data['productBookingUpdates']))
							<ul class="ls-none mar-t-15 status-updates-wrap">
								@foreach ($data['productBookingUpdates'] as $update)
									@if(isset($update))
										<li @if(!$update['handlerName']) class="bg-auto-update" @endif>
											[ {{ AppUtil::formatDateTimeForBooking($update['statusAt']) }} ] ==> Status updated to
											<b>{{ $update['statusName'] }}</b> by
											@if ($update['handlerName'])
												<u>{{ $update['handlerName'] }}</u>.
											@else
												<u>Auto.</u>
											@endif
											@if ($update['comments'])
												<span>with <b>comments:</b> {!! $update['comments'] !!}</span>
											@endif
										</li>
									@endif
								@endforeach
							</ul>
						@else
							<div class="text-danger text-bold">No updates were found for this ticket.</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="product-booking-info mar-t-10" id="productBookinginfo">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="sec-sub-title in-blk mar-r-10">Product Booking info</h4>
					<table class="table table-bordered" style="margin-top:20px">
						<tr>
							<th>
								Name
							</th>
							<td>
								@if(isset($data['productBookingInfo']['customerName']) &&$data['productBookingInfo']['customerName'])
									@if(isset($data['productBookingInfo']['customerGender']) && $data['productBookingInfo']['customerGender']==1)
										{{'Mr.'}}
									@elseif(isset($data['productBookingInfo']['customerGender']) && $data['productBookingInfo']['customerGender']==2)
										{{'Ms.'}}
									@endif
									{{ ucfirst($data['productBookingInfo']['customerName']) }}
								@else
									--
								@endif
							</td>
							<th>Booking Id</th>
							<td>
								@if(isset($data['productBookingInfo']['productBookingId']) && $data['productBookingInfo']['productBookingId'])
									{{ $data['productBookingInfo']['productBookingId'] }}
								@else
									--
								@endif
							</td>
						</tr>
						<tr>
							<th>Phone Number</th>
							<td>
								@if(isset($data['productBookingInfo']['customerPhone']) && $data['productBookingInfo']['customerPhone'])
									@if(isset($data['productBookingInfo']['callingCode']) && $data['productBookingInfo']['callingCode'])
										{{$data['productBookingInfo']['callingCode']}}
									@endif
									{{ $data['productBookingInfo']['customerPhone'] }}
								@else
									--
								@endif
							</td>
							<th>Status</th>
							<td>
								@if(isset($data['productBookingInfo']['status']) && $data['productBookingInfo']['status'])
									{{ $data['productBookingInfo']['status'] }}
								@else
									--
								@endif
							</td>
						</tr>
						<tr>
							<th>Email</th>
							<td>
								@if(isset($data['productBookingInfo']['customerEmail']) && $data['productBookingInfo']['customerEmail'])
									{{ $data['productBookingInfo']['customerEmail'] }}
								@else
									--
								@endif
							</td>
							<th>Product Category</th>
							<td>
								@if(isset($data['productBookingInfo']['productCategory']) && $data['productBookingInfo']['productCategory'])
									{{ $data['productBookingInfo']['productCategory'] }}
								@else
									--
								@endif
							</td>
						</tr>
						<tr>
							<th>Address</th>
							<td>
								@if(isset($data['productBookingInfo']['address']) && $data['productBookingInfo']['address'])
									{{ $data['productBookingInfo']['address'] }}
								@else
									--
								@endif
							</td>
							<th>Product Info</th>
							<td>
								@if(isset($data['productBookingInfo']['productName']) && $data['productBookingInfo']['productName'])
									{{ $data['productBookingInfo']['productName'] }}
								@else
									--
								@endif
							</td>
						</tr>
						<tr>

							<th>Landmark</th>
							<td>
								@if(isset($data['productBookingInfo']['landmark']) && $data['productBookingInfo']['landmark'])
									{{ $data['productBookingInfo']['landmark'] }}
								@else
									--
								@endif
							</td>

							<th>Product Style</th>
							<td>
								@if(isset($data['productBookingInfo']['productStyle']) && $data['productBookingInfo']['productStyle'])
									{{ $data['productBookingInfo']['productStyle'] }}
								@else
									--
								@endif
							</td>
						</tr>
						<tr>

							<th>Pin code</th>
							<td>
								@if(isset($data['productBookingInfo']['zipCode']))
									{{ $data['productBookingInfo']['zipCode'] }}
								@else
									--
								@endif
							</td>

							<th>Product Price</th>
							<td>
								@if(isset($data['productBookingInfo']['productPrice']))
									{{ $data['productBookingInfo']['productPrice'] }}
								@else
									--
								@endif
							</td>
						</tr>
						<tr>
							<th>City</th>
							<td>@if(isset($data['productBookingInfo']['city']) && $data['productBookingInfo']['city'])
									{{ $data['productBookingInfo']['city'] }}
								@else
									--
								@endif</td>

							<th>Delivery charge</th>
							<td>
								@if(isset($data['productBookingInfo']['deliveryCharge']))
									{{ $data['productBookingInfo']['deliveryCharge'] }}
								@else
									--
								@endif
							</td>
						</tr>
						<tr>
							<th>State</th>
							<td>
								@if(isset($data['productBookingInfo']['state']) && $data['productBookingInfo']['state'])
									{{ $data['productBookingInfo']['state'] }}
								@else
									--
								@endif
							</td>

							<th>Coupon discount</th>
							<td>
								@if(isset($data['productBookingInfo']['couponDiscount']))
									{{ $data['productBookingInfo']['couponDiscount'] }}
								@else
									--
								@endif
								@if(isset($data['productBookingInfo']['couponCode']))
									[{{$data['productBookingInfo']['couponCode']}}]
								@endif

							</td>
						</tr>
						<tr>
							<th>Alt. Phone</th>
							<td>
								@if(isset($data['productBookingInfo']['altPhone']) && $data['productBookingInfo']['altPhone'])
									{{ $data['productBookingInfo']['altPhone'] }}
								@else
									--
								@endif
							</td>

							<th>Booking amount</th>
							<td>{{ $data['productBookingInfo']['productPrice']-$data['productBookingInfo']['couponDiscount'] }}</td>
						</tr>
						<tr>
							<th>Alt. Email</th>
							<td>
								@if(isset($data['productBookingInfo']['altEmail']) && $data['productBookingInfo']['altEmail'])
									{{ $data['productBookingInfo']['altEmail'] }}
								@else
									--
								@endif
							</td>

							<th>Payment Done At</th>
							<td>
								@if(isset($data['productBookingInfo']['paymentDate']) && $data['productBookingInfo']['paymentDate'])
									{{ $data['productBookingInfo']['paymentDate'] }}
								@else
									--
								@endif
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div id="productBookingsUpdateModal" class="modal fade in hide" style="display: block;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Please fill the details</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="type-status col-sm-6">
							<div class="form-group">
								<label for="updateStatus"> Status* </label>
								<select id="updateStatus" name="updateStatus" class="form-control " required>
									<option selected disabled>--Select Status Type--</option>
									@if(isset($data['productBookingStatus']) && count($data['productBookingStatus']))
										@foreach($data['productBookingStatus'] as $option)
											<option value="{{$option['statusId']}}">{{$option['statusName']}}</option>
										@endforeach
									@endif
								</select>
							</div>
						</div>
						<input type="hidden" id="hidBookingStatusDelivered" value="{{ config('evibe.product-booking-status.delivered') }}">
						<input type="hidden" id="hidBookingStatusCancelled" value="{{ config('evibe.product-booking-status.cancelled') }}">
						<div class="form-group col-sm-6 hide">
							<label for="expectedDeliveryDate">Expected Delivery Date</label>
							<input id="expectedDeliveryDate" name="expectedDeliveryDate" type="text" class="form-control"
									@if(isset($data['productBookingInfo']['deliveryDate'])) value="{{ date('Y-m-d H:i', strtotime($data['productBookingInfo']['deliveryDate'])) }}"@endif>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<label for="updateComments">Enter Comments</label>
						<textarea id="updateComments" name="updateComments" rows="5" placeholder="Type your text here(minimum length of 4 letters)" class="form-control" required=""></textarea>
					</div>
					<div class="form-group btn btn-info update-customer" style="width:50%; background: #c9c9c9;border:0px">
						<input type="checkbox" class="no-pad-l" id="isUpdateCustomer" name="isUpdateCustomer" value="1"/>
						<label class="font-14 text-black">Send update to customer</label>
					</div>
					<div class="pull-right">
						<a class="btn btn-default btn-close mar-r-10" data-dismiss="modal">Close</a>
						<input class="btn btn-primary btn-status-update mar-l-5" data-id="{{$data['productBookingInfo']['id']}}" type="button" value="update"/>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$('.close,.btn-close').on('click', function () {
				$('#productBookingsUpdateModal').addClass('hide');
				$('body').css('overflow', 'scroll');
			});

			$('.product-booking-update-btn').on('click', function () {
				$('body').css('overflow', 'hidden');
				$('#productBookingsUpdateModal').removeClass('hide');
			});
			$('#expectedDeliveryDate').datetimepicker({
				step: 30,
				minDate: 0,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: false,
				maxDate: false,
				timepicker: true,
				format: 'Y-m-d H:i'
			});

			$('#updateStatus').on('change', function () {
				var $presentStatus = $(this).val();

				if(($presentStatus == $('#hidBookingStatusDelivered').val()) || ($presentStatus == $('#hidBookingStatusCancelled').val())) {
					$('#expectedDeliveryDate').parent().addClass('hide');
				}
				else {
					$('#expectedDeliveryDate').parent().removeClass('hide');
				}
			});

			$(".update-customer").click(function () {
				$('input[name=isUpdateCustomer]').click();
			});

			$('.btn-status-update').on('click', function () {
				$('.btn-status-update').attr("disabled", true);

				$('#productBookingsUpdateModal').addClass('hide');
				window.showLoading();

				var productBookingId = $(this).data("id");
				var isCustomerUpdate = 0;

				if ($("#isUpdateCustomer").is(":checked")) {
					isCustomerUpdate = 1;
				}

				$.ajax({
					url: '/piab-dashboard/product-bookings/update-status',
					type: 'POST',
					dataType: 'JSON',
					data: {
						productBookingId: productBookingId,
						comment: $('#updateComments').val(),
						statusId: $('#updateStatus').val(),
						expectedDeliveryDate: $('#expectedDeliveryDate').val(),
						isCustomerUpdate: isCustomerUpdate
					},
					success: function (data) {
						window.hideLoading();
						if (data.success) {
							showNotySuccess(data.successMsg);
							setTimeout(function () {
								window.location.reload();
							}, 1000);
						} else {
							var errorMsg = "Some error occurred while updating the status. Kindly refresh the page and try again.";
							if (data.errorMsg) {
								errorMsg = data.errorMsg;
								$('#productBookingsUpdateModal').removeClass('hide');
							}
							showNotyError(errorMsg);
							$('.btn-status-update').attr("disabled", false);

						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.hideLoading();
						showNotyError("Error occurred while updating status ");
						$('.btn-status-update').attr("disabled", false);
					}
				})

			});

		});
	</script>
@endsection