@extends('layout.base')

@section('custom-head')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
@endsection

@section('content')
	<div class="panel panel-default page-content no-border">
		<div class="panel-body pad-b-10">
			<div class="title-sec">
				<div>
					<div class="col-sm-6 col-sm-offset-3 text-center">
						<div class="btn-group" role="group" aria-label="...">
							<a class="btn btn-default @if(request()->is('*product-bookings*')) active @endif"
									href="{{route('piab-dashboard.product-bookings.list')}}">Product Bookings
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="content-sec">
				<div class="col-xs-12">
					@yield("listData")
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection
