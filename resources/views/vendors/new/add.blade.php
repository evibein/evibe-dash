@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
			<div class="app-tabs">
				@include('app.tabs')
			</div>
			<ol class="breadcrumb">
				<li><a href="{{ route('vendor.list') }}">Shasow All Vendors</a></li>
			</ol>
			<div class="page-content vendors-wrap">
				<div class="vendors-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="pull-left">
								<h2 class="panel-title">Add New Vendor</h2>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							@if ($errors->count() > 0 || session()->has('custom_error'))
								<div class="errors-cnt alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;
									</button>
									<ul class="erros-list">
										<li>{{ $errors->first() }}</li>
										@if (session()->has('custom_error'))
											<li>{{ session('custom_error') }}</li>
										@endif
									</ul>
								</div>
							@endif
							<form action="{{ route('vendor.new.save') }}" method="POST" role="form">
								<div class="alert alert-warning">
									<h4 class="text-center">Contact Information</h4>

									<div>
										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
											<label>Type</label>
											<select id="type" class="form-control" name="type"
													data-old="@if(old('type')){{ old('type') }}@else 0 @endif">
												<option value="0">Non-venue</option>
												<option value="1">Venue</option>
											</select>
										</div>
									</div>
									<div>
										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
											<div class="form-group vendor-type">
												<label>
													<span>Vendor Type</span>
												</label>
												<select name="vendorType" id="vendorType" class="form-control"
														data-old="@if(old('vendorType')){{old('vendorType')}}@else 1 @endif">
													@foreach ($data['vendorTypes'] as $vendorType)
														<option value="{{ $vendorType->id }}">{{ $vendorType->name }}</option>
													@endforeach
												</select>
												<input type="hidden" name="roleId" id="roleId" value=""/>
											</div>
											<div class="form-group venue-type hide">
												<label>
													<span>Venue Type</span>
												</label>
												<select name="venueType" id="venueType" class="form-control"
														data-old="@if(old('venueType')){{ old('venueType') }} @else 1 @endif">
													@foreach ($data['venueTypes'] as $venueType)
														<option value="{{ $venueType->id }}">{{ $venueType->name }}</option>
													@endforeach
												</select>
												<input type="hidden" name="roleId" id="roleId" value=""/>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Company Name*</span>
												</label>
												<input id="vendorName" name="vendorName" type="text"
														class="form-control"
														placeholder="Enter company name"
														value="{{ old('vendorName') }}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label><span>Contact Person*</span></label>
												<input id="vendorPerson" name="vendorPerson" type="text"
														class="form-control"
														placeholder="Enter contact person name"
														value="{{ old('vendorPerson') }}">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label title="Enter at-least one from email and phone">Phone Number **</label>
												<input id="vendorPhone" name="vendorPhone" type="text"
														class="form-control"
														placeholder="Enter contact number"
														value="{{ old('vendorPhone') }}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label title="Enter at-least one from email and phone">Email **</label>
												<input id="vendorEmail" name="vendorEmail" type="text"
														class="form-control"
														placeholder="Enter email address"
														value="{{ old('vendorEmail') }}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>City</span>
												</label>
												<select name="city" id="city" class="form-control"
														data-old="{{ old('city') }}">
													@foreach ($data['cities'] as $city)
														<option value="{{ $city->id }}">{{ $city->name }}</option>
													@endforeach
												</select>
												<ul id="allAreaOptions" class="hide">
													@foreach ($data['area'] as $area)
														<li data-id="{{ $area->id }}"
																data-city_id="{{ $area->city_id }}"
																data-name="{{ $area->name }}"></li>
													@endforeach
												</ul>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Email 1</label>
												<input type="text" name="altEmail1" class="form-control"
														placeholder="Enter alternative email"
														value="{{old('altEmail1')}}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Email 2</label>
												<input type="text" name="altEmail2" class="form-control"
														placeholder="Enter alternative email"
														value="{{old('altEmail2')}}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Phone*</label>
												<input type="text" name="altPhone" class="form-control"
														placeholder="Enter alternative phone"
														value="{{old('altPhone')}}">
											</div>
										</div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Location</span>
													<a href="#" class="btn-add-new-location text-normal"
															data-redirectto="vendors/new">(Add New Location)</a>
												</label>
												<select name="area" id="area" class="form-control"
														data-old="{{ old('area') }}">
													@foreach ($data['areas'] as $area)
														<option value="{{ $area->id }}">{{ $area->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Sub Location</span>
												</label>
												<input id="vendorSubLocation" name="vendorSubLocation" type="text"
														class="form-control"
														placeholder="Enter sub location"
														value="{{ old('vendorSubLocation') }}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>ZIP Code* (To calculate transport charges)</span>
												</label>
												<input id="vendorZipCode" name="vendorZipCode" type="text"
														class="form-control"
														placeholder="Enter ZIP Code"
														value="{{ old('vendorZipCode') }}">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="source">Source Raised Through*</label>
												<select name="source" id="source" class="form-control">
													<option value="-1"
															@if(old('source') == -1) selected @endif>-- Select Source --
													</option>
													@foreach ($data['source'] as $source)
														<option value="{{ $source->id }}"
																@if(old('source') == $source->id) selected @endif>{{ $source->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Source Specific</label>
												<input type="text" name="sourceSpecific" class="form-control"
														placeholder="Enter specific source"
														value="{{old('sourceSpecific')}}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Facebook url</label>
												<input type="text" name="sourceFacebook" class="form-control"
														placeholder="Enter company facebook profile"
														value="{{old('sourceFacebook')}}">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Google plus url</label>
												<input type="text" name="sourceGooglePlus" class="form-control"
														placeholder="Enter company google plus profile"
														value="{{old('sourceGooglePlus')}}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Instagram url</label>
												<input type="text" name="sourceInstagram" class="form-control"
														placeholder="Enter company instagram profile"
														value="{{old('sourceInstagram')}}">
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="leadStatus">Lead Status **</label>
												<select name="leadStatus" id="leadStatus" class="form-control">
													<option value="-1"
															@if(old('leadstatus') == -1) selected @endif>-- Select Lead Status --
													</option>
													@foreach ($data['leadStatus'] as $status)
														<option value="{{ $status->id }}"
																@if(old('leadStatus') == $status->id) selected @endif>{{ $status->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="handler"><span>Handled By</span></label>
												<select name="handler" id="handler" class="form-control">
													@foreach ($data['handlers'] as $handler)
										vMappingLongitude				<option value="{{ $handler->id }}"
																@if(old('handler') == $handler->id) selected @endif>{{ $handler->name }}</option>
													@endforeach
												</select>
											</div>
										</div>

										<div class="pad-r-20 pad-l-10 col-sm-4 col-md-4 col-lg-4 col-xs-12 vMappingLongitude hide">
											<div class="form-group">
												<label for="vMappingLongitude">Longitude</label>
												<input type="text"
														name="vMappingLongitude"
														id="vMappingLongitude"
														class="form-control"
														placeholder="Enter the longitude">
											</div>
										</div>
										<div class="pad-r-20 pad-l-10 col-sm-4 col-md-4 col-lg-4 col-xs-12 vMappingLatitude hide">
											<div class="form-group">
												<label for="mapCoordinates">Latitude</label>
												<input class="form-control" type="text"
														name="vMappingLatitude"
														id="vMappingLatitude"
														placeholder="Enter the latitude">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="comments">Comments</label>
												<textarea id="comments" name="comments"
														class="form-control vertical-resize"
														rows="4" placeholder="enter comments..."
														data-val="{{ old('comments') }}">{{{ Input::old('comments') }}}</textarea>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
										<div class="form-group">
											<a class="pad-l-10 btn btn-danger  mar-l-10"
													href="{{ route('vendor.new.list') }}">
												<i class="glyphicon glyphicon-remove"></i> Cancel</a>
											<button type="submit" class="btn btn-primary mar-l-10 "><i
														class="glyphicon glyphicon-floppy-save"></i> Submit
											</button>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/vendors.js");

			//select venue/non-venue
			function typeToggle(type) {
				$('.venue-type').toggleClass(type);
				$('.vendor-type').toggleClass(type);
				$('.vMappingLongitude').toggleClass('hide');
				$('.vMappingLatitude').toggleClass('hide');
			}

			$('#type').on('change', function (event) {
				event.preventDefault();
				typeToggle('hide');
			});

			//set the value for type,venue_type
			$arrayIds = ['#venueType', '#type', '#vendorType'];
			$.each($arrayIds, function (key, value) {
				var oldVal = $(value).data('old');
				$(value).val($.trim(oldVal));
			});

			if ($('#type').val() == 1) {
				typeToggle('hide');
			}

		});
	</script>
@stop