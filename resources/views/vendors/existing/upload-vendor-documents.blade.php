@extends('layout.base')

@section('content')
	<div class="container">
		<div class="mar-b-10">
			@if($partnerTypeId == config('evibe.ticket_type.planners'))
				<a class="font-16" href="{{ route('vendor.view', $partnerId) }}"><< Go To Profile</a>
			@else
				<a class="font-16" href="{{ route('venues.view',$partnerId) }}"><< Go To Profile</a>
			@endif
		</div>
		<div class="upload-documents">
			<h2 class="text-center">Upload Docouments</h2>
			<div class="alert-info pad-10 mar-b-15">
				<i class="glyphicon glyphicon-eye-open"></i>
				Please upload only PDF or image files and file size should be less than 2MB.
			</div>

			@if ($errors->count() > 0)
				<div class="mar-t-20 ">
					<div class="col-sm-12">
						<div class="alert alert-danger pad-10">
							<ul class="erros-list ls-none">
								<li>{{ $errors->first() }}</li>
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			@endif

			@if(session()->has('successMessage'))
				<div class="alert alert-success mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert"
							aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-remove-sign"></i>
					{{ session('successMessage') }}
				</div>
			@endif

			@if(session()->has('failureMessage'))
				<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert"
							aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-remove-sign"></i>
					{{ session('failureMessage') }}
				</div>
			@endif

			@if(session()->has('fileUploadFailure'))
				<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert"
							aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-remove-sign"></i>
					{{ session('fileUploadFailure') }}
				</div>
			@endif
			<form action="{{ route('save.documents', [$partnerId,$partnerTypeId]) }}" method="post"
					enctype="multipart/form-data">
				<div class="col-sm-6 col-sm-offset-3">
					<label>Select document type *</label>
					<select name="type_document_id" class="form-control mar-b-15" autofocus>
						<option value="">choose the document</option>
						@foreach($typeDocumentIds as $type_document)
							<option value="{{$type_document->id}}">{{$type_document->name}}</option>
						@endforeach
					</select>
					<label>Enter document name</label>
					<input type="text" class="form-control" name="title" size="40"
							placeholder="eg. Aadhar Card, Driving License etc."><br/><br/>
					<input type="file" name="uploaded_file" class="mar-b-15">
					<div class="text-center">
						<button type="submit" value="Submit" class="btn btn-primary mar-r-15">Upload</button>
					</div>
				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
@stop