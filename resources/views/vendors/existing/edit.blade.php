@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
			<div class="app-tabs">
				@include('app.tabs')
			</div>
			<ol class="breadcrumb no-mar">
				<li><a href="{{ route('vendor.list') }}"><< Show All Vendors</a></li>
			</ol>
			<div class="page-content vendors-wrap">
				<div class="vendors-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="pull-left">
								<h2 class="panel-title">Edit Vendor Details</h2>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							@if ($errors->count() > 0)
								<div class="errors-cnt alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;
									</button>
									<ul class="erros-list">
										<li>{{ $errors->first() }}</li>
									</ul>
								</div>
							@endif
							<form action="{{ route('vendor.edit.save', $data['vendor']->id) }}" method="POST"
									role="form">
								<div class="alert alert-warning">
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Vendor Type</span>
												</label>
												<select name="vendorType" id="vendorType" class="form-control">
													@foreach ($data['vendorTypes'] as $vendorType)
														<option value="{{ $vendorType->id }}"
																data-role="{{ $vendorType->role_id }}"
																@if((old('vendorType') && $vendorType->id == old('vendorType')) || (old('vendorType') == '' && $vendorType->id == $data['vendor']->type_id)) selected @endif>
															{{ $vendorType->name }}
														</option>
													@endforeach
												</select>
												<input type="hidden" name="roleId" id="roleId" value=""/>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Company Name</span>
												</label>
												<input id="vendorName" name="vendorName" type="text"
														class="form-control"
														placeholder="Enter company name"
														value="@if (old('vendorName')){{ old('vendorName') }}@else{{ $data['vendor']->name }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label><span>Contact Person</span></label>
												<input id="vendorPerson" name="vendorPerson" type="text"
														class="form-control"
														placeholder="Enter contact person name"
														value="@if (old('vendorPerson')){{ old('vendorPerson') }}@else{{ $data['vendor']->person }}@endif">
											</div>
										</div>

										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Phone Number</label>
												<input id="vendorPhone" name="vendorPhone" type="text"
														class="form-control"
														placeholder="Enter contact number"
														value="@if (old('vendorPhone')){{ old('vendorPhone') }}@else{{ $data['vendor']->phone }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>City</span>
												</label>
												<select name="city" id="city" class="form-control"
														data-old="@if(old('city')){{ old('city') }}@elseif($data['vendor']->city_id){{ $data['vendor']->city_id }}@endif">
													@foreach ($data['cities'] as $city)
														<option value="{{ $city->id }}">{{ $city->name }}</option>
													@endforeach
												</select>
												<ul id="allAreaOptions" class="hide">
													@foreach ($data['areas'] as $area)
														<li data-id="{{ $area->id }}"
																data-city_id="{{ $area->city_id }}"
																data-name="{{ $area->name }}"></li>
													@endforeach
												</ul>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Location</span>
													<a href="#" class="btn-add-new-location text-normal"
															data-redirectto="vendors/new">(Add New Location)</a>
												</label>
												<select name="area" id="area" class="form-control"
														data-old="@if(old('area')){{ old('area') }}@elseif($data['vendor']->area_id){{ $data['vendor']->area_id }}@endif">
													@foreach ($data['areas'] as $area)
														<option value="{{ $area->id }}">
															{{ $area->name }}
														</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Email 1</label>
												<input type="text" name="altEmail1" class="form-control"
														placeholder="Enter alternative email"
														value="@if(old('altEmail1')){{ old('altEmail1') }}@else{{ $data['vendor']->alt_email_1 }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Email 2</label>
												<input type="text" name="altEmail2" class="form-control"
														placeholder="Enter alternative email"
														value="@if(old('altEmail2')){{ old('altEmail2') }}@else{{ $data['vendor']->alt_email_2 }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Phone</label>
												<input type="text" name="altPhone" class="form-control"
														placeholder="Enter alternative phone"
														value="@if(old('altPhone')){{ old('altPhone') }}@else{{ $data['vendor']->alt_phone }}@endif">
											</div>
										</div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label><span>Sub Location</span></label>
												<input id="vendorSubLocation" name="vendorSubLocation" type="text"
														class="form-control"
														placeholder="Enter sub location"
														value="@if (old('vendorSubLocation')){{ old('vendorSubLocation') }}@else{{ $data['vendor']->sub_location }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label><span>ZIP Code</span></label>
												<input id="vendorZipCode" name="vendorZipCode" type="text"
														class="form-control"
														placeholder="Enter ZIP Code"
														value="@if (old('vendorZipCode')){{ old('vendorZipCode') }}@else{{ $data['vendor']->zip }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Short URL</span>
													<span class="text-muted small-font text-normal">(Optional)</span>
												</label>
												<input id="vendorShortURL" name="vendorShortURL" type="text"
														class="form-control"
														placeholder="Enter sub location"
														value="@if (old('vendorShortURL')){{ old('vendorShortURL') }}@else{{ $data['vendor']->short_url }}@endif">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label><span>Commission %</span></label>
												<input id="vendorCommissionRate" name="vendorCommissionRate" type="text"
														class="form-control"
														placeholder="Enter commission %"
														value="@if (old('vendorCommissionRate')){{ old('vendorCommissionRate') }}@else{{ $data['vendor']->commission_rate }}@endif">
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="source">Source Raised Through</label>
												<select name="source" id="source" class="form-control">
													@foreach ($data['source'] as $source)
														<option value="{{ $source->id }}"
																@if($data['vendor']->source_id == 	$source->id) selected @endif>{{ $source->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Source Specific</label>
												<input id="sourceSpecific" name="sourceSpecific" type="text"
														class="form-control"
														placeholder="Enter specific source"
														value="@if(old('sourceSpecific')){{ old('sourceSpecific') }}@else{{$data['vendor']->source_specific}}@endif">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Facebook url</label>
												<input id="sourceFacebook" name="sourceFacebook" type="text"
														class="form-control"
														placeholder="Enter company facebook profile"
														value="@if(old('sourceFacebook')){{ old('sourceFacebook') }}@else{{$data['vendor']->facebook_url}}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Google-plus url</label>
												<input id="sourceGooglePlus" name="sourceGooglePlus" type="text"
														class="form-control"
														placeholder="Enter company google plus profile"
														value="@if(old('sourceGooglePlus')){{ old('sourceGooglePlus') }}@else{{$data['vendor']->google_plus_url}}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Instagram url</label>
												<input id="sourceInstagram" name="sourceInstagram" type="text"
														class="form-control"
														placeholder="Enter company instagram profile"
														value="@if(old('sourceInstagram')){{ old('sourceInstagram') }}@else{{$data['vendor']->instagram_url}}@endif">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="leadStatus">Lead Status</label>
												<select name="leadStatus" id="leadStatus" class="form-control">
													@foreach ($data['leadStatus'] as $status)
														<option value="{{ $status->id }}"
																@if($data['vendor']->lead_status_id == $status->id) selected @endif>{{ $status->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="comments">Comments</label>
												<textarea id="comments" name="comments"
														class="form-control vertical-resize"
														rows="4" placeholder="enter comments..."
														data-val="{{ $data['vendor']->comments }}">{!! $data['vendor']->comments !!}</textarea>
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="handler"><span>Handled By</span></label>
												<select name="handler" id="handler" class="form-control">
													@foreach ($data['handlers'] as $handler)
														<option value="{{ $handler->id }}"
																@if($handler->id == $data['vendor']->handler_id) selected @endif>{{ $handler->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="text-center">
									<div class="form-group">
										<input type="submit" value="Submit" class="btn btn-primary"/>
										<a class="pad-l-10"
												href="{{ route('vendor.view', $data['vendor']->id) }}">Cancel</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/vendors.js");
		});
	</script>
@stop