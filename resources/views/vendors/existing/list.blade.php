@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">

			<div class="page-content container vendors-wrap">
				<div class="vendors-cnt">
					<div class="panel panel-default">
						<div class="panel-body">
							@include('vendors.base.header')
							<div class="upper">
								<div class="pull-left">
									<h2 class="panel-title">Vendors List</h2>
								</div>
								<div class="pull-right">
									<select name="city" id="city" class="form-control">
										<option value="0"
												@if(request()->input('city') == 0) selected @endif>All
										</option>
										@foreach($data['cities'] as $city)
											<option value="{{ $city->id }}"
													@if(request()->input('city')== $city->id) selected @endif>{{ $city->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="pad-t-10">
								<div class="pull-left">
									<div class="form-inline" role="form">
										<div class="form-group">
											<input name="query" id="query" type="text" class="form-control"
													value="{{ request()->input('query') }}"
													placeholder="name, number, company etc."/>
										</div>
										<div class="form-group">
											<button id="submitSearch" class="btn btn-info">Search</button>
										</div>
										<div class="form-group">
											@if(request()->has('query'))
												<a class="btn btn-link" id="clearSearch">Cancel</a>
											@endif
										</div>
									</div>
								</div>
								<div class="pull-right">
									<div class="form-group in-blk mar-r-10">
										<select name="page-count" id="pageCount" class="form-control">
											<option value="20" @if(request()->input('count') == 20) selected @endif>20
											</option>
											<option value="40" @if(request()->input('count') == 40) selected @endif>40
											</option>
											<option value="60" @if(request()->input('count') == 60) selected @endif>60
											</option>
											<option value="80" @if(request()->input('count') == 80) selected @endif>80
											</option>
											<option value="100"
													@if(request()->input('count') == 100) selected @endif>100
											</option>
										</select>
									</div>
									<a href="{{ route('vendor.new.add') }}"
											class="btn btn-primary btn-add-new-vendor">Add New Vendor</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<hr>
							<div class="col-sm-12 col-md-12 col-lg-12">
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h4 class="in-blk">Filters</h4>
									@if(request()->has('status') || request()->has('sort') || request()->has('type') || request()->has('area'))
										<button class="btn btn-xs btn-warning pull-right mar-t-10"
												id="btn-clear-filter">Clear All Filters
										</button>
									@endif
									<div class="filter-wrap">
										<div class="filter-title">
											<div class="pull-left"></div>
											By Area
											<div class="pull-right">
												<button class="btn btn-info btn-xs" id="btnAreaFilter"><i
															class="glyphicon glyphicon-search"></i>
												</button>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="form-group no-pad no-mar mar-t-5">
											<select name="areaFilter" id="areaFilter" class="form-control">
												<option value="0"
														@if(request()->input('area')== 0) selected @endif>-- Select Area --
												</option>
												@foreach($data['areas'] as $area)
													<option value="{{ $area->id }}"
															@if(request()->input('area')== $area->id) selected @endif>{{ $area->name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="filter-wrap">
										<div class="filter-title">By Status</div>
										<ul class="filter-group-options ls-none no-mar no-pad">
											@foreach($data['allStatus'] as $status)
												<li class="filter-option v-st-{{$status->id}}">
													<label>
														<input name="status" type="radio" value="{{$status->name}}"
																@if(request()->input('status') == trim($status->name)) checked @endif />{{$status->name}}
													</label>
												</li>
											@endforeach
										</ul>
									</div>

									<div class="filter-wrap mar-t-20">
										<div class="filter-title">
											<div class="pull-left"></div>
											By Vendor type
											<div class="pull-right">
												<button class="btn btn-info btn-sm " id="btn-vendorType"><i
															class="glyphicon glyphicon-search"></i>
												</button>
											</div>
											<div class="clearfix"></div>
										</div>
										<ul class="filter-group-options ls-none no-mar no-pad">
											@foreach($data['vendorTypes'] as $vendorType)
												<li class="filter-option">
													<label>
														<input type="checkbox" name="vendorType" class="vendor-type"
																value="{{$vendorType->name}}"
																@if(in_array($vendorType->name,$data['typeNames'])) checked @endif>{{$vendorType->name}}
													</label>
												</li>
											@endforeach
										</ul>
									</div>
								</div>
								<div class="col-sm-9 col-md-9 col-lg-9">
									@if(!$data['vendors']->count())
										<div class="alert alert-danger">Sorry No Vendors available.</div>
									@endif
									@foreach ($data['vendors'] as $vendor)
										<div class="vendor-list-wrap">
											<a href="{{route('vendor.view',$vendor->id)}}">
												<div class="vendor-header @if($vendor->is_live) existing @else new @endif">
													<div class="col-sm-3 col-md-3 col-lg-3">
														<div class="sub-title">Name</div>
														{{$vendor->name}}
													</div>
													<div class="col-sm-3 col-md-3 col-lg-3">
														<div class="sub-title">Contact Person</div>
														<i class="glyphicon glyphicon-user"></i> {{$vendor->person}}
													</div>
													<div class="col-sm-3 col-md-3 col-lg-3">
														<div class="sub-title">Phone</div>
														<i class="glyphicon glyphicon-phone"></i> @if($vendor->phone){{$vendor->phone}}@else ---- @endif
													</div>
													<div class="col-sm-3 col-md-3 col-lg-3">
														<div class="sub-title">Type</div>
														<i class="glyphicon glyphicon-th"></i> @if($vendor->type) {{$vendor->type->name}}@else N/A @endif
													</div>
													<div class="clearfix"></div>
												</div>
											</a>
											<div class="vendor-body pad-10">
												<div class="col-sm-3 col-md-3 col-lg-3">
													{{$vendor->code}}
												</div>
												<div class="col-sm-2 col-md-2 col-lg-2 v-st-{{$vendor->status_id}}">
													<i class="glyphicon {{AppUtil::getVendorStatusIcon($vendor->status_id)}}"></i>
													@if($vendor->status) {{$vendor->status->name}} @else N/A @endif
												</div>
												<div class="col-sm-3 col-md-3 col-lg-3">
													<i class="glyphicon glyphicon-map-marker"></i>
													@if($vendor->area)
														{{$vendor->area->name}}
													@else
														N/A
													@endif
												</div>
												<div class="col-sm-4 col-md-4 col-lg-4">

													<a href="{{route('vendor.view',$vendor->id)}}"
															class="btn btn-xs btn-warning mar-r-10">
														<i class="glyphicon glyphicon-eye-open"></i> see details
													</a>
													<a href="{{route('vendor.delete',$vendor->id)}}"
															onclick="return confirm('Are you sure?')"
															class="btn btn-xs btn-danger mar-r-10">
														<i class="glyphicon glyphicon-trash"></i> Delete
													</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="vendor-bottom">
												<i>Vendor created at
													<b>{{AppUtil::formatDateTimeForBooking(strtotime($vendor->created_at))}}</b> by
													<b>
														@if ($vendor->handler)
															{{ $vendor->handler->name }}
														@else
															N/A
														@endif
													</b>
												</i>
											</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
									<div class="text-center">
										{{ $data['vendors']->appends(request()->except('page'))->links() }}
									</div>
								</div>
								<div class="hide">
									<input type="hidden" value="{{route('vendor.list')}}" id="currentUrl"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>

@stop

@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {

			var $area = $('#areaFilter').selectize();

			//area filter
			$('#btnAreaFilter').click(function (e) {
				e.preventDefault();
				var areaFilterValue = $('#areaFilter').val();
				if (areaFilterValue != 0) {
					window.refreshUrlWithExistingParam('area', areaFilterValue);
				}
			});

			if (window.location.search.indexOf('new=true') > -1) {
				if (!alert('A new vendor has been created')) {
					window.location = "/vendors";
				}
			}

			if (window.location.search.indexOf('delete=true') > -1) {
				if (!alert('Vendor has been deleted')) {
					window.location = "/vendors";
				}
			}

			$('#btn-clear-filter,#clearSearch').click(function (e) {
				e.preventDefault();
				var city = $('#cityFilter').val();
				var currentUrl = $('#currentUrl').val();

				window.location = city ? currentUrl + '?city=' + city : currentUrl;
			});

			$("input:radio[name='status']").click(function () {
				if ($(this).prop('checked')) {
					var status = $(this).val();
					window.refreshUrlWithExistingParam("status", status);
				}
			});
			//page count filter
			$('#pageCount').change(function (e) {
				e.preventDefault();
				var count = $(this).val();
				window.refreshUrlWithExistingParam("count", count);
			});

			// city filter

			$('#city').change(function (e) {
				e.preventDefault();
				var count = $(this).val();
				window.refreshUrlWithExistingParam("city", count);
			});

			$('#submitSearch').click(function (e) {
				e.preventDefault();

				var search = $('#query').val();
				window.refreshUrlWithExistingParam("query", search);
			});

			//vendor Type filter
			$('#btn-vendorType').click(function (e) {
				e.preventDefault();
				var checkedType = checkedVendorType();
				var formattedType = checkedType.join(",");

				window.refreshUrlWithExistingParam('type', formattedType);
			});

			//vendor type checked
			function checkedVendorType() {
				var allValues = [];
				$('.vendor-type').each(function () {
					if ($(this).prop('checked')) {
						allValues.push($(this).val());
					}
				});
				return allValues;
			}

			//copy button needs to be implemented
		});

	</script>
@stop