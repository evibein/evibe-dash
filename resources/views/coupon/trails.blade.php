@extends('layout.base')

@section('custom-head')
	<link rel="stylesheet" type="text/css"
			href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.css"/>
@endsection

@section('content')
	<div class="panel panel-default page-content mar-l-10 mar-r-10">
		<div class="panel-body">
			<div class="title-sec no-pad">
				<div class="text-center">
					<div class="btn-group" role="group" aria-label="...">
						<a class="btn btn-default" href="{{route('coupon.redeemed')}}">Redeemed</a>
						<a class="btn btn-default" href="{{route('coupon.available')}}">Available</a>
						<a class="btn btn-default" href="{{route('coupon.trails')}}">Trails</a>
					</div>
				</div>
				<div class="pad-b-20 pad-t-20">
					<div class="font-20">Showing Available Coupons</div>
				</div>
			</div>
		</div>
		<div class="bottom-sec pad-10 mar-b-10">
			@if(count($data["logCouponUsage"]) > 0)
				<table id="couponAvailableTable" class="table table-bordered table-responsive table-hover">
					<thead class="text-info">
					<tr>
						<td>Coupon Code</td>
						<td>Is Coupon Exists?</td>
						<td>Booking Amount</td>
						<td>Discount Amount</td>
						<td>Is success</td>
						<td>Ticket Info</td>
						<td>User</td>
						<td>City</td>
						<td>Occasion</td>
						<td>Created At</td>
					</tr>
					</thead>
					@foreach($data['logCouponUsage'] as $coupon)
						<tr>
							<td>{{ $coupon["coupon_code"] }}</td>
							<td>@if(!is_null($coupon["coupon_id"]))<span class="text text-success"><i
											class="glyphicon glyphicon-ok"></i> YES</span>
								@else<span class="text text-danger"><i
												class="glyphicon glyphicon-remove"></i> NO</span>@endif</td>
							<td><span>&#8377;</span> {{ $coupon["booking_amount"] }}</td>
							<td>@if($coupon["discount_amount"])
									<span>&#8377;</span> {{ $coupon["discount_amount"] }}
								@else{{ "--" }}
								@endif
							</td>
							<td>
								@if(!is_null($coupon["is_success"]))<span class="text text-success"><i
											class="glyphicon glyphicon-ok"></i> YES</span>
								@else<span class="text text-danger"><i
											class="glyphicon glyphicon-remove"></i> NO</span>@endif
							</td>
							<td>
								<a href="{{ route("ticket.details.bookings", $coupon["ticket_id"]) }}">{{ $coupon["ticket_id"] }}</a>
							</td>
							<td>@if(isset($data['users'][$coupon["user_id"]])){{ $data['users'][$coupon["user_id"]] }}@else{{ "--" }}@endif</td>
							<td>@if(isset($data['cities'][$coupon["city_id"]])){{ $data['cities'][$coupon["city_id"]] }}@endif</td>
							<td>@if(isset($data['occasions'][$coupon["occasion_id"]])){{ $data['occasions'][$coupon["occasion_id"]] }}@endif</td>
							<td>{{ $coupon["created_at"] }}</td>
						</tr>
					@endforeach
				</table>
			@else
				<div class="mar-l-10 mar-b-10 alert alert-danger text-center">No Coupon Codes applied until now.</div>
			@endif
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			(function initFunctions() {
				// initiate dataTables
				$('#couponAvailableTable').DataTable();
			})();
		});
	</script>
@endsection