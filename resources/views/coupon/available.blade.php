@extends('layout.base')

@section('custom-head')
	<link rel="stylesheet" type="text/css"
			href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.css"/>
@endsection

@section('content')
	<div class="panel panel-default page-content mar-l-10 mar-r-10">
		<div class="panel-body">
			<div class="title-sec no-pad">
				<div class="text-center">
					<div class="btn-group" role="group" aria-label="...">
						<a class="btn btn-default" href="{{route('coupon.redeemed')}}">Redeemed</a>
						<a class="btn btn-default" href="{{route('coupon.available')}}">Available</a>
						<a class="btn btn-default" href="{{route('coupon.trails')}}">Trails</a>
					</div>
				</div>
				<div class="pad-b-20 pad-t-20">
					<div class="font-20">Showing Available Coupons</div>
				</div>
			</div>
		</div>
		<div class="bottom-sec pad-10 mar-b-10">
			@if(count($data["coupons"]) > 0)
				<table id="couponAvailableTable" class="table table-bordered table-responsive table-hover">
					<thead class="text-info">
					<tr>
						<td>Coupon Code</td>
						<td>Offer Time</td>
						<td>Min Order Amount</td>
						<td>Discount</td>
						<td>Max Discount Amount</td>
						<td>Partner Discount</td>
						<td>Max Partner Discount</td>
						<td>Generated For</td>
						<td>Terms</td>
					</tr>
					</thead>
					@foreach($data['coupons'] as $coupon)
						<tr>
							<td>{{ $coupon["coupon_code"]." (". $coupon["max_usage_count"] .")" }}</td>
							<td>{{ $coupon["offer_start_time"] . " - " . $coupon["offer_end_time"] }}</td>
							<td>@if($coupon["min_order"])
									<span>&#8377;</span> {{ $coupon["min_order"] }}@else{{ "--" }}@endif</td>
							<td>@if($coupon["discount_amount"])
									<span>&#8377;</span> {{ $coupon["discount_amount"] }}
								@elseif($coupon["discount_percent"]){{ $coupon["discount_percent"]."%" }}
								@else{{ "--" }}
								@endif
							</td>
							<td>@if($coupon["max_discount_amount"]){{ $coupon["max_discount_amount"] }}@else{{ "--" }}@endif</td>
							<td>@if($coupon["parnter_share_amount"])
									<span>&#8377;</span> {{ $coupon["parnter_share_amount"] }}
								@elseif($coupon["partner_share_percent"]){{ $coupon["partner_share_percent"]."%" }}
								@else{{ "--" }}
								@endif
							</td>
							<td>{{ $coupon["partner_share_max_amount"] }}</td>
							<td>{{ $coupon["description"] }}</td>
							<td>{{ $coupon["terms"] }}</td>
						</tr>
					@endforeach
				</table>
			@else
				<div class="mar-l-10 mar-b-10 alert alert-danger text-center">No bookings found with coupon code applied.</div>
			@endif
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			(function initFunctions() {
				// initiate dataTables
				$('#couponAvailableTable').DataTable();
			})();
		});
	</script>
@endsection