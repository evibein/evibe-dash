@extends('layout.base')

@section('custom-head')
	<link rel="stylesheet" type="text/css"
			href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.css"/>
@endsection

@section('content')
	<div class="panel panel-default page-content mar-l-10 mar-r-10">
		<div class="panel-body">
			<div class="title-sec no-pad">
				<div class="text-center">
					<div class="btn-group" role="group" aria-label="...">
						<a class="btn btn-default" href="{{route('coupon.redeemed')}}">Redeemed</a>
						<a class="btn btn-default" href="{{route('coupon.available')}}">Available</a>
						<a class="btn btn-default" href="{{route('coupon.trails')}}">Trails</a>
					</div>
				</div>
				<div class="pad-b-20 pad-t-20">
					<div class="col-sm-6 col-xs-12 font-20">Showing Coupon Redeemed Bookings</div>
					<div class="col-sm-6 col-xs-12">
						<table class="table table-bordered no-mar-b text-center">
							<tr>
								<td class="col-xs-4">Total Redeemed Coupons<br><span class="text-bold font-16">
										@if($data["totalRedeemedBookings"] > 0){{ $data["totalRedeemedBookings"] }}@else{{ "--" }}@endif</span>
								</td>
								<td class="col-xs-4">Total Discounted Amount<br><span class="text-bold font-16">
										@if($data["totalDiscountAmount"] > 0){{ $data["totalDiscountAmount"] }}@else{{ "--" }}@endif</span>
								</td>
								<td class="col-xs-4">Total Booking Amount<br><span class="text-bold font-16">
										@if($data["totalBookingAmount"] > 0){{ $data["totalBookingAmount"] }}@else{{ "--" }}@endif</span>
								</td>
							</tr>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="bottom-sec pad-10 mar-b-10">
			@if($data["totalRedeemedBookings"] > 0)
				<table id="couponRedeemedTable" class="table table-bordered table-responsive table-hover">
					<thead class="text-info">
					<tr>
						<td>Customer</td>
						<td>Party Date</td>
						<td>City</td>
						<td>Occasion</td>
						<td>Booking Amount</td>
						<td>Coupon Applied</td>
						<td>Coupon Discount</td>
					</tr>
					</thead>
					@foreach($data['bookings'] as $booking)
						<tr>
							<td>{{ $booking["customerName"] }}</td>
							<td>{{ date('d M Y', $booking["party_date_time"]) }}</td>
							<td>@if(isset($data['cities'][$booking["cityId"]])){{ $data['cities'][$booking["cityId"]] }}@endif</td>
							<td>@if(isset($data['occasions'][$booking["eventId"]])){{ $data['occasions'][$booking["eventId"]] }}@endif</td>
							<td>{{ $booking["booking_amount"] }}</td>
							<td>@if(isset($data["coupons"][$booking["coupon_id"]])){{ $data["coupons"][$booking["coupon_id"]] }}@endif</td>
							<td>{{ $booking["discount_amount"] }}</td>
						</tr>
					@endforeach
				</table>
			@else
				<div class="mar-l-10 mar-b-10 alert alert-danger text-center">No bookings found with coupon code applied.</div>
			@endif
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			(function initFunctions() {
				// initiate dataTables
				$('#couponRedeemedTable').DataTable();
			})();
		});
	</script>
@endsection