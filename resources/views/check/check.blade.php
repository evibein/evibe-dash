<form action="/apis/upload-attachments" method="POST" role="form"
	enctype="multipart/form-data" accept-charset="UTF-8">
	<div class="form-group">
		<label>Type</label>
		<input type="text" name="type">
	</div>
	<div class="form-group">
		<label>Type Id</label>
		<input type="text" name="typeId">
	</div>
	<div class="form-group">
		<label>Upload</label>
		<input type="file" name="attachments[]" multiple="true">
	</div>
	<input type="submit" value="Submit" name="submit">
</form>