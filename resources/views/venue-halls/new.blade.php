@extends('layout.base')

@section('title')
	<title>{{ $data['venue']->name }} - Add New Hall | EvibeDash</title>
@stop

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="inner">
					<div class="pull-left mar-t-10">
						<h3 class="panel-title">{{ $data['venue']->name }} - Add New Hall</h3>
					</div>
					<div class="pull-right">
						<a href="{{ route('venue-halls.list') }}" class="btn btn-default">Cancel</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel-body">
				<div class="errors-cnt">
					<div class="errors-box">
						@if ($errors->any() || Session::has('custom_error'))
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								@if (Session::has('custom_error'))
									<ul>
										<li>{{ Session::get('custom_error') }}</li>
									</ul>
								@endif
								@if ($errors->any())
									<ul>
										{{$errors->first()}}
									</ul>
								@endif
							</div>
						@endif
					</div>
				</div>
				<form action="{{ route('venue-halls.new.save', $data['venue']->id) }}" method="POST" class="form">
					<div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="type">Hall Type*</label>
								<select name="type" id="type" class="form-control">
									@foreach ($data['hallTypes'] as $hallType)
										<option value="{{ $hallType->id }}"
												@if(old('type') == $hallType->id) selected="selected" @endif>
											{{ $hallType->name }}
										</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="name">Hall Name</label>
								<input id="name" name="name" type="text"
										class="form-control" placeholder="enter hall name"
										value="{{ old('name') }}"/>
							</div>
							<div class="form-group">
								<label for="floor">Floor Number*</label>
								<span class="text-muted"><i>(0 - ground floor)</i></span>
								<input id="floor" name="floor" type="text"
										class="form-control" placeholder="enter floor number"
										value="{{ old('floor') }}"/>
							</div>
							<div class="form-group">
								<label for="events">Events Supported*</label>
								<select name="events[]" id="events" class="form-control"
										data-old="{{old('events')}}">
									<option value="">Select events supported...</option>
									@foreach($data['typeEvents'] as $event)
										<option value="@if(old('events[]')){{old('events')}}@else{{ $event->id }}@endif">{{ $event->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="tax">Tax % </label>
								<input name="tax" type="text"
										class="form-control" placeholder="enter tax % "
										value="{{ old('tax') }}"/>
							</div>
							<div class="form-group">
								<label for="floor">Has AC?*</label>
								<label class="font-normal mar-r-10">
									<input type="radio" name="hasAc" value="1"
											@if (old('hasAc')) checked="checked" @endif> Yes
								</label>
								<label class="font-normal">
									<input type="radio" name="hasAc" value="0"
											@if (!old('hasAc')) checked="checked" @endif> No
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="pad-l-15">Hall Rent </label><span
									class="text-muted"> (Min, Max, Worth, Min duration)</span>
							<div class="col-sm-6">
								<div class="col-sm-3 no-pad-l">
									<input type="text" name="rentMin" class="form-control"
											placeholder="min rent" value="{{old('rentMin')}}">
								</div>
								<div class="col-sm-3">
									<input type="text" name="rentMax" class="form-control"
											placeholder="max rent" value="{{old('rentMax')}}">
								</div>
								<div class="col-sm-3">
									<input type="text" name="rentWorth" class="form-control"
											placeholder="worth rent" value="{{old('rentWorth')}}">
								</div>
								<div class="col-sm-3">
									<input type="text" name="rentMinDuration" class="form-control"
											placeholder="min rent dur"
											value="{{old('rentMinDuration')}}">
								</div>
								<div class="clearfix"></div>
								<label class="mar-t-15">Capacity </label><span
										class="text-muted in-blk"> (Min, Max, Floating) </span><br>
								<div class="col-sm-4 no-pad-l">
									<input id="capMin" name="capMin" type="text"
											class="form-control" placeholder="minimum capacity"
											value="{{ old('capMin') }}"/>
								</div>
								<div class="col-sm-4">
									<input id="capMax" name="capMax" type="text"
											class="form-control" placeholder="maximum capacity"
											value="{{ old('capMax') }}"/>
								</div>
								<div class="col-sm-4">
									<input id="capFloat" name="capFloat" type="text"
											class="form-control" placeholder="floating capacity"
											value="{{ old('capFloat') }}"/>
								</div>
								<label class="mar-t-15">Food veg </label> <span
										class="text-muted"> (Min, Max, Worth)</span><br>
								<div class="col-sm-4 no-pad-l">
									<input type="text" class="form-control" placeholder="min veg price"
											name="foodMinVeg" value="{{old('foodMinVeg')}}">
								</div>
								<div class="col-sm-4">
									<input type="text" class="form-control" placeholder="max veg price"
											name="foodMaxVeg" value="{{old('foodMaxVeg')}}">
								</div>
								<div class="col-sm-4">
									<input type="text" class="form-control" placeholder="worth veg price"
											name="foodWorthVeg" value="{{old('foodWorthVeg')}}">
								</div>
								<label class="mar-t-15">Food Non Veg </label><span
										class="text-muted"> (Min, Max, Worth)</span><br>
								<div class="col-sm-4 no-pad-l">
									<input type="text" class="form-control" placeholder="min non veg price"
											name="foodMinNonVeg" value="{{old('foodMinNonVeg')}}">
								</div>
								<div class="col-sm-4">
									<input type="text" class="form-control" placeholder="max non veg price"
											name="foodMaxNonVeg" value="{{old('foodMaxNonVeg')}}">
								</div>
								<div class="col-sm-4">
									<input type="text" class="form-control" placeholder="worth non veg price"
											name="foodWorthNonVeg" value="{{old('foodWorthNonVeg')}}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="pad-t-20 text-center">
						<a href="{{ route('venue-halls.list') }}" class="btn btn-default mar-r-10">Cancel</a>
						<input id="submit" name="submit" type="submit"
								class="btn btn-success" value="Submit"/>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			var venueHallSelects = {};
			var items = [{key: 'events', el: '#events'}];

			//select event support items

			item = items[0];
			venueHallSelects[item['key']] = $(item['el']).selectize({
				maxItems: 1000,
				create: false,
				hideSelected: true
			});

			function setData() {
				var ids = '', selEl = '', j = 0;
				for (i = 0; i < items.length; i++) {
					if ($(items[i]['el']).data('old')) {
						ids = $(items[i]['el']).data('old').toString().split(",");
						selEl = venueHallSelects[items[i]['key']][0].selectize;
						for (j = 0; j < ids.length; j++) selEl.addItem(ids[j], false);
					}
				}
			}

			setData();
		});
	</script>
@stop