@extends('layout.base')

@section('title')
	<title>{{ $data['venue']->name }} - Edit Hall Details | EvibePro</title>
@stop

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="inner">
					<div class="pull-left">
						<div class="in-blk mar-r-20">
							<a class="btn btn-default btn-sm" href="{{ route('venue-halls.profile.view', $data['hall']->id) }}">
								<i class="glyphicon glyphicon-backward"></i>
								<span>Back</span>
							</a>
						</div>
						<div class="in-blk">
							<h3 class="panel-title">{{ $data['venue']->name }} - Edit Hall Details</h3>
						</div>
					</div>
					<div class="pull-right">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel-body">
				<div class="errors-cnt">
					<div class="errors-box">
						@if ($errors->any() || Session::has('custom_error'))
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								@if (Session::has('custom_error'))
									<ul>
										<li>{{ Session::get('custom_error') }}</li>
									</ul>
								@endif
								@if ($errors->any())
									<ul>
										@foreach($errors->all() as $message)
											<li>{!! $message !!}</li>
										@endforeach
									</ul>
								@endif
							</div>
						@endif
					</div>
				</div>
				<form action="{{ route('venue-halls.profile.edit.save', $data['hall']->id) }}" method="POST" class="form">
					<div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="type">Hall Type*</label>
								<select name="type" id="type" class="form-control">
									@foreach ($data['hallTypes'] as $hallType)
										<option value="{{ $hallType->id }}"
												@if (old('type'))
												@if (old('type') == $data['hall']->type_id)
												selected="selected"
												@endif
												@else
												@if ($data['hall']->type_id == $hallType->id)
												selected="selected"
												@endif
												@endif>
											{{ $hallType->name }}
										</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="name">Hall Name</label>
								<input id="name" name="name" type="text"
										class="form-control" placeholder="enter hall name"
										value="@if(old('name')){{ old('name') }}@else{{ $data['hall']->name }}@endif"/>
							</div>
							<div class="form-group">
								<label for="floor">Floor Number*</label>
								<span class="text-muted"><i>(0 - ground floor)</i></span>
								<input id="floor" name="floor" type="text"
										class="form-control" placeholder="enter floor number"
										value="@if(old('floor')){{ old('floor') }}@else{{ $data['hall']->floor }}@endif"/>
							</div>
							<div class="form-group">
								<label>Events Supported*</label>
								<select name="events[]" id="events" class="form-control"
										data-old="@if(old('events')){{ old('events') }}@else{{ $data['hallEvents'] }}@endif">
									<option value="">Select events supported...</option>
									@foreach($data['typeEvents'] as $event)
										<option value="{{ $event->id }}">{{ $event->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="tax">Tax % </label>
								<input name="tax" type="text"
										class="form-control" placeholder="enter tax % "
										value="@if(old('tax')){{ old('tax') }}@elseif($data['hall']->tax && $data['hall']->tax>0){{$data['hall']->tax}}@endif"/>
							</div>
							<div class="form-group">
								<label for="floor">Has AC?*</label>
								<label class="font-normal mar-r-10">
									<input type="radio" name="hasAc" value="1"
											@if (old('hasAc', false) !== false)
											@if (old('hasAc') == 1) checked="checked" @endif
									@elseif ($data['hall']->has_ac)
									checked="checked"
									@else @endif> Yes
								</label>
								<label class="font-normal">
									<input type="radio" name="hasAc" value="0"
											@if (old('hasAc', false) !== false)
											@if (old('hasAc') == 0) checked="checked" @endif
									@elseif (!$data['hall']->has_ac)
									checked="checked"
									@else @endif> No
								</label>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="pad-l-15">Hall Rent </label><span
									class="text-muted"> (Min, Max, Worth, Min duration)</span><br>
							<div class="col-sm-3">
								<input type="text" name="rentMin" class="form-control"
										placeholder="min rent"
										value="@if(old('rentMin')) {{ old('rentMin') }}@elseif($data['hall']->rent_min && $data['hall']->rent_min >0){{ $data['hall']->rent_min }}@endif">
							</div>
							<div class="col-sm-3">
								<input type="text" name="rentMax" class="form-control"
										placeholder="max rent"
										value="@if(old('rentMin')) {{ old('rentMax') }}@elseif($data['hall']->rent_max >0){{ $data['hall']->rent_max }}@endif">
							</div>
							<div class="col-sm-3">
								<input type="text" name="rentWorth" class="form-control"
										placeholder="worth rent"
										value="@if(old('rentWorth')) {{ old('rentWorth') }}@elseif($data['hall']->rent_worth > 0){{ $data['hall']->rent_worth }}@endif">
							</div>
							<div class="col-sm-3">
								<input type="text" name="rentMinDuration" class="form-control"
										placeholder="min rent duration"
										value="@if(old('rentMinDuration')){{ old('rentMinDuration' )}}@elseif($data['hall']->min_rent_duration>0){{ $data['hall']->min_rent_duration }}@endif">
							</div>
							<div class="clearfix"></div>
							<label class="pad-l-15 mar-t-15">Capacity </label><span
									class="text-muted in-blk"> (Min, Max, Floating) </span><br>
							<div class="col-sm-4">
								<input id="capMin" name="capMin" type="text"
										class="form-control" placeholder="enter minimum capacity"
										value="@if(old('capMin')){{ old('capMin') }}@else{{ $data['hall']->cap_min }}@endif"/>
							</div>
							<div class="col-sm-4">
								<input id="capMax" name="capMax" type="text"
										class="form-control" placeholder="enter maximum capacity"
										value="@if(old('capMax')){{ old('capMax') }}@else{{ $data['hall']->cap_max }}@endif"/>
							</div>
							<div class="col-sm-4">
								<input id="capFloat" name="capFloat" type="text"
										class="form-control" placeholder="enter floating capacity"
										value="@if(old('capFloat')){{ old('capFloat') }}@else{{ $data['hall']->cap_float }}@endif"/>
							</div>
							<label class="mar-t-15 pad-l-15">Food veg </label> <span
									class="text-muted"> (Min, Max ,Worth)</span><br>
							<div class="col-sm-4 ">
								<input type="text" class="form-control" placeholder="min veg price"
										name="foodMinVeg"
										value="@if(old('foodMinVeg')){{old('foodMinVeg')}}
										@elseif($data['hall']->price_min_veg && $data['hall']->price_min_veg > 0){{$data['hall']->price_min_veg}}@endif">
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="max veg price"
										name="foodMaxVeg"
										value="@if(old('foodMaxVeg')){{old('foodMaxVeg')}}
										@elseif($data['hall']->price_max_veg && $data['hall']->price_max_veg > 0){{$data['hall']->price_max_veg}}@endif">
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="worth veg price"
										name="foodWorthVeg"
										value="@if(old('foodWorthVeg')){{old('foodWorthVeg')}}
										@elseif($data['hall']->price_worth_veg && $data['hall']->price_worth_veg > 0){{$data['hall']->price_worth_veg}}@endif">
							</div>
							<label class="mar-t-15 pad-l-15">Food Non Veg </label><span
									class="text-muted"> (Min, Max, Worth)</span><br>
							<div class="col-sm-4 ">
								<input type="text" class="form-control" placeholder="min non veg price"
										name="foodMinNonVeg"
										value="@if(old('foodMinNonVeg')){{old('foodMinNonVeg')}}
										@elseif($data['hall']->price_min_nonveg && $data['hall']->price_min_nonveg > 0){{$data['hall']->price_min_nonveg}}@endif">
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="max non veg price"
										name="foodMaxNonVeg"
										value="@if(old('foodMaxNonVeg')){{old('foodMaxNonVeg')}}
										@elseif($data['hall']->price_max_nonveg && $data['hall']->price_max_nonveg > 0){{$data['hall']->price_max_nonveg}}@endif">
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="worth non veg price"
										name="foodWorthNonVeg"
										value="@if(old('foodWorthNonVeg')){{old('foodWorthNonVeg')}}
										@elseif($data['hall']->price_worth_nonveg && $data['hall']->price_worth_nonveg > 0){{$data['hall']->price_worth_nonveg}}@endif">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div>
						<div>
							<div class="col-sm-6">
								<div class="form-group">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="pad-t-20 text-center">
							<a href="{{ route('venue-halls.profile.view', $data['hall']->id) }}"
									class="btn btn-default mar-r-10">Cancel</a>
							<input id="submit" name="submit" type="submit"
									class="btn btn-success" value="Submit"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			var venueHallSupportedEventSelects = {};
			var item = '', i = 0;
			var items = [{key: 'events', el: '#events'}];

			function initPlugin() {

				//code for supported events

				item = items[0];
				venueHallSupportedEventSelects[item['key']] = $(item['el']).selectize({
					maxItems: 1000,
					create: false,
					hideSelected: true
				});

				//existing supported events
			}

			function setData() {
				var ids = '', selEl = '', j = 0;
				for (i = 0; i < items.length; i++) {
					if ($(items[i]['el']).data('old')) {
						ids = $(items[i]['el']).data('old').toString().split(",");
						selEl = venueHallSupportedEventSelects[items[i]['key']][0].selectize;
						for (j = 0; j < ids.length; j++) selEl.addItem(ids[j], false);
					}
				}
			}

			initPlugin();
			setData();

		});
	</script>
@stop