@extends('layout.base')

@section('title')
	<title xmlns="http://www.w3.org/1999/html">@if($data['venueHall']->type){{ $data['venueHall']->type->name }} @endif :: {{ $data['venueHall']->name }} Details | EvibeDash</title>
@stop

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="inner">
					<div class="pull-left">
						<div class="in-blk mar-r-20">
							<a class="btn btn-default btn-sm" href="{{ route('venue-halls.list') }}">
								<i class="glyphicon glyphicon-backward"></i>
								<span>List of Halls</span>
							</a>
						</div>
						<div class="in-blk">
							<h3 class="panel-title">
								<span class="text-bold">{{ $data['venueHall']->name }} | </span>
								<span>
									{{ $data['venueHall']->venue->name }} :: @if($data['venueHall']->type) {{ $data['venueHall']->type->name }} @endif
								</span>
							</h3>
						</div>
					</div>
					<div class="pull-right">
						@if ($data['venueHall']->venue->is_live)
							<a href="{{$data['venueHall']->getLiveLink()}}" class="btn btn-info btn-sm mar-r-10" target="_blank">
								<i class="glyphicon glyphicon-eye-open"></i>
								<span>View On Website</span>
							</a>
						@else
							<a class="btn btn-default btn-sm mar-r-10" disabled>
								<i class="glyphicon glyphicon-eye-close"></i>
								<span>Venue Not live</span>
							</a>
						@endif
						<a href="{{ route('venue-halls.profile.delete', $data['venueHall']->id) }}" class="btn btn-danger btn-sm"
								onclick="return confirm('Are you sure?');">
							<i class="glyphicon glyphicon-trash"></i>
							<span>Delete Hall</span>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel-body">
				<div class="errors-cnt">
					<div class="errors-box">
						@if ($errors->any() || Session::has('custom_error'))
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<ul class="no-pad no-mar ls-none">
									@if (Session::has('custom_error'))
										<li>{{ Session::get('custom_error') }}</li>
									@endif
									@if ($errors->any())
										@foreach($errors->all() as $message)
											<li>{{ $message }}</li>
										@endforeach
									@endif
								</ul>
							</div>
						@endif
					</div>
				</div>
				<section class="data-sec pad-b-20">
					<h4 class="text-danger">
						<span class="mar-r-10">Information</span>
						<div class="in-blk">
							<a class="text-normal font-14"
									href="{{ route('venue-halls.profile.edit.show', $data['venueHall']->id) }}">
								<i class="glyphicon glyphicon-pencil"></i>
								<span>Edit</span>
							</a>
						</div>
					</h4>
					<div>
						<div class="col-sm-6 no-pad-l">
							<table class="table table-striped table-bordered">
								<tr>
									<td><label>Type</label></td>
									<td>@if($data['venueHall']->type) {{ $data['venueHall']->type->name }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Code</label></td>
									<td>{{ $data['venueHall']->code ? $data['venueHall']->code : '--' }}</td>
								</tr>
								<tr>
									<td><label>Name</label></td>
									<td>{{ $data['venueHall']->name ? $data['venueHall']->name : '--' }}</td>
								</tr>
								<tr>
									<td><label>Floor Number</label></td>
									<td>{{ $data['venueHall']->floor }}</td>
								</tr>
								<tr>
									<td><label>Has AC</label></td>
									<td>{{ $data['venueHall']->has_ac ? ' Yes ' : ' No ' }}</td>
								</tr>
								<tr>
									<td><label>Capacity</label></td>
									<td>{{ $data['venueHall']->cap_min }} - {{ $data['venueHall']->cap_max }} (Float: {{ $data['venueHall']->cap_float }})</td>
								</tr>
								<tr>
									<td><label>Rent</label></td>
									<td>
										@if($data['venueHall']->rent_min)
											@price($data['venueHall']->rent_min)
											@if($data['venueHall']->rent_max)
												- @price($data['venueHall']->rent_max)
											@endif
											@if($data['venueHall']->rent_worth)
												(
												<span class="text-strike"> @price($data['venueHall']->rent_worth) </span> )
											@endif
										@else
											--
										@endif
									</td>
								</tr>
								<tr>
									<td><label>Minimum Rent Duration (in hrs)</label></td>
									<td>
										@if($data['venueHall']->min_rent_duration && $data['venueHall']->min_rent_duration > 0)
											{{$data['venueHall']->min_rent_duration}} hrs
										@else
											--
										@endif
									</td>
								</tr>
								<tr>
									<td><label>Food (veg)</label></td>
									<td>
										@if($data['venueHall']->price_min_veg)
											@price($data['venueHall']->price_min_veg)
											@if($data['venueHall']->price_max_veg)
												- @price($data['venueHall']->price_max_veg)
											@endif
											@if($data['venueHall']->price_worth_veg)
												(
												<span class="text-strike"> @price($data['venueHall']->price_worth_veg) </span> )
											@endif
										@else
											--
										@endif
									</td>
								</tr>
								<tr>
									<td><label>Food (Non-Veg)</label></td>
									<td>
										@if($data['venueHall']->price_min_nonveg)
											@price($data['venueHall']->price_min_nonveg)
											@if($data['venueHall']->price_max_nonveg)
												- @price($data['venueHall']->price_max_nonveg)
											@endif
											@if($data['venueHall']->price_worth_nonveg)
												(
												<span class="text-strike"> @price($data['venueHall']->price_worth_nonveg) </span> )
											@endif
										@else
											--
										@endif
									</td>
								</tr>
								<tr>
									<td><label>Tax </label></td>
									<td>
										@if($data['venueHall']->tax && $data['venueHall']->tax > 0)
											{{$data['venueHall']->tax}} %
										@else
											--
										@endif
									</td>
								</tr>
							</table>
						</div>
						<div class="clearfix"></div>
					</div>
				</section>
				<section class="data-sec pad-b-20">
					<div class="alert alert-success">
						<h4>Event Supported*</h4>
						<div class="form-group">
							@if($data['eventSupported'])
								<div class="col-lg-14">
									@foreach($data['eventSupported'] as $event)
										<div class="form-control-static in-blk font-12 label-event">{{$event->event->name}}</div>
									@endforeach
								</div>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
					<h4 class="text-danger">Gallery</h4>
					@if ($data['venueHall']->gallery->count())
						<ul class="no-pad no-mar ls-none">
							@foreach ($data['venueHall']->gallery as $item)
								<li class="venue-hall-img">
									<img src="{{ $item->getPath() }}" alt="">
									<div class="view">
										<div class="pad-t-10">
											<div><label>Title</label>: {{ $item->title }}</div>
											<div><label>Category</label>: {{ $item->type->name }}</div>
										</div>
										<div class="pad-t-10">
											<a class="mar-r-10 hall-img-edit">
												<i class="glyphicon glyphicon-edit"></i> Edit
											</a>
											<a href="{{ route('venue-halls.profile.gallery.delete', [$data['venueHall']->id, $item->id]) }}"
													onclick="return confirm('Are you sure?')" class="mar-r-10">
												<i class="glyphicon glyphicon-trash"></i> Delete
											</a>
											@if ($item->is_profile)
												<a href="{{ route('venue-halls.profile.gallery.unset', [$data['venueHall']->id, $item->id]) }}"
														class="text-danger">
													<i class="glyphicon glyphicon-remove"></i> Unset Profile
												</a>
											@else
												<a href="{{ route('venue-halls.profile.gallery.set', [$data['venueHall']->id, $item->id]) }}"
														class="text-success">
													<i class="glyphicon glyphicon-ok"></i> Set Profile
												</a>
											@endif
										</div>
									</div>
									<div class="edit hide">
										<form action="{{ route('venue-halls.profile.gallery.edit', [$data['venueHall']->id, $item->id]) }}"
												method="POST" role="form" class="form">
											<div class="form-group">
												<label>Title</label>
												<input id="editTitle" name="editTitle" type="text" class="form-control"
														value="{{ $item->title }}" placeholder="enter title">
											</div>
											<div class="form-group">
												<label>Category</label>
												<select id="editType" name="editType" class="form-control">
													@foreach ($data['galleryTypes'] as $galleryType)
														<option value="{{ $galleryType->id }}"
																@if($galleryType->id == $item->type_id) selected="selected" @endif>
															{{ $galleryType->name }}
														</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<a class="hall-img-edit-cancel mar-r-10">Cancel</a>
												<input id="submit" name="submit" type="submit" value="Save"
														class="btn btn-info"/>
											</div>
										</form>
									</div>
								</li>
							@endforeach
						</ul>
					@else
						<div class="alert alert-warning">
							No images were uploaded. Please user form below to upload image for this hall.
						</div>
					@endif
					<div class="pad-t-20">
						<div class="alert alert-info col-sm-4">
							<form action="{{ route('venue-halls.profile.gallery.upload', $data['venueHall']->id) }}"
									method="POST" class="form" role="form" enctype="multipart/form-data">
								<div class="form-title">Add Images</div>
								<div class="form-group">
									<label for="type">Category</label>
									<select name="type" id="type" class="form-control">
										@foreach ($data['galleryTypes'] as $galleryType)
											<option value="{{ $galleryType->id }}">{{ $galleryType->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group pad-r-20">
									<label for="">Select Multiple Images</label>
									<input type="file" name="images[]" id="images[]" multiple="true">
								</div>
								<div class="form-group">
									<button type="submit" class="form-control btn btn-info btn-sm">
										<i class="glyphicon glyphicon-cloud-upload"> </i><span> Upload</span>
									</button>
								</div>
							</form>
						</div>
						<div class="clearfix"></div>
					</div>
				</section>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			$('.hall-img-edit, .hall-img-edit-cancel').click(function (event) {
				event.preventDefault();

				var $parent = $(this).parents('.venue-hall-img');
				$parent.find('.edit').toggleClass('hide');
				$parent.find('.view').toggleClass('hide');
			});
		});
	</script>
@stop