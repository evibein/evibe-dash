@extends('layout.base')
@section('content')
	<div class="heading">
		<div class="in-blk">
			Add on checkout fields
			@if($checkoutFields)
				<span class="mar-l-5">({{ count($checkoutFields) }})</span>
			@endif
		</div>
		<div class="in-blk mar-l-10">
			<form id="addOnsCheckFields" action="/test/create-add-on-check-fields" method="POST">
				<button type="submit" class="btn btn-info">Create normal checkout fields</button>
			</form>
		</div>
	</div>
	<div class="col-lg-12">
		@if($checkoutFields)
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th class="text-center">Id</th>
					<th class="text-center">Type Ticket Booking Id</th>
					<th class="text-center">Type Field Id</th>
					<th class="text-center">Event Id</th>
					<th class="text-center">Name</th>
					<th class="text-center">Identifier</th>
					<th class="text-center">Hint</th>
					<th class="text-center">Is Crm</th>
					<th class="text-center">Is AB</th>
					<th class="text-center">Updated At</th>
				</tr>
				</thead>
				<tbody>
				@foreach($checkoutFields as $checkoutField)
					<tr>
						<td class="text-center">{{ $checkoutField->id }}</td>
						<td class="text-center">{{ $checkoutField->type_ticket_booking_id }}</td>
						<td class="text-center">{{ $checkoutField->type_field_id }}</td>
						<td class="text-center">{{ $checkoutField->event_id }}</td>
						<td class="text-center">{{ $checkoutField->name }}</td>
						<td class="text-center">{{ $checkoutField->identifier }}</td>
						<td class="text-center">{!! $checkoutField->hint_message !!}</td>
						<td class="text-center">{{ $checkoutField->is_crm }}</td>
						<td class="text-center">{{ $checkoutField->is_auto_booking }}</td>
						<td class="text-center">{{ $checkoutField->updated_at }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		@else
			<h3>No bookings after now</h3>
		@endif
	</div>
@endsection