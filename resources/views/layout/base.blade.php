<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>

	<!-- trackers -->

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">

	@section('title')
		<title>EvibeDash by Evibe.in</title>
	@show

	@section('stylesheets')
		<link rel="stylesheet" href="{{ elixir('css/all.css') }}">
	@show

	@section('custom-head')
	@show

	@if (auth()->check())
		<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async='async'></script> <!-- Logged In user: OneSignal web push notifications -->
	@endif
	<link rel="icon" href="https://gallery.evibe.in/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16">

</head>

@section('custom-body')
	<body>
	@show

	@section('app-modals')
		<!-- Add new area modal begin -->
		<div id="addNewAreaModal" class="modal " tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<input type="hidden" id="hidAdvPercentage" value="{{ config('evibe.advance.percentage') }}">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Add New Area</h4>
					</div>
					<div class="modal-body">
						<form id="addAreaForm" action="/add-area" method="POST">
							<div class="form-group">
								<label>City</label>
								<select name="addAreaCity" id="addAreaCity" class="form-control">
									@foreach (\City::all() as $city)
										<option value="{{ $city->id }}">{{ $city->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="from-control">
								<label>Area Name</label>
								<input type="text" id="areaName" class="form-control">
								<input type="hidden" id="locationDetails" value="">
							</div>
							<div class="form-group pad-t-10">
								<label>Area PinCode</label>
								<input type="text" class="form-control" id="zipCode" name="zipCode"
										placeholder="Enter Pin Code">
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary btn-submit-new-area">Submit</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Add new area modal end -->

		<!-- Loading modal begin -->
		<div id="modalLoading" class="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-body">
						<h4>
							<img src="{{ AppUtil::getGalleryBaseUrl() }}/img/icons/loading.gif"
									alt="Loading icon"> Loading, please wait...
						</h4>
						<div class="text-danger">(Please do not press refresh / back button)</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Loading modal end -->

		<!-- Generic purpose modal begin -->
		<div id="modalGeneric" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body"></div>
					<div class="text-center hide whatsapp-payment-link-button">
						<a id="whatsappPaymentLink" class="btn btn-primary"><img
									src="http://gallery.evibe.in/img/icons/whatsapp-logo.png" alt="whatsapp-icon"
									class="whatsapp-icon pad-r-5">Send Payment Link via Whatsapp</a>
						<a id="whatsappPaymentLinkLoading" class="btn btn-primary hide disabled" disabled="disabled">
							Fetching the data, Please wait...
						</a>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success" data-dismiss="modal">Okay</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Generic purpose modal end -->

		<!--Internal Questions(IQ) model start -->
		<div class="modal iqModal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
									aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Internal Questions</h4>
					</div>
					<div class="modal-body">
						<form class="form" id="addIq">
							<div class="form-group">
								<label>Questions*</label>
								<input type="text" name="question" id="question" class="form-control "
										placeholder="Enter the question">
							</div>
							<div class="form-group">
								<label>Answer</label>
								<textarea class="form-control " id="answer" name="answer"
										placeholder="Enter the answer"></textarea>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary iqSubmit" id="">Save changes</button>
					</div>
				</div>
			</div>
		</div>
		<!--Internal Questions(IQ) model end -->

		<!--Edit Internal Questions(IQ) model start -->
		<div class="modal edit-iqModal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
									aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Internal Questions</h4>
					</div>
					<div class="modal-body">
						<form class="form" id="addIq">
							<div class="form-group">
								<label>Questions*</label>
								<input type="text" name="edit-question" id="editQuestion" class="form-control "
										placeholder="Enter the question">
							</div>
							<div class="form-group">
								<label>Answer</label>
								<textarea class="form-control " id="editAnswer" name="edit-answer"
										placeholder="Enter the answer"></textarea>
							</div>

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary edit-iqSubmit" id="">Save changes</button>
					</div>
				</div>
			</div>
		</div>
		<!--Internal Questions(IQ) model start -->

		@include('tickets.whatsapp-modal')
	@show

	<!-- Application content -->
	@section('app')

		<div class="application">

			<!-- application header -->
			<header>
				@include('app.header')
			</header>

			<!-- application content -->
			<div class="container-wrap">
				@section('content')
				@show
				<div class="hide">
					<input type="hidden" id="tsDuplicate" value="{{ config('evibe.status.duplicate') }}">
				</div>
			</div>

			<!-- application footer -->
			<footer>
				@include('app.footer')
			</footer>

		</div>
	@show

	<!-- Application javascript file  -->
	<script src="{{ elixir('js/all.js') }}"></script>

	@section("google-maps-apis")
		<script src="https://maps.googleapis.com/maps/api/js?key={{ config('evibe.google.places_key') }}&libraries=places"></script>
	@show

	@section('javascript')
	@show

	<!-- OneSignal initiation -->
	@include('plugins.onesignal')

	</body>
</html>