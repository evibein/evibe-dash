@extends("layout.base")

@section('stylesheets')
	@parent
	<style type="text/css" rel="stylesheet" media="all">
		.vp-item-title {
			font-size: 12px;
			text-transform: capitalize;
		}

		.vp-item-text {
			font-weight: 600;
		}
	</style>
@endsection

@section("content")

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		@if(isset($data['virtualParty']) && $data['virtualParty'])
			<div class="vp-header">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-left">
					<a href="{{ route('virtual-party.home') }}">
						<div class="btn btn-link mar-t-5">
							<< All Virtual Parties
						</div>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10 text-left">
					<div class="font-24 mar-t-5">{{ $data['virtualParty']->celebrity_text }}'s {{ $data['virtualParty']->event }}</div>
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
					<div class="btn btn-primary" id="editVPModalBtn">
						<span class="glyphicon glyphicon-pencil"></span> Edit Virtual Party
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<hr>
			<div class="vp-body pad-b-20">
				<div class="mar-t-20">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						@if($data['virtualParty']->invite_url)
							<div class="vp-invite-img-wrap">
								<img class="full-width" src="{{ config('evibe.gallery.host').'/virtual-party/'.$data['virtualParty']->id.'/'.$data['virtualParty']->invite_url }}">
							</div>
						@else
							<div class="mar-t-10">Unable to fetch invite</div>
						@endif
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-left">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10">
							<div class="vp-item-title">
								Customer Name
							</div>
							<div class="vp-item-text">
								{{ $data['virtualParty']->name }}
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10">
							<div class="vp-item-title">
								Party Date Time
							</div>
							<div class="vp-item-text">
								{{ date('d M Y, h:i A', strtotime($data['virtualParty']->party_date_time)) }}
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10">
							<div class="vp-item-title">
								Phone
							</div>
							<div class="vp-item-text">
								{{ $data['virtualParty']->phone }}
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10">
							<div class="vp-item-title">
								Email
							</div>
							<div class="vp-item-text">
								{{ $data['virtualParty']->email }}
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10">
							<div class="vp-item-title">
								Celebrity Text
							</div>
							<div class="vp-item-text">
								{{ $data['virtualParty']->celebrity_text }}
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10">
							<div class="vp-item-title">
								Event
							</div>
							<div class="vp-item-text">
								{{ $data['virtualParty']->event }}
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-b-10">
							<div class="vp-item-title">
								Zoom Link
							</div>
							<div class="vp-item-text">
								<a href="{{ $data['virtualParty']->zoom_link }}" target="_blank">
									{{ $data['virtualParty']->zoom_link }}
								</a>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-b-10">
							<div class="vp-item-title">
								Party Url
							</div>
							<div class="vp-item-text">
								<a href="{{ config('evibe.live.host').'/virtual-party/'.$data['virtualParty']->url }}" target="_blank">
									{{ config('evibe.live.host').'/virtual-party/'.$data['virtualParty']->url }}
								</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		@else
			<div class="text-red font-24 mar-t-30 text-center">Some error occurred. Kindly report to tech team.</div>
		@endif

	</div>
	<div class="clearfix"></div>

	@include('virtual-party.modals.edit')

@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {

			$("#vpEditPartyDateTime").datetimepicker({ // party date
				timepicker: true,
				scrollInput: false,
				scrollMonth: false,
				closeOnDateSelect: false,
				format: "Y-m-d H:i"
			});

			$("#editVPModalBtn").on('click', function (event) {
				event.preventDefault();
				$("#modalEditVirtualParty").modal('show');
			});

			$(".edit-virtual-party-submit-btn").on('click', function (event) {
				event.preventDefault();

				var $data = new FormData();

				$data.append('name', $("#vpEditCustomerName").val());
				$data.append('phone', $("#vpEditPhone").val());
				$data.append('email', $("#vpEditEmail").val());
				$data.append('partyDateTime', $("#vpEditPartyDateTime").val());
				$data.append('zoomLink', $("#vpEditZoomLink").val());
				$data.append('celebrityText', $("#vpEditCelebrityText").val());
				$data.append('event', $("#vpEditEvent").val());
				$data.append('url', $("#vpEditUrl").val());

				var invite = $("#vpEditInvite")[0].files;
				$data.append('invite', invite[0]);

				var url = $(this).data('url');
				$.ajax({
					url: url,
					type: "POST",
					data: $data,
					cache: false,
					processData: false,
					contentType: false,
					success: function (data) {
						if (data.success) {
							window.showNotySuccess('Virtual Party data has been edited successfully');
							setTimeout(function () {
								location.reload();
							}, 2000);
						} else {
							var $errorMsg = 'Some error occurred while editing the virtual party. Please try again later.';
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError("Some error occurred while submitting your request");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

		});
	</script>
@endsection