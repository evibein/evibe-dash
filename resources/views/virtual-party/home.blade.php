@extends("layout.base")

@section('stylesheets')
	@parent
	<style type="text/css" rel="stylesheet" media="all">
		.vp-list-card {
			border: 1px solid #EFEFEF;
			background-color: #EFEFEF;
			border-radius: 8px;
			font-size: 14px;
			padding: 15px 0;
		}

		a .vp-list-card {
			color: #333333;
		}

		.vp-item-title {
			font-size: 12px;
			text-transform: capitalize;
		}

		.vp-item-text {
			font-weight: 600;
		}
	</style>
@endsection

@section("content")

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="vp-header">
			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-left">
				<div class="font-24 mar-t-5">Virtual Parties</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
				<div id="addVPModalBtn" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add Virtual Party
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<hr>
		<div class="vp-body">
			@if(isset($data['virtualParties']) && count($data['virtualParties']))
				@foreach($data['virtualParties'] as $virtualParty)

					<div class="vp-list-card mar-b-20">
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
							<div class="vp-item-title">
								Party
							</div>
							<div class="vp-item-text">
								{{ $virtualParty['celebrity_text'] }}'s {{ $virtualParty['event'] }}
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
							<div class="vp-item-title">
								Party Date Time
							</div>
							<div class="vp-item-text">
								{{ date('d M Y, h:i A', strtotime($virtualParty['party_date_time'])) }}
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
							<div class="vp-item-title">
								Customer Name
							</div>
							<div class="vp-item-text">
								{{ $virtualParty['name'] }}
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 mar-t-10">
							<a href="{{ route('virtual-party.details', ['id' => $virtualParty['id']]) }}">
								View Party Profile
							</a>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 mar-t-10">
							<div class="vp-item-title">
								Party Url
							</div>
							<div class="vp-item-text">
								<a href="{{ config('evibe.live.host').'/virtual-party/'.$virtualParty['url'] }}" target="_blank">
									{{ config('evibe.live.host').'/virtual-party/'.$virtualParty['url'] }}
								</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach
			@else
				<div>There are no virtual parties for the selected preferences</div>
			@endif
		</div>

	</div>
	<div class="clearfix"></div>

	@include('virtual-party.modals.add')

@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {

			$("#vpAddPartyDateTime").datetimepicker({ // party date
				timepicker: true,
				scrollInput: false,
				scrollMonth: false,
				closeOnDateSelect: false,
				format: "Y-m-d H:i"
			});

			$("#addVPModalBtn").on('click', function (event) {
				event.preventDefault();
				$("#modalAddVirtualParty").modal('show');
			});

			$(".add-virtual-party-submit-btn").on('click', function (event) {
				event.preventDefault();

				var $data = new FormData();

				$data.append('name', $("#vpAddCustomerName").val());
				$data.append('phone', $("#vpAddPhone").val());
				$data.append('email', $("#vpAddEmail").val());
				$data.append('partyDateTime', $("#vpAddPartyDateTime").val());
				$data.append('zoomLink', $("#vpAddZoomLink").val());
				$data.append('celebrityText', $("#vpAddCelebrityText").val());
				$data.append('event', $("#vpAddEvent").val());
				$data.append('url', $("#vpAddUrl").val());

				var invite = $("#vpAddInvite")[0].files;
				$data.append('invite', invite[0]);

				var url = $(this).data('url');
				$.ajax({
					url: url,
					type: "POST",
					data: $data,
					cache: false,
					processData: false,
					contentType: false,
					success: function (data) {
						if (data.success) {
							window.showNotySuccess('Virtual Party has been successfully created');
							setTimeout(function () {
								location.reload();
							}, 2000);
						} else {
							var $errorMsg = 'Some error occurred while creating the virtual party. Please try again later.';
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError("Some error occurred while submitting your request");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

		});
	</script>
@endsection