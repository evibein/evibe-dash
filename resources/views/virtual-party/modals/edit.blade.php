<div class="modal fade" id="modalEditVirtualParty" data-url="" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Update Virtual Party</h4>
			</div>
			@if(isset($data['virtualParty']) && $data['virtualParty'])
				<div class="modal-body">
					<div class="alert alert-danger hide"></div>
					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Customer Name
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<input type="text" id="vpEditCustomerName" name="vpEditCustomerName" class="form-control" @if($data['virtualParty']->name) value="{{ $data['virtualParty']->name }}" @endif>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Phone
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<input type="text" id="vpEditPhone" name="vpEditPhone" class="form-control" @if($data['virtualParty']->phone) value="{{ $data['virtualParty']->phone }}" @endif>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Email
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<input type="text" id="vpEditEmail" name="vpEditEmail" class="form-control" @if($data['virtualParty']->email) value="{{ $data['virtualParty']->email }}" @endif>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Party Date Time
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<input type="text" id="vpEditPartyDateTime" name="vpEditPartyDateTime" class="form-control" @if($data['virtualParty']->party_date_time) value="{{ $data['virtualParty']->party_date_time }}" @endif>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Zoom Link
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<input type="text" id="vpEditZoomLink" name="vpEditZoomLink" class="form-control" @if($data['virtualParty']->zoom_link) value="{{ $data['virtualParty']->zoom_link }}" @endif>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Celebrity Name
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<input type="text" id="vpEditCelebrityText" name="vpEditCelebrityText" class="form-control" @if($data['virtualParty']->celebrity_text) value="{{ $data['virtualParty']->celebrity_text }}" @endif>
							<div class="font-12 text-italic">(Eg: 'Sia' for birthday,'Raj and Sam' for anniversary)</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Event
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<input type="text" id="vpEditEvent" name="vpEditEvent" class="form-control" @if($data['virtualParty']->event) value="{{ $data['virtualParty']->event }}" @endif>
							<div class="font-12 text-italic">(Eg: 2nd Birthday Party)</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Party Url
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<div class="in-blk">evibe.in/virtual-party/</div>
							<div class="in-blk">
								<input type="text" id="vpEditUrl" name="vpEditUrl" class="form-control in-blk" @if($data['virtualParty']->url) value="{{ $data['virtualParty']->url }}" @endif>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="mar-t-15">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label class="">
								Invite
							</label>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							@if($data['virtualParty']->name)
								<div class="pad-b-5">
									<div class="in-blk mar-r-5">
										Existing Invite:
									</div>
									<div class="in-blk">
										<a href="{{ config('evibe.gallery.host').'/virtual-party/'.$data['virtualParty']->id.'/'.$data['virtualParty']->invite_url }}" target="_blank">
											{{ config('evibe.gallery.host').'/virtual-party/'.$data['virtualParty']->id.'/'.$data['virtualParty']->invite_url }}
										</a>
									</div>
								</div>
							@endif
							<input type="file" id="vpEditInvite" name="vpEditInvite">
							<div class="font-12 text-italic">(Max: 1MB | Allowed Formats: png, jpg, jpeg)</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary edit-virtual-party-submit-btn" data-url="{{ route('virtual-party.update', ['id' => $data['virtualParty']->id]) }}">Update</button>
				</div>
			@else
				<div>Unable to fetch virtual party data to update. Please refresh the page and try again.</div>
			@endif
		</div>
	</div>
</div>