<div class="modal fade" id="modalAddVirtualParty" data-url="" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Create Virtual Party</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger hide"></div>
				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Customer Name
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<input type="text" id="vpAddCustomerName" name="vpAddCustomerName" class="form-control">
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Phone
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<input type="text" id="vpAddPhone" name="vpAddPhone" class="form-control">
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Email
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<input type="text" id="vpAddEmail" name="vpAddEmail" class="form-control">
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Party Date Time
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<input type="text" id="vpAddPartyDateTime" name="vpAddPartyDateTime" class="form-control">
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Zoom Link
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<input type="text" id="vpAddZoomLink" name="vpAddZoomLink" class="form-control">
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Celebrity Name
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<input type="text" id="vpAddCelebrityText" name="vpAddCelebrityText" class="form-control">
						<div class="font-12 text-italic">(Eg: 'Sia' for birthday,'Raj and Sam' for anniversary)</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Event
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<input type="text" id="vpAddEvent" name="vpAddEvent" class="form-control">
						<div class="font-12 text-italic">(Eg: 2nd Birthday Party)</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Party Url
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<div class="in-blk">evibe.in/virtual-party/</div>
						<div class="in-blk">
							<input type="text" id="vpAddUrl" name="vpAddUrl" class="form-control in-blk">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="mar-t-15">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<label class="">
							Invite
						</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
						<input type="file" id="vpAddInvite" name="vpAddInvite">
						<div class="font-12 text-italic">(Max: 1MB | Allowed Formats: png, jpg, jpeg)</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add-virtual-party-submit-btn" data-url="{{ route('virtual-party.save') }}">Save</button>
			</div>
		</div>
	</div>
</div>