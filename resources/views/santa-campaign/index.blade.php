@extends('layout.base')

@section('content')
    <div class="container">
        <form action="{{ URL::route('santa.poster.create') }}" method="POST" role="form"
              enctype="multipart/form-data" class="hide form">
            <div class="form-group">
                <label>Select Image(s)</label>
                <input type="file" name="images[]" id="images" multiple="true">
            </div>
            <div clas="form-group">
                <select name="type" id="type">
                    @for ($i = 1; $i <= 5; $i++)
                        <option value="{{ $i }}">Frame {{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group mar-t-10">
                <button class="btn btn-success">
                    <i class="glyphicon glyphicon-upload"></i> Upload
                </button>
            </div>
        </form>
        <div class="mar-b-20">
            <form action="{{ URL::route('santa.poster.show') }}" method="GET" class="form form-inline">
                <div class="in-blk mar-r-10">
                    <label>Templates: </label>
                    <select name="template" id="template">
                        <option value="-1" selected="selected">All</option>
                        @for ($i = 1; $i <= 5; $i++)
                            <option value="{{ $i }}" @if(Input::get('template') == $i) selected="selected" @endif>
                                Template {{ $i }}</option>
                        @endfor
                    </select>
                </div>
                <div class="in-blk mar-r-10">
                    <label>Date: </label>
                    <select name="date" id="date">
                        <option value="-1" selected="selected">All</option>
                        <option value="5" @if(Input::get('date') == 5) selected="selected" @endif>Dec 5</option>
                        <option value="6" @if(Input::get('date') == 6) selected="selected" @endif>Dec 6</option>
                        <option value="12" @if(Input::get('date') == 12) selected="selected" @endif>Dec 12</option>
                        <option value="13" @if(Input::get('date') == 13) selected="selected" @endif>Dec 13</option>
                        <option value="19" @if(Input::get('date') == 19) selected="selected" @endif>Dec 19</option>
                        <option value="20" @if(Input::get('date') == 20) selected="selected" @endif>Dec 20</option>
                        <option value="25" @if(Input::get('date') == 25) selected="selected" @endif>Dec 25</option>
                    </select>
                </div>
                <div class="in-blk mar-r-10">
                    <label>Status: </label>
                    <select name="status" id="status">
                        <option value="-1" selected="selected">All</option>
                        <option value="1" @if(Input::get('status') == 1) selected="selected" @endif>Email Sent</option>
                        <option value="2" @if(Input::get('status') == 2) selected="selected" @endif>Email Not Sent
                        </option>
                    </select>
                </div>
                <div class="in-blk mar-r-10">
                    <label>Marked: </label>
                    <select name="marked" id="marked">
                        <option value="-1" selected="selected">All</option>
                        <option value="1" @if(Input::get('marked') == 1) selected="selected" @endif>Valid</option>
                        <option value="2" @if(Input::get('marked') == 2) selected="selected" @endif>
                            Invalid
                        </option>
                    </select>
                </div>
                <div class="in-blk mar-r-10">
                    <button type="submit" class="btn btn-warning btn-sm">
                        <i class="glyphicon glyphicon-filter"></i> Filter
                    </button>
                </div>
            </form>
        </div>
        <div class="select-actions mar-b-20">
            <div class="pull-left">
                <a id="emailPosters" href="#" class="btn btn-default btn-md mar-r-10" disabled="disabled">
                    <i class="glyphicon glyphicon-send"></i> Email Posters
                </a>
                <a id="smsInvalidEmailBtn" href="#" class="btn btn-info btn-md mar-r-10"
                   data-url="{{ URL::route('santa.poster.sms.invalid.emails') }}">
                    <i class="glyphicon glyphicon-send"></i> SMS Invalid Emails
                </a>
            </div>
            <div class="pull-right">
                <form action="{{ URL::route('santa.poster.show') }}">
                    <input type="text" name="query" class="in-blk form-control" style="width: 200px;"
                           placeholder="Enter search query" value="{{ Input::get('query') }}">
                    <button type="submit" class="in-blk btn btn-primary btn-sm">
                        <i class="glyphicon glyphicon-search"></i> Search
                    </button>
                    <a href="{{ URL::route('santa.poster.show') }}" class="btn btn-default btn-sm">
                        <i class="glyphicon glyphicon-remove"></i> Cancel
                    </a>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        @if ($data['users']->count())
            <table class="table table-bordered table-striped" role="table">
                <thead>
                <tr>
                    <th></th>
                    <th>Poster</th>
                    <th>Actions</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Date</th>
                    <th>Pic</th>
                    <th>Template</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach ($data['users'] as $user)
                    <tr>
                        <td>
                            @if (is_null($user->email_sent_at) && !$user->is_invalid && $user->email)
                                <label><input type="checkbox" name="select-poster" class="select-poster"
                                              data-id="{{ $user->id }}"> Select</label>
                            @endif
                        </td>
                        <td>
                            @if ($user->poster_url)
                                <img src="{{ $user->getPosterUrl() }}" class="poster" style="width: 60px; height: 40px;"
                                     rel="popover" data-toggle="popover" data-container="body">
                                <div class="hide big-image" style="position: absolute; z-index: 1002;">
                                    <img src="{{ $user->getPosterUrl() }}" style="width: 400px;">
                                </div>
                            @else
                                --
                            @endif
                        </td>
                        <td class="action" data-mbad="{{ URL::route('santa.poster.mark-bad', $user->id) }}"
                            data-munbad="{{ URL::route('santa.poster.unmark-bad', $user->id) }}">
                            <div class="editable-actions">
                                @if (!$user->status)
                                    @if ($user->is_invalid)
                                        <div class="case1">
                                            <div class="text-danger">Marked Bad</div>
                                            <a data-type="unbad" href="#" class="text-primary btn-mark">
                                                <i class="glyphicon glyphicon-remove"></i> Un-Mark Bad
                                            </a>
                                        </div>
                                    @elseif ($user->email)
                                        <div class="case3">
                                            <a data-type="bad" href="#" class="text-danger btn-mark">
                                                <i class="glyphicon glyphicon-remove"></i> Mark Bad
                                            </a>
                                        </div>
                                    @elseif (!$user->email && $user->invalid_email_sms_sent_at)
                                        <div class="case4 text-info">Invalid SMS Sent
                                            <div>{{ date('d M', strtotime($user->invalid_email_sms_sent_at)) }}</div>
                                        </div>
                                    @else
                                        <div class="case5"><span class="text-danger">No Email</span></div>
                                    @endif
                                @else
                                    <div class="text-success">Sent
                                        on {{ date("d M", strtotime($user->email_sent_at)) }}</div>
                                @endif
                            </div>
                            <div>
                                <a href="#" class="text-warning btn-update-details"
                                   data-name="{{ $user->name }}"
                                   data-email="{{ $user->email }}"
                                   data-phone="{{ $user->phone }}"
                                   data-frame="{{ $user->frame_id }}"
                                   data-pic="{{ $user->pic_id }}"
                                   data-url="{{ URL::route('santa.poster.update', $user->id) }}">
                                    <i class="glyphicon glyphicon-pencil"></i> Update Details
                                </a>
                            </div>
                        </td>
                        <td>
                            @if ($user->status)
                                <span class="text-success text-bold">Email Sent</span>
                            @else
                                <span class="text-danger">Email Not Sent</span>
                            @endif
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>
                            @if ($user->email)
                                {{ $user->email }}
                            @else --
                            @endif
                        </td>
                        <td>
                            @if ($user->phone)
                                {{ $user->phone }}
                            @else
                                --
                            @endif
                        </td>
                        <td>{{ date("d M y", strtotime($user->taken_at)) }}</td>
                        <td>{{ $user->pic_id }}</td>
                        <td>{{ $user->frame_id }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="mar-t-15 text-center">
                {{ $data['users']->appends(Input::except('page'))->links(); }}
            </div>
        @else
            <div class="alert alert-danger">
                <i class="glyphicon glyphicon-warning-sign"></i> No users found.
            </div>
        @endif
    </div>
    <div class="hide markups">
        <div class="case1">
            <div class="text-danger">Marked Bad</div>
            <a data-type="unbad" href="#" class="text-primary btn-mark">
                <i class="glyphicon glyphicon-remove"></i> Un-Mark Bad
            </a>
        </div>
        <div class="case3">
            <a data-type="bad" href="#" class="text-danger btn-mark">
                <i class="glyphicon glyphicon-remove"></i> Mark Bad
            </a>
        </div>
    </div>
    <div id="modalUpdateDetails" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Update Details</h4>
                </div>
                <div class="modal-body">
                    <div class="mar-b-10 alert alert-danger error-cnt hide">
                        <div class="error"></div>
                    </div>
                    <form id="formUpdateDetails" action="" method="POST">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" id="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" id="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" id="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Pic Id</label>
                            <input type="text" id="pic" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Frame Id</label>
                            <input type="text" id="frame" class="form-control">
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" id="isResend" checked="checked"> Mark for Resend email</label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btnSubmitDetails" type="button" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {

            // email posters
            $('table').on('change', '.select-poster', function (event) {
                event.preventDefault();
                if ($('.select-poster:checked').length) {
                    $('#emailPosters').removeAttr('disabled').addClass('btn-success');
                } else {
                    $('#emailPosters').attr('disabled', 'disabled').removeClass('btn-success');
                }
            });

            $('#emailPosters').click(function (event) {
                event.preventDefault();
                $('#modalLoading').modal('show');
                var ids = '';
                $.each($('.select-poster:checked'), function (key, item) {
                    ids += $(item).data('id') + ',';
                });
                if (ids) {
                    ids = ids.substr(0, ids.length - 1); // remove trailing ,
                    $.ajax({
                        url: '/santa-poster/send',
                        type: 'POST',
                        dataType: 'json',
                        data: {ids: ids}
                    }).done(function (data, textStatus, jqXHR) {
                        $('#modalLoading').modal('show');
                        if (data.success) {
                            alert("All valid posters emailed successfully");
                            location.reload();
                        } else {
                            alert("FAILED TO EMAIL POSTERS!!!");
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        alert("FAILED TO EMAIL POSTERS!!!");
                    });
                }
            });

            // show large posters
            $('.poster').mouseover(function (event) {
                $(this).parent().find('.big-image').removeClass('hide');
            }).mouseout(function (event) {
                $(this).parent().find('.big-image').addClass('hide');
            });

            // mark / unmark bad
            $('.action').on('click', '.btn-mark', function (event) {
                event.preventDefault();
                var parent = $(this).parents('.action');
                var editableParent = parent.find('.editable-actions');
                $.ajax({
                    url: $(this).data('type') == "bad" ? $(parent).data('mbad') : $(parent).data('munbad'),
                    type: 'POST',
                    dataType: 'json'
                }).done(function (data, textStatus, jqXHR) {
                    if (data.success) {
                        if (data.type == 'bad') {
                            var markup = $('.markups').find('.case1').clone();
                            editableParent.html(markup);
                        }
                        if (data.type == 'not-bad') {
                            var markup = $('.markups').find('.case3').clone();
                            editableParent.html(markup);
                        }
                    } else {
                        alert("Unable to complete action");
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert("Unable to complete action, report to Admin");
                });
            });

            // update details - show modal
            $('.btn-update-details').click(function (event) {
                event.preventDefault();
                var that = $(this);
                var modal = $('#modalUpdateDetails');
                modal.find('#name').val(that.data('name'));
                modal.find('#email').val(that.data('email'));
                modal.find('#phone').val(that.data('phone'));
                modal.find('#pic').val(that.data('pic'));
                modal.find('#frame').val(that.data('frame'));
                modal.find('#formUpdateDetails').attr('action', that.data('url'));
                modal.find('.error-cnt').addClass('hide');
                modal.modal('show');
            });

            // update details - save
            $('#btnSubmitDetails').click(function (event) {
                event.preventDefault();
                var modal = $('#modalUpdateDetails');
                var data = {
                    'name': modal.find('#name').val(),
                    'email': modal.find('#email').val(),
                    'phone': modal.find('#phone').val(),
                    'frame': modal.find('#frame').val(),
                    'pic': modal.find('#pic').val(),
                    'isResend': modal.find('#isResend').is(':checked') ? 1 : 0
                };

                $.ajax({
                    url: $('#formUpdateDetails').attr('action'),
                    type: 'POST',
                    dataType: 'JSON',
                    data: data
                }).done(function (data, textStatus, jqXHR) {
                    if (data.success) {
                        location.reload();
                    }
                    else {
                        modal.find('.error').text(data.error);
                        modal.find('.error-cnt').removeClass('hide');
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert('ERROR :: FAILED TO UPDATE DETAILS!!!');
                    modal.modal('hide');
                });
            });

            // SMS invalid email
            $('#smsInvalidEmailBtn').click(function (event) {
                event.preventDefault();
                $('#modalLoading').modal('show');

                $.ajax({
                    url: $(this).data('url'),
                    type: 'POST',
                    dataType: 'JSON'
                }).done(function (data, textStatus, jqXHR) {
                    if (data.success) {
                        location.reload();
                    }
                    else {
                        $('#modalLoading').modal('hide');
                        alert('FAILED TO SEND SMSes');
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert('FAILED TO SEND SMSes, REPORT ADMIN');
                });
            });
        });
    </script>
@stop