@extends('layout.base')

@section('page-title')
	<title>Error 404. Page not found | Evibe</title>
@endsection

@section("content")
	<section class="mar-t-30">
		<div class="text-center">
			<img src="{{ config("evibe.gallery.host") }}/main/img/icons/error-404.png" alt="Error page">
		</div>
		<div class="mar-t-30 text-center">
			<h1 class="text-danger">Oops... page not found :(</h1>
			<p class="text-muted">Sorry, the page you are looking for could not be found. The page might have been moved, temporarily unavailalbe or never existed.</p>
		</div>
	</section>
@endsection