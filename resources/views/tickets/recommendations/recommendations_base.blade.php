<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">

	@section('title')
		<title>EvibeDash by Evibe.in</title>
	@show

	@section('stylesheets')
		<link rel="stylesheet" href="{{ elixir('css/all.css') }}">
	@show

	@section('custom-head')
	@show

	@if (auth()->check())
		<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"
				async='async'></script> <!-- Logged In user: OneSignal web push notifications -->
	@endif
	<link rel="icon" href="https://gallery.evibe.in/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16">

</head>

@section('custom-body')
	<body>
	@show

	@section('app-modals')
		<!-- Add new area modal begin -->
		<div id="addNewAreaModal" class="modal " tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Add New Area</h4>
					</div>
					<div class="modal-body">
						<form id="addAreaForm" action="/add-area" method="POST">
							<div class="form-group">
								<label>City</label>
								<select name="addAreaCity" id="addAreaCity" class="form-control">
									@foreach (\City::all() as $city)
										<option value="{{ $city->id }}">{{ $city->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="from-group">
								<label>Area Name</label>
								<input type="text" id="areaName" class="form-control">
								<input type="hidden" id="locationDetails" value="">
							</div>
							<div class="form-group pad-t-10">
								<label>Area PinCode</label>
								<input type="text" class="form-control" id="zipCode" name="zipCode"
										placeholder="Enter Pin Code">
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary btn-submit-new-area">Submit</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Add new area modal end -->

		<!-- Loading modal begin -->
		<div id="modalLoading" class="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-body">
						<h4>
							<img src="{{ AppUtil::getGalleryBaseUrl() }}/img/icons/loading.gif"
									alt="Loading icon"> Loading, please wait...
						</h4>
						<div class="text-danger">(Please do not press refresh / back button)</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Loading modal end -->

		<!-- Generic purpose modal begin -->
		<div id="modalGeneric" class="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<input type="hidden" value="{{config('evibe.gallery.host')}}" class="gallery-url">
					<div class="modal-header">
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success" data-dismiss="modal">Okay</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Generic purpose modal end -->
	@show

	<!-- Application content -->
	@section('app')
		<div class="application">
			<!-- application content -->
			<div class="container-wrap">
				@section('content')
				@show
			</div>
		</div>
	@show

	<!-- Application javascript file  -->
	<script src="{{ elixir('js/all.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key={{ config('evibe.google.places_key') }}&libraries=places"></script>

	@section('javascript')
	@show

	</body>
</html>