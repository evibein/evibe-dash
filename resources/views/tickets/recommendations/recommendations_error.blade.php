@extends('tickets.recommendations.recommendations_base')

@section('content')
	<div class="alert alert-danger font-16 text-center mar-l-20 mar-r-20 mar-t-30"><b>Event Type</b> is not defined for the ticket. Please put an <b>Event Type</b> for the ticket to continue.</div>
	<a href="/tickets/{{$data['ticketId']}}/bookings">Click Here To go to previous page.</a>
@endsection