<div class="pad-l-10 pad-r-10">
	@if(isset($data['recoUrl']) && $data['recoUrl'])
		<div class="text-center mar-b-10">
			<a href="{{ $data['recoUrl'] }}?isFromDash=true" target="_blank">
				<div class="btn btn-primary">See Recommendations Page</div>
			</a>
			@if(isset($data['lastRecommendedAt']) && $data['lastRecommendedAt'])
				<span class="text-info mar-l-5">
					<span class="text-black">Last recommended at: </span>
					{{ date('d M Y H:i A', strtotime($data['lastRecommendedAt'])) }}
				</span>
			@endif
		</div>
		<div class="clearfix"></div>
	@endif
	<div class="text-center">
		<div class="text-bold in-blk font-24">Selected Options</div>
	</div>
	<div class="clearfix"></div>
	@if(count($data['recommendationData']) > 0)
		@foreach ($data['recommendationData'] as $key => $options)
			<div>
				<h4><b>{{ $key }}</b></h4>
				@php $optionCount = 0 @endphp
				@foreach($options as $option)
					@php $optionCount++ @endphp
					<div class="col-sm-2 @if(($optionCount-1)%5 != 0){{ "col-half-offset" }}@endif no-pad-r panel no-pad-l panel-default remove-btn-{{ $option["id"] }}">
						<div class="col-sm-3 no-pad-l no-pad-r">
							<a href="{{ $option["url"] }}" target="_blank">
								<img class="profile-img" src="{{ $option["imageUrl"] }}" width="60" height="70">
							</a>
						</div>
						<div class="col-sm-9 no-pad-l pull-left">
							<div class="title pad-l-10">
								<a href="{{ $option["url"] }}" target="_blank">{{ $option["name"] }}</a>
							</div>
							<div class="pad-t-3 pad-l-10">
								<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($option["priceMin"]) }}
								@if ($option["priceMax"])
									-
									<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($option["priceMax"]) }}
								@endif
							</div>
						</div>
						<div class="clearfix"></div>
						<a class="btn remove-btn btn-xs option-remove-btn"
								data-id="{{ $option["id"] }}"
								data-service-id="{{ $option["ticketTypeId"] }}"
								data-value="0"
								data-url="{{ route("ticket.reco.edit.option", $data["ticketId"]) }}">Remove</a>
					</div>
				@endforeach
			</div>
			<div class="clearfix"></div>
		@endforeach
		<div class="clearfix"></div>
		<hr>
		<div class="text-center mar-b-20">
			<a href="{{ route("ticket.details.info", [$data["ticketId"]]) }}" class="btn btn-link"><i
						class="glyphicon glyphicon-remove-circle"></i> CLOSE</a>
			<a class="btn btn-success btn-lg in-blk btn-send-reco "
					data-url="{{ route("ticket.reco.send", $data["ticketId"]) }}"><i
						class="glyphicon glyphicon-send"
						@if(isset($data['statusId']) && $data['statusId'] == config('evibe.status.invalid_email')) disabled @endif></i> Send Recommendation Email</a>
		</div>
	@else
		<div class="alert alert-danger">No options Selected, Please select atleast one option to send recommendations</div>
	@endif
</div>