@extends('tickets.recommendations.recommendations_base')

@section('content')
	<div id="wizard" class="form_wizard wizard_horizontal mar-t-10">
		<ul class="wizard_steps wizard-steps-service-tag-wrap anchor mar-t-10">
			<li>
				<a class="selected selected-screen-progress-tags pink-btn text-bold selected-screen-progress-steps"
						isdone="1"
						rel="1">
					<span class="step_no">1</span>
					<span class="step_descr">Goto Selected Services<br>
					</span>
				</a>
			</li>
			@php $stepCount = 1 @endphp
			@if(isset($data["selectedServiceTagIds"]))
				@foreach($data["selectedServiceTagIds"] as $tag)
					@php $typeTicket = $data["typeTicket"]->where("id", $tag)->first() @endphp
					@if($typeTicket)
						@php $stepCount++ @endphp
						<li>
							<a data-url="{{ route("ticket.reco.service.options", [$data["ticketId"], $typeTicket->id]) }}"
									class="selected selected-screen-progress selected-screen-progress-steps" isdone="1"
									rel="{{ $stepCount }}">
								<span class="step_no">{{ $stepCount }}</span>
								<span class="step_descr">Goto {{ ucfirst($typeTicket->name) }}</span>
							</a>
						</li>
					@endif
				@endforeach
			@endif
			@php $stepCount++ @endphp
			<li>
				<a data-url="{{ route("ticket.reco.selected.options", [$data["ticketId"]]) }}"
						class="selected selected-screen-progress selected-screen-progress-steps" isdone="1"
						rel="{{ $stepCount }}">
					<span class="step_no">{{ $stepCount }}</span>
					<span class="step_descr">Goto Preview<br></span>
				</a>
			</li>
		</ul>
	</div>
	<hr class="mar-t-10">
	<div class="selected-service-tag-wrap">
		@if(isset($data['recoUrl']) && $data['recoUrl'])
			<div class="text-center mar-b-10">
				<a href="{{ $data['recoUrl'] }}?isFromDash=true" target="_blank">
					<div class="btn btn-primary">See Recommendations Page</div>
				</a>
				@if(isset($data['lastRecommendedAt']) && $data['lastRecommendedAt'])
					<span class="text-info mar-l-5">
						<span class="text-black">Last recommended at: </span> {{ date('d M Y H:i A', strtotime($data['lastRecommendedAt'])) }}
					</span>
				@endif
			</div>
			<div class="clearfix"></div>
		@endif
		<div class="text-center">
			<h3 class="text-center mar-t-10 mar-b-10 text-bold in-blk">Selected Services</h3>
			<a href="{{ route("ticket.modify.edit.services", [$data["ticketId"], 1, "redirect" => route("ticket.reco.selected.services", $data["ticketId"])]) }}"
					class="btn btn-warning btn-xs mar-l-10 mar-t-10 valign-top"><i
						class="glyphicon glyphicon-pencil"></i> Edit</a>
			<a href="{{ route("ticket.details.info", $data["ticketId"]) }}"
					class="pull-right btn btn-link text-danger in-blk"><i
						class="glyphicon glyphicon-remove-circle"></i> Close</a>
		</div>
		@if(session()->has('errorMsg'))
			<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
				<a href="#" class="close" data-dismiss="alert"
						aria-label="close">&times;</a>
				<i class="glyphicon glyphicon-remove-sign"></i>
				{{ session('errorMsg') }}
			</div>
		@endif
		@if(isset($data["categorizedServices"]))
			<div class="mar-l-10 mar-r-10 no-mar-b">
				@foreach($data["categorizedServices"] as $key => $service)
					<h4 class="pad-l-15"><b>{{ $key }} :</b></h4>
					<div class="panel-body no-pad mar-r-10 categorized-services-wrap">
						@php $count = 0; @endphp
						@foreach($service as $serviceTag)
							@php $count++; @endphp
							<div class="col-md-3 col-sm-3 col-xs-3 no-pad-r pad-b-10">
								<div class="panel panel-default no-pad-b no-mar-b">
									<div class="pad-10">
										<b>{{ $serviceTag["name"] }}</b><br>
									</div>
								</div>
							</div>
							@if($count%4 == 0)
								<div class="clearfix"></div>
							@endif
						@endforeach
						<div class="clearfix"></div>
					</div>
				@endforeach
			</div>
		@endif
	</div>
	<div class="recommendations-wrap hide">

	</div>
	<div class="recommendations-loading hide"></div>
	<div id="modalAskRecoFollowupTime" class="modal" tabindex="-1" role="dialog" data-url="" data-type="">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header mar-t-10">
					<h4 class="modal-title">Set customer auto followup date & time</h4>
				</div>
				<div class="modal-body">
					<div class="hide alert-danger pad-6 mar-b-15 " id="errorBox"></div>
					<div class="form-group">
						<div class="alert alert-info">
							<i class="glyphicon glyphicon-info-sign"></i> Please set correct date & time to auto followup customer. An Email & SMS notifications will be sent to customer. Team reminder will be sent for this followup.
						</div>
						<div class="clearfix"></div>
						<div class="pad-l-20 pad-r-20">
							<div class="no-pad-l col-md-12">
								<div class="form-group">
									<input type="hidden" class="reco-followup-date" value="">
									<div class="col-md-4 text-bold">
										Followup date:
									</div>
									<div class="col-md-8">
										<input type="text"
												class="edit-followup-date form-control edit-followup-date-reco"
												value="">
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12 no-pad-l mar-t-10">
								<div class="form-group">
									<div class="col-md-4 text-bold">
										FollowUp Type:
									</div>
									<div class="col-md-8">
										<select name="followupComment" id="followupComment"
												class="form-control text-normal">
											<option selected value="">
												--Select FollowUp Type--
											</option>
											@foreach($data['typeFollowUps'] as $typeFollowUp)
												<option value="{{$typeFollowUp['name']}}">{{$typeFollowUp['name']}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12 no-pad-l">
								<div class="form-group comment-section">
									<div class="col-md-4 text-bold">
										Comments:
									</div>
									<div class="col-md-8">
								<textarea class="followup-edit-comments form-control" rows="3"
										placeholder="Enter followups comment"></textarea>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="no-pad-l col-md-12">
								<div class="form-group">
									<input type="hidden" class="closure-date" value="">
									<div class="col-md-4 text-bold">Closure Date:</div>
									<div class="col-md-8">
										<input type="text" class="edit-closure-date form-control edit-closure-date-reco"
												value="">
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="selected-service-modal-whatsapp-section mar-t-20"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a class="btn btn-success btn-save-reco-followup"
							data-redirect-url="{{ route("ticket.details.mappings", $data["ticketId"]) }}"
							data-ticket-id="{{ $data["ticketId"] }}"><i
								class="glyphicon glyphicon-floppy-save"></i> Save
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/recommendations.js");
		});
	</script>
@endsection