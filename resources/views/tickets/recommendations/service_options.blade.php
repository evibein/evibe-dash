<div class="text-center pad-b-10">
	@if(isset($data['recoUrl']) && $data['recoUrl'])
		<div class="text-center mar-b-10">
			<a href="{{ $data['recoUrl'] }}?isFromDash=true" target="_blank">
				<div class="btn btn-primary">See Recommendations Page</div>
			</a>
			@if(isset($data['lastRecommendedAt']) && $data['lastRecommendedAt'])
				<span class="text-info mar-l-5">
					<span class="text-black">Last recommended at: </span> {{ date('d M Y H:i A', strtotime($data['lastRecommendedAt'])) }}
				</span>
			@endif
		</div>
		<div class="clearfix"></div>
	@endif
	<h3 class="text-bold in-blk no-mar-t">{{ $data["properties"]["name"] }}</h3>
	<a href="{{ route("ticket.details.info", $data["ticketId"]) }}" class="pull-right btn btn-link text-danger in-blk">
		<i class="glyphicon glyphicon-remove-circle"></i>
		<span> Close</span>
	</a>
	@if (isset($data['priceSort']) && ($data['priceSort'] == 'plth'))
		<a class="pull-right btn btn-link text-primary in-blk  btn-link text-primary in-blk sort-option"
				data-price="phtl">Price Low to High</a>
	@elseif (isset($data['priceSort']) && ($data['priceSort'] == 'phtl'))
		<a class="pull-right btn btn-link text-primary in-blk  btn-link text-primary in-blk sort-option"
				data-price="">Price High to Low</a>
	@else
		<a class="pull-right btn btn-link text-primary in-blk  btn-link text-primary in-blk sort-option"
				data-price="plth">Price</a>
	@endif
	<div class="pull-right btn">Sort By:</div>
</div>
<div class="mar-l-10">
	<div class="col-sm-2 alert alert-info">
		@if ($data['filters']['clearFilter'])
			<div class="text-center pad-b-10">
				<a class="btn btn-warning btn-xs reset-filters">
					<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
				</a>
			</div>
		@endif
		<label>Search</label>
		<div class="mar-b-15 search-input-wrap">
			<input id="searchInput" type="text" placeholder="By name, code"
					value="{{ $data['filters']['search'] }}"
					class="form-control"
					data-old="{{ $data['filters']['search'] }}"/>
		</div>
		<label>&#8377; Price</label>
		<div class="reco-options-price-filter-wrap">
			<div class="col-xs-6 col-sm-6 no-pad-l pad-r-6">
				<input id="priceMin" class="form-control pad-l-5 pad-r-6" type="text"
						value="{{ $data['filters']['priceMin'] }}">
				<div class="text-muted text-center">From</div>
			</div>
			<div class="col-xs-6 col-sm-6 no-pad-r pad-l-5 in-blk">
				<input id="priceMax" class="form-control pad-l-5 pad-r-6" type="text"
						value="{{ $data['filters']['priceMax'] }}">
				<div class="text-muted text-center">To</div>
			</div>
		</div>
		@php $selectedFilters = isset($data['filters']['active']) ? explode(",", $data['filters']['active']) : []; @endphp
		@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
			<label class="mar-t-15">Select Category</label>
			<div class="filter link-filter categories-filter" data-type="category">
				<ul class="no-mar no-pad ls-none item-sub-cats packages-sub-cats">
					@foreach ($data['filters']['allCategories'] as $catId => $catData)
						@if (array_key_exists($catId, $data['filters']['catCounts']))
							<li class="@if (!count($catData['child']) && $data['filters']['active'] == $catData['url']) active @endif">
								<div>
									<div class="pad-t-5">
										@if(!count($catData['child']) && $data['filters']['catCounts'][$catId])
											<input class="decor-cat-link"
													type="checkbox" name="{{ $catData['url'] }}"
													id="{{ $catData['url'] }}" value="{{ $catData['url'] }}"
													@if(in_array(strval($catData['url']), $selectedFilters)) checked @endif>
											<label class="text-normal inline"
													for="{{ $catData['url'] }}">{{ ucwords($catData['name']) }}</label>
										@elseif(count($catData['child']))
											<span>{{ ucwords($catData['name']) }}</span>
										@else
											<span class="disabled">{{ ucwords($catData['name']) }}</span>
										@endif
										@if (!count($catData['child']))
											<span class="font-12 text-muted count @if(!$data['filters']['catCounts'][$catId]) disabled @endif">({{ $data['filters']['catCounts'][$catId] }})</span>
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
								@if (count($catData['child']))
									<ul class="no-mar no-pad item-child-cats">
										@foreach($catData['child'] as $childCatId => $childCatData)
											<li class="@if ($data['filters']['active'] == $childCatData['url']) active @endif">
												<div class="pad-t-5">
													@if($data['filters']['catCounts'][$childCatId])
														<input class="decor-cat-link"
																type="checkbox" name="{{ $childCatData['url'] }}"
																id="{{ $childCatData['url'] }}"
																@if(in_array(strval($childCatData['url']), $selectedFilters)) checked
																@endif
																value="{{ $childCatData['url'] }}">
														<label class="text-normal inline"
																for="{{ $childCatData['url'] }}">{{ ucwords($childCatData['name']) }}</label>
													@else
														<span class="disabled">{{ ucwords($childCatData['name']) }}</span>
													@endif
													<span class="font-12 text-muted count @if(!$data['filters']['catCounts'][$childCatId]) disabled @endif">({{ $data['filters']['catCounts'][$childCatId] }})</span>
												</div>
												<div class="clearfix"></div>
											</li>
										@endforeach
									</ul>
								@endif
							</li>
						@endif
					@endforeach
				</ul>
			</div>
		@endif
			@if(isset($data['filters']['allAreas']))
				<label class="mar-t-15"> Select Area</label>
				<div style="overflow-y: scroll; height:400px;">
				<div class="filter link-filter areas-filter" data-type="area">
					<ul class="no-mar no-pad ls-none item-sub-cats packages-sub-cats">

						@foreach($data['filters']['allAreas'] as $area)
							@if(isset($area['area_id']))
								<input class="city-area"
										type="checkbox" name="{{ $area['area_name'] }}"
										id="{{ $area['area_id'] }}" value="{{ $area['area_id'] }}"
										@if(in_array(strval($area['area_id']), $data['filters']['activeAreas'])) checked @endif
								>
								<label class="text-normal inline"
										for="{{ $area['area_id'] }}">{{ ucwords($area['area_name']) }}</label>
								<div class="clearfix"></div>
							@endif
						@endforeach
					</ul>
				</div></div>
			@endif
		<div class="text-center pad-t-10">
			<a class="btn btn-success btn-reco-options-filter">FILTER</a>
		</div>
	</div>
	<div class="col-sm-10 no-pad-l">
		@if(count($data['options']) > 0)
			@foreach ($data['options'] as $options)
				<div class="col-sm-2 no-pad-r">
					<div class="a-package pos-rel">
						<div class="status-corner-tag status-corner-tag-{{ $options['id'] }} @if($options["isSelected"] == 0){{ "hide" }}@endif">
							<div class="ribbon-reco pink">
								<span>Selected</span></div>
						</div>
						<div class="">
							<a href="{{ $options["url"] }}" target="_blank">
								<img class="profile-img"
										src="{{ $options["profilePic"] }}">
							</a>
						</div>
						<div class="pad-l-5 text-center">
							<div class="title pad-t-5">
								<a href="{{ $options["url"] }}"
										target="_blank">{{ $options["name"] }}</a>
							</div>
							<div class="pad-t-5 pad-b-4">
								@if($data["serviceId"] == config("evibe.ticket_type.cakes"))
									<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($options["pricePerKg"]) }}
								@else
									<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($options["minPrice"]) }}
									@if ($options["maxPrice"])- <span
											class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($options["maxPrice"]) }}@endif
								@endif
							</div>
						</div>
						<a class="btn recommend-this-btn recommend-this-btn-{{ $options["id"] }} option-recommendation-btn @if($options["isSelected"] != 0){{ "hide" }}@endif no-pad-l no-pad-r text-center"
								data-id="{{ $options["id"] }}"
								data-value="1"
								data-service-id="{{ $data["serviceId"] }}"
								data-url="{{ route("ticket.reco.edit.option", $data["ticketId"]) }}">SELECT THIS</a>
						<a class="btn recommended-btn recommended-btn-{{ $options["id"] }} option-recommendation-btn @if($options["isSelected"] == 0){{ "hide" }}@endif"
								data-id="{{ $options["id"] }}"
								data-value="0"
								data-service-id="{{ $data["serviceId"] }}"
								data-url="{{ route("ticket.reco.edit.option", $data["ticketId"]) }}">UNSELECT</a>
					</div>
				</div>
			@endforeach
			<div class="clearfix"></div>
		@else
			<div class="alert alert-danger mar-l-10 mar-r-10">No options found</div>
		@endif
	</div>
	<div class="clearfix"></div>
	<input type="hidden" name="mainQueryUrl" id="mainQueryUrl"
			value="{{ route("ticket.reco.service.options", [$data["ticketId"], $data["serviceId"]]) }}">
</div>