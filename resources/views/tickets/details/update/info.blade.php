@extends('tickets.details.base.base')
@section('section-info-message')
	Ticket Updates + Ticket Followup
@endsection
@section('ticket-details-content')
	<ul class="sub-heading list-unstyled list-inline text-center">
		<li><a class="btn btn-evibe-info btn-link" href="#ticketUpdates">Ticket Update</a></li>
		<li><a class="btn btn-evibe-info btn-link" href="#followup-section">Ticket Followup</a></li>
	</ul>
	<div id="ticketUpdates" class="ticket-actions-wrap mar-t-25">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="sec-sub-title pull-left">Ticket Updates</h4>
					<div class="pull-right">
						<span class="evibe-info text-normal font-12">
									<span class="badge font-12 evibe-badge">{{count($ticket->statusUpdates)}}</span>
									<span class="mar-l-2">Updates</span>
								</span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div id="ticketUpdateBody" class="panel-body ">
					@if (session()->has('statusUpdateError') || $errors->count() > 0)
						<div class="alert alert-danger pad-8" role="alert">
							<ul class="erros-list ls-none">
								@if (session()->has('statusUpdateError'))
									<li>{{ session('statusUpdateError') }}</li>
								@endif
								@if ($errors->count())
									<li>{{ $errors->first() }}</li>
								@endif
							</ul>
						</div>
					@endif
					<form action="{{ route('ticket.status.update', $ticket->id) }}" method="POST" class=" mar-b-20">
						<div>
							<div class="col-md-3 no-pad-l">
								<div class="form-group">
									<div><label for="tuDate">Update Date*</label></div>
									<input id="tuDate" name="tuDate" type="text" class="form-control tu-date"
											placeholder="when did this update happen?"
											value="@if(old('tuDate')){{ old('tuDate') }}@else{{ date("Y/m/d H:i") }}@endif">
								</div>
							</div>
							@if($ticket->status_id != config('evibe.status.booked') &&
									 $ticket->status_id != config('evibe.status.confirmed') &&
										 $ticket->status_id != config('evibe.status.service_auto_pay') &&
											 $ticket->status_id != config('evibe.status.auto_pay'))
								<div class="col-md-3">
									<div class="form-group">
										<div><label for="tuStatus">Status*</label></div>
										<select id="tuStatus" name="tuStatus" class="form-control">
											@php $validStatuses = array_diff_key($statuses, [config("evibe.status.booked") => 0, config("evibe.status.auto_pay") => 0, config("evibe.status.service_auto_pay") => 0]); @endphp
											@foreach ($validStatuses as $key => $value)
												<option value="{{ $key }}"
														@if(old("tuStatus") == $key) selected @endif>{{ $value }}</option>
											@endforeach
										</select>
									</div>
								</div>
							@endif
							<div class="col-md-3">
								<div class="form-group">
									<div><label for="tuType">Through*</label></div>
									<select id="tuType" name="tuType" class="form-control">
										<option value=""
												@if(old("tuType") == "") selected @endif>--Select the source--
										</option>
										<option value="phone"
												@if(old("tuType") == "phone") selected @endif>Phone
										</option>
										<option value="email"
												@if(old("tuType") == "email") selected @endif>Email
										</option>
										<option value="whatsapp"
												@if(old("tuType") == "whatsapp") selected @endif>WhatsApp
										</option>
										<option value="other"
												@if(old("tuType") == "other") selected @endif>Other
										</option>
									</select>
								</div>
							</div>
							<div class="col-md-2 mar-t-25">
								<div class="form-group valign-bottom">
									<button type="submit" class="btn btn-primary">
										<i class="glyphicon glyphicon-plus"></i> Add Update
									</button>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="related_ticket_wrap hide">
							<div class="col-lg-8 no-pad-l pad-b-10">
								<div class="form-group no-mar-b">
									<label for="relatedTicket">Enter Related Ticket ID*</label>
									<input id="relatedTicket" name="relatedTicket" type="text" class="form-control" value="">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="followup-date-wrap no-response-wrap hide">
							<div class="form-group col-sm-4 no-pad-l followup-date-wrap no-response-wrap">
								<div><label for="followUpDate">Next Follow up date*</label></div>
								<input id="followUpDate" name="followUpDate" type="text" class="form-control tu-date"
										placeholder="when will be the next followup ?"
										value="{{ old("followUpDate") }}">
							</div>
							<div class="form-group col-sm-4 no-pad-l followup-date-wrap hide">
								<label for="followUpType">Followup Type</label>
								<select name="followUpType" id="followUpType" class="form-control text-normal">
									<option value=""
											@if(old("followUpType") == "") selected @endif>--Select FollowUp Type--
									</option>
									@foreach(config("evibe.type_followup") as $typeFollowUp)
										<option value="{{ $typeFollowUp }}"
												@if(old("followUpType") == $typeFollowUp) selected @endif>{{ $typeFollowUp }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-sm-4 no-pad-l followup-date-wrap hide">
								<label class="closureDates">Closure date</label>
								<input type="text" name="closureDate" class="form-control tu-date"
										id="closureDate" value="{{ old("closureDate") }}"
										placeholder="Expected Closure Date">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-lg-8 no-pad-l">
							<div class="form-group no-mar-b">
								<div><label for="tuComments">Comments*</label></div>
								<textarea id="tuComments" name="tuComments" cols="20" class="form-control"
										placeholder="Type your comments">{{ old('tuComments') }}</textarea>
							</div>
						</div>
						<div class="clearfix"></div>
					</form>
					@if (count($ticketUpdates) > 0)
						<ul class="ls-none no-mar status-updates-wrap">
							@foreach ($ticketUpdates as $ticketUpdate)
								@if(isset($ticketUpdate['type_reminder_group_id']))
									<li class="bg-auto-update">
										[ {{ AppUtil::formatDateTimeForBooking(strtotime($ticketUpdate['created_at'])) }} ] ==> Auto Followup reminder
										<b>RR{{ $ticketUpdate['type_reminder_group_id'] }}</b> has been sent.
										@if ($ticketUpdate['comments'])
											<span>with <b>comments:</b> {{ $ticketUpdate['comments'] }}</span>
										@endif
									</li>
								@else
									<li @if(!isset($handlers[$ticketUpdate["handler_id"]])) class="bg-auto-update" @endif>
										[ {{ AppUtil::formatDateTimeForBooking($ticketUpdate['status_at']) }} ] ==> Status updated to
										<b>{{ $statuses[$ticketUpdate["status_id"]] }}</b> by
										@if (isset($handlers[$ticketUpdate["handler_id"]]))
											<u>{{ $handlers[$ticketUpdate["handler_id"]] }}</u>.
										@else
											<u>Auto.</u>
										@endif
										<span> Update happened through <b>{{ $ticketUpdate['type_update'] }}</b> source.</span>
										@if ($ticketUpdate['comments'])
											<span class="a-no-decoration">with <b>comments:</b> {{ $ticketUpdate['comments'] }}</span>
										@endif
									</li>
								@endif
							@endforeach
						</ul>
					@else
						<div class="text-danger text-bold">No updates were found for this ticket.</div>
					@endif
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="mar-t-10" id="followup-section">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="sec-sub-title pull-left">Ticket Followups</h4>
					<div class="pull-right">
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-body ticket-followup-body">
					<ul class="nav nav-tabs" id="followup-tab">
						<li class="active mar-r-10"><a data-target="#active" data-toggle="tab">Active</a>
						</li>
						<li><a data-target="#archive" data-toggle="tab">Archive</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="active">
							@if($errors->followupError->first())
								<div class="alert-danger pad-8 mar-t-10 mar-b-6">
									<i class="glyphicon glyphicon-info-sign"></i>
									<a href="#" class="close" data-dismiss="alert"
											aria-label="close">&times;</a>
									{{$errors->followupError->first()}}
								</div>
							@endif
							@if(session()->has('followupSuccess'))
								<div class="alert-success pad-8 mar-t-10 mar-b-6">
									<i class="glyphicon glyphicon-ok-sign"></i>
									<a href="#" class="close" data-dismiss="alert"
											aria-label="close">&times;</a>
									{{ session('followupSuccess') }}
								</div>
							@endif
							<ul class="list-unstyled mar-t-20">
								@if($ticket->activeFollowup)
									<li class="followup-details-wrap"
											style="@if($ticket->activeFollowup->followup_type =="recommendation") background-color: #98d6c2 @endif">
										<div class="pull-left col-lg-10">
											@if($ticket->activeFollowup->followup_type =="recommendation") Auto @endif Follow-up
											<b>set to </b> {{Apputil::formatDateTimeForBooking($ticket->activeFollowup->followup_date)}} by
											<u>@if($ticket->activeFollowup->handler) {{$ticket->activeFollowup->handler->name}} @else -- @endif</u>
											@if($ticket->activeFollowup->comments && $ticket->activeFollowup->followup_type !=="recommendation")
												with <b>comments: </b> {!! $ticket->activeFollowup->comments !!}
											@endif
											[{!! $ticket->activeFollowup->followup_type !!}]
											<div class="pad-t-10 font-12 small-label">
												<i>Followup created at {{Apputil::formatDateTimeForBooking(strtotime($ticket->activeFollowup->created_at))}}</i>
											</div>
										</div>
										<div class="pull-right">
											@if(AppUtil::isTabVisible('ticket_followup::edit'))
												<button class="mar-r-10 btn btn-followup-done"
														data-url="{{route('ticket.followup.done',$ticket->activeFollowup->id)}}">
													<i class="glyphicon glyphicon-ok "></i>
												</button>
											@endif
										</div>
										<div class="clearfix"></div>
									</li>
								@else
									<div class="alert alert-danger">
										No followup has been set fot this ticket.
									</div>
								@endif
							</ul>
							<input type="text" class="hide" id="followupUrl"
									value="{{route('ticket.followup.save.popup',$ticket->id)}}">
						</div>

						<div class="tab-pane" id="archive">
							<ul class="list-unstyled mar-t-10">
								@if($ticket->archiveFollowups->count())
									@foreach($ticket->archiveFollowups as $followup)
										<li class="followup-details-wrap"
												style="@if($followup->followup_type =="recommendation") background-color: #98d6c2 @endif">
											<div class="pull-left col-lg-10">
												@if($followup->followup_type =="recommendation") Auto @endif Follow-up marked
												<b>completed by </b><u>@if($followup->handler){{$followup->handler->name}}@else Auto @endif</u>
												at {{Apputil::formatDateTimeForBooking(strtotime($followup->updated_at))}}
												@if($followup->done_comment && $followup->followup_type !=="recommendation")
													with <b> comments: </b> {!! $followup->done_comment !!}
												@endif
												. Follow up was set
												{{Apputil::formatDateTimeForBooking($followup->followup_date)}}
											</div>
											<div class="clearfix"></div>
										</li>
									@endforeach
								@else
									<div class="alert alert-danger">
										No followup has been marked as done, please mark a followup as done from
										<b>Active </b> tab see in this list.
									</div>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('tickets.details.update.modals')
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/base.js");
			$.getScript("/js/tickets/update.js");
		});
	</script>
@endsection