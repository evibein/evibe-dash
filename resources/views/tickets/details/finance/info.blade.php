@extends('tickets.details.base.base')

@section('section-info-message')
	Total Ticket Value + Booking wise price split up + Detailed booking price components.,
@endsection

@section('ticket-details-content')
	@if($ticket->bookings->count())
		<ul class="sub-heading list-unstyled list-inline text-center">
			@foreach($ticket->bookings as $booking)
				<li><a href="#{{ $booking->id }}"># {{ $booking->booking_id }}</a></li>
			@endforeach
		</ul>
		<div class="bookings-info-wrap mar-t-10" id="bookingSection">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<div>
					<ul class="ls-none mar-t-15">
						@foreach ($ticket->bookings as $ticketBooking)
							<?php $mappedValues = $ticketBooking->mapping ? $ticketBooking->mapping->getMappedValues() : null; ?>
							@if($mappedValues)
								<li class="ticket-booking-wrap pad-t-3" id="{{ $ticketBooking->id }}">
									<div>
										<div class="col-lg-12 no-pad-l mar-b-10">
											{{strtoupper($mappedValues['name'])}} @if($ticketBooking->booking_type_details) ({!! $ticketBooking->booking_type_details !!})@endif
											<div class="pull-right">
												<div class="mar-b-10 in-blk" data-id="{{ $ticketBooking->id }}">
												<!-- @todo: cta -->
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 booking-details">
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>Booking Id: </label>
												<div> {{ $ticketBooking->booking_id }}</div>
											</div>
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>Party Date </label>
												<div> {{date("d M Y",$ticketBooking->party_date_time) }}</div>
											</div>
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>Start Time</label>
												<div> {{date("h:i A",$ticketBooking->party_date_time) }}</div>
											</div>
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>Booking Status</label>
												<div>
													@if(!$ticketBooking->cancelled_at)
														<span class="text-green">Alive</span>
													@else
														<span class="text-red">Cancelled</span>
													@endif
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 booking-details mar-t-10">
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>Booking Amount </label>
												<div>
													<span class="rupee-font">&#8377;</span>
													<span>{{ AppUtil::formatPrice($ticketBooking->booking_amount) }}</span>
												</div>
											</div>
											<div class="col-lg-3 col-sm-3 col-md-3">
												@if($ticketBooking->is_advance_paid)
													<div class="adv-paid">
														<label>Advance Paid</label>
														<div>
															<span class="rupee-font">&#8377;</span>
															<span>{{ AppUtil::formatPrice($ticketBooking->advance_amount) }}</span>
														</div>
													</div>
												@else
													<div class="adv-unpaid">
														<label>Advance to be Paid</label>
														<div>
															<span class="rupee-font">&#8377;</span>
															<span>{{ AppUtil::formatPrice($ticketBooking->advance_amount) }}</span>
														</div>
													</div>
												@endif
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3">
												<label>Balance Amount</label>
												<div>
													<span class="rupee-font">&#8377;</span>
													<span>
												@if ($ticketBooking->is_advance_paid){{ AppUtil::formatPrice($ticketBooking->booking_amount - $ticketBooking->advance_amount) }}
														@else{{ AppUtil::formatPrice($ticketBooking->booking_amount) }}
														@endif
											</span>
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3">
												<label>Payment Reference </label>
												<div>
													@if ($ticketBooking->payment_reference)
														{{ $ticketBooking->payment_reference }}
													@else
														---
													@endif
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 booking-details mar-t-10">
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>Evibe Service Fee</label>
												<div>
													@if($ticketBooking->evibe_service_fee)
														<span class="rupee-font">&#8377;</span>
														{{ AppUtil::formatPrice($ticketBooking->evibe_service_fee) }}
														<span>
														@else
																---
															@endif
													</span>
												</div>
											</div>
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>CGST</label>
												<div>
													@if($ticketBooking->cgst_amount)
														<span class="rupee-font">&#8377;</span>
														{{ AppUtil::formatPrice($ticketBooking->cgst_amount) }}
														<span>
														@else
																---
															@endif
													</span>
												</div>
											</div>
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>SGST</label>
												<div>
													@if($ticketBooking->sgst_amount)
														<span class="rupee-font">&#8377;</span>
														{{ AppUtil::formatPrice($ticketBooking->sgst_amount) }}
														<span>
															@else
																---
															@endif
													</span>
												</div>
											</div>
											<div class="col-lg-3 col-sm-3 col-md-3">
												<label>IGST</label>
												<div>
													@if($ticketBooking->igst_amount)
														<span class="rupee-font">&#8377;</span>
														{{ AppUtil::formatPrice($ticketBooking->igst_amount) }}
														<span>
														@else
																---
															@endif
													</span>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										@if($ticketBooking->refund_amount)
											<div class="col-lg-12 col-md-12 col-sm-12 booking-details mar-t-10">
												<div class="col-lg-3 col-sm-3 col-md-3">
													<label>Refund Amount</label>
													<div>
														@if($ticketBooking->refund_amount)
															<span class="rupee-font">&#8377;</span>
															{{ AppUtil::formatPrice($ticketBooking->refund_amount) }}
															<span>
														@else
																	---
																@endif
													</span>
													</div>
												</div>
												<div class="col-lg-3 col-sm-3 col-md-3">
													<label>Evibe Bear</label>
													<div>
														@if($ticketBooking->evibe_bear)
															<span class="rupee-font">&#8377;</span>
															{{ AppUtil::formatPrice($ticketBooking->evibe_bear) }}
															<span>
														@else
																	---
																@endif
													</span>
													</div>
												</div>
												<div class="col-lg-3 col-sm-3 col-md-3">
													<label>Partner Bear</label>
													<div>
														@if($ticketBooking->partner_bear)
															<span class="rupee-font">&#8377;</span>
															{{ AppUtil::formatPrice($ticketBooking->partner_bear) }}
															<span>
														@else
																	---
																@endif
													</span>
													</div>
												</div>
												<div class="col-lg-3 col-sm-3-col-md-3">
													<label>Refund Type</label>
													<div>
														@if($ticketBooking->cancelled_at)
															Cancellation
														@else
															Post Party Issues
														@endif
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										@endif
										<div class="clearfix"></div>
									</div>
									<div class="booking-details mar-t-10" id="{{ $ticketBooking->id }}">
										<div class="pad-l-10 pad-r-10">
											<h4 class="mar-t-5 no-mar-b">Log</h4>
											<div class="mar-t-10 no-pad">
												@if($ticketBooking->logBookingFinance->count() > 0)
													<ul class="ls-none no-mar status-updates-wrap">
														@foreach($ticketBooking->logBookingFinance as $logFinance)
															<li>
																[ {{ AppUtil::formatDateTimeForBooking(strtotime($logFinance->created_at)) }} ] ==> Action performed by
																@if ($logFinance->handler)
																	<u>{{ $logFinance->handler->name }}</u>.
																@else
																	<u>Auto.</u>
																@endif
																<span> Type update: <b>{{ $logFinance->type_update }}</b>.</span>
																<span> Action comments: <b>{!! $logFinance->comments !!}</b> source.</span>
																<div class="finance-log-wrap">
																	<div class="mar-t-5">
																		<div class="col-sm-3 col-md-3 col-lg-3">
																			<div>Total Booking Amount</div>
																			<div class="text-bold">
																				@if($logFinance->total_booking_amount)
																					@price($logFinance->total_booking_amount)
																				@else
																					--
																				@endif
																			</div>
																		</div>
																		<div class="col-sm-3 col-md-3 col-lg-3">
																			<div>Evibe Service Fee</div>
																			<div class="text-bold">
																				@if($logFinance->evibe_service_fee)
																					@price($logFinance->evibe_service_fee)
																				@else
																					--
																				@endif
																			</div>
																		</div>
																		<div class="col-sm-3 col-md-3 col-lg-3">
																			<div>Partner Settlement Amount</div>
																			<div class="text-bold">
																				@if($logFinance->partner_settlement_amount)
																					@price($logFinance->partner_settlement_amount)
																				@else
																					--
																				@endif
																			</div>
																		</div>
																		<div class="col-sm-3 col-md-3 col-lg-3">
																			<div>Partner Adjustment Amount</div>
																			<div class="text-bold">
																				@if($logFinance->partner_adjustment_amount)
																					@price($logFinance->partner_adjustment_amount)
																				@else
																					--
																				@endif
																			</div>
																		</div>
																		<div class="clearfix"></div>
																	</div>
																	@if($logFinance->refund_amount)
																		<div class="mar-t-5">
																			<div class="col-sm-3 col-md-3 col-lg-3">
																				<div>Refund Amount</div>
																				<div class="text-bold">
																					@if($logFinance->refund_amount)
																						@price($logFinance->refund_amount)
																					@else
																						--
																					@endif
																				</div>
																			</div>
																			<div class="col-sm-3 col-md-3 col-lg-3">
																				<div>Evibe Bear</div>
																				<div class="text-bold">
																					@if($logFinance->evibe_bear)
																						@price($logFinance->evibe_bear)
																					@else
																						--
																					@endif
																				</div>
																			</div>
																			<div class="col-sm-3 col-md-3 col-lg-3">
																				<div>Partner Bear</div>
																				<div class="text-bold">
																					@if($logFinance->partner_bear)
																						@price($logFinance->partner_bear)
																					@else
																						--
																					@endif
																				</div>
																			</div>
																			<div class="clearfix"></div>
																		</div>
																	@endif
																</div>
															</li>
														@endforeach
													</ul>
												@else
													<div class="alert alert-info pad-8 no-mar-b">
														<i class="glyphicon glyphicon-warning-sign"></i>
														No booking finance logs are found for this ticket booking.
													</div>
												@endif
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</li>
							@else
								<div class="alert alert-danger text-center" data-id="{{ $ticketBooking->id }}">
									Mapping for this
									<b>Auto-booking</b> ticket has been deleted, Please use copy ticket (or)
									<a class="btn btn-danger btn-xs btn-del-tkt-bkng">
										<i class="glyphicon glyphicon-trash"></i> Delete
									</a>
									this booking.
								</div>
							@endif
						@endforeach
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	@else
		<div class="alert alert-danger">
			<i class="glyphicon glyphicon-warning-sign"></i> No bookings done yet for this ticket. You can add a booking by finalizing a mapping / confirmed enquiry.
		</div>
	@endif
@endsection