@extends('tickets.details.base.base')

@section('section-info-message')
	Avail checks
@endsection
@section('ticket-details-content')

	<div class="panel panel-default">
		@if (AppUtil::isAdmin() || !$ticket->isSuccessful())
			<div class="panel-heading">
				<h4 class="sec-sub-title"> Create Avail Check</h4>
			</div>
			<div class="panel-body" style="width: 100%; margin: auto">
				<div class="subsec-add-mapping">
					<div class="mar-t-10">
						<input type="hidden" id="ticketId" value="{{ $ticket->id }}">
						<div class="form-group mar-r-10 col-sm-4">
							<select id="optionTypeId" name="optionTypeId" class="form-control"
									data-url="{{ route("ticket.avail-checks.get-options", $ticket->id) }}">
								<option value="-1">-- select type --</option>
								@foreach ($options as $option)
									<option value="{{ $option['optionId'] }}">{{ $option['optionValue']}}</option>
								@endforeach
								<option value="{{ config('evibe.ticket_type.planners') }}"> Custom</option>
							</select>
						</div>

						<div class="form-group mar-r-10 col-sm-6">
							<select id="availCheckOptions" name="availCheckOptions"
									class="form-control"></select>
						</div>
						<div class="clearfix"></div>

						<div class="form-group mar-r-10 in-blk hide mapping-type-options">
							<select id="mapId" name="mapId"
									class="form-control"></select>
						</div>

					</div>
					<div class="avail-check-options hide" data-url="{{ route('ticket.avail-checks.fetch-info', $ticket->id) }}">
						<div class="mar-b-10 mar-t-10">
							<div class="form-group col-sm-4">
								<div><label> Party Date</label></div>
								<input id="partyDateCheck" name="partyDateCheck" type="text" class="form-control" data-id="{{ $ticket->id }}" disabled style="width:auto;">
							</div>
							<div class="form-group mar-l-20 col-sm-4">
								<div><label for="budgetCheck"> Suggested Budget </label></div>
								<input id="budgetCheck" name="budgetCheck" type="text" class="form-control" style="width:auto;">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div>
								<label> Inclusions </label>
							</div>
							<div>
								<textarea class="editor-plugin form-control" rows="8" name="check-info" id="checkInfo" placeholder="Enter information" style="width: 100%;"></textarea>
							</div>
						</div>

						<div class="option-images">
							<div class="mar-t-10 mar-l-10">
								<label> Existing Option Images </label>
							</div>
							<div class="panel panel-default">
								<div class="existing-images">

								</div>
							</div>
						</div>

						<div class="new-option-images hide">
							<div class="mar-t-10 mar-l-10">
								<label> Uploaded Images from Links/Pc </label>
							</div>
							<div class="panel panel-default">

								<div id="linkImages" class="link-images hide">
								</div>
								<div id="uploadedImages" class="mar-l-20 mar-b-10">

								</div>
							</div>
						</div>

						<div class="panel panel-default" style="width:100%;">
							<div class="mar-l-20 mar-t-10">
								<h4 class="modal-title"> Upload New Images<span
											class="font-13 text-info"> (Choose one way to upload the images)</span>
								</h4>
							</div>
							<hr>
							<div class="check-upload-type">
								<div class="text-black text-center mar-b-10 mar-t-10 ">
									<div class="col-sm-6">
										<label class="mar-r-10"><input type="radio" name="uploadType" value="1" checked> Upload From Link</label>
									</div>
									<div class="col-sm-6">
										<label class="mar-r-10"><input type="radio" name="uploadType" value="2"> Upload From Image</label>
									</div>
									<div class="clearfix"></div>
								</div>

								<div class="form-group pad-10 link-input">
									<div class="alert alert-info no-pad">
										<div class="mar-t-10 pad-t-10">
											<div class="no-pad text-center pad-r-3 pad-l-3" style="width: auto">
												<input type="url" id="uploadImageLink" placeholder="Enter valid image url" class="form-control image-links">
											</div>
											<div class="mar-t-10 text-center pad-b-10">
												<button class="btn btn-info btn-add-upload-link">
													<i class="glyphicon glyphicon-cloud-upload"></i> Add Image Link
												</button>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group pad-10 hide pc-input">
									<div class="alert alert-info no-pad">
										<div class="form-group no-mar-b" id="imageCheck">
											<div class="gal-type-image">
												<div class="text-center pad-t-10 pad-b-10">
													<label for="files" class="btn btn-info">
														<i class="glyphicon glyphicon-cloud-upload"></i> Click To Add Images
													</label>
													<input type="file" id="files" accept="image/png, image/jpeg, image/jpg, image/JPG" class="hide" name="files[]" multiple/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group text-center">
						<div id="submitCheck" data-url="{{ route("ticket.avail-checks.create-check", $ticket->id) }}"></div>
						<button id="btnAddAvailCheck" type="submit"
								class="btn btn-primary">
							<i class="glyphicon glyphicon-plus"></i> Add AvailCheck
						</button>
					</div>
				</div>
			</div>
		@endif
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="sec-sub-title"> Availability Checks</h4>
		</div>
		<div class="panel-body">
			<div class="data-sec mar-t-10 pad-b-20">
				<div id="callUrl" data-url="{{route("ops-dashboard.avail-checks.details")}}"></div>
				@if(count($availCheckData) > 0)
					<table id="availCheckSummary" class="table table-bordered table-responsive table-hover">
						<thead class="text-info">
						<tr>
							<th>Party Date</th>
							<th>Created At</th>
							<th>Type</th>
							<th>Option</th>
							<th>Assigned Partners</th>
							<th>Budget By OPS</th>
							<th width="30%">Avail Check Info</th>
							<th width="8%">Partner Reply</th>
						</tr>
						</thead>
						<tbody>
						@php $check =""; @endphp
						@foreach($availCheckData as $check)
							@php
								$span=1;
								if(count($check['partners'])>0)
								{
									$span=count($check['partners']);
								}

							@endphp
							<tr>
								<td rowspan="{{$span}}">
									<div>
										@if(isset($check["partyDate"]))

											{{ date("d M y, g:i a", $check["partyDate"])}}
										@else
											<div class="text-white font-1">9999</div>
											---
										@endif</div>
								</td>
								<td rowspan="{{$span}}">
									<div>
										@if(isset($check["createdDate"]))

											{{ date("d M y, g:i a", strtotime($check["createdDate"]["date"]))}}
										@else
											<div class="text-white font-1">9999</div>
											---
										@endif</div>
								</td>

								<td rowspan="{{$span}}">
									<div>
										@if(isset($check['optionType']))

											{{$check['optionType']}}

										@else
											{{"optionType not found"}}

										@endif
									</div>
								</td>

								<td rowspan="{{$span}}">
									<div>
										@if(isset($check['option']))

											{{$check['option']}}

										@else

											{{"option not found"}}

										@endif
									</div>
								</td>

								@if(count($check['partners'])>0)

									<td>
										{{$check['partners'][0]['Name']}}
									</td>
									<td>
										{{$check['partners'][0]['budget']}}
									</td>
									<td>
										<div class="avail-check-info-cell">
											{{$check['partners'][0]['info']}}
										</div>
									</td>
									<td>
										@if($check['partners'][0]['isReplied'])
											<div><b>{{$check['partners'][0]['replyDecision']?'Yes':'No'}}</b></div>
											<div class="mar-t-5">{{$check['partners'][0]['replyText']}}</div>
											<div class="font-12 mar-t-5"><i>{{$check['partners'][0]['repliedAt']}}</i>
											</div>
										@else
											---
										@endif
									</td>
								@else
									<td rowspan="1">
										No Partner Assigned
									</td>
									<td rowspan="1">
										--
									</td>
									<td rowspan="1">
										--
									</td>
									<td rowspan="1">
										--
									</td>

								@endif
							</tr>
							@foreach($check['partners'] as $key => $partner)
								@if($key!=0)
									<tr>
										<td>
											{{$check['partners'][$key]['Name']}}
										</td>
										<td>
											{{$check['partners'][$key]['budget']}}
										</td>
										<td>
											<div class="avail-check-info-cell">
												{!! $check['partners'][$key]['info'] !!}
											</div>
										</td>
										<td>
											@if($check['partners'][$key]['isReplied'])
												<div><b>{{$check['partners'][$key]['replyDecision']?'Yes':'No'}}</b>
												</div>
												<div class="mar-t-5">{{$check['partners'][$key]['replyText']}}</div>
												<div class="font-12 mar-t-5">
													<i>{{$check['partners'][$key]['repliedAt']}}</i></div>
											@else
												---
											@endif
										</td>
									</tr>
								@endif
							@endforeach


						@endforeach
						</tbody>
					</table>
				@else
					<div class="alert-danger text-center text-bold pad-10">
						<i class="glyphicon glyphicon-eye-open"></i>
						No avail checks were found.
					</div>
				@endif
			</div>
		</div>
	</div>

	@if(count($availCheckImages) > 0)
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="sec-sub-title"> Uploaded Images</h4>
			</div>
			<div class="panel-body">
				<div class="data-sec mar-t-10 pad-b-20">
					@foreach($availCheckImages as $image)
						<div class="in-blk">
							<img class="gallery-item" src="{{ $image['url'] }}">
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@endif

	<div class="hide">
		<input type="hidden" id="hidTicketTypePlanner" value="{{ config('evibe.ticket_type.planners') }}">
		<input type="hidden" id="hidTicketTypeVenue" value="{{ config('evibe.ticket_type.venues') }}">
		<input type="hidden" id="hidDefaultPlannerId" value="{{ config('evibe.default_partner_id') }}">
	</div>

@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			$.getScript("/js/tickets/base.js");

			var $ticketTypePlannerId = $('#hidTicketTypePlanner').val();
			var $defaultPlannerId = $('#hidDefaultPlannerId').val();

			//options retrieval
			var $selectMapValue = $('#availCheckOptions').selectize({
				valueField: 'id',
				labelField: 'info',
				searchField: ['label', 'name', 'code', 'price'],
				render: {
					option: function (item, escape) {
						return '<div>' +
							'<div><b>' + escape(item.label) + '</b></div>' +
							'<div class="font-12">' + escape(item.name) + '</div>' +
							'<div> Rs. ' + escape(item.price) + '</div>' +
							'</div>';
					}
				}
			});

			var selectMapValue = $selectMapValue[0].selectize;
			selectMapValue.disable();

			$('#optionTypeId').selectize({
				onChange: function () {
					selectMapValue.disable();
					$("body").css("cursor", "wait");
					var optionTypeId = $('#optionTypeId').val();
					if (optionTypeId >= 1 && optionTypeId != $ticketTypePlannerId) {
						$.ajax({
							url: $('#optionTypeId').data("url") + "?optionId=" + optionTypeId,
							type: 'post',
							dataType: 'html',
							success: function (data) {
								$("body").css("cursor", "default");
								$('.mapping-type-options').empty();
								$('.mapping-type-options').append(data);

								var results = [];
								$('.map-type-options option').each(function (count) {
									results.push({
										id: $(this).data('val'),
										label: $(this).data('code'),
										name: $(this).data('name'),
										price: $(this).data('price'),
										info: $(this).data('code') + ' - ' + $(this).data('name')
									});
								});

								selectMapValue.disable();
								selectMapValue.clearOptions();
								selectMapValue.enable();
								selectMapValue.addOption(results);
								selectMapValue.refreshOptions();

								$('#btnAddMapping').removeAttr('disabled');
							},
							error: function (jqXHR, textStatus, errorThrown) {
								showNotyError('Error occurred while fetching data, Please reload the page.')
							}
						});
					} else if (optionTypeId == $ticketTypePlannerId) {
						$("body").css("cursor", "default");
						$('.mapping-type-options').empty();

						var results = [];

						results.push({
							id: $defaultPlannerId,
							label: " Select this to check the availability of custom option ",
							name: "custom Option",
							price: " -- ",
							info: 'Custom Option - Check the availability of custom option'
						});

						selectMapValue.disable();
						selectMapValue.clearOptions();
						selectMapValue.enable();
						selectMapValue.addOption(results);
						selectMapValue.refreshOptions();

					} else {
						hideLoading();
						selectMapValue.disable();
						selectMapValue.clearOptions();
					}
				}
			});

			$('#availCheckOptions').on('change', function () {

				var option = $('#availCheckOptions').val();
				var $availCheckOptionsData = [];

				$availCheckOptionsData.push({
					'optionId': option,
					'optionTypeId': $('#optionTypeId').val()
				});

				if (option > 0 && option != $ticketTypePlannerId) {
					$(".avail-check-options").removeClass("hide");
					var date = $('#basePartyTime').text();
					$('#partyDateCheck').val(date);
					$('.existing-images').empty();

					$ajaxUrl = $('.avail-check-options').data('url');

					$.ajax({
						url: $ajaxUrl,
						type: "POST",
						dataType: "JSON",
						data: {
							'availCheckOptionsData': $availCheckOptionsData
						},
						success: function (data) {
							if (data.success) {

								if (data.price) {
									$('#budgetCheck').val(data.price);
								}
								if (data.info) {
									$('#checkInfo').data('wysihtml5').editor ? $('#checkInfo').data('wysihtml5').editor.setValue(data.info) : "";
								}

								if (data.imageLinks) {
									$.each(data.imageLinks, function (key, value) {

										var imgElement = "<div id='img" + key + "' class='in-blk slide-image'>" +
											"<a href='" + value + "' target='_blank' class='booking-gallery-link'>" +
											"<img src='" + value + "' class='no-pad-r no-mar-r gallery-item img-links' style='vertical-align: text-top;'>" +
											"</a><a>" +
											"<div class='btn no-pad-l no-pad-b delete-image' id='" + key + "'>" +
											"<i class='glyphicon no-pad-l glyphicon-remove-circle'></i>" +
											"</div>	</a></div>";

										$('.existing-images').prepend(imgElement);

									});
									$('.delete-image').click(function () {
										/*if (confirm('delete the image' + '#img' + key)) {	}*/
										var id = $(this).attr('id');
										$('#img' + id).remove();
									});
								}

							} else {
								var $errorMsg = 'Some error occurred while submitting data. Kindly refresh the page and try again';
								if (data.errorMsg) {
									$errorMsg = data.errorMsg;
								}
								window.showNotyError($errorMsg);
							}
						},
						error: function (data) {
							window.showNotyError("Some error occurred, please try again");
						}
					});

				} else {
					$('#budgetCheck').val('');
					$('#checkInfo').data('wysihtml5').editor ? $('#checkInfo').data('wysihtml5').editor.setValue("") : "";
					$(".avail-check-options").addClass("hide");
				}

			});

			//image uploads

			var fileList = [];
			var fileName = [];

			var $modal = $('.check-upload-type');

			var $uploadType = $modal.find('input[name = uploadType]');

			$uploadType.click(function (event) {
				var value = $(this).val();

				// upload from link
				if (value == 1) {
					$modal.find('.pc-input').addClass('hide');
					$modal.find('.link-input').removeClass('hide');
				}

				// upload from pc
				else if (value == 2) {
					$modal.find('.link-input').addClass('hide');
					$modal.find('.pc-input').removeClass('hide');
				}
			});

			function showImageSection() {
				$('.new-option-images').removeClass('hide');
			}

			$('#uploadImageLink').on('keypress', function (e) {

				if (e.which == 13) {
					if (confirm(" add link")) {
						var link = $('#uploadImageLink').val();

						var imgElement = "<div class='in-blk remove-image slide-image'>" +
							"<a href='" + link + "' target='_blank' class='booking-gallery-link'>" +
							"<img src='" + link + "' class='no-pad-r no-mar-r gallery-item img-links slide-image' style='vertical-align: text-top;'>" +
							"</a>" +
							"<div class='btn no-pad-l no-pad-b delete-link'>" +
							"<i class='glyphicon no-pad-l glyphicon-remove-circle'></i>" +
							"</div></div>";

						$("#uploadedImages").append(imgElement);

						$('#uploadImageLink').val('');

						$(".delete-link").off('click').on('click', function () {
							$(this).parent(".remove-image").remove();
						});
						showImageSection();
					}
				}
			});

			$('.btn-add-upload-link').on('click', function () {

				var link = $('#uploadImageLink').val();

				var imgElement = "<div class='in-blk remove-image slide-image'>" +
					"<a href='" + link + "' target='_blank' class='booking-gallery-link'>" +
					"<img src='" + link + "' class='no-pad-r no-mar-r gallery-item img-links' style='vertical-align: text-top;'>" +
					"</a>" +
					"<div class='btn no-pad-l no-pad-b delete-link'>" +
					"<i class='glyphicon no-pad-l glyphicon-remove-circle'></i>" +
					"</div></div>";

				$("#uploadedImages").append(imgElement);

				$('#uploadImageLink').val('');

				$(".delete-link").off('click').on('click', function () {
					$(this).parent(".remove-image").remove();
				});
				showImageSection();

			});

			function removeFromList(ele, index) {

				for (var i = 0; i < fileList.length; i++) {
					if (fileName[i] === index) {
						$(ele).parent(".remove-image").remove();
						delete fileList[i];
						break;
					}
				}
			}

			if (window.File && window.FileList && window.FileReader) {
				$("#files").on("change", function (e) {
					var files = e.target.files,
						filesLength = files.length;

					for (var i = 0; i < filesLength; i++) {
						var f = files[i];
						fileList.push(f);
						fileName.push(f.name);

						var fileReader = new FileReader();
						fileReader.fileName = f.name;

						fileReader.onload = (function (e) {
							var file = e.target;

							var imgElement = "<div class='in-blk remove-image slide-image' style='max-height: 200px'>" +
								"<a href='" + e.target.result + "' target='_blank' class='booking-gallery-link'>" +
								"<img src='" + e.target.result + "' class='no-pad-r no-mar-r mar-l-10 gallery-item' style='vertical-align: text-top;'>" +
								"</a>" +
								"<div class='btn no-pad-l no-pad-b delete-src-image' name='" + file.fileName + "' >" +
								"<i class='glyphicon no-pad-l glyphicon-remove-circle'></i>" +
								"</div>" +
								"<div style='text-align: center;font-size: 12px;white-space: nowrap;width: 120px;overflow: hidden;text-overflow: ellipsis;'> " + file.fileName + "</div>" +
								"</div>";

							$("#uploadedImages").append(imgElement);

							$('.delete-src-image').off('click').on('click', function () {
								var name = $(this).attr('name');
								removeFromList(this, name);
							});

						});

						fileReader.readAsDataURL(f);
					}
					showImageSection();
				});

			} else {
				alert("Your browser doesn't support to File API")
			}

			//submit the avail-check
			$('#btnAddAvailCheck').click(function () {

				var $data = new FormData();
				$optionsData = [];

				$optionsData.push({
					'ticketId': $('#ticketId').val(),
					'partyDate': $('#partyDateCheck').val(),
					'optionType': $('#optionTypeId').val(),
					'optionId': $('#availCheckOptions').val(),
					'optionInfo': $('#checkInfo').val(),
					'optionBudget': $('#budgetCheck').val()
				});

				var linkImages = [];

				$(".img-links").each(function () {
					imgsrc = this.src;
					linkImages.push(imgsrc);
				});

				fileList.forEach(function (item) {
					var file = item;
					$data.append('photos[]', file, file.name);
				});

				$data.append('optionsData', JSON.stringify($optionsData));
				$data.append('uploadedImageLInks', JSON.stringify(linkImages));

				var formUrl = $("#submitCheck").data('url');

				$.ajax({
					url: formUrl,
					type: "POST",
					data: $data,
					processData: false,
					contentType: false,
					success: function (data) {
						if (data.success) {
							window.showNotySuccess("Availability check asked successfully");
							setTimeout(function () {
								window.location.reload();
							}, 2000);
						} else {
							var $errorMsg = "some error occurred while asking availability check. Kindly refresh the page and try again";
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
								if (data.imageError) {
									$.each(data.imageName, function (key, value) {
										$errorMsg = data.errorMsg;
										var img = $("div[name*='" + value + "']");
										img.parent('.remove-image').css({"opacity": "0.1", "border": "3px solid red"});

										$errorMsg = $errorMsg + " " + value;

										window.showNotyError($errorMsg);
									});
								} else {
									window.showNotyError($errorMsg);
								}
							} else {

								window.showNotyError($errorMsg);
							}
						}
					},
					error: function (data) {
						window.showNotyError("Some error occurred, please try again");
					}
				});

			});

		});

	</script>
@endsection
