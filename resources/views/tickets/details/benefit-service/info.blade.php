@extends('tickets.details.base.base')

@section('section-info-message')
	E-Invite + Thank you card + Other customer benefits.,
@endsection

@section('ticket-details-content')
	@if($ticket->e_invite)
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="in-blk">
						<h4 class="no-mar">E-Invite</h4>
					</div>
					@if($eInvite && $eInvite->invite_sent_at)
						<div class="mar-l-10 in-blk text-info text-italic">
							(Last sent at: {{ date('d M Y h:i A', strtotime($eInvite->invite_sent_at)) }})
						</div>
					@endif
				</div>
				<div class="panel-body">
					@if($eInvite)
						<div id="existingInviteSection" class="mar-b-15">
							<div class="invite-wrap">
								<div class="alert-info in-blk pad-l-10 pad-r-10 pad-t-5 pad-b-5">
									Existing Invite
								</div>
								<div class="col-lg-12 no-pad mar-t-10">
									<div class="col-lg-4 no-pad-l">
										<label>Invite Card</label>
										<div class="invite-img-wrap">
											<img class="invite-img" src="{{ $inviteUrlPath.$eInvite->url }}" alt="RSVP invite card">
										</div>
									</div>
									<div class="col-lg-4 no-pad-l">
										<label>RSVP Link</label>
										<div>{{ $eInvite->rsvp_link }}</div>
									</div>
									<div class="col-lg-4 no-pad-l">
										<div class="valign-mid">
											<a href="" class="btn btn-primary btn-sm" id="btnResendInvite" data-url="{{ route('ticket.benefit-services.e-invite.resend', $ticket->id) }}">
												<i class="glyphicon glyphicon-refresh"></i> Resend Invite
											</a>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					@endif
					<div id="newInviteSection">
						<div class="invite-wrap">
							<div class="alert-info in-blk pad-l-10 pad-r-10 pad-t-5 pad-b-5">
								New Invite
							</div>
							<div id="generateRSVPLinkSection" class="mar-t-10">
								<a href="" id="btnGenerateRSVPLink" class="btn btn-warning btn-sm" data-url="{{ route('ticket.benefit-services.e-invite.generate-rsvp', $ticket->id) }}">
									Generate RSVP Link
								</a>
							</div>
							<div id="sendInviteSection" class="hide mar-t-10">
								<form id="uploadEInviteImage">
									<input type="hidden" id="hidRSVPLink" value="">
									<div>
										<div class="in-blk"><label>RSVP Link: </label></div>
										<div class="in-blk mar-l-5">
											<div id="newRSVPLink"></div>
										</div>
									</div>
									<div class="form-group mar-t-10">
										<div class="in-blk">
											<input type="file" name="inviteImage"/>
										</div>
										<div class="in-blk">
											<button type="submit" id="btnSendInvite" class="btn btn-success btn-sm" data-url="{{ route('ticket.benefit-services.e-invite.send', $ticket->id) }}">
												<i class="glyphicon glyphicon-send"></i> Send Invite
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	@endif
	@if($ticket->e_thank_you_card)
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="no-mar">E-Thank you card</h4>
				</div>
				<div class="panel-body">
					@if($eTYC)
						<div class="col-lg-4 no-pad-l">
							<div class="invite-img-wrap">
								<img class="invite-img" src="{{ $TYCUrlPath.$eTYC->url }}" alt="Thank You Card">
							</div>
						</div>
					@else
						<div>Thank you card hasn't been created yet</div>
					@endif
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	@endif
	@if(!$ticket->e_invite && !$ticket->e_thank_you_card)
		<div class="alert alert-danger">
			<i class="glyphicon glyphicon-warning-sign"></i> Customer hasn't opted for any of the benefit services.
		</div>
	@endif
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			$('#btnGenerateRSVPLink').on('click', function (event) {
				event.preventDefault();

				$.ajax({
					url: $(this).data('url'),
					dataType: 'JSON',
					type: 'GET',
					data: {},
					success: function (data) {
						if (data.success) {
							$('#hidRSVPLink').val(data.RSVPLink);
							$('#newRSVPLink').text(data.RSVPLink);
							$('#generateRSVPLinkSection').addClass('hide');
							$('#sendInviteSection').removeClass('hide');
						}
						else {
							window.showNotyError(data.errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError(data.errorMsg);
						window.notifyTeam({
							"url": $('#uploadImagesForm').data('url'),
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

			$('#btnSendInvite').on('click', function (event) {
				event.preventDefault();

				var formData = new FormData();
				var fileUploads = $('input[type=file]')[0].files;

				$.each(fileUploads, function (i, file) {
					formData.append('images[' + i + ']', file);
				});
				formData.append('RSVPLink', $('#hidRSVPLink').val());

				$.ajax({
					url: $(this).data('url'),
					dataType: 'JSON',
					type: 'POST',
					processData: false,
					cache: false,
					contentType: false,
					data: formData,
					success: function (data) {
						if (data.success) {
							window.showNotySuccess('Invite successfully sent to customer', 1000);
						}
						else {
							window.showNotyError(data.errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError(data.errorMsg);
						window.notifyTeam({
							"url": $('#uploadImagesForm').data('url'),
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

			$('#btnResendInvite').on('click', function (event) {
				event.preventDefault();

				$.ajax({
					url: $(this).data('url'),
					dataType: 'JSON',
					type: 'POST',
					data: {},
					success: function (data) {
						if (data.success) {
							window.showNotySuccess("Invite successfully sent to customer", 1000);
						}
						else {
							window.showNotyError(data.errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError(data.errorMsg);
						window.notifyTeam({
							"url": $('#uploadImagesForm').data('url'),
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

		});
	</script>
@endsection