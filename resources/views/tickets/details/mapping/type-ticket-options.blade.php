<select id="mapId" name="mapId" class="form-control map-type-options">
	@foreach($mappingValues as $key => $mapping)
		<option id={{ $key }} name={{ $key }} data-val="{{ $key }}" data-code="{{ $mapping["code"] }}"
				data-name="{{ $mapping["name"] }}" data-price="{{ $mapping["price"] }}"></option>
	@endforeach
</select>