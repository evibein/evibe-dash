@extends('tickets.details.base.base')

@section('section-info-message')
	Ticket Mapping information + Finalize +  Ask enquiry (availability + custom Quote) from partner + Multiple custom quote etc.,
@endsection

@section('ticket-details-content')

	@if($ticket->showEnquiry->count())
		<ul class="sub-heading list-unstyled list-inline text-center">
			<li><a class="btn btn-evibe-info btn-link" href="#enquirySection">Ticket Enquiry</a></li>
			<li><a class="btn btn-evibe-info btn-link" href="#mappingSection">Ticket Mapping</a></li>
		</ul>
		@include('tickets.details.mapping.enquiry')
	@endif

	@if($availCheckMappings->count())
		@include('tickets.details.mapping.avail-check')
	@endif

	<div class="ticket-mappings-wrap mar-t-10" id="mappingSection">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<input type="hidden" id="eventName" value="{{ $eventName }}">
					@if ($ticket->mappings->count())
						<!-- @see: removed existing permission  [&& (AppUtil::isAdmin() || !$ticket->isSuccessful())] -->
						<h4 class="in-blk no-mar">Ticket Mappings</h4>
						<ul class="no-pad-l in-blk list-inline no-mar-b">
							<li>
								<a id="sendCustomQuoteBtn" class="btn btn-info btn-sm">
									<i class="glyphicon glyphicon-list"></i> Send Custom Quote
								</a>
							</li>
							<li>
								<a id="sendRecommendationEmailBtn" class="btn btn-info btn-sm">
									<i class="glyphicon glyphicon-send"></i> Send Recommendation Email
								</a>
							</li>
							@if(!AppUtil::isAdmin() && $ticket->isExchangeContactNonAdmin() )
								<li class="hide">
									<a id="exchangeContactsBtn" class="btn btn-info btn-sm"
											data-url="{{route('ticket.action.exchange_contact', $ticket->id)}}">
										<i class="glyphicon glyphicon-sort"></i> Exchange Contacts
									</a>
								</li>
							@elseif(AppUtil::isAdmin())
								<li class="hide">
									<a id="exchangeContactsBtn" class="btn btn-info btn-sm"
											data-url="{{route('ticket.action.exchange_contact',$ticket->id)}}">
										<i class="glyphicon glyphicon-sort"></i> Exchange Contacts
									</a>
								</li>
							@endif
							@if($ticket->isVenueMappings())
								<li>
									<a id="askVenueAvailability" class="btn btn-info btn-sm">
										<i class="glyphicon glyphicon-bullhorn"></i> Check Availability
										<span style="color:#9a1e1e; font-weight: 600">(Venue)</span>
									</a>
								</li>
							@endif
							@if(count($multipleQuoteMappings) > 0)
								<li>
									<a id="btnAskMultipleQuote" type="button"
											class="btn btn-warning btn-sm ">
										<i class="glyphicon glyphicon-compressed"></i> Ask Multiple Quote
									</a>
								</li>
							@endif
							<li>
								<a id="btnAvailCheck" type="button"
										class="btn btn-primary btn-sm ">
									<i class="glyphicon glyphicon-question-sign"></i> Avail Checks
								</a>
							</li>
						</ul>
					@else
						<h4 class="in-blk no-mar">Ticket Mappings</h4>
					@endif
				</div>
				@if($lastRecommendedAt || $ticket->reco_url)
					<div class="panel-heading">
						@if($ticket->reco_url)
							<a href="{{ $ticket->reco_url }}?isFromDash=true" target="_blank" type="button" class="btn btn-info btn-sm">
								<i class="glyphicon glyphicon-retweet"></i> See Recommendations Page
							</a>
						@endif
						@if($lastRecommendedAt)
							<span class="mar-l-10">Last recommended at: {{ date('d M Y j:i A', strtotime($lastRecommendedAt)) }}</span>
						@endif
					</div>
				@endif
				@if($ticket->last_reco_rated_at || $ticket->last_reco_skipped_at)
					<div class="panel-heading">
						@if($ticket->last_reco_rated_at && $ticket->last_reco_rating)
							<h5 class="in-blk no-mar">Customer rated our recommendations as : {{ $ticket->last_reco_rating }} / 5</h5>
							<h5 class="in-blk no-mar mar-l-20">Rated at : {{ date("d/m/y H:i", strtotime($ticket->last_reco_rated_at)) }}</h5>
						@elseif($ticket->last_reco_skipped_at)
							<h5 class="in-blk no-mar">Customer has skipped rating our recommendations at : {{ date("d/m/y H:i", strtotime($ticket->last_reco_skipped_at)) }}</h5>
						@endif
					</div>
				@endif
				<div class="panel-body">
					@if (AppUtil::isAdmin() || !$ticket->isSuccessful())
						<div class="subsec-add-mapping">
							@if(is_null($ticket->city_id))
								<div class="alert alert-info"><i
											class="glyphicon glyphicon-info-sign"></i> Please select a city to provide best results & mappings.
								</div>
							@else
								<div class="form-group mar-r-10 in-blk">
									<select id="mappingCityId" name="mappingCityId"
											class="form-control" @if(!is_null($ticket->city_id)) disabled @endif>
										<option value="-1">-- select city --</option>
										@foreach ($cities as $city)
											<option value="{{ $city['id'] }}"
													@if($city['id'] == $ticket->city_id) selected
													@elseif(is_null($ticket->city_id) && request('cityId') == $city['id']) selected @endif>{{ $city['name'] }}</option>
										@endforeach
									</select>
								</div>
								<input type="hidden" id="ticketId" value="{{ $ticket->id }}">
								<form action="{{ route('ticket.mapping.new', [$ticket->id]) }}"
										method="POST" class="form form-inline in-blk">
									<div class="form-group mar-r-10 in-blk">
										@php $typeTicket = ""; @endphp
										<select id="mapTypeId" name="mapTypeId" class="form-control"
												data-url="{{ route("ticket.mapping.get.options", $ticket->id) }}">
											<option value="-1">-- select type --</option>
											@foreach ($mapTypeIdsOccasion as $mapId)
												@if($typeTicket = $typeTickets->find($mapId))
													<option value="{{ $typeTicket->id }}">{{ $typeTicket->name }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="form-group mar-r-10 in-blk">
										<select id="mappingOptions" name="mappingOptions"
												class="form-control"></select>
									</div>
									<div class="form-group mar-r-10 in-blk hide mapping-type-options">
										<select id="mapId" name="mapId"
												class="form-control"></select>
									</div>
									<div class="form-group">
										<button id="btnAddMapping" type="submit"
												class="btn btn-primary btn-sm">
											<i class="glyphicon glyphicon-plus"></i> Add Mapping
										</button>
									</div>
									<div class="form-group pull-right @if(is_null($ticket->venue_date_1)) hide @endif">
										<a id="show-avl-info">Show Check Avl Info</a>
									</div>
								</form>
							@endif
						</div>
					@endif
					<div class="ticket-mapping-list">
						@if (!count($ticket->mappings))
							<div class="alert alert-danger no-mar">
								No ticket mappings were found. Use "Add Mapping" button above to add a mapping.
							</div>
						@else
							@if (count($ticket->quotes))
								<div class="mar-t-20 alert alert-info pad-4-10">
									<i class="glyphicon glyphicon-ok-circle"></i> A custom quote has been sent at
									<b>{{ $ticket->quotes->first()->created_at }}</b>
								</div>
							@endif
							<table class="table table-condensed table-striped table-bordered no-mar-b">
								<thead>
								<tr>
									<th>Type</th>
									<th>Link</th>
									<th>Recommended?</th>
									<th title="Contact exchange ?">Exchanged?</th>
									<th>Selected?</th>
									<th>Last seen at</th>
									<th>Action</th>
								</tr>
								</thead>
								@foreach ($ticket->mappings as $ticketMapping)
									<?php $mappedValues = $ticketMapping->getMappedValues(); ?>
									<tr class="@if($mappedValues['deleted']) deleted-mapping @endif">
										<td>{{ $ticketMapping->mapType ? $ticketMapping->mapType->name:"--" }}</td>
										<td>
											@if($mappedValues['deleted'])
												<span class="text-strike">{{ substr($mappedValues['code'], 0, 15) }}</span>
											@else
												<a href="{{ $mappedValues['link'] }}"
														target="_blank">{{ substr($mappedValues['code'], 0, 15) }}</a>
											@endif
										</td>
										<td>
											@if (is_null($ticketMapping->recommended_at))
												<i class="glyphicon glyphicon-remove"></i> NO
											@else
												<i class="glyphicon glyphicon-ok"></i> Yes ({{ date("d/m/y H:i", strtotime($ticketMapping->recommended_at)) }})
											@endif
										</td>
										<td>
											@if ($ticketMapping->map_type_id == config('evibe.ticket_type.services'))
												-- N/A --
											@else
												@if (is_null($ticketMapping->contact_exchanged_at))
													<i class="glyphicon glyphicon-remove"></i> NO
												@else
													<i class="glyphicon glyphicon-ok"></i> Yes ({{ date("d/m/y H:i", strtotime($ticketMapping->contact_exchanged_at)) }})
												@endif
											@endif
										</td>
										<td>
											@if(is_null($ticketMapping->recommended_at))
												-- N/A --
											@else
												@if($ticketMapping->is_selected == 1)
													<i class="glyphicon glyphicon-thumbs-up"></i> Yes
												@else
													<i class="glyphicon glyphicon-thumbs-down"></i>  No
												@endif
											@endif
										</td>
										<td>
											@if (is_null($ticketMapping->last_seen_at))
												-- N/A --
											@else
												{{ date("d/m/y H:i", strtotime($ticketMapping->last_seen_at)) }}
											@endif
										</td>
										<!--todo remove finalizeButtonCount once enquiry module launch -->
										<?php $finalizeButtonCount = 0 ?>
										<td style="width: 22%;">
											@if (is_null($ticketMapping->booking))
												@if ($ticketMapping->map_type_id != config('evibe.ticket_type.services'))
													@if($ticketMapping->map_type_id == config('evibe.ticket_type.venues'))
														@if(AppUtil::isAdmin() && !$mappedValues['deleted'])
															<a data-id="{{ $ticketMapping->id }}"
																	class="btn btn-primary btn-xs btn-sm btn-mark-booked mar-r-4" data-map-type-id="{{ $ticketMapping->map_type_id }}">
																<i class="glyphicon glyphicon-saved"></i> Finalize
															</a> <?php $finalizeButtonCount++ ?>
														@endif
														@if($ticketMapping->enquiry_created_at)
															@if($ticketMapping->enquiry_sent_at)
																@if($ticketMapping->enquiry)
																	@if($ticketMapping->enquiry->type_id == config('evibe.enquiry.avl_check'))
																		@if(is_null($ticketMapping->enquiry->reply_decision))
																			<span class="avl-label avl-warning"><i
																						class="glyphicon glyphicon-bullhorn"></i> Enquiring..</span>
																		@elseif($ticketMapping->enquiry->reply_decision == 0)
																			<span class="avl-label avl-danger pad-10"><i
																						class="glyphicon glyphicon-thumbs-down"></i> Not Available</span>
																		@elseif($ticketMapping->enquiry->reply_decision == 1)
																			<span class="avl-label avl-success pad-10"><i
																						class="glyphicon glyphicon-thumbs-up"></i> Available</span>
																		@endif
																	@elseif($ticketMapping->enquiry->type_id == config('evibe.enquiry.custom_quote') && $ticketMapping->enquiry->is_replied == 1)

																		<span class="avl-label avl-success pad-10"><i
																					class="glyphicon glyphicon-ok"></i> Replied</span>
																	@endif
																@else
																	<span class="avl-label avl-info"> Avl check initiated</span>
																@endif
															@endif
														<!--venue avl part, asked from Bd todo: should be removed if partner app launches successfully -->
														@elseif($ticketMapping->lastAvailability()  && $ticketMapping->lastAvailability()->is_available == 1)
															@if(!$mappedValues['deleted'])
																<a data-id="{{ $ticketMapping->id }}"
																		class="btn btn-primary btn-xs btn-sm btn-mark-booked mar-r-4" data-map-type-id="{{ $ticketMapping->map_type_id }}">
																	<i class="glyphicon glyphicon-saved"></i> Finalize
																</a>
																<?php $finalizeButtonCount++ ?>
															@endif
														@else
															<a data-url="{{ route('enquiry.partner.ask.details',$ticketMapping->id) }}"
																	class="btn btn-warning btn-xs btn-sm btn-check-enquiry-initiate mar-r-4" data-map-type-id="{{$ticketMapping->map_type_id}}">
																<i class="glyphicon glyphicon-export"></i> Enquire
															</a>
														@endif
														@if($ticketMapping->venue_availability_asked_at)
															@if($ticketMapping->lastAvailability())
																@if(is_null($ticketMapping->lastAvailability()->is_available))
																	<span class="avl-label avl-info"> <span
																				class="in-blk">Checking Avl</span></span>
																@elseif($ticketMapping->lastAvailability()->is_available == 0)
																	<span class="avl-label avl-danger pad-10"><i
																				class="glyphicon glyphicon-remove"></i> Unavailable</span>
																@elseif( $ticketMapping->lastAvailability()->is_available == 1)
																	<span class="avl-label avl-success"><i
																				class="glyphicon glyphicon-ok"></i> Available</span>
																@endif
															@else
																<a class="avl-label avl-danger pad-10">Avail check deleted</a>
															@endif
														@endif
													@else
														@if(!$mappedValues['deleted'])
															@if(AppUtil::isAdmin())
																<a data-id="{{ $ticketMapping->id }}"
																		class="btn btn-primary btn-xs btn-sm btn-mark-booked mar-r-4" data-map-type-id="{{ $ticketMapping->map_type_id }}">
																	<i class="glyphicon glyphicon-saved"></i> Finalize
																</a>
																<?php $finalizeButtonCount++ ?>
															@endif
															@if($ticketMapping->enquiry_created_at)
																@if($ticketMapping->enquiry)
																	@if($ticketMapping->enquiry->type_id == config('evibe.enquiry.avl_check'))
																		@if(is_null($ticketMapping->enquiry->reply_decision))
																			<span class="avl-label avl-warning"><i
																						class="glyphicon glyphicon-bullhorn"></i> Enquiring..</span>
																		@elseif($ticketMapping->enquiry->reply_decision == 0)
																			<span class="avl-label avl-danger pad-10"><i
																						class="glyphicon glyphicon-thumbs-down"></i> Not Available</span>
																		@elseif($ticketMapping->enquiry->reply_decision == 1)

																			<span class="avl-label avl-success pad-10"><i
																						class="glyphicon glyphicon-thumbs-up"></i> Available</span>
																		@endif
																	@elseif($ticketMapping->enquiry->type_id == config('evibe.enquiry.custom_quote') && $ticketMapping->enquiry->is_replied == 1)

																		<span class="avl-label avl-success pad-10"><i
																					class="glyphicon glyphicon-ok"></i> Replied</span>
																	@endif
																@else
																	<span class="avl-label avl-warning"><i
																				class="glyphicon glyphicon-bullhorn"></i> Enquiring..</span>
																@endif
															@else
																<a data-url="{{ route('enquiry.partner.ask.details',$ticketMapping->id) }}"
																		class="btn btn-warning btn-xs btn-sm btn-check-enquiry-initiate mar-r-4" data-map-type-id="{{$ticketMapping->map_type_id}}">
																	<i class="glyphicon glyphicon-export"></i> Enquire
																</a>
															@endif
														@endif
													@endif
												@endif
											<!-- todo : remove this section of finalize button -->
												@if($finalizeButtonCount == 0)
													<a data-id="{{ $ticketMapping->id }}"
															class="btn btn-primary btn-xs btn-sm btn-mark-booked mar-r-4" data-map-type-id="{{ $ticketMapping->map_type_id }}">
														<i class="glyphicon glyphicon-saved"></i> Finalize
													</a>
												@endif
												@if(!$mappedValues['deleted'])
													<a class="btn btn-default btn-xs btn-sm btn-remove-map"
															data-id="{{ $ticketMapping->id }}">
														<i class="glyphicon glyphicon-trash"></i>
													</a>
												@endif
											@else
												<i class="glyphicon glyphicon-ok"></i> Order finalized on {{ date("d/m/y H:i", strtotime($ticketMapping->booking->created_at)) }}
											@endif
										</td>
									</tr>
								@endforeach
							</table>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		@include('tickets.details.mapping.modals')
	</div>
	<input type="hidden" value="{{config('evibe.gallery.host')}}" class="gallery-url">
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/base.js");
			$.getScript("/js/tickets/mapping.js");
		});
	</script>
@endsection