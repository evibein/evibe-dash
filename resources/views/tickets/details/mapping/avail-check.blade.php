<div class="col-sm-12 col-md-12 col-lg-12 mar-b-20" id="enquirySection">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="sec-sub-title">Availability Checks</h4>
		</div>
		@if($availCheckMappings->count())
			<div class="panel-body no-pad-b">
				<table style="border: 1px solid black" class="table table-bordered">
					<thead>
					<tr>
						<th class="text-center" style="width:15%;">Type</th>
						<th class="text-center">Option</th>
						<th class="text-center">Partner</th>
						<th class="text-center">Budget</th>
						<th class="text-center">Info</th>
						<th class="text-center">Partner Reply</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($availCheckMappings as $ticketMapping)
						<?php $mappedValues = $ticketMapping->getMappedValues(); ?>
						<tr>
							<td rowspan="{{ $ticketMapping->notifications->count() + 1 }}">
								{{ $ticketMapping->mapType ? $ticketMapping->mapType->name:"--" }}
							</td>
							<td rowspan="{{ $ticketMapping->notifications->count() + 1 }}">
								@if($mappedValues['deleted'])
									<span class="text-strike">{{ substr($mappedValues['code'], 0, 15) }}</span>
								@else
									<a href="{{ $mappedValues['link'] }}"
											target="_blank">{{ substr($mappedValues['code'], 0, 15) }}</a>
								@endif
							</td>
						</tr>
						@if($ticketMapping->notifications->count())
							@foreach($ticketMapping->notifications as $notification)
								<?php $partner = $notification->getPartner(); ?>
								<tr>
									<td>
										{{ $partner->name }}
									</td>
									<td>
										{{ $notification->budget_price }}
									</td>
									<td>
										{!! $notification->text !!}
									</td>
									<td>
										@if($notification->is_replied)
											<div><b>{{ $notification->reply_decision ? 'Yes' : 'No' }}</b></div>
											<div class="mar-t-5">{!! $notification->reply_text !!}</div>
											<div class="font-12 mar-t-5"><i>{{ $notification->replied_at }}</i></div>
										@endif
									</td>
								</tr>
							@endforeach
						@endif
					@endforeach
					</tbody>
				</table>
			</div>
		@endif
	</div>
</div>