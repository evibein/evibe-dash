@extends('layout.base')

@section('title')
	<title>Create new ticket | Evibe.in Administrator</title>
@stop

@section('content')
	<div class="container page-content new-ticket-warp">
		<div class="mar-b-10">
			<a href="{{ route('ticket.list') }}" class="font-16"><< Show all tickets</a>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">

				<!-- title section begin -->
				<div class="title-sec pad-t-10">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h3 class="no-mar">Create new ticket</h3>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--  title section end -->

				<!-- new ticket form begin -->
				@if ($errors->count() > 0)
					<div class="mar-t-20 ">
						<div class="col-sm-12">
							<div class="alert alert-danger pad-10">
								<ul class="erros-list ls-none">
									<li>{{ $errors->first() }}</li>
								</ul>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
				<form action="/tickets/new" method="POST" role="form" class="mar-t-20">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="name">Name*</label>
							<input type="text" class="form-control" id="customerName" name="customerName"
									value="{{ old('customerName') }}" placeholder="Enter customer name">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="email">Email**</label>
							<input type="email" class="form-control" id="email" name="email"
									value="{{ old('email') }}" placeholder="Enter customer email">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="phone">Phone**</label>
							<input type="text" class="form-control" id="phone" name="phone"
									value="{{ old('phone') }}" placeholder="10 digit phone number">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="altEmail">Alt. Email</label>
							<input type="text" class="form-control" id="altEmail" name="altEmail"
									value="{{ old('altEmail') }}" placeholder="Enter alternate email">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="altPhone">Alt. Phone</label>
							<input type="text" class="form-control" id="altPhone" name="altPhone"
									value="{{ old('altPhone') }}" placeholder="Enter alternate email">
						</div>
					</div>
					<div class="col-sm-4 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="eventDate">Party Date*</label>
							<input type="text" class="form-control" id="eventDate" name="eventDate"
									value="{{ old('eventDate') }}" placeholder="Select party date" autocomplete="off">
						</div>
					</div>
					<div class="col-sm-3 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="budget">Budget</label>
							<input type="text" class="form-control" id="budget" name="budget"
									value="{{ old('budget') }}" placeholder="Enter budget">
						</div>
					</div>
					<div class="col-sm-3 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="city">City</label>
							<select name="city" id="city" class="form-control">
								<option value="-1" selected>-- select city --</option>
								@foreach($data['city'] as $city)
									<option value="{{ $city->id }}"
											@if(old('city') && old('city') == $city->id) selected @endif>{{ $city->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-3 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="area">
								<span class="mar-r-10">Location</span>
								<a href="#" class="btn-add-loc text-normal"
										data-redirectto="{{ route('ticket.new') }}">Add New Location</a>
							</label>
							<select name="area" id="area" class="form-control"
									data-default="@if(old('area')){{ old('area') }}@else{{ "" }}@endif"></select>
							<ul id="allAreaOptions" class="hide">
								@foreach ($data['area'] as $area)
									<li data-id="{{ $area->id }}"
											data-city_id="{{ $area->city_id }}"
											data-name="{{ $area->name }}"></li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="col-sm-4 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="occasion">Occasion Type*</label>
							<select name="occasion" id="occasion" class="form-control">
								<option value="-1">-- Select Occasion Type --</option>
								@foreach($data['occasion'] as $occasion)
									<option value="{{ $occasion->id }}"
											@if(old('occasion')==$occasion->id) selected @endif>{{ $occasion->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-4 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="landmark">Landmark</label>
							<input type="text" class="form-control" name="landmark" placeholder="enter landmark"
									value="{{ old('landmark') }}">
						</div>
					</div>
					<div class="col-sm-4 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="zip">Zip Code</label>
							<input type="text" class="form-control" name="zip" placeholder="enter zip code"
									value="{{ old('zip') }}">
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-4 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="source">Ticket Raised Through</label>
							<select name="source" id="source" class="form-control">
								<option value="-1" @if(old('source') == -1) selected @endif>-- Select Ticket Source --
								</option>
								@foreach ($data['source'] as $source)
									<option value="{{ $source->id }}"
											@if(old('source') == $source->id) selected @endif>{{ $source->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<div class="form-group">
							<label>Source Specific</label>
							<input type="text" name="sourceSpecific" class="form-control"
									placeholder="Enter specific source"
									value="{{old('sourceSpecific')}}">
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="comments">Comments</label>
							<textarea id="comments" name="comments" class="form-control vertical-resize"
									rows="4" placeholder="enter comments..."
									data-val="{{ old('comments') }}"></textarea>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-3 col-xs-12">
						<div class="form-group">
							<label for="leadStatus">Lead Status*</label>
							<select name="leadStatus" id="leadStatus" class="form-control">
								<option value="-1" @if(old('leadstatus') == -1) selected @endif>-- Select Lead Status --
								</option>
								@foreach ($data['leadStatus'] as $status)
									<option value="{{ $status->id }}"
											@if(old('leadStatus') == $status->id) selected @endif>{{ $status->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-12 col-md-12 col-lg-12 text-center">
						<div class="form-group">
							<a href="{{ route('ticket.list') }}" class="btn btn-default mar-r-10"><i
										class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
							<button type="submit" class="btn btn-danger"><i
										class="glyphicon glyphicon-plus-sign"></i> Submit
							</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</form>
				<!-- new ticket form end -->

			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/create.js");
		});
	</script>
@stop