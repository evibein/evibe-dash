<ul class="tickets-list tickets-list-new ls-none">
	@php $ids = []; $statusId = !is_null($data['statusId']) ? $data['statusId'] : 0; @endphp
	@if(!count($data['tickets']))
		<h4 class="text-danger text-center">No matching tickets!</h4>
	@else
		@foreach ($data['tickets'] as $ticket)
			@php $ids[] = $ticket['id'] @endphp
			<li class="ticket-wrap ticket-wrap-li tickets-wrap-{{ $statusId }}" data-id="{{ $ticket['id'] }}">
				<div class="top-sec-list no-mar-b">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pad-5 no-mar no-pad-b">
						<a href="{{ route('ticket.details.bookings',$ticket['id']) }}">
							<div class="col-sm-7 col-md-7 col-lg-7 pad-l-5 no-pad-r">
								<div class="sub-sec-value name-truncate">
									<i class="glyphicon glyphicon-user"></i>
									@if ($ticket['name'])
										{{ $ticket['name'] }}
									@else
										---
									@endif
								</div>
							</div>
							<div class="col-sm-5 col-md-5 col-lg-5 no-pad-l no-pad-r">
								<div class="sub-sec-value">
									<i class="glyphicon glyphicon-phone"></i>
									{{ $ticket['phone'] }}
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-7 col-md-7 col-lg-7 no-pad-r pad-l-5 font-12 pad-t-3">
								<div class="sub-sec-value">
									<i class="glyphicon glyphicon-map-marker"></i>
									@if ($ticket['area_id'])
										@foreach($data['areas'] as $area)
											@if($ticket['area_id'] == $area->id)
												{{ ucfirst($area->name) }}
											@endif
										@endforeach
									@else
										---
									@endif
								</div>
							</div>
							<div class="col-sm-5 col-md-5 col-lg-5 no-pad pad-t-3 font-12">
								<div class="sub-sec-value">
									<i class="glyphicon glyphicon-time"></i>
									@if ($ticket['event_date'])
										{{ date("d M y (D)", $ticket['event_date']) }}
									@else
										---
									@endif
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-7 col-md-7 col-lg-7 no-pad-r pad-l-5 font-12 pad-t-3">
								<div class="sub-sec-value">
									<b>{{ "@" }}</b>
									<?php $handlerCount = 0; $handlerName = ""; ?>
									@if ($data['handlerIds'] && count($data['handlerIds']) > 0)
										@foreach($data['handlerIds'] as $handler)
											@if($ticket['handler_id'] == $handler->id)
												<?php $handlerCount++; $handlerName = $handler->name ?>
												<b>{{ ucfirst($handler->name) }}</b>
											@endif
										@endforeach
									@endif
									@if($handlerCount == 0)
										<b>Auto</b>
									@endif
								</div>
							</div>
							<div class="col-sm-5 col-md-5 col-lg-5 no-pad pad-t-3 font-11">
								<div class="sub-sec-value">
									#{{ $ticket['enquiry_id'] }}
								</div>
							</div>
							<div class="clearfix"></div>
						</a>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-pad no-mar st-bg-{{ $ticket['status_id'] }} ticket-list-status-box text-center">
						<div class="pad-5 font-20 status-inner-short-text">
							@foreach($data['ticketStatus'] as $ticketState)
								@if($ticket['status_id'] == $ticketState->id)
									<div data-toggle="tooltip" data-placement="top" title="{{ $ticketState->name }}">
										#{{ config("evibe.short_name.$ticketState->id") }}
									</div>
								@endif
							@endforeach
						</div>
						<div>
							<a href="{{ route('ticket.copy',$ticket['id']) }}"
									class="btn btn-primary btn-xs pull-right mar-r-4"
									onclick="return confirm('Are you sure to copy this ticket?','Yes','No')"
									data-toggle="tooltip" data-placement="top" title="Copy Ticket">
								<i class="glyphicon glyphicon-import"></i>
							</a>
						</div>
						<div>
							<a class="ticket-update-mappings-info btn btn-info btn-xs"
									data-id="{{ $ticket['id'] }}"
									data-url="{{ config("evibe.host") }}tickets/update/info/{{ $ticket['id'] }}"
									data-name="Reco"
									data-handler="{{ $handlerName }}"
									data-toggle="tooltip" data-placement="top" title="See All Updates">
								<i class="glyphicon glyphicon-list-alt"></i>
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="ticket-bottom-wrap ticket-bottom-wrap-{{ $statusId }} @if($ticket['status_id'] == config("evibe.status.followup") && $ticket['active_followup']) hide @endif">
					<span class="loading-text"><i>No ticket updates found</i></span>
				</div>
				<div class="@if($ticket['status_id'] != config("evibe.status.followup") || !$ticket['active_followup']) hide @endif">
					<div class="font-11 @if($ticket['active_followup']['followup_date'] < \Carbon\Carbon::now()->timestamp) follow-up-lapsed-box @else next-follow-up-box @endif">
						@if($ticket['active_followup']['followup_date'] < \Carbon\Carbon::now()->timestamp)
							Followup:
							<span class="font-14">
											{{ date("h:i A, d M", $ticket['active_followup']['followup_date']) }}
					</span>
						@else
							Followup:
							<span class="font-14">
											{{ date("h:i A,", $ticket['active_followup']['followup_date']) }} {{ \Carbon\Carbon::createFromTimestamp($ticket['active_followup']['followup_date'])->diffForHumans()  }}
					</span>
						@endif
					</div>
				</div>
			</li>
		@endforeach
	@endif
	<input id="ticketInfo{{ $statusId }}" type="hidden" data-ids="{{ json_encode($ids) }}"
			data-url="{{ route('ticket.loadExtraData') }}">
</ul>
<div class="pad-t-10 text-center">
	@if(!request('query'))

	@endif
</div>
