<!---Model for status update in ticket list page-->
<div id="modalStatusUpdate" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header mar-t-10">
				<h4 class="modal-title">Update the ticket status</h4>
			</div>
			<div class="modal-body">
				<div class="alert-danger pad-10 error-message hide"></div>
				<form id="modalStatusUpdateForm">
					<div class="form-group">
						<div><label for="tuDate">Update Date*</label></div>
						<input id="tuDate" name="tuDate" type="text"
								class="form-control tu-date"
								placeholder="When did this update happen?"
								value="{{ date("Y/m/d H:i") }}">
					</div>
					<div class="form-group">
						<div><label for="tuStatus">Status*</label></div>
						<select id="tuStatus" name="tuStatus"
								class="form-control ticket-statuses-through-ajax"></select>
					</div>
					<div class="form-group">
						<div><label for="tuType">Through*</label></div>
						<select id="tuType" name="tuType" class="form-control">
							<option value="">--Select the source--</option>
							<option value="phone">Phone</option>
							<option value="email">Email</option>
							<option value="whatsapp">WhatsApp</option>
							<option value="other">Other</option>
						</select>
					</div>
					<div>
						<div class="form-group no-mar-b related_ticket_wrap hide">
							<label for="relatedTicket">Choose Related Ticket ID*</label>
							<select id="relatedTicket" name="relatedTicket" cols="20"
									class="form-control related-ticket-data-through-ajax">
							</select>
						</div>
					</div>
					<div class="form-group followup-date_wrap no-response-date-wrap hide">
						<label for="followUpDateFromModal">Follow Up date*</label>
						<input id="followUpDateFromModal" name="followUpDateFromModal" type="text"
								class="form-control" placeholder="Follow up date" value="">
					</div>
					<div class="form-group followup-date_wrap hide">
						<label for="followupComment">Followup Type</label>
						<select name="followupComment" id="followupComment" class="form-control text-normal">
							<option selected value="">--Select FollowUp Type--</option>
							@foreach(config("evibe.type_followup") as $typeFollowUp)
								<option value="{{ $typeFollowUp }}">{{ $typeFollowUp }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group followup-date_wrap hide">
						<label class="updateClosureDate">Closure date</label>
						<input type="text" name="updateClosureDate" class="form-control" id="updateClosureDate"
								value="" placeholder="Expected Closure Date">
					</div>
					<div class="form-group no-mar-b pad-t-5">
						<div><label for="tuComments">Comments*</label></div>
						<textarea id="tuComments" name="tuComments" cols="20"
								class="form-control" placeholder="Mention details of the update"></textarea>
					</div>
				</form>
				<div class="clearfix"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">
					<i class="glyphicon glyphicon-remove"></i> Cancel
				</button>
				<button type="button" class="btn btn-success ticket-update-modal-submit" id="btnUpdateStatusAccept">
					<i class="glyphicon glyphicon-send"></i> Update Status
				</button>
				<button type="button" class="btn btn-success ticket-update-modal-submitting hide">
					<i class="glyphicon glyphicon-cloud-upload"></i> Updating ....
				</button>
			</div>
		</div>
	</div>
</div>