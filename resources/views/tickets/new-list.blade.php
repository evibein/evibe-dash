@extends('layout.base')

@section('content')
	<div class="">
		<div class="ticket-list-filters">
			<form id="searchForm" role="form" class="form form-horizontal in-blk pad-t-10 pad-l-3">
				<div class="in-blk mar-r-4 valign-mid">
					<input id="searchQuery" type="text" name="query"
							class="form-control ticket-list-seach-box"
							placeholder="Search tickets by name, email, phone"
							value="{{ request('query') }}"/>
				</div>
				<div class="in-blk valign-mid">
					<input type="submit" class="btn btn-info btn-xs" value="Search">
				</div>
			</form>
			<div class="in-blk filter-date-top-bar pad-t-10 pad-l-10">
				<ul class="filter-group-options ls-none no-mar no-pad">
					<li class="in-blk text-center">
						<input type="text" class="form-control filter-date filter-pd-start"
								value="{{ request('pds') }}" placeholder="Party start date">
					</li>
					<li class="in-blk text-center">
						<input type="text" class="form-control filter-date filter-pd-end"
								value="{{ request('pde') }}" placeholder="Party end date">
					</li>
					<li class="in-blk">
						<a class="btn btn-primary btn-filter-by-pd btn-xs">
							<i class="glyphicon glyphicon-chevron-right"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class=" filter-status-top-bar pad-t-10 in-blk pad-l-10">
				<ul class="filter-group-options ls-none no-mar no-pad">
					<li class="in-blk text-center">
						<select class="filter-group-options ls-none no-mar no-pad form-control ticket-list-status-filter font-12">
							<option class="filter-option st-7"
									@if(request('status') == 'all') selected @endif value="all"> -- Select Status --
							</option>
							<option class="filter-option st-7"
									@if(request('status') == 'followup') selected
									@endif value="followup">Follow Up
							</option>
							<option class="filter-option st-1"
									@if(request('status') == 'initiated') selected
									@endif value="initiated">Initiated
							</option>
							<option class="filter-option st-2"
									@if(request('status') == 'in-progress') selected
									@endif value="in-progress">In Progress
							</option>
							<option class="filter-option st-3"
									@if(request('status') == 'confirmed') selected
									@endif value="confirmed">Confirmed
							</option>
							<option class="filter-option st-4"
									@if(request('status') == 'booked') selected
									@endif value="booked">Booked
							</option>
							<option class="filter-option st-5"
									@if(request('status') == 'cancelled') selected
									@endif value="cancelled">Cancelled
							</option>
							<option class="filter-option st-6"
									@if(request('status') == 'just-enquiry') selected
									@endif value="just-enquiry">Just Enquiry
							</option>
							<option class="filter-option st-8"
									@if(request('status') == 'auto-pay') selected
									@endif value="auto-pay">Auto Pay
							</option>
							<option class="filter-option st-9"
									@if(request('status') == 'auto-cancel') selected
									@endif value="auto-cancel">Auto Cancel
							</option>
							<option class="filter-option st-12"
									@if(request('status') == 'service-auto-pay') selected
									@endif value="service-auto-pay">Service Auto Pay
							</option>
							<option class="filter-option st-13"
									@if(request('status') == 'not-interested') selected
									@endif value="not-interested">Not Interested
							</option>
							<option class="filter-option st-13"
									@if(request('status') == 'duplicate') selected
									@endif value="duplicate">Duplicate
							</option>
						</select>
					</li>
				</ul>
			</div>
			@if ($data['filters']['reset'])
				<a id="resetAllFilters" class="btn btn-warning btn-xs in-blk">Reset all filters</a>
			@endif
			<div class="add-ticket-btn mar-t-10 in-blk pad-r-10 pull-right">
				<a href="{{route('ticket.new')}}" class="btn btn-primary in-blk valign-mid">
					<i class="glyphicon glyphicon-plus"></i> Add New Ticket
				</a>
			</div>
		</div>
	</div>
	<div class="ticket-list-details">
		<div class="mar-t-10 ticket-list-details-wrap">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pad-r-10 pad-l-10 load-hot-ticket-data"
					data-url="{{ route("ticket.status.list", [config("evibe.lead_status.hot")]) }}">
				<div class="load-hot-ticket-data-title text-center pad-5"><b>HOT</b></div>
				<div class="load-hot-ticket-data-body">
					<div class="load-hot-ticket-data-body-wrap">
						<div class="loader">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 no-pad-r no-pad-l load-medium-ticket-data"
					data-url="{{ route("ticket.status.list", [config("evibe.lead_status.medium")]) }}">
				<div class="load-medium-ticket-data-title text-center pad-5"><b>MEDIUM</b></div>
				<div class="load-medium-ticket-data-body">
					<div class="load-medium-ticket-data-body-wrap">
						<div class="loader">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pad-r-10 pad-l-10 load-unmapped-ticket-data"
					data-url="{{ route("ticket.status.list", [config("evibe.lead_status.unmapped")]) }}">
				<div class="load-unmapped-ticket-data-title text-center pad-5"><b>UN CATEGORISED</b></div>
				<div class="load-unmapped-ticket-data-body">
					<div class="load-unmapped-ticket-data-body-wrap">
						<div class="loader">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pad-r-10 no-pad-l load-cold-ticket-data"
					data-url="{{ route("ticket.status.list", [config("evibe.lead_status.cold")]) }}">
				<div class="load-cold-ticket-data-title text-center pad-5"><b>COLD</b></div>
				<div class="load-cold-ticket-data-body">
					<div class="load-cold-ticket-data-body-wrap">
						<div class="loader">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<input type="hidden" class="status_id_hot" value="{{ config("evibe.lead_status.hot") }}">
	<input type="hidden" class="status_id_medium" value="{{ config("evibe.lead_status.medium") }}">
	<input type="hidden" class="status_id_cold" value="{{ config("evibe.lead_status.cold") }}">
	<input type="hidden" class="status_id_unmapped" value="{{ config("evibe.lead_status.unmapped") }}">
	</div>
	<div id="modalShowTicketUpdate" class="modal  " tabindex="-1" role="dialog" data-url="">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<div class="pull-left">
						<h4 class="no-mar">Showing All Updates</h4>
					</div>
					<div class="pull-right">
						<div class="font-16">Last Handler: <b class="handler"></b></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="modal-body">
					<div class="form-group ticket-update-body">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal"><i
								class="glyphicon glyphicon-thumbs-up"></i> Got it
					</button>
				</div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$.getScript("/js/tickets/ticketList.js");
	</script>
@stop