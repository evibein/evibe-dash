@extends('tickets.create-ticket-base')

@section('content')
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="top-nav-bar-create-ticket active"><a href="#">Party Info</a></li>
					<li class="top-nav-bar-create-ticket"><a href="#">Additional Info</a></li>
					<li class="top-nav-bar-create-ticket"><a href="#">Services</a></li>
					<li class="top-nav-bar-create-ticket"><a href="#">Contact Info</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="clearfix"></div>
	<div class="create-package-content-div">
		<h3 class="text-center mar-b-20">Party Info</h3>
		<div class="col-sm-10 col-sm-offset-1 mar-t-30">
			<form action="" id="newTicketCreateForm"
					class="new-ticket-create-form">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" data-validation="required" id="customerName"
								name="customerName"
								data-validation-error-msg="Please enter the customer name"
								value="{{ old('customerName') }}" placeholder="Enter customer name">
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="city">City</label>
						<select class="form-control" id="city" name="city">
							@foreach($data['cities'] as $city)
								<option value="{{ $city->id }}">{{ $city->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="occasion">Occasion</label>
						<select class="form-control" id="occasion" name="occasion">
							@foreach($data['occasions'] as $occasion)
								<option value="{{ $occasion->id }}">{{ $occasion->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="altEmail">Budget <I>(optional)</I></label>
						<input type="text" class="form-control" id="altEmail" name="altEmail"
								value="{{ old('altEmail') }}" placeholder="Enter budget for the party">
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="eventDate">Party Date</label>
						<input type="text" class="form-control" id="eventDate" name="eventDate"
								value="{{ old('eventDate') }}" data-validation="required"
								data-validation-error-msg="Please enter party date" placeholder="Select party date"
								autocomplete="off">
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="flexEventDate">Flexible Party Date <I>(optional)</I>
							<span href="javascript:;" data-toggle="tooltip"
									title="If customer is not sure of particular date for his party, Please mention flexible end date"><i
										class="glyphicon glyphicon-question-sign"></i></span></label>
						<input type="text" class="form-control" id="flexEventDate" name="flexEventDate"
								value="{{ old('flexEventDate') }}" placeholder="Select flexible party date"
								autocomplete="off">
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="text-center mar-t-30">
					<button type="submit" class="btn btn-success btn-new-ticket-create">NEXT <i
								class="glyphicon glyphicon-arrow-right"></i></button>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/create.js");
		});
	</script>
@endsection