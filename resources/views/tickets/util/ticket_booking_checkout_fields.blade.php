@foreach($data["reqFieldsArray"] as $checkoutField)
	<div class="col-md-4 col-lg-4 col-sm-4 no-pad-l pad-b-10">
		<label for="{{ $checkoutField["id"] }}">{{ $checkoutField["identifier"] }}</label><br>
		<input type="text" class="form-control" id="{{ $checkoutField["id"] }}" name="{{ $checkoutField["id"] }}"
				value="@if($checkoutField["value"]){{ $checkoutField["value"] }}@endif">
	</div>
@endforeach