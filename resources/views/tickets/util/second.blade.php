<h3 class="text-center mar-b-20">Additional Party Info</h3>
<h6 class="text-center mar-b-20"><I>(*Additional party info based on the occasion)</I></h6>
<div class="col-sm-10 col-sm-offset-1 mar-t-30">
	<form action="" id="newTicketCreateForm"
			class="new-ticket-create-form">
		<div class="col-xs-12 col-sm-4">
			<div class="form-group">
				<label for="name">Gender</label>
				<input type="text" class="form-control" data-validation="required" id="customerName"
						name="customerName"
						data-validation-error-msg="Please enter the customer name"
						value="{{ old('customerName') }}" placeholder="Enter customer name">
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="form-group">
				<label for="city">Birthday Celebrator Name</label>
				<input type="text" class="form-control" data-validation="required" id="customerName"
						name="customerName"
						data-validation-error-msg="Please enter the customer name"
						value="{{ old('customerName') }}" placeholder="Enter customer name">
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="text-center mar-t-30">
			<button type="submit" class="btn btn-success btn-new-ticket-create">NEXT <i
						class="glyphicon glyphicon-arrow-right"></i></button>
		</div>
	</form>
</div>