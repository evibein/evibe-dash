@extends('layout.base')

@section('content')
	<div class="container page-content tickets-warp">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="title-sec pad-t-10">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h2 class="panel-title">Showing list of all tickets</h2>
						<div class="pad-t-20">
							<!-- search from begin -->
							<div class="pull-left">
								<form id="searchForm" role="form" class="form form-horizontal">
									<div class="in-blk mar-r-10 valign-mid">
										<input id="searchQuery" type="text" name="query"
												class="form-control u-seach-box"
												placeholder="Search tickets by name, email, phone"
												value="{{ request('query') }}"/>
									</div>
									<div class="in-blk valign-mid">
										<input type="submit" class="btn btn-info" value="Search">
										@if (request()->has('query') && request('query'))
											<a id="resetSearch" class="pad-l-3">Clear search</a>
										@endif
									</div>
								</form>
							</div>
							<!-- search form end -->
							<!-- sort options begin -->
							<div class="pull-right">
								<div class="mar-r-10 in-blk">
									<select name="count" id="perPage" class="form-control">
										<option value="21"
												@if(request()->has('count') && request('count') == '21') selected @endif>21
										</option>
										<option value="10"
												@if(request()->has('count') && request('count') == '10') selected @endif>10
										</option>
										<option value="20"
												@if(request()->has('count') && request('count') == '20') selected @endif>20
										</option>
										<option value="50"
												@if(request()->has('count') && request('count') == '50') selected @endif>50
										</option>
										<option value="100"
												@if(request()->has('count') && request('count') == '100') selected @endif>100
										</option>
									</select>
								</div>
								<div class="u-sort-options ticket-sort-options valign-mid">
									<select id="sortOptions" name="sort" class="form-control">
										<option value="updated-first"
												@if(request()->has('sort') && request('sort') == 'updated-first') selected @endif>Updated: latest first
										</option>
										<option value="created-first"
												@if(request()->has('sort') && request('sort') == 'created-first') selected @endif>Created: latest first
										</option>
										<option value="date-asc"
												@if(request()->has('sort') && request('sort') == 'date-asc') selected @endif>Party Date: near to far
										</option>
										<option value="booking-desc"
												@if(request()->has('sort') && request('sort') == 'booking-desc') selected @endif>Booking Amount: high to low
										</option>
										<option value="updated-last"
												@if(request()->has('sort') && request('sort') == 'updated-last') selected @endif>Updated: latest last
										</option>
										<option value="created-last"
												@if(request()->has('sort') && request('sort') == 'created-last') selected @endif>Created: latest last
										</option>
										<option value="date-desc"
												@if(request()->has('sort') && request('sort') == 'date-desc') selected @endif>Party Date: far to near
										</option>
										<option value="booking-asc"
												@if(request()->has('sort') && request('sort') == 'booking-asc') selected @endif>Booking Amount: low to high
										</option>
									</select>
								</div>
								<a href="{{route('ticket.create.initialise.details')}}" class="btn btn-primary in-blk valign-mid">
									<i class="glyphicon glyphicon-plus"></i> Add New Ticket
								</a>
							</div>
							<!-- sort options end -->
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="pad-t-30">
					<div class="col-sm-3 col-md-3 col-lg-3">
						<h4 class="no-mar in-blk">Filter options</h4>
						@if ($data['filters']['reset'])
							<a id="resetAllFilters" class="btn btn-warning pull-right">Reset all filters</a>
						@endif
						<div class="filter-group">
							<div class="filter-title">By Booking Likeliness</div>
							<div class="no-mar no-pad pad-t-5 mar-t-5 col-lg-12">
								<div class="col-lg-10 no-pad">
									<select name="bookingLikelyNesId" id="bookingLikelyNesId" class="form-control text-normal">
										<option value="0"
												@if(request()->input('bookingLikelyNes')== 0) selected @endif>-- All Options --
										</option>
										@foreach($data['bookingLikelyNesOptions'] as $key=>$value)
											<option value="{{ $key}}" @if(request()->input('bookingLikelyNes')== $key) selected @endif>{{ $value }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-lg-2">
									<button class="btn btn-info btn-icon" id="btnBookingLikelyNes">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Status</div>
							<div class="no-mar pad-t-5 mar-t-5">
								<div class="col-lg-10 no-pad">
									<select name="statusId" id="statusId" class="form-control text-normal">
										<option value="0"
												@if(request()->input('bookingLikelyNes')== 0) selected @endif>-- All Options --
										</option>
										<option value="followup" @if(request()->input('status')== "followup") selected @endif class="st-7">Follow Up</option>
										<option value="initiated" @if(request()->input('status')== "initiated") selected @endif class="st-1">Initiated</option>
										<option value="in-progress" @if(request()->input('status')== "in-progress") selected @endif class="st-2">In Progress</option>
										<option value="confirmed" @if(request()->input('status')== "confirmed") selected @endif class="st-3">Confirmed</option>
										<option value="booked" @if(request()->input('status')== "booked") selected @endif class="st-4">Booked</option>
										<option value="cancelled" @if(request()->input('status')== "cancelled") selected @endif class="st-5">Cancelled</option>
										<option value="just-enquiry" @if(request()->input('status')== "just-enquiry") selected @endif class="st-6">Just Enquiry</option>
										<option value="auto-pay" @if(request()->input('status')== "auto-pay") selected @endif class="st-8">Auto Pay</option>
										<option value="auto-cancel" @if(request()->input('status')== "auto-cancel") selected @endif class="st-9">Auto Cancel</option>
										<option value="service-auto-pay" @if(request()->input('status')== "service-auto-pay") selected @endif class="st-12">Service Auto Pay</option>
										<option value="not-interested" @if(request()->input('status')== "not-interested") selected @endif class="st-13">Not Interested</option>
										<option value="duplicate" @if(request()->input('status')== "duplicate") selected @endif class="st-12">Duplicate</option>
										<option value="no-response" @if(request()->input('status')== "no-response") selected @endif class="st-17">No Response</option>
										<option value="invalid-email" @if(request()->input('status')== "invalid-email") selected @endif class="st-18">Invalid Email</option>
										<option value="drop-off" @if(request()->input('status')== "drop-off") selected @endif class="st-19">Drop Off</option>
									</select>
								</div>
								<div class="col-lg-2">
									<button class="btn btn-info btn-icon" id="btnStatus">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Handler</div>
							<div class="no-mar no-pad pad-t-5 mar-t-5 col-lg-12">
								<div class="col-lg-10 no-pad">
									<select name="handlerId" id="handlerId"
											class="form-control text-normal">
										<option value="0"
												@if(request()->input('handler')== 0) selected @endif>-- All Options --
										</option>
										@foreach($data['handlerIds'] as $handler)
											<option value="{{ $handler["id"] }}"
													@if(request()->input('handler') == $handler["id"]) selected @endif>{{ $handler["name"] }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-lg-2">
									<button class="btn btn-info btn-icon" id="btnHandlerFilter">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Lead Status</div>
							<ul class="filter-group-options ls-none no-mar no-pad">
								<li class="filter-option">
									<label>
										<input name="leadStatus" type="radio" value="hot"
												@if(request('leadStatus') == 'hot') checked @endif />
										<span class="text-danger">Hot</span>
									</label>
								</li>
								<li class="filter-option">
									<label>
										<input name="leadStatus" type="radio" value="medium"
												@if(request('leadStatus') == 'medium') checked @endif />
										<span class="text-warning">Medium</span>
									</label>
								</li>
								<li class="filter-option">
									<label>
										<input name="leadStatus" type="radio" value="cold"
												@if(request('leadStatus') == 'cold') checked @endif />
										<span class="text-info">Cold</span>
									</label>
								</li>
							</ul>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Occasion</div>
							<div class="pad-t-5 mar-t-5">
								<div class="pull-left">
									<select name="occasionId" id="occasionId" class="form-control text-normal">
										<option value="-1">All Occasions</option>
										@foreach($data['events'] as $event)
											<option value="{{ $event->id }}" @if(request()->input('occasion')== $event->id)selected @endif>{{ $event->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="pull-right">
									<button class="btn btn-info btn-icon" id="btnOccasionFilter">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Recommended Date</div>
							<ul class="filter-group-options ls-none no-mar no-pad">
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-rd-start" value="{{ request('rds') }}">
									<div class="text-muted font-13">From</div>
								</li>
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-rd-end" value="{{ request('rde') }}">
									<div class="text-muted font-13">To</div>
								</li>
								<li class="in-blk filter-date-btn-wrap">
									<a class="btn btn-icon btn-primary btn-filter-by-rd">
										<i class="glyphicon glyphicon-chevron-right"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Party Date</div>
							<ul class="filter-group-options ls-none no-mar no-pad">
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-pd-start" value="{{ request('pds') }}">
									<div class="text-muted font-13">From</div>
								</li>
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-pd-end" value="{{ request('pde') }}">
									<div class="text-muted font-13">To</div>
								</li>
								<li class="in-blk filter-date-btn-wrap">
									<a class="btn btn-icon btn-primary btn-filter-by-pd">
										<i class="glyphicon glyphicon-chevron-right"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Updated Date</div>
							<ul class="filter-group-options ls-none no-mar no-pad">
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-ud-start"
											value="{{ request('uds') }}">
									<div class="text-muted font-13">From</div>
								</li>
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-ud-end"
											value="{{ request('ude') }}">
									<div class="text-muted font-13">To</div>
								</li>
								<li class="in-blk filter-date-btn-wrap">
									<a class="btn btn-icon btn-primary btn-filter-by-ud">
										<i class="glyphicon glyphicon-chevron-right"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Created Date</div>
							<ul class="filter-group-options ls-none no-mar no-pad">
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-creation-start-time"
											value="{{ request('cds') }}">
									<div class="text-muted font-13">From</div>
								</li>
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-creation-end-time"
											value="{{ request('cde') }}">
									<div class="text-muted font-13">To</div>
								</li>
								<li class="in-blk filter-date-btn-wrap">
									<a class="btn btn-icon btn-primary btn-filter-by-cd">
										<i class="glyphicon glyphicon-chevron-right"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Payment Done Date</div>
							<ul class="filter-group-options ls-none no-mar no-pad">
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-payment-done-start-time"
											value="{{ request('pdds') }}">
									<div class="text-muted font-13">From</div>
								</li>
								<li class="in-blk text-center">
									<input type="text" class="form-control filter-date filter-payment-done-end-time"
											value="{{ request('pdde') }}">
									<div class="text-muted font-13">To</div>
								</li>
								<li class="in-blk filter-date-btn-wrap">
									<a class="btn btn-icon btn-primary btn-filter-by-pdd">
										<i class="glyphicon glyphicon-chevron-right"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Enquiry Source</div>
							<div class="no-mar pad-t-5 mar-t-5">
								<div class="pull-left">
									<select name="enqSourceId" id="enqSourceId" class="form-control text-normal">
										<option value="0"
												@if(request()->input('enqSource')== 0) selected @endif>-- All Enq Sources --
										</option>
										@foreach($data['enquirySources'] as $enqSource)
											<option value="{{ $enqSource->id }}"
													@if(request()->input('enqSource')== $enqSource->id) selected @endif>{{ $enqSource->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="pull-right">
									<button class="btn btn-info btn-icon" id="btnEnquirySourceFilter">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="filter-group">
							<div class="filter-title">By Customer Source</div>
							<div class="no-mar pad-t-5 mar-t-5">
								<div class="pull-left">
									<select name="customerSourceId" id="customerSourceId"
											class="form-control text-normal">
										<option value="0"
												@if(request()->input('customerSource')== 0) selected @endif>-- All Sources --
										</option>
										@foreach($data['customerSources'] as $customerSource)
											<option value="{{ $customerSource->id }}"
													@if(request()->input('customerSource')== $customerSource->id) selected @endif>{{ $customerSource->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="pull-right">
									<button class="btn btn-info btn-icon" id="btnCustomerSourceFilter">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="col-sm-9 col-md-9 col-lg-9">
						<div class="tickets-list-wrap">
							@if (count($data['tickets']) == 0)
								<div class="alert alert-danger">
									<span>Sorry, no tickets were found.</span>
								</div>
							@else
								<?php $ids = [] ?>
								<ul class="tickets-list ls-none">
									@foreach ($data['tickets'] as $ticket)
										@php $ids[] = $ticket->id @endphp
										<li class="ticket-wrap" data-id="{{ $ticket->id }}">
											<div class="top-sec no-mar-b">
												@if($data['leadStatus'] && count($data['leadStatus']) > 0)
													@foreach($data['leadStatus'] as $leadState)
														@if($ticket->lead_status_id == $leadState->id)
															<div class="status-corner-tag">
																<div class="ribbon {{ $leadState->color }}">
																	<span>{{ ucfirst($leadState->name) }}</span></div>
															</div>
														@endif
													@endforeach
												@endif
												<a href="{{ route('ticket.details.bookings',$ticket->id) }}">
													<div class="col-sm-3 col-md-3 col-lg-3">
														<div class="sub-sec-title">Name</div>
														<div class="sub-sec-value">
															<i class="glyphicon glyphicon-user"></i>
															@if ($ticket->name)
																{{ $ticket->name }}
															@else
																---
															@endif
															@if ($ticket->enquiry_source_id == config('evibe.ticket_enquiry_source.vday_landing_page'))
																<br><span class="text-danger text font-14">[VDAY 2020]</span>
															@elseif($ticket->enquiry_source_id == config('evibe.ticket_enquiry_source.inspiration_page'))
																<br><span class="text-danger text font-14">[Inspiration Page]</span>
															@elseif($ticket->enquiry_source_id == config('evibe.ticket_enquiry_source.navigation_modal'))
																<br><span class="text-danger text font-14">[Navigation Modal]</span>
															@endif
														</div>
													</div>
													<div class="col-sm-3 col-md-3 col-lg-3 no-pad-l">
														<div class="sub-sec-title">Phone</div>
														<div class="sub-sec-value left width-80-imp">
															<i class="glyphicon glyphicon-phone"></i>
															@if($ticket->calling_code && ($ticket->calling_code !== "+91"))
																<span class="mar-r-2">{{ $ticket->calling_code }}</span>
															@endif
															<span>{{ $ticket->phone }}</span>
														</div>
														<span class="mar-l-5 whatsapp-button" data-name="{{$ticket->name}}" data-phone="{{$ticket->phone}}" data-id="{{$ticket->id}}">
															<img src="{{config('evibe.gallery.host')}}/img/icons/whatsapp-logo.png">
														</span>
													</div>
													<div class="col-sm-4 col-md-4 col-lg-4 no-pad-l">
														<div class="sub-sec-title">Party Date</div>
														<div class="sub-sec-value">
															<i class="glyphicon glyphicon-time"></i>
															@php $bookings = $data['ticketBookings'] ? $data['ticketBookings']->where('ticket_id', $ticket->id) : []; @endphp
															@if($bookings && count($bookings) > 1)
																@php
																	$minBookTime = $bookings->first()->party_date_time;
																	$maxBookTime = $bookings->first()->party_date_time;
																@endphp
																@foreach($bookings as $booking)
																	@if($booking->party_date_time > $maxBookTime)
																		@php $maxBookTime = $booking->party_date_time; @endphp
																	@endif
																	@if($booking->party_date_time < $minBookTime)
																		@php $minBookTime = $booking->party_date_time; @endphp
																	@endif
																@endforeach
																@if($minBookTime != 0)
																	@if($minBookTime == $maxBookTime)
																		{{ AppUtil::formatDateTimeForBooking($minBookTime) }}
																	@else
																		{{ AppUtil::formatDateTimeForBooking($minBookTime) }} -
																		<br>{{ AppUtil::formatDateTimeForBooking($maxBookTime) }}
																	@endif
																@else
																	@if($ticket->event_date && ($ticket->event_date != 0))
																		{{ AppUtil::formatDateForTicket($ticket->event_date) }}
																	@else
																		---
																	@endif
																@endif
															@elseif($bookings && count($bookings) == 1)
																@if($bookings->first()->party_date_time && $bookings->first()->party_date_time != 0)
																	{{ AppUtil::formatDateTimeForBooking($bookings->first()->party_date_time) }}
																@else
																	@if($ticket->event_date && $ticket->event_date != 0)
																		{{ AppUtil::formatDateForTicket($ticket->event_date) }}
																	@else
																		---
																	@endif
																@endif
															@else
																@if($ticket->event_date && $ticket->event_date != 0)
																	{{ AppUtil::formatDateForTicket($ticket->event_date) }}
																@else
																	---
																@endif
															@endif
														</div>
													</div>
													<div class="col-sm-2 col-md-2 col-lg-2 no-pad">
														<div class="sub-sec-title">City</div>
														<div class="sub-sec-value">
															<i class="glyphicon glyphicon-map-marker"></i>
															@if ($ticket->city_id)
																{{ $ticket->city->name}}
															@else
																---
															@endif
														</div>
													</div>
													<div class="clearfix"></div>
												</a>
											</div>
											<div class="no-mar">
												<div class="col-sm-4 col-md-4 col-lg-4 font-13 no-mar no-pad mar-t-15">
													@php $followupCount = 0; @endphp
													@foreach($data['activeFollowUps'] as $followUp)
														@if($followUp->ticket_id == $ticket->id)
															<div class="font-11 in-blk pad-t-5 @if($followUp->followup_date < \Carbon\Carbon::now()->timestamp) follow-up-lapsed-box @else next-follow-up-box @endif">
																@if($followUp->followup_date < \Carbon\Carbon::now()->timestamp)
																	Lapsed Followup:
																	<span class="font-14">{{ date("h:i A, d M", $followUp->followup_date) }}</span>
																@else
																	Next Followup:
																	<span class="font-14">{{ date("h:i A,", $followUp->followup_date) }} {{ \Carbon\Carbon::createFromTimestamp($followUp->followup_date)->diffForHumans()  }}</span>
																@endif
															</div>
															@php $followupCount++; @endphp
															@break
														@endif
													@endforeach
													@if($followupCount == 0)
														<span class="pad-l-5 font-14">#{{ $ticket->enquiry_id }}</span>
													@endif
												</div>
												<div class="col-sm-2 col-md-2 col-lg-2 no-mar no-pad font-16 mar-t-15">
													<div class="st-{{ $ticket->status_id }}">
														<i class="glyphicon {{ AppUtil::getTicketStatusIcon($ticket->status_id) }}"></i>
														@foreach($data['ticketStatus'] as $ticketState)
															@if($ticket->status_id == $ticketState->id)
																{{ $ticketState->name }}
															@endif
														@endforeach
													</div>
												</div>
												<div class="col-sm-3 col-md-3 col-lg-3 no-mar no-pad font-14 mar-t-10">
													<div class="">
														<i class="glyphicon glyphicon-gift"></i>
														@if($ticket->event_id)
															<span class="text-bold">{{$ticket->event->name}}</span>
														@else
															---
														@endif
													</div>
													<div class="pad-t-5">
														@if($ticket->booking_likeliness_id)
															<i class="glyphicon glyphicon-thumbs-up"></i>
															{{ config('evibe.bookingLikeliness.' . $ticket->booking_likeliness_id) }}
														@else
															<i class="text-warning font-13">Booking likeliness not set</i>

														@endif
													</div>
												</div>

												<div class="col-sm-3 col-md-3 col-lg-3 no-mar no-pad mar-t-15">
													<form action="{{ route('ticket.copy',$ticket->id) }}" method="post" class="in-blk">
														<button type="submit" class="btn btn-primary btn-xs"
																onclick="return confirm('Are you sure to copy this ticket?','Yes','No')">
															<i class="glyphicon glyphicon-import"></i> Copy
														</button>
													</form>
													<a class="btn btn-info btn-xs btn-ticket-update"
															data-ticket-id="{{ $ticket->id }}">
														<i class="glyphicon glyphicon-arrow-up"></i> Update
													</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="mar-t-10 ticket-bottom-wrap">
												<span class="loading-text"><i>No ticket updates found</i></span>
											</div>
										</li>
									@endforeach
									<input id="ticketInfo" type="hidden" data-ids="{{ json_encode($ids) }}"
											data-url="{{ route('ticket.loadExtraData') }}">
								</ul>
								<div class="pad-t-10 text-center">
									@if(!request('query'))
										<div>{{ $data['tickets']->appends(Input::except('page'))->links() }}</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div id="modalShowTicketUpdate" class="modal  " tabindex="-1" role="dialog" data-url="">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<div class="pull-left">
									<h4 class="no-mar">Showing All Updates</h4>
								</div>
								<div class="pull-right">
									<div class="font-16">Last Handler: <b class="handler"></b></div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="modal-body">
								<div class="form-group ticket-update-body">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-success" data-dismiss="modal"><i
											class="glyphicon glyphicon-thumbs-up"></i> Got it
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="ticketUpdateModalId" value="">
@stop

@include('tickets.list.modals')

@section("google-maps-apis")
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			function getQueryParams(a) {
				if (a == "") return {};
				var b = {};
				for (var i = 0; i < a.length; ++i) {
					var p = a[i].split('=');
					if (p.length != 2) continue;
					b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
				}
				return b;
			}

			function formUrl(cUrl, key, value, isIgnore, ignores) {
				cUrl = cUrl ? cUrl.substr(cUrl.indexOf('?') + 1).split('&') : window.location.search.substr(1).split('&');
				var cParams = getQueryParams(cUrl), queryString = "", loc = window.location;
				var url = loc.protocol + '//' + loc.host + loc.pathname;
				if (isIgnore) { // remove from current object
					if (cParams.hasOwnProperty(key)) {
						try {
							delete cParams[key];
						} catch (e) {
						}
					}
				} else cParams[key] = value;
				for (key in cParams) { // form string
					if (ignores && ignores.constructor === Array && window.indexOf.call(ignores, key) >= 0) continue;
					queryString += key + "=" + cParams[key] + "&";
				}
				if (queryString) {
					url += '?' + queryString;
					url = url.slice(0, -1); // remove trailing '&'
				}
				return url;
			}

			function refreshUrl(url) {
				window.location.href = url;
			}

			function triggerJSUpdateModal() {
				$('#tuDate, #followUpDateFromModal, #updateClosureDate').datetimepicker({
					step: 30,
					minDate: 0
				});
				$('#relatedTicket').selectize();

				$('#tuStatus').on('change', function () {
					var updatedStatus = $('#tuStatus :selected').val();
					if (updatedStatus == 15) {
						$('.followup-date_wrap').addClass('hide');
						$('.no-response-date-wrap').addClass('hide');
						$('.related_ticket_wrap').removeClass('hide');
					} else if (updatedStatus == 7) {
						$('.related_ticket_wrap').addClass('hide');
						$('.no-response-date-wrap').addClass('hide');
						$('.followup-date_wrap').removeClass('hide');
					} else if (updatedStatus == 17) {
						$('.related_ticket_wrap').addClass('hide');
						$('.followup-date_wrap').addClass('hide');
						$('.no-response-date-wrap').removeClass('hide');
					} else {
						$('.related_ticket_wrap').addClass('hide');
						$('.followup-date_wrap').addClass('hide');
						$('.no-response-date-wrap').addClass('hide');
					}

					if (updatedStatus == $('#tsDuplicate').val()) {
						$('#tuType').val('other');
						$('#tuComments').val('Another valid ticket already exists!');
					}
				});

				$('.btn-ticket-update').on('click', function (event) {
					event.preventDefault();

					$('#modalStatusUpdateForm')[0].reset();
					$('.related_ticket_wrap').addClass('hide');
					$('.followup-date_wrap').addClass('hide');
					$('.no-response-date-wrap').addClass('hide');

					var ticketId = $(this).data('ticketId');
					$('#ticketUpdateModalId').val(ticketId);
					$('#modalStatusUpdate').modal('show');
				});

				$("#followUpDateFromModal").blur(function () {
					$("#updateClosureDate").val($(this).val());
				});

				$('#btnUpdateStatusAccept').on('click', function () {

					$('.ticket-update-modal-submit').addClass('hide');
					$('.ticket-update-modal-submitting').removeClass('hide');

					var ticketUpdateDateAndTime = $('#tuDate').val();
					var ticketUpdateStatusId = $('#tuStatus').val();
					var ticketUpdateComments = $('#tuComments').val();
					var ticketUpdateType = $('#tuType').val();
					var followUpDate = $('#followUpDateFromModal').val();
					var followUpType = $('#followupComment').val();
					var closureDate = $('#updateClosureDate').val();
					var isAjaxRequest = 1;

					var ticketUpdateRelatedTicketId = '';
					if (parseInt(ticketUpdateStatusId) == 15) {
						ticketUpdateRelatedTicketId = $('#relatedTicket').val();
					}

					$.ajax({
						url: 'tickets/' + $('#ticketUpdateModalId').val() + '/status-update',
						type: 'POST',
						dataType: 'json',
						data: {
							tuDate: ticketUpdateDateAndTime,
							tuType: ticketUpdateType,
							tuStatus: ticketUpdateStatusId,
							tuComments: ticketUpdateComments,
							relatedTicket: ticketUpdateRelatedTicketId,
							isAjaxRequest: isAjaxRequest,
							followUpDate: followUpDate,
							followUpType: followUpType,
							closureDate: closureDate
						},

						success: function (data, textStatus, jqXHR) {
							if (data.success) {
								window.showNotySuccess("Ticket status updated successfully");
								location.reload();
							} else {
								$('.ticket-update-modal-submit').removeClass('hide');
								$('.ticket-update-modal-submitting').addClass('hide');

								window.showNotyError(data.error);
							}
						},
						error: function (data, textStatus, jqXHR) {
							$('.ticket-update-modal-submit').removeClass('hide');
							$('.ticket-update-modal-submitting').addClass('hide');

							window.showNotyError(data.error);
						}
					});
				});
			}

			(function initPlugins() {
				$('.filter-pd-start, .filter-pd-end,.filter-ud-start , .filter-ud-end, .filter-rd-start, .filter-rd-end').datetimepicker({ // party date
					timepicker: false,
					scrollInput: false,
					scrollMonth: false,
					closeOnDateSelect: true,
					format: "d-m-Y"
				});
				$('label:has(input[disabled])').addClass('text-strike'); // strike through disabled filters

				$('.filter-creation-start-time, .filter-creation-end-time').datetimepicker({ // created date and timings
					timepicker: true,
					scrollInput: false,
					scrollMonth: false,
					closeOnDateSelect: false,
					format: "d-m-Y H:i:s"
				});
				$('label:has(input[disabled])').addClass('text-strike'); // strike through disabled filters

				$('.filter-payment-done-start-time, .filter-payment-done-end-time').datetimepicker({ // created date and timings
					timepicker: true,
					scrollInput: false,
					scrollMonth: false,
					closeOnDateSelect: false,
					format: "d-m-Y H:i:s"
				});
				$('label:has(input[disabled])').addClass('text-strike'); // strike through disabled filters

				//show ticket update info modal
				$('#modalShowTicketUpdate').modal({
					show: false,
					keyboard: false,
					backdrop: 'static'
				});

				$('#modalStatusUpdate').modal({
					show: false,
					keyboard: false,
					backdrop: 'static'
				});

				$('#enqSourceId').selectize();
			})();

			(function filterData() {
				$('#searchForm').submit(function (event) { // search results
					event.preventDefault();
					var searchQuery = $('#searchQuery').val();
					refreshUrl(formUrl('', 'query', searchQuery, false, ['page']));
				});

				$('#resetSearch').click(function (event) { // reset search
					event.preventDefault();
					refreshUrl(formUrl('', 'query', '', true, ['page']));
				});

				$('#sortOptions').change(function (event) { // sort results
					var sortVal = $('#sortOptions').val();
					refreshUrl(formUrl('', 'sort', sortVal, false, ['page']));
				});

				$('#perPage').change(function (event) { // results per page
					var perPage = $('#perPage').val();
					refreshUrl(formUrl('', 'count', perPage, false, ['page']));
				});

				$('.filter-option input').change(function (event) { // apply filters
					var type = $(this).prop('name');
					var filterVal = $('input[name="' + type + '"]:checked').val();
					refreshUrl(formUrl('', type, filterVal, false, ['page']));
				});

				$('.filter-option input:checked').click(function () { // uncheck any checked filter
					$(this).removeAttr('checked');
					var type = $(this).prop('name');
					var filter = [type, 'page'];
					refreshUrl(formUrl('', 'status', '', true));
				});

				// filter button for recommendation date
				$('.btn-filter-by-rd').click(function (event) { // filter by party date
					event.preventDefault();
					var startDate = $('.filter-rd-start').val();
					var endDate = $('.filter-rd-end').val();
					if (!startDate && !endDate) return false;
					var url = '';
					if (startDate) url = formUrl(url, 'rds', startDate, false, ['page']);
					if (endDate) url = formUrl(url, 'rde', endDate, false, ['page']);
					refreshUrl(url);
				});

				$('.btn-filter-by-pd').click(function (event) { // filter by party date
					event.preventDefault();
					var startDate = $('.filter-pd-start').val();
					var endDate = $('.filter-pd-end').val();
					if (!startDate && !endDate) return false;
					var url = '';
					if (startDate) url = formUrl(url, 'pds', startDate, false, ['page']);
					if (endDate) url = formUrl(url, 'pde', endDate, false, ['page']);
					refreshUrl(url);
				});

				$('.btn-filter-by-cd').click(function (event) { // filter by party date
					event.preventDefault();
					var startDate = $('.filter-creation-start-time').val();
					var endDate = $('.filter-creation-end-time').val();
					if (!startDate && !endDate) return false;
					var url = '';
					if (startDate) url = formUrl(url, 'cds', startDate, false, ['page']);
					if (endDate) url = formUrl(url, 'cde', endDate, false, ['page']);
					refreshUrl(url);
				});

				$('.btn-filter-by-pdd').click(function (event) { // filter by payment done date
					event.preventDefault();
					var startDate = $('.filter-payment-done-start-time').val();
					var endDate = $('.filter-payment-done-end-time').val();
					if (!startDate && !endDate) return false;
					var url = '';
					if (startDate) url = formUrl(url, 'pdds', startDate, false, ['page']);
					if (endDate) url = formUrl(url, 'pdde', endDate, false, ['page']);
					refreshUrl(url);
				});

				$('.btn-filter-by-ud').click(function (event) { // filter by party date
					event.preventDefault();
					var startDate = $('.filter-ud-start').val();
					var endDate = $('.filter-ud-end').val();
					if (!startDate && !endDate) return false;
					var url = '';
					if (startDate) url = formUrl(url, 'uds', startDate, false, ['page']);
					if (endDate) url = formUrl(url, 'ude', endDate, false, ['page']);
					refreshUrl(url);
				});

				$('#btnHandlerFilter').click(function (event) { // filter by handler
					event.preventDefault();
					var handlerId = $('#handlerId').val();
					if (handlerId != 0) {
						refreshUrl(formUrl('', 'handler', handlerId, false, ['page']));
					}
				});

				$('#btnEnquirySourceFilter').click(function (event) { // filter by enquiry source
					event.preventDefault();
					var enqSourceId = $('#enqSourceId').val();
					if (enqSourceId != 0) {
						refreshUrl(formUrl('', 'enqSource', enqSourceId, false, ['page']));
					}
				});

				$('#btnCustomerSourceFilter').click(function (event) { // filter by enquiry source
					event.preventDefault();
					var customerSourceId = $('#customerSourceId').val();
					if (customerSourceId != 0) {
						refreshUrl(formUrl('', 'customerSource', customerSourceId, false, ['page']));
					}
				});

				$('#btnBookingLikelyNes').click(function (event) { // filter by enquiry source
					event.preventDefault();
					var bookingLikelyNesId = $('#bookingLikelyNesId').val();
					if (bookingLikelyNesId >= 0) {
						refreshUrl(formUrl('', 'bookingLikelyNes', bookingLikelyNesId, false, ['page']));
					}
				});

				$('#btnStatus').click(function (event) {
					event.preventDefault();
					var statusId = $('#statusId').val();
					refreshUrl(formUrl('', 'status', statusId, false, ['page']));
				});

				$('#btnOccasionFilter').click(function (event) {
					event.preventDefault();
					var occasionId = $('#occasionId').val();
					refreshUrl(formUrl('', 'occasion', occasionId, false, ['page']));
				});

				$('#resetAllFilters').click(function (event) { // reset all filters
					event.preventDefault();
					refreshUrl(formUrl('', '', '', true, ['status', 'leadStatus', 'occasion', 'page', 'pds', 'pde', 'cds', 'cde', 'uds', 'ude', 'pdds', 'pdde', 'handler', 'enqSource', 'bookingLikelyNes']));
				});
			})();

			(function loadTicketExtraData() {
				// code for getting the extra data
				$(window).load(function () {
					var ticketInfo = $('#ticketInfo');
					var ticketIds = ticketInfo.data('ids');
					var url = ticketInfo.data('url');

					if (ticketIds.length && url) {
						$.ajax({
							type: 'POST',
							url: url,
							dataType: 'JSON',
							data: {ticketIds: ticketIds},
							success: function (data) {
								if (data.success) {
									$('.ticket-wrap').each(function (key, val) {
										var tId = $(this).data('id');
										var ticketData = data.extraData[tId];

										if (ticketData.status) {
											var firstUpdateMarkup = "[" + ticketData.statusAt + "] ==> Status updated to <b> " + ticketData.statusName + "</b> by <u>" + ticketData.handler + "</u> " +
												"<span>Update happened through <b>" + ticketData.source + "</b></span>";
											if (ticketData.comment) {
												firstUpdateMarkup += "with <b>comments:</b> " + ticketData.comment;
											}

											firstUpdateMarkup += "<a class='ticket-update-info' data-id=" + tId + " data-url=" + ticketData.url + " data-name=" + ticketData.name + " data-handler=" + ticketData.handler + ">See All Updates</a>";
											$(this).find('.loading-text').addClass('hide');
											$(this).find('.ticket-bottom-wrap').append(firstUpdateMarkup);
										} else {
											// Show no ticket updates found if ticket updates are not there
											// $(this).find('.ticket-bottom-wrap').addClass('hide');
										}
									});

								} else {
									window.showNotyError("Some error occurred while loading the extra data of tickets");
								}
							},
							fail: function () {
								window.showNotyError("Some error occurred while loading the extra data of tickets");
							}
						});
					}
				});
			})();

			(function loadUpdateTicketStatusModal() {
				$.ajax({
					url: 'tickets/status-update/details',
					type: 'GET',
					dataType: 'json',
					success: function (data, textStatus, jqXHR) {
						var optionHtml = '';
						var relatedTicket = '';

						if (data.ticketsStatuses) {
							$.each(data.ticketsStatuses, function (key, statusOptions) {
								optionHtml = optionHtml + "<option value='" + statusOptions.id + "'>" + statusOptions.name + "</option>";
							});

							$(optionHtml).appendTo('.ticket-statuses-through-ajax');
						}

						if (data.ticketsData) {
							$.each(data.ticketsData, function (count, relatedTicketDetails) {
								relatedTicket = relatedTicket + "<option value='" + relatedTicketDetails.id + "'>"
									+ relatedTicketDetails.id + " / " + relatedTicketDetails.name + " / " + relatedTicketDetails.email
									+ " / " + relatedTicketDetails.phone + "</option>";
							});

							$(relatedTicket).appendTo('.related-ticket-data-through-ajax');
						}

						triggerJSUpdateModal();
					},
					error: function (data, textStatus, jqXHR) {
						window.showNotyError("Some Error occured while getting the data, Please reload the page.");
					}
				});
			})();

			(function btnSeeAllUpdates() {
				$('.ticket-wrap').on('click', '.ticket-update-info', function () {
					window.showLoading();
					$('#modalShowTicketUpdate').modal('hide');
					var url = $(this).data('url');
					var handler = $(this).data('handler');

					$.ajax({
						url: url,
						type: 'POST',
						dataType: 'json',
						success: function (data) {
							window.hideLoading();
							if (data.success) {
								var $modal = $('#modalShowTicketUpdate');
								$modal.find('.ticket-update-body').empty();
								$modal.find('.handler').text(handler);

								$.each(data.ticketUpdates, function (key, value) {
									var markup = '<div class="status-updates-wrap pad-t-8">[' + value['date'] + '] ==> Status updated to <b>' + value['status'] + '</b> by ' +
										'<u>' + value['handler'] + '</u>. Update happened through ' + value['source'] + ' source ';
									if (value['comments']) {
										markup += 'with <b>comments: </b>' + value['comments'];
									}
									markup += '</div>';

									$modal.find('.ticket-update-body').append(markup);
								});

								$modal.modal('show');
							}
						},
						fail: function () {
							alert('not okay');
							window.showNotyError();
						}
					})
				});
			})();

			(function addedTicketSuccessNotification() {
				// show notification, if ticket added successfully
				if (window.location.search.indexOf('success=1') > -1) {
					noty({
						text: 'New ticket has been created',
						layout: 'top',
						type: 'success',
						closeWith: ['click', 'hover'],
						timeout: 5000,
						callback: {
							onClose: function () {
								var loc = window.location.href;
								var qIndex = loc.indexOf('?');
								loc = loc.substring(0, qIndex);
								window.location = loc;
							}
						}
					});
					$('#query').focus();
				}
			})();
		});

	</script>
@stop