<div style='background-color:#F5F5F5;padding:10px;max-width:700px;'>
	<div style='padding: 20px 30px 10px 30px;background-color: #FFFFFF;'>
		<div>
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'>
						<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt=''>
					</a>
				</div>
				<div style='padding-top:10px;'>Phone: +91 9035009897</div>
			</div>
			<div style='float:right;'>
				<div style='padding-top:10px;'>
					<span>Booking Date: </span>
					<span>{{ date('d/m/y h:i A') }}</span>
				</div>
				
			</div>
			<div style='clear:both;'></div>
		</div>
		<hr>
		<div style='padding-top: 10px;'>
			<p>Dear {{ $data['customer']['name'] }},</p>

			@if ($data['bookings']['totalAdvanceToPay'] > 0)
				<p>We are pleased to inform you that your booking has been confirmed with Evibe. Please check below for more details.</p>
			@else
				<p>Your party order in under process. <b>Please pay total advance of Rs. {{ AppUtilFacade::formatPrice($data['bookings']['totalAdvanceToPay']) }} by {{ date('d/m/y', strtotime('+2 days')) }}</b> to confirm your booking. Also, reply to this email with the transaction receipt / screenshot for reference.</p>

			<div style='padding-top: 10px;'>
				<div style='color:#EF3E75;'><b>Contact Information</b></div>
				<div style='padding-top:10px;'>
					<table style='max-width:600px;border:1px solid #AAA;border-collapse:collapse;'>
						<tr>
							<td style='border:1px solid #AAA;padding:4px;'><b>Name</b></td>
							<td style='border:1px solid #AAA;padding:4px;'>{{ $data['customer']['name'] }}</td>
						</tr>
						<tr>
							<td style='border:1px solid #AAA;padding:4px;'><b>Phone</b></td>
							<td style='border:1px solid #AAA;padding:4px;'>{{ $data['customer']['phone'] }}</td>
						</tr>
						<tr>
							<td style='border:1px solid #AAA;padding:4px;'><b>Party Date</b></td>
							<td style='border:1px solid #AAA;padding:4px;'>{{ $data['additional']['partyDateTime'] }}</td>
						</tr>
					</table>
				</div>
			</div>
			<div style='padding-top:20px;'>
				<div style='color:#EF3E75;'><b>Booking Details</b></div>
				<div style='padding-top:8px;'>
					@foreach ($data['bookings']['list'] as $booking)
							<table style='max-width:600px;padding-bottom:15px;border:1px solid #AAA;border-collapse:collapse;'>
								<tr>
									<td style='border:1px solid #AAA;padding:4px;'><b>Booking Id</b></td>
									<td style='border:1px solid #AAA;padding:4px;'>{{ $booking['bookingId'] }}</td>
								</tr>
								<tr>
									<td style='border:1px solid #AAA;padding:4px;'><b>Details</b></td>
									<td style='border:1px solid #AAA;padding:4px;'>{!! $booking['bookingInfo'] !!}</td>
								</tr>
								<tr>
									<td style='border:1px solid #AAA;padding:4px;'><b>Booking Amount</b></td>
									<td style='border:1px solid #AAA;padding:4px;'>Rs. {{ AppUtilFacade::formatPrice($booking['bookingAmount']) }}</td>
								</tr>
								<tr>
									<td style='border:1px solid #AAA;padding:4px;'>
										@if ($booking['advancePaid'] == 1)
											<b>Advance Paid</b>
										@else
											<b>Advance Amount</b>
										@endif
									</td>
									<td style='border:1px solid #AAA;padding:4px;'>Rs. {{ AppUtilFacade::formatPrice($booking['advanceAmount']) }}</td>
								</tr>
						</table>
					@endforeach
				</div></div>

	@if ($data['bookings']['totalAdvanceToPay'] > 0)
		<div style='padding-top:10px;'>
			<div style='color:#EF3E75;'><b>Bank Details</b></div>
			<div style='padding-top:8px;'>
				<table style='max-width:600px;border:1px solid #AAA;border-collapse:collapse;'>
					<tr>
						<td style='border:1px solid #AAA;padding:4px;'>
							<ul>
								<li>Account Name: Evibe Technologies</li>
								<li>Account Number: 0202102000015817</li>
								<li>Bank Name: IDBI Bank</li>
								<li>Account Type: Current Account</li>
								<li>IFSC Code: IBKL0000202</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td style='border:1px solid #AAA;padding:4px;'><b>Cancellation policy: </b> No refund</td>
					</tr>
				</table>
			</div>
		</div>
	@endif
		<div style='padding-top:20px;'>
		Thanks for choosing evibe.in to book your party. We wish you a wonderful and memorable party. 
		</div>
		<div style='padding-top:10px;'>
			<div>Best Regards,</div>
			<div>Team Evibe</div>
		</div>
	</div>
	</div>
	<div style='padding:0 30px 10px 30px;'>
		<p>
			<div style='text-decoration:underline;color:#999;'><b>Note:</b></div>
			<div style='color:#999;'>
				<ul>
					<li>Incase any of your details mentioned in this email are incorrect, please 'reply-all' with correct details.</li>
					<li>You need to pay the remaining balance before your event begins.</li>
					<li>All the decoration items used are only on hire basis.</li>
					<li>Booking with us implies you accept to all our terms (<a href='http://evibe.in/terms' target='_blank'>http://evibe.in/terms</a>)</li>
				</ul>
			</div>
		</p>
	</div>
</div>
<div style='padding-top:10px;font-size:12px;color:#999'>If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>