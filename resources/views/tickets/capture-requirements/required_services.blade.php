@extends('tickets.capture-requirements.capture_requirements_base')

@section('content')
	<div id="wizard" class="form_wizard wizard_horizontal mar-t-30 mar-b-20">
		<ul class="wizard_steps anchor">
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.party.details", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="selected" isdone="1" rel="1">
					<span class="step_no">1</span>
					<span class="step_descr">Step 1<br>
						<small>Party requirements</small>
					</span>
				</a>
			</li>
			<li>
				<a class="selected" isdone="1" rel="2">
					<span class="step_no">2</span>
					<span class="step_descr"><b>Step 2</b><br>
						<small><b>Services Required</b></small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.contact.info", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="disabled" isdone="0" rel="3">
					<span class="step_no">3</span>
					<span class="step_descr">Step 3<br>
						<small>Closure</small>
					</span>
				</a>
			</li>
		</ul>
	</div>
	<hr>
	<div>
		@if($data['customEnquiry']['enquiryData'])
			<h3 class="text-center mar-t-30 mar-b-20">Required Services</h3>
			<div class="panel panel-default mar-l-10 mar-r-10">
				<h4 class="pad-l-15"><b>Customizations</b></h4>
				<div class="panel-body no-pad font-13">
					<div class="col-md-4 col-sm-4 col-lg-4">
						@if($data['customEnquiry']['enquiryData']['foodType'])
							<div>
								<b>Food Type : </b>{{ $data['customEnquiry']['enquiryData']['foodType'] }}
							</div>
						@endif
						@if($data['customEnquiry']['enquiryData']['foodServiceType'])
							<div>
								<b>Food Service Type : </b>{{ $data['customEnquiry']['enquiryData']['foodServiceType'] }}
							</div>
						@endif
						<div class="pad-b-10">
							<div class="valign-top"><b>Uploaded Images : </b></div>
							@foreach($data['customEnquiry']['uploadGallery'] as $image)
								<a href="{{ $image['imageLink'] }}" target="_blank">
									<img src="{{ $image['imageLink'] }}" width="75" height="75">
								</a>
							@endforeach
						</div>
					</div>
					<div class="col-md-8 col-sm-8 col-lg-8 pad-b-10">
						<div>
							<label for="typeTicketBookingId">Ticket booking type</label><br>
							<select class="form-control" id="typeTicketBookingId" name="typeTicketBookingId"
									data-url="{{ route("ticket.create.get.checkout.fields", [$data['ticketId']]) }}">
								<option value="0">-- Select ticket booking type --</option>
								@foreach($data['customEnquiry']['enquiryData']['bookingTypes'] as $bookingType)
									<option value="{{ $bookingType['id'] }}"
											@if((old('typeTicketBookingId') == $bookingType['id']) || (isset($data["customEnquiry"]["enquiryData"]["selectedBookingType"]["BookingTypeId"]) && ($data["customEnquiry"]["enquiryData"]["selectedBookingType"]["BookingTypeId"] == $bookingType['id']))) selected @endif>{{ $bookingType['name'] }}</option>
								@endforeach
							</select>
						</div>
						<div class="ticket-booking-checkout-fields pad-t-10">
							@if(isset($data["customEnquiry"]["enquiryData"]["selectedBookingType"]["requirementFields"]))
								@foreach($data["customEnquiry"]["enquiryData"]["selectedBookingType"]["requirementFields"] as $checkoutField)
									<div class="col-md-4 col-lg-4 col-sm-4 no-pad-l pad-b-10">
										<label for="{{ $checkoutField["id"] }}">{{ $checkoutField["identifier"] }}</label><br>
										<input type="text" class="form-control" id="{{ $checkoutField["id"] }}"
												name="{{ $checkoutField["id"] }}"
												value="@if($checkoutField["value"]){{ $checkoutField["value"] }}@endif">
									</div>
								@endforeach
							@endif
						</div>
					</div>
					<div class="clearfix"></div>
					<input type="hidden" name="ticketEventId" id="ticketEventId"
							value="{{ $data['ticket']['event_id'] }}">
				</div>
			</div>
		@endif
	</div>
	<div class="create-package-content-div hide">
		@if($data['enquiredOption'])
			<div class="panel panel-default mar-l-10 mar-r-10">
				<h4 class="pad-l-15"><b>Interested Product</b></h4>
				<div class="panel-body no-pad font-13">
					<div class="col-md-1 col-sm-1 col-lg-1 pad-b-4"><a target="_blank"
								href="{{ $data['enquiredOption']['profilePic'] }}"><img
									width="100" height="100" src="{{ $data['enquiredOption']['profilePic'] }}"></a>
					</div>
					<div class="col-md-11 col-sm-11 col-lg-11">
						<div class="col-md-6 col-sm-6 col-lg-6 no-pad-l no-pad-r">
							<b>Name</b> : {{ $data['enquiredOption']['name'] }}</div>
						<div class="col-md-6 col-sm-6 col-lg-6 no-pad-l no-pad-r">
							<div class="col-md-4 col-sm-4 col-lg-4 no-pad-l no-pad-r">
								<b>Code</b> : <a target="_blank"
										href="{{ $data['enquiredOption']['dashLink'] }}">{{ $data['enquiredOption']['code'] }}</a>
							</div>
							<div class="col-md-8 col-sm-8 col-lg-8 no-pad-l no-pad-r"><b>Price : </b>
								<strike>&#8377; {{ $data['enquiredOption']['priceWorth'] }}</strike> &#8377; {{ $data['enquiredOption']['minPrice'] }} @if($data['enquiredOption']['maxPrice'])- &#8377; {{ $data['enquiredOption']['maxPrice'] }}@endif
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-6 col-sm-6 col-lg-6 no-pad-l no-pad-r">
							<b>Price Info : </b>{{ $data['enquiredOption']['priceRangeInfo'] }}</div>

						@if(isset($data['enquiredOption']['pricePerKg']) && $data['enquiredOption']['pricePerKg'])
							<div class="col-md-2 col-sm-2 col-lg-2 no-pad-l no-pad-r">
								<b>Price Per Kg : </b>&#8377; {{ $data['enquiredOption']['pricePerKg'] }}</div>
						@endif
						@if(isset($data['enquiredOption']['pricePerExtraGuest']) && $data['enquiredOption']['pricePerExtraGuest'])
							<div class="col-md-4 col-sm-4 col-lg-4 no-pad-l no-pad-r">
								<b>Price Per Extra Guest : </b>{{ $data['enquiredOption']['pricePerExtraGuest'] }}</div>
						@endif
						<div class="clearfix"></div>
						<div class="in-blk valign-top"><b>Inclusions :</b></div>
						<div class="in-blk">{{ $data['enquiredOption']['inclusions'] }}</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		@endif
		@if(isset($data["categorizedServices"]))
			<div class="panel panel-default mar-l-10 mar-r-10 no-mar-b ticket-selected-category">
				@foreach($data["categorizedServices"] as $key => $service)
					<h4 class="pad-l-15 mar-t-30"><b>{{ $key }}</b></h4>
					<div class="panel-body no-pad mar-r-10 categorized-services-wrap">
						@php $count = 0; @endphp
						@foreach($service as $serviceTag)
							@php $count++; @endphp
							<div class="col-md-3 col-sm-3 col-xs-3 no-pad-r pad-b-4">
								<label class="text-normal">
									<div class="panel panel-default no-pad-b no-mar-b pad-10">
										<div>
											<div class="in-blk">
												<div class="services-check-box-list-wrap">
													<!-- @see: should check the condition in the next flow iteration -->
													<input type="checkbox" name="{{ $serviceTag["id"] }}" id="{{ $serviceTag["id"] }}"
															class="sub-tags-select" @if($serviceTag['id'] == config('evibe.default-category-tag.'.$serviceTag["mapTypeId"])) checked @endif>
												</div>
											</div>
											<div class="in-blk pad-l-5">
												<b>{{ $serviceTag["name"] }}</b><br>
												@if ($serviceTag["description"])
													{{ $serviceTag["description"] }}
												@endif
											</div>
											<div class="font-13 mar-l-20 in-blk">
												@if($serviceTag["minPrice"] > 0)
													<b>&#8377;{{ $serviceTag["minPrice"] }}@if($serviceTag["maxPrice"]){{ " - &#8377;" . $serviceTag["maxPrice"] }}@endif</b>
													@if ($serviceTag["optionsCount"])
														( <span class="pad-l-3">Total options: <b>{{ $serviceTag["optionsCount"] }}</b></span> )
													@endif
												@endif
											</div>
										</div>

									</div>
								</label>
							</div>
							@if($count%4 == 0)
								<div class="clearfix"></div>
							@endif
						@endforeach
						<div class="clearfix"></div>
					</div>
				@endforeach
			</div>
		@endif
		<div class="pad-10 pad-t-20">
			<label for="teamNotes">Additional Comments</label>
			<textarea id="teamNotes" name="teamNotes" class="form-control vertical-resize"
					rows="4"
					placeholder="enter comments...">@if(isset($data["teamNotes"]) && $data["teamNotes"]){{$data["teamNotes"]}}@elseif (isset($data["comments"]) && $data["comments"]) {{$data["comments"]}}@endif</textarea>
		</div>
		<input type="hidden" id="isClose" name="isClose" value="0">
		<input type="hidden" id="redirectUrl" name="redirectUrl" value="{{ request("redirect") }}">
		<div class="mar-t-30 mar-b-20">
			<div class="text-center mar-b-15">
				@php $ticketWorkflowRoleId = \Illuminate\Support\Facades\Auth::user()->role_id; @endphp
				@if($ticketWorkflowRoleId == config("evibe.roles.syrow"))
					<a href="{{ route("syrow.tickets.list") }}" class="btn btn-link mar-r-10">
						<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
					</a>
					<button type="submit" class="btn btn-success btn-lg btn-add-services" data-is-close=0 data-url="{{ route("ticket.create.save.services", [$data['ticketId']]) }}">
						NEXT <i class="glyphicon glyphicon-arrow-right"></i>
					</button>
				@else
					<a href="@if($data["isEdit"] != 1){{ route("ticket.list") }}@elseif(isset($data["ticketId"])){{ route("ticket.details.info", [$data["ticketId"]]) }}@endif"
							class="btn btn-link mar-r-10">
						<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
					</a>
					@if($data["isEdit"] == 1)
						<button type="submit"
								data-is-close=1
								data-url="@if($data["isEdit"] == 1){{ route("ticket.modify.save.services", [$data['ticketId'], 1]) }}@else{{ route("ticket.create.save.services", [$data['ticketId']]) }}@endif"
								class="btn btn-success btn-add-services btn-lg">{{ "SAVE & CLOSE " }}<i
									class="glyphicon glyphicon-remove-circle"></i></button>
						<div class="in-blk pad-10"></div>
					@endif
					<button type="submit" class="btn btn-success btn-lg btn-add-services"
							data-is-close=0
							data-url="@if($data["isEdit"] == 1){{ route("ticket.modify.save.services", [$data['ticketId'], 1]) }}@else{{ route("ticket.create.save.services", [$data['ticketId']]) }}@endif">@if($data["isEdit"] == 1){{ "SAVE & NEXT" }}@else{{ "NEXT" }}@endif
						<i class="glyphicon glyphicon-arrow-right"></i></button>
				@endif
			</div>
		</div>
	</div>
	<div class="ticket-workflow-show-selected-categories">
		<h3 class="text-center">Select top level Services Required</h3>
		<div class="text-center">
			<small>Select sub categories in the next screen</small>
		</div>
		<div class="col-sm-4 mar-t-30 ">
			<div class="pull-right">
				@foreach($data["selectedServices"] as $key => $selectedService)
					<input type="checkbox" name="ticketWorkFlowSelectedCategories" class="ticket-selected-categories"
							id="label-category-{{ $key }}" @if($selectedService == 1) checked @endif value="{{ $key }}">
					<label for="label-category-{{ $key }}" class="pad-l-5">
						<h4>{{ $key }}</h4>
					</label>
					<br>
				@endforeach
			</div>
		</div>
		<div class="col-sm-4 mar-t-30">
			<div class="pull-right">
				<div class="col-sm-6">
					<label for="minBudget">Min. Budget*</label>
					<select class="form-control" id="minBudget" name="minBudget">
						<option value="">--Min Budget--</option>
						@foreach(config('evibe.type_customer_budget') as $budget)
							<option value="{{$budget}}" @if(isset($data['minBudget']) && $data['minBudget'] == $budget)selected @endif>{{$budget}}₹</option>
						@endforeach
					</select>
				</div>
				<div class="col-sm-6">
					<label for="maxBudget">Max. Budget*</label>
					<select class="form-control" id="maxBudget" name="maxBudget">
						<option value="">--Max Budget--</option>
						@foreach(config('evibe.type_customer_budget') as $budget)
							<option value="{{$budget}}" @if(isset($data['maxBudget']) && $data['maxBudget'] == $budget)selected @endif>{{$budget}}₹</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="text-center mar-t-30">
			@if($ticketWorkflowRoleId == config("evibe.roles.syrow"))
				<a href="{{ route("syrow.tickets.list") }}" class="btn btn-link mar-r-10">
					<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
				</a>
			@else
				<a href="@if($data["isEdit"] != 1){{ route("ticket.list") }}@elseif(isset($data["ticket"]->id)){{ route("ticket.details.info", [$data["ticket"]->id]) }}@endif"
						class="btn btn-link mar-r-10">
					<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
				</a>
			@endif
			<button type="submit" class="btn btn-success btn-lg btn-selected-service-continue">Continue <i
						class="glyphicon glyphicon-arrow-right"></i></button>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/capture-requirement.js");
		});
	</script>
@endsection