@extends('tickets.capture-requirements.capture_requirements_base')

@section('content')
	<div id="wizard" class="form_wizard wizard_horizontal mar-t-30 mar-b-20">
		<ul class="wizard_steps anchor">
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.party.details", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="selected" isdone="1" rel="1">
					<span class="step_no">1</span>
					<span class="step_descr">Step 1<br>
						<small>Party requirements</small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.additional.party.details", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="selected" isdone="1" rel="2">
					<span class="step_no">2</span>
					<span class="step_descr">Step 2<br>
						<small>Additional party requirements based on occasion</small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.services", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="selected" isdone="1" rel="3">
					<span class="step_no">3</span>
					<span class="step_descr">Step 3<br>
						<small>Customer required services</small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.contact.info", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="selected" isdone="1" rel="4">
					<span class="step_no">4</span>
					<span class="step_descr">Step 4<br>
						<small>Contact Details</small>
					</span>
				</a>
			</li>

			<li>
				<a class="selected" isdone="1" rel="5">
					<span class="step_no">5</span>
					<span class="step_descr"><b>Step 5</b><br>
						<small><b>Closure</b></small>
					</span>
				</a>
			</li>

		</ul>
	</div>
	<hr>
	<div class="create-package-content-div">
		<h3 class="text-center mar-t-30 mar-b-20">Party Status Details</h3>
		<div class="col-sm-10 col-sm-offset-1 mar-t-30">
			@if(session()->has('errorMsg'))
				<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert"
							aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-remove-sign"></i>
					{{ session('errorMsg') }}
				</div>
			@endif
			<form action="@if($data["isEdit"] == 1){{ route("ticket.modify.save.status.info", [$data["ticketId"], 1]) }}@else{{ route("ticket.create.save.status.info",$data['ticketId']) }}@endif"
					id="saveStatusDetailsForm"
					class="new-ticket-create-form" method="post">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="status">Booking Likeliness *</label>
						<select class="form-control" id="status" name="status" data-validation="required"
								data-validation-error-msg="Please Select Booking Likeliness">
							<option value="">--Select Booking Likeliness--</option>
							@foreach($data['whenCustomerPlanToBookThisServices'] as $leadStatus)
								<option value="{{$leadStatus["value"]}}"
										@if($leadStatus["value"] == $data["bookingLikelinessId"]) selected @endif>{{$leadStatus["name"]}}</option>
							@endforeach
						</select>
						<div class="text-left pad-t-3">
							<small>By when are you planning to book these services ?</small>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="nextFollowUp">Next Follow Up will be *</label>
						<input type="text" class="form-control" id="eventDate" name="nextFollowUp"
								value="@if(old('nextFollowUp')){{old('nextFollowUp')}}@elseif(isset($data['nextAutoFollowUp'])){{date('Y/m/d H:i',strtotime($data['nextAutoFollowUp']))}}@endif"
								data-validation="required"
								data-validation-error-msg="Please enter followup date"
								autocomplete="off">
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="typeFollowup">Followup Reason</label>
						<select class="form-control" id="typeFollowup" name="typeFollowup">
							<option value="">--Select Followup Reason--</option>
							@foreach($data['followupType'] as $followupType)
								<option value="{{$followupType['name']}}"
										@if(isset($data['ticketFollowup']) && $data['ticketFollowup'] == $followupType['name'] ) selected @endif>{{$followupType['name']}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<div>
							<label for="popUpEnquiryTimeSlot"
									class="custom-mdl-label">Best Time To Reach?</label>
							<select class="form-control" id="popUpEnquiryTimeSlot" name="popUpEnquiryTimeSlot">
								<option value="">--Select an option--</option>
							</select>
						</div>
						@php
							if($data['popUpEnquiryTimeSlot'] == 'Immediate')
							{
							$data['popUpEnquiryTimeSlot'] = 'Within 1 Hour';
							}
						@endphp
						<div class="hide" id="popUpEnquiryTimeSlotOptions">
							@foreach(config('evibe.customer_preferred_slot') as $value)
								<option value="{{$value}}"
										@if(isset($data['popUpEnquiryTimeSlot']) && $data['popUpEnquiryTimeSlot'] == $value ) selected @endif>{{$value}}</option>
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="ticketSource">Ticket Source *</label>
						<select name="ticketSource" id="ticketSource" class="form-control" data-validation="required"
								data-validation-error-msg="Please Select Ticket Source">
							<option value="">-- Please Select Ticket Source --</option>
							@foreach($data['ticketSource'] as $key=>$value)
								<option
										@if((old('ticketSource') == $value['id']) ||
										((isset($data['thisTicketSourceId']) && $data['thisTicketSourceId'] == $value['id']))) selected
										@endif
										value="{{$value['id']}}">{{$value['name']}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="sourceSpecific">Source Specific</label>
						<input type="text" class="form-control" id="sourceSpecific" name="sourceSpecific" placeholder="Enter Source specific details"
								value="@if(old('sourceSpecific')){{ old('sourceSpecific') }}@elseif(isset($data["sourceSpecific"])){{ $data["sourceSpecific"] }}@endif">
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="decisionMaker">Decision Maker *</label>
						<select name="decisionMaker" id="decisionMaker" class="form-control" data-validation="required"
								data-validation-error-msg="Please Select Decision Maker">
							<option value="">-- Please Select Decision Maker --</option>
							@foreach(config('evibe.type_customer_relation') as $key=>$value)
								<option value="{{$key}}"
										@if(isset($data['decisionMaker']) && $data['decisionMaker'] == $key) selected @endif>{{$value}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 decision-maker-contact">
					<div class="form-group">
						<label for="altEmail">Alt Email</label>
						<input type="text" class="form-control email-typo-error" id="altEmail" name="altEmail"
								value="@if(old('altEmail')){{ old('altEmail') }}@elseif(isset($data["altEmail"])){{ $data["altEmail"] }}@endif"
								placeholder="Enter alternate email id">
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 decision-maker-contact">
					<div class="form-group">
						<label for="altPhone">Alt Phone number</label>
						<input type="text" class="form-control" id="altPhone" name="altPhone"
								value="@if(old('altPhone')){{ old('altPhone') }}@elseif(isset($data["altPhone"])){{ $data["altPhone"] }}@endif"
								placeholder="Enter alternate phone number"
								autocomplete="off">
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="pad-10 pad-t-20">
					<label for="teamNotes">Additional Comments</label>
					<textarea id="teamNotes" name="teamNotes" class="form-control vertical-resize"
							rows="4"
							placeholder="enter comments...">@if(isset($data["teamNotes"]) && $data["teamNotes"]){{$data["teamNotes"]}}@elseif (isset($data["comments"]) && $data["comments"]) {{$data["comments"]}}@endif</textarea>
				</div>
				<input type="hidden" id="isClose" name="isClose" value="0">
				<div class="text-center mar-t-30">
					<a href="@if($data["isEdit"] != 1){{ route("ticket.list") }}@elseif(isset($data["ticketId"])){{ route("ticket.details.info", [$data["ticketId"]]) }}@endif"
							class="btn btn-link mar-r-10">
						<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
					</a>
					@if($data["isEdit"] == 1)
						<button type="submit" data-is-close=1
								class="btn btn-success btn-submit-status-info btn-lg"><i
									class="glyphicon glyphicon-remove-circle"></i>{{ " SAVE & CLOSE" }}</button>
						<div class="in-blk pad-10"></div>
					@endif
					<button type="submit" class="btn btn-success btn-lg btn-submit-status-info" data-is-close=0><i
								class="glyphicon glyphicon-ok"></i> @if($data["isEdit"] == 1){{ "SAVE & NEXT" }}@else{{ "NEXT" }}@endif
					</button>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/capture-requirement.js");
		});
	</script>
@endsection