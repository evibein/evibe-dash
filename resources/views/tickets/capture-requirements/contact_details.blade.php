@extends('tickets.capture-requirements.capture_requirements_base')

@section('content')
	<div id="wizard" class="form_wizard wizard_horizontal mar-t-30 mar-b-20">
		<ul class="wizard_steps anchor">
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.party.details", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="selected" isdone="1" rel="1">
					<span class="step_no">1</span>
					<span class="step_descr">Step 1<br>
						<small>Party requirements</small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.services", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="selected" isdone="1" rel="2">
					<span class="step_no">2</span>
					<span class="step_descr">Step 2<br>
						<small>Services Required</small>
					</span>
				</a>
			</li>
			<li>
				<a class="selected" isdone="1" rel="3">
					<span class="step_no">3</span>
					<span class="step_descr"><b>Step 3</b><br>
						<small><b>Closure</b></small>
					</span>
				</a>
			</li>
		</ul>
	</div>
	<hr>
	<div class="create-package-content-div">
		<h3 class="text-center mar-t-30 mar-b-20">Contact Details</h3>
		<div class="col-sm-10 col-sm-offset-1 mar-t-30">
			@if(session()->has('errorMsg'))
				<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert"
							aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-remove-sign"></i>
					{{ session('errorMsg') }}
				</div>
			@endif
			<form action="@if($data["isEdit"] == 1){{ route("ticket.modify.save.contact.info", [$data['ticketId'], 1]) }}@else{{ route("ticket.create.save.contact.info", [$data['ticketId']]) }}@endif"
					id="saveContactDetailsForm"
					class="new-ticket-create-form" method="post">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="name">Phone number* </label>
						<input type="text" class="form-control" data-validation="required" id="phone" name="phone"
								data-validation-error-msg="Please enter the phone number"
								value="@if(old('phone')){{ old('phone') }}@elseif(isset($data["phone"])){{ $data["phone"] }}@endif"
								placeholder="Enter customer phone number">
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="name">Alt Phone number </label>
						<input type="text" class="form-control" id="altPhone" name="altPhone"
								value="@if(old('altPhone')){{ old('altPhone') }}@elseif(isset($data["altPhone"])){{ $data["altPhone"] }}@endif"
								placeholder="Enter customer Alternate phone number">
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="hide">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="landmark">Landmark</label>
						<input type="text" class="form-control" name="landmark" id="landmark"
								placeholder="enter landmark"
								value="@if(old('landmark')){{ old('landmark') }}@elseif(isset($data["venueLandmark"])){{ $data["venueLandmark"] }}@endif">
					</div>
				</div>
				<div class="clearfix"></div>
					<div class="col-xs-12 col-sm-4">
						<div class="form-group">
							<label for="nextFollowUp">Next Follow Up will be *</label>
							<input type="text" class="form-control event-date" id="nextFollowUp" name="nextFollowUp"
									value="@if(old('nextFollowUp')){{old('nextFollowUp')}}@elseif(isset($data['nextAutoFollowUp'])){{date('d/m/Y H:i',$data['nextAutoFollowUp'])}}@endif"
									data-validation="required" data-validation-error-msg="Please enter followup date" autocomplete="off">
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="form-group">
							<label for="typeFollowup">Followup Reason*</label>
							<select class="form-control" id="typeFollowup" name="typeFollowup">
								<option value="">--Select Followup Reason--</option>
								@foreach($data['followupType'] as $followupType)
									<option value="{{$followupType['name']}}"
											@if(isset($data['ticketFollowup']) && $data['ticketFollowup'] == $followupType['name'] ) selected @endif>{{$followupType['name']}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="form-group">
							<label for="closureDate">Closure Date*</label>
							<input type="text" class="form-control event-date" id="closureDate" name="closureDate"
									value="@if(old('closureDate')){{old('closureDate')}}@elseif(isset($data['closureDate'])){{date('d/m/Y H:i',$data['closureDate'])}}@endif"
									data-validation="required" data-validation-error-msg="Please enter closure date" autocomplete="off">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				@php
					if($data['popUpEnquiryTimeSlot'] == 'Immediate')
					{
					$data['popUpEnquiryTimeSlot'] = 'Within 1 Hour';
					}
				@endphp
				<div class="pad-10 pad-t-20">
					<label for="teamNotes">Additional Comments</label>
					<textarea id="teamNotes" name="teamNotes" class="form-control vertical-resize"
							rows="4"
							placeholder="enter comments...">@if(isset($data["teamNotes"]) && $data["teamNotes"]){{$data["teamNotes"]}}@elseif (isset($data["comments"]) && $data["comments"]) {{$data["comments"]}}@endif</textarea>
				</div>
				<input type="hidden" id="isClose" name="isClose" value="0">
				<div class="text-center mar-t-30 mar-b-20">
					@php $ticketWorkflowRoleId = \Illuminate\Support\Facades\Auth::user()->role_id; @endphp
					@if($ticketWorkflowRoleId == config("evibe.roles.syrow"))
						<a href="{{ route("syrow.tickets.list") }}" class="btn btn-link mar-r-10">
							<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
						</a>
						<button type="submit" class="btn btn-success btn-lg btn-submit-contact-info" data-is-close=0><i class="glyphicon glyphicon-ok"></i>
							NEXT <i class="glyphicon glyphicon-arrow-right"></i>
						</button>
					@else
						<a href="@if($data["isEdit"] != 1){{ route("ticket.list") }}@elseif(isset($data["ticketId"])){{ route("ticket.details.info", [$data["ticketId"]]) }}@endif"
								class="btn btn-link mar-r-10">
							<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
						</a>
						@if($data["isEdit"] == 1)
							<button type="submit" data-is-close=1
									class="btn btn-success btn-submit-contact-info btn-lg"><i
										class="glyphicon glyphicon-remove-circle"></i>{{ " SAVE & CLOSE" }}</button>
							<div class="in-blk pad-10"></div>
						@endif
						<button type="submit" class="btn btn-success btn-lg btn-submit-contact-info" data-is-close=0><i
									class="glyphicon glyphicon-ok"></i> @if($data["isEdit"] == 1){{ "SAVE & NEXT" }}@else{{ "NEXT" }}@endif
						</button>
					@endif
				</div>
			</form>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/capture-requirement.js");
		});
	</script>
@endsection