@extends('tickets.capture-requirements.capture_requirements_base')

@section('content')
	<nav class="navbar navbar-default hide">
		<div class="container-fluid">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav top-nav-bar-create-ticket-wrap">
					<li data-value="1" class="top-nav-bar-create-ticket disabled"><a
								href="{{ route("ticket.create.edit.party.details", [$data["ticketId"]]) }}">Party Info</a>
					</li>
					<li data-value="2" class="top-nav-bar-create-ticket active"><a>Additional Info</a></li>
					<li data-value="3" class="top-nav-bar-create-ticket"><a
								href="{{ route("ticket.create.edit.services", [$data["ticketId"]]) }}">Services</a></li>
					<li data-value="4" class="top-nav-bar-create-ticket"><a
								href="{{ route("ticket.create.edit.contact.info", [$data["ticketId"]]) }}">Contact Info</a>
					</li>
					<li class="top-nav-bar-create-ticket"><a
								href="@if(isset($data["ticketId"])){{ route("ticket.create.edit.status.info", [$data["ticketId"]]) }}@else{{ route("ticket.create.initialise.details") }}@endif">Status Info</a>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="clearfix"></div>
	<div id="wizard" class="form_wizard wizard_horizontal mar-t-30 mar-b-20">
		<ul class="wizard_steps anchor">
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.party.details", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="selected" isdone="1" rel="1">
					<span class="step_no">1</span>
					<span class="step_descr">Step 1<br>
						<small>Party requirements</small>
					</span>
				</a>
			</li>
			<li>
				<a class="selected" isdone="1" rel="2">
					<span class="step_no">2</span>
					<span class="step_descr"><b>Step 2</b><br>
						<small><b>Additional party requirements based on occasion</b></small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.services", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="disabled" isdone="0" rel="3">
					<span class="step_no">3</span>
					<span class="step_descr">Step 3<br>
						<small>Services Required</small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.contact.info", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="disabled" isdone="0" rel="4">
					<span class="step_no">4</span>
					<span class="step_descr">Step 4<br>
						<small>Contact Details</small>
					</span>
				</a>
			</li>

			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.status.info", [$data["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif"
						class="disabled" isdone="0" rel="5">
					<span class="step_no">5</span>
					<span class="step_descr">Step 5<br>
						<small>Closure</small>
					</span>
				</a>
			</li>

		</ul>
	</div>
	<hr>
	<div class="create-package-content-div">
		<h3 class="text-center mar-t-30 mar-b-20">Additional Party Requirements</h3>
		<div class="col-sm-10 col-sm-offset-1 mar-t-30">
			@if(session()->has('errorMsg'))
				<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert"
							aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-remove-sign"></i>
					{{ session('errorMsg') }}
				</div>
			@endif
			<form action="@if($data["isEdit"] == 1){{ route("ticket.modify.save.additional.party.details", [$data['ticketId'], 1]) }}@else{{ route("ticket.create.save.additional.party.details", [$data['ticketId']]) }}@endif"
					id="submitAdditionalFieldsForm"
					class="new-ticket-create-form" method="post">
				<div class="additional-fields-input">
					<div class="col-xs-12 col-sm-4">
						<label for="childEvents">Select Specific Event*</label>
						<select class="form-control" id="childEvents" name="childEvents" data-validation="required" data-validation-error-msg="Please Select Event">
							<option value="">--Select Event--</option>
							@foreach($data['kidsBirthDayInfo'] as $kidsBirthDayInfo)
								<option value="{{$kidsBirthDayInfo['id']}}"
										@if($data['eventId'] == $kidsBirthDayInfo['id']) selected @endif>{{$kidsBirthDayInfo['name']}}</option>
							@endforeach
						</select>
					</div>
					@if(($data['eventId'] == config("evibe.event.kids_birthday"))||($data['parentIdOfEventId'] == config("evibe.event.kids_birthday")))
						<div class="col-xs-12 col-sm-4">
							<label for="kidsBirthdayGender">Select the baby gender *</label>
							<select class="form-control" name="kidsBirthdayGender" id="kidsBirthdayGender" data-validation="required" data-validation-error-msg="Please Select Customer Gender">
								<option value="">--Select Gender--</option>
								<option value="Boy"
										@if($data['kidsBirthdayGender'] == 'Boy') selected @endif>Boy
								</option>
								<option value="Girl"
										@if($data['kidsBirthdayGender'] == 'Girl') selected @endif>Girl
								</option>
								<option value="Both"
										@if($data['kidsBirthdayGender'] == 'Both') selected @endif>Both
								</option>
							</select>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
				<div class="pad-10 pad-t-20">
					<label for="teamNotes">Additional Comments</label>
					<textarea id="teamNotes" name="teamNotes" class="form-control vertical-resize"
							rows="4"
							placeholder="enter comments...">@if(isset($data["teamNotes"]) && $data["teamNotes"]){{$data["teamNotes"]}}@elseif (isset($data["customerComments"]) && $data["customerComments"]) {{$data["customerComments"]}}@endif</textarea>
				</div>
				<input type="hidden" id="isClose" name="isClose" value="0">
				<div class="text-center mar-t-30">
					<a href="@if($data["isEdit"] != 1){{ route("ticket.list") }}@elseif(isset($data["ticketId"])){{ route("ticket.details.info", [$data["ticketId"]]) }}@endif"
							class="btn btn-link mar-r-10">
						<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
					</a>
					@if($data["isEdit"] == 1)
						<button type="submit" data-is-close=1
								class="btn btn-success btn-submit-additional-fields btn-lg">{{ "SAVE & CLOSE " }}<i
									class="glyphicon glyphicon-remove-circle"></i></button>
						<div class="in-blk pad-10"></div>
					@endif
					<button type="submit" data-is-close=0
							class="btn btn-lg btn-success btn-submit-additional-fields">@if($data["isEdit"] == 1){{ "SAVE & NEXT" }}@else{{ "NEXT" }}@endif
						<i class="glyphicon glyphicon-arrow-right"></i></button>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/capture-requirement.js");
		});
	</script>
@endsection