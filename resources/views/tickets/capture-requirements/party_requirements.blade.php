@extends('tickets.capture-requirements.capture_requirements_base')

@section('content')
	<div id="wizard" class="form_wizard wizard_horizontal mar-t-30 mar-b-20">
		<ul class="wizard_steps anchor">
			<li>
				<a class="selected" isdone="1" rel="1">
					<span class="step_no">1</span>
					<span class="step_descr"><b>Step 1</b><br>
						<small><b>Party requirements</b></small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.services", [$data["response"]["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif" class="disabled" isdone="0" rel="2">
					<span class="step_no">2</span>
					<span class="step_descr">Step 2<br>
						<small>Services Required</small>
					</span>
				</a>
			</li>
			<li>
				<a href="@if($data["isEdit"] == 1){{ route("ticket.modify.edit.contact.info", [$data["response"]["ticketId"], 1]) }}@else{{ "javascript:void(0)" }}@endif" class="disabled" isdone="0" rel="3">
					<span class="step_no">3</span>
					<span class="step_descr">Step 3<br>
						<small>Closure</small>
					</span>
				</a>
			</li>
		</ul>
	</div>
	<hr>
	<div class="create-package-content-div">
		<h3 class="text-center mar-t-20 no-mar-b">Party Requirements</h3>
		<div class="col-sm-10 col-sm-offset-1 mar-t-30">
			@if(session()->has('errorMsg'))
				<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-remove-sign"></i>
					{{ session('errorMsg') }}
				</div>
			@endif
			<form action="@if($data["isEdit"] == 1){{ route("ticket.modify.save.party.details", [$data["response"]["ticketId"], 1]) }}@else{{ route("ticket.create.save.initialise.details") }}@endif"
					id="newTicketCreateForm" class="new-ticket-create-form" method="post">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<div class="col-lg-3 no-pad-l">
							<label for="customerGender">Title*</label>
							<select class="form-control no-pad" data-validation="required" id="customerGender" name="customerGender" data-validation-error-msg="Please Select Title">
								<option value="" selected>--Select--</option>
								<option value="1"
										@if(old('customerGender') == 1 || ((isset($data["response"]["customerGender"])) && ($data["response"]["customerGender"])  == 1)) selected @else @endif>Mr
								</option>
								<option value="2"
										@if(old('customerGender') == 2 || ((isset($data["response"]["customerGender"])) && ($data["response"]["customerGender"])  == 2)) selected @else @endif>Ms
								</option>
							</select>
						</div>
						<div class="col-lg-9 no-pad">
							<label for="customerName">Customer Name*</label>
							<input type="text" class="form-control" data-validation="required" id="customerName"
									name="customerName" data-validation-error-msg="Please enter the customer name"
									value="@if(old('customerName')){{ old('customerName') }}@elseif(isset($data["response"]["name"])){{ $data["response"]["name"] }}@endif"
									placeholder="Enter customer name">
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="occasion">Primary Event*</label>
						<select class="form-control" id="occasion" name="occasion" data-validation="required"
								data-validation-error-msg="Please Select Event">
							<option value="">--Select Event--</option>
							@foreach($data['occasions'] as $occasion)
								<option @if(old('occasion') == $occasion->id) selected
										@elseif(isset($data["parentEventId"]) && $data["parentEventId"] == $occasion->id) selected
										@endif value="{{ $occasion->id }}">{{ $occasion->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<div class="col-lg-7 no-pad-l">
							<label for="eventDate">Party Date*
								<small>(mm/dd/yyyy)</small>
							</label>
							<input type="text" class="form-control" id="eventDate" name="eventDate"
									value="@if(old('eventDate')){{ old('eventDate') }}@elseif(isset($data["response"]["eventDate"])){{ date('Y/m/d',$data["response"]["eventDate"]) }}@endif"
									data-validation="required" data-validation-error-msg="Please enter party date" placeholder="Select party date" autocomplete="off">
						</div>
						<div class="col-lg-5 no-pad">
							<label for="eventSlot">Party Time*</label>
							<input type="text" class="form-control"  name="eventSlot" id="eventSlot"
									value="@if(old('eventSlot')){{ old('eventSlot') }}@elseif(isset($data["response"]["eventSlot"])){{ date('H:i',$data["response"]["eventSlot"]) }}@endif"
									data-validation="required" data-validation-error-msg="Please Select a party Time" placeholder="Select party Time">
						</div>

					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="city">City*</label>
						<select name="city" id="city" class="form-control">
							<option value="-1" selected>-- select city --</option>
							@foreach($data['city'] as $city)
								<option value="{{ $city->id }}"
										@if(((isset($data['response']['cityId'])) && (($data['response']['cityId']) == $city->id))
										|| ((old('city') == $city->id))) selected
										@endif>{{ $city->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="area">
							<span class="mar-r-10">Location*</span>
							<a href="#" class="btn-add-loc text-normal" data-redirectto="{{ route('ticket.new') }}">Add New Location</a>
						</label>
						<select name="area" id="area" class="form-control" pincode="" data-default="@if(isset($data['response']['area_id'])){{$data['response']['area_id']}}@endif">
						</select>
						@if($data['area'])
							<ul id="allAreaOptions" class="hide">
								@foreach ($data['area'] as $area)
									<li data-id="{{ $area->id }}" data-city_id="{{ $area->city_id }}" data-name="{{ $area->name }}" data-pin="{{ $area->zip_code }}"></li>
								@endforeach
							</ul>
						@endif
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="zip">Zip Code* </label>
						<input type="text" class="form-control" id="zip" name="zip" placeholder="enter zip code"
								data-validation="number" data-validation-allowing="range[100000;999999]"
								value="@if(old('zip')){{ old('zip') }}@elseif(isset($data['response']['zip_code'])){{$data['response']['zip_code']}}@endif">
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label for="email">Email* </label>
						<input type="text" class="form-control email-typo-error" id="email" name="email" data-validation="required"
								data-validation-error-msg="Please enter the email"
								value="@if(old('email')){{ old('email') }}@elseif(isset($data["response"]["email"])){{ $data["response"]["email"] }}@endif"
								placeholder="Enter email of the customer">
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-12 col-sm-4">
					<div class="form-group create-ticket-venue-type hide">
						<label for="typeVenue">Where*</label>
						<select class="form-control" id="typeVenue" name="typeVenue" data-validation="required" data-validation-error-msg="Please Select The Location">
							<option value="">--Select Place--</option>
							@foreach($data['typeVenues'] as $typeVenue)
								<option class="create-ticket-venue-type-option" @if(old('typeVenue') == $typeVenue->id) selected
										@elseif(isset($data["response"]["typeVenueId"]) && $data["response"]["typeVenueId"] == $typeVenue->id) selected
										@endif value="{{ $typeVenue->id }}">{{ $typeVenue->name }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="col-xs-12 col-sm-4 additional-fields-input hide">
					<div class="form-group">
						<label for="childEvents">Select Specific Event*</label>
						<select class="form-control" id="childEvents" name="childEvents" data-validation="required" data-validation-error-msg="Please Select Event">
							<option value="">--Select Event--</option>
							@foreach($data['kidsBirthDayInfo'] as $kidsBirthDayInfo)
								<option value="{{$kidsBirthDayInfo['id']}}"
										@if((old("childEvents") == $kidsBirthDayInfo['id']) || $data['eventId'] == $kidsBirthDayInfo['id']) selected @endif>{{$kidsBirthDayInfo['name']}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 additional-fields-input-gender hide">
					<div class="form-group">
						<label for="kidsBirthdayGender">Select the baby gender *</label>
						<select class="form-control" name="kidsBirthdayGender" id="kidsBirthdayGender" data-validation="required" data-validation-error-msg="Please Select Customer Gender">
							<option value="">--Select Gender--</option>
							<option value="Boy"
									@if((old("kidsBirthdayGender") == "Boy") || (isset($data["response"]["kidsBirthdayGender"]) && $data["response"]["kidsBirthdayGender"] == 'Boy')) selected @endif>Boy
							</option>
							<option value="Girl"
									@if((old("kidsBirthdayGender") == "Girl") || (isset($data["response"]["kidsBirthdayGender"]) && $data["response"]["kidsBirthdayGender"] == 'Girl')) selected @endif>Girl
							</option>
							<option value="Both"
									@if((old("kidsBirthdayGender") == "Both") || (isset($data["response"]["kidsBirthdayGender"]) && $data["response"]["kidsBirthdayGender"] == 'Both')) selected @endif>Both
							</option>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				<input type="hidden" id="isClose" name="isClose" value="0">
				<input type="hidden" id="selectedEventId" name="selectedEventId" value="@if(isset($data["response"]["eventId"])){{ $data["response"]["eventId"] }}@endif">
				<div class="pad-10 pad-t-20">
					<label for="teamNotes">Additional Comments</label>
					<textarea id="teamNotes" name="teamNotes" class="form-control vertical-resize" rows="4"
							placeholder="enter comments...">@if(old("teamNotes") != ""){{old("teamNotes")}}
						@elseif(isset($data["response"]["teamNotes"]) && $data["response"]["teamNotes"]){{$data["response"]["teamNotes"]}}
						@elseif(isset($data["response"]["customerComments"]) && $data["response"]["customerComments"]) {{$data["response"]["customerComments"]}}
						@endif{{""}}</textarea>
				</div>
				<div class="text-center mar-t-30 mar-b-20">
					@php $ticketWorkflowRoleId = \Illuminate\Support\Facades\Auth::user()->role_id; @endphp
					@if($ticketWorkflowRoleId == config("evibe.roles.syrow"))
						<a href="{{ route("syrow.tickets.list") }}" class="btn btn-link mar-r-10">
							<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
						</a>
						<button type="submit" data-is-close=0 class="btn btn-success btn-new-ticket-create btn-lg">NEXT
							<i class="glyphicon glyphicon-arrow-right"></i></button>
					@else
						<a href="@if($data["isEdit"] != 1){{ route("ticket.list") }}@elseif(isset($data["response"]["ticketId"])){{ route("ticket.details.info", [$data["response"]["ticketId"]]) }}@endif"
								class="btn btn-link mar-r-10">
							<i class="glyphicon glyphicon-remove-circle"></i> CLOSE
						</a>
						@if($data["isEdit"] == 1)
							<button type="submit" data-is-close=1 class="btn btn-success btn-new-ticket-create btn-lg">{{ "SAVE & CLOSE " }}
								<i class="glyphicon glyphicon-remove-circle"></i></button>
							<div class="in-blk pad-10"></div>
						@endif
						<button type="submit" data-is-close=0 class="btn btn-success btn-new-ticket-create btn-lg">@if($data["isEdit"] == 1){{ "SAVE & NEXT" }}@else{{ "NEXT" }}@endif
							<i class="glyphicon glyphicon-arrow-right"></i></button>
					@endif
				</div>
			</form>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tickets/capture-requirement.js");
		});
	</script>
@endsection