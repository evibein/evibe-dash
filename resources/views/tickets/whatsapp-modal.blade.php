<div id="whatsappModal" class="modal" tabindex="-1" role="dialog">
	<!-- Modal content -->
	<div class="modal-dialog modal-sm">
		<div class="modal-content text-center">
			<div class="container text-center">
				<div class="modal-header mar-t-10">
					<h4 class="modal-title">
						<img src="http://gallery.evibe.in/img/icons/whatsapp-logo.png"> Send Whatsapp Message
					</h4>
				</div>
				<div class="mar-t-10 pad-b-10">
					<input type="hidden" class="hidden-message" value="">
					<label for="typeMessage"></label>
					<label class="pad-r-15"><input type="radio" name="typeMessage" id="typeMessage"
								value="followUp"> FollowUp</label>
					<label class="pad-r-15"><input type="radio" name="typeMessage" id="typeMessage"
								value="noResponse"> No Response</label>
					<label class="pad-r-15"><input type="radio" name="typeMessage" id="typeMessage" value="other"> Other</label>
				</div>
			</div>
			<div class="type-options hide" style="text-align: center;">
				<input id="updateDate" name="updateDate" type="hidden" value="{{date("Y/m/d H:i")}}">
				<input id="followupStatus" name="followupStatus" type="hidden" value="{{config('evibe.status.followup')}}">
				<input id="noResponseStatus" name="noResponseStatus" type="hidden" value="{{config('evibe.status.no_response')}}">
				<input id="route" name="route" type="hidden" value="{{route('ticket.status.update', "Id-req")}}">
				<div style="width:60%;margin:auto;">
					<div class="form-group mar-t-20 " >
						<label for="followupDate">Next Follow up Date*</label>
						<input id="followupDate" name="followupDate" type="text" class="form-control"
								placeholder="when is next followup?">
					</div>
				</div>
				<div class="type-followup hide" style="width:60%;margin:auto;">
					<div class="mar-t-20 form-group">
						<label for="followupType"> Followup Type* </label>
						<select id="followupType" name="followupType" class="form-control ">
							<option value="0">--Select Followup Type--</option>
							@foreach(config('evibe.type_followup') as $value=>$key)
								<option value="{{$value}}">
									{{$key}}
								</option>
							@endforeach
						</select>
					</div>
				</div>
				<div style="width:60%;margin:auto;">
					<div class="form-group mar-t-20">
						<div><label for="followupComments">Comments*</label></div>
						<textarea id="followupComments" name="followupComments" cols="20" class="form-control" placeholder="Type your comments"></textarea>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div>
				<a data-phone="" data-name=""
						class="send-whatsapp-msg btn btn-success mar-b-20 mar-t-10 disabled ">Send Message</a>
				<a class="send-whatsapp-msg-loading hide disabled btn btn-success mar-b-20 mar-t-10">Fetching the message, Please wait...</a>
			</div>
		</div>
	</div>
</div>