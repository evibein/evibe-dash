@extends('layout.base')

@section('content')
	<div class="booking-calendar-wrap">
		<div class="col-sm-10 col-md-10 col-lg-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="font-16 pull-left">
						<i class="glyphicon glyphicon-calendar"></i> Booking Calendar
					</div>
					<div class="pull-right">
						<select id="sortBy" class="form-control">
							<option value="-1" @if(!(request()->has('type'))) selected @endif>All Partners</option>
							@foreach($partners as $partner)
								<option value="{{ $partner['map_type_id'] }}-{{ $partner['id'] }}"
										@if($partner['map_type_id'].'-'.$partner['id'] == request()->input('type')) selected @endif>
									{{ $partner['name'] }} :: {{ $partner['person'] }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-body">
					<div id="loading" class="">
						<div class="text-center">
							<img src="{{ AppUtil::getGalleryBaseUrl() }}/img/prettyPhoto/dark_rounded/loader.gif"
									alt=""> Loading data ....
						</div>
					</div>
					<div id="showCalendar" class="pad-10"></div>
				</div>
			</div>
			<input type="hidden" id="binfo" value="{{ $bookingUrl }}" data-url="{{ route('calendar.booking.show') }}">
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			var body = $('body');
			var $calendar = $('#showCalendar');

			$(window).load(function () {
				$('#loading').addClass('hide');
			});

			// make ajax request when page loads
			var sortBySelector = $('#sortBy');

			sortBySelector.selectize({
				onChange: refreshPageOnFilter
			});

			// make a request to load data when page is refreshed (default showing current month data only)
			$calendar.fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				dayClick: function (date, allDay, jsEvent, view) {
					if (view.name === "month") {
						$calendar.fullCalendar('gotoDate', date);
						$calendar.fullCalendar('changeView', 'agendaDay');
					}
				}
			});
			loadCalendarData();

			body.on('click', '.fc-button-prev span', function (event) {
				loadCalendarData($calendar.fullCalendar('getView'));
			});

			body.on('click', '.fc-button-next span', function (event) {
				loadCalendarData($calendar.fullCalendar('getView'));
			});

			// functions handling calendar
			function loadCalendarData(range) {
				var sortBy = sortBySelector.val();
				var url = $('#binfo').val();
				var data = {type: sortBy, hasRange: 0};

				if (range && range.start && range.end) {
					data.start = range.start;
					data.end = range.end;
					data.hasRange = 1;
				}

				// show loading modal
				showLoading();

				$.ajax({
					type: 'POST',
					dataType: 'JSON',
					url: url,
					data: data,
					async: false,
					success: function (data) {
						if (data.success) {
							var eventData = [];
							var eventCount = [];
							$.each(data.bookings, function (index, value) {
								eventData.push({
									title: value.title,
									start: new Date(Date.parse(value.start)),
									allDay: false
								});

								if(typeof eventCount[value.start.substring(0, 10)] !== "undefined") {
									var count = parseInt(eventCount[value.start.substring(0, 10)], 10) + 1;
								} else {
									var count = 1;
								}
								eventCount[value.start.substring(0, 10)] = count;
							});

							console.log(eventCount);

							// update calendar data
							$calendar.fullCalendar('removeEvents');
							$calendar.fullCalendar('removeEventSource', eventData);
							$calendar.fullCalendar('addEventSource', eventData);

							hideLoading();
						}
						else {
							window.showNotyError("Data couldn't load properly, Please refresh the page or report tech team")
						}
					},
					error: function () {
						window.showNotyError("Failed to load data, please refresh the page, if error persist please contact tech team");
					}
				});
			}

			function refreshPageOnFilter(value) {

				if (value) {
					var url = $('#binfo').data('url');
					if (value == -1) {
						location.href = url;
					}
					else {
						location.href = url + '?type=' + value
					}
				}
			}

		});
	</script>
@endsection