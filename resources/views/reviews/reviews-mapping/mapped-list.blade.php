@extends('reviews.reviews-mapping.base')

@section('list-data')
	<div class="pad-t-20">
		@if(isset($data) && $data)
			<div class="col-sm-2 col-md-2 col-lg-2">
				<div>
					<h5 class="no-mar pull-left">Filter Options</h5>
					<div class="pull-right">
						@if($data['isShowReset'])
							<a class="btn btn-xs btn-warning mar-l-20"
									href="{{route('reviews-mapping.mapped')}}">Reset</a>
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="filter-group">
					<div class="pad-b-20 ">
						<label for="searchQuery" class="filter-title">Search</label>
						<div class="text-center">
							<input id="searchQuery" class="form-control" type="text" name="query"
									placeholder="Name, Phone, Email, Review"
									value="{{ request()->input('query') }}"/>
						</div>
					</div>
					<div class="ls-none no-mar no-pad">
						<label class="filter-title">By Party Date</label>
						<div class="pad-b-20">
							<div class="text-center">
								<input type="text" class="form-control filter-pd-start"
										value="{{ request()->input('start_date') }}" placeholder="Start Date">
							</div>
							<div class="in-blk text-center mar-t-10">
								<input type="text" class="form-control filter-pd-end"
										value="{{ request()->input('end_date') }}" placeholder="End Date">
							</div>
						</div>

						<label class="filter-title">By Rating</label>
						<div class="pad-b-20">
							<div class="text-center">
								<input type="text" class="form-control min-rating"
										value="{{ request()->input('min_rating') }}" placeholder="Min. Rating">
							</div>
							<div class="in-blk text-center mar-t-10">
								<input type="text" class="form-control max-rating"
										value="{{ request()->input('max_rating') }}" placeholder="Max. Rating">
							</div>
						</div>

						@php $explodePartnerId = explode('-', request()->input('p_id')) @endphp
						<div class="form-group order-tracking-provider-filter">
							<label for="selectPartner" class="filter-title">By Partner</label>
							<select name="selectPartner" id="selectPartner" class="form-control">
								<option value="all" data-type="all">All Partners</option>
								@foreach($data['partners'] as $partner)
									<option value="{{ $partner['id']."-".$partner['type'] }}"
											@if(isset($explodePartnerId[0]) && isset($explodePartnerId[1])
											&& ($explodePartnerId[0] == $partner['id'])
											&& ($explodePartnerId[1] == $partner['type']))selected @endif>{{ $partner['name'] . " - " . $partner['person'] }}</option>
								@endforeach

							</select>
						</div>
						<div class="pad-b-20">
							<label for="city" class="filter-title">By City</label>
							<select name="city" id="city" class="form-control">
								<option value="all">All Cities</option>
								@foreach($data['cities'] as $city)
									<option value="{{ $city['id'] }}"
											@if(request()->input('city')== $city['id'])selected @endif>{{ $city['name'] }}</option>
								@endforeach
							</select>
						</div>
						<div class="pad-b-20">
							<label for="occasion" class="filter-title">By Occasion</label>
							<select name="occasion" id="occasion" class="form-control">
								<option value="all">All Occasions</option>
								@foreach($data['occasions'] as $occasion)
									<option value="{{ $occasion['id']}}"
											@if(request()->input('occasion')== $occasion['id'])selected @endif>{{ $occasion['name'] }}</option>
								@endforeach
							</select>
						</div>
						<div class="pad-t-15 text-center">
							<button id="btnFilter" class="btn btn-info btn-filter-by-price">FILTER NOW</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-9 col-md-9 col-lg-9">
				<div class="text-center font-16 pad-b-10">
					Mapped Reviews List
				</div>
				@if(count($data['reviewsData'])>0)
					@foreach($data['reviewsData'] as $key=>$review)
						<div class="ticket-wrap">
							<div class="top-sec tracking-header no-mar-b">
								<div class="in-blk mar-l-10 mar-t-5 mar-b-5" style="vertical-align: center">
									<a class="font-16"><i
												class="glyphicon glyphicon-screenshot"></i> Review Given At :
									</a>
									@if($review['reviewAt'])
										<b>{{ $review['reviewAt'] }}</b>
									@else
										---
									@endif
								</div>
								<div class="in-blk mar-r-10 mar-b-10 edit-review" style="float: right" id="{{$review['reviewId']}}">
									<div class="btn btn-danger"><i
												class="glyphicon glyphicon-pencil"></i> Edit
									</div>
								</div>
							</div>
							<div class="map-review-{{$review['reviewId']}}" class="mar-l-10 mar-b-10">
								<div class="no-mar tracking-body">
									<div class="pad-l-10 pad-r-10 info-box">
										<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 no-pad-l no-pad-r">
											<div class="text-center no-pad-b">

												<div class="col-lg-3 col-sm-3 col-md-3 info-box">
													<div class="sub-sec-title no-pad-l head-color"> Name</div>
													<div class="sub-sec-value">
														<i class="glyphicon glyphicon-user"></i> {{$review['customerName']}}
													</div>
												</div>

												<div class="col-lg-4 col-sm-4 col-md-4 info-box">
													<div class="sub-sec-title no-pad-l head-color">Assigned Partner</div>
													<div class="sub-sec-value"> {{$review['partnerName'].'-'.$review['partnerPerson']}} </div>
												</div>

												<div class="col-lg-3 col-sm-3 col-md-3 info-box">
													<div class="sub-sec-title no-pad-l head-color"> Party Date</div>
													<div class="sub-sec-value">
														<i class="glyphicon glyphicon-time"></i> {{$review['partyDate']}}
													</div>
												</div>

												<div class="col-lg-2 col-sm-2 col-md-2 info-box">
													<div class="sub-sec-title no-pad-l head-color"> Rating</div>
													@if($review['rating'])
														<div class="sub-sec-value text-bold">
															<i class="glyphicon glyphicon-star"></i> {{$review['rating']}}
														</div>
													@else
														<div class="sub-sec-value"> --</div>
													@endif
												</div>

												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 mar-t-10 no-pad-l no-pad-r">
											<div class="col-lg-7 col-md-7 col-sm-7 no-pad-l">
												<div class="info-box text-center">
													<label>Customer Review </label>
													<div class="text-italic">
														@if($review['review'])
															{{$review['review']}}

														@else
															--
														@endif
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="col-lg-5 col-md-5 col-sm-5 no-pad-l no-pad-r">
												<div class="info-box">
													<label> Review Option </label>
													<div>
														{{$review['optionInfo']}}
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="clearfix"></div>
										@if((count($review['links']) > 0) || (count($review['unSelectedImages']) > 0))
											<div class="panel panel-default mar-t-10 no-mar-b">
												<div class="pad-l-10 pad-r-10">
													<div class="font-16 panel-heading no-pad-b text-red"> Delivery Images</div>
													<div class="in-blk panel-body no-mar-t no-pad-t">
														@foreach($review['links'] as $key=>$link )
															<img class="gallery-item in-blk img-links-{{$review['reviewId']}}" src="{{$link}}" data-link="{{$link}}" data-id="{{$key}}">
														@endforeach
														@foreach($review['unSelectedImages'] as $key=>$link )
															<img class="hide gallery-item in-blk deselect-img-links-{{$review['reviewId']}}" src="{{$link}}" data-link="{{$link}}" data-id="{{$key}}">
														@endforeach
													</div>
												</div>
											</div>
										@endif
									</div>
									<div class="modal-data">
										<input type="hidden" id='reviewId' value="{{$review['reviewId']}}">
										<input type="hidden" id='customerName' value="{{$review['customerName']}}">
										<input type="hidden" id='customerLocation' value="{{$review['customerLocation']}}">
										<input type="hidden" id='partyDate' value="{{$review['partyDate']}}">
										<input type="hidden" id='review' value="{{$review['review']}}">
										<input type="hidden" id='rating' value="{{$review['rating']}}">
										<input type="hidden" id='defaultId' value="{{$review['optionId']}}">
										<input type="hidden" id='defaultTypeId' value="{{$review['optionTypeId']}}">
										<div class="hide" id='reviewBookingInfo'> {{$review['bookingInfo']}}</div>
										<input type="hidden" id='reviewGivenAt' value="{{$review['reviewAt']}}">
										<input type="hidden" id='plannerInfo' value="{{$review['partnerName'].'-'.$review['partnerPerson']}}">
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<div class="alert alert-danger">
						No Mapped Reviews found.
					</div>
				@endif
				<div id="modal" class="modal" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-tracking-wrap">
						<div class="modal-content">
							<div class="modal-header">
								<div class="modal-title text-center">
									<h4 class="font-16 no-mar-b no-mar-t order-track-modal-header">Edit the Review</h4>
								</div>
							</div>
							<form class="form order-track-form" role="form">
								<div class="modal-footer no-mar-t">
									<div class="col-md-12 col-xs-12 pad-t-3 font-12 no-pad-l no-pad-r" id="">
										<div class="col-lg-3 col-sm-3 col-md-3 info-box">
											<div class="sub-sec-title head-color no-pad-l"> Name</div>
											<div class="sub-sec-value">
												<i class="glyphicon glyphicon-user"></i>
												<div class="in-blk" id="modalCustomerName">
													--
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-md-3 info-box">
											<div class="sub-sec-title no-pad-l head-color"> Assigned Partner</div>
											<div id="modalPlannerInfo">
												--
											</div>
										</div>
										<div class="col-lg-2 col-sm-2 col-md-2 info-box">
											<div class="sub-sec-title no-pad-l head-color"> Review Given At</div>
											<div id="modalReviewGivenAt">
												--
											</div>
										</div>
										<div class="col-lg-2 col-sm-2 col-md-2 info-box">
											<div class="sub-sec-title no-pad-l head-color"> Party Date</div>
											<div id="modalPartyDate">
												--
											</div>
										</div>
										<div class="col-lg-2 col-md-2 col-sm-2 info-box">
											<div class="sub-sec-title no-pad-l head-color"> Customer Rating</div>
											<div id="modalRating">
												--
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="mar-t-10 mar-b-10">
										<div class="col-sm-6 no-mar no-pad">
											<div class="col-sm-12 no-mar no-pad">
												<div class="info-box">
													<label class="no-pad-b">Customer Review </label>
													<div id="modalReview" class="text-italic" style="overflow-y: scroll;max-height:80px">
														---
													</div>
												</div>
											</div>
											<div class="col-sm-12 no-pad no-mar">
												<div class="mar-t-20 text-bold">Map To An Option</div>
												<div class="mar-t-5 no-pad no-mar">
													<div class="col-sm-4 no-pad-l no-mar-l">
														<select id="optionTypeId" name="optionTypeId" class="form-control"
																data-url="{{ route("reviews-mapping.getOptions") }}">
															<option value="-1">-- select type --</option>
															@foreach ($data['optionTypeData'] as $option)
																<option value="{{ $option['optionTypeId'] }}">{{ $option['optionTypeValue']}}</option>
															@endforeach
														</select>
													</div>
													<input type="hidden" value="" id="modalOptionTypeId">

													<div class="col-sm-8 no-mar no-pad">
														<select id="mappingOptions" name="mappingOptions"
																class="form-control"></select>
													</div>
													<input type="hidden" value="" id="modalOptionId">

													<div class="clearfix"></div>

													<div class="form-group hide mapping-type-options">
														<select id="mapId" name="mapId"
																class="form-control"></select>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>

										<div class="pad-l-5 col-sm-6">
											<div class="info-box">
												<label>Booking Info </label>
												<div id="modalBookingInfo" style="overflow-y: scroll;max-height:120px">
													---
												</div>
											</div>
										</div>
										<div class="clearfix"></div>

										<div class="mar-t-10 delivery-image-count hide">
											<label> Delivery Images </label>
											<div class="info-box">
												<div class="delivery-images mar-l-20 no-mar-t">
												</div>
											</div>
										</div>
										<div class="text-center mar-t-20 pad-t-10">
											<button type="button" class="btn btn-default in-blk" data-dismiss="modal">Cancel</button>
											<button type="button" data-id='' data-url='{{route('reviews-mapping.save-option-mapping')}}'
													class="btn btn-danger in-blk btn-review-map-save" disabled>
												CONFIRM
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="pad-t-10 text-center">
				<div class="page-links">
					{{$data['links']}}
				</div>
			</div>
		@endif
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">

		$(".edit-review").on('click', function () {

			$("#modal").modal('show');

			selectMapValue.clear();
			selectType.setValue('-1');
			$('.delivery-images').empty();
			$('.btn-review-map-save').attr('disabled', true);

			let id = $(this).attr('id');
			let info = $('.map-review-' + id);

			id && (!$('#modalReviewId').text(id) || $('.btn-review-map-save').data('id', id));
			let data;
			(data = info.find('#customerName').val()) ? $('#modalCustomerName').text(data) : $('#modalCustomerName').text('--');
			(data = info.find('#customerLocation').val()) ? $('#modalCustomerLocation').text(data) : $('#modalCustomerLocation').text('--');
			(data = info.find('#partyDate').val()) ? $('#modalPartyDate').text(data) : $('#modalPartyDate').text('--');
			(data = info.find('#review').val()) ? $('#modalReview').text(data) : $('#modalReview').text('--');
			(data = info.find('#rating').val()) ? $('#modalRating').text(data) : $('#modalRating').text('--');
			(data = info.find('#defaultTypeId').val()) ? ($('#modalOptionTypeId').val(data) && selectType.setValue(data)) : $('#modalOptionTypeId').val(null);
			(data = info.find('#defaultId').val()) ? ($('#modalOptionId').val(data) && console.log('option id ' + data)) : $('#modalOptionId').val(null);
			(data = info.find('#reviewBookingInfo').html()) ? $('#modalBookingInfo').html(data) : $('#modalBookingInfo').html('--');
			(data = info.find('#reviewGivenAt').val()) ? $('#modalReviewGivenAt').text(data) : $('#modalReviewGivenAt').text('--');
			(data = info.find('#plannerInfo').val()) ? $('#modalPlannerInfo').text(data) : $('#modalPlannerInfo').text('--');

			$('.img-links-' + id).each(function (key, value) {

				let source = $(this).data('link');
				let id = $(this).data('id');
				//var img="<img class=\"new-image\" width='150px' src=\"" +source+ "\" id=\"" + id + "\"/>";

				var img = "<div id='" + id + "' class='in-blk mar-r-20 mar-b-20 text-center'>" +
					"<div class='review-mapping-img-wrap'>" +
					"<a href='" + source + "' target='_blank' class='booking-gallery-link'>" +
					"<img src='" + source + "'  class='new-image image-selected review-mapping-img' id='" + id + "'>" +
					"</a>" +
					"</div>" +
					"<button type='button' class=\"select-image text-center btn btn-default mar-t-10\">Select</button>" + "</div>";

				$('.delivery-image-count').removeClass('hide');
				$(".delivery-images").append(img);
			});
			$('.deselect-img-links-' + id).each(function (key, value) {

				console.log('found');
				let source = $(this).data('link');
				let id = $(this).data('id');
				//var img="<img class=\"new-image\" width='150px' src=\"" +source+ "\" id=\"" + id + "\"/>";

				var img = "<div id='" + id + "' class='in-blk mar-r-20 mar-b-20 text-center'>" +
					"<div class='review-mapping-img-wrap'>" +
					"<a href='" + source + "' target='_blank' class='booking-gallery-link'>" +
					"<img src='" + source + "'  class='new-image review-mapping-img' id='" + id + "'>" +
					"</a>" +
					"</div>" +
					"<button type='button' class=\"select-image text-center btn btn-default mar-t-10\">Select</button>" + "</div>";

				$('.delivery-image-count').removeClass('hide');
				$(".delivery-images").append(img);
			});

			$('.select-image').on('click', function () {
				!$(this).parent().find('.new-image').hasClass('image-selected') ? ($(this).parent().find('.new-image').addClass("image-selected") && $(this).text("Remove")) : ($(this).parent().find('.new-image').removeClass("image-selected") && $(this).text("Select"));

			});
		});

		var $selectMapValue = $('#mappingOptions').selectize({
			valueField: 'id',
			labelField: 'displayName',
			searchField: ['label', 'name', 'code'],
			render: {
				option: function (item, escape) {
					return '<div>' +
						'<div><b>' + escape(item.label) + '</b></div>' +
						'<div class="font-12">' + escape(item.name) + '</div>' +
						'</div>';
				}
			}
		});
		var selectMapValue = $selectMapValue[0].selectize;
		selectMapValue.disable();

		var $selectType = $('#optionTypeId').selectize({
			onChange: function () {
				var optionTypeId = $('#optionTypeId').val();
				if (optionTypeId >= 1) {
					$("body").css("cursor", "wait");
					$.ajax({
						url: $('#optionTypeId').data("url") + "?optionTypeId=" + optionTypeId,
						type: 'post',
						dataType: 'html',
						success: function (data) {
							$("body").css("cursor", "default");
							$('.mapping-type-options').empty();
							$('.mapping-type-options').append(data);
							var results = [];
							$('.map-type-options option').each(function (count) {
								results.push({
									id: $(this).data('val'),
									label: $(this).data('code'),
									name: $(this).data('name'),
									displayName: $(this).data('code') + " - " + $(this).data('name')
								});
							});

							selectMapValue.disable();
							selectMapValue.clearOptions();
							selectMapValue.enable();
							selectMapValue.addOption(results);

							let id;
							(id = $('#modalOptionId').val() || selectMapValue.refreshOptions()) && selectMapValue.setValue(id);

							$('.btn-review-map-save').removeAttr('disabled');
						},
						error: function (jqXHR, textStatus, errorThrown) {
							$("body").css("cursor", "default");
							showNotyError('Error occurred while fetching data, Please reload the page.')
						}
					});
				} else {
					hideLoading();
					selectMapValue.disable();
					selectMapValue.clearOptions();
				}
			}
		});

		var selectType = $selectType[0].selectize;

		$('.btn-review-map-save').on('click', function () {

			var reviewId = $(this).data('id');
			var optionTypeId = $('#optionTypeId').val();
			var optionId = $('#mappingOptions').val();
			var imageId = [];

			$('.image-selected').each(function (key, value) {
				imageId.push($(this).attr('id'));
			});

			var url = $(this).data('url');
			var data = {
				isEdit: 1,
				reviewId: reviewId,
				optionTypeId: optionTypeId,
				optionId: optionId,
				imagesId: imageId
			};
			console.log(data);
			window.showLoading();

			$.ajax({
				url: url,
				type: 'POST',
				data: data
			}).done(function (data) {
				window.hideLoading();
				if (data.success) {
					showNotySuccess("Review Saved Successfully");
					setTimeout(function () {
						window.location.reload();
					}, 1500);
				} else {
					window.showNotyError();
				}

			}).error(function () {
				window.hideLoading();
				window.showNotyError();
			});

		});

		//filters

		$('.filter-pd-start').datetimepicker({
			format: 'Y/m/d',
			onShow: function (ct) {
				this.setOptions({
					maxDate: $('.filter-pd-end').val() ? $('.filter-pd-end').val() : false
				})
			},
			timepicker: false,
			scrollInput: false,
			scrollMonth: false,
			closeOnDateSelect: true
		});

		$('.filter-pd-end').datetimepicker({
			format: 'Y/m/d',
			onShow: function (ct) {
				this.setOptions({
					minDate: $('.filter-pd-start').val() ? $('.filter-pd-start').val() : false
				})
			},
			timepicker: false,
			scrollInput: false,
			scrollMonth: false,
			closeOnDateSelect: true
		});

		function updateUrlByFilters(event) {
			event.preventDefault();

			var startDate = $('.filter-pd-start').val();
			var endDate = $('.filter-pd-end').val();
			var minRating = $('.min-rating').val();
			var maxRating = $('.max-rating').val();
			var query = $('#searchQuery').val();
			var provider = $('#selectPartner').val();
			var city = $('#city').val();
			var occasion = $('#occasion').val();
			var url = "mapped?";
			var isUrlChanged = false;

			if (city) {
				isUrlChanged = true;
				url += '&city=' + city;
			}

			if (occasion) {
				isUrlChanged = true;
				url += '&occasion=' + occasion;
			}

			if (query) {
				isUrlChanged = true;
				url += '&query=' + query;
			}

			if (startDate || endDate) {
				isUrlChanged = true;
				url += '&start_date=' + startDate + '&end_date=' + endDate;
			}

			if (minRating || maxRating) {
				isUrlChanged = true;
				url += '&min_rating=' + minRating + '&max_rating=' + maxRating;
			}

			if (provider) {
				url += '&p_id=' + provider;
			}

			if (isUrlChanged) {
				location.href = url;
			}
		}

		$('#searchQuery').keyup(function (event) {
			if (event.which == 13 && $(this).val()) { // on enter submit search
				updateUrlByFilters(event);
			}
		});

		$('#city').change(updateUrlByFilters);

		$('#occasion').change(updateUrlByFilters);

		$('#selectPartner').selectize({
			maxItems: 1
		});

		$('#btnFilter').click(updateUrlByFilters);

		$('.pagination li a').on('click', function (e) {
			e.preventDefault();
			let page = $(this).text();
			var link = location.href;
			link.indexOf('?') > -1 ? window.location = location.href + "&page=" + page : window.location = location.href + "?page=" + page;

		});

	</script>
@stop