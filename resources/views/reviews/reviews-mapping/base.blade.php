@extends('layout.base')

@section('title')
	<title>Reviews Mapping | Evibe.in Dash</title>
@stop

@section('stylesheets')
	@parent
	<style type="text/css" rel="stylesheet" media="all">
		.review-delivery-images-highlight-wrap {
			padding: 4px 8px;
			border: 1px solid red;
			color: red;
			background: #FDFDFD;
			display: inline-block;
		}
	</style>
@endsection

@section('content')
	<div class="page-content">
		<div class="panel panel-default mar-r-10 mar-l-10">
			<div class="pad-t-10 text-center">
				<ul class="list-inline package-heading">
					<li class="@if(request()->is('reviews-mapping/pending*')) active @endif">
						<a href="{{route('reviews-mapping.pending')}}"> Pending </a>
					</li>
					<li class="@if(request()->is('reviews-mapping/mapped*')) active @endif">
						<a href="{{route('reviews-mapping.mapped')}}">Mapped </a>
					</li>
				</ul>
			</div>
			@yield('list-data')
		</div>
	</div>
@stop