@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="mar-r-10 mar-l-10">
			<div class="app-tabs">
				@include('app/tabs')
			</div>
			<div class="page-content venues-wrap">
				<div class="venues-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="upper text-center">
								<div class="pull-left">
									<h2 class="panel-title">All Reviews</h2>
								</div>
								<div class="text-center in-blk">
									<div class="no-mar mar-t-5 font-16">
										<a href="{{ route("reviews.list", "accepted") }}"
												class="mar-r-20 @if(!request()->is('*reviews/accepted*')) text-muted @else text-success @endif ">Accepted Reviews</a>
										<a href="{{ route("reviews.list", "pending") }}"
												class="mar-r-20 @if(!request()->is('*reviews/pending*')) text-muted @else text-success @endif">Pending Reviews</a>
										<a href="{{ route("reviews.list", "rejected") }}"
												class="@if(!request()->is('*reviews/rejected*')) text-muted @else text-success @endif">Rejected Reviews</a>
									</div>
								</div>
								<div class="pull-right">
									<a href="{{ route("reviews.new") }}"
											class="btn btn-primary btn-add-new-vendor">Add New Review</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="panel-body">
							@if ($errors->count() > 0)
								<div class="errors-cnt alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;
									</button>
									<ul class="erros-list">
										@foreach ($errors->all() as $message)
											<li>{!! $message !!}</li>
										@endforeach
									</ul>
								</div>
							@endif
							@if (Session::has('custom_success'))
								<div class="alert alert-success alert-dismissable">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<span class="">{{ Session::get('custom_success') }}</span>
									<?php Session::forget('custom_success'); ?>
								</div>
							@endif
							<div class="reviews-cnt">
								@if(count($data['reviews']) > 0)
									<table class="table table-condensed">
										<thead>
										<tr>
											<th>Date</th>
											<th>Name</th>
											<th>Area</th>
											<th>Rating</th>
											<th>Review</th>
											<th>Vendor</th>
										</tr>
										</thead>
										<tbody>
										@foreach ($data['reviews'] as $review)
											<tr class="a-review" data-id="{{ $review->id }}"
													data-hasreply="<?php echo count($review->reply) ?>">
												<td style="width:100px;">{{ date('d M y', strtotime($review->created_at)) }}</td>
												<td>{{ $review->name }}</td>
												<td>{{ $review->location }}</td>
												<td>{{ $review->rating }}</td>
												<td class="review-data">{!! $review->review !!}</td>
												<td>@if($review->provider){{ $review->provider->name }} @else -- @endif</td>
												@if(AppUtil::isTabVisible('review_edit') && $review->is_accepted != 1 && $review->is_rejected != 1)
													<td style="width: 140px">
														<a class="btn btn-success btn-xs mar-r-10 act-rej-review-btn"
																data-title="Please enter comments for accepting review"
																data-url="{{ route("reviews.accept.review", $review->id) }}">Accept</a>
														<a class="btn btn-danger btn-xs act-rej-review-btn"
																data-title="Please enter comments for rejecting review"
																data-url="{{ route("reviews.reject.review", $review->id) }}">Reject</a>
													</td>
												@endif
											</tr>
											@if (count($review->reply) > 0)
												<tr>
													<td colspan="1"></td>
													<td colspan="5" class="review-reply">
														<div class="pull-left">{!! $review->reply[0]->reply !!}</div>
														<div class="pull-right">
															<button class="btn btn-xs btn-danger btn-del-review-reply"
																	data-url="{{ route("reviews.delete.partner.review") }}"
																	data-id="{{ $review->reply[0]->id }}">Del
															</button>
														</div>
														<div class="clearfix"></div>
													</td>
												</tr>
											@endif
										@endforeach
										</tbody>
									</table>
									<div class="pad-t-10 text-center">{{ $data['reviews']->links() }}</div>
								@else
									<div class="mar-l-10 mar-r-10 alert-info alert no-mar-b"><i
												class="glyphicon glyphicon-info-sign"></i> No reviews found.
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="modal fade add-reply-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span
								aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Review Reply</h4>
				</div>
				<div class="modal-body">
					<form action="{{ route("reviews.add.partner.review") }}" method="POST">
						<div class="form-group">
							<label for="replyData">Enter Reply</label>
							<textarea name="replyData" id="replyData" rows="5" placeholder="Start typing here..."
									class="form-control"></textarea>
						</div>
						<input type="hidden" value="" id="reviewId" name="reviewId"/>
						<div class="form-group">
							<input type="submit" value="Save Changes" class="btn btn-primary"/>
							<a class="btn btn-default" data-dismiss="modal">Close</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade accept-reject-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span
								aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Review Reply</h4>
				</div>
				<div class="modal-body">
					<form id="acceptRejectReviewForm" action="" method="POST">
						<div class="form-group">
							<label for="acceptRejectComment">Enter Comments</label>
							<textarea name="acceptRejectComment" id="acceptRejectComment" rows="5"
									placeholder="Start typing here...(N/A if not there)"
									class="form-control" required></textarea>
						</div>
						<input type="hidden" value="" id="reviewId" name="reviewId"/>
						<div class="pull-right">
							<a class="btn btn-default" data-dismiss="modal">Close</a>
							<input type="submit" value="Submit" class="btn btn-primary"/>
						</div>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade edit-review-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span
								aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Edit Review</h4>
				</div>
				<div class="modal-body">
					<form action="{{ route("reviews.edit") }}" method="POST">
						<div class="form-group">
							<textarea name="editReviewData" id="editReviewData" rows="5"
									placeholder="Start typing here..." class="form-control"></textarea>
						</div>
						<input type="hidden" value="" id="editReviewId" name="editReviewId"/>
						<div class="form-group">
							<input type="submit" value="Save Changes" class="btn btn-primary"/>
							<a class="btn btn-default" data-dismiss="modal">Close</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {

			$('.a-review').hover(function () {

				var reviewId = $(this).data('id');
				var count = $(this).data('hasreply');
				var optionsMarkup = '<div class="review-options">';

				if (count == 0) {
					optionsMarkup += '<div class="in-blk option"><a data-id="' + reviewId + '" class="btn btn-sm btn-primary btn-review-add-reply">Add Partner Reply</a></div>';
				}

				optionsMarkup += '<div class="in-blk option"><a data-id="' + reviewId + '" class="btn btn-sm btn-info btn-review-edit">Edit Review</a></div>' +
					'<div class="in-blk option"><a href="/reviews/delete/' + reviewId + '" class="btn btn-sm btn-danger btn-review-delete">Delete Review</a></div>' +
					'</div>';

				$(this).append(optionsMarkup);

			}, function () {
				$(this).find('.review-options').remove();
			});

			$('body').on('click', '.btn-review-delete', function () {
				return confirm("Are you sure?", "Yes", "No");
			});

			$('body').on('click', '.btn-review-edit', function (e) {

				e.preventDefault();

				var reviewId = $(this).data('id');
				var reviewData = $(this).closest('.a-review').find('.review-data').text();
				$('#editReviewId').val(reviewId);
				$('#editReviewData').val(reviewData);
				$('.edit-review-modal').modal();

			});

			$('body').on('click', '.btn-review-add-reply', function () {

				var reviewId = $(this).data('id');
				$('#reviewId').val(reviewId);
				$('.add-reply-modal').modal();

			});

			$('.act-rej-review-btn').on('click', function () {

				var url = $(this).data('url');
				var title = $(this).data("title");
				$('#acceptRejectReviewForm').attr('action', url);
				$('.accept-reject-modal').find('.modal-title').text(title);
				$('.accept-reject-modal').modal();
			});

			$('.btn-del-review-reply').click(function (e) {

				e.preventDefault();
				var replyId = $(this).data('id');

				if (confirm("Are you sure?", "Yes", "No")) {
					$.ajax({
						url: $(this).data("url"),
						type: 'POST',
						dataType: 'json',
						data: {
							replyId: replyId
						},
					})
						.done(function (data) {
							if (data.success == true) location.reload();
							else alert('An error occured. Could not delete review reply');
						})
						.fail(function () {
							alert('Error! Could not delete review reply');
						});
				}
			})

		});

	</script>
@stop