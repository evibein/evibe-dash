@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
			<div class="app-tabs">
				@include('app/tabs')
			</div>
			<div class="page-content venues-wrap">
				<div class="venues-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="upper">
								<div class="pull-left">
									<h2 class="panel-title">Add New Review</h2>
								</div>
								<div class="pull-right">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="panel-body">
							<div class="add-review-cnt">
								@if ($errors->count() > 0)
									<div class="errors-cnt alert alert-danger alert-dismissable">
										<button type="button" class="close" data-dismiss="alert"
												aria-hidden="true">&times;
										</button>
										<ul class="erros-list">
											@foreach ($errors->all() as $message)
												<li>{!! $message !!}</li>
											@endforeach
										</ul>
									</div>
								@endif
								<form action="{{ route("reviews.new.save") }}" method="POST" role="form">
									<div>
										<div class="col-md-6 col-lg-6">
											<div class="form-group">
												<label>Customer Name</label>
												<input id="clientName" name="clientName" type="text"
														class="form-control"
														placeholder="Enter customer name"
														value="{{ Input::old('clientName') }}">
											</div>
											<div class="form-group">
												<label>Party Location</label>
												<input id="location" name="location" type="text" class="form-control"
														placeholder="Enter party location"
														value="{{ Input::old('location') }}">
											</div>
											<div class="form-group">
												<label>Party Date</label>
												<input id="reviewPartyDate" name="reviewPartyDate" type="text"
														class="form-control"
														placeholder="Enter party date"
														value="{{ Input::old('reviewPartyDate') }}">
											</div>
										</div>
										<div class="col-md-6 col-lg-6">
											<div class="form-group">
												<label>Vendor</label>
												<select id="vendor" name="vendor" class="form-control">
													@foreach($data['vendors'] as $vendor)
														<option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label>Rating</label>
												<input id="rating" name="rating" type="text" class="form-control"
														placeholder="Enter rating" value="{{ Input::old('rating') }}">
											</div>
											<div class="form-group">
												<label>Review</label>
												<textarea id="reviewData" name="reviewData" class="form-control"
														placeholder="Write review">{{ Input::old('reviewData') }}</textarea>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-md-12 col-lg-12">
											<div class="form-group">
												<input type="submit" value="Submit" class="btn btn-primary"/>
												<a class="pad-l-10"
														href="{{ route("reviews.list", "accepted") }}">Cancel</a>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			$('#reviewPartyDate').datepicker({
				'changeMonth': true,
				'changeYear': true
			});
		});
	</script>
@stop