<div id="acceptImageModal" class="modal accept-image-modal">
	<!-- Modal content -->
	<div class="modal-content text-center">
		<div class="container text-center">
			<div class="alert alert-warning">
				<i class="glyphicon glyphicon-warning-sign"></i> Select Custom Category
			</div>
		</div>
		<hr class="no-mar-t">
		<div class=" form-group filter-group pad-l-10">
			<div class="filter-title">By Category</div>
			<select class="form-control accept-map-type-id">
				<option value="0" @if(!request("typeTicketId"))selected @endif>--Select Category--</option>
				@foreach($data['typeTicket'] as $type)
					<option value="{{ $type->id }}">{{ $type->name }}
					</option>
				@endforeach
			</select>
		</div>
		<div class=" form-group filter-group pad-l-10">
			<div class="filter-title">By Sub-Category</div>
			<select class="form-control accept-sub-category">
				<option value="0" selected>--Select Sub-Category--</option>
			</select>
		</div>
		<div class=" form-group filter-group accept-by-code pad-l-10 hide">
			<div class="filter-title">By Product</div>
			<div class="category-id">
				<select class="default-select form-control text-left">
					<option value="0" selected>--Please Select any Category--</option>
				</select>
			</div>
		</div>
		<div class="accept-image-rotate-accept hide">
			<div class="text-warning">All the angles will work as anti-clockwise</div>
			<label class="mar-r-10 hide"><input type="radio" name="acceptImageRotateAccept" value="0" checked>Default</label>
			<label class="mar-r-10"><input type="radio" name="acceptImageRotateAccept" value="90">90</label>
			<label class="mar-r-10"><input type="radio" name="acceptImageRotateAccept" value="180">180</label>
			<label class="mar-r-10"><input type="radio" name="acceptImageRotateAccept" value="270">270</label>
		</div>
		<label><input type="checkbox" name="wantImageSelectByCode">Select By Code</label>
		<label><input type="checkbox" name="wantImageRotationAccept">Rotate Image</label>
		<div class="clearfix"></div>
		<div class="text-center btn btn-success accept-custom-mapping">
			Submit
		</div>

	</div>
</div>