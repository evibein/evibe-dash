@extends('layout.base')
@section('content')
	@include('delivery-image.accept_modal')
	@include('delivery-image.add_to_product_modal')
	<div class="container text-center">
		<h3 class="in-blk mar-t-5">Delivery Images</h3>
	</div>
	<div class="text-center">
		<span class="btn btn-default @if(request('typeImages')==0 || !request('typeImages')) active @endif" id="new" data-image-status="0">Uncategorized</span>
		<span class="btn btn-default @if(request('typeImages')==1) active @endif" id="selected" data-image-status="1">Selected</span>
		<span class="btn btn-default @if(request('typeImages')==2) active @endif" id="rejected" data-image-status="2">Rejected</span>
		<span class="btn btn-default @if(request('typeImages')==3) active @endif" id="product" data-image-status="3">Added To Product</span>
	</div>
	<hr>
	<div class="text-center col-lg-3 col-md-3 col-sm-3 col-xs-3 no-pad-r">
		<div class="filter-group no-mar-t">
			<h4 class="pull-left mar-l-10">Filter Options:</h4>
			<div class="clearfix"></div>
			<div class=" form-group filter-group pad-l-10">
				<div class="filter-title">By Category</div>
				<select class="form-control delivery-image-categories">
					<option value="0" @if(!request("typeTicketId"))selected @endif>--Select Category--</option>
					@foreach($data['typeTicket'] as $type)
						<option value="{{ $type->id }}"
								@if($type->id == request("typeTicketId")) selected @endif>{{ $type->name }}
						</option>
					@endforeach
				</select>
			</div>
			<div class="form-group filter-group pad-l-10 filter-group">
				<div class="filter-title">By Event</div>
				<select class="form-control delivery-image-events">
					<option value="0" @if(!request("eventId"))selected @endif>--Select Occasion--</option>
					@foreach($data['typeEvent'] as $event)
						<option value="{{ $event->id }}"
								@if($event->id == request("eventId")) selected @endif>{{ $event->name }}</option>
					@endforeach
				</select>
			</div>
			<div class=" form-group filter-group pad-l-10">
				<div class="filter-title">By City</div>
				<select class="form-control delivery-image-cities">
					<option value="0" @if(!request("cityId"))selected @endif>--Select City--</option>
					@foreach($data['cities'] as $city)
						<option value="{{ $city->id }}"
								@if($city->id == request("cityId")) selected @endif>{{ $city->name }}
						</option>
					@endforeach
				</select>
			</div>
			<div class=" form-group filter-group pad-l-10">
				<div class="filter-title">By Price</div>
				<select class="form-control delivery-image-price">
					<option value="" selected>--Select Price--</option>
					@foreach (config('evibe.price_filter') as $value=>$price)
						@if($price['max']==0)
							<option value="{{$value}}" @if(request('priceRange')==$value) selected @endif> >{{$price['min']}}</option>
						@else
							<option value="{{$value}}" @if(request('priceRange')==$value) selected @endif>{{$price['min']}}-{{$price['max']}}</option>
						@endif
					@endforeach
				</select>
			</div>
			@if(!(request('typeImages')==4))
				<div class=" form-group filter-group pad-l-10">
					<div class="filter-title">By Planner</div>
					<select class="form-control delivery-image-planner text-left">
						<option value="0" @if(!request("planner"))selected @endif>--Select Planner--</option>
						@foreach($data['plannerData'] as $planner)
							<option value="{{ $planner->planner_id }}"
									@if($planner->planner_id == request("planner")) selected @endif>{{ $planner->planner_code }}</option>
						@endforeach
					</select>
				</div>
			@endif
		</div>
		@php
			$typePage = request('typeImages') ? request('typeImages') : 0;
		@endphp
		<div class="mar-t-10">
			<a class="btn btn-warning font-16" href="{{config('evibe.host')}}/delivery-images?typeImages={{$typePage}}">Reset Filters</a>
		</div>
	</div>
	@if(isset($data['imageUrl']))
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 ">

			@foreach($data['imageUrl'] as $imageUrl)
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mar-b-15 delivery-image-{{$imageUrl->id}}">
					<div>
						<div style="height: 300px">
							<img src="{{config('evibe.gallery.host')}}/ticket/{{$imageUrl->ticket_id}}/{{$imageUrl->ticket_booking_id}}/partner/{{$imageUrl->url}}" style="width: 100%;height: 100%">
						</div>
						<div class="mar-t-5">
							<div class="btn btn-success mar-b-15  imageAccept" data-image-id="{{$imageUrl->id}}" data-ticket-id="{{$imageUrl->ticket_id}}" data-booking-id="{{$imageUrl->ticket_booking_id}}" data-map-type-id="{{$imageUrl->map_type_id}}" data-map-id="{{$imageUrl->map_id}}" data-image-title="{{$imageUrl->title}} ">
								<i class="glyphicon glyphicon-thumbs-up no-pad-t no-pad"></i></div>
							@if(!(request('typeImages')==2))
								<div class="btn btn-danger mar-b-15 imageReject" data-image-id="{{$imageUrl->id}}">
									<i class="glyphicon glyphicon-thumbs-down"></i></div>
							@endif
							@if(!($imageUrl->map_type_id == config('evibe.ticket_type.planners')))
								<div class="btn btn-warning mar-b-15 imageSetAsProfile" data-image-url="{{$imageUrl->url}}" data-image-id="{{$imageUrl->id}}" data-ticket-id="{{$imageUrl->ticket_id}}" data-booking-id="{{$imageUrl->ticket_booking_id}}" data-map-type-id="{{$imageUrl->map_type_id}}" data-map-id="{{$imageUrl->map_id}}" data-image-title="{{$imageUrl->title}}">Add To Product</div>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<div class="clearfix"></div>
		<div class="text-center">
			{{ $data['imageUrl']->appends(Input::except('page'))->links() }}
		</div>
	@endif
	@if(isset($data['selectedImage']))
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 ">
			@foreach($data['selectedImage'] as $selectedImage)

				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mar-b-15 selected-delivery-image-{{$selectedImage['id']}}">
					<div>
						<div style="height: 300px">
							<img src="{{config('evibe.gallery.host')}}/live-partner-images/{{$selectedImage['url']}}" style="width: 100%;height: 100%">
						</div>
						<div class="mar-t-5">
							@if (request('typeImages')==3)
								<div>{{$selectedImage['code']}}</div>
							@endif
							@if(request('typeImages')==1)
								<div class="btn btn-danger mar-b-15 selected-image-reject" data-image-id="{{$selectedImage['id']}}">
									<i class="glyphicon glyphicon-thumbs-down"></i></div>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<div class="clearfix"></div>
	@endif
@endsection
@section('javascript')
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var $imageAccept;
			var $imageSetProfile;
			$('.delivery-image-planner').selectize({
				onChange: function (val) {
					if (val.length) {
						var searchParams = new URLSearchParams(window.location.search);
						var param = searchParams.get('typeImages') === null ? "0" : searchParams.get('typeImages');
						var url = $(location).attr('protocol') + "//" + $(location).attr('hostname') + $(location).attr('pathname')
							+ "?typeImages=" + param
							+ "&typeTicketId=" + $('.delivery-image-categories').val()
							+ "&eventId=" + $('.delivery-image-events').val()
							+ "&cityId=" + $('.delivery-image-cities').val()
							+ "&planner=" + $('.delivery-image-planner').val()
							+ "&priceRange=" + $('.delivery-image-price').val();
						window.location.replace(url);
					}
				}
			});

			$('.delivery-image-events, .delivery-image-categories, .delivery-image-cities, .delivery-image-price').on('change', function () {
				var searchParams = new URLSearchParams(window.location.search);
				var param = searchParams.get('typeImages') === null ? "0" : searchParams.get('typeImages');
				var url = $(location).attr('protocol') + "//" + $(location).attr('hostname') + $(location).attr('pathname')
					+ "?typeImages=" + param
					+ "&typeTicketId=" + $('.delivery-image-categories').val()
					+ "&eventId=" + $('.delivery-image-events').val()
					+ "&cityId=" + $('.delivery-image-cities').val()
					+ "&priceRange=" + $('.delivery-image-price').val();
				window.location.replace(url);
			});

			$('#new,#selected,#rejected,#product').on('click', function () {
				var url = $(location).attr('protocol') + "//" + $(location).attr('hostname') + $(location).attr('pathname')
					+ "?typeImages=" + $(this).data('image-status');
				window.location.replace(url);
			});

			$('.default-select').selectize();

			$('.default-select-profile').selectize();

			$('.accept-map-type-id').on('change', function () {
				var options = "";
				var subCategories = "";
				var mapTypeId = $('.accept-map-type-id').val();
				$.ajax({
					url: "delivery-images/populate-map-id",
					type: 'post',
					data: {'mapTypeId': mapTypeId},
					success: function (data) {
						if (data.success) {
							subCategories = subCategories + '<option value="" selected >--Select Sub-Category--</option>';
							$.each(data.subCategory, function (i, item) {
								subCategories = subCategories + '<option value=' + i + '>' + item + '</option>';
							});
							$('.accept-sub-category').html(subCategories);
							options = options + '<select  class="text-left accept-map-id form-control">' +
								'<option value="0" selected>-- select id --</option>';
							$.each(data.data, function (i, item) {
								options = options + '<option value=' + item.id + '>' + item.code + '-' + item.name + '-' + item.price + '</option>';
							});
							options = options + '</select>';

							$('.category-id').html(options);
							$('.accept-map-id').selectize();

						}
						else {
							window.showNotyError(data.error);
						}
					},
					error: function () {
						window.showNotyError('Some Error in processing the request');
					}
				})
			});

			$('.accept-map-type-id-profile').on('change', function () {
				var options = "";
				var mapTypeId = $('.accept-map-type-id-profile').val();
				$.ajax({
					url: "delivery-images/populate-map-id",
					type: 'post',
					data: {'mapTypeId': mapTypeId},
					success: function (data) {
						if (data.success) {
							options = options + '<select  class="text-left accept-map-id-profile form-control">' +
								'<option value="0" selected>-- select id --</option>';
							$.each(data.data, function (i, item) {
								options = options + '<option value=' + item.id + '>' + item.code + '-' + item.name + '-' + item.price + '</option>';
							});
							options = options + '</select>';

							$('.category-id-profile').html(options);
							$('.accept-map-id-profile').selectize();

						}
						else {
							window.showNotyError(data.error);
						}
					},
					error: function () {
						window.showNotyError('Some Error in processing the request');
					}
				})
			});

			$('.imageReject').on('click', function () {
				if (confirm("Are you sure,you want to reject this image?")) {
					window.showLoading();
					var imageId = $(this).data('image-id');
					var data = {'imageId': imageId};
					$.ajax({
						url: "/delivery-images/reject-image",
						type: 'post',
						data: data,
						success: function (data) {
							window.hideLoading();
							if (data.success) {
								showNotySuccess("Image Successfully Rejected");
								$('.delivery-image-' + imageId).remove();
							}
							else if (data.error) {
								showNotySuccess(data.error);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							showNotyError('Error occurred while rejecting image, Please try again.')
						}
					});
				}
			});

			$('.imageAccept').on('click', function () {
				$('#acceptImageModal').modal('show');
				$imageAccept = $(this);
			});

			$('.imageSetAsProfile').on('click', function () {
				$('#addToProductImageModal').modal('show');
				$imageSetProfile = $(this);
			});

			$('.selected-image-reject').on('click', function () {
				if (confirm("Are you sure,you want to reject this image?")) {
					window.showLoading();
					var imageId = $(this).data('image-id');
					$.ajax({
						url: "delivery-images/reject-select-image",
						type: 'post',
						data: {'imageId': imageId},
						success: function (data) {
							window.hideLoading();
							if (data.success) {
								$('.selected-delivery-image-' + imageId).remove();
								window.showNotySuccess("Successfully Rejected the image");
							}
							else {
								window.showNotyError("Some Error In Processing the request");
							}

						},
						error: function () {
							window.showNotyError("Some Error In Processing the request");
						}
					})
				}
			});

			$('input[name = wantImageSelectByCode]').on('click', function () {
				if ($('input[name = wantImageSelectByCode]').prop('checked')) {
					$('.accept-by-code').removeClass('hide');
				}
				else {
					$('.accept-by-code').addClass('hide');
				}
			});

			$('input[name = wantImageRotationAccept]').on('click', function () {
				if ($('input[name = wantImageRotationAccept]').prop('checked')) {
					$('.accept-image-rotate-accept').removeClass('hide');
				}
				else {
					$('.accept-image-rotate-accept').addClass('hide');
				}
			});

			$('input[name = wantImageRotationProductAccept]').on('click', function () {
				if ($('input[name = wantImageRotationProductAccept]').prop('checked')) {
					$('.product-image-rotate-accept').removeClass('hide');
				}
				else {
					$('.product-image-rotate-accept').addClass('hide');
				}
			});

			$('.accept-custom-mapping').on('click', function () {
				$('.accept-image-modal').modal('hide');
				if (confirm("Are you sure,you want to accept this image?")) {
					$('#acceptImageModal').modal('hide');
					window.showLoading();
					var angle = $('input[name = acceptImageRotateAccept]:checked').val();
					var mapTypeId = $('.accept-map-type-id').val();
					var imageId = $imageAccept.data('image-id');
					var mapId = $('.accept-map-id').val();
					var ticketId = $imageAccept.data('ticket-id');
					var bookingId = $imageAccept.data('booking-id');
					var imageUrl = $(this).data('image-url');
					var imageTitle = $imageAccept.data('image-title');
					var subCategory = $('.accept-sub-category').val();
					var sendData = {
						'imageId': imageId,
						'ticketId': ticketId,
						'bookingId': bookingId,
						'mapId': mapId,
						'mapTypeId': mapTypeId,
						'imageTitle': imageTitle,
						'imageUrl': imageUrl,
						'imageRotate': angle,
						'subCategory': subCategory
					};
					$.ajax({
						url: "/delivery-images/accept-image",
						type: 'post',
						data: sendData,
						success: function (data) {
							window.hideLoading();
							if (data.success) {
								$('.delivery-image-' + imageId).remove();
								showNotySuccess("Image Successfully Accepted");
							}
							else if (data.error) {
								$('#acceptImageModal').modal('show');
								showNotyError(data.error);
							}
						},
						error: function () {
							$('#acceptImageModal').modal('show');
							showNotyError('Error occurred while accepting, Please try again.')
						}
					});
				}
			});

			$('.accept-custom-mapping-profile').on('click', function () {
				if (confirm("Are you sure,you want to accept this image?")) {
					$('#addToProductImageModal').modal('hide');
					window.showLoading();
					var angle = $('input[name = productImageRotateAccept]:checked').val();
					var imageId = $imageSetProfile.data('image-id');
					var mapTypeId = $('.accept-map-type-id-profile').val();
					var mapId = $('.accept-map-id-profile').val();
					var ticketId = $imageSetProfile.data('ticket-id');
					var bookingId = $imageSetProfile.data('booking-id');
					var imageUrl = $imageSetProfile.data('image-url');
					var imageTitle = $imageSetProfile.data('image-title');
					var data = {
						'mapTypeId': mapTypeId,
						'mapId': mapId,
						'imageUrl': imageUrl,
						'imageTitle': imageTitle,
						'imageId': imageId,
						'bookingId': bookingId,
						'ticketId': ticketId,
						'imageRotate': angle
					};
					$.ajax({
						url: "/delivery-images/set-profile-image",
						type: 'post',
						data: data,
						success: function (data) {
							window.hideLoading();
							if (data.success) {
								showNotySuccess("Image Successfully Accepted as Profile");
								$('.delivery-image-' + imageId).remove();
							}
							else if (data.error) {
								$('#addToProductImageModal').modal('show');
								showNotyError(data.error);
							}

						},
						error: function () {
							$('#addToProductImageModal').modal('show');
							showNotyError('Error occurred while setting as profile, Please try again.')
						}
					});
				}
			});

			$('input[name = wantImageRotationProductSkip]').on('click', function () {
				if ($('input[name = wantImageRotationProductSkip]').prop('checked')) {
					$('.product-image-rotate-skip').removeClass('hide');
				}
				else {
					$('.product-image-rotate-skip').addClass('hide');
				}
			});

			/*$('input[name = wantImageRotationSkip]').on('click',function () {
							if($('input[name = wantImageRotationSkip]').prop('checked'))
							{
								$('.accept-image-rotate-skip').removeClass('hide');
							}
							else {
								$('.accept-image-rotate-skip').addClass('hide');
							}
						});*/

			$('.accept-skip-image-profile').on('click', function () {
				if (confirm("Are you sure,you want to add this image to profile?")) {
					$('#addToProductImageModal').modal('hide');
					window.showLoading();
					var angle = $('input[name = productImageRotateSkip]:checked').val();
					var imageId = $imageSetProfile.data('image-id');
					var mapTypeId = $imageSetProfile.data('map-type-id');
					var mapId = $imageSetProfile.data('map-id');
					var ticketId = $imageSetProfile.data('ticket-id');
					var bookingId = $imageSetProfile.data('booking-id');
					var imageUrl = $imageSetProfile.data('image-url');
					var imageTitle = $imageSetProfile.data('image-title');
					var data = {
						'mapTypeId': mapTypeId,
						'mapId': mapId,
						'imageUrl': imageUrl,
						'imageTitle': imageTitle,
						'imageId': imageId,
						'bookingId': bookingId,
						'ticketId': ticketId,
						'imageRotate': angle
					};
					$.ajax({
						url: "/delivery-images/set-profile-image",
						type: 'post',
						data: data,
						success: function (data) {
							window.hideLoading();
							if (data.success) {
								showNotySuccess("Image Successfully Accepted as Profile");
								$('.delivery-image-' + imageId).remove();
							}
							else if (data.error) {
								$('#addToProductImageModal').modal('show');
								showNotyError(data.error);
							}

						},
						error: function () {
							$('#addToProductImageModal').modal('show');
							showNotyError('Error occurred while setting as profile, Please try again.')
						}
					});
				}
			});

			/*$('.accept-skip-image').on('click', function () {
				if (confirm("Are you sure,you want to accept this image?")) {
					$('#acceptImageModal').modal('hide');
					window.showLoading();
					var angle = $('input[name = acceptImageRotateSkip]:checked').val();
					var imageId = $imageAccept.data('image-id');
					var mapTypeId = $imageAccept.data('map-type-id');
					var mapId = $imageAccept.data('map-id');
					var ticketId = $imageAccept.data('ticket-id');
					var bookingId = $imageAccept.data('booking-id');
					var imageTitle = $imageAccept.data('image-title');
					var sendData = {
						'imageId': imageId,
						'ticketId': ticketId,
						'bookingId': bookingId,
						'mapId': mapId,
						'mapTypeId': mapTypeId,
						'imageTitle': imageTitle,
						'imageRotate': angle
					};
					$.ajax({
						url: "/delivery-images/accept-image",
						type: 'post',
						data: sendData,
						success: function (data) {
							window.hideLoading();
							if (data.success) {
								$('.delivery-image-' + imageId).remove();
								showNotySuccess("Image Successfully Accepted");
							}
							else if (data.error) {
								$('#acceptImageModal').modal('show');
								showNotySuccess(data.error);
							}
						},
						error: function () {
							$('#acceptImageModal').modal('show');
							showNotyError('Error occurred while accepting, Please try again.')
						}
					});
				}
			});*/
		});
	</script>
@endsection