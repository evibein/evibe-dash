<div id="addToProductImageModal" class="modal accept-image-modal">
	<!-- Modal content -->
	<div class="modal-content text-center">
		<div class="container text-center">
			<div class="alert alert-warning">
				<i class="glyphicon glyphicon-warning-sign"></i> Select Custom Category
			</div>
		</div>
		<hr class="no-mar-t">
		<div class=" form-group filter-group pad-l-10">
			<div class="filter-title">By Category</div>
			<select class="form-control accept-map-type-id-profile">
				<option value="0" @if(!request("typeTicketId"))selected @endif>--Select Category--</option>
				@foreach($data['typeTicket'] as $type)
					<option value="{{ $type->id }}">{{ $type->name }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="hide populate-data"></div>
		<div class=" form-group filter-group pad-l-10">
			<div class="filter-title">By Product</div>
			<div class="category-id-profile">
				<select class="default-select-profile form-control text-left">
					<option value="0" selected>--Please Select any Category--</option>
				</select>
			</div>
		</div>
		<label><input type="checkbox" name="wantImageRotationProductAccept">Rotate Image</label>
		<div class="product-image-rotate-accept hide">
			<div class="text-warning">All the angles will work as anti-clockwise</div>
			<label class="mar-r-10 hide"><input type="radio" name="productImageRotateAccept" value="0" checked>Default</label>
			<label class="mar-r-10"><input type="radio" name="productImageRotateAccept" value="90">90</label>
			<label class="mar-r-10"><input type="radio" name="productImageRotateAccept" value="180">180</label>
			<label class="mar-r-10"><input type="radio" name="productImageRotateAccept" value="270">270</label>
		</div>
		<div class="clearfix"></div>
		<div class="text-center btn btn-success accept-custom-mapping-profile">
			Submit
		</div>
		<hr class="hr-text no-mar" data-content="OR">
		<label><input type="checkbox" name="wantImageRotationProductSkip">Rotate Image</label>
		<div class="product-image-rotate-skip hide">
			<div class="text-warning">All the angles will work as anti-clockwise</div>
			<label class="mar-r-10 hide"><input type="radio" name="productImageRotateSkip" value="0" checked>Default</label>
			<label class="mar-r-10"><input type="radio" name="productImageRotateSkip" value="90">90</label>
			<label class="mar-r-10"><input type="radio" name="productImageRotateSkip" value="180">180</label>
			<label class="mar-r-10"><input type="radio" name="productImageRotateSkip" value="270">270</label>
		</div>
		<div class="clearfix"></div>
		<div class="text-center btn btn-warning accept-skip-image-profile">
			Add as Default
		</div>
	</div>
</div>