@extends("add-ons.base")

@section("inner-content")

	@if(isset($data['addOns']) && $data['addOns'])
		<div class="pad-l-15">
			<div>
				<div class="text-left font-20 in-blk mar-t-5">
					List of all the add-ons<span class="mar-l-5 text-warning font-14 text-italic">(Add-on creation, edit and deletion have been disabled for now!)</span>
				</div>
				<div class="in-blk pull-right">
					<div class="btn btn-primary mar-r-15 disabled">
						<i class="glyphicon glyphicon-plus"></i> Create Add-On
					</div>
				</div>
			</div>
			<div class="mar-t-10 mar-b-30">
				@foreach($data['addOns'] as $addOn)
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 no-pad-l mar-t-15">
						<div class="a-add-on">
							<div class="top">
								<a href="{{ route('add-ons.info', $addOn->id) }}" style="pointer-events: none">
									<img class="profile-img" src="{{ $addOn->getProfilePic() }}">
								</a>
							</div>
							<div class="pad-10 text-center">
								<div class="title">
									<a href="{{ route('add-ons.info', $addOn->id) }}" style="pointer-events: none"
											title="{{ $addOn->name }}">
										{{ $addOn->name }}
									</a>
								</div>

								<div class="service-action-button mar-t-10">
									<a disabled href="{{route('add-ons.info',$addOn->id)}}"
											class="btn btn-xs btn-primary btn-pck">
										<i class="glyphicon glyphicon-pencil"></i>
										<span>Edit</span>
									</a>
									@if(AppUtil::isAdmin())
										<a disabled href="{{route('collection.delete',$addOn->id)}}"
												class="btn btn-xs btn-danger"
												onclick="return confirm('Are you sure?', 'Yes', 'No');">
											<i class="glyphicon glyphicon-trash"></i>
											<span>Delete</span>
										</a>
									@endif
								</div>
							</div>
						</div>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		</div>
	@else
		<div class="text-center error-box error-message">
			There are no add-ons
		</div>
	@endif

@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

		});
	</script>
@endsection