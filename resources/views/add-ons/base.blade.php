@extends("layout.base")

@section("content")

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 no-pad">
			<a href="{{ route('add-ons.list') }}"
					class="btn btn-default mar-l-15 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.add-on.details'))) btn-warning @endif">
				Add-On Options</a>
			<a href="{{ route('add-ons.option-mappings.map') }}"
					class="btn btn-default mar-l-15 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.add-on.option-mappings'))) btn-warning @endif">
				Option mappings</a>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="pull-right">
				<button class="btn btn-link" data-toggle="modal" data-target="">
					<i class="glyphicon glyphicon-info-sign"></i> Help
				</button>
			</div>
			<input type="hidden" id="hidPlannerType" value="{{ config('evibe.ticket_type.planners') }}">
			<input type="hidden" id="hidVenueType" value="{{ config('evibe.ticket_type.venues') }}">
			<input type="hidden" id="optionAddOnsToggleUrl" value="{{ route('add-ons.option-mappings.toggle') }}">
		</div>
		<div class="clearfix"></div>
		<hr>
		@yield("inner-content")
	</div>

@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {



		});
	</script>
@endsection