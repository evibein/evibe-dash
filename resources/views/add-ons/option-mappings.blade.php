@extends("add-ons.base")

@section("inner-content")

	@if(isset($data['cities']) && $data['cities'] && isset($data['mapTypes']) && count($data['mapTypes']))
		<div>
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<label for="city">City</label>
				<select name="city" id="city" class="form-control">
					@foreach($data['cities'] as $city)
						<option value="{{ $city->id }}">{{ $city->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<label for="mapType">Ticket Type</label>
				<select name="mapType" id="mapType" class="form-control">
					<option value="-1">--Select Option--</option>
					@foreach($data['mapTypes'] as $mapType)
						<option value="{{ $mapType->id }}">{{ $mapType->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-xs-5 col-sm-5 col-lg-5 col-md-5">
				<label for="items">Option</label>
				<select name="items" id="items" class="form-control"></select>
			</div>
			<input type="hidden" id="hidFetchOptionsUrl" value="{{ route('add-ons.option-mappings.options') }}">
			<input type="hidden" id="selectedMapTypeId" value="{{ $data['mapTypeId'] }}">
			<input type="hidden" id="selectedMapId" value="{{ $data['mapId'] }}">
			<div class="clearfix"></div>
		</div>

		@if(isset($data['optionData']) && $data['optionData'])
			<div class="ao-option-container mar-t-20 mar-b-20">
				<div class="ao-option-data mar-t-20">
					<div class="col-sm-4 col-md-4 col-lg-4 text-center">
						<div class="ao-option-img-wrap">
							<img src="{{ $data['optionData']['profileImgUrl'] }}" class="ao-option-img">
						</div>
					</div>
					<div class="col-sm-8 col-md-8 col-lg-8">
						<div class="font-22">
							<span class="ao-option-code">
								@if(isset($data['optionData']['code']) && $data['optionData']['code'])
									[{{ $data['optionData']['code'] }}]
								@else
									--
								@endif
							</span>
							<span class="ao-option-title mar-l-5">
								@if(isset($data['optionData']['name']) && $data['optionData']['name'])
									{{ $data['optionData']['name'] }}
								@else
									--
								@endif
							</span>
						</div>
						<div class="font-20 mar-t-5">
							@if(isset($data['optionData']['price']) && $data['optionData']['price'])
								@if(isset($data['optionData']['priceWorth']) && $data['optionData']['priceWorth'])
									<span class="ao-option-price">Rs. {{ $data['optionData']['price'] }}</span>
									<span class="ao-option-price-worth mar-l-5">Rs. {{ $data['optionData']['priceWorth'] }}</span>
								@else
									<span class="ao-option-price">Rs. {{ $data['optionData']['price'] }}</span>
								@endif
							@else
								--
							@endif
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr>
				<div class="ao-option-add-ons">
					@if(isset($data['optionData']['addOns']) && count($data['optionData']['addOns']))
						<div class="pad-l-15">
							@foreach($data['optionData']['addOns'] as $addOn)
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l mar-b-20">
									<div class="ao-add-on-wrap @if($addOn->is_enable) ao-add-on-wrap-enabled @endif">
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-pad-l">
											<div class="enabled-label @if($addOn->is_enable) @else hide @endif">Enabled</div>
											<div class="ao-add-on-img-wrap">
												<img src="{{ $addOn->getProfilePic() }}" class="ao-add-on-img">
											</div>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-pad-r">
											<div class="ao-add-on-title mar-t-5">{{ $addOn->name }}</div>
											<div class="ao-add-on-price mar-t-5">Rs. {{ $addOn->price_per_unit }}</div>
											<div class="ao-add-on-cta mar-t-10" data-id="{{ $addOn->id }}" data-name="{{ $addOn->name }}">
												<a href="" class="ao-add-on-btn-enable @if($addOn->is_enable) hide @else @endif"><i class="glyphicon glyphicon-plus"></i> Enable</a>
												<a href="" class="ao-add-on-btn-disable @if($addOn->is_enable) @else hide @endif"><i class="glyphicon glyphicon-minus"></i> Disable</a>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							@endforeach
							<div class="clearfix"></div>
						</div>
					@else
						<div class="text-center">
							There are no add-ons available for this option
						</div>
					@endif
				</div>
			</div>
		@endif

	@else
		<div class="text-center text-red font-20">
			Uhu! Something is not right. Please refresh the page and try again
		</div>
	@endif

@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			function fetchOptions() {
				var selectMapType, $selectMapType;
				var selectItem, $selectItem;

				$selectMapType = $('#mapType').selectize({
					onChange: mappingTypeOnChange
				});

				$selectItem = $('#items').selectize({
					valueField: 'id',
					labelField: 'label',
					searchField: ['label', 'name', 'code'],
					render: {
						option: function (item, escape) {
							return '<div>' +
								'<div><b>' + escape(item.label) + '</b></div>' +
								'<div class="font-12">' + escape(item.name) + '</div>' +
								'</div>';
						}
					}
				});

				selectItem = $selectItem[0].selectize;
				selectMapType = $selectMapType[0].selectize;
				selectMapType.setValue('-1');

				selectItem.disable();
				selectItem.clearOptions();

				// disable by default (as selected mappingType is -1)

				function mappingTypeOnChange(value) {
					var results = [];

					selectItem.disable();
					selectItem.clearOptions();
					if (!value.length || value == -1) {
						return;
					}

					// make an Ajax request to get the data

					$.ajax({
						type: 'GET',
						url: $('#hidFetchOptionsUrl').val(),
						dataType: 'JSON',
						async: false,
						data: {
							'type': $('#mapType').val(),
							'city': $('#city').val()
						},
						success: function (data) {

							if (data.success) {

								$.each(data.mapData, function (key, value) {
									results.push({
										id: value.id,
										label: value.code,
										name: value.name
									});
								});
							} else {
								window.showNotyError('Some error occurred while fetching the data')
							}
						},
						fail: function () {
							window.showNotyError("Some unexpected error occurred, refresh the page and try again, if error persist report tech team")
						}
					});

					selectItem.enable();
					selectItem.addOption(results);
					selectItem.refreshOptions();

				}
			}

			$('#items').on('change', function () {
				var $currentUrl = window.location.href;
				$currentUrl = $currentUrl.split('?')[0];

				var $cityId = $('#city').val();
				var $mapTypeId = $('#mapType').val();
				var $mapId = $('#items').val();
				if (($mapId > 0) && ($mapTypeId > 0)) {
					window.location.href = $currentUrl + '?cityId=' + $cityId + '&mapTypeId=' + $mapTypeId + '&mapId=' + $mapId;
				}
			});

			$('.ao-add-on-btn-enable').on('click', function (event) {
				event.preventDefault();

				/*  make necessary css here */
				$(this).parent().parent().parent().addClass('ao-add-on-wrap-enabled');
				$(this).parent().parent().parent().find('.enabled-label').removeClass('hide');
				$(this).parent().children('.ao-add-on-btn-disable').removeClass('hide');
				$(this).addClass('hide');

				$.ajax({
					type: 'POST',
					url: $('#optionAddOnsToggleUrl').val(),
					dataType: 'JSON',
					async: false,
					data: {
						'addOnId': $(this).parent().data('id'),
						'mapId': $('#selectedMapId').val(),
						'mapTypeId': $('#selectedMapTypeId').val(),
						'isEnable': 1
					},
					success: function (data) {
						if (data.success) {
							/* do nothing */
						} else {
							/* revert back css */
							$(this).parent().parent().parent().removeClass('ao-add-on-wrap-enabled');
							$(this).parent().parent().parent().find('.enabled-label').addClass('hide');
							$(this).parent().children('.ao-add-on-btn-disable').addClass('hide');
							$(this).removeClass('hide');
							/* show error msg clearly */
							window.showNotyError("unable to enable add-on: " + $(this).parent().data('name'));
						}
					},
					fail: function () {
						/* revert back css */
						$(this).parent().parent().parent().removeClass('ao-add-on-wrap-enabled');
						$(this).parent().parent().parent().find('.enabled-label').addClass('hide');
						$(this).parent().children('.ao-add-on-btn-disable').addClass('hide');
						$(this).removeClass('hide');
						/* show error msg clearly */
						window.showNotyError("unable to enable add-on: " + $(this).parent().data('name'));
					}
				});

			});

			$('.ao-add-on-btn-disable').on('click', function (event) {
				event.preventDefault();

				/*  make necessary css here */
				$(this).parent().parent().parent().removeClass('ao-add-on-wrap-enabled');
				$(this).parent().parent().parent().find('.enabled-label').addClass('hide');
				$(this).parent().children('.ao-add-on-btn-enable').removeClass('hide');
				$(this).addClass('hide');

				$.ajax({
					type: 'POST',
					url: $('#optionAddOnsToggleUrl').val(),
					dataType: 'JSON',
					async: false,
					data: {
						'addOnId': $(this).parent().data('id'),
						'mapId': $('#selectedMapId').val(),
						'mapTypeId': $('#selectedMapTypeId').val(),
						'isEnable': 0
					},
					success: function (data) {
						if (data.success) {
							/* do nothing */
						} else {
							/* revert back css */
							$(this).parent().parent().parent().addClass('ao-add-on-wrap-enabled');
							$(this).parent().parent().parent().find('.enabled-label').removeClass('hide');
							$(this).parent().children('.ao-add-on-btn-enable').addClass('hide');
							$(this).removeClass('hide');
							/* show error msg clearly */
							window.showNotyError("unable to disable add-on: " + $(this).parent().data('name'));
						}
					},
					fail: function () {
						/* revert back css */
						$(this).parent().parent().parent().addClass('ao-add-on-wrap-enabled');
						$(this).parent().parent().parent().find('.enabled-label').removeClass('hide');
						$(this).parent().children('.ao-add-on-btn-enable').addClass('hide');
						$(this).removeClass('hide');
						/* show error msg clearly */
						window.showNotyError("unable to disable add-on: " + $(this).parent().data('name'));
					}
				});

			});

			fetchOptions();
		});
	</script>
@endsection