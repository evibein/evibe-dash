@extends("add-ons.base")

@section("inner-content")

	@if(isset($data['addOn']) && $data['addOn'])
		<div class="pad-l-15">
			<div>
				<div class="text-left font-20 in-blk">
					<a href="{{ route('add-ons.list') }}" class="btn btn-link">
						<< view all add-ons
					</a>
				</div>
				<div class="in-blk pull-right">
					<a href="" class="btn btn-primary mar-r-15">
						<i class="glyphicon glyphicon-pencil"></i> Edit
					</a>
				</div>
			</div>
			<div class="mar-t-10 mar-b-30">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="inner">
							<div class="pull-left">
								<input type="hidden" id="decorId" value="{{ $data['addOn']->id }}">
								<h3 class="panel-title">[{{ $data['addOn']->code }}] {{ $data['addOn']->name }}</h3>
							</div>
							<div class="pull-right disabled">

								@if($data['approvalButton'])
									<a href="{{ route('approval.accept',[$data['addOn']->id,config('evibe.ticket_type.decors')]) }}"
											class="btn btn-success btn-sm mar-r-10"
											onclick="return confirm('Are you sure?','Yes','No')"><i
												class="glyphicon glyphicon-thumbs-up"></i> Accept Approval</a>
									<button id="approvalRejectBtn"
											data-url="{{ route('approval.reject',[$data['addOn']->id,config('evibe.ticket_type.decors')]) }}"
											class="btn btn-danger btn-sm mar-r-10"><i
												class="glyphicon glyphicon-thumbs-down"></i> Reject Approval
									</button>
								@endif
								@if($data['askApprovalButton'])
									<a id="askApproval"
											data-url="{{ route('approval.ask',[$data['addOn']->id,config('evibe.ticket_type.decors')]) }}"
											class="btn btn-warning btn-sm mar-r-10"
											data-askreason="@if($data['isApprovalReason']) 1 @else 0 @endif"><i
												class="glyphicon glyphicon-question-sign"></i> Ask Approval</a>
									@if (AppUtil::isTabVisible('decors::edit_info'))
										<a id="editDecorInfoBtn"
												class="btn btn-danger btn-sm mar-r-10">
											<i class="glyphicon glyphicon-edit"></i> Edit
										</a>
									@endif
								@elseif (AppUtil::isTabVisible('decors::edit_info'))
									<a id="editDecorInfoBtn"
											class="btn btn-danger btn-sm mar-r-10">
										<i class="glyphicon glyphicon-edit"></i> Edit
									</a>
								@endif
								@if ($data['addOn']->is_live)
									<a href="{{ AppUtil::getLiveUrl(config('evibe.ticket_type.decors'), $data['addOn'])}}"
											target="_blank"
											class="btn btn-sm btn-primary mar-r-10">
										<i class="glyphicon glyphicon-eye-open"></i> View Live
									</a>
								@endif
								@if (AppUtil::isSuperAdmin)
									@if ($data['addOn']->is_live)
										<a href="{{ route('decor.actions.deactivate', $data['addOn']->id) }}"
												class="mar-r-10 btn btn-sm btn-warning"
												onclick="return confirm('Are you sure?');">
											<i class="glyphicon glyphicon-off"></i> Deactivate
										</a>
									@endif
									<a href="{{ route('decor.actions.delete', $data['addOn']->id) }}"
											class="btn btn-sm btn-danger"
											onclick="return confirm('Are you sure?');">
										<i class="glyphicon glyphicon-trash"></i>
									</a>
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="panel-body">

						<!-- take action begin -->
						@if ((AppUtil::isTabVisible('decors::makeactive')  || AppUtil::isTabVisible('activate_options')) && !$data['addOn']->is_live)
							<div class="mar-b-20">
								<div class="alert alert-danger text-center pad-5">
									<b>THIS ADD-ON IS NOT ACTIVE.</b>
									<a href="{{ route('decor.actions.activate', $data['addOn']->id) }}"
											class="btn btn-sm btn-success mar-l-10" disabled
											onclick="return confirm('Are you sure?');">Make Active</a>
								</div>
							</div>
					@endif
					<!-- take action end -->

						<!--Internal question start -->
						<div class="col-lg-12">
							<div class="text-center">
								@if(session()->has('customError'))
									<div class="pad-6 alert-danger in-blk">
										{{ session('customError') }}
									</div>
								@elseif(session()->has('customSuccess'))
									<div class="alert-success pad-6 in-blk">
										{{ session('customSuccess') }}
									</div>
								@endif
							</div>
						</div>

						<!--Internal questions end-->

						<!-- header begin -->
						<section class="data-sec pad-b-20">
							<div class="text-center">
								<div class="pricing in-blk">
									@if ($data['addOn']->price_worth_per_unit)
										<div class="worth in-blk">
											<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($data['addOn']->price_worth_per_unit) }}
										</div>
									@endif
									<div class="in-blk">
										<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($data['addOn']->price_per_unit) }}
									</div>
								</div>
							</div>
						</section>
						<!-- header end -->
						<div class="mar-b-6">
							@if($data['askApprovalAlert'])
								{{ $data['askApprovalAlert'] }}
							@elseif($data['isActivateAlert'])
								{{ $data['isActivateAlert'] }}
							@endif
							@if($data['rejectionComment'])
								{!! $data['rejectionComment'] !!}
							@endif
						</div>
						<!-- gallery begin -->
						<section class="data-sec pad-b-20">
							<div class="sec-body pad-t-10">

								<!-- upload new images begin -->
								@if (AppUtil::isTabVisible('decors::edit_gallery'))
									@if (session()->has('imageError'))
										<div class="alert alert-danger alert-dismissible">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<i class="glyphicon glyphicon-remove-sign"></i> {{ session('imageError') }}
										</div>
									@endif
									@if (session()->has('successMsg'))
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<i class="glyphicon glyphicon-ok-sign"></i> {{ session('successMsg') }}
										</div>
									@endif
									<div class="upload-img-wrap">
										<form action="{{ route('decor.gallery.upload', $data['addOn']->id) }}"
												method="POST" role="form"
												enctype="multipart/form-data" accept-charset="UTF-8" class="form">
											<div class="mar-b-10">
												<label>
													<input type="radio" name="gal-type"
															value="{{ config('evibe.gallery.type.image') }}"
															@if(old('gal-type') == 0) checked
															@elseif(old('gal-type') !=1) checked @endif> Image
												</label>
												<label class="mar-l-10">
													<input type="radio" name="gal-type"
															value="{{ config('evibe.gallery.type.video') }}"
															@if(old('gal-type') == 1) checked
															@endif @if(session('type') == 'video')  checked @endif> Video
												</label>
											</div>
											<div class="alert alert-warning">
												<div class="gal-type-image">
													<h4>Upload New Images <span class="font-14">( Valid formats: JPG, jpeg, jpg and png. Maximum file size: <b><u>5 MB</u></b> )</span>
													</h4>
													<div class="form-group">
														<input type="file" name="decorImages[]" multiple="true"/>
													</div>
													<div class="form-group">
														<label for="ignoreWatermark" class="text-normal text-black">
															<input type="checkbox" id="ignoreWatermark"
																	name="ignoreWatermark"> Ignore Watermark
														</label>
													</div>
												</div>
												<div class="gal-type-video hide">
													<div class="mar-b-15">
														<input type="text" name="video" class="form-control"
																placeholder="Enter youtube video link"
																value="{{ old('video') }}">
													</div>
												</div>
												<button type="submit" class="btn btn-sm btn-warning">
													<i class="glyphicon glyphicon-plus"></i> Upload
												</button>
											</div>
										</form>
									</div>
								@endif

								@if ($data['images']->count() || $data['videos']->count())
									<section class="data-sec pad-b-20">
										@if($data['images']->count())
											<div class="sec-head">
												<h4 class="text-danger in-blk">Images</h4>
											</div>
											<ul class="ls-none no-mar no-pad images-list">
												@foreach($data['images'] as $image)
													<li class="image-cnt">
														<img class="an-image" src="{{ $image->getLink() }}">
														<div class="text-center pad-t-3">
															<div class="img-title">
																<div class="view">
																	<div class="image-link"
																			title="[{{ $image->code }}] {{ $image->title }}">
																		<a href="{{ $image->getLink() }}"
																				target="_blank">[{{ $image->code }}] {{ $image->title }}</a>
																	</div>
																</div>
																<div class="edit hide">
																	<form action="{{ route('decor.gallery.title', [$data['addOn']->id, $image->id]) }}"
																			method="POST">
																		<input type="text" name="title"
																				placeholder="type image title..."
																				class="form-control"
																				value="{{ $image->title }}">
																		<button class="btn btn-xs btn-success">
																			<i class="glyphicon glyphicon-check"></i> Save
																		</button>
																		<button class="btn btn-link btn-img-title-edit-cancel">Cancel</button>
																	</form>
																</div>
															</div>
														</div>
														@if (AppUtil::isTabVisible('decors::edit_gallery'))
															<div class="img-actions">
																@if ($image->is_profile)
																	<a href="{{ route('decor.gallery.unset', [$data['addOn']->id, $image->id]) }}"
																			class="btn btn-xs btn-link">
																		<i class="glyphicon glyphicon-remove"></i> Unset Profile
																	</a>
																@else
																	<a href="{{ route('decor.gallery.set', [$data['addOn']->id, $image->id]) }}"
																			class="btn btn-xs btn-link">
																		<i class="glyphicon glyphicon-ok"></i> Set Profile
																	</a>
																@endif
																<a class="btn btn-xs btn-link btn-edit-img-title">
																	<i class="glyphicon glyphicon-pencil"></i> Edit
																</a>
																<a href="{{ route('decor.gallery.delete', [$data['addOn']->id, $image->id]) }}"
																		class="btn btn-xs btn-link"
																		onclick="return confirm('Are you sure?');">
																	<i class="glyphicon glyphicon-trash"></i> Del
																</a>
															</div>
														@endif
														@if ($image->is_profile)
															<div class="profile-label label label-success">
																<i class="glyphicon glyphicon-star"></i> Profile
															</div>
														@endif
													</li>
												@endforeach
											</ul>
										@endif
										@if($data['videos']->count())
											<div class="sec-head">
												<h4 class="in-blk text-danger">Videos</h4>
											</div>
											<div>
												@foreach($data['videos'] as $video)
													<div class="col-sm-4">
														<div class="mar-b-20">
															<iframe height="315" width="100%" class="no-pad"
																	src="https://www.youtube.com/embed/{{ $video->url }}">
															</iframe>
															<a href="{{ route('decor.gallery.delete', [$data['addOn']->id, $video->id]) }}"
																	class="btn btn-xs btn-danger btn-block pad-5"
																	onclick="return confirm('Are you sure?');">
																<i class="glyphicon glyphicon-trash"></i> Delete
															</a>
														</div>
													</div>
												@endforeach
												<div class="clearfix"></div>
											</div>
										@endif
									</section>
								@else
									<div class="alert alert-danger">
										<i class="glyphicon glyphicon-warning-sign"></i> No images/videos were uploaded for this decor style.
									</div>
								@endif
							</div>
						</section>
						<!-- gallery end -->

						<!-- description begin -->
						<section class="data-sec pad-b-20">
							<div class="sec-head">
								<h4 class="in-blk mar-r-10 text-danger">Details</h4>
							</div>
							<div class="sec-body">
								<div class="col-sm-8 no-pad-l">
									<div class="sub-sec mar-b-20">
										<h5 class="no-mar text-muted">Info</h5>
										<div class="pad-t-10">{{ $data['addOn']->info }}</div>
									</div>
									@if ($data['decorEvents'])
										<div class="sub-sec  mar-b-20">
											<h5 class="no-mar text-muted">Events supported</h5>
											<div class="pad-t-10">
												@foreach($data['decorEvents'] as $event)
													<span class="label-event">{{ $event->event->name }}</span>
												@endforeach
											</div>
										</div>
									@endif
								</div>
								<div class="col-sm-4 no-pad-l">
									<table id="decor-info" class="table table-striped table-bordered">
										<tr>
											<td><label>Free KMs</label></td>
											@if ($data['addOn']->kms_free)
												<td>{{ $data['addOn']->kms_free }} KMs</td>
											@else
												<td> --</td>
											@endif
										</tr>
										<tr>
											<td><label>Max KMs</label></td>
											@if ($data['addOn']->kms_max)
												<td>{{ $data['addOn']->kms_max }} KMs</td>
											@else
												<td> --</td>
											@endif
										</tr>
										<tr>
											<td><label>Transport Charges</label></td>
											<td>
												@if (!$data['addOn']->trans_min)
													Free
												@else
													<span class="rupee-font">&#8377;</span> {{ $data['addOn']->trans_min }}
													@if ($data['addOn']->trans_max && $data['addOn']->trans_max > $data['addOn']->trans_min)
														-
														<span class="rupee-font">&#8377;</span> {{ $data['addOn']->trans_max }}
													@endif
												@endif
											</td>
										</tr>
										<tr>
											<td><label>Setup Time</label></td>
											@if ($data['addOn']->time_setup)
												<td>{{ $data['addOn']->time_setup }} Hrs</td>
											@else
												<td> --</td>
											@endif
										</tr>
										<tr>
											<td><label>Rent Duration</label></td>
											@if ($data['addOn']->time_duration)
												<td>{{ $data['addOn']->time_duration }} Hrs</td>
											@else
												<td> --</td>
											@endif
										</tr>
										<tr>
											<td><label>Name</label></td>
											<td>{{$data['addOn']->name}}</td>
										</tr>
										<tr>
											<td><label>Code</label></td>
											<td>{{$data['addOn']->code}}</td>
										</tr>
										<tr>
											<td><label>Provider</label></td>
											<td>
												@if (!AppUtil::isTabVisible('decors::view_provider'))
													{{ $data['addOn']->provider->name }}
												@else
													<a href="{{ $data['addOn']->provider->getLocalLink() }}"
															target="_blank">
														{{ $data['addOn']->provider->name }}
													</a>
												@endif
											</td>
										</tr>
										<tr>
											<td><label>Handler</label></td>
											<td>
												{{ $data['addOn']->handler ? $data['addOn']->handler->name : "--" }}
											</td>
										</tr>
										@if($data['addOn']->provider)
											<tr>
												<td><label>Location</label></td>
												<td>@if($data['addOn']->provider->area){{ $data['addOn']->provider->area->name }}@else -- @endif</td>
											</tr>
											<tr>
												<td><label>City</label></td>
												<td>@if($data['addOn']->provider->area){{ $data['addOn']->provider->city->name }}@else -- @endif</td>
											</tr>
										@endif
									</table>
								</div>
								<div class="clearfix"></div>
							</div>
						</section>
						<!-- description end -->

						<!-- tags section begin -->
						<section class="data-sec pad-b-20">
							<div class="sec-head">
								<h4 class="in-blk mar-r-10 text-danger">Tags</h4>
							</div>
							<div class="sec-body">

								<!-- add new tags begin -->
								@if (AppUtil::isTabVisible('decors::edit_tags'))
									<div class="alert alert-warning upload-img-wrap">
										<h4>Add More Tags</h4>
										<div>You can add a new tag by just selecting a parent tag (or) both parent tag and child tag.</div>
										<form action="{{ route('decor.tags.add', $data['addOn']->id) }}" method="POST"
												role="form" enctype="multipart/form-data" accept-charset="UTF-8"
												class="form-inline pad-t-10">
											<div class="form-group">
												<select id="selectParentTag" name="selectParentTag" class="form-control">
													<option value="-1">--select parent tag--</option>
													@foreach ($data['tagsList'] as $key=>$tag)
														<option value="{{ $key }}">{{ $tag['name'] }}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<select id="selectChildTag" name="selectChildTag" class="form-control">
													<option value="-1">--select child tag--</option>
												</select>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-sm btn-warning in-blk">
													<i class="glyphicon glyphicon-plus"></i> Add Tag
												</button>
											</div>
										</form>
									</div>
								@endif
							<!-- add new tags end -->

								@if (count($data['tags']))
									<ul class="ls-none no-mar no-pad tags-list">
										@foreach ($data['tags'] as $key => $tag)
											<li class="label-tag">{{ $tag }}
												<a href="{{ route('decor.tag.delete', [$data['addOn']->id, $key]) }}"
														class="text-danger">
													<i class="glyphicon glyphicon-trash"></i>
												</a>
											</li>
										@endforeach
									</ul>
								@else
									<div class="alert alert-danger">
										<i class="glyphicon glyphicon-warning-sign"></i> No tags were assigned for this decor style.
									</div>
								@endif
							</div>
						</section>
						<!-- tags section end -->

					</div>
				</div>
			</div>
		</div>
	@else
		<div class="text-center error-box error-message">
			There is some issue with this add-on. Kindly report to tech team.
		</div>
	@endif

@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {



		});
	</script>
@endsection