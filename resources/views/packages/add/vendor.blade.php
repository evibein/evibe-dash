@extends('layout.base')

@section('title')
	<title>@if($package) Edit @else Add New @endif Package | EvibeDash</title>
@stop

@section('content')
	<div class="container page-content new-package-warp">
		<div class="mar-b-10">
			<a href="{{ route('packages.vendor.list') }}" class="font-16"><< Show all packages</a>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="font-22 text-center">
					<span>@if($package) Edit @else Create New @endif Package</span>
				</div>
				<hr>

				@if($errors->count())
					<div class="alert alert-danger">
						<a class="close mar-l-5" data-dismiss="alert">&times;</a>
						{{$errors->first()}}
					</div>
				@endif

				<form action="{{route('package.new.save')}}" class="vendorDetailsPackageForm" method="post">
					<div class="col-lg-4 pull-left">
						<div class="form-group">
							<label for="pkgName"><b>Name</b></label>
							<label class="text-mandatory">*</label>
							<input type="text" id="pkgName" name="pkgName"
									value="@if(old('pkgName')){{ old('pkgName') }}@elseif($package){{ $package['name'] }}@endif"
									placeholder="Enter your package name" class="form-control">
						</div>
						<div class="form-group">
							<label for="pkgPrice"><b>Price</b></label>
							<label class="text-mandatory">*</label>
							<input type="text" id="pkgPrice" name="pkgPrice"
									value="@if(old('pkgPrice')){{ old('pkgPrice') }}@elseif($package){{ $package['price'] }}@endif"
									placeholder="Enter your package price" class="form-control">
						</div>
						<div class="form-group">
							<label for="pkgPriceWorth"><b>Price Worth</b></label>
							<label class="text-mandatory">*</label>
							<input type="text" id="pkgPriceWorth" name="pkgPriceWorth"
									value="@if(old('pkgPriceWorth')){{ old('pkgPriceWorth') }}@elseif($package){{ $package['price_worth'] }}@endif"
									placeholder="Enter your package price worth" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 pull-left">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Kms (Free - Max) in hrs</label><label class="text-mandatory">*</label>
								<div>
									<div class="col-sm-5 no-pad-l">
										<input class="form-control " type="text"
												value="@if(old('kmsFree')){{ old('kmsFree') }}@elseif($package){{ $package['kms_free'] }}@endif"
												placeholder="Free kms"
												name="kmsFree">
									</div>
									<div class="col-sm-5 no-pad-l">
										<input class="form-control " type="text"
												value="@if(old('kmsMax')){{ old('kmsMax') }}@elseif($package){{ $package['kms_max'] }}@endif"
												placeholder="Max Kms"
												name="kmsMax">

									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Transportation charges (Min - Max)</label><label
										class="text-mandatory">*</label>
								<div>
									<div class="col-sm-5 no-pad-l">
										<input class="form-control" type="text"
												value="@if(old('transMin')){{ old('transMin') }}@elseif($package){{ $package['trans_min'] }}@endif"
												placeholder="Min price" name="transMin">
									</div>
									<div class="col-sm-5 no-pad-l">
										<input class="form-control" type="text"
												value="@if(old('transMax')){{ old('transMax') }}@elseif($package){{ $package['trans_max'] }}@endif"
												placeholder="Max Price" name="transMax">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Setup time & duration (Hrs)</label><label class="text-mandatory">*</label>
								<div>
									<div class="col-sm-5 no-pad-l">
										<input type="text" class="form-control form-inline" name="setupTime"
												placeholder="Setup time"
												value="@if(old('setupTime')){{ old('setupTime') }}@elseif($package){{ $package['time_setup'] }}@endif">

									</div>
									<div class="col-sm-5 no-pad-l">
										<input type="text" class="form-inline form-control" name="setupDuration"
												placeholder="party-duration"
												value="@if(old('setupDuration')){{ old('setupDuration') }}@elseif($package){{ $package['time_duration'] }}@endif">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div class=" col-lg-4 pull-left">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-l">
							<div class="form-group">
								<label for="pkgInfo"><b>Inclusions</b></label>
								<label class="text-mandatory">*</label>
								<textarea placeholder="Enter More Info" name="pkgInfo" id="pkgInfo"
										class="editor-box form-control editor-html" rows="8">
										@if(old('pkgInfo')){{ old('pkgInfo') }}@elseif($package){!! $package['info'] !!}@endif</textarea>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="mar-t-10">
						<div class="col-lg-4 pull-left">
							<div class="form-group">
								<label for="pkgFacts"><b>Package Facts</b></label>
								<span class="text-italic mar-l-15">Copy from: </span>
								<select name="copyFacts" id="copyFacts">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($facts as $fact)
										<option value="{{ $fact->value }}">{{ $fact->name }}</option>
									@endforeach
								</select>
								<textarea placeholder="Enter Package Facts" name="pkgFacts" id="pkgFacts"
										class="editor-box form-control editor-html" rows="8">
										@if(old('pkgFacts')){{ old('pkgFacts') }}@elseif($package){!! $package['facts'] !!}@endif</textarea>
							</div>
						</div>
						<div class="col-lg-4 pull-left">
							<div class="form-group">
								<label class="text-bold ">Prerequisites</label>
								<span class="text-italic pad-l-15">Copy from: </span>
								<select name="copyPreReq" id="copyPreReq">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($preReqs as $req)
										<option value="{{ $req->value }}">{{ $req->name }}</option>
									@endforeach
								</select>
								<textarea name="pkgPrereq" class="form-control editor-box editor-html" id="pkgPrereq"
										rows="8"
										placeholder="Enter the terms of booking">
										@if(old('pkgPrereq')){{ old('pkgPrereq') }}@elseif($package){!! $package['prerequisites'] !!}@endif</textarea>
							</div>
						</div>
						<div class="col-lg-4 pull-left">
							<label for="pkgTerms" class="text-bold">Booking Terms</label>
							<span class="text-italic pad-l-15">Copy from: </span>
							<select name="copyTerms" id="copyTerms">
								<option value="-1" selected="selected">-- select ---</option>
								@foreach ($terms as $term)
									<option value="{{ $term->value }}">{{ $term->name }}</option>
								@endforeach
							</select>
							<textarea name="pkgTerms" class="form-control editor-box editor-html" id="pkgTerms"
									rows="8	"
									placeholder="Enter the terms of booking">
									@if(old('pkgTerms')){{ old('pkgTerms') }}@elseif($package){!! $package['terms'] !!}@endif</textarea>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="mar-t-10">
						<div class="col-lg-4">
							<div class="form-group">
								<label for="">Group Count <label class="text-mandatory">* <i
												class="text-muted">( Enter 0 if N/A)</i></label></label>
								<input type="text" name="groupCount"
										placeholder="Enter the number of person in the guests" class="form-control"
										value="@if(old('groupCount')){{ old('groupCount') }}@elseif($package){{ $package['group_count'] }}@elseif(request('occasion') == config('evibe.event.special_experience')){{ "0" }}@endif">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label for="">Price / Person (Extra guest) <i
											class="text-muted">( Enter 0 if N/A)</i></label>
								<input type="text" name="pricePerPersonExtraGuest"
										placeholder="Enter the price per person for extra guests"
										class="form-control"
										value="@if(old('pricePerPersonExtraGuest')){{old('pricePerPersonExtraGuest')}}@elseif($package){{ $package['price_per_extra_guest'] }}@elseif(request('occasion') == config('evibe.event.special_experience')){{ "0" }}@endif">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					@if(request('occasion') != config('evibe.event.special_experience'))
						<div class="col-lg-12 no-pad-l mar-b-20">
							<div class="col-lg-4">
								<label for="">Check-in</label>
								<input type="text" name="checkIn" id="checkIn" class="form-control"
										placeholder="Enter check in time"
										value="@if(old('checkIn')){{ old('checkIn') }}@elseif($package){{ $package['check_in'] }}@endif">
							</div>
							<div class="col-lg-4">
								<label for="">Check-out</label>
								<label class="mar-l-40 text-normal "><input
											type="checkbox" name="checkoutNextDay"
											@if(old('checkoutNextDay') == "on" || ($package && array_key_exists("1",explode(" (Next day)",$package['check_out']))) ) checked @endif> Next day ?</label>
								<input type="text" name="checkOut" id="checkOut" class="form-control"
										placeholder="Enter check out time"
										value="@if(old('checkOut')){{ old('checkOut') }}@elseif($package){{ explode(" (Next day)",$package['check_out'])[0] }}@endif">
							</div>
							<div class="clearfix"></div>
						</div>
					@endif
					<div class="clearfix"></div>

					<!-- Showing dynamic fields to planner package only for surprises -->
					@if((request('occasion') && (request('occasion') == config("evibe.event.special_experience"))) || (isset($inputData['occasion']) && ($inputData['occasion'] == config("evibe.event.special_experience"))))
						<div class="">
							<div class="form-group">
								@if(count($dynamicFields) >= 1)
									@foreach($dynamicFields as $category => $dynamicField)
										<?php $count = 1 ?>
										@foreach($dynamicField as $field)
											@if($field['typeField'] == config('evibe.input.text') || $field['typeField'] == config('evibe.input.number'))
												@include('packages.dynamicFields.text')
											@elseif($field['typeField'] == config('evibe.input.textarea'))
												@include('packages.dynamicFields.textarea')
											@elseif($field['typeField'] == config('evibe.input.option'))
												@include('packages.dynamicFields.option')
											@elseif($field['typeField'] == config('evibe.input.radio'))
												@include('packages.dynamicFields.radio')
											@elseif($field['typeField'] == config('evibe.input.checkbox'))
												@include('packages.dynamicFields.checkbox')
											@endif

											@if(($count % 3) == 0)
												<div class="clearfix"></div>
											@endif
											<?php $count++ ?>
										@endforeach
										<div class="clearfix"></div>
									@endforeach
								@else
									<p class="text-center"><b>N/A</b></p>
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
					@endif

					<input type="hidden"
							value="@if(request('occasion')) {{ request('occasion') }} @elseif(isset($inputData['occasion'])){{ $inputData['occasion'] }}@endif"
							name="occasion">
					<input type="hidden" value="{{ request('pageType') }}" name="typePackage">
					<input type="hidden" value="{{ request('type') }}" name="type">
					<input type="hidden" value="{{ request('vendorId') }}" name="vendorId">
					<input type="hidden" value="{{ request('venueId') }}" name="venueId">
					<input type="hidden" value="@if(is_null($package)){{ false }}@else{{ true }}@endif" name="isUpdate">
					<input type="hidden" value="@if(!is_null($package)){{ $package['id'] }}@endif" name="packageId">
					<input type="hidden"
							value="@if(isset($inputData['map_type_id'])){{ $inputData['map_type_id'] }}@endif"
							name="mapTypeId">
					<div class="btn-action col-lg-8 mar-t-10">
						<div class="col-lg-3 pull-right">
							<button type="submit" name="myBtn" id="myBtn" value="Save"
									class="pad-10 btn btn-success btn-block">
								<span class="glyphicon glyphicon-floppy-save"></span>
								@if(is_null($package)) Save @else Update @endif
							</button>
						</div>
						<div class="col-lg-3 pull-right">
							<a href="{{ route('packages.vendor.list') }}"
									class="pad-10 btn btn-danger btn-block">
								<i class="glyphicon glyphicon-remove"></i> Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/package/package.js");
		});
	</script>
@stop