@extends('layout.base')

@section('title')
	<title>@if($package) Edit @else Add New @endif Package | EvibeDash</title>
@stop

@section('content')
	<div class="container page-content new-package-warp">
		<div class="mar-b-10">
			<a href="{{ route('packages.vendor.list') }}" class="font-16"><< Show all packages</a>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="font-22 text-center">
					<span>@if($package) Edit @else Create New @endif Package</span>
				</div>
				<hr>

				@if($errors->count())
					<div class="alert alert-danger">
						<a class="close mar-l-5" data-dismiss="alert">&times;</a>
						{{$errors->first()}}
					</div>
				@endif

				<form action="{{route('package.new.save')}}" class="venueDetailsPackageForm" method="post">
					<div class="col-lg-4 pull-left">
						<div class="form-group">
							<label for="pkgName"><b>Name</b></label>
							<label class="text-mandatory">*</label>
							<input type="text" id="pkgName" name="pkgName"
									value="@if(old('pkgName')){{ old('pkgName') }}@elseif($package){{ $package['name'] }}@endif"
									placeholder="Enter your package name" class="form-control">
						</div>
						<label>Price ( Min<label class="text-mandatory">*</label>, Max, Worth )</label>
						<div class="clearfix"></div>
						<div class="col-lg-4 no-pad-l">
							<div class="form-group">
								<input type="text" id="pkgPrice" name="pkgPrice"
										value="@if(old('pkgPrice')){{ old('pkgPrice') }}@elseif($package){{ $package['price'] }}@endif"
										placeholder="Min Price" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 no-pad-l">
							<div class="form-group">
								<input type="text" id="priceMax" name="priceMax"
										value="@if(old('priceMax')){{ old('priceMax') }}@elseif($package['price_max'] > 0){{ $package['price_max'] }}@endif"
										placeholder="Max Price" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 no-pad-l">
							<div class="form-group">
								<input type="text" id="pkgPriceWorth" name="pkgPriceWorth"
										value="@if(old('pkgPriceWorth')){{ old('pkgPriceWorth') }}@elseif($package){{ $package['price_worth'] }}@endif"
										placeholder="Worth Price" class="form-control">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-lg-4">
						<label for="rangeInfo"
								title="Range info field is mandatory with max price">Range Info **</label>
						<textarea name="rangeInfo" id="rangeInfo" rows="4" class="form-control editor-html"
								placeholder="Describe the price range, why this price?">
								@if(old('rangeInfo')){{ old('rangeInfo') }}@elseif($package){!! $package['range_info'] !!}@endif</textarea>
					</div>
					<div class="col-lg-4 pull-left">
						@if(request('occasion') != config('evibe.event.special_experience'))
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<label>Guest (Min, Max)</label>
									<div>
										<div class="col-sm-6 no-pad-l">
											<input class="form-control " type="text"
													value="@if(old('guestMin')){{ old('guestMin') }}@elseif($package['guests_min'] > 0){{ $package['guests_min'] }}@endif"
													placeholder="Min Guest" name="guestMin">
										</div>
										<div class="col-sm-6 no-pad-l">
											<input class="form-control " type="text"
													value="@if(old('guestMax')){{ old('guestMax') }}@elseif($package['guests_max'] > 0){{ $package['guests_max'] }}@endif"
													placeholder="Max Guest" name="guestMax">
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<label>Guest Kid (Min, Max)</label>
									<div>
										<div class="col-sm-6 no-pad-l">
											<input class="form-control " type="text"
													value="@if(old('guestKidMin')){{ old('guestKidMin') }}@elseif($package['guests_kid_min'] > 0){{ $package['guests_kid_min'] }}@endif"
													placeholder="Min Kid Guests"
													name="guestKidMin">
										</div>
										<div class="col-sm-6 no-pad-l">
											<input class="form-control " type="text"
													value="@if(old('guestKidMax')){{ old('guestKidMax') }}@elseif($package['guests_kid_max'] > 0){{ $package['guests_kid_max'] }}@endif"
													placeholder="Max Kid Guests"
													name="guestKidMax">

										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						@endif
					</div>
					<div class="clearfix"></div>
					<div class="mar-t-10">
						<div class=" col-lg-6 pull-left">
							<div class="form-group">
								<label for="pkgInfo"><b>Inclusions</b></label>
								<label class="text-mandatory">*</label>
								<textarea placeholder="Enter More Info" name="pkgInfo" id="pkgInfo"
										class="editor-box form-control editor-html" rows="8">
										@if(old('pkgInfo')){{ old('pkgInfo') }}@elseif($package){!! $package['info'] !!}@endif</textarea>
							</div>
						</div>
						<div class="col-lg-6 pull-left">
							<div class="form-group">
								<label for="pkgFacts"><b>Package Facts</b></label>
								<span class="text-italic mar-l-15">Copy from: </span>
								<select name="copyFacts" id="copyFacts">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($facts as $fact)
										<option value="{{ $fact->value }}">{{ $fact->name }}</option>
									@endforeach
								</select>
								<textarea placeholder="Enter Package Facts" name="pkgFacts" id="pkgFacts"
										class="editor-box form-control editor-html" rows="8">
										@if(old('pkgFacts')){{ old('pkgFacts') }}@elseif($package){!! $package['facts'] !!}@endif</textarea>
							</div>
						</div>
						<div class="col-lg-6 pull-left">
							<div class="form-group">
								<label class="text-bold ">Prerequisites</label>
								<span class="text-italic pad-l-15">Copy from: </span>
								<select name="copyPreReq" id="copyPreReq">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($preReqs as $preReq)
										<option value="{{ $preReq->value }}">{{ $preReq->name }}</option>
									@endforeach
								</select>
								<textarea name="pkgPrereq" class="form-control editor-box editor-html" id="pkgPrereq"
										rows="8"
										placeholder="Enter the Prerequisites of booking">
										@if(old('pkgPrereq')){{ old('pkgPrereq') }}@elseif($package){!! $package['prerequisites'] !!}@endif</textarea>
							</div>
						</div>
						<div class="col-lg-6 pull-left">
							<div class="form-group">
								<label for="pkgTerms" class="text-bold">Booking Terms</label>
								<span class="text-italic pad-l-15">Copy from: </span>
								<select name="copyTerms" id="copyTerms">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($terms as $term)
										<option value="{{ $term->value }}">{{ $term->name }}</option>
									@endforeach
								</select>
								<textarea name="pkgTerms" class="form-control editor-box editor-html" id="pkgTerms"
										rows="8"
										placeholder="Enter the terms of booking">
										@if(old('pkgTerms')){{ old('pkgTerms') }}@elseif($package){!! $package['terms'] !!}@endif</textarea>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="mar-t-10">
						<div class="col-lg-4">
							<div class="form-group">
								<label for="hallType">Property Type</label><label class="text-mandatory">*</label>
								<select name="hallType" id="hallType" class="form-control"
										data-old="@if(old('hallType')){{old('hallType')}}@else{{-1}}@endif">
									<option value="-1">-- Select Property Type --</option>
									@foreach($hallTypes as $hallType)
										<option value="{{$hallType->id}}"
												@if(old('hallType') == $hallType->id || ($package && $package['hall_type_id'] == $hallType->id)) selected @endif>{{$hallType->name}}
										</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Group Count <label class="text-mandatory">*</label></label>
								<input type="text" name="groupCount"
										placeholder="Enter the number of person in the guests" class="form-control"
										value="@if(old('groupCount')){{ old('groupCount') }}@elseif($package){{ $package['group_count'] }}@endif">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label for="">Price / Person (Extra guest) <i
											class="text-muted">( Enter 0 if N/A)</i></label>
								<input type="text" name="pricePerPersonExtraGuest"
										placeholder="Enter the price per person for extra guests" class="form-control"
										value="@if(old('pricePerPersonExtraGuest')){{old('pricePerPersonExtraGuest')}}@elseif($package){{ $package['price_per_extra_guest'] }}@endif">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="mar-t-10">
						<div class="col-lg-4">
							<div class="form-group">
								<label for="area">Area</label><label class="text-mandatory">*</label>
								<select name="area" id="area" class="form-control"
										data-old="@if(old('area')){{old('area')}}@else{{-1}}@endif">
									<option value="-1">-- Select Area Type --</option>
									@php
										$inpArea = old('area') ?: (($package && $package['area_id']) ? $package['area_id'] : ((isset($partner['areaId']) && $partner['areaId']) ? $partner['areaId'] : null));
									@endphp
									@foreach($areas as $area)
										<option value="{{$area->id}}" data-zip-code="{{ $area->zip_code }}"
												@if($inpArea == $area->id) selected @endif>{{$area->name}}
										</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Pin Code <label class="text-mandatory">*</label></label>
								@php
									$inpZip = old('zip') ?: (($package && $package['zip']) ? $package['zip'] : ((isset($partner['zip']) && $partner['zip']) ? $partner['zip'] : null));
								@endphp
								<input type="text" name="zip"
										placeholder="Enter the pin code of venue area" class="form-control" maxlength="6"
										value="@if($inpZip){{ $inpZip }}@endif">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Landmark <label class="text-mandatory">*</label></label>
								@php
									$inpLandmark = old('landmark') ?: (($package && $package['landmark']) ? $package['landmark'] : ((isset($partner['landmark']) && $partner['landmark']) ? $partner['landmark'] : null));
								@endphp
								<input type="text" name="landmark"
										placeholder="Enter the landmark near venue" class="form-control"
										value="@if($inpLandmark){{ $inpLandmark }}@endif">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="mar-t-10">
						<div class="col-lg-6 pull-left">
							<div class="form-group">
								<label for="fullAddress" class="text-bold">Venue Address
									<label class="text-mandatory">*</label></label>
								@php
									$inpVenueAddress = old('fullAddress') ?: (($package && $package['full_address']) ? $package['full_address'] : ((isset($partner['fullAddress']) && $partner['fullAddress']) ? $partner['fullAddress'] : null));
								@endphp
								<textarea name="fullAddress" class="form-control" id="fullAddress" rows="8" placeholder="Enter the terms of booking">@if($inpVenueAddress){{ $inpVenueAddress }}@endif</textarea>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="mar-t-10">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Map Latitude <label class="text-mandatory">*</label></label>
								@php
									$inpMapLat = old('mapLat') ?: (($package && $package['map_lat']) ? $package['map_lat'] : ((isset($partner['mapLat']) && $partner['mapLat']) ? $partner['mapLat'] : null));
								@endphp
								<input type="text" name="mapLat"
										placeholder="Enter the latitude of venue" class="form-control"
										value="@if($inpMapLat){{ $inpMapLat }}@endif">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Map longitude <label class="text-mandatory">*</label></label>
								@php
									$inpMapLong = old('mapLong') ?: (($package && $package['map_long']) ? $package['map_long'] : ((isset($partner['mapLong']) && $partner['mapLong']) ? $partner['mapLong'] : null));
								@endphp
								<input type="text" name="mapLong"
										placeholder="Enter the longitude of venue" class="form-control"
										value="@if($inpMapLong){{ $inpMapLong }}@endif">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label for="venueType">Type venue</label><label class="text-mandatory">*</label>
								<select name="venueType" id="venueType" class="form-control"
										data-old="@if(old('venueType')){{old('venueType')}}@else{{-1}}@endif">
									<option value="-1">-- Select Venue Type --</option>
									@php
										$inpTypeVenue = old('venueType') ?: (($package && $package['type_venue_id']) ? $package['type_venue_id'] : ((isset($partner['typeVenueId']) && $partner['typeVenueId']) ? $partner['typeVenueId'] : null));
									@endphp
									@foreach($venueTypes as $venueType)
										<option value="{{$venueType->id}}"
												@if($inpTypeVenue == $venueType->id) selected @endif>{{$venueType->name}}
										</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="mar-t-10">
						<div class="col-lg-6 pull-left">
							<div class="form-group">
								<label class="text-bold ">Package Tag Line</label>
								<textarea name="packageTagLine" class="form-control" id="packageTagLine" rows="8"
										placeholder="Enter the package tag line">@if(old('packageTagLine')){{ old('packageTagLine') }}@elseif($package){{ $package['package_tag_line'] }}@endif</textarea>
							</div>
						</div>
						<div class="col-lg-6 pull-left">
							<div class="form-group">
								<label for="aboutPackage" class="text-bold">About Package</label>
								<textarea name="aboutPackage" class="form-control editor-box editor-html" id="aboutPackage" rows="8"
										placeholder="Enter description about the package">@if(old('aboutPackage')){{ old('aboutPackage') }}@elseif($package){{ $package['about_package'] }}@endif</textarea>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="mar-t-10">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Book Before Hours</label>
								<input type="text" name="bookBeforeHrs"
										placeholder="Enter book before hrs for the package" class="form-control"
										value="@if(old('bookBeforeHrs')){{ old('bookBeforeHrs') }}@elseif($package){{ $package['book_before_hrs'] }}@endif">
							</div>
						</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label>Table Setup</label>
									<input type="text" name="tableSetup"
											placeholder="Enter the CLD table setup" class="form-control"
											value="@if(old('tableSetup')){{ old('tableSetup') }}@elseif($package){{ $package['table_setup'] }}@endif">
								</div>
							</div>
						<div class="clearfix"></div>
					</div>
					@if(request('occasion') == config('evibe.event.special_experience'))
						<div class="pad-l-15"><b>Suggest Timings:</b></div>
					@endif
					<div class="col-lg-12 no-pad-l mar-b-20">
						<div class="col-lg-4">
							<label for="">
								@if(request('occasion') == config('evibe.event.special_experience'))
									Start Time
								@else
									Check-in
								@endif
							</label>
							<input type="text" name="checkIn" id="checkIn" class="form-control"
									placeholder="Enter check in time"
									value="@if(old('checkIn')){{ old('checkIn') }}@elseif($package){{ $package['check_in'] }}@endif">
						</div>
						<div class="col-lg-4">
							<label for="">
								@if(request('occasion') == config('evibe.event.special_experience'))
									End Time
								@else
									Check-out
								@endif
							</label>
							<label class="mar-l-40 text-normal "><input
										type="checkbox" name="checkoutNextDay"
										@if(old('checkoutNextDay') == "on" || ($package && array_key_exists("1",explode(" (Next day)",$package['check_out']))) ) checked @endif> Next day ?</label>
							<input type="text" name="checkOut" id="checkOut" class="form-control"
									placeholder="Enter check out time"
									value="@if(old('checkOut')){{ old('checkOut') }}@elseif($package){{ explode(" (Next day)",$package['check_out'])[0] }}@endif">
						</div>
						<div class="col-lg-4">
							<label for="">Token advance</label>
							<input type="text" name="tokenAdvance" id="tokenAdvance" class="form-control"
									placeholder="Enter token advance amount"
									value="@if(old('tokenAdvance')){{old('tokenAdvance')}}@elseif($package){{ $package['token_advance'] }}@endif">
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>

					<div class="">
						<div class="form-group">
							@if(count($dynamicFields) >= 1)
								@foreach($dynamicFields as $category => $dynamicField)
									<?php $count = 1 ?>
									@foreach($dynamicField as $field)
										@if($field['typeField'] == config('evibe.input.text') || $field['typeField'] == config('evibe.input.number'))
											@include('packages.dynamicFields.text')
										@elseif($field['typeField'] == config('evibe.input.textarea'))
											@include('packages.dynamicFields.textarea')
										@elseif($field['typeField'] == config('evibe.input.option'))
											@include('packages.dynamicFields.option')
										@elseif($field['typeField'] == config('evibe.input.radio'))
											@include('packages.dynamicFields.radio')
										@elseif($field['typeField'] == config('evibe.input.checkbox'))
											@include('packages.dynamicFields.checkbox')
										@endif

										@if(($count % 3) == 0)
											<div class="clearfix"></div>
										@endif
										<?php $count++ ?>
									@endforeach
									<div class="clearfix"></div>
								@endforeach
							@else
								<p class="text-center"><b>N/A</b></p>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>

					<input type="hidden"
							value="@if(request('occasion')){{ request('occasion') }}@elseif(isset($inputData['occasion'])){{ $inputData['occasion'] }}@endif"
							name="occasion">
					<input type="hidden" value="{{ request('pageType') }}" name="typePackage">
					<input type="hidden" value="{{ request('type') }}" name="type">
					<input type="hidden" value="{{ request('vendorId') }}" name="vendorId">
					<input type="hidden" value="{{ request('venueId') }}" name="venueId">
					<input type="hidden" value="@if(is_null($package)){{ false }}@else{{ true }}@endif" name="isUpdate">
					<input type="hidden" value="@if(!is_null($package)){{ $package['id'] }}@endif" name="packageId">
					<input type="hidden"
							value="@if(isset($inputData['map_type_id'])){{ $inputData['map_type_id'] }}@endif"
							name="mapTypeId">
					<div class="btn-action col-lg-8 mar-t-10">
						<div class="col-lg-3 pull-right">
							<button type="submit" name="myBtn" id="myBtn" value="Save"
									class="pad-10 btn btn-success btn-block">
								<span class="glyphicon glyphicon-floppy-save"></span>
								@if(is_null($package)) Save @else Update @endif
							</button>
						</div>
						<div class="col-lg-3 pull-right">
							<a href="{{ route('packages.vendor.list') }}"
									class="pad-10 btn btn-danger btn-block">
								<i class="glyphicon glyphicon-remove"></i> Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/package/package.js");
		});
	</script>
@stop