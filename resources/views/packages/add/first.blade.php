@extends('layout.base')

@section('title')
	<title>Add New Package | EvibeDash</title>
@stop

@section('content')
	<div class="container page-content new-package-warp">
		<div class="mar-b-10">
			<a href="{{ route('packages.vendor.list') }}" class="font-16"><< Show all packages</a>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="font-22 text-center">
					<span>Create New Package</span>
				</div>
				<hr>
				<div class="alert alert-info mar-10">
					<i class="glyphicon glyphicon-info-sign"></i>
					<span>Select Provider name (based on <b><i>venue</i></b> or
						<b><i>non-venue</i></b>), Occasion & Package type.</span>
				</div>

				<form data-url="{{route('package.new.show.details')}}" class="newPackageFirstForm" method="get">
					<div class="col-lg-3 col-md-3">
						<div class="form-group">
							<label for="type">Select Provider Type</label>
							<select name="type" id="type" class="form-control"
									data-old="@if(old('type')){{ old('type') }}@else -1 @endif">
								<option value="0">Non-venue</option>
								<option value="1">Venue</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="form-group vendor-details">
							<label for="vendor">Select Vendor</label>
							<select name="vendor" id="vendor" class="form-control"
									data-old="@if(old('vendor')){{old('vendor')}}@else 1 @endif">
								@foreach ($data['vendors'] as $vendor)
									<option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
								@endforeach
							</select>
							<input type="hidden" name="roleId" id="roleId" value=""/>
						</div>
						<div class="form-group venue-details hide">
							<label for="venue">Select Venue</label>
							<select name="venue" id="venue" class="form-control"
									data-old="@if(old('venue')){{ old('venue') }} @else 1 @endif">
								@foreach ($data['venues'] as $venue)
									<option value="{{ $venue->id }}">{{ $venue->name }}</option>
								@endforeach
							</select>
							<input type="hidden" name="roleId" id="roleId" value=""/>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="form-group">
							<label for="occasion">Select Occasion Type</label>
							<select name="occasion" id="occasion" class="form-control"
									data-old="@if(old('occasion')){{ old('occasion') }}@else -1 @endif">
								<option value="-1">-- Select --</option>
								@foreach($data['typeEvents'] as $event)
									<option value="{{$event->id}}">{{ucfirst($event->name)}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="form-group select-tags-wrap">
							<label for="pageType">Select Package Type</label>
							<select name="pageType" id="pageType" class="form-control"
									data-old="@if(old('pageType')){{ old('pageType') }}@else -1 @endif">
								<option value="-1">-- Select --</option>
								@foreach($data['typePackages'] as $id => $name)
									<option value="{{$id}}">{{$name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="text-center pad-t-20">
						<button class="btn btn-info button-add-package">Next <i
									class="glyphicon glyphicon-forward mar-l-10"></i>
						</button>
					</div>
				</form>
				<input type="hidden" id="surpriseEventId" value="{{ config("evibe.event.special_experience") }}">
				<input type="hidden" id="surpriseEventTypeTicketId"
						value="{{ config("evibe.ticket_type.couple-experiences") }}">
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/package/package.js");
		});
	</script>
@stop