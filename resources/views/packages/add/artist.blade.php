@extends('layout.base')

@section('title')
	<title>@if($package) Edit @else Add New @endif Package | EvibeDash</title>
@stop

@section('content')
	<div class="container page-content new-package-warp">
		<div class="mar-b-10">
			<a href="{{ route('packages.vendor.list') }}" class="font-16"><< Show all packages</a>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="font-22 text-center">
					<span>@if($package) Edit @else Create New @endif Package</span>
				</div>
				<hr>

				@if($errors->count())
					<div class="alert alert-danger">
						<a class="close mar-l-5" data-dismiss="alert">&times;</a>
						{{$errors->first()}}
					</div>
				@endif

				<form action="{{route('package.new.save')}}" class="artistDetailsPackageForm" method="post">
					<table class="table table-bordered table-fixed">
						<tr>
							<td>
								<div class="form-group">
									<label>Package Name*</label>
									<input type="text" name="pkgName" id="pkgName" placeholder="Enter package name"
											class="form-control"
											value="@if(old('pkgName')){{ old('pkgName') }}@elseif($package){{ $package['name'] }}@endif">
								</div>
								<div class="form-group">
									<label>Package Description*</label>
									<textarea name="pkgInfo" id="pkgInfo" rows="8"
											class="form-control vertical-resize editor-html"
											placeholder="Enter package description">
											@if(old('pkgInfo')){{ old('pkgInfo') }}@elseif($package){!! $package['info'] !!}@endif</textarea>
								</div>
							</td>
							<td>
								<div class="form-group">
									<div class="col-sm-4 no-pad-l">
										<label>Min. Price*</label>
										<input type="text" name="pkgPrice" id="pkgPrice" placeholder="Enter min. price"
												class="form-control"
												value="@if(old('pkgPrice')){{ old('pkgPrice') }}@elseif($package){{ $package['price'] }}@endif">
									</div>
									<div class="col-sm-4">
										<label>Max. Price</label>
										<input type="text" name="priceMax" id="priceMax" placeholder="Enter max. price"
												class="form-control"
												value="@if(old('priceMax')){{ old('priceMax') }}@elseif($package){{ $package['price_max'] }}@endif">
									</div>
									<div class="col-sm-4 no-pad-r">
										<label>Worth</label>
										<input type="text" name="pkgPriceWorth" id="pkgPriceWorth"
												placeholder="Enter price worth"
												class="form-control"
												value="@if(old('pkgPriceWorth')){{ old('pkgPriceWorth') }}@elseif($package){{ $package['price_worth'] }}@endif">
									</div>
									<div class="clearfix"></div>
								</div>
								@if(request('occasion') != config('evibe.event.special_experience'))
									<div class="form-group">
										<div class="text-center mar-b-10"><label>Guests Count</label></div>
										<div class="col-sm-3 no-pad-l">
											<label>Min. Total</label>
											<input type="text" name="guestMin" id="guestMin"
													placeholder="Enter min. guests"
													class="form-control"
													value="@if(old('guestMin')){{ old('guestMin') }}@elseif($package){{ $package['guests_min'] }}@endif">
										</div>
										<div class="col-sm-3">
											<label>Max. Total</label>
											<input type="text" name="guestMax" id="guestMax"
													placeholder="Enter max. guests"
													class="form-control"
													value="@if(old('guestMax')){{ old('guestMax') }}@elseif($package){{ $package['guests_max'] }}@endif">
										</div>
										<div class="col-sm-3 no-pad-r">
											<label>Min. Kids</label>
											<input type="text" name="guestKidMin" id="guestKidMin"
													placeholder="Enter min. kid guests" class="form-control"
													value="@if(old('guestKidMin')){{ old('guestKidMin') }}@elseif($package){{ $package['guests_kid_min'] }}@endif">
										</div>
										<div class="col-sm-3 no-pad-r">
											<label>Max. Kids</label>
											<input type="text" name="guestKidMax" id="guestKidMax"
													placeholder="Enter max. kid guests" class="form-control"
													value="@if(old('guestKidMax')){{ old('guestKidMax') }}@elseif($package){{ $package['guests_kid_max'] }}@endif">
										</div>
										<div class="clearfix"></div>
									</div>
								@endif
								<div class="form-group">
									<div class="col-sm-6 no-pad-l">
										<label>Min. @if(request('occasion') == config('evibe.event.special_experience')) Surprise @endif Duration (in mins)</label>
										<input type="text" name="minDur" id="minDur" placeholder="Enter min. duration"
												class="form-control"
												value="@if(old('minDur')){{ old('minDur') }}@elseif($package){{ $package['duration_min'] }}@endif">
									</div>
									<div class="col-sm-6">
										<label>Max. @if(request('occasion') == config('evibe.event.special_experience')) Surprise @endif Duration (in mins)</label>
										<input type="text" name="maxDur" id="maxDur" placeholder="Enter max. duration"
												class="form-control"
												value="@if(old('maxDur')){{ old('maxDur') }}@elseif($package){{ $package['duration_max'] }}@endif">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="mar-t-10">
									<div class="form-group">
										<div class="col-sm-6 no-pad-l">
											<label for="">Group Count <label class="text-mandatory">*</label></label>
											<input type="text" name="groupCount"
													placeholder="Enter no of guest in party" class="form-control"
													value="@if(old('groupCount')){{ old('groupCount') }}@elseif($package){{ $package['group_count'] }}@elseif(request('occasion') == config('evibe.event.special_experience')){{ "0" }}@endif">
										</div>
										<div class="col-sm-6">
											<label for="">Price / Person (Extra guest)</label>
											<input type="text" name="pricePerPersonExtraGuest"
													placeholder="Enter the price per person for extra guests"
													class="form-control"
													value="@if(old('pricePerPersonExtraGuest')){{old('pricePerPersonExtraGuest')}}@elseif($package){{ $package['price_per_extra_guest'] }}@elseif(request('occasion') == config('evibe.event.special_experience')){{ "0" }}@endif">
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								@if(request('occasion') != config('evibe.event.special_experience'))
									<div class="mar-t-10">
										<span><b>Suggest Timings (if applicable):</b></span>
										<div class="form-group">
											<div class="col-sm-6 no-pad-l">
												<label for="">Check-in</label>
												<input type="text" name="checkIn" id="checkIn" class="form-control"
														placeholder="Enter check in time"
														value="@if(old('checkIn')){{ old('checkIn') }}@elseif($package){{ $package['check_in'] }}@endif">
											</div>
											<div class="col-sm-6">
												<label for="">Check-out</label>
												<label class="mar-l-40 text-normal "><input
															type="checkbox" name="checkoutNextDay"
															@if(old('checkoutNextDay') == "on") checked
															@elseif($package && array_key_exists("1",explode(" (Next day)",$package['check_out'])) ) checked @endif> Next day ?</label>
												<input type="text" name="checkOut" class="form-control" id="checkOut"
														placeholder="Enter check out time"
														value="@if(old('checkOut')){{ old('checkOut') }}@elseif($package){{ explode(" (Next day)",$package['check_out'])[0] }}@endif">
											</div>
										</div>
									</div>
								@endif
							</td>
						</tr>
						<tr>
							<td>
								<div class="form-group">
									<label for="pkgFacts"><b>Package Facts</b></label>
									<span class="text-italic mar-l-15">Copy from: </span>
									<select name="copyFacts" id="copyFacts">
										<option value="-1" selected="selected">-- select ---</option>
										@foreach ($facts as $fact)
											<option value="{{ $fact->value }}">{{ $fact->name }}</option>
										@endforeach
									</select>
									<textarea placeholder="Enter Package Facts" name="pkgFacts" id="pkgFacts"
											class="editor-box form-control editor-html" rows="8">
											@if(old('pkgFacts')){{ old('pkgFacts') }}@elseif($package){!! $package['facts'] !!}@endif</textarea>
								</div>
							</td>
							<td>
								<div class="form-group">
									<label class="text-bold ">Prerequisites</label>
									<span class="text-italic pad-l-15">Copy from: </span>
									<select name="copyPreReq" id="copyPreReq">
										<option value="-1" selected="selected">-- select ---</option>
										@foreach ($preReqs as $req)
											<option value="{{ $req->value }}">{{ $req->name }}</option>
										@endforeach
									</select>
									<textarea name="pkgPrereq" class="form-control editor-html" id="pkgPrereq" rows="8"
											placeholder="Enter Prerequisites">
											@if(old('pkgPrereq')){{ old('pkgPrereq') }}@elseif($package){!! $package['prerequisites'] !!}@endif</textarea>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label for="pkgTerms" class="text-bold">Booking Terms</label>
								<span class="text-italic pad-l-15">Copy from: </span>
								<select name="copyTerms" id="copyTerms">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($terms as $term)
										<option value="{{ $term->value }}">{{ $term->name }}</option>
									@endforeach
								</select>
								<textarea name="pkgTerms" class="form-control editor-html" type="text" id="pkgTerms"
										rows="8"
										placeholder="Enter the terms of booking">
										@if(old('pkgTerms')){{ old('pkgTerms') }}@elseif($package){!! $package['terms'] !!}@endif</textarea>
							</td>
							<td>

							</td>
						</tr>
					</table>
					<div class="clearfix"></div>

					<!-- Showing dynamic fields to artist package only for surprises -->
					@if((request('occasion') && (request('occasion') == config("evibe.event.special_experience"))) || (isset($inputData['occasion']) && ($inputData['occasion'] == config("evibe.event.special_experience"))))
						<div class="">
							<div class="form-group">
								@if(count($dynamicFields) >= 1)
									@foreach($dynamicFields as $category => $dynamicField)
										<?php $count = 1 ?>
										@foreach($dynamicField as $field)
											@if($field['typeField'] == config('evibe.input.text') || $field['typeField'] == config('evibe.input.number'))
												@include('packages.dynamicFields.text')
											@elseif($field['typeField'] == config('evibe.input.textarea'))
												@include('packages.dynamicFields.textarea')
											@elseif($field['typeField'] == config('evibe.input.option'))
												@include('packages.dynamicFields.option')
											@elseif($field['typeField'] == config('evibe.input.radio'))
												@include('packages.dynamicFields.radio')
											@elseif($field['typeField'] == config('evibe.input.checkbox'))
												@include('packages.dynamicFields.checkbox')
											@endif

											@if(($count % 3) == 0)
												<div class="clearfix"></div>
											@endif
											<?php $count++ ?>
										@endforeach
										<div class="clearfix"></div>
									@endforeach
								@else
									<p class="text-center"><b>N/A</b></p>
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
					@endif

					<input type="hidden"
							value="@if(request('occasion')) {{ request('occasion') }} @else @if(isset($inputData['occasion'])){{ $inputData['occasion'] }}@endif @endif"
							name="occasion">
					<input type="hidden" value="{{ request('pageType') }}" name="typePackage">
					<input type="hidden" value="{{ request('type') }}" name="type">
					<input type="hidden" value="{{ request('vendorId') }}" name="vendorId">
					<input type="hidden" value="{{ request('venueId') }}" name="venueId">
					<input type="hidden" value="@if(is_null($package)){{ false }}@else{{ true }}@endif" name="isUpdate">
					<input type="hidden" value="@if(!is_null($package)){{ $package['id'] }}@endif" name="packageId">
					<input type="hidden"
							value="@if(isset($inputData['map_type_id'])){{ $inputData['map_type_id'] }}@endif"
							name="mapTypeId">
					<div class="btn-action col-lg-8 mar-t-10">
						<div class="col-lg-3 pull-right">
							<button type="submit" name="myBtn" id="myBtn" value="Save"
									class="pad-10 btn btn-success btn-block">
								<span class="glyphicon glyphicon-floppy-save"></span>
								@if(is_null($package)) Save @else Update @endif
							</button>
						</div>
						<div class="col-lg-3 pull-right">
							<a href="{{ route('packages.vendor.list') }}"
									class="pad-10 btn btn-danger btn-block">
								<i class="glyphicon glyphicon-remove"></i> Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/package/package.js");
		});
	</script>
@stop