@extends('layout.base')

@section('title')
	<title>View All Packages | EvibeDash</title>
@stop

@section('content')
	<div class="container page-content packages-warp">
		<div class="title-sec">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<!-- show custom success message begin -->
				@if (session()->has('custom_success'))
					<div class="alert alert-success alert-dismissible mar-t-10" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
						<span>{{ session('custom_success') }}</span>
					</div>
			@endif
			<!-- show custom success message end -->
				<div class="pad-t-20">

					<div class="col-md-6">
						<h2 class="panel-title">Showing list of all packages</h2>
						<!-- search from begin -->
						<div class="pull-left pad-t-10">
							<form id="searchForm" role="form" class="form form-horizontal">
								<div class="pull-left mar-r-10">
									<input id="searchQuery" type="text" name="query"
											class="form-control u-seach-box"
											placeholder="Search packages by code, name, price"
											value="{{ request()->input('query')}}"/>
								</div>
								<div class="pull-right">
									<input type="submit" class="btn btn-info" value="Search">
									@if (request()->has('query') && request()->input('query'))
										<a id="resetSearch" class="pad-l-3">Clear Search</a>
									@endif
								</div>
								<div class="clearfix"></div>
							</form>
						</div>
						<!-- search form end -->
					</div>

					<div class="col-md-6">
						<!-- sort options begin -->
						<div class="pull-right">
							<div class="">
								@if(AppUtil::isTabVisible('new-package'))
									<label for="typeTicketOptions">Select package type for copy</label>
									<select id="typeTicketOptions" name="typeTicketOptions" class="form-control in-blk">
										<option value="{{ config('evibe.ticket_type.planners') }}">Vendor</option>
										<option value="{{ config('evibe.ticket_type.venues') }}">Venue</option>
										<option value="{{ config('evibe.ticket_type.artists') }}">Artist</option>
									</select>
									@include('app.copy-data')
									<input type="hidden" name="typeTicketId" id="typeTicketId"
											value="{{ config('evibe.ticket_type.planners') }}">
								@endif
							</div>
							<div class="mar-t-5">
								<div class="u-sort-options packages-sort-options in-blk no-mar-r">
									<select id="sortOptions" name="sort" class="form-control">
										<option value="created-first"
												@if(request()->has('sort') && request()->input('sort') == 'created-first') selected @endif>Created: latest first
										</option>
										<option value="created-last"
												@if(request()->has('sort') && request()->input('sort') == 'created-last') selected @endif>Created: latest last
										</option>
										<option value="price-asc"
												@if(request()->has('sort') && request()->input('sort') == 'price-asc') selected @endif>Price: low to high
										</option>
										<option value="price-desc"
												@if(request()->has('sort') && request()->input('sort') == 'price-desc') selected @endif>Price: high to low
										</option>
									</select>
								</div>
								@if(AppUtil::isTabVisible('new-package'))
									<div class="mar-l-10 in-blk">
										<a href="{{ route('package.new.show') }}" class="btn in-blk btn-primary">
											<i class="glyphicon glyphicon-plus"></i>Add New Package
										</a>
									</div>
								@endif
							</div>
						</div>
						<!-- sort options end -->
					</div>

					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="mar-t-20">
			<div class="col-sm-3 col-md-3 col-lg-3 package-list-page">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="panel-title pull-left">Filter Options</h5>
						@if (request()->has('category') || request()->has('priceRange') || request()->input('location')
						 || request()->input('event') || request()->input('city') || request()->input('partner') || request()->input('typePackage'))
							<a href="{{route('packages.all.list')}}"
									class="btn btn-xs btn-warning pull-right">Reset Filter</a>
						@endif
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="price-filter-option">
								<label for="priceRangeMin">
									<span class="rupee-font">&#8377;</span> Price
								</label>
								<div>
									<div class="col-md-5 no-pad-l text-center">
										<input id="priceRangeMin" type="text" name="priceRangeMin"
												class="form-control" placeholder="Min."
												value="@if(count(($data['priceMinSelected']))){{ $data['priceMinSelected'] }}@endif">
									</div>
									<div class="col-md-5 no-pad-r text-center">
										<input id="priceRangeMax" type="text" name="priceRangeMax"
												class="form-control" placeholder="Max."
												value="@if(count($data['priceMaxSelected'])){{ $data['priceMaxSelected'] }}@endif">
									</div>
									<div class="col-md-2">
										<div class="in-blk btn btn-xs btn-danger btn-price-filter"><i
													class="glyphicon glyphicon-search"></i>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="city-filter-option">
								<label for="city">Select city</label>
								<select name="city" id="city" class="form-control">
									<option value="0"
											@if(request()->input('city') == "0") selected @endif>All
									</option>
									@foreach($data['cities'] as $city)
										<option value="{{ $city->id }}"
												@if(request()->input('city') == $city->id) selected @endif>{{ $city->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="event-filter-option">
								<label for="event">Select occasion</label>
								<select name="event" id="event" class="form-control">
									<option value="0"
											@if(request()->input('event') == "0") selected @endif>All
									</option>
									@foreach($data['occasions'] as $event)
										<option value="{{ $event->id }}"
												@if(request()->input('event') == $event->id) selected @endif>{{ $event->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="type-package-filter-option">
								<label for="typePackage">Select Provider Type</label>
								<select name="typePackage" id="typePackage" class="form-control">
									<option value="0"
											@if(request()->input('typePackage') == "0") selected @endif>All
									</option>
									<option value="{{ config('evibe.ticket_type.planners') }}"
											@if(request()->input('typePackage') == config('evibe.ticket_type.planners')) selected @endif>Vendor
									</option>
									<option value="{{ config('evibe.ticket_type.venues') }}"
											@if(request()->input('typePackage') == config('evibe.ticket_type.venues')) selected @endif>Venue
									</option>
									<option value="{{ config('evibe.ticket_type.artists') }}"
											@if(request()->input('typePackage') == config('evibe.ticket_type.artists')) selected @endif>Artist
									</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="selectSellingStatus">Select Selling Status</label>
							<select name="selectSellingStatus" id="selectSellingStatus" class="form-control">
								<option value="0">All</option>
								@foreach($data['typeSellingStatus'] as $sellingState)
									<option value="{{ $sellingState->id }}"
											@if(request()->input('sellingStatus')== $sellingState->id)selected @endif>{{ ucfirst(strtolower($sellingState->name)) }}</option>
								@endforeach
							</select>
						</div>
						<hr>
						<div class="form-group">
							<div class="partner-filter-option">
								<label for="partner">Select partner</label>
								<div>
									<div class="pull-left">
										<select name="partner" id="partner" class="form-control">
											<option value="0_0"
													@if(request()->input('partner') == "0_0") selected @endif>All
											</option>
											@foreach($data['planners'] as $partner)
												<option value="{{ config('evibe.ticket_type.planners')."_".$partner->id }}"
														@if(request()->input('partner') == (config('evibe.ticket_type.planners')."_".$partner->id)) selected @endif>{{ $partner->name }}</option>
											@endforeach
											@foreach($data['venues'] as $partner)
												<option value="{{ config('evibe.ticket_type.venues')."_".$partner->id }}"
														@if(request()->input('partner') == (config('evibe.ticket_type.venues')."_".$partner->id)) selected @endif>{{ $partner->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="pull-right btn btn-xs btn-danger btn-partner-search">
										<i class="glyphicon glyphicon-search"></i>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="form-group hide">
							<div class="location-filter-option">
								<label for="location">Select location</label>
								<div>
									<div class="pull-left">
										<select name="location" id="location" class="form-control">
											<option value="0"
													@if(request()->input('location') == "0") selected @endif>All
											</option>
											@foreach($data['locationList'] as $key => $location)
												<option value="{{ $key }}"
														@if(request()->input('location') == $key) selected @endif>{{ $location }}</option>
											@endforeach
										</select>
									</div>
									<div class="pull-right btn btn-xs btn-danger btn-location-search">
										<i
												class="glyphicon glyphicon-search"></i>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="tag-filter-option mar-t-10 text-center hide">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
									href="#collapseOne" aria-expanded="true"
									aria-controls="collapseOne"><i
										class="glyphicon glyphicon-plus"></i> Search by tags</a>
							<div id="collapseOne" class="panel-collapse collapse pad-t-10" role="tabpanel"
									aria-labelledby="headingOne">
								<div class="form-group text-left">
									@if($data['filterCategoryTag'])
										@foreach($data['filterCategoryTag']['filters'] as $catUrl => $catVal)
											@if(array_key_exists('subTags', $catVal))
												<div class="parent-tag">{{$catVal['name']}}</div>
												@foreach($catVal['subTags'] as $subTagUrl => $subTagsVal)
													<div class="mar-l-10">
														<label class="filter-tag-value">
															<input type="checkbox" name="subcategory"
																	value="{{$subTagsVal['id']}}"
																	@if(in_array($subTagsVal['id'],$data['categorySelected'])) checked="checked"@endif>
															<span>{{$subTagsVal['name']}}</span>
														</label>
													</div>
												@endforeach
											@else
												<div>
													<label class="filter-tag-value">
														<input type="checkbox" name="category"
																value="{{$catVal['id']}}"
																@if(in_array($catVal['id'],$data['categorySelected'])) checked="checked"@endif>
														<span class="parent-tag">{{$catVal['name']}}</span>
													</label>
												</div>
											@endif
										@endforeach
									@endif
								</div>
								<div class="form-group">
									<a class="btn btn-danger btn-sm btn-block btn-category-search">SEARCH</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-9">
				@if (count($data['packages']) > 0)
					<div class="packages-list">
						@foreach ($data['packages'] as $package)
							<div class="col-sm-3 no-pad-r">
								<div class="a-package">
									<div class="top">
										<a href="{{route('package.view', $package->id)}}">
											@php $url = $data['packageGallery']->where('planner_package_id', $package->id)->sortByDesc('is_profile')->sortByDesc('updated_at')->first(); @endphp
											@if($url && $package->map_type_id == config('evibe.ticket_type.venues'))
												@php $imgUrl = config('evibe.gallery.host') . '/venues/' . $package->map_id . '/images/packages/' . $package->id . '/'. $url->url; @endphp
											@elseif($url)
												@php $imgUrl = config('evibe.gallery.host') . '/planner/' . $package->map_id . '/images/packages/' . $package->id . '/'. $url->url; @endphp
											@else
												@php $imgUrl = config('evibe.gallery.host') . '/' . config('evibe.gallery.default_profile_pic.root') . '/' . config('evibe.gallery.default_profile_pic.package'); @endphp
											@endif
											<img class="profile-img"
													src="{{ $imgUrl }}">
										</a>
										<div class="status @if(!$package->is_live) inactive @endif">
											@if($package->is_live)
												@if ($package->provider && $data['cities']->where('id', $package->provider->city_id)->first())
													@if($package->is_live)
														<a href="http://evibe.in/{{ $data['cities']->where('id', $package->provider->city_id)->first()->url }}/birthday-party-package/{{ $package->url }}"
																class="btn btn-xs btn-success mar-l-10 pad-t-3"
																target="_blank">View Live</a>
													@endif
												@endif
											@else
												<i class="glyphicon glyphicon-remove-sign"></i> Inactive
											@endif
										</div>
									</div>
									<div class="pad-10 text-center">
										<div class="title">
											<a href="{{route('package.view',$package->id)}}">
												[{{ $package->code }}] {{ $package->name }}
											</a>
										</div>
										<div class="pad-t-3 pricing">
											@if ($package->price_worth)
												<del class="pad-r-6">
													<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($package->price_worth) }}
												</del>
											@endif
											<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($package->price) }}
											@if ($package->price_max && $package->price > $package->price)
												-
												<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($package->price_max) }}
											@endif

										</div>
									</div>
									<div class="provider">
										@if($package->provider)
											<a href="{{ $package->provider->getLocalLink() }}" target="_blank">
												<i class="glyphicon glyphicon-user"></i> {{ $package->provider->code }}
											</a>
										@else
											<span class="text-danger text-strike">Provider Deleted</span>
										@endif
									</div>
								</div>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
					<div class="pad-t-10 text-center">
						@if(!request()->input('query'))
							<div>{{ $data['packages']->appends($data['existingQueryParams'])->links() }}</div>
						@endif
					</div>
				@else
					<div class="alert alert-danger">
						No results found.
					</div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {

			function formUrl(currentParam, currentParamValue, ignorePage) {
				var ignorePage = (typeof ignorePage !== 'undefined') ? ignorePage : false;
				var loc = window.location;
				var url = loc.origin + loc.pathname + '?';
				var queryParams = loc.search.substr(1).split("&");
				var count = 0;
				var trimmedQueryParams = '';

				for (key in queryParams) {
					var cParam = queryParams[key].split("=");
					var qKey = cParam[0];
					var qVal = cParam[1];

					if (qKey == 'page' && ignorePage) {
						continue;
					}

					// form array
					if (!(currentParam instanceof Array)) currentParam = [currentParam];

					if (qKey && $.inArray(qKey, currentParam) == -1) {
						count++;
						trimmedQueryParams += queryParams[key] + '&';
					}
				}

				if (currentParamValue) url += currentParam + '=' + currentParamValue + '&';
				if (count > 0) url += trimmedQueryParams;

				return url.substr(0, url.length - 1);
			}

			// search results
			$('#searchForm').submit(function (event) {
				event.preventDefault();
				var searchQuery = $('#searchQuery').val();

				window.location = formUrl('query', searchQuery, true);
			});

			// sort results
			$('#sortOptions').change(function (event) {

				var sortVal = $('#sortOptions').val();
				window.location = formUrl('sort', sortVal, true);
			});

			// sort city
			$('#city').change(function (event) {
				event.preventDefault();

				var cityVal = $(this).val();
				window.location = formUrl('city', cityVal, true)
			});

			// sort by type package
			$('#typePackage').change(function (event) {
				event.preventDefault();

				var typePackageVal = $(this).val();
				window.location = formUrl('typePackage', typePackageVal, true)
			});

			// sort by event
			$('#event').change(function (event) {
				event.preventDefault();

				var eventVal = $(this).val();
				window.location = formUrl('event', eventVal, true)
			});

			// sort by partner
			$('.btn-partner-search').click(function (event) {
				event.preventDefault();

				var partnerVal = $('#partner').val();
				window.location = formUrl('partner', partnerVal, true)
			});

			// sort by location
			$('.btn-location-search').click(function (event) {
				event.preventDefault();

				var locationVal = $('#location').val();
				window.location = formUrl('location', locationVal, true)
			});

			// sort by selling status
			$('#selectSellingStatus').on('change', function (e) {
				e.preventDefault();

				var sellingStatusId = $('#selectSellingStatus').val();
				window.location = formUrl('sellingStatus', sellingStatusId, true)
			});

			// sort by location
			$('.btn-category-search').click(function (event) {
				event.preventDefault();

				categoryVal = "";

				$('input[name="category"]:checked').each(function () {
					categoryVal += $(this).val() + ',';
				});

				$('input[name="subcategory"]:checked').each(function () {
					categoryVal += $(this).val() + ',';
				});

				categoryVal = categoryVal.substring(0, categoryVal.length - 1);

				window.location = formUrl('category', categoryVal, true)
			});

			// sort by price
			$('.btn-price-filter').click(function (event) {
				event.preventDefault();

				var priceRangeMin = $('#priceRangeMin').val();
				var priceRangeMax = $('#priceRangeMax').val();
				if (priceRangeMin && priceRangeMax) {
					window.location = formUrl('priceRange', priceRangeMin + "," + priceRangeMax, true)
				}
				else {
					window.showNotyError("Please enter minimum price & maximum price.")
				}
			});

			// reset search
			$('#resetSearch').click(function (event) {
				event.preventDefault();
				window.location = formUrl('query', null);
			});

			$('#partner').selectize();
			$('#location').selectize();

			$('#typeTicketOptions').on('change', function (e) {
				e.preventDefault();
				var typeTicketId = $('#typeTicketOptions').val();
				$("#typeTicketId").val(typeTicketId);
			});
		});

	</script>
@stop