@extends('layout.base')

@section('title')
	<title>{{ $data['package']->code }} Package Details | Evibe</title>
@stop

@section('content')
<div class="container-fluid">
	<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
		<div class="app-tabs">
			@include('app/tabs')
		</div>
		<ol class="breadcrumb">
			<li><a href="/package">Show All Packages</a></li>
		</ol>
		<div class="page-content">
			<div class="vendor-details-cnt">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="pull-left">
							<h2 class="panel-title">Package Details</h2>
						</div>
						<div class="pull-right">
							<a href="/package/edit/{{ $data['package']->id }}" class="btn btn-sm btn-danger">
								<i class="glyphicon glyphicon-pencil"></i>
								<span>Edit</span>
							</a>
							<a href="/package" class="btn btn-sm btn-link">
								<i></i>
								<span>Back</span>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">
						<div class="package-data-cnt">
							<form action="#" role="form" class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 item-label">Name</label>
									<div class="col-sm-10">
										<span>{{ $data['package']->name }}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Information</label>
									<div class="col-sm-10">
										<span>{!! $data['package']->info !!}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Price</label>
									<div class="col-sm-10">
										<span>Rs. {{ AppUtil::formatPrice($data['package']->price) }}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Provider</label>
									<div class="col-sm-10">
										<a href="/vendors/view/{{ $data['package']->provider->id }}">
											{{ $data['package']->provider->name }}
										</a>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">URL</label>
									<div class="col-sm-10">
										<span>{{ $data['package']->url }}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Priority</label>
									<div class="col-sm-10">
										<span>{{ $data['package']->priority }}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">City</label>
									<div class="col-sm-10">
										<span>{{ $data['package']->city->name }}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Services</label>
									<div class="col-sm-10">
										@if (count($data['package']->services) > 0)
											@foreach ($data['package']->services as $packageService)
												<div class="package-service">
													<strong>{{ $packageService->service->alt_name }}</strong>
													<div>{!! $packageService->info !!}</div>
												</div>
											@endforeach
										@endif
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Category Tags</label>
									<div class="col-sm-10">
										<input type="text" name="pcategorytags" id="pcategorytags" value="{{ $data['package']->getTagsString(1) }}" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Service Tags</label>
									<div class="col-sm-10">
										<input type="text" name="pservicetags" id="pservicetags" value="{{ $data['package']->getTagsString(0) }}" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Photos</label>
									<div class="col-sm-10">
										@if (count(AppUtil::filterGalleryItems($data['package']->gallery, 0)) == 0)
											<div>No images found</div>
										@endif
										@foreach ($data['package']->gallery as $galleryItem)
											@if ($galleryItem->type == 0)
												<img class="gallery-item" alt="{{ $galleryItem->title }}"
													src="{{ AppUtil::getGalleryBaseUrl() . '/package/' . $data['package']->id . '/images/' . $galleryItem->url }}" >
											@endif
										@endforeach
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 item-label">Videos</label>
									<div class="col-sm-10">
										@if (count(AppUtil::filterGalleryItems($data['package']->gallery, 1)) == 0)
											<div>No videos found</div>
										@endif
										@foreach ($data['package']->gallery as $galleryItem)
											@if ($galleryItem->type == 1)
												<embed width="420" height="100" src="http://www.youtube.com/v/{{ $galleryItem->url }}" type="application/x-shockwave-flash">
											@endif
										@endforeach
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-2"></div>
									<div class="col-sm-10">
										<a href="/package/edit/{{ $data['package']->id }}" class="btn btn-danger">
											<i class="glyphicon glyphicon-pencil"></i>
											<span>Edit</span>
										</a>
										<a href="/package" class="btn btn-link">
											<i></i>
											<span>Back</span>
										</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
	$(document).ready(function() {

		// category tags
		$('#pcategorytags').tagit({
			readOnly: true
		});

		// service tags
		$('#pservicetags').tagit({
			readOnly: true,
		});
	});
</script>
@stop