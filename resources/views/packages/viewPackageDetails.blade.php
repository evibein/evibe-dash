@extends('layout.base')
@section('content')
	<div class="container pkg-container package-page-content">
		<div class="mar-b-10">
			<a href="{{ route('packages.all.list') }}"><< Show all Packages</a>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="inner">
					<div class="pull-left in-blk">
						<h3 class="mar-t-5 in-blk">[{{ $package->code }}] <span>{{ $package->name }} </span></h3>
						@if ($package->event)
							<div class="font-14 pad-10 alert-info in-blk mar-l-5">{{ $package->event->name }}
								@if($package->map_type_id == config('evibe.ticket_type.venues')) (Venue) @else {{ "(Non-venue)" }} @endif</div>
						@endif
					</div>
					<div class=" pull-right">
						@php $sellingStatus = $sellingStatus ? $typeSellingStatus->find($sellingStatus->product_selling_status_id) : $typeSellingStatus->find(config("evibe.selling.status.default")); @endphp
						<div class="font-18 top-header-bar form-group no-mar in-blk">
							<div class="tkt-lead-status-wrap ticket-header-info-status-{{ $sellingStatus ? strtolower($sellingStatus->name) : "medium"}}">
								<select name="ticketLeadStatus" id="optionSellingStatus" class="form-control"
										data-url="{{ route("selling.status.update", [config("evibe.ticket_type.packages"), $package->id]) }}">
									@foreach($typeSellingStatus as $typeStatus)
										<option value="{{ $typeStatus->id }}"
												data-color="{{ strtolower($typeStatus->name) }}"
												@if($sellingStatus && ($sellingStatus->id == $typeStatus->id)) selected @endif>{{ $typeStatus->name }}
										</option>
									@endforeach
								</select>
							</div>
						</div>
						@if($package->is_live)
							<a href="@if($package->provider) {{ AppUtil::getLiveUrl(config('evibe.ticket_type.packages'), $package) }} @endif "
									target="_blank"
									class="btn btn-sm btn-primary mar-r-10"
									@if(!$package->provider) disabled="disabled" @endif>
								<i class="glyphicon glyphicon-eye-open"></i> View Live
							</a>
						@endif
						@if($approvalButton)
							<a href="{{ route('approval.accept',[$package->id,config('evibe.ticket_type.packages')]) }}"
									class="btn btn-success btn-sm mar-r-10"
									onclick="return confirm('Are you sure?','Yes','No')"><i
										class="glyphicon glyphicon-thumbs-up"></i> Accept Approval</a>
							<button id="approvalRejectBtn"
									data-url="{{ route('approval.reject',[$package->id,config('evibe.ticket_type.packages')]) }}"
									class="btn btn-danger btn-sm mar-r-10"><i
										class="glyphicon glyphicon-thumbs-down"></i> Reject Approval
							</button>
						@endif
						@if((AppUtil::isAdmin() || AppUtil::isTabVisible('activate_options') || AppUtil::isSrBd()) && !$approvalButton)
							@if($package->is_live)
								@if(AppUtil::isSuperAdmin())
									<a href="{{route('package.deactivate',$package->id)}}"
											class="mar-r-10 btn btn-sm btn-warning"
											onclick="return confirm('Are you sure?');">
										<i class="glyphicon glyphicon-off"></i> Deactivate</a>
								@endif
							@else
								<a href="{{route('package.activate',$package->id)}}"
										class="mar-r-10 btn btn-sm btn-success"
										onclick="return confirm('Are you sure?');">
									<i class="glyphicon glyphicon-off"></i> Activate</a>
							@endif
						@endif
						@if($askApprovalButton)
							<a id="askApproval"
									data-url="{{ route('approval.ask',[$package->id,config('evibe.ticket_type.packages')]) }}"
									class="btn btn-warning btn-sm mar-r-10"
									data-askreason="@if($isApprovalReason) 1 @else 0 @endif"><i
										class="glyphicon glyphicon-question-sign"></i> Ask Approval</a>
							@if (AppUtil::isTabVisible('packages::edit_info'))
								<a href="{{ route('package.edit',$package->id) }}"
										class="btn btn-primary btn-sm mar-r-10">
									<i class="glyphicon glyphicon-edit"></i> Edit
								</a>
							@endif
						@elseif (AppUtil::isTabVisible('packages::edit_info'))
							<a href="{{ route('package.edit',$package->id) }}"
									class="btn btn-primary btn-sm mar-r-10">
								<i class="glyphicon glyphicon-edit"></i> Edit
							</a>
						@endif
						@if (AppUtil::isAdmin())
							@if(AppUtil::isSuperAdmin())
								<a data-url="{{route('package.delete',$package->id)}}"
										data-redirect="{{route('packages.all.list')}}"
										class="btn btn-sm btn-danger btn-pkg-delete">
									<i class="glyphicon glyphicon-trash "></i>
								</a>
							@endif
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel-body">
				@if(session()->has('successMsg'))
					<div class="alert alert-success alert-dismissible">
						<a class="close mar-l-5" data-dismiss="alert">&times;</a>
						<span>{{ session('successMsg') }}</span>
					</div>
				@endif
				@if (session()->has('imageError'))
					<div class="alert alert-danger alert-dismissible">
						<a class="close mar-l-5" data-dismiss="alert">&times;</a>
						<span>{{ session('imageError') }}</span>
					</div>
				@endif

				<section class="data-sec pad-b-20">
					<div class="sec-body">
						<div class="sec-head">
							<h4 class="in-blk">Internal Questions</h4>
							<a class="btn btn-sm btn-info mar-r-10 iq in-blk pull-right"
									data-url="{{route('iq.add',[ 'mapId' => $package->id,'mapTypeID' => config('evibe.ticket_type.packages')])}}"
									data-redirect="{{route('package.view',$package->id)}}"
									data-id="{{$package->id}}">

								<i class="glyphicon glyphicon-plus"></i> Add IQ
							</a>
						</div>
						<section class="data-sec pad-b-10 pad-t-10" style="background-color: #f5f5f5">
							@if ($iqs->count())
								<ul class="iq-list no-mar no-pad">
									@foreach($iqs as $iq)
										<li class="@if ($iq->deleted_at) strike @endif">
											<div class="pull-left">
												<div class="iq-q">{!! $iq->question !!}</div>
												<div class="iq-ans">{!! $iq->answer !!}</div>
												<div class="iq-info">
													<i>{{ date('j F, g:i a',strtotime($iq->created_at)) }} by
														<b>{{ $iq->user ? $iq->user->name:"--" }}</b>
														@if ($iq->deleted_by)
															Deleted by
															<b>{{ $iq->deletedBy ? $iq->deletedBy->name:"--" }}</b>
														@endif
													</i>
												</div>
											</div>
											<div class="pull-right">
												@if (!$iq->deleted_at)
													<button class=" btn btn-sm btn-primary in-blk iq-edit"
															data-url="{{route('iq.edit',$iq->id)}}"
															data-question="{!! $iq->question !!}"
															data-redirect="{{route('package.view',$package->id)}}"
															data-id="{{$iq->id}}">
														<i class="glyphicon glyphicon-edit"></i>
													</button>
													<button class=" btn btn-sm btn-danger in-blk iq-delete"
															data-url="{{route('iq.delete',$iq->id)}}"><i
																class="glyphicon glyphicon-trash"></i>
													</button>
												@endif
											</div>
											<div class="clearfix"></div>
										</li>
									@endforeach
								</ul>
							@else
								<div class="text-danger">No internal questions were added yet. Click on
									<b>Add IQ</b> button to create first internal question.
								</div>
							@endif
						</section>
					</div>
				</section>
				<!--Internal questions end-->

				<?php $packageProvider = $package->provider; ?>
			<!-- price location header begins-->
				<section class="data-sec pad-b-20">
					<div class="text-center" style="background-color: #f5f5f5">
						<div class="pricing in-blk">
							<div class="in-blk">
								<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($package->price) }}
								@if ($package->price_max && $package->price_max > $package->price_max)
									-
									<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($package->price_max) }}
								@endif
							</div>
							@if ($package->price_worth)
								<div class="worth in-blk">
									<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($package->price_worth) }}
								</div>
							@endif
						</div>
						@if($packageProvider)
							<div class="in-blk font-22 text-bold pad-l-20">
								<i class="glyphicon glyphicon-user"></i>
								<a href="{{ $packageProvider->getLocalLink() }}" target="_blank" style="color: black">
									{{$packageProvider->name}}
								</a>
								<a class="in-blk btn btn-link update-package-provider no-pad-l"
										data-url="{{ route('package.provider.edit', $package->id) }}">(Change)
								</a>
							</div>
						@endif
						<div class="location in-blk">
							<i class="glyphicon glyphicon-map-marker"></i>
							<b>
								@if($package->map_type_id == config('evibe.ticket_type.venues'))
									@if($package->area && $package->city){{ $package->area->name .", ". $package->city->name }} @else
										<span class="text-danger text-strike text-bold font-22">Provider Deleted</span>@endif
								@else
									@if($packageProvider && $packageProvider->area && $packageProvider->city){{ $packageProvider->area->name .", ". $packageProvider->city->name }} @else
										<span class="text-danger text-strike text-bold font-22">Provider Deleted</span>@endif
								@endif
							</b>
						</div>
					</div>

					<div class="alert-message-wrap mar-b-6 mar-t-10">
						<b>
							@if($askApprovalAlert)
								{!! $askApprovalAlert !!}
							@elseif($isActivateAlert)
								{!! $isActivateAlert !!}
							@endif
							@if($rejectionComment)
								{!! $rejectionComment !!}
							@endif
						</b>
					</div>
					@if($package->map_type_id == config('evibe.ticket_type.venues'))
						<div class="mar-t-15 mar-b-15">
							<table class="table table-bordered alert-default">
								<tr>
									<td colspan="3">
										<div class="text-center font-20 text-bold">Venue Information</div>
									</td>
								</tr>
								<tr>
									<td colspan="3" class="font-16">
										<label class="mar-r-4">Address :</label> {!! $package->full_address !!},
										<label class="mar-r-4">Landmark: </label> {!! $package->landmark !!},
										<label class="mar-r-4">Pin Code: </label> {{$package->zip}}
									</td>
								</tr>
							</table>
						</div>
					@endif
					@if ($package->range_info)
						<div class="pad-t-5 alert alert-info alert-range-info">
							<h5 class="no-mar text-muted">
								<i class="glyphicon glyphicon-info-sign"></i> Price range information
							</h5>
							<div class="pad-t-10">{!! $package->range_info !!}</div>
						</div>
					@endif
				</section>
				<!--Pricing, location header end-->

				<!-- surprise tags section begin -->
				<?php $parentTagNames = ['Surprises_Relationship', 'Surprises_Occasion', 'Surprises_Age_Group']; ?>
				@if($package->event_id == config("evibe.event.special_experience"))
					<section class="data-sec pad-b-4" id="tags">
						<div class="sec-head">
							<h4 class="in-blk mar-r-10">Surprise Category Tags</h4>
							(<a id="surpriseValidTags">View Valid relationship tags</a>)
						</div>
						@if (AppUtil::isTabVisible('packages::edit_tags'))
							<div class="" style="background-color: #f5f5f5">
								<form action="{{ route('package.tags.surprises.add', $package->id) }}" method="POST"
										class="form-inline pad-t-10" id="addSurpriseTagsForm">
									<div class="col-sm-11 no-pad-r">
										<div class="form-group col-sm-4 no-pad-l">
											<label for="selectRelationShipTag">Select Relationship</label>
											@if(array_key_exists(config('evibe.tags.surprise_relation'), $tagList))
												<select id="selectRelationShipTag" name="selectRelationShipTag[]"
														class="form-control">
													<option value="">Select multiple relations supported...</option>
													@foreach ($tagList[config('evibe.tags.surprise_relation')]["childs"] as $key=>$tag)
														<option value="{{ $key }}">{{ $tag['name'] }}</option>
													@endforeach
												</select>
											@endif
										</div>
										<div class="form-group col-sm-4 no-pad-l">
											<label for="selectOccasionTag">Select Occasion</label>
											@if(array_key_exists(config('evibe.tags.surprise_occasion'), $tagList))
												<select id="selectOccasionTag" name="selectOccasionTag[]"
														class="form-control">
													<option value="">Select multiple occasions supported...</option>
													@foreach ($tagList[config('evibe.tags.surprise_occasion')]["childs"] as $key=>$tag)
														<option value="{{ $key }}">{{ $tag['name'] }}</option>
													@endforeach
												</select>
											@endif
										</div>
										<div class="form-group col-sm-4 no-pad-l">
											<label for="selectAgeGroupTag">Select Age Group</label>
											@if(array_key_exists(config('evibe.tags.surprise_age_group'), $tagList))
												<select id="selectAgeGroupTag" name="selectAgeGroupTag[]"
														class="form-control">
													<option value="">Select multiple age groups supported...</option>
													@foreach ($tagList[config('evibe.tags.surprise_age_group')]["childs"] as $key=>$tag)
														<option value="{{ $key }}">{{ $tag['name'] }}</option>
													@endforeach
												</select>
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="col-sm-1 no-pad">
										<div class="form-group mar-t-20">
											<button type="button"
													class="btn btn-sm btn-warning in-blk btn-surprise-add-tag">
												<i class="glyphicon glyphicon-plus"></i> Add Tags
											</button>
										</div>
									</div>
									<div class="clearfix"></div>
									<input type="hidden" name="packageId" value="{{ $package->id }}">
								</form>
								<div class="sec-body pad-10">
									@if (count($surCatTags))
										<ul class="ls-none no-mar no-pad tags-list">
											To Whom :
											@foreach ($surCatTags->where('parent_id', intval(config('evibe.tags.surprise_relation'))) as $tag)
												<li class="label-tag">{{ $tag->name }}
													<a href="{{ route('package.tag.delete', [$package->id, $tag->id]) }}"
															class="text-danger">
														<i class="glyphicon glyphicon-trash"></i>
													</a>
												</li>
											@endforeach
											<br>
											Which Occasion :
											@foreach ($surCatTags->where('parent_id', intval(config('evibe.tags.surprise_occasion'))) as $tag)
												<li class="label-tag">{{ $tag->name }}
													<a href="{{ route('package.tag.delete', [$package->id, $tag->id]) }}"
															class="text-danger">
														<i class="glyphicon glyphicon-trash"></i>
													</a>
												</li>
											@endforeach
											<br>
											Age Group :
											@foreach ($surCatTags->where('parent_id', intval(config('evibe.tags.surprise_age_group'))) as $tag)
												<li class="label-tag">{{ $tag->name }}
													<a href="{{ route('package.tag.delete', [$package->id, $tag->id]) }}"
															class="text-danger">
														<i class="glyphicon glyphicon-trash"></i>
													</a>
												</li>
											@endforeach
										</ul>
									@else
										<div class="alert alert-danger">
											<i class="glyphicon glyphicon-warning-sign"></i> No surprise tags were assigned for this package style.
										</div>
									@endif
								</div>
							</div>
						@endif
					</section>

					<!-- view valid surprise tags modal-->
					<div id="modalViewSurpriseTagsModal" class="modal fade" tabindex="-1" role="dialog">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Valid Surprise Tags</h4>
								</div>
								<div class="modal-body">
									<table class="table table-bordered">
										@foreach($sTags as $key => $tag)
											<tr>
												<td>
													{{ $key }}
												</td>
												<td>
													@foreach($tag as $subTag)
														<div class="alert-info in-blk pad-l-3 pad-r-3 mar-r-4">{{ $subTag }} </div>
													@endforeach
												</td>
											</tr>
										@endforeach
									</table>
									<div class="text-center">
										<button type="button" class="btn btn-danger"
												data-dismiss="modal">Close
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
			@endif
			<!-- surprise tags section end -->

				<!-- gallery begin -->
				<section class="data-sec pad-b-20">
					<div class="sec-body">
						<div class="sec-head">
							<h4 class="in-blk">Images</h4>
							<a class="btn btn-link no-pad-l" role="button" data-toggle="collapse"
									href="#collapseGallery"
									aria-expanded="false" aria-controls="collapseGallery">(Add Images)
							</a>
						</div>
						@if ($images->count() || $videos->count())
							<section class="data-sec pad-b-20">
								@if($images->count())
									<ul class="ls-none no-mar no-pad images-list">
										@foreach($images as $image)
											<li class="image-cnt">
												<img class="an-image" src="{{ AppUtil::getPackageGalleryUrl($image) }}">
												<div class="text-center pad-t-3">
													<div class="img-title">
														<div class="view">
															<div class="image-link"
																	title="{{ $image->title }}">
																<a href="{{ AppUtil::getPackageGalleryUrl($image) }}"
																		target="_blank">{{ $image->title }}</a>
															</div>
														</div>
														<div class="edit hide">
															<form action="{{ route('package.gallery.title', [$package->id, $image->id]) }}"
																	method="POST">
																<input type="text" name="title"
																		placeholder="type image title..."
																		class="form-control"
																		value="{{ $image->title }}">
																<button class="btn btn-xs btn-success">
																	<i class="glyphicon glyphicon-check"></i> Save
																</button>
																<button class="btn btn-link btn-img-title-edit-cancel">Cancel</button>
															</form>
														</div>
													</div>
												</div>
												@if (AppUtil::isTabVisible('packages::edit_gallery'))
													<div class="img-actions">
														@if ($image->is_profile)
															<a href="{{ route('package.gallery.unset', [$package->id, $image->id]) }}"
																	class="btn btn-xs btn-link">
																<i class="glyphicon glyphicon-remove"></i> Unset Profile
															</a>
														@else
															<a href="{{ route('package.gallery.set', [$package->id, $image->id]) }}"
																	class="btn btn-xs btn-link">
																<i class="glyphicon glyphicon-ok"></i> Set Profile
															</a>
														@endif
														<a class="btn btn-xs btn-link btn-edit-img-title">
															<i class="glyphicon glyphicon-pencil"></i> Edit
														</a>
														<a href="{{ route('package.gallery.delete', [$package->id, $image->id]) }}"
																class="btn btn-xs btn-link"
																onclick="return confirm('Are you sure?');">
															<i class="glyphicon glyphicon-trash"></i> Del
														</a>
													</div>
												@endif
												@if ($image->is_profile)
													<div class="profile-label label label-success">
														<i class="glyphicon glyphicon-star"></i> Profile
													</div>
												@endif
											</li>
										@endforeach
									</ul>
								@endif
								@if($videos->count())
									<div class="sec-head">
										<h4 class="in-blk text-danger">Videos</h4>
									</div>
									<div>
										@foreach($videos as $video)
											<div class="col-sm-4">
												<div class="mar-b-20">
													<iframe height="315" width="100%" class="no-pad"
															src="https://www.youtube.com/embed/{{ $video->url }}">
													</iframe>
													<a href="{{ route('package.gallery.delete', [$package->id, $video->id]) }}"
															class="btn btn-xs btn-danger btn-block pad-5"
															onclick="return confirm('Are you sure?');">
														<i class="glyphicon glyphicon-trash"></i> Delete
													</a>
												</div>
											</div>
										@endforeach
										<div class="clearfix"></div>
									</div>
								@endif
							</section>
						@else
							<div class="alert alert-danger">
								<i class="glyphicon glyphicon-warning-sign"></i> No images/videos were uploaded for this Package,
								<a class="btn btn-link no-pad-l pad-r-6" role="button" data-toggle="collapse"
										href="#collapseGallery"
										aria-expanded="false"
										aria-controls="collapseGallery">Click here</a>to add images/videos.
							</div>
					@endif

					<!-- upload new images begin -->
						<div class="collapse" id="collapseGallery">
							<div class="well">
								@if (AppUtil::isTabVisible('packages::edit_gallery'))
									<div class="upload-img-wrap">
										<form action="{{ route('package.gallery.upload', $package->id) }}"
												method="POST" role="form" enctype="multipart/form-data" class="form">
											<div class="mar-b-10">
												<label>
													<input type="radio" name="gal-type"
															value="{{ config('evibe.gallery.type.image') }}"
															@if(old('gal-type') == 0) checked
															@elseif(old('gal-type') !=1) checked @endif> Image
												</label>
												<label class="mar-l-10">
													<input type="radio" name="gal-type"
															value="{{ config('evibe.gallery.type.video') }}"
															@if(old('gal-type') == 1) checked
															@endif @if(session('type') == 'video')  checked @endif> Video
												</label>
											</div>
											<div class="">
												<div class="gal-type-image">
													( Valid formats: JPG, jpeg, jpg and png. Maximum file size:
													<b><u>5 MB</u></b> )
													<div class="form-group">
														<input type="file" name="packageImages[]" multiple="true"/>
													</div>
													<div class="form-group">
														<label for="ignoreWatermark" class="text-normal text-black">
															<input type="checkbox" id="ignoreWatermark"
																	name="ignoreWatermark"> Ignore Watermark
														</label>
													</div>
												</div>
												<div class="gal-type-video hide">
													<div class="mar-b-15">
														<input type="text" name="video" class="form-control"
																placeholder="Enter youtube video link"
																value="{{ old('video') }}">
													</div>
												</div>
												<button type="submit" class="btn btn-sm btn-warning">
													<i class="glyphicon glyphicon-plus"></i> Upload
												</button>
											</div>
										</form>
									</div>
								@endif
							</div>
						</div>
					</div>
				</section>
				<!-- gallery end -->

				<section class="data-sec pad-b-4" id="info">
					<div class="sec-head">
						<h4 class="in-blk mar-r-10">Details</h4>
					</div>
					<div class="sec-body">
						<div>
							<div class="col-sm-6 no-pad-l">
								<table class="table table-striped table-bordered ">
									<tr>
										<td><label>Free KMs</label></td>
										@if ($package->kms_free && $package->kms_free >0)
											<td>{{ $package->kms_free }} KMs</td>
										@else
											<td> --</td>
										@endif
									</tr>
									<tr>
										<td><label>Max KMs</label></td>
										@if ($package->kms_max && $package->kms_max >0)
											<td>{{ $package->kms_max }} KMs</td>
										@else
											<td> --</td>
										@endif
									</tr>
									<tr>
										<td><label>Transport Charges</label></td>
										<td>
											@if (!$package->trans_min)
												Free
											@else
												<span class="rupee-font">&#8377;</span> {{ $package->trans_min }}
												@if ($package->trans_max && $package->trans_max > $package->trans_min)
													- <span class="rupee-font">&#8377;</span> {{ $package->trans_max }}
												@endif
											@endif
										</td>
									</tr>
									<tr>
										<td><label>Setup Time</label></td>
										@if ($package->time_setup)
											<td>{{ $package->time_setup }} Hrs</td>
										@else
											<td> --</td>
										@endif
									</tr>
									<tr>
										<td><label>Rent Duration</label></td>
										@if ($package->time_duration)
											<td>{{ $package->time_duration }} Hrs</td>
										@else
											<td> --</td>
										@endif
									</tr>
									@if($package->map_type_id == config('evibe.ticket_type.venues'))
										<tr>
											<td><label>Book Before Hrs</label></td>
											@if ($package->book_before_hrs)
												<td>{{ $package->book_before_hrs }} Hrs</td>
											@else
												<td> --</td>
											@endif
										</tr>
										<tr>
											<td><label>Table Setup</label></td>
											@if ($package->table_setup)
												<td>{{ $package->table_setup }}</td>
											@else
												<td> --</td>
											@endif
										</tr>
									@endif
								</table>
							</div>
							<div class="col-sm-6 no-pad-l">
								<table class="table table-striped table-bordered ">
									<tr>
										<td>
											<label>Group Count</label>
										</td>
										<td>
											@if($package->group_count && $package->group_count >0)
												{{$package->group_count }}
											@else
												--
											@endif
										</td>
									</tr>
									<tr>
										<td>
											<label>Price / Person (Extra Guest)</label>
										</td>
										<td>
											@if($package->price_per_extra_guest && $package->price_per_extra_guest >0)
												{{$package->price_per_extra_guest }}
											@else
												--
											@endif
										</td>
									</tr>
									<tr>
										<td>
											<label>Type Package</label>
										</td>
										<td>
											@if($package->type_ticket_id)
												@php
													$key = array_search($package->type_ticket_id,array_column($typeTicket,'id'));
													$data = $typeTicket;
												@endphp
												{{$typeTicket[$key]['name']}}
												<input type="hidden" id="packageId" name="packageId"
														value="{{$package->id}}">
												<button class="btn btn-warning btn-sm pad-r-10 change-type-ticket">Change</button>
											@else
												{{"Not set"}}
											@endif
										</td>
									</tr>
									@if($package->check_in)
										<tr>
											<td><label>Check-in</label></td>
											<td>{{$package->check_in}}</td>
										</tr>
									@endif
									@if($package->check_out)
										<tr>
											<td><label>Check-out</label></td>
											<td>{{$package->check_out}}</td>
										</tr>
									@endif
									@if($package->map_type_id == config('evibe.ticket_type.venues'))
										<tr>
											<td><label>Guests (Min-Max)</label></td>
											@if ($package->guests_min && $package->guests_max)
												<td>{{ $package->guests_min }} - {{$package->guests_max}}</td>
											@else
												<td> --</td>
											@endif
										</tr>
										<tr>
											<td><label>Guests Kid (Min-Max)</label></td>
											@if ($package->guests_kid_min && $package->guests_kid_max)
												<td>{{ $package->guests_kid_min }} - {{$package->guests_kid_max}}</td>
											@else
												<td> --</td>
											@endif
										</tr>
										<tr>
											<td><label>Property Type</label></td>
											@if ($package->hall_type_id)
												<td>{{ $package->property->name }}.</td>
											@else
												<td> --</td>
											@endif
										</tr>
									@endif
								</table>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 no-pad-l">
							<div class="sub-sec mar-b-20">
								<h5 class="no-mar text-muted">INFO</h5>
								@if(($package->info))
									<div class="pad-t-10">{!! $package->info !!}</div>
								@endif
							</div>
							@if ($package->prerequisites)
								<div class="sub-sec mar-b-20">
									<h5 class="no-mar text-muted">PREREQUISITES</h5>
									<div class="pad-t-10">{!! $package->prerequisites !!}</div>
								</div>
							@endif
							@if ($package->facts)
								<div class="sub-sec mar-b-20">
									<h5 class="no-mar text-muted">FACTS</h5>
									<div class="pad-t-10">{!! $package->facts !!}</div>
								</div>
							@endif
							@if ($package->terms)
								<div class="sub-sec mar-b-20">
									<h5 class="no-mar text-muted">TERMS</h5>
									<div class="pad-t-10">{!! $package->terms !!}</div>
								</div>
							@endif
							@if($package->map_type_id == config('evibe.ticket_type.venues'))
								<div class="sub-sec mar-b-20">
									<h5 class="no-mar text-muted">TAG LINE</h5>
									@if ($package->package_tag_line)
										<div class="pad-t-10">{!! $package->package_tag_line !!}</div>
									@else
										--
									@endif
								</div>
								<div class="sub-sec mar-b-20">
									<h5 class="no-mar text-muted">ABOUT PACKAGE / VENUE</h5>
									@if ($package->about_package)
										<div class="pad-t-10">{!! $package->about_package !!}</div>
									@else
										--
									@endif
								</div>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
				</section>
				<section>
					@if(count($dynamicFields) >0)
						<div class="alert panel-default">
							<div class="text-center occasion-header">
								@if($package->event) {{$package->event->name}} Specific @else <span
										class="text-strike text-danger"> Event Not Found </span>@endif
							</div>
							<hr>

							@foreach($dynamicFields as $catField=>$catValues)
								<?php $i = 0?>
								<div class="text-danger font-20 ">{!! $catField !!}</div>
								@foreach($catValues as $catValue)
									@if($i ==3)
										<div class="clearfix"></div>
									@endif
									<div class="card-items">
										<div class="name">{{$catValue['name']}}</div>
										<div class="in-blk pad-10">{{$catValue['value']}}</div>
									</div>
									<?php $i++ ?>
								@endforeach
								<div class="clearfix"></div>
							@endforeach
						</div>
					@endif
				</section>
				<!-- description end -->

				<!-- tags section begin -->
				<section class="data-sec pad-b-4" id="tags">
					<div class="sec-head">
						<h4 class="in-blk">Tags</h4>
						<a class="btn btn-link no-pad-l" role="button" data-toggle="collapse" href="#collapseTags"
								aria-expanded="false" aria-controls="collapseTaga">(Add Tags)
						</a>
					</div>
					<div class="sec-body">
						@if (count($tags))
							<ul class="ls-none no-mar no-pad tags-list">
								@foreach ($tags as $key => $tag)
									@if(!in_array(explode(" ",$tag)[0], $parentTagNames))
										<li class="label-tag">{{ $tag }}
											<a href="{{ route('package.tag.delete', [$package->id, $key]) }}"
													class="text-danger">
												<i class="glyphicon glyphicon-trash"></i>
											</a>
										</li>
									@endif
								@endforeach
							</ul>
						@else
							<div class="alert alert-danger">
								<i class="glyphicon glyphicon-warning-sign"></i> No tags were assigned for this package style, Please
								<a class="btn btn-link no-pad-l pad-r-6" role="button" data-toggle="collapse"
										href="#collapseTags"
										aria-expanded="false" aria-controls="collapseTaga">Click here</a>to add.
							</div>
						@endif
					</div>
					<div class="collapse" id="collapseTags">
						<div class="well">
							@if (AppUtil::isTabVisible('packages::edit_tags'))
								<div class="upload-img-wrap">
									<div>You can add a new tag by just selecting a parent tag (or) both parent tag and child tag.</div>
									<form action="{{ route('package.tags.add', $package->id) }}" method="POST"
											class="form-inline pad-t-10">
										<div class="form-group">
											<select id="selectParentTag" name="selectParentTag" class="form-control">
												<!-- @todo: need to change logic in controller, if more tags need to be negated based on the occasion -->
												<option value="-1">--select parent tag--</option>
												@foreach ($tagList as $key=>$tag)
													@if($key == config('evibe.tags.surprise_relation') || $key == config('evibe.tags.surprise_occasion') || $key == config('evibe.tags.surprise_age_group'))
													@else
														<option value="{{ $key }}">{{ $tag['name'] }}</option>
													@endif
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<select id="selectChildTag" name="selectChildTag" class="form-control">
												<option value="-1">--select child tag--</option>
											</select>
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-sm btn-warning in-blk">
												<i class="glyphicon glyphicon-plus"></i> Add Tag
											</button>
										</div>
									</form>
								</div>
							@endif
						</div>
					</div>
				</section>
				<div class="hide">
					<select id="duplicateChildTags">
						@foreach($tagList as $key => $tag)
							@foreach ($tag['childs'] as $childKey => $childTag)
								<option value="{{ $childKey }}"
										data-parentid="{{ $key }}">{{ $childTag['name'] }}</option>
							@endforeach
						@endforeach
					</select>
				</div>
				<!-- tags section end -->

				<!--FAQ start -->
				<section class="data-sec pad-b-4" id="faqs">
					<div class="sec-head">
						<h4 class="in-blk mar-r-10">Faq's</h4>
					</div>
					<div class="sec-body">
						@if(count($package->faqs) > 0)
							<div class="sub-sec mar-b-20">
								@foreach($package->faqs as $faq)
									{{ Form::label('question', $faq->question ,['class' => 'font-16 first-letter-upper']) }}

									<div class="pad-b-10">
										{!! $faq->answer !!}
									</div>
								@endforeach
							</div>
						@else
							<div class="pad-5">
								<span>No FAQs Found.</span>
							</div>
						@endif
					</div>
				</section>
				<!--FAQ End-->

				<!-- Package Itinerary Wrap Start -->
				<section class="data-sec pad-b-4" id="itinerary">
					<div class="sec-head">
						<h4 class="">Itinerary</h4>
					</div>
					<div class="sec-body">
						@if(count($itineraries)>0)
							@foreach($itineraries as $itinerary)
								<label class="font-16">{{$itinerary->name}}</label>
								@if($itinerary->itineraries->count())
									<table class="table table-fixed table-bordered">
										<tr>
											<th width="15%">Start Time</th>
											<th width="15%">End Time</th>
											<th width="70%">Info</th>
										</tr>
										@foreach($itinerary->itineraries as $itinerary)
											<tr>
												<?php $time = explode(' - ', $itinerary->time) ?>
												<td>@if(array_key_exists('0',$time)) {{$time[0]}} @endif </td>
												<td>@if(array_key_exists('1',$time)) {{$time[1]}} @else -- N/A -- @endif</td>
												<td>{!! $itinerary->info !!}</td>
											</tr>
										@endforeach
									</table>
								@endif
							@endforeach
						@else
							No Itinerary Items Added for this package.
						@endif
					</div>
				</section>
				<!-- Package Itinerary Wrap End -->
			</div>
		</div>
		@include('layout.approval_action')
	</div>
@endsection

@section('app-modals')
	@parent
	<!-- modal asking to change the type ticket of the package -->
	<div id="modalAskChangeTypeTicket" class="modal" tabindex="-1" role="dialog" data-url="" data-type="">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header mar-t-10">
					<h4 class="modal-title">Change the occasion of the package</h4>
				</div>
				<div class="modal-body">
					<div class="hide alert-danger pad-6 mar-b-15 " id="errorBox"></div>
					<div class="form-group">
						<div class="col-sm-12 col-lg-12 col-md-12">
							<label class="text-info">SELECT THE OCCASION</label>
							<select id="typeTicket" name="typeTicket"
									class="form-control type-ticket-details">
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">
						<i class="glyphicon glyphicon-remove"></i> Cancel
					</button>
					<button type="button" class="btn btn-success type-ticket-update" id="btnSaveTypeTicketForPackage"><i
								class="glyphicon glyphicon-floppy-save"></i> Update
					</button>
				</div>
			</div>
		</div>
	</div>

	<!-- edit package provider modal-->
	<div id="modalChangePackageProvider" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Select the new provider</h4>
				</div>
				<form class="form form-horizontal" action="{{ route('package.provider.edit',$package->id) }}"
						method="post" role="form">
					<div class="modal-body">
						<div class="pad-l-20 pad-r-20">
							<div class="form-group">
								<label class="col-sm-3">Provider</label>
								<div class="col-sm-9">
									<select class="form-control package-planner" name="packageProviderId">
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-9 col-sm-offset-3">
								<input type="checkbox" id="termsCheckBox"
										class="mar-t-5"> I am sure to change the partner.
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" id="btnProviderChangeSubmit" class="btn btn-danger">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/package/package.js");
		});
	</script>
@stop