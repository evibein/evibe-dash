<div class='col-lg-4 mar-b-20'>
	<label>{{ ucfirst($field['field']['meta_name']) }} :</label>
	<input type=text class="form-control" name="{{ $field['field']['name'] }}"
			placeholder="@if($field['field']['placeholder']) {{ $field['field']['placeholder'] }} @else Enter
			{{ $field['field']['meta_name'] }} @endif"
			value="@if(old($field['field']['name'])){{ old($field['field']['name']) }}@elseif($field['oldValue']){{ $field['oldValue'] }}@endif">
</div>