@extends('layout.base')
@section('content')
	<div class="heading">
		Ticket bookings after {{ $time }}
	</div>
	<div class="col-lg-12">
		@if($bookings)
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th class="text-center">S.No</th>
					<th class="text-center">Booking Id</th>
					<th class="text-center">Booking Type</th>
					<th class="text-center">Party Date Time</th>
					<th class="text-center">Check In</th>
					<th class="text-center">Party End Time</th>
					<th class="text-center">Check Out</th>
				</tr>
				</thead>
				<tbody>
				@php $i = 1; @endphp
				@foreach($bookings as $booking)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $booking->booking_id }}</td>
						<td class="text-center">{{ ucwords($booking->booking_type_details) }}</td>
						<td class="text-center">{{ date('d/m/Y h:i A', $booking->party_date_time) }}</td>
						<td class="text-center">{{ $booking->check_in }}</td>
						<td class="text-center">{{ date('d/m/Y h:i A', $booking->party_end_time) }}</td>
						<td class="text-center">{{ $booking->check_out }}</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		@else
			<h3>No bookings after now</h3>
		@endif
	</div>
@endsection