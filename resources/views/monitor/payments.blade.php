@extends('layout.base')

@section('custom-head')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.css"/>
@endsection

@section('content')
	<div class="panel panel-default page-content mar-l-10 mar-r-10">
		<form method="get" class="mar-t-10">
			<div class="col-sm-4">
				<label for="id">Ticket Id</label>
				<input class="form-control in-blk" value="{{ request("id") }}" name="id" id="id" placeholder="Enter ticket Id">
			</div>
			<div class="col-sm-4">
				<label for="startTime">Start Time</label>
				<input class="form-control in-blk filter-analytics-date" name="startTime" id="startTime" value="{{ $data["startTime"] }}">
			</div>
			<div class="col-sm-4">
				<label for="endTime">End Time</label>
				<input class="form-control in-blk filter-analytics-date" name="endTime" id="endTime" value="{{ $data["endTime"] }}">
			</div>
			<div class="clearfix"></div>
			<div class="text-center">
				<button type="submit" class="btn btn-primary mar-t-5">Search</button>
			</div>
		</form>
		<div class="bottom-sec pad-10 mar-b-10">
			@if(count($data["transactions"]) > 0)
				<table id="paymentTransitionsTable" class="table table-bordered table-responsive table-hover">
					<thead class="text-info">
					<tr>
						<td>Id</td>
						<td>Url</td>
						<td>Browser Info</td>
						<td>Transaction Id</td>
						<td>Payment Interval</td>
						<td>Gateway Success</td>
						<td>Payment Success</td>
						<td>Gateway</td>
						<td>Ref Id</td>
						<td>Trans Details</td>
					</tr>
					</thead>
					@foreach($data['transactions'] as $transaction)
						<tr>
							<td><a href="{{ route("ticket.details.info", $transaction["ticket_id"]) }}">{{ $transaction["ticket_id"] }}</a></td>
							<td><a href="{{ $transaction["url"] }}">Link</a></td>
							<td>{{ $transaction["browser_info"] }}</td>
							<td>{{ $transaction["payment_transaction_id"] }}</td>
							<td>
								<div class="text-white font-1">{{ $transaction["payment_start_time"] }}</div>
								@if($transaction["payment_start_time"]){{ date("d M y, h:i A", $transaction["payment_start_time"]) }}@endif
								- @if($transaction["payment_end_time"]){{ date("d M y, h:i A", $transaction["payment_end_time"]) }}@endif
							</td>
							<td>
								@if($transaction["payment_bank_success"] == 2)
									<span class="text text-success">YES</span>
								@else
									<span class="text text-danger">NO</span>
								@endif
							</td>
							<td>
								@if($transaction["payment_evibe_success"] == 2)
									<span class="text text-success">YES</span>
								@else
									<span class="text text-danger">NO</span>
								@endif
							</td>
							<td>{{ $transaction["payment_gateway_id"] }}</td>
							<td>
								@if($transaction["pay_reference_id"])
									{{ $transaction["pay_reference_id"] }}
								@else
									{{ "--" }}
								@endif
							</td>
							<td>
								@php
									$details = explode(",", $transaction["comments"]);
								@endphp
								@if(count($details) > 1)
									<span class="complete hide">
										@foreach($details as $detail)
											{!! $detail !!}<br>
										@endforeach
									</span>
									<a class="more btn btn-info">Show Details</a>
								@else
									{{ "--" }}
								@endif
							</td>
						</tr>
					@endforeach
				</table>
				<div>
					GateWay: 1 - Razorpay, 2 - Payu, 3 - Freecharge, 4 - Paytm
				</div>
				<div class="text-center">
					<div>{{ $data['transactions']->appends(Input::except('page'))->links() }}</div>
				</div>
			@else
				<div class="mar-l-10 mar-b-10 alert alert-danger text-center">No Transaction Found.</div>
			@endif
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			(function initFunctions() {

				$('.filter-analytics-date').datetimepicker({
					timepicker: true,
					scrollInput: false,
					scrollMonth: false,
					closeOnDateSelect: false,
					format: "Y-m-d H:i:s",
					formatTime: "H:i"
				});

				// initiate dataTables
				$('#paymentTransitionsTable').DataTable();

				$("#paymentTransitionsTable").on("click", ".more", function () {
					$(this).siblings(".complete").toggleClass("hide");
				});
			})();
		});
	</script>
@endsection