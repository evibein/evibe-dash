<div style="padding:15px; border:5px solid #f5e8e3">
	<p>
		Hi Team,
	</p>
	<p>
		While editing the ticket booking, some error occurred while updating party time. Kindly check whether appropriate timings were sent to customer and partner in updated receipts.
		Below are the booking details (Check ticket update for the timeline).
	</p>

	<table style="border-collapse: collapse;text-align: left;">
		@if(isset($data['bookingId']) && $data['bookingId'])
			<tr>
				<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Id</b></td>
				<td style="border: 1px solid #dddddd; padding: 8px;">
					@if(isset($data['bookingLink']) && $data['bookingLink'])
						<a href="{{ $data['bookingLink'] }}" target="_blank">{{ $data['bookingId'] }}</a>
					@else
						{{ $data['bookingId'] }}
					@endif
				</td>
			</tr>
		@endif
		@if(isset($data['partyDate']) && $data['partyDate'])
			<tr>
				<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
				<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDate'] }}</td>
			</tr>
		@endif
		@if(isset($data['observedPartyDateTime']) && $data['observedPartyDateTime'])
			<tr>
				<td style="border: 1px solid #dddddd; padding: 8px;"><b>Time shown in bookings tab</b></td>
				<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['observedPartyDateTime'] }}</td>
			</tr>
		@endif
		@if(isset($data['actualPartyDateTime']) && $data['actualPartyDateTime'])
			<tr>
				<td style="border: 1px solid #dddddd; padding: 8px;"><b>Actual Time Update</b></td>
				<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['actualPartyDateTime'] }}</td>
			</tr>
		@endif
	</table>

	<div style="margin-top:10px">
		<div>{{ config('evibe.email_salutation') }}</div>
		<div>Team Evibe.in</div>
	</div>
</div>