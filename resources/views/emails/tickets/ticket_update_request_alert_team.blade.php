<p>Hello Team,</p>

<p>New Ticket updated by syrow user. Please check the link below & update the customer ASAP.</p>

<div>
	<table style="max-width:600px;border:1px solid #AAA;border-collapse:collapse;">
		<tr>
			<td style="border:1px solid #AAA;padding:4px;"><b>Ticket Comments</b></td>
			<td style="border:1px solid #AAA;padding:4px;">{!! $data['ticketComments'] !!}</td>
		</tr>
		<tr>
			<td style="border:1px solid #AAA;padding:4px;"><b>Dash link to ticket.</b></td>
			<td style="border:1px solid #AAA;padding:4px;">
				<a href="{{ config("evibe.host") . "/tickets/" . $data["ticketId"] }}" target="_blank">click here</a>
			</td>
		</tr>
	</table>
</div>
<p>
	Let's make their party Evibe special! <br>
</p>