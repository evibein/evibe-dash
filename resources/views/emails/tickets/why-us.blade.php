<div style="margin-top: 15px;">
	<div style=" margin-bottom: 10px; font-size:20px; padding-top:15px">Why Choose Evibe<span
				style="text-transform: lowercase">.in</span>?
	</div>
	<div>
		<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
			<div style="margin-bottom: 4px;">
				<img src="https://gallery.evibe.in/main/img/icons/guarantee.png" alt="clarify"
						style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
				<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Trusted by 1,50,000+ Customers</div>
			</div>
			<div style="color: #696969;font-size:15px">
				Your event is in safe hands. 1000s of customers book with us for our quality service & support.
			</div>
		</div>
		<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
			<div style="margin-bottom: 4px;">
				<img src="https://gallery.evibe.in/main/img/icons/easy-booking.png" alt="clarify"
						style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
				<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Stress-Free Party Planning</div>
			</div>
			<div style="color: #696969;font-size:15px">
				Get peace of mind. Book everything at one place with assurance of professional service.
			</div>
		</div>
		<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
			<div style="margin-bottom: 4px;">
				<img src="https://gallery.evibe.in/main/img/icons/real_photos_reviews.png" alt="clarify"
						style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
				<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Real Pictures & Reviews</div>
			</div>
			<div style="color: #696969;font-size:15px">
				It’s now easy to select perfect options. See 5,500+ real pictures & customer reviews.
			</div>
		</div>
		<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
			<div style="margin-bottom: 4px;">
				<img src="https://gallery.evibe.in/main/img/icons/best_deals.png" alt="clarify"
						style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
				<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Best Prices & Packages</div>
			</div>
			<div style="color: #696969;font-size:15px">
				Get best value for money. Our prices are pre-negotiated and best in market!
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>