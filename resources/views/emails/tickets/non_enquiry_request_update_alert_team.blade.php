<p>Hello Team,</p>

<p>Non enquiry ticket has been raised by SYROW user, Please check the comments below.</p>

<table style="border-collapse: collapse;text-align: left;">
	@if(isset($data["comments"]) && $data['comments']!="")
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Comments</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{!! $data["comments"] !!}</td>
		</tr>
	@endif
</table>

<p>
	<a href="{{ config("evibe.host") . "/tickets/" . $data["ticketId"] }}" target="_blank">Dash link to ticket.</a>
</p>
<p>
	Let's make their party Evibe special! <br>
</p>