<p>Dear {{ $data['customerName'] }},</p>

<p>
	Share this customized e-invitation card with your guests on Whatsapp / Facebook / SMS / Email or using any other medium to invite them to your party and RSVP their status.
</p>

<p>
	You can also share your unique invite link along with the image for guests to RSVP. You can use the same like to see latest RSVP status.
</p>

<div style="margin-top: 20px; margin-bottom: 30px;">
	<div style="width: 200px; text-align: center;">
		<a href="{{ $data['RSVPLink'] }}"><img style="width: 100%; height: 100%;" src="{{ $data['RSVPImage'] }}" alt="RSVP Invitation card"></a>
		<div style="margin-top: 10px;">
			<a style="border: 1px solid #1E347B; color: #FFFFFF; background-color: #1E347B; padding: 5px; text-decoration: none" href="{{ $data['RSVPLink'] }}">Download</a>
		</div>
	</div>
</div>

<p>
	RSVP is a simple request asking the guests to respond whether they are going to take part in the celebration.
</p>

<p>
	Hope you like it.
</p>

<p>
	Kindly reply to this email if there are any corrections or modifications required.
</p>

<div style="margin-top: 20px; margin-bottom: 15px;">
	<div>Cheers,</div>
	<div>Team Evibe.in</div>
</div>