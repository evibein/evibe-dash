<p>Namaste <b>{{$data['name']}}</b></p>

<p>
	I am {{ucfirst($data['handlerName'])}} from Evibe.in: your favourite party planner.
	@if (isset($data['partyDate']) && $data['partyDate'])
		This email is regarding your enquiry for party on <b>{{$data['partyDate']}}</b>
	@endif
	I tried calling you on <b> {{$data['phone']}}</b>, but could not connect.
	I hope your enquiry is still open. Please call me back on {{$data['handlerPhone']}} (or) {{config('evibe.operations.phone')}} to discuss complete details. You can also reply to this email as per your convenience.
</p>

<p>{{config('evibe.email_salutation')}}</p>
<p>{{$data['handlerName']}}</p>
<p>{{$data['handlerPhone']}}, {{config('evibe.operations.phone')}}</p>
<a href="https://www.evibe.in">Evibe.in</a>