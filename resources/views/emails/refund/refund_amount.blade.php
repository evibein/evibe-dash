<p>Namaste {{ $data['ticket']->name }},</p>

<p>Greetings from Evibe.in.</p>

<p>We are writing to you let you know that we have processed refund of
	<b>Rs. {{ $data["booking"]->refund_amount }}</b> for your party order {{ $data['booking']->booking_id }}. You should receive the amount within 3-5 Business days depending on your bank. @if(count($data['gallery']) > 0) Please check the attached screenshot for your reference. @endif
</p>

<p>Please accept our sincere apologies for any inconvenience that you might have faced. Given a chance, we’d love to serve you again to your satisfaction.</p>

<p>Should you have any questions, please reply to this email, we will be happy to help you.</p>

<p>
	Thanks in advance,<br>
	{{ $data['handler']->name }},<br>
	{{ $data['handler']->phone }}, {{ config("evibe.phone_plain") }},<br>
	www.evibe.in
</p>

@if(count($data['gallery']) > 0)
	@foreach ($data['gallery'] as $image)
		<?php $link = config('evibe.gallery.host') . '/ticket/' . $data['ticket']->id . '/' . $data['booking']->id . '/refund/' . $image->url; ?>
		<a href="{{ $link }}"><img style="max-height: 100px;max-width: 100px"
					src="{{ $link }}"></a>
	@endforeach
@endif