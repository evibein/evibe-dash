<div style="margin-top:10px;">
	<p style="padding-bottom: 0; margin-bottom: 0;">Hi Team,</p>
	<br>
	<p style="padding-top: 0; margin-top: 0;"> There is an order update from @if($data['isCustomer'] == 1) Customer @else Partner @endif, Please go through the details below.</p>
	<div>
		<?php $mappedValues = $data["booking"]->mapping->getMappedValues(); ?>
		<div class="col-lg-12 no-pad-l">
			<table style="border: 1px solid #ddd; text-align: left; border-collapse: collapse; width: 100%;">
				<tr>
					<td style="border: 1px solid #ddd;">
						Booking Id
					</td>
					<td style="border: 1px solid #ddd;">
						<a href="{{ config("evibe.host")."tickets/".$data["booking"]->ticket_id."/bookings#".$data["booking"]->id }}">{{ $data["booking"]->booking_id }}</a>
					</td>
				</tr>
				@if($data['isCustomer'] == 1)
					<tr>
						<td style="border: 1px solid #ddd;">
							Customer Name
						</td>
						<td style="border: 1px solid #ddd;">
							@if($data['ticket']->name)
								{{ $data['ticket']->name }}
							@else
								---
							@endif
						</td>
					</tr>
					<tr>
						<td style="border: 1px solid #ddd;">
							Customer Phone
						</td>
						<td style="border: 1px solid #ddd;">
							@if($data['ticket']->phone)
								{{ $data['ticket']->phone }} / @else --- @endif
							@if($data['ticket']->alt_phone)
								{{ $data['ticket']->alt_phone }} @else --- @endif
						</td>
					</tr>
				@else
					<tr>
						<td style="border: 1px solid #ddd;">
							Partner Name
						</td>
						<td style="border: 1px solid #ddd;">
							@if($data['provider']){{ "<b>".$data['provider']->person."</b>" }}@else --- @endif
						</td>
					</tr>
					<tr>
						<td style="border: 1px solid #ddd;">
							Partner Phone
						</td>
						<td style="border: 1px solid #ddd;">
							@if($data['provider'] && $data['provider']->phone)
								{{ $data['provider']->phone }} /  @else --- @endif
							@if($data['provider'] && $data['provider']->alt_phone)
								{{ $data['provider']->alt_phone }} @else --- @endif
						</td>
					</tr>
				@endif
				<tr>
					<td style="border: 1px solid #ddd;">
						Party Location
					</td>
					<td style="border: 1px solid #ddd;">
						@if ($data["ticket"]->area && $data["ticket"]->area->name)
							{{ $data["ticket"]->area->name }}, @else --- @endif
						@if ($data["ticket"]->city && $data["ticket"]->city->name)
							{{ $data["ticket"]->city->name }} @else --- @endif
					</td>
				</tr>
				<tr>
					<td style="border: 1px solid #ddd;">
						Party Date
					</td>
					<td style="border: 1px solid #ddd;">
						{{ date("d M Y \(D\), H:i:s", $data["booking"]->party_date_time) }}
					</td>
				</tr>
				<tr>
					<td style="border: 1px solid #ddd;">
						Tracking Update
					</td>
					<td style="border: 1px solid #ddd;">
						<span style="background-color: yellow; font-size: 14px;">@if($data['isCustomer'] == 1) {{ $data["customerOrderUpdate"] }} @else {{ $data["partnerOrderUpdate"] }} @endif</span>
					</td>
				</tr>
			</table>
			<table style="border: 1px solid #ddd; text-align: left; border-collapse: collapse; width: 100%; margin-top: 10px;">
				@if($data['isCustomer'] != 1)
					<tr>
						<td style="border: 1px solid #ddd;">
							Customer Name
						</td>
						<td style="border: 1px solid #ddd;">
							@if($data['ticket']->name)
								{{ $data['ticket']->name }}
							@else
								---
							@endif
						</td>
					</tr>
					<tr>
						<td style="border: 1px solid #ddd;">
							Customer Phone
						</td>
						<td style="border: 1px solid #ddd;">
							@if($data['ticket']->phone)
								{{ $data['ticket']->phone }} / @else --- @endif
							@if($data['ticket']->alt_phone)
								{{ $data['ticket']->alt_phone }} @else --- @endif
						</td>
					</tr>
				@else
					<tr>
						<td style="border: 1px solid #ddd;">
							Partner Name
						</td>
						<td style="border: 1px solid #ddd;">
							@if($data['provider']){{ "<b>".$data['provider']->person."</b>" }}@else --- @endif
						</td>
					</tr>
					<tr>
						<td style="border: 1px solid #ddd;">
							Partner Phone
						</td>
						<td style="border: 1px solid #ddd;">
							@if($data['provider'] && $data['provider']->phone)
								{{ $data['provider']->phone }} /  @else --- @endif
							@if($data['provider'] && $data['provider']->alt_phone)
								{{ $data['provider']->alt_phone }} @else --- @endif
						</td>
					</tr>
				@endif
			</table>
		</div>
	</div>
</div>