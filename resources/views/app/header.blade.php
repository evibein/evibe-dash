<nav class="navbar navbar-inverse" role="navigation" xmlns="http://www.w3.org/1999/html">
	<div class="navbar-header">
		<a class="navbar-brand" href="/">Evibe.in - Administrator</a>
	</div>
	@php
		$user = auth()->user();
	@endphp
	@if (session('user_id'))
		<ul class="nav navbar-nav navbar-right">
			@if(!AppUtil::isAdmin() && AppUtil::isTabVisible('tracking') && ($user->role_id != config('evibe.roles.marketing')))
				<li class="app-tab @if(request()->is('*tracking*')) active @endif">
					<a href="{{route('tracking.list')}}"><i class="glyphicon glyphicon-record"></i> Tracking</a>
				</li>
			@endif
			@if(!AppUtil::isAdmin() && AppUtil::isTabVisible('customer-feedback'))
				<li class="app-tab @if(request()->is('*feedback*')) active @endif">
					<a href="{{route('tracking.pending.feedback')}}"><i class="glyphicon glyphicon-book"></i> Feedback</a>
				</li>
			@endif
			@if(!AppUtil::isAdmin() && AppUtil::isTabVisible('crmDashBoard'))
				<li class="app-tab @if(request()->is('*sales/dashboard*')) active @endif">
					<a href="{{ route('sales.tickets.list', "new-tickets") }}"><i
								class="glyphicon glyphicon-stats"></i> CRM DashBoard</a>
				</li>
			@endif
			@if(!AppUtil::isAdmin() && AppUtil::isTabVisible('opsDashboard'))
				<li class="app-tab @if(request()->is('*ops-dashboard*')) active @endif">
					<a href="{{route('ops-dashboard.order-confirmation-tickets.list')}}"><i
								class="glyphicon glyphicon-stats"></i> OPS Dashboard</a>
				</li>
			@endif
			@if(AppUtil::isTabVisible('analytics'))
				<li class="dropdown @if(request()->is('*/analytics/*')) active @endif">
					<a href="{{ route('analytics.cumulative') }}">
						<i class="glyphicon glyphicon-align-center"></i> Analytics
					</a>
				</li>
			@endif
			@if (AppUtil::isTabVisible('availability'))
				<li class="app-tab @if(request()->is('*avail-check*')) active @endif">
					<a href="{{ route('avail.check.initialize') }}">
						<i class="glyphicon glyphicon-bullhorn"></i> Avail Check
					</a>
				</li>
			@endif
			@if (AppUtil::isTabVisible('tickets'))
				<li class="app-tab @if(request()->is('*/quick-book*')) active @endif hide">
					<a href="/quick-book"><i class="glyphicon glyphicon-flash"></i> Quick Book</a>
				</li>
				@if(!in_array($user->role_id, [config('evibe.roles.syrow')]))
					<li class="app-tab @if(request()->is('*/tickets*')) active @endif">
						<a href="/tickets"><i class="glyphicon glyphicon-th-list"></i> Tickets</a>
					</li>
				@endif
			@endif
			@if(AppUtil::isTabVisible('booking-calendar'))
				<li class="app-tab @if(request()->is('*calendar/booking*')) active @endif">
					<a href="{{ route('calendar.booking.show') }}"><i
								class="glyphicon glyphicon-calendar"></i> Calendar</a>
			@endif
			@if(!in_array($user->role_id, [config('evibe.roles.syrow'), config("evibe.roles.tech"), config('evibe.roles.accounts')]))
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-star"></i> Options
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						@if (AppUtil::isTabVisible('packages'))
							<li>
								<a href="{{route('packages.all.list')}}"><i
											class="glyphicon glyphicon-gift"></i> Packages</a>
							</li>
						@endif
						@if (AppUtil::isTabVisible('decors'))
							<li>
								<a href="/decors"><i class="glyphicon glyphicon-tree-conifer"></i> Decors</a></li>
						@endif
						@if (AppUtil::isTabVisible('trends'))
							<li>
								<a href="/trends"><i class="glyphicon glyphicon-flash"></i> Trends</a>
							</li>
						@endif
						@if(AppUtil::isTabVisible('services'))
							<li>
								<a href="{{route('services.show')}}"><i class="glyphicon glyphicon-star-empty"></i> Services</a>
							</li>
						@endif
						@if (AppUtil::isTabVisible('cakes'))
							<li>
								<a href="/cakes"><i class="glyphicon glyphicon-adjust"></i> Cakes</a>
							</li>
						@endif
						@if(AppUtil::isTabVisible('collection'))
							<li class="app-tab @if(request()->is('*collection*')) active @endif">
								<a href="{{ route('collection.list') }}"><i
											class="glyphicon glyphicon-zoom-in"></i> Collection</a>
						@endif
						@if (AppUtil::isTabVisible('venue-halls'))
							<li class="app-tab @if(request()->is('*venue-halls*')) active @endif">
								<a href="/venue-halls"><i class="glyphicon glyphicon-stats"></i> Halls</a>
							</li>
						@endif
						@if(AppUtil::isTabVisible('add-ons'))
							<li class="app-tab @if(request()->is('*add-ons*')) active @endif">
								<a href="{{ route('add-ons.option-mappings.map') }}"><i
											class="glyphicon glyphicon-plus"></i> Add-Ons</a>
						@endif
					</ul>
				</li>
			@endif
			@if (AppUtil::isTabVisible('monitor'))
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-tasks"></i> Monitor
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a href="{{route('monitor.payments')}}">&#8377; Payments</a>
						</li>
					</ul>
				</li>
			@endif
			@if($user->role_id == config('evibe.roles.bd') || $user->role_id == config('evibe.roles.sr_bd'))
				@if (AppUtil::isTabVisible('vendors'))
					<li class="app-tab @if(request()->is('*vendors*')) active @endif">
						<a href="{{route('vendor.new.list')}}"><i class="glyphicon glyphicon-stats"></i> Partners</a>
					</li>
				@endif
				@if (AppUtil::isTabVisible('venues'))
					<li class="app-tab @if(request()->is('*venues*')) active @endif">
						<a href="/venues"><i class="glyphicon glyphicon-stats"></i> Halls</a>
					</li>
				@endif
				@if (AppUtil::isTabVisible('new-package'))
					<li><a href="/partner/add/new-package"><i
									class="glyphicon glyphicon-certificate"></i> Inbound Packages</a></li>
				@endif
			@elseif(AppUtil::isTabVisible('vendors') || AppUtil::isTabVisible('venues'))
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-align-justify"></i> Partners
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						@if (AppUtil::isTabVisible('vendors'))
							<li>
								<a href="{{route('vendor.new.list')}}"><i
											class="glyphicon glyphicon-stats"></i> Vendors</a>
							</li>
						@endif
						@if (AppUtil::isTabVisible('venues'))
							<li>
								<a href="/venues/list"><i class="glyphicon glyphicon-stats"></i> Venues</a>
							</li>
						@endif
						@if (AppUtil::isTabVisible('new-package'))
							<li><a href="/partner/add/new-package"><i
											class="glyphicon glyphicon-certificate"></i> Inbound Packages</a></li>
						@endif
					</ul>
				</li>
			@endif
			@if(AppUtil::isTabVisible('stories') && $user->role_id == config('evibe.roles.marketing'))
				<li class="app-tab	 @if(request()->is('*stories*')) active @endif">
					<a href="{{route('stories.customer.list')}}">
						<i class="glyphicon glyphicon-heart"></i> Stories
					</a>
				</li>
				<li class="app-tab	 @if(request()->is('*images*')) active @endif">
					<a href="{{route('download.images.getCodes')}}">
						<i class="glyphicon glyphicon-download"></i> Download-Images
					</a>
				</li>
			@endif
			@if(AppUtil::isTabVisible('errors') && $user->role_id == config('evibe.roles.tech'))
				<li class="app-tab	 @if(request()->is('errors*')) active @endif">
					<a href="/errors">
						<i class="glyphicon glyphicon-exclamation-sign"></i> Error
					</a>
				</li>
			@endif
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="glyphicon glyphicon-th"></i> Others <b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					@if(AppUtil::isTabVisible('coupon'))
						<li>
							<a href="{{ route("coupon.redeemed") }}"><i
										class="glyphicon glyphicon-film"></i> Coupon</a>
						</li>
					@endif
						@if(AppUtil::isTabVisible('virtual-party'))
							<li>
								<a href="{{ route("virtual-party.home") }}"><i class="glyphicon glyphicon-globe"></i> Virtual Party</a>
							</li>
						@endif
					@if(AppUtil::isAdmin())
						<li>
							<a href="{{route('piab-dashboard.product-bookings.list')}}"><i class="glyphicon glyphicon-stats"></i> PIAB Dashboard</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('reviews_mapping'))
						<li>
							<a href="{{route('reviews-mapping.pending')}}"><i class="glyphicon glyphicon-list"></i> Reviews Mapping</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('revamp_tags'))
						<li>
							<a href="{{ route("revamp.tags.list") }}"><i
										class="glyphicon glyphicon-refresh"></i> Tags Revamp</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('booking_concept'))
						<li>
							<a href="/booking-concept"><i class="glyphicon glyphicon-tasks"></i> Booking Concept</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('delivery_images'))
						<li>
							<a href="/delivery-images"><i class="glyphicon glyphicon-flash"></i>Delivery Images</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('images'))
						<li>
							<a href="/images-search"><i class="glyphicon glyphicon-picture"></i> Images</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('content'))
						<li><a href="{{route('content.show')}}"><i class="glyphicon glyphicon-plus"></i> Content</a>
						</li>
					@endif
					<div class="hide">
						@if(AppUtil::isTabVisible('audit'))
							<li>
								<a href="{{route('audit.edit-new')}}"><i class="glyphicon glyphicon-edit hide"></i> Product Audit</a>
							</li>
						@endif
					</div>
						@if (AppUtil::isTabVisible('option-availability'))
							<li>
								<a href="{{ route('option-availability') }}">
									<i class="glyphicon glyphicon-exclamation-sign"></i> Option Availability
								</a>
							</li>
						@endif
					@if (AppUtil::isTabVisible('reorder'))
						<li>
							<a href="{{ route('reorder.filter') }}">
								<i class="glyphicon glyphicon-refresh"></i> Reorder
							</a>
						</li>
					@endif
					@if(AppUtil::isAdmin())
						<li class="app-tab @if(request()->is('*tracking*')) active @endif">
							<a href="{{route('tracking.list')}}"><i class="glyphicon glyphicon-record"></i> Tracking</a>
						</li>
						<li class="app-tab @if(request()->is('*sales/dashboard*')) active @endif">
							<a href="{{ route('sales.tickets.list', "new-tickets") }}"><i
										class="glyphicon glyphicon-stats"></i> CRM DashBoard</a>
						</li>
						<li class="app-tab @if(request()->is('*ops-dashboard*')) active @endif">
							<a href="{{route('ops-dashboard.order-confirmation-tickets.list')}}"><i
										class="glyphicon glyphicon-stats"></i> OPS Dashboard</a>
						</li>
						<li>
							<a href="{{route('auto.booking.list')}}">
								<i class="glyphicon glyphicon-shopping-cart"></i> Auto booking
							</a>
						</li>
						<li>
							<a href="{{route('availability.venue.live')}}">
								<i class="glyphicon glyphicon-bullhorn"></i> Availability
							</a>
						</li>
					@endif
					@if (AppUtil::isTabVisible('finance'))
						<li>
							<a href="{{route('finance.settlement.get')}}">
								<i class="glyphicon glyphicon-usd"></i> Finance
							</a>
						</li>
					@endif
					@if (AppUtil::isTabVisible('automation'))
						<li>
							<a href="{{ route('automation.home') }}">
								<i class="glyphicon glyphicon-new-window"></i> Automation
							</a>
						</li>
					@endif
					@if (AppUtil::isTabVisible('errors'))
						<li>
							<a href="/errors">
								<i class="glyphicon glyphicon-exclamation-sign"></i> Errors
							</a>
						</li>
					@endif
					@if (AppUtil::isTabVisible('downloads'))
						<li>
							<a href="{{ route('downloads.home') }}">
								<i class="glyphicon glyphicon-download"></i> Downloads
							</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('package_field'))
						<li>
							<a href="{{route('field.package.list')}}"><i
										class="glyphicon glyphicon-book"></i> Package Field</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('checkout_field::show'))
						<li>
							<a href="{{route('field.checkout.list')}}"><i
										class="glyphicon glyphicon-paperclip"></i> Checkout Field</a>
						</li>
					@endif
					@if (AppUtil::isTabVisible('activation'))
						<li>
							<a href="{{ route('partner.activation.accepted') }}"><i
										class="glyphicon glyphicon-stats"></i> Activation</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible("delivery"))
						<li>
							<a href="{{ route('partner.delivery.list') }}">
								<i class="glyphicon glyphicon-screenshot"></i> Partner Delivery
							</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible("live_profile"))
						<li>
							<a href="{{ route('partner.options.home') }}">
								<i class="glyphicon glyphicon-globe"></i> Partner Options
							</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('stories') && $user->role_id != config('evibe.roles.marketing'))
						<li>
							<a href="{{route('stories.customer.list')}}">
								<i class="glyphicon glyphicon-heart"></i> Stories
							</a>
						</li>
					@endif
					@if(AppUtil::isTabVisible('stories') && $user->role_id != config('evibe.roles.marketing'))
						<li>
							<a href="{{route('download.images.getCodes')}}">
								<i class="glyphicon glyphicon-download"></i> Download-Images
							</a>
						</li>
					@endif
					@if (AppUtil::isTabVisible('reviews'))
						<li><a href="{{ route("reviews.list", "accepted") }}"><i
										class="glyphicon glyphicon-comment"></i> Reviews</a>
						</li>
					@endif
					@if (AppUtil::isTabVisible('nearby-areas'))
						<li><a href="/nearby-areas"><i class="glyphicon glyphicon-map-marker"></i> Areas</a></li>
					@endif
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="glyphicon glyphicon-user"></i><b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li class="pad-l-20 pad-b-4 pad-t-5">@if (session('user_id')) {{ $user->username }} @endif</li>
					@if(AppUtil::isTabVisible('user_management'))
						<li>
							<a href="{{ route('user.list') }}">
								<i class="glyphicon glyphicon-user"></i> User management</a>
						</li>
						<li>
							<a href="{{ route('team.list') }}">
								<i class="glyphicon glyphicon-tower"></i> Team Profile</a>
						</li>
					@endif
					@if($user->id == config("evibe.default_handler.metrics_data_user_id"))
						<li>
							<a href="{{ route('metrics.data') }}">
								<i class="glyphicon glyphicon-user"></i> Metrics
							</a>
						</li>
					@endif
					<li><a href="/change-password"><i class="glyphicon glyphicon-pencil"></i> Change password</a></li>
					<li><a href="/logout"><i class="glyphicon glyphicon-log-out"></i> Sign out</a></li>
				</ul>
			</li>
		</ul>
	@endif
</nav>