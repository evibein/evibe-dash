<div class="tabs-cnt hide">
	<ul class="nav nav-tabs app-tabs">
		<li class="app-tab @if(Request::is('*tickets*')) active @endif">
			<a href="/tickets" class="tab-name">Tickets</a>
		</li>
		<li class="app-tab @if(Request::is('*vendors*')) active @endif">
			<a href="/vendors" class="tab-name">Vendors</a>
		</li>
		<li class="app-tab @if(Request::is('*venues*')) active @endif">
			<a href="/venues" class="tab-name">Venues</a>
		</li>
		<li class="app-tab @if(Request::is('*package*')) active @endif">
			<a href="/package" class="tab-name">Packages</a>
		</li>
		<li class="app-tab @if(Request::is('*trends*')) active @endif">
			<a href="/trends" class="tab-name">Trending</a>
		</li>
		<li class="app-tab @if(Request::is('*utilities*')) active @endif">
			<a href="/utilities" class="tab-name">Utilities</a>
		</li>
		<li class="app-tab @if(Request::is('*reviews*')) active @endif">
			<a href="/reviews" class="tab-name">Reviews</a>
		</li>
		<li class="app-tab @if(Request::is('*errors*')) active @endif">
			<a href="/errors" class="tab-name">Errors</a>
		</li>
	</ul>
</div>