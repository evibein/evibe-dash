@extends('layout.base')

@section('content')

<div class="row">
	<div class="col-md-4 col-sm-8 col-md-offset-4 col-sm-offset-2">

		<div class="changepwd-cnt">
			@if (!Session::get('update_success'))
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Change Password</h3>
					</div>
					<div class="panel-body">
						<div class="login-form">
						{{ Form::open(array(
							'url' => 'change-password',
							'autocomplete' => 'on',
							'class' => 'form-horizontal',
							'method' => 'post'
						)) }}
							<div class="form-group">
									{{ Form::label('old_pwd', 'Old Password') }}
									{{ Form::input('password', 'old_pwd', '', array(
										'placeholder' => '••••••••',
										'class' => 'form-control'
									)) }}
							</div>
							<div class="form-group">
									{{ Form::label('new_pwd', 'New Password') }}
									{{ Form::input('password', 'new_pwd', '', array(
										'placeholder' => '••••••••',
										'class' => 'form-control'
									)) }}
							</div>
							<div class="form-group">
									{{ Form::label('new_pwd_confirmation', 'Repeat Password') }}
									{{ Form::input('password', 'new_pwd_confirmation', '', array(
										'placeholder' => '••••••••',
										'class' => 'form-control'
									)) }}
							</div>
							<div class="form-group pull-right">
								<a href="/" class="pad-r-20">Cancel</a>
									{{ Form::input('submit', 'update', 'Update', array(
										'class' => 'btn btn-primary'
									)) }}

							</div>

						{{ Form::close() }}
						</div>
						</div>
					</div>
				</div>
			@else
				<div class="alert alert-success">
					<span class="">Password updated succesfully. </span>
					<span><a href="/" class="btn btn-default">Go To Home</a></span>
				</div>
			@endif
		</div>
	</div>
	<div class="errors-box col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
		@if ($errors->any() || Session::has('custom_error'))
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				@if (Session::has('custom_error'))
					<ul><li>{{ Session::get('custom_error') }}</li></ul>
				@endif
				@if ($errors->any())
					<ul>
						@foreach($errors->all() as $message)
							<li>{!! $message !!}</li>
						@endforeach
					</ul>
				@endif
			</div>
		@endif
	</div>
@stop