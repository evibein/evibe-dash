<div class="in-blk">
	<a class="btn btn-primary in-blk btn-copy-package"
			data-url="{{ route('show.copy.model') }}">
		<i class="glyphicon glyphicon-import"></i> Copy
	</a>
</div>

@section('app-modals')
	@parent
	<!-- edit package provider modal-->
	<div id="modalCopyPackage" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title text-center">Select the option you need to Copy</h4>
				</div>
				<form class="form form-horizontal copy-data-form" data-url="{{ route('copy.model.save') }}"
						role="form">
					<div class="modal-body">
						<div class="pad-l-20 pad-r-20">
							<div class="form-group">
								<label class="col-sm-4 pad-t-10 type-mapping-name">Sample</label>
								<div class="col-sm-8">
									<select class="form-control copy-package-ids" name="copyPackageId"
											id="copyPackageId"
											data-old="{{ old('copyPackageId') }}">
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group hide copy-planner">
								<label class="col-sm-4 pad-t-10">Provider :</label>
								<div class="col-sm-8">
									<select class="form-control copy-package-planner" name="copyPackagePlannerId"
											id="copyPackagePlannerId">
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 pad-t-10">Occasion :</label>
								<div class="col-sm-8">
									<select class="form-control copy-occasion-type" name="copyOccasionTypeId"
											id="copyOccasionTypeId">
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group hide type-ticket">
								<label class="col-sm-4 pad-t-10">Package Type :</label>
								<div class="col-sm-8">
									<select class="form-control copy-page-type" name="copyPageTypeId"
											id="copyPageTypeId">
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer text-center">
						<a class="btn btn-default in-blk" href="{{ route("packages.all.list") }}">Cancel</a>
						<button type="submit" class="btn btn-danger in-blk btn-copy-save">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop