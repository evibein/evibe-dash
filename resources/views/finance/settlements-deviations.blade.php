<html>
<body>
<table>
	<tr>
		<th>Company Name</th>
		<th>Deviation</th>
	</tr>
	@if(isset($data['deviationsList']) && count($data['deviationsList']))
		@foreach($data['deviationsList'] as $deviation)
			<tr>
				<td>
					@if(isset($deviation['partnerCompany']) && $deviation['partnerCompany'])
						{{ $deviation['partnerCompany'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($deviation['deviation']) && $deviation['deviation'])
						{{ $deviation['deviation'] }}
					@else
						--
					@endif
				</td>
			</tr>
		@endforeach
	@endif
</table>
</body>
</html>