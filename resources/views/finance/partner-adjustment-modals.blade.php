<!-- Modal to add partner adjustment -->
<div id="modalAddPartnerAdjustment" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Add Partner Adjustment</h4>
				<div class="finance-list-info mar-t-5">
					<ul>
						<li>Kindly select the partner and his/her related booking for which you want to add an adjustment.</li>
						<li>In case the adjustment amount need to be paid to us by partner, kindly select “Pay Evibe?” to “Yes”.</li>
					</ul>
				</div>
			</div>
			<div class="pad-l-20 pad-r-20 pad-t-20">
				<form class="form form-horizontal" role="form">
					<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
					<div class="form-group">
						<label class="col-sm-3">Partner Type</label>
						<div class="col-sm-9">
							<select class="form-control pa-partner-type">
								<option value="{{ config('evibe.ticket_type.planners') }}" selected>Planner</option>
								<option value="{{ config('evibe.ticket_type.venues') }}">Venue</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Partner</label>
						<div class="col-sm-9">
							<select class="form-control pa-planner-select" id="paPlannerSelect">
								<option value="0">-- Select a planner --</option>
								@if(isset($data['plannerPartnersList']) && count($data['plannerPartnersList']))
									@foreach($data['plannerPartnersList'] as $plannerDetails)
										<option value="{{ $plannerDetails['id'] }}">{{ $plannerDetails['name'] }} - {{ $plannerDetails['email'] }}</option>
									@endforeach
								@endif
							</select>
							<select class="form-control pa-venue-select" id="paVenueSelect">
								<option value="0">-- Select a venue --</option>
								@if(isset($data['venuePartnersList']) && count($data['venuePartnersList']))
									@foreach($data['venuePartnersList'] as $venueDetails)
										<option value="{{ $venueDetails['id'] }}">{{ $venueDetails['name'] }} - {{ $venueDetails['email'] }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Ticket Booking</label>
						<div class="col-sm-9">
							<select class="pa-ticket-booking-id form-control" id="paTicketBookingId">
								<option value="0">-- Select Ticket Booking --</option>
								@if(isset($data['ticketBookings']) && count($data['ticketBookings']))
									@foreach($data['ticketBookings'] as $ticketBooking)
										<option value="{{ $ticketBooking['id'] }}"
												data-partner-id="@if(isset($ticketBooking['map_id'])) {{ $ticketBooking['map_id'] }} @endif"
												data-partner-type-id="@if(isset($ticketBooking['map_type_id'])) {{ $ticketBooking['map_type_id'] }} @endif">
											@if(isset($ticketBooking['name'])) {{ $ticketBooking['name'] }} @endif -
											@if(isset($ticketBooking['party_date_time']) && ($ticketBooking['party_date_time'] > 0)) {{ date('d/m/Y h:i A', $ticketBooking['party_date_time']) }} @endif -
											@if(isset($ticketBooking['phone'])) {{ $ticketBooking['phone'] }} @endif -
											@if(isset($ticketBooking['booking_id'])) {{ $ticketBooking['booking_id'] }} @endif
										</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Adjustment Amount</label>
						<div class="col-sm-9">
							<input class="pa-adjustment-amount form-control" type="text"
									id="paAdjustmentAmount" placeholder="enter adjustment amount">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Pay Evibe?</label>
						<div class="col-sm-9">
							<select id="paIsPayback" class="pa-payback form-control">
								<option value="0" selected>No</option>
								<option value="1">Yes</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Adjustment Comments</label>
						<div class="col-sm-9">
							<textarea id="paComments" class="pa-comments form-control"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-pa-submit"
						data-url="{{ route("finance.partner-adjustment.create") }}">Submit
				</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal to edit partner adjustment -->
<div id="modalEditPartnerAdjustment" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Partner Adjustment</h4>
			</div>
			<div class="pad-l-20 pad-r-20 pad-t-20">
				<form class="form form-horizontal" role="form">
					<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
					<div class="form-group">
						<label class="col-sm-3">Partner Type</label>
						<div class="col-sm-9">
							<div class="pa-edit-partner-type"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Partner</label>
						<div class="col-sm-9">
							<div class="pa-edit-partner"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Booking Id</label>
						<div class="col-sm-9">
							<div class="pa-edit-booking-id"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Adjustment Amount</label>
						<div class="col-sm-9">
							<input class="pa-edit-adjustment-amount form-control" type="text"
									id="paEditAdjustmentAmount" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Pay Evibe?</label>
						<div class="col-sm-9">
							<select id="paEditIsPayback" class="pa-edit-is-payback form-control">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Schedule Date</label>
						<div class="col-sm-9">
							<input type="text" class="pa-edit-schedule-date form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Adjustment Comments</label>
						<div class="col-sm-9">
							<div class="pa-edit-comments"></div>
						</div>
					</div>
					<input type="hidden" class="hid-pa-adjustment-id" value="">
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-pa-update"
						data-url="{{ route("finance.partner-adjustment.update") }}">Submit
				</button>
			</div>
		</div>
	</div>
</div>