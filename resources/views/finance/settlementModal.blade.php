
<div class="modal" tabindex="-1" role="dialog" id="settlementDateModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Settlement Date (Wednesdays)</h5>
      </div>
      <div class="modal-body">
        <p id="errorMessage" style="color:red" class="hide"></p>
        <div class='mar-t-5 mar-b-5'>Note: Parties from a week's Monday to Sunday will be settled on next week's Wednesday</div>
        <div class='input-group'>
              <input type="text" class="form-control filter-date filter-rd-start" id="customSettlementDate" value="">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" id="createSettlement" class="btn btn-primary" data-url="{{ route('finance.settlement.create') }}">
            @if(isset($buttonText))
                {{ $buttonText }} 
            @else
                  Create Settlement
            @endif
        </button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>