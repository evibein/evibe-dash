<html>
<body>
<table>
	<tr>
		<th>Company Name</th>
		<th>Partner Name</th>
		<th>Partner City</th>
		<th>Booking Amount</th>
		<th>Advance Amount</th>
		<th>Evibe Service Fee</th>
		<th>CGST</th>
		<th>SGST</th>
		<th>IGST</th>
		<th>GST</th>
		<th>Refund Amount</th>
		<th>Settlement Amount</th>
		<th>Settlement Done At</th>
		<th>Account Number</th>
		<th>Bank Name</th>
		<th>IFSC Code</th>
		<th>GSTIN</th>
	</tr>
	@if(isset($data['settlementsList']) && count($data['settlementsList']))
		@foreach($data['settlementsList'] as $settlement)
			<tr>
				<td>
					@if(isset($settlement['partnerCompanyName']) && $settlement['partnerCompanyName'])
						{{ $settlement['partnerCompanyName'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['partnerName']) && $settlement['partnerName'])
						{{ $settlement['partnerName'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['partnerCity']) && $settlement['partnerCity'])
						{{ $settlement['partnerCity'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['bookingAmount']) && $settlement['bookingAmount'])
						{{ $settlement['bookingAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['advanceAmount']) && $settlement['advanceAmount'])
						{{ $settlement['advanceAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['evibeServiceFee']) && $settlement['evibeServiceFee'])
						{{ $settlement['evibeServiceFee'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['cgstAmount']) && $settlement['cgstAmount'])
						{{ $settlement['cgstAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['sgstAmount']) && $settlement['sgstAmount'])
						{{ $settlement['sgstAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['igstAmount']) && $settlement['igstAmount'])
						{{ $settlement['igstAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['gstAmount']) && $settlement['gstAmount'])
						{{ $settlement['gstAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['partnerBoreRefund']) && $settlement['partnerBoreRefund'])
						{{ $settlement['partnerBoreRefund'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['settlementAmount']) && $settlement['settlementAmount'])
						{{ $settlement['settlementAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['settlementDoneAt']) && $settlement['settlementDoneAt'])
						{{ $settlement['settlementDoneAt'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['accountNumber']) && $settlement['accountNumber'])
						{{ $settlement['accountNumber'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['bankName']) && $settlement['bankName'])
						{{ $settlement['bankName'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['ifscCode']) && $settlement['ifscCode'])
						{{ $settlement['ifscCode'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['gstin']) && $settlement['gstin'])
						{{ $settlement['gstin'] }}
					@else
						--
					@endif
				</td>
			</tr>
		@endforeach
	@endif
	<tr>
		<th colspan="3">Total</th>
		<th>
			@if(isset($data['totalBookingAmount']) && $data['totalBookingAmount'])
				{{ $data['totalBookingAmount'] }}
			@else
				0
			@endif
		</th>
		<th>
			@if(isset($data['totalAdvanceAmount']) && $data['totalAdvanceAmount'])
				{{ $data['totalAdvanceAmount'] }}
			@else
				0
			@endif
		</th>
		<th>
			@if(isset($data['totalEvibeServiceFee']) && $data['totalEvibeServiceFee'])
				{{ $data['totalEvibeServiceFee'] }}
			@else
				0
			@endif
		</th>
		<th>
			@if(isset($data['totalCGSTAmount']) && $data['totalCGSTAmount'])
				{{ $data['totalCGSTAmount'] }}
			@else
				0
			@endif
		</th>
		<th>
			@if(isset($data['totalSGSTAmount']) && $data['totalSGSTAmount'])
				{{ $data['totalSGSTAmount'] }}
			@else
				0
			@endif
		</th>
		<th>
			@if(isset($data['totalIGSTAmount']) && $data['totalIGSTAmount'])
				{{ $data['totalIGSTAmount'] }}
			@else
				0
			@endif
		</th>
		<th>
			@if(isset($data['totalGSTAmount']) && $data['totalGSTAmount'])
				{{ $data['totalGSTAmount'] }}
			@else
				0
			@endif
		</th>
		<th>
			@if(isset($data['totalRefundAmount']) && $data['totalRefundAmount'])
				{{ $data['totalRefundAmount'] }}
			@else
				0
			@endif
		</th>
		<th>
			@if(isset($data['totalSettlementAmount']) && $data['totalSettlementAmount'])
				{{ $data['totalSettlementAmount'] }}
			@else
				0
			@endif
		</th>
	</tr>
</table>
</body>
</html>