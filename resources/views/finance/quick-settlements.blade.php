@extends("finance.base")

@section("settlement-data")

	@if(isset($error) && $error)
		<div class="text-danger">{!! $error !!}</div>
	@else
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mar-t-30">
			@if(isset($data['partners']) && count($data['partners']))
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-center">
					<select id="qsPartnerId" name="qsPartnerId" class="form-control"></select>
					<ul id="qsPartnerOptions" class="hide">
						@foreach($data['partners'] as $partner)
							<li data-id="{{ $partner['partner_type_id'] }}_{{ $partner['partner_id'] }}"
									data-name="{{ $partner['name'] }}"
									data-code="{{ $partner['code'] }}"
									data-partner-id="{{ $partner['partner_id'] }}"
									data-partner-type-id="{{ $partner['partner_type_id'] }}"></li>
						@endforeach
					</ul>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
					<button id="previewQsBtn" class="btn btn-primary" data-url="{{ route('finance.quick-settlements.get') }}">Preview Settlement</button>
				</div>
				<div class="clearfix"></div>
			@endif
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mar-t-30">
			@if(isset($data['partnerSettlementData']) && $data['partnerSettlementData'])
				<div class="partner-settlement-wrap mar-b-30">
					@if(isset($data['partnerSettlementData']['partnerData']) && $data['partnerSettlementData']['partnerData'])
						<div class="partner-info-wrap">
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-12">
								<ul class="tickets-list ls-none">
									<li class="ticket-wrap">
										<div class="top-sec">
											<div class="col-sm-4 col-md-4 col-lg-4">
												<div class="sub-sec-title">Partner Name</div>
												<div class="sub-sec-value">
													<i class="glyphicon glyphicon-user"></i>
													{{ $data['partnerSettlementData']['partnerData']['name'] }} ({{ $data['partnerSettlementData']['partnerData']['partnerType'] }})
													@if(isset($data['partnerSettlementData']['partnerData']['dashLink']) && $data['partnerSettlementData']['partnerData']['dashLink'])
														<form target="_blank"
																action="{{ $data['partnerSettlementData']['partnerData']['dashLink'] }}">
															<input type="submit" class="btn btn-link btn-xs no-pad"
																	value="Partner Info"/>
														</form>
													@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-lg-4">
												<div class="sub-sec-title">Partner Email</div>
												<div class="sub-sec-value">
													<i class="glyphicon glyphicon-envelope"></i>
													{{ $data['partnerSettlementData']['partnerData']['email'] }}
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-lg-4">
												<div class="sub-sec-title">Partner Phone</div>
												<div class="sub-sec-value">
													<i class="glyphicon glyphicon-phone"></i>
													{{ $data['partnerSettlementData']['partnerData']['phone'] }}
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="mid-sec no-mar pad-b-10">
											<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
												Account Number:
												<span class="font-14 text-bold">
												@if(isset($data['partnerSettlementData']['partnerData']['accountNumber']) && $data['partnerSettlementData']['partnerData']['accountNumber'])
														{{ $data['partnerSettlementData']['partnerData']['accountNumber'] }}
													@else
														--
													@endif
											</span>
											</div>
											<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
												Bank Name:
												<span class="font-14 text-bold">
												@if(isset($data['partnerSettlementData']['partnerData']['bankName']) && $data['partnerSettlementData']['partnerData']['bankName'])
														{{ $data['partnerSettlementData']['partnerData']['bankName'] }}
													@else
														--
													@endif
											</span>
											</div>
											<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
												IFSC Code:
												<span class="font-14 text-bold">
												@if(isset($data['partnerSettlementData']['partnerData']['ifscCode']) && $data['partnerSettlementData']['partnerData']['ifscCode'])
														{{ $data['partnerSettlementData']['partnerData']['ifscCode'] }}
													@else
														--
													@endif
											</span>
											</div>
											<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
												Partner City:
												<span class="font-14 text-bold">
												@if(isset($data['partnerSettlementData']['partnerData']['cityName']) && $data['partnerSettlementData']['partnerData']['cityName'])
														{{ $data['partnerSettlementData']['partnerData']['cityName'] }}
													@else
														--
													@endif
											</span>
											</div>
											<div class="clearfix"></div>
										</div>
									</li>
								</ul>
							</div>
							<div class="clearfix"></div>
						</div>
					@endif
					@if(isset($data['partnerSettlementData']['settlementItemsList']) && count($data['partnerSettlementData']['settlementItemsList']))
						<div class="settlements-components-wrap">
							<div class="font-14 text-bold mar-b-10">
								Settlement Items (Bookings + Adjustments)
								<span class="pull-right">
									<span class="cancelled-info-box mar-r-5"></span>
									<span class="text-normal">Cancelled Booking</span>
								</span>
							</div>
							<table class="table table-bordered table-hover">
								<tr>
									<th>Customer Name</th>
									<th>Party Date</th>
									<th>Location</th>
									<th>Booking Id</th>
									<th>Booking Amount</th>
									<th>Advance Amount</th>
									<th>Evibe Service Fee</th>
									<th>CGST</th>
									<th>SGST</th>
									<th>IGST</th>
									<th>Refund Amount</th>
									<th>Partner Bear</th>
									<th>Reference</th>
									<th>Settlement Amount</th>
									<!-- @todo: booking settlement / adjustment with reference -->
								</tr>
								@foreach($data['partnerSettlementData']['settlementItemsList'] as $settlementItem)
									<tr @if(isset($settlementItem['cancelledAt']) && $settlementItem['cancelledAt']) class="cancelled-booking-settlement" @endif>
										<td>
											@if(isset($settlementItem['customerName']) && $settlementItem['customerName'])
												{{ $settlementItem['customerName'] }}
											@else
												--
											@endif
										</td>
										<td>
											@if(isset($settlementItem['partyDateTime']) && $settlementItem['partyDateTime'])
												{{ $settlementItem['partyDateTime'] }}
											@else
												--
											@endif
										</td>
										<td>
											@if(isset($settlementItem['partyLocation']) && $settlementItem['partyLocation'])
												{{ $settlementItem['partyLocation'] }}
											@else
												--
											@endif
										</td>
										<td>
											@if(isset($settlementItem['bookingId']) && $settlementItem['bookingId'])
												@if(isset($settlementItem['bookingDashLink']) && $settlementItem['bookingDashLink'])
													<a href="{{ $settlementItem['bookingDashLink'] }}"
															target="_blank">{{ $settlementItem['bookingId'] }}</a>
												@else
													{{ $settlementItem['bookingId'] }}
												@endif
											@else
												--
											@endif
										</td>
										@if(isset($settlementItem['adjustmentComment']) && $settlementItem['adjustmentComment'])
											<td colspan="8">
												<b>Adjustment Comments: </b>{!! $settlementItem['adjustmentComment'] !!}
											</td>
										@else
											<td>
												@if(isset($settlementItem['bookingAmount']) && $settlementItem['bookingAmount'])
													@price($settlementItem['bookingAmount'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['advanceAmount']) && $settlementItem['advanceAmount'])
													@price($settlementItem['advanceAmount'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['evibeServiceFee']) && $settlementItem['evibeServiceFee'])
													@price($settlementItem['evibeServiceFee'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['cgst']) && $settlementItem['cgst'])
													@price($settlementItem['cgst'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['sgst']) && $settlementItem['sgst'])
													@price($settlementItem['sgst'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['igst']) && $settlementItem['igst'])
													@price($settlementItem['igst'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['refundAmount']) && $settlementItem['refundAmount'])
													@price($settlementItem['refundAmount'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['partnerBear']) && $settlementItem['partnerBear'])
													@price($settlementItem['partnerBear'])
												@else
													--
												@endif
											</td>
										@endif
										<td>
											@if(isset($settlementItem['bookingSettlementLink']) && $settlementItem['bookingSettlementLink'])
												<a href="{{ $settlementItem['bookingSettlementLink'] }}"
														target="_blank">Booking Settlement</a>
											@elseif(isset($settlementItem['adjustmentLink']) && $settlementItem['adjustmentLink'])
												<a href="{{ $settlementItem['adjustmentLink'] }}"
														target="_blank">Adjustment</a>
											@endif
										</td>
										<td>
											@if(isset($settlementItem['settlementAmount']) && $settlementItem['settlementAmount'])
												@if(isset($settlementItem['isPayback']) && $settlementItem['isPayback'])
													<span class="text-green">- @price($settlementItem['settlementAmount'])</span>
												@else
													@price($settlementItem['settlementAmount'])
												@endif
											@else
												--
											@endif
										</td>
									</tr>
								@endforeach
								<tr>
									<td colspan="13">
										<b>Total Settlement Amount: </b>{!! $settlementItem['adjustmentComment'] !!}
									</td>
									<td>
										@if(isset($data['partnerSettlementData']['settlementSummary']['settlementAmount']) && $data['partnerSettlementData']['settlementSummary']['settlementAmount'])
											<b>@price($data['partnerSettlementData']['settlementSummary']['settlementAmount'])</b>
										@else
											--
										@endif
									</td>
								</tr>
							</table>
						</div>
						<div class="settlement-cta-wrap text-center">
							<button class="btn btn-success btn-quick-settle"
									data-toggle="modal"
									data-target="#modalProcessQuickSettlement">Settle Up
							</button>
						</div>
					@else
						<div class="qs-no-booking-msg">
							There are no new bookings or adjustment that needs to be settled for this partner
						</div>
					@endif
				</div>
			@elseif(isset($data['partnerSettlementMsg']) && $data['partnerSettlementMsg'])
				<div class="partner-settlement-error-wrap">
					<div class="partner-settlement-error-msg">
						{{ $data['partnerSettlementMsg'] }}
					</div>
				</div>
			@endif


		</div>
		<div class="clearfix"></div>
	@endif

	<!-- Hidden data -->
	<input type="hidden" id="hidSelectedPartnerId" value="{{ $data['selectedPartnerId'] }}">
	<input type="hidden" id="hidSelectedPartnerTypeId" value="{{ $data['selectedPartnerTypeId'] }}">

	<!-- Modal to capture payment reference for quick settlement -->
	<div id="modalProcessQuickSettlement" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Process Quick Settlement</h4>
				</div>
				<div class="pad-l-20 pad-r-20 pad-t-20">
					<form class="form form-horizontal" role="form">
						<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
						<div class="form-group">
							<label class="col-sm-3">Partner</label>
							<div class="col-sm-9">
								@if(isset($data['partnerSettlementData']['partnerData']['name']) && $data['partnerSettlementData']['partnerData']['name'])
									<span>{{ $data['partnerSettlementData']['partnerData']['name'] }}</span>
									@if(isset($data['partnerSettlementData']['partnerData']['partnerType']) && $data['partnerSettlementData']['partnerData']['partnerType'])
										<span>({{ $data['partnerSettlementData']['partnerData']['partnerType'] }})</span>
									@endif
								@endif
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Settlement Amount</label>
							<div class="col-sm-9">
								<span>
									@if(isset($data['partnerSettlementData']['settlementSummary']['settlementAmount']) && $data['partnerSettlementData']['settlementSummary']['settlementAmount'])
										<span>@price($data['partnerSettlementData']['settlementSummary']['settlementAmount'])</span>
									@endif
								</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Settlement Payment Reference</label>
							<div class="col-sm-9">
								<input type="text" class="settlement-reference form-control"
										id="settlementReference" value="default_payment_reference" placeholder="Settlement transfer reference"/>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-danger btn-quick-settlement-process"
							data-url="{{ route("finance.quick-settlements.process") }}">Submit
					</button>
				</div>
			</div>
			<div class="finance-loader-wrap hide">
				<div class="loader-animation-wrap">
					<img class="loader-animation" src="{{ config('evibe.gallery.host').'/img/app/load_spinner.gif' }}" alt="">
				</div>
			</div>
		</div>
	</div>

@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			(function initViews() {
				$('#modalProcessQuickSettlement').modal({
					show: false,
					keyboard: false,
					backdrop: 'static'
				});
			})();

			var partnerId = $('#hidSelectedPartnerId').val();
			var partnerTypeId = $('#hidSelectedPartnerTypeId').val();

			var $qsPartnerValue;
			var $selectQsPartner;
			var $qsPartner = $('#qsPartnerId');
			var $partnerResults = [];

			// form partners data
			$('#qsPartnerOptions').find('li').each(function (count) {
				$partnerResults.push({
					id: $(this).data('id'),
					name: $(this).data('name'),
					code: $(this).data('code'),
					label: '[' + $(this).data('code') + '] ' + $(this).data('name'),
					partnerId: $(this).data('partner-id'),
					partnerTypeId: $(this).data('partner-type-id')
				});

			});

			$selectQsPartner = $qsPartner.selectize({
				valueField: 'id',
				labelField: 'label',
				searchField: ['label', 'name', 'code'],
				placeholder: "Select a partner",
				render: {
					option: function (data, escape) {
						return "<div data-partner-id='" + data.partnerId + "' data-partner-type-id='" + data.partnerTypeId + "'>" + escape(data.label) + "</div>";
					}
				},
				onChange: function (val) {
					if (!val.length || val == -1) {
						return false;
					}

					var data = this.options[val];
					if ((typeof (data) !== "undefined")) {
						partnerId = data.partnerId;
						partnerTypeId = data.partnerTypeId;
					}
				}
			});

			$qsPartnerValue = $selectQsPartner[0].selectize;

			$qsPartnerValue.enable();
			$qsPartnerValue.addOption($partnerResults);

			if (partnerId && partnerTypeId) {
				$qsPartnerValue.setValue(partnerTypeId + '_' + partnerId, false);
			}

			$('#previewQsBtn').on('click', function () {
				if (!partnerId || !partnerTypeId) {
					alert('Kindly select a partner');
					return false;
				}
				var url = $(this).data('url');
				url += '?';
				url += 'partnerId=' + partnerId + '&partnerTypeId=' + partnerTypeId;
				window.location.href = url;
			});

			$('.btn-quick-settlement-process').click(function (event) {
				event.preventDefault();

				$('.finance-loader-wrap').removeClass('hide');

				$('#modalProcessQuickSettlement .error-msg').addClass('hide');

				$.ajax({
					url: $(this).data('url'),
					type: 'PUT',
					dataType: 'json',
					data: {
						partnerId: partnerId,
						partnerTypeId: partnerTypeId,
						settlementReference: $('#settlementReference').val()
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							$('.finance-loader-wrap').addClass('hide');
							window.showNotySuccess("Settlement is processed successfully");
							setTimeout(function () {
								location.reload();
							}, 2000);
						} else {
							var $error = 'Some error occurred while processing settlement';
							if (data.error) {
								$error = data.error;
							}
							$('.finance-loader-wrap').addClass('hide');

							$('#modalProcessQuickSettlement .error-msg').text($error);
							$('#modalProcessQuickSettlement .error-msg').removeClass('hide')
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

		});
	</script>
@endsection