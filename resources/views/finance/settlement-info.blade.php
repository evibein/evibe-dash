@extends("finance.base")

@section("settlement-data")

	@if(isset($error) && $error)
		<div class="text-danger">{!! $error !!}</div>
	@else
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-b-20 mar-b-10 text-center">
			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
				<a href="{{ route('finance.settlement.get') }}"><< all settlements</a>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 text-center">
				<div class="list-header">
					Settlement Details
				</div>
			</div>
			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-center">
				@if(isset($data['settlementDetails']['settlementDoneAt']) && $data['settlementDetails']['settlementDoneAt'] && (strtotime($data['settlementDetails']['settlementDoneAt']) > 0))
					<div class="font-12">Settlement Done At</div>
					<div>
						<span class="text-green text-bold font-16">
							{{ date('d M Y h:i A', strtotime($data['settlementDetails']['settlementDoneAt'])) }}
						</span>
					</div>
				@else
					<button class="btn btn-success btn-settle"
							data-url="{{ route('finance.settlement.process', $data['settlementDetails']['settlementId']) }}"
							data-toggle="modal"
							data-target="#modalProcessSettlement">Settle Up
					</button>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			@if(isset($data['settlementDetails']) && $data['settlementDetails'])
				<div class="settlement-wrap mar-b-30">
					<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-10 col-lg-offset-1">
						<div class="settlement-info-wrap">
							<ul class="tickets-list ls-none">
								<li class="ticket-wrap">
									<div class="top-sec">
										<div class="col-sm-4 col-md-4 col-lg-4">
											<div class="sub-sec-title">Partner Name</div>
											<div class="sub-sec-value">
												<i class="glyphicon glyphicon-user"></i>
												{{ $data['settlementDetails']['partnerDetails']['name'] }} ({{ $data['settlementDetails']['partnerDetails']['partnerType'] }})
												@if(isset($data['settlementDetails']['partnerDetails']['dashLink']) && $data['settlementDetails']['partnerDetails']['dashLink'])
													<form target="_blank"
															action="{{ $data['settlementDetails']['partnerDetails']['dashLink'] }}">
														<input type="submit" class="btn btn-link btn-xs no-pad"
																value="Partner Info"/>
													</form>
												@endif
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4">
											<div class="sub-sec-title">Partner Email</div>
											<div class="sub-sec-value">
												<i class="glyphicon glyphicon-envelope"></i>
												{{ $data['settlementDetails']['partnerDetails']['email'] }}
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4">
											<div class="sub-sec-title">Settlement Amount</div>
											<div class="sub-sec-value">
												@price($data['settlementDetails']['settlementAmount'])
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="mid-sec no-mar pad-b-10">
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											Account Number:
											<span class="font-14 text-bold">
												@if(isset($data['settlementDetails']['partnerDetails']['accountNumber']) && $data['settlementDetails']['partnerDetails']['accountNumber'])
													{{ $data['settlementDetails']['partnerDetails']['accountNumber'] }}
												@else
													--
												@endif
											</span>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											Bank Name:
											<span class="font-14 text-bold">
												@if(isset($data['settlementDetails']['partnerDetails']['bankName']) && $data['settlementDetails']['partnerDetails']['bankName'])
													{{ $data['settlementDetails']['partnerDetails']['bankName'] }}
												@else
													--
												@endif
											</span>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											IFSC Code:
											<span class="font-14 text-bold">
												@if(isset($data['settlementDetails']['partnerDetails']['ifscCode']) && $data['settlementDetails']['partnerDetails']['ifscCode'])
													{{ $data['settlementDetails']['partnerDetails']['ifscCode'] }}
												@else
													--
												@endif
											</span>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											Partner City:
											<span class="font-14 text-bold">
												@if(isset($data['settlementDetails']['partnerDetails']['cityName']) && $data['settlementDetails']['partnerDetails']['cityName'])
													{{ $data['settlementDetails']['partnerDetails']['cityName'] }}
												@else
													--
												@endif
											</span>
										</div>
										<div class="clearfix"></div>
										@if(isset($data['settlementDetails']['settlementDoneAt']) && $data['settlementDetails']['settlementDoneAt'] && (strtotime($data['settlementDetails']['settlementDoneAt']) > 0))
											<div class="mar-t-10">
												<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
													Settlement Reference:
													<span class="font-14 text-bold">
												@if(isset($data['settlementDetails']['settlementReference']) && $data['settlementDetails']['settlementReference'])
															{{ $data['settlementDetails']['settlementReference'] }}
														@else
															--
														@endif
											</span>
												</div>
												<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
													Invoice Number:
													<span class="font-14 text-bold">
												@if(isset($data['settlementDetails']['invoiceNumber']) && $data['settlementDetails']['invoiceNumber'])
															{{ $data['settlementDetails']['invoiceNumber'] }}
														@else
															--
														@endif
											</span>
												</div>
												<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
													@if(isset($data['settlementDetails']['invoiceUrl']) && $data['settlementDetails']['invoiceUrl'])
														<a href="{{ config('evibe.gallery.host').'/invoices/'.$data['settlementDetails']['invoiceUrl'] }}"
																target="_blank" download>
															Download Invoice
														</a>
													@else
														--
													@endif
												</div>
												<div class="clearfix"></div>
											</div>
										@endif
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
					@if(isset($data['settlementDetails']['settlementItemsList']) && count($data['settlementDetails']['settlementItemsList']))
						<div class="booking-settlements-wrap">
							<div class="font-14 text-bold mar-b-10">
								Settlement Items (Bookings + Adjustments)
								<span class="pull-right">
									<span class="cancelled-info-box mar-r-5"></span>
									<span class="text-normal">Cancelled Booking</span>
								</span>
							</div>
							<table class="table table-bordered table-hover">
								<tr>
									<th>Customer Name</th>
									<th>Party Date</th>
									<th>Location</th>
									<th>Booking Id</th>
									<th>Booking Amount</th>
									<th>Advance Amount</th>
									<th>Evibe Service Fee</th>
									<th>CGST</th>
									<th>SGST</th>
									<th>IGST</th>
									<th>Refund Amount</th>
									<th>Partner Bear</th>
									<th>Reference</th>
									<th>Settlement Amount</th>
								<!-- @todo: booking settlement / adjustment with reference -->
								</tr>
								@foreach($data['settlementDetails']['settlementItemsList'] as $settlementItem)
									<tr @if(isset($settlementItem['cancelledAt']) && $settlementItem['cancelledAt']) class="cancelled-booking-settlement" @endif>
										<td>
											@if(isset($settlementItem['customerName']) && $settlementItem['customerName'])
												{{ $settlementItem['customerName'] }}
											@else
												--
											@endif
										</td>
										<td>
											@if(isset($settlementItem['partyDateTime']) && $settlementItem['partyDateTime'])
												{{ $settlementItem['partyDateTime'] }}
											@else
												--
											@endif
										</td>
										<td>
											@if(isset($settlementItem['partyLocation']) && $settlementItem['partyLocation'])
												{{ $settlementItem['partyLocation'] }}
											@else
												--
											@endif
										</td>
										<td>
											@if(isset($settlementItem['bookingId']) && $settlementItem['bookingId'])
												@if(isset($settlementItem['bookingDashLink']) && $settlementItem['bookingDashLink'])
													<a href="{{ $settlementItem['bookingDashLink'] }}"
															target="_blank">{{ $settlementItem['bookingId'] }}</a>
												@else
													{{ $settlementItem['bookingId'] }}
												@endif
											@else
												--
											@endif
										</td>
										@if(isset($settlementItem['adjustmentComment']) && $settlementItem['adjustmentComment'])
											<td colspan="8">
												<b>Adjustment Comments: </b>{{ $settlementItem['adjustmentComment'] }}
											</td>
										@else
											<td>
												@if(isset($settlementItem['bookingAmount']) && $settlementItem['bookingAmount'])
													@price($settlementItem['bookingAmount'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['advanceAmount']) && $settlementItem['advanceAmount'])
													@price($settlementItem['advanceAmount'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['evibeServiceFee']) && $settlementItem['evibeServiceFee'])
													@price($settlementItem['evibeServiceFee'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['cgst']) && $settlementItem['cgst'])
													@price($settlementItem['cgst'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['sgst']) && $settlementItem['sgst'])
													@price($settlementItem['sgst'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['igst']) && $settlementItem['igst'])
													@price($settlementItem['igst'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['refundAmount']) && $settlementItem['refundAmount'])
													@price($settlementItem['refundAmount'])
												@else
													--
												@endif
											</td>
											<td>
												@if(isset($settlementItem['partnerBear']) && $settlementItem['partnerBear'])
													@price($settlementItem['partnerBear'])
												@else
													--
												@endif
											</td>
										@endif
										<td>
											@if(isset($settlementItem['bookingSettlementLink']) && $settlementItem['bookingSettlementLink'])
												<a href="{{ $settlementItem['bookingSettlementLink'] }}"
														target="_blank">Booking Settlement</a>
											@elseif(isset($settlementItem['adjustmentLink']) && $settlementItem['adjustmentLink'])
												<a href="{{ $settlementItem['adjustmentLink'] }}"
														target="_blank">Adjustment</a>
											@endif
										</td>
										<td>
											@if(isset($settlementItem['settlementAmount']) && $settlementItem['settlementAmount'])
												@if(isset($settlementItem['isPayback']) && $settlementItem['isPayback'])
													<span class="text-green">- @price($settlementItem['settlementAmount'])</span>
												@else
													@price($settlementItem['settlementAmount'])
												@endif
											@else
												--
											@endif
										</td>
									</tr>
								@endforeach
							</table>
						</div>
					@endif
				</div>
			@endif
		</div>
		<div class="clearfix"></div>
	@endif

	<!-- Modal to capture payment reference for settlement -->
	<div id="modalProcessSettlement" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Process Settlement</h4>
				</div>
				<div class="pad-l-20 pad-r-20 pad-t-20">
					<form class="form form-horizontal" role="form">
						<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
						<div class="form-group">
							<label class="col-sm-3">Partner</label>
							<div class="col-sm-9">
								@if(isset($data['settlementDetails']['partnerDetails']['name']) && $data['settlementDetails']['partnerDetails']['name'])
									<span>{{ $data['settlementDetails']['partnerDetails']['name'] }}</span>
									@if(isset($data['settlementDetails']['partnerDetails']['partnerType']) && $data['settlementDetails']['partnerDetails']['partnerType'])
										<span>({{ $data['settlementDetails']['partnerDetails']['partnerType'] }})</span>
									@endif
								@endif
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Settlement Amount</label>
							<div class="col-sm-9">
								<span>
									@if(isset($data['settlementDetails']['settlementAmount']) && $data['settlementDetails']['settlementAmount'])
										<span>@price($data['settlementDetails']['settlementAmount'])</span>
									@endif
								</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Settlement Payment Reference</label>
							<div class="col-sm-9">
								<input type="text" class="settlement-reference form-control"
										id="settlementReference" value="default_payment_reference" placeholder="Settlement transfer reference"/>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-danger btn-settlement-process"
							data-url="{{ route("finance.settlement.process", $data['settlementDetails']['settlementId']) }}">Submit
					</button>
				</div>
			</div>
			<div class="finance-loader-wrap hide">
				<div class="loader-animation-wrap">
					<img class="loader-animation" src="{{ config('evibe.gallery.host').'/img/app/load_spinner.gif' }}" alt="">
				</div>
			</div>
		</div>
	</div>

@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {

			(function initViews() {
				$('#modalProcessSettlement').modal({
					show: false,
					keyboard: false,
					backdrop: 'static'
				});
			})();

			$('.btn-settlement-process').click(function (event) {
				event.preventDefault();

				$('.finance-loader-wrap').removeClass('hide');

				$('#modalProcessSettlement .error-msg').addClass('hide');

				$.ajax({
					url: $(this).data('url'),
					type: 'PUT',
					dataType: 'json',
					data: {
						settlementReference: $('#settlementReference').val()
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							$('.finance-loader-wrap').addClass('hide');
							window.showNotySuccess("Settlement is processed successfully");
							setTimeout(function () {
								location.reload();
							}, 2000);
						}
						else {
							var $error = 'Some error occurred while processing settlement';
							if (data.error) {
								$error = data.error;
							}
							$('.finance-loader-wrap').addClass('hide');

							$('#modalProcessSettlement .error-msg').text($error);
							$('#modalProcessSettlement .error-msg').removeClass('hide')
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

		});
	</script>
@endsection