<html>
<body>
<table>
	@if(isset($data['settlementsList']) && count($data['settlementsList']))
		@foreach($data['settlementsList'] as $settlement)
			<tr>
				<td>
					EVIBETECH
				</td>
				<td>
					{{ $data['generatedAt'] }}
				</td>
				<td>
					EVIBETECH
				</td>
				<td>
					{{ $settlement['settlementAmountDecimal'] }}
				</td>
				<td>
					0202102000015817
				</td>
				<td>
					I
				</td>
				<td>
					@if(isset($settlement['ifscCode']) && $settlement['ifscCode'])
						{{ $settlement['ifscCode'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['accountName']) && $settlement['accountName'])
						{{ $settlement['accountName'] }}
					@else
						--
					@endif
				</td>
				<td></td>
				<td>
					998
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
					@if(isset($settlement['accountNumber']) && $settlement['accountNumber'])
						{{ $settlement['accountNumber'] }}
					@else
						--
					@endif
				</td>
			</tr>
		@endforeach
	@endif
</table>
</body>
</html>