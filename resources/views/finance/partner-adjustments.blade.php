@extends("finance.base")

@section("settlement-data")

	@if(isset($error) && $error)
		<div class="text-danger">{!! $error !!}</div>
	@else
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center hide">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 text-left no-pad-l">
				<div class="list-header">
					Partner Adjustments
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-b-20 mar-b-10 text-center">
			<div class="list-top-filters">
				<a href="{{ route('finance.partner-adjustment.list', 'pending') }}"
						class="@if(isset($data['query']) && ($data['query'] == 'pending')) active @endif">Created</a>
				<a href="{{ route('finance.partner-adjustment.list', 'processed') }}"
						class="@if(isset($data['query']) && ($data['query'] == 'processed')) active @endif">Processed</a>
				<a href="{{ route('finance.partner-adjustment.list', 'settled') }}"
						class="@if(isset($data['query']) && ($data['query'] == 'settled')) active @endif">Settled</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="list-side-filters pad-10">
				<div class="list-filter-title mar-t-10">
					Filters
					@if(isset($data['filters']['clearFilter']) && $data['filters']['clearFilter'])
						<span class="btn btn-xs btn-warning pull-right btn-clear-filter">Clear all filters</span>
					@endif
					<div class="clearfix"></div>
				</div>
				<hr class="list-filter-hr">
				<div class="list-filter-wrap">
					<div class="list-filter-sub-title mar-b-10">
						Select Partner:
					</div>
					<div class="list-filter-sub-body">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<label>
								<input type="radio" name="typePartner"
										value="{{ config('evibe.ticket_type.planners') }}"
										@if(isset($data['filters']['partnerTypeId']) && $data['filters']['partnerTypeId'] == config('evibe.ticket_type.planners')) checked="checked" @endif>
								Planner
							</label>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<label>
								<input type="radio" name="typePartner" value="{{ config('evibe.ticket_type.venues') }}"
										@if(isset($data['filters']['partnerTypeId']) && $data['filters']['partnerTypeId'] == config('evibe.ticket_type.venues')) checked="checked" @endif>
								Venue
							</label>
						</div>
						<div class="clearfix"></div>
						<select class="partner-select planner-select full-width hide" id="plannerSelect">
							<option value="0">-- Select a planner --</option>
							@if(isset($data['plannerPartnersList']) && count($data['plannerPartnersList']))
								@foreach($data['plannerPartnersList'] as $plannerPartner)
									<option value="{{ $plannerPartner['id'] }}"
											@if(isset($data['filters']['partnerId']) && $data['filters']['partnerId'] == $plannerPartner['id']) selected @endif>{{ $plannerPartner['name'] }} - {{ $plannerPartner['email'] }}</option>
								@endforeach
							@endif
						</select>
						<select class="partner-select venue-select full-width hide" id="venueSelect">
							<option value="0">-- Select a venue --</option>
							@if(isset($data['venuePartnersList']) && count($data['venuePartnersList']))
								@foreach($data['venuePartnersList'] as $venuePartner)
									<option value="{{ $venuePartner['id'] }}"
											@if(isset($data['filters']['partnerId']) && $data['filters']['partnerId'] == $venuePartner['id']) selected @endif>{{ $venuePartner['name'] }} - {{ $venuePartner['email'] }}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
				<hr class="list-filter-hr">
				<div class="list-filter-wrap">
					<div class="list-filter-sub-title mar-b-10">
						Schedule Date:
					</div>
					<div class="list-filter-sub-body">
						<input type="text" class="schedule-date form-control"
								@if(isset($data['filters']['scheduleDate']) && $data['filters']['scheduleDate'])
								value="{{ $data['filters']['scheduleDate'] }}" @endif>
					</div>
				</div>
				<div class="list-filter-cta text-center mar-t-15 mar-b-5">
					<div class="btn btn-primary" id="filterAdjustments">Filter</div>
				</div>
				<input type="hidden" id="hidAdjustmentQuery"
						value="@if(isset($data['query']) && $data['query']) {{ $data['query'] }} @endif">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<div class="finance-list-info">
				<i class="glyphicon glyphicon-info-sign"></i>
				<span>
					@if(isset($data['query']) && ($data['query'] == 'pending'))
						Partner Adjustments that have been created and will be included in a settlement based on their schedule dates.
					@elseif(isset($data['query']) && ($data['query'] == 'processed'))
						Partner Adjustments that have been processed, but not yet settled. (Included in a settlement, but settlement has not been yet settled. Editing or deleting the adjustments will result in affecting all the settlements which contain those adjustments.
					@elseif(isset($data['query']) && ($data['query'] == 'settled'))
						Partner Adjustments that have been settled to partners.
					@endif
				</span>
			</div>
			@if(isset($data['adjustmentsList']) && count($data['adjustmentsList']))
				<div class="adjustments-list-wrap">
					<ul class="tickets-list ls-none">
						@foreach($data['adjustmentsList'] as $adjustment)
							<li class="ticket-wrap">
								<div class="top-sec">
									<div class="col-sm-3 col-md-3 col-lg-3">
										<div class="sub-sec-title">Partner Name</div>
										<div class="sub-sec-value">
											@if($adjustment['partnerDetails']['name'])
												<i class="glyphicon glyphicon-user"></i>
												{{ $adjustment['partnerDetails']['name'] }}
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-sm-3 col-md-3 col-lg-3 no-pad-l">
										<div class="sub-sec-title">Partner Email</div>
										<div class="sub-sec-value">
											@if($adjustment['partnerDetails']['email'])
												<i class="glyphicon glyphicon-envelope"></i>
												{{ $adjustment['partnerDetails']['email'] }}
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-sm-3 col-md-3 col-lg-3 no-pad-l">
										<div class="sub-sec-title">Adjustment Amount</div>
										@if($adjustment['isPayback'])
											<div class="font-16 text-bold text-green">
												- @price( $adjustment['adjustmentAmount'] )
											</div>
										@else
											<div class="sub-sec-value">
												@price( $adjustment['adjustmentAmount'] )
											</div>
										@endif
									</div>
									<div class="col-sm-1 col-md-1 col-lg-1 no-pad-l hide">
										<div class="sub-sec-title">Payback</div>
										<div class="sub-sec-value">
											@if($adjustment['isPayback'])
												<i class="glyphicon glyphicon-ok"></i>
											@else
												<i class="glyphicon glyphicon-remove"></i>
											@endif
										</div>
									</div>
									<div class="col-sm-3 col-md-3 col-lg-3 no-pad-l">
										<div class="sub-sec-title">
											@if($adjustment['isProcessed'])
												Processed at
											@else
												Will be processed at
											@endif
										</div>
										<div class="sub-sec-value">
											<i class="glyphicon glyphicon-time"></i>
											{{ $adjustment['scheduleDate'] }}
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="mid-sec no-mar pad-b-10">
									@if($adjustment['isPayback'])
										<div class="col-sm-12 col-md-12 col-lg-12 font-13 mar-b-10">
											<span class="text-green">
												@if(isset($adjustment['adjustmentDoneAt']) && $adjustment['adjustmentDoneAt'])
													Adjustment amount has been paid by partner to us
												@else
													Adjustment amount shall be paid by partner to us
												@endif
											</span>
										</div>
										<div class="clearfix"></div>
									@endif
									<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar hide">
										Handler:
										<span class="pad-l-5 font-14 text-bold">{{ $adjustment['handlerDetails']['name'] }}</span>
									</div>
									<div class="col-sm-5 col-md-5 col-lg-5 font-13 no-mar">
										<div>
											Adjusted At:
											<span class="pad-l-5 font-14 text-bold">
												@if($adjustment['adjustmentDoneAt'])
													<span class="text-green">
													<i class="glyphicon glyphicon-ok-sign"></i>
														{{ $adjustment['adjustmentDoneAt'] }}
												</span>
												@else
													--
												@endif
											</span>
										</div>
										<div>
											Settlement Reference:
											<span class="pad-l-5 font-14 text-bold">
												@if($adjustment['settlementReference'])
													{{ $adjustment['settlementReference'] }}
												@else
													--
												@endif
											</span>
										</div>
										@if($adjustment['settlementId'])
											<div>
												<a href="{{ route('finance.settlement.info', $adjustment['settlementId']) }}"
														target="_blank">(link to settlement)</a>
											</div>
										@endif
									</div>
									<div class="col-sm-5 col-md-5 col-lg-5 font-13 no-mar no-pad-l">
										@if($adjustment['ticketBookingId'])
											<div>
												Customer Name:
												<span class="pad-l-5 font-14 text-bold">
												@if($adjustment['customerName'])
														{{ $adjustment['customerName'] }}
													@else
														--
													@endif
											</span>
											</div>
											<div>
												Party Date:
												<span class="pad-l-5 font-14 text-bold">
												@if($adjustment['partyDateTime'])
														{{ $adjustment['partyDateTime'] }}
													@else
														--
													@endif
											</span>
											</div>
											<div>
												Booking ID:
												<span class="pad-l-5 font-14 text-bold">
												@if($adjustment['bookingId'])
														@if($adjustment['ticketId'])
															<a href="{{ route('ticket.details.bookings', $adjustment['ticketId']) }}"
																	target="_blank">{{ $adjustment['bookingId'] }}</a>
														@else
															{{ $adjustment['bookingId'] }}
														@endif
													@else
														--
													@endif
										</span>
											</div>
										@endif
									</div>
									<div class="col-sm-2 col-md-2 col-lg-2 font-13 no-mar no-pad-l">
										@if($adjustment['adjustmentDoneAt'])
											<div class="finance-status text-green">Settled</div>
										@else
											<button class="btn btn-primary btn-xs btn-edit-adjustment"
													data-toggle="modal"
													data-target="#modalEditPartnerAdjustment"
													data-adjustment-id="{{ $adjustment['id'] }}"
													data-partner-id="{{ $adjustment['partnerDetails']['partnerId'] }}"
													data-partner="{{ $adjustment['partnerDetails']['name'] }}"
													data-partner-type-id="{{ $adjustment['partnerDetails']['partnerTypeId'] }}"
													data-partner-type="{{ $adjustment['partnerDetails']['partnerType'] }}"
													data-adjustment-amount="{{ $adjustment['adjustmentAmount'] }}"
													data-is-payback="{{ $adjustment['isPayback'] }}"
													data-booking-id="{{ $adjustment['bookingId'] }}"
													data-schedule-date="{{ $adjustment['scheduleDate'] }}"
													data-comments="{!! $adjustment['comments'] !!}"
													@if(isset($adjustment['adjustmentDoneAt']) && $adjustment['adjustmentDoneAt']) disabled @endif>
												<i class="glyphicon glyphicon-pencil"></i> Edit
											</button>
											<button class="btn btn-danger btn-xs btn-deleted-adjustment"
													data-url="{{ route('finance.partner-adjustment.delete') }}"
													data-adjustment-id="{{ $adjustment['id'] }}"
													@if(isset($adjustment['adjustmentDoneAt']) && $adjustment['adjustmentDoneAt']) disabled @endif>
												<i class="glyphicon glyphicon-trash"></i> Delete
											</button>
										@endif
									</div>
									<div class="clearfix"></div>
									<div class="col-sm-12 col-md-12 col-lg-12 font-13 mar-t-10">
										Comments:
										<span class="pad-l-5 font-14 text-bold">{!! $adjustment['comments'] !!}</span>
									</div>
									<div class="col-sm-12 col-md-12 col-lg-12 font-11 mar-t-10 text-muted">
										@if($adjustment['adjustmentUpdatedAt'] > $adjustment['adjustmentCreatedAt'])
											Adjustment has been last updated at
											<b>{{ $adjustment['adjustmentUpdatedAt'] }}</b> by
											<b>{{ $adjustment['handlerDetails']['name'] }}</b>
										@else
											Adjustment has been created at
											<b>{{ $adjustment['adjustmentCreatedAt'] }}</b> by
											<b>{{ $adjustment['handlerDetails']['name'] }}</b>
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			@else
				<div class="text-danger">No partner adjustments are found!</div>
			@endif
		</div>
		<div class="clearfix"></div>

		@include("finance.partner-adjustment-modals")
	@endif

@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var $plannerType = $('#hidPlannerType').val();
			var $venueType = $('#hidVenueType').val();

			(function initViews() {
				$('#modalAddPartnerAdjustment, #modalEditPartnerAdjustment').modal({
					show: false,
					keyboard: false,
					backdrop: 'static'
				});

				$('.pa-ticket-booking-id').selectize();

				$('.pa-planner-select').selectize();
				$('.pa-venue-select').selectize();
			})();

			$('.schedule-date, .pa-edit-schedule-date').datetimepicker({
				step: 30,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				maxDate: false,
				timepicker: false,
				format: 'Y-m-d',
				beforeShowDay: function (date) {
					var day = date.getDay();
					return [day == 4, ""];
				}
			});

			function filterAdjustments() {
				var $currentUrl = window.location.href;
				$currentUrl = $currentUrl.split('?')[0];
				var $partnerId = null;

				var $partnerTypeId = $("input[type='radio'][name='typePartner']:checked").val();
				if ($partnerTypeId === $plannerType) {
					$partnerId = $('#plannerSelect').val();
				}
				else if ($partnerTypeId === $venueType) {
					$partnerId = $('#venueSelect').val();
				}

				var $scheduleDate = $('.schedule-date').val();

				if (typeof $partnerTypeId == 'undefined') {
					$partnerTypeId = null;
				}

				if ($partnerId == 0) {
					$partnerId = null;
				}

				if ($partnerTypeId || $scheduleDate) {
					$currentUrl += '?';
					if ($partnerTypeId) {
						$currentUrl += 'partnerTypeId=' + $partnerTypeId + '&';
					}

					if ($partnerId) {
						$currentUrl += 'partnerId=' + $partnerId + '&';
					}

					if ($scheduleDate) {
						$currentUrl += 'scheduleDate=' + $scheduleDate;
					}
				}

				window.location.href = $currentUrl;
			}

			$('#filterAdjustments').click(function (event) {
				event.preventDefault();

				filterAdjustments();
			});

			$('.btn-clear-filter').click(function (event) {
				event.preventDefault();

				var $clearFilterUrl = window.location.href.split('?')[0];
				window.location.href = $clearFilterUrl;
			});

			$('#modalAddPartnerAdjustment').on('shown.bs.modal', function (e) {
				partnerSelectModal();
				$('#modalAddPartnerAdjustment .error-msg').addClass('hide');
			});

			$('.pa-partner-type').on('change', function (event) {
				event.preventDefault();

				partnerSelectModal();
			});

			function partnerSelectModal() {
				$('.pa-planner-select').addClass('hide');
				$('.pa-venue-select').addClass('hide');

				if ($('.pa-partner-type').val() === $plannerType) {
					$('.pa-planner-select').removeClass('hide');
				}
				else {
					$('.pa-venue-select').removeClass('hide');
				}
			}

			$('.btn-pa-submit').click(function (event) {
				event.preventDefault();

				$('#modalAddPartnerAdjustment .error-msg').addClass('hide');
				var $partnerTypeId = $('.pa-partner-type').val();

				if ($partnerTypeId === $plannerType) {
					var $partnerId = $('#paPlannerSelect').val();
				}
				else {
					var $partnerId = $('#paVenueSelect').val();
				}

				$.ajax({
					url: $(this).data('url'),
					type: 'POST',
					dataType: 'json',
					data: {
						partnerId: $partnerId,
						partnerTypeId: $partnerTypeId,
						ticketBookingId: $('#paTicketBookingId').val(),
						adjustmentAmount: $('#paAdjustmentAmount').val(),
						isPayback: $('#paIsPayback').val(),
						comments: $('#paComments').val()
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							window.showNotySuccess("Partner adjustment added successfully");
							setTimeout(function () {
								location.reload();
							}, 2000);
						}
						else {
							var $error = 'Some error occurred while creating adjustment';
							if (data.error) {
								$error = data.error;
							}
							$('#modalAddPartnerAdjustment .error-msg').text($error);
							$('#modalAddPartnerAdjustment .error-msg').removeClass('hide');
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

			$('.btn-edit-adjustment').click(function (event) {
				event.preventDefault();

				$('.hid-pa-adjustment-id').val($(this).data('adjustment-id'));
				$('.pa-edit-partner-type').text($(this).data('partner-type'));
				$('.pa-edit-partner').text($(this).data('partner'));
				$('.pa-edit-booking-id').text($(this).data('booking-id'));
				$('.pa-edit-adjustment-amount').val($(this).data('adjustment-amount'));
				$('.pa-edit-is-payback').val($(this).data('is-payback'));
				$('.pa-edit-schedule-date').val($(this).data('schedule-date'));
				$('.pa-edit-comments').text($(this).data('comments'));
			});

			$('#modalEditPartnerAdjustment').on('shown.bs.modal', function (e) {
				$('#modalEditPartnerAdjustment .error-msg').addClass('hide');
			});

			$('.btn-pa-update').click(function (event) {
				event.preventDefault();

				$('#modalEditPartnerAdjustment .error-msg').addClass('hide');

				$.ajax({
					url: $(this).data('url'),
					type: 'PUT',
					dataType: 'json',
					data: {
						adjustmentId: $('.hid-pa-adjustment-id').val(),
						adjustmentAmount: $('.pa-edit-adjustment-amount').val(),
						isPayback: $('.pa-edit-is-payback').val(),
						scheduleDate: $('.pa-edit-schedule-date').val()
						//						comments: $('.pa-edit-comments').val()
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							window.showNotySuccess("Partner adjustment edited successfully");
							setTimeout(function () {
								location.reload();
							}, 2000);
						}
						else {
							var $error = 'Some error occurred while editing adjustment';
							if (data.error) {
								$error = data.error;
							}
							$('#modalEditPartnerAdjustment .error-msg').text($error);
							$('#modalEditPartnerAdjustment .error-msg').removeClass('hide');
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

			$('.btn-deleted-adjustment').click(function (event) {
				event.preventDefault();

				var c = confirm('Are you sure to deleted this adjustment?');
				if (c == true) {
					$.ajax({
						url: $(this).data('url'),
						type: 'PUT',
						dataType: 'json',
						data: {
							adjustmentId: $(this).data('adjustment-id')
						},
						success: function (data) {
							if (data.success && (data.success === true)) {
								window.showNotySuccess("Partner adjustment deleted successfully");
								setTimeout(function () {
									location.reload();
								}, 2000);
							}
							else {
								var $error = 'Some error occurred while deleting adjustment';
								if (data.error) {
									$error = data.error;
								}
								window.showNotyError($error);
							}
						},
						fail: function () {
							window.showNotyError();
						}
					});
				}
			});

		});
	</script>
@endsection