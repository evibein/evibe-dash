@extends("finance.base")

@section("settlement-data")

	@if(isset($error) && $error)
		<div class="text-danger">{!! $error !!}</div>
	@else
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center hide">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 text-left no-pad-l">
				<div class="list-header">
					Customer Refunds
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-b-20 mar-b-10 text-center">
			<div class="list-top-filters">
				<a href="{{ route('finance.customer-refund.list', 'pending') }}"
						class="@if(isset($data['query']) && ($data['query'] == 'pending')) active @endif">Created</a>
				<a href="{{ route('finance.customer-refund.list', 'refunded') }}"
						class="@if(isset($data['query']) && ($data['query'] == 'refunded')) active @endif">Refunded</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="list-side-filters pad-10">
				<div class="list-filter-title mar-t-10">
					Filters
					@if(isset($data['filters']['clearFilter']) && $data['filters']['clearFilter'])
						<span class="btn btn-xs btn-warning pull-right btn-clear-filter">Clear all filters</span>
					@endif
					<div class="clearfix"></div>
				</div>
				<hr class="list-filter-hr">
				<div class="list-filter-wrap">
					<div class="list-filter-sub-title mar-b-10">
						Search:
					</div>
					<div class="list-filter-sub-body">
						<input type="text" id="refundSearch" class="form-control"
								@if(isset($data['filters']['search']) && $data['filters']['search'])
								value="{{ $data['filters']['search'] }}" @endif
						placeholder="Customer Name or Booking Id">
					</div>
				</div>
				@if(isset($data['query']) && ($data['query'] == 'refunded'))
					<hr class="list-filter-hr">
					<div class="list-filter-wrap">
						<div class="list-filter-sub-title mar-b-10">
							Refunded Date:
						</div>
						<div class="list-filter-sub-body">
							<input type="text" class="refund-date form-control" id="minRefundDate"
									@if(isset($data['filters']['minRefundDate']) && $data['filters']['minRefundDate'])
									value="{{ $data['filters']['minRefundDate'] }}" @endif>
							<div class="text-center mar-t-5 mar-b-5 font-12">TO</div>
							<input type="text" class="refund-date form-control" id="maxRefundDate"
									@if(isset($data['filters']['maxRefundDate']) && $data['filters']['maxRefundDate'])
									value="{{ $data['filters']['maxRefundDate'] }}" @endif>
						</div>
					</div>
				@endif
				<div class="list-filter-cta text-center mar-t-15 mar-b-5">
					<div class="btn btn-primary" id="filterRefunds">Filter</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<div class="finance-list-info">
				<i class="glyphicon glyphicon-info-sign"></i>
				<span>
					@if(isset($data['query']) && ($data['query'] == 'pending'))
						Customer refunds that have been created, which needs to be processed.
					@elseif(isset($data['query']) && ($data['query'] == 'refunded'))
						Customer refunds, that have been processed (sent to customers).
					@endif
				</span>
			</div>
			@if(isset($data['refundsList']) && count($data['refundsList']))
				<div class="refunds-list-wrap">
					<ul class="tickets-list ls-none">
						@foreach($data['refundsList'] as $refund)
							<li class="ticket-wrap">
								<div class="top-sec">
									<div class="col-sm-3 col-md-3 col-lg-3">
										<div class="sub-sec-title">Customer Name</div>
										<div class="sub-sec-value">
											@if(isset($refund['customerName']) && $refund['customerName'])
												<i class="glyphicon glyphicon-user"></i>
												{{ $refund['customerName'] }}
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-sm-3 col-md-3 col-lg-3">
										<div class="sub-sec-title">Party Date</div>
										<div class="sub-sec-value">
											@if(isset($refund['partyDateTime']) && $refund['partyDateTime'])
												<i class="glyphicon glyphicon-time"></i>
												{{ $refund['partyDateTime'] }}
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-sm-3 col-md-3 col-lg-3">
										<div class="sub-sec-title">Party Location</div>
										<div class="sub-sec-value">
											@if(isset($refund['partyLocation']) && $refund['partyLocation'])
												{{ $refund['partyLocation'] }} @if(isset($refund['partyCity']) && $refund['partyCity']) ({{ $refund['partyCity'] }}) @endif
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-sm-3 col-md-3 col-lg-3">
										<div class="sub-sec-title">Refund Amount</div>
										<div class="sub-sec-value">
											@if(isset($refund['refundAmount']) && $refund['refundAmount'])
												@price($refund['refundAmount'])
											@else
												--
											@endif
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="mid-sec pad-b-15">
									<div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											<div>
												Booking Amount:
												<span class="pad-l-5 font-14 text-bold">
													@if(isset($refund['bookingAmount']) && $refund['bookingAmount'])
														@price($refund['bookingAmount'])
													@else
														--
													@endif
												</span>
											</div>
											<div>
												Advance Amount:
												<span class="pad-l-5 font-14 text-bold">
													@if(isset($refund['advanceAmount']) && $refund['advanceAmount'])
														@price($refund['advanceAmount'])
													@else
														--
													@endif
												</span>
											</div>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											<div>
												Partner Bear:
												<span class="pad-l-5 font-14 text-bold">
												@if(isset($refund['partnerBear']) && $refund['partnerBear'])
														@price($refund['partnerBear'])
													@else
														--
													@endif
												</span>
											</div>
											<div>
												Partner Name:
												<span class="pad-l-5 font-14 text-bold">
													@if(isset($refund['partnerDetails']['name']) && $refund['partnerDetails']['name'])
														@if(isset($refund['partnerDetails']['name']) && $refund['partnerDetails']['name'])
															{{ $refund['partnerDetails']['name'] }} ({{ $refund['partnerDetails']['partnerType'] }})
														@else
															{{ $refund['partnerDetails']['name'] }}
														@endif
													@else
														--
													@endif
												</span>
											</div>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											Evibe Bear:
											<span class="pad-l-5 font-14 text-bold">
												@if(isset($refund['evibeBear']) && $refund['evibeBear'])
													@price($refund['evibeBear'])
												@else
													--
												@endif
											</span>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											@if(isset($refund['refundedAt']) && $refund['refundedAt'])
												<div class="finance-status text-green">Refunded</div>
											@else
												<button class="btn btn-primary btn-xs btn-edit-refund"
														data-toggle="modal"
														data-target="#modalEditCustomerRefund"
														data-ticket-booking-id="{{ $refund['ticketBookingId'] }}"
														data-booking-id="{{ $refund['bookingId'] }}"
														data-party-date="{{ $refund['partyDateTime'] }}"
														data-party-location="{{ $refund['partyLocation'] }}"
														data-customer-name="{{ $refund['customerName'] }}"
														data-booking-amount="{{ $refund['bookingAmount'] }}"
														data-advance-amount="{{ $refund['advanceAmount'] }}"
														data-refund-amount="{{ $refund['refundAmount'] }}"
														data-evibe-bear="{{ $refund['evibeBear'] }}"
														data-partner-bear="{{ $refund['partnerBear'] }}"
														data-comments="{!! $refund['comments'] !!}">
													<i class="glyphicon glyphicon-pencil"></i> Edit
												</button>
												@if(AppUtil::isAdmin())
													<button class="btn btn-success btn-xs btn-approve-refund"
															data-toggle="modal"
															data-target="#modalApproveCustomerRefund"
															data-ticket-booking-id="{{ $refund['ticketBookingId'] }}"
															data-booking-id="{{ $refund['bookingId'] }}"
															data-party-date="{{ $refund['partyDateTime'] }}"
															data-party-location="{{ $refund['partyLocation'] }}"
															data-customer-name="{{ $refund['customerName'] }}"
															data-booking-amount="{{ $refund['bookingAmount'] }}"
															data-advance-amount="{{ $refund['advanceAmount'] }}"
															data-refund-amount="{{ $refund['refundAmount'] }}"
															data-evibe-bear="{{ $refund['evibeBear'] }}"
															data-partner-bear="{{ $refund['partnerBear'] }}"
															data-comments="{!! $refund['comments'] !!}">
														<i class="glyphicon glyphicon-ok"></i> Process
													</button>
												@endif
												<button class="btn btn-danger btn-xs btn-reject-refund"
														data-toggle="modal"
														data-target="#modalRejectCustomerRefund"
														data-ticket-booking-id="{{ $refund['ticketBookingId'] }}"
														data-booking-id="{{ $refund['bookingId'] }}"
														data-party-date="{{ $refund['partyDateTime'] }}"
														data-party-location="{{ $refund['partyLocation'] }}"
														data-customer-name="{{ $refund['customerName'] }}">
													<i class="glyphicon glyphicon-trash"></i> Reject
												</button>
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="mar-t-15">
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											Booking ID:
											<span class="pad-l-5 font-14 text-bold">
												@if(isset($refund['bookingId']) && $refund['bookingId'])
													@if(isset($refund['bookingDashLink']) && $refund['bookingDashLink'])
														<a href="{{ $refund['bookingDashLink'] }}"
																target="_blank">{{ $refund['bookingId'] }}</a>
													@else
														{{ $refund['bookingId'] }}
													@endif
												@else
													--
												@endif
											</span>
										</div>
										<div class="col-sm-5 col-md-5 col-lg-5 font-13 no-mar">
											<div>
												Customer Email:
												<span class="pad-l-5 font-14 text-bold">
													@if(isset($refund['customerEmail']) && $refund['customerEmail'])
														{{ $refund['customerEmail'] }}
													@else
														--
													@endif
												</span>
											</div>
											<div>
												Customer Phone:
												<span class="pad-l-5 font-14 text-bold">
													@if(isset($refund['customerPhone']) && $refund['customerPhone'])
														{{ $refund['customerPhone'] }}
													@else
														--
													@endif
												</span>
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4 font-13 no-mar">
											<div>
												Refunded At:
												<span class="pad-l-5 font-14 text-bold text-green">
												@if(isset($refund['refundedAt']) && $refund['refundedAt'])
														<i class="glyphicon glyphicon-ok-sign"></i>
														{{ $refund['refundedAt'] }}
													@else
														--
													@endif
												</span>
											</div>
											<div>
												Refunded Reference:
												<span class="pad-l-5 font-14 text-bold">
												@if(isset($refund['refundReference']) && $refund['refundReference'])
														{{ $refund['refundReference'] }}
													@else
														--
													@endif
												</span>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="mar-t-15">
										<div class="col-sm-12 col-md-12 col-lg-12 font-13 no-mar">
											Refund Comments:
											<span class="pad-l-5 font-14 text-bold">
												@if(isset($refund['comments']) && $refund['comments'])
													{!! $refund['comments'] !!}
												@else
													--
												@endif
											</span>
										</div>
									</div>
									@if(isset($refund['cancelledAt']) && $refund['cancelledAt'])
										<div class="mar-t-15">
											<div class="col-sm-4 col-md-4 col-lg-4 font-13 no-mar">
												Booking Cancelled:
												<span class="pad-l-5 font-14 text-bold">
												@if(isset($refund['cancelledAt']) && $refund['cancelledAt'])
														{{ $refund['cancelledAt'] }}
													@else
														--
													@endif
											</span>
											</div>
											<div class="col-sm-5 col-md-5 col-lg-5 font-13 no-mar">
												Booking Cancelled By:
												<span class="pad-l-5 font-14 text-bold">
												@if(isset($refund['cancelledUserDetails']['name']) && $refund['cancelledUserDetails']['name'])
														@if(isset($refund['cancelledUserDetails']['role']) && $refund['cancelledUserDetails']['role'])
															{{ $refund['cancelledUserDetails']['name'] }} ({{ $refund['cancelledUserDetails']['role'] }})
														@else
															{{ $refund['cancelledUserDetails']['name'] }}
														@endif
													@else
														--
													@endif
											</span>
											</div>
											<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
												<div class="finance-status text-red">Cancellation Refund</div>
											</div>
											<div class="clearfix"></div>
										</div>
									@endif
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			@else
				<div class="text-danger">No customer refunds are found!</div>
			@endif
		</div>
		<div class="clearfix"></div>
	@endif

	@include("finance.customer-refund-modals")

@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			(function initViews() {
				$('#modalAddCustomerRefund,' +
					' #modalEditCustomerRefund,' +
					' #modalRejectCustomerRefund,' +
					' #modalApproveCustomerRefund').modal({
					show: false,
					keyboard: false,
					backdrop: 'static'
				});

				$('.cr-ticket-booking-id').selectize({
					render: {
						option: function (data, escape) {
							return "<div data-advance-amount='" + data.advanceAmount + "' data-booking-amount='" + data.bookingAmount + "'>" + data.text + "</div>"
						}
					},
					onChange: function (val) {
						var data = this.options[val];

						if ((typeof(data.bookingAmount) !== "undefined") && data.bookingAmount) {
							$('.cr-booking-amount').text('Rs.' + data.bookingAmount);
						}
						else {
							$('.cr-booking-amount').text('');
						}

						if ((typeof(data.advanceAmount) !== "undefined") && data.advanceAmount) {
							$('.cr-advance-amount').text('Rs.' + data.advanceAmount);
						}
						else {
							$('.cr-advance-amount').text('');
						}
					}
				});

			})();

			$('#modalAddCustomerRefund').on('shown.bs.modal', function (e) {
				$('#modalAddCustomerRefund .error-msg').addClass('hide');
			});

			$('.btn-cr-submit').click(function (event) {
				event.preventDefault();

				$('#modalAddCustomerRefund .error-msg').addClass('hide');

				if(!confirm("Are you sure?\n\nThis is a partial refund to the customer.\nIncluding partner bear or evibe bear may vary settlement amount.\nIf full refund is to be created, kindly ensure that respective booking is cancelled and automatically created refund is not processed.")) {
					event.preventDefault();
					return false;
				}

				$.ajax({
					url: $(this).data('url'),
					type: 'POST',
					dataType: 'json',
					data: {
						ticketBookingId: $('.cr-ticket-booking-id').val(),
						refundAmount: $('.cr-refund-amount').val(),
						evibeBear: $('.cr-evibe-bear').val(),
						partnerBear: $('.cr-partner-bear').val(),
						refundComments: $('.cr-comments').val()
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							$('#modalAddCustomerRefund').modal('hide');
							var $success = "Customer refund created successfully";
							if (data.successMsg) {
								$success = data.successMsg;
							}
							window.showNotySuccess($success);
							setTimeout(function () {
								location.reload();
							}, 2000);
						}
						else {
							var $error = 'Some error occurred while creating refund';
							if (data.error) {
								$error = data.error;
							}
							$('#modalAddCustomerRefund .error-msg').text($error);
							$('#modalAddCustomerRefund .error-msg').removeClass('hide');
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

			$('.btn-edit-refund').click(function (event) {
				event.preventDefault();

				$('.cr-edit-booking-id').text($(this).data('booking-id'));
				$('.cr-edit-customer-name').text($(this).data('customer-name'));
				$('.cr-edit-party-date').text($(this).data('party-date'));
				$('.cr-edit-party-location').text($(this).data('party-location'));
				$('.cr-edit-booking-amount').text('Rs. ' + $(this).data('booking-amount'));
				$('.cr-edit-advance-amount').text('Rs. ' + $(this).data('advance-amount'));
				$('.cr-edit-refund-amount').val($(this).data('refund-amount'));
				$('.cr-edit-evibe-bear').val($(this).data('evibe-bear'));
				$('.cr-edit-partner-bear').val($(this).data('partner-bear'));
				$('#hidCrEditTicketBookingId').val($(this).data('ticket-booking-id'));
			});

			$('#modalEditCustomerRefund').on('shown.bs.modal', function (e) {
				$('#modalEditCustomerRefund .error-msg').addClass('hide');
			});

			$('.btn-cr-update').click(function (event) {
				event.preventDefault();

				$('#modalEditCustomerRefund .error-msg').addClass('hide');

				$.ajax({
					url: $(this).data('url'),
					type: 'PUT',
					dataType: 'json',
					data: {
						ticketBookingId: $('#hidCrEditTicketBookingId').val(),
						refundAmount: $('.cr-edit-refund-amount').val(),
						evibeBear: $('.cr-edit-evibe-bear').val(),
						partnerBear: $('.cr-edit-partner-bear').val()
						//						refundComments: $('.cr-comments').val()
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							$('#modalEditCustomerRefund').modal('hide');
							var $success = "Customer refund edited successfully";
							if (data.successMsg) {
								$success = data.successMsg;
							}
							window.showNotySuccess($success);
							setTimeout(function () {
								location.reload();
							}, 2000);
						}
						else {
							var $error = 'Some error occurred while editing refund';
							if (data.error) {
								$error = data.error;
							}
							$('#modalEditCustomerRefund .error-msg').text($error);
							$('#modalEditCustomerRefund .error-msg').removeClass('hide');
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

			$('.btn-reject-refund').click(function (event) {
				event.preventDefault();

				$('.cr-reject-booking-id').text($(this).data('booking-id'));
				$('.cr-reject-customer-name').text($(this).data('customer-name'));
				$('.cr-reject-party-date').text($(this).data('party-date'));
				$('.cr-reject-party-location').text($(this).data('party-location'));
				$('#hidCrRejectTicketBookingId').val($(this).data('ticket-booking-id'));

			});

			$('.btn-cr-reject').click(function (event) {
				event.preventDefault();

				$('#modalRejectCustomerRefund .error-msg').addClass('hide');

				$.ajax({
					url: $(this).data('url'),
					type: 'PUT',
					dataType: 'json',
					data: {
						ticketBookingId: $('#hidCrRejectTicketBookingId').val(),
						rejectionComments: $('.cr-reject-comments').val()
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							window.showNotySuccess("Customer refund rejected successfully");
							setTimeout(function () {
								location.reload();
							}, 2000);
						}
						else {
							var $error = 'Some error occurred while rejecting refund';
							if (data.error) {
								$error = data.error;
							}
							$('#modalRejectCustomerRefund .error-msg').text($error);
							$('#modalRejectCustomerRefund .error-msg').removeClass('hide');
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

			$('.btn-approve-refund').click(function (event) {
				event.preventDefault();

				$('.cr-approve-booking-id').text($(this).data('booking-id'));
				$('.cr-approve-customer-name').text($(this).data('customer-name'));
				$('.cr-approve-party-date').text($(this).data('party-date'));
				$('.cr-approve-party-location').text($(this).data('party-location'));
				$('.cr-approve-booking-amount').text('Rs. ' + $(this).data('booking-amount'));
				$('.cr-approve-advance-amount').text('Rs. ' + $(this).data('advance-amount'));
				$('.cr-approve-refund-amount').text('Rs. ' + $(this).data('refund-amount'));
				$('.cr-approve-evibe-bear').text('Rs. ' + $(this).data('evibe-bear'));
				$('.cr-approve-partner-bear').text('Rs. ' + $(this).data('partner-bear'));
				$('#hidCrApproveTicketBookingId').val($(this).data('ticket-booking-id'));
			});

			$('.btn-cr-approve').click(function (event) {
				event.preventDefault();

				$.ajax({
					url: $(this).data('url'),
					type: 'PUT',
					dataType: 'json',
					data: {
						ticketBookingId: $('#hidCrApproveTicketBookingId').val(),
						refundReference: $('.cr-approve-refund-reference').val()
					},
					success: function (data) {
						if (data.success && (data.success === true)) {

							var successMsg = "Customer refund approved successfully";
							if (data.successMsg) {
								successMsg = data.successMsg;
							}
							window.showNotySuccess(successMsg);

							setTimeout(function () {
								location.reload();
							}, 2000);
						}
						else {
							var $error = 'Some error occurred while approving reference';
							if (data.error) {
								$error = data.error;
							}
							$('#modalRejectCustomerRefund .error-msg').text($error);
							$('#modalRejectCustomerRefund .error-msg').removeClass('hide');
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

			$('.refund-date').datetimepicker({
				step: 30,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				maxDate: false,
				timepicker: false,
				format: 'Y-m-d'
			});

			$('.btn-clear-filter').click(function (event) {
				event.preventDefault();

				window.location.href = window.location.href.split('?')[0];
			});

			$('#filterRefunds').click(function (event) {
				event.preventDefault();

				var $currentUrl = window.location.href;
				$currentUrl = $currentUrl.split('?')[0];

				var minRefundDate = $('#minRefundDate').val();
				var maxRefundDate = $('#maxRefundDate').val();
				var search = $('#refundSearch').val();

				$currentUrl += '?';

				if (minRefundDate) {
					$currentUrl += 'minRefundDate=' + minRefundDate + '&';
				}

				if (maxRefundDate) {
					$currentUrl += 'maxRefundDate=' + maxRefundDate + '&';
				}

				if (search) {
					$currentUrl += 'search=' + search + '&';
				}

				window.location.href = $currentUrl;

			});
		});
	</script>
@endsection