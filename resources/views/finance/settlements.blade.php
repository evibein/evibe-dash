@extends("finance.base")

@section("settlement-data")

	@if(isset($error) && $error)
		<div class="text-danger">{!! $error !!}</div>
	@else
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center hide">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 text-left no-pad-l">
				<div class="list-header">
					Settlements
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-b-20 mar-b-10 text-center">
			<div class="list-top-filters">
				<a href="{{ route('finance.settlement.list', 'created') }}"
						class="@if(isset($data['query']) && ($data['query'] == 'created')) active @endif">Created</a>
				<a href="{{ route('finance.settlement.list', 'settled') }}"
						class="@if(isset($data['query']) && ($data['query'] == 'settled')) active @endif">Settled</a>
				<a href="{{ route('finance.settlement.list', 'pending') }}"
						class="@if(isset($data['query']) && ($data['query'] == 'pending')) active @endif">Pending</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="text-center">
				@if(isset($data['query']) && ($data['query'] == 'created'))
					<form action="{{ route('finance.settlement.download', 'created') }}" method="post">
						<button type="submit" class="btn btn-success mar-b-20" id="downCreatedSettlements"
								data-url="{{ route('finance.settlement.download', 'created') }}">Download Created Settlements Sheet
						</button>
					</form>
					<form action="{{ route('finance.settlement.download', 'created-bank') }}" method="post">
						<button type="submit" class="btn btn-primary mar-b-20" id="downSettlementsBankSheet"
								data-url="{{ route('finance.settlement.download', 'created-bank') }}">Download Settlement Bank Sheet
						</button>
					</form>
					<form action="{{ route('finance.settlement.download', 'deviations') }}" method="post">
						<button type="submit" class="btn btn-primary mar-b-20" id="downCreatedSettlementsDeviations"
								data-url="{{ route('finance.settlement.download', 'deviations') }}">Download Settlement Deviations Sheet
						</button>
					</form>
				@endif
				@if(isset($data['query']) && ($data['query'] == 'settled'))
					<button class="btn btn-success mar-b-20"
							id="downSettledSettlements">Download Settled Settlements Sheet
					</button>
				@endif
				@if(isset($data['query']) && ($data['query'] == 'pending'))
					<form action="{{ route('finance.settlement.download', 'pending') }}" method="post">
						<button type="submit" class="btn btn-success mar-b-20" id="downPendingSettlements"
								data-url="{{ route('finance.settlement.download', 'pending') }}">Download Pending Settlements Sheet
						</button>
					</form>
				@endif
				<form action="{{ route('finance.settlement.commission') }}" method="post">
					<button type="submit" class="btn btn-normal mar-b-20" id="viewSettlementCommission">
						View settlement service fee
					</button>
				</form>
			</div>
			<div class="list-side-filters pad-10">
				<div class="list-filter-title mar-t-10">
					Filters
					@if(isset($data['filters']['clearFilter']) && $data['filters']['clearFilter'])
						<span class="btn btn-xs btn-warning pull-right btn-clear-filter">Clear all filters</span>
					@endif
					<div class="clearfix"></div>
				</div>
				<hr class="list-filter-hr">
				<div class="list-filter-wrap">
					<div class="list-filter-sub-title mar-b-10">
						Select Partner:
					</div>
					<div class="list-filter-sub-body">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<label>
								<input type="radio" name="typePartner"
										value="{{ config('evibe.ticket_type.planners') }}"
										@if(isset($data['filters']['partnerTypeId']) && $data['filters']['partnerTypeId'] == config('evibe.ticket_type.planners')) checked="checked" @endif>
								Planner
							</label>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<label>
								<input type="radio" name="typePartner" value="{{ config('evibe.ticket_type.venues') }}"
										@if(isset($data['filters']['partnerTypeId']) && $data['filters']['partnerTypeId'] == config('evibe.ticket_type.venues')) checked="checked" @endif>
								Venue
							</label>
						</div>
						<div class="clearfix"></div>
						<select class="partner-select planner-select full-width" id="plannerSelect">
							<option value="0">-- Select a planner --</option>
							@if(isset($data['plannerPartnersList']) && count($data['plannerPartnersList']))
								@foreach($data['plannerPartnersList'] as $plannerPartner)
									<option value="{{ $plannerPartner['id'] }}"
											@if(isset($data['filters']['partnerId']) && $data['filters']['partnerId'] == $plannerPartner['id']) selected @endif>{{ $plannerPartner['name'] }} - {{ $plannerPartner['email'] }}</option>
								@endforeach
							@endif
						</select>
						<select class="partner-select venue-select full-width" id="venueSelect">
							<option value="0">-- Select a venue --</option>
							@if(isset($data['venuePartnersList']) && count($data['venuePartnersList']))
								@foreach($data['venuePartnersList'] as $venuePartner)
									<option value="{{ $venuePartner['id'] }}"
											@if(isset($data['filters']['partnerId']) && $data['filters']['partnerId'] == $venuePartner['id']) selected @endif>{{ $venuePartner['name'] }} - {{ $venuePartner['email'] }}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
				<hr class="list-filter-hr hide">
				<div class="list-filter-wrap hide">
					<div class="list-filter-sub-title mar-b-10">
						Settlement Created Date:
					</div>
					<div class="list-filter-sub-body">
						<input type="text" class="settlement-date form-control" id="minSettlementCreateDate"
								@if(isset($data['filters']['minSettlementCreateDate']) && $data['filters']['minSettlementCreateDate'])
								value="{{ $data['filters']['minSettlementCreateDate'] }}" @endif>
						<div class="text-center mar-t-5 mar-b-5 font-12">TO</div>
						<input type="text" class="settlement-date form-control" id="maxSettlementCreateDate"
								@if(isset($data['filters']['maxSettlementCreateDate']) && $data['filters']['maxSettlementCreateDate'])
								value="{{ $data['filters']['maxSettlementCreateDate'] }}" @endif>
					</div>
				</div>
				@if(isset($data['query']) && ($data['query'] == 'settled'))
					<hr class="list-filter-hr">
					<div class="list-filter-wrap">
						<div class="list-filter-sub-title mar-b-10">
							Settlement Done Date:
						</div>
						<div class="list-filter-sub-body">
							<input type="text" class="settlement-date form-control" id="minSettlementProcessDate"
									@if(isset($data['filters']['minSettlementProcessDate']) && $data['filters']['minSettlementProcessDate'])
									value="{{ $data['filters']['minSettlementProcessDate'] }}" @endif>
							<div class="text-center mar-t-5 mar-b-5 font-12">TO</div>
							<input type="text" class="settlement-date form-control" id="maxSettlementProcessDate"
									@if(isset($data['filters']['maxSettlementProcessDate']) && $data['filters']['maxSettlementProcessDate'])
									value="{{ $data['filters']['maxSettlementProcessDate'] }}" @endif>
						</div>
					</div>
				@endif
				<div class="list-filter-cta text-center mar-t-15 mar-b-5">
					<div class="btn btn-primary" id="filterSettlements">Filter</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<div class="finance-list-info">
				<i class="glyphicon glyphicon-info-sign"></i>
				<span>
					@if(isset($data['query']) && ($data['query'] == 'created'))
						List of all the settlements that have been created, but not processed. Settlements, if not processed after two days of creation, will be moved to pending settlements tab.
					@elseif(isset($data['query']) && ($data['query'] == 'settled'))
						List of all the settlements that have been settled with partners.
					@elseif(isset($data['query']) && ($data['query'] == 'pending'))
						List of all the settlements that have been pending and yet to be settled.
					@endif
				</span>
			</div>
			@if(isset($data['settlementsList']) && count($data['settlementsList']))
				<div class="settlements-list-wrap">
					<div class="mar-b-15">
						<div class="in-blk">
							<button class="btn btn-default" id="selectAllSettlements">
								<span class="glyphicon glyphicon-ok-sign"></span>Select All
							</button>
						</div>
						<div class="in-blk pull-right">
							@if(isset($data['query']) && ($data['query'] == 'created'))
								<button data-url="" class="btn btn-settle-up btn-primary pull-right text-center mar-r-5">
									Settle Up
								</button>
							@else
								<div class="mar-t-5 text-italic text-grey">
									No bulk functionality available
								</div>
							@endif
						</div>
					</div>
					<ul class="tickets-list ls-none">
						@foreach($data['settlementsList'] as $settlement)
							<li class="ticket-wrap">
								<div class="top-sec">

									<div class="col-sm-1 col-md-1 col-lg-1 text-center pad-t-10">
										<input type="checkbox" class="select-settlement" data-amount="{{$settlement['settlementAmount']}}" data-id="{{$settlement['id']}}" checked="">
									</div>

									<div class="col-sm-4 col-md-4 col-lg-4">
										<div class="sub-sec-title">Partner Name</div>
										<div class="sub-sec-value">
											<i class="glyphicon glyphicon-user"></i>
											{{ $settlement['partnerDetails']['name'] }} ({{ $settlement['partnerDetails']['partnerType'] }})
											@if(isset($settlement['partnerDetails']['dashLink']) && $settlement['partnerDetails']['dashLink'])
												<form target="_blank" action="{{ $settlement['partnerDetails']['dashLink'] }}">
													<input type="submit" class="btn btn-link btn-xs no-pad" value="Partner Info"/>
												</form>
											@endif
										</div>
									</div>
									<div class="col-sm-4 col-md-4 col-lg-4">
										<div class="sub-sec-title">Partner Email</div>
										<div class="sub-sec-value">
											<i class="glyphicon glyphicon-envelope"></i>
											{{ $settlement['partnerDetails']['email'] }}
										</div>
									</div>
									<div class="col-sm-3 col-md-3 col-lg-3">
										<div class="sub-sec-title">Settlement Amount</div>
										<div class="sub-sec-value">
											@price( $settlement['settlementAmount'] )
										</div>
									</div>

									<div class="col-sm-4 col-md-4 col-lg-4 mar-t-5"></div>
									<div class="col-sm-4 col-md-4 col-lg-4 mar-t-5">
										<div class="sub-sec-title">Settlement done at</div>
										<div class="sub-sec-value">
											@if(isset($settlement['settlementDoneAt']) && $settlement['settlementDoneAt'])
												<span class="text-green">
														<i class="glyphicon glyphicon-ok-sign"></i>
														{{ $settlement['settlementDoneAt'] }}
													</span>
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-sm-3 col-md-3 col-lg-3 mar-t-5">
										<a href="{{ route('finance.settlement.info', $settlement['id']) }}" class="btn-link"><span class="glyphicon glyphicon-eye-open"></span> View Settlement</a>
									</div>

									<div class="clearfix"></div>

								</div>
								@if((isset($settlement['settlementReference']) && $settlement['settlementReference']) ||
								 (isset($settlement['invoiceNumber']) && $settlement['invoiceNumber']))
									<div class="mid-sec no-mar pad-b-10">
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											Settlement Reference:
											<span class="font-14 text-bold">
											@if(isset($settlement['settlementReference']) && $settlement['settlementReference'])
													{{ $settlement['settlementReference'] }}
												@else
													--
												@endif
										</span>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											Invoice Number:
											<span class="font-14 text-bold">
											@if(isset($settlement['invoiceNumber']) && $settlement['invoiceNumber'])
													{{ $settlement['invoiceNumber'] }}
												@else
													--
												@endif
										</span>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-3 font-13 no-mar">
											@if(isset($settlement['invoiceUrl']) && $settlement['invoiceUrl'])
												<a href="{{ config('evibe.gallery.host').'/invoices/'.$settlement['invoiceUrl'] }}"
														target="_blank" download>
													Download Invoice
												</a>
											@else
												--
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
								@else
									<div class="mar-t-15-n"></div>
								@endif
							</li>
						@endforeach
					</ul>
				</div>
			@else
				<div class="text-danger">No settlements are found!</div>
			@endif
		</div>
		<div class="clearfix"></div>

	@endif

	<!-- Modal to select settled settlements date -->
	<div id="modalSettlementsMonth" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Download Settled Settlement</h4>
				</div>
				<div class="pad-l-20 pad-r-20 pad-t-20">
					<form class="form form-horizontal" role="form" id="downSettlementForm"
							action="{{ route('finance.settlement.download', 'settled') }}" method="POST">
						<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
						<div class="form-group">
							<label class="col-sm-3">Settlement Month</label>
							<div class="col-sm-9">
								<input type="text" id="settlementMonth" name="settlementMonth" class="settlement-month form-control" id="">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger btn-down-settlement"
							form="downSettlementForm" disabled>Submit
					</button>
				</div>
			</div>
			<div class="finance-loader-wrap hide">
				<div class="loader-animation-wrap">
					<img class="loader-animation" src="{{ config('evibe.gallery.host').'/img/app/load_spinner.gif' }}"
							alt="">
				</div>
			</div>
		</div>
	</div>
	<!-- Modal to capture payment reference for settlement -->
	<div id="modalProcessSettlement" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Process Settlements</h4>
				</div>
				<div class="pad-l-20 pad-r-20 pad-t-20">
					<form class="form form-horizontal" role="form">
						<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
						<div class="form-group">
							<label class="col-sm-4">No. Of Settlements Selected</label>
							<div class="col-sm-8">
								<span id="settlementsCount"></span>
							</div>
							<div class="clearfix"></div>
							<label class="col-sm-4 pad-t-15">Total Settlement Amount</label>
							<div class="col-sm-8 pad-t-15">
								<span id="settlement-amount">
								</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4">Settlement Payment Reference</label>
							<div class="col-sm-8">
								<input type="text" class="settlement-reference form-control"
										id="settlementReference" value="default_payment_reference" placeholder="Settlement transfer reference"/>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary btn-submit-settlement mar-l-5" data-url="{{ route('finance.settlement.multi-settle-up') }}">Submit</button>
				</div>
			</div>
			<div class="finance-loader-wrap hide">
				<div class="loader-animation-wrap">
					<img class="loader-animation" src="{{ config('evibe.gallery.host').'/img/app/load_spinner.gif' }}" alt="">
				</div>
			</div>
		</div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var totalAmount = 0;
			var totalSettlementsSelected = 0;
			var selectedIdsForSettlements = "";

			$('.settlements-list-wrap input[type="checkbox"]').prop('checked', false);
			$("#selectAllSettlements").text("Select All").removeClass("selected").prepend("<span class='glyphicon glyphicon-ok-sign mar-r-5'></span>");
			$('#selectAllSettlements').on("click", function () {
				if ($(this).hasClass("selected")) {
					$('.settlements-list-wrap input[type="checkbox"]').prop('checked', false);
					$("#selectAllSettlements").text("Select All").removeClass("selected").prepend("<span class='glyphicon glyphicon-ok-sign mar-r-5'></span>");
				} else {
					$('.settlements-list-wrap input[type="checkbox"]').prop('checked', true);
					$("#selectAllSettlements").text("Unselect All").addClass("selected").prepend("<span class='glyphicon  glyphicon glyphicon-remove mar-r-5'></span>");
				}
			});

			$(".btn-settle-up").on("click", function () {
				$('.settlements-list-wrap').find(".select-settlement").each(function () {
					if ($(this).prop("checked")) {
						selectedIdsForSettlements = selectedIdsForSettlements + "#" + $(this).data('id');
						totalSettlementsSelected++;
						totalAmount += parseInt($(this).data('amount'));
					}
				});

				$("#settlement-amount").text(totalAmount);
				$("#settlementsCount").text(totalSettlementsSelected);

				$('#modalProcessSettlement .error-msg').addClass('hide');
				$('#modalProcessSettlement').modal('show');
			});

			$(".btn-submit-settlement").on("click", function () {
				$('.finance-loader-wrap').removeClass('hide');
				$('.btn-submit-settlement').addClass('hide');

				$('#modalProcessSettlement .error-msg').addClass('hide');

				if (totalSettlementsSelected <= 0) {
					$('#modalProcessSettlement .error-msg').text("Kindly select at least one settlement to settle up");
					$('#modalProcessSettlement .error-msg').removeClass('hide');

					$('.finance-loader-wrap').addClass('hide');
					$('.btn-submit-settlement').removeClass('hide');

					return false;
				}

				$.ajax({
					url: $(this).data('url'),
					type: 'PUT',
					method: 'POST',
					dataType: 'json',
					data: {
						settlementReference: $(".settlement-reference").val(),
						selectedIds: selectedIdsForSettlements
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							$('.finance-loader-wrap').addClass('hide');
							if (totalSettlementsSelected == 1) {
								window.showNotySuccess("Settlement has been processed successfully");
							} else {
								window.showNotySuccess("Settlements have been processed successfully");
							}
							setTimeout(function () {
								location.reload();
							}, 2000);
						} else {
							var $error = 'Some error occurred while processing settlement';
							if (data.error) {
								$error = data.error;
							}
							$('.finance-loader-wrap').addClass('hide');
							$('.btn-submit-settlement').show();

							$('#modalProcessSettlement .error-msg').text($error);
							$('#modalProcessSettlement .error-msg').removeClass('hide');
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});

			});

			var $plannerType = $('#hidPlannerType').val();
			var $venueType = $('#hidVenueType').val();

			$('#modalSettlementsMonth').modal({
				show: false,
				keyboard: false,
				backdrop: 'static'
			});

			$('.settlement-date').datetimepicker({
				step: 30,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				maxDate: false,
				timepicker: false,
				format: 'Y-m-d'
			});

			$('.settlement-month').datepicker({
				changeMonth: true,
				changeYear: true,
				showButtonPanel: true,
				dateFormat: 'MM yy',
				onClose: function (dateText, inst) {
					$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
					if ($(this).val() !== '') {
						$('.btn-down-settlement').prop('disabled', false);
					}
				}
			});

			$('#downSettledSettlements').on('click', function (event) {
				event.preventDefault();

				$('#modalSettlementsMonth').modal('show');
			});

			$('.btn-clear-filter').click(function (event) {
				event.preventDefault();

				window.location.href = window.location.href.split('?')[0];
			});

			$('#filterSettlements').click(function (event) {
				event.preventDefault();

				var $currentUrl = window.location.href;
				$currentUrl = $currentUrl.split('?')[0];

				var minSettlementCreateDate = $('#minSettlementCreateDate').val();
				var maxSettlementCreateDate = $('#maxSettlementCreateDate').val();
				var minSettlementProcessDate = $('#minSettlementProcessDate').val();
				var maxSettlementProcessDate = $('#maxSettlementProcessDate').val();

				var $partnerId = null;
				var $partnerTypeId = $("input[type='radio'][name='typePartner']:checked").val();
				if ($partnerTypeId === $plannerType) {
					$partnerId = $('#plannerSelect').val();
				} else if ($partnerTypeId === $venueType) {
					$partnerId = $('#venueSelect').val();
				}

				if (typeof $partnerTypeId == 'undefined') {
					$partnerTypeId = null;
				}

				if ($partnerId == 0) {
					$partnerId = null;
				}

				$currentUrl += '?';

				if ($partnerTypeId) {
					$currentUrl += 'partnerTypeId=' + $partnerTypeId + '&';
				}

				if ($partnerId) {
					$currentUrl += 'partnerId=' + $partnerId + '&';
				}

				if (minSettlementCreateDate) {
					$currentUrl += 'minSettlementCreateDate=' + minSettlementCreateDate + '&';
				}

				if (maxSettlementCreateDate) {
					$currentUrl += 'maxSettlementCreateDate=' + maxSettlementCreateDate + '&';
				}

				if (minSettlementProcessDate) {
					$currentUrl += 'minSettlementProcessDate=' + minSettlementProcessDate + '&';
				}

				if (maxSettlementProcessDate) {
					$currentUrl += 'maxSettlementProcessDate=' + maxSettlementProcessDate + '&';
				}

				window.location.href = $currentUrl;
			});
		});
	</script>
@endsection