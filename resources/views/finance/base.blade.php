@extends("layout.base")

@section("content")

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 no-pad">
			<a href="{{ route('finance.customer-refund.get') }}"
					class="btn btn-default mar-l-20 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.finance.customer-refunds'))) btn-warning @endif">
				Customer Refunds</a>
			<a href="{{ route('finance.partner-adjustment.get') }}"
					class="btn btn-default mar-l-20 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.finance.partner-adjustments'))) btn-warning @endif">
				Partner Adjustments</a>
			<a href="{{ route('finance.quick-settlements.get') }}"
					class="btn btn-default mar-l-10 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.finance.quick-settlements'))) btn-warning @endif">
				Quick Settlements</a>
			<a href="{{ route('finance.settlement.get') }}"
					class="btn btn-default mar-l-10 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.finance.settlements'))) btn-warning @endif">
				Settlements</a>
			<a href="{{ route('settlement.requirement') }}" class="mar-l-20" target="_blank">Old settlements page</a>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="pull-right">
				<button class="btn btn-link" data-toggle="modal" data-target="#modalShowFinanceInfo">
					<i class="glyphicon glyphicon-info-sign"></i> Help
				</button>
				@if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.finance.settlements')))
					<button class="btn btn-primary"  data-toggle="modal" data-target="#settlementDateModal">
						<span class="glyphicon glyphicon-plus"></span> Create Settlement
					</button>
				@endif
				@if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.finance.partner-adjustments')))
					<button class="btn btn-primary" id="addAdjustment" data-toggle="modal"
							data-target="#modalAddPartnerAdjustment">
						<span class="glyphicon glyphicon-plus"></span> Add Adjustment
					</button>
				@endif
				@if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.finance.customer-refunds')))
					<button class="btn btn-primary" id="addRefund" data-toggle="modal"
							data-target="#modalAddCustomerRefund">
						<span class="glyphicon glyphicon-plus"></span> Add Refund
					</button>
				@endif
			</div>
			<input type="hidden" id="hidPlannerType" value="{{ config('evibe.ticket_type.planners') }}">
			<input type="hidden" id="hidVenueType" value="{{ config('evibe.ticket_type.venues') }}">
		</div>
		<div class="clearfix"></div>
		<hr>
		@yield('settlement-data')
	</div>

	@include("finance.how-it-works")
	@include("finance.settlementModal");
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			$('.filter-date').datetimepicker({
				timepicker: false,
				scrollInput: false,
				scrollMonth: false,
				closeOnDateSelect: true,
				format: "Y-m-d",
				disabledWeekDays: [1,2,4,5,6,0]
			});
			var $plannerType = $('#hidPlannerType').val();
			var $venueType = $('#hidVenueType').val();

			(function initViews() {
				$('.ticket-booking-id').selectize();

				$('.planner-select').selectize();
				$('.venue-select').selectize();
				partnerSelect();
			})();

			function partnerSelect() {
				// because, 'id' is not being transferred to selectize div
				$('.planner-select').addClass('hide');
				$('.venue-select').addClass('hide');
				$selectedPartnerType = $("input[type='radio'][name='typePartner']:checked").val();
				if ($selectedPartnerType === $plannerType) {
					$('.planner-select').removeClass('hide');
				}
				else if ($selectedPartnerType === $venueType) {
					$('.venue-select').removeClass('hide');
				}
			}

			$("input[type='radio'][name='typePartner']").change(function () {
				partnerSelect();
			});

			$('#createSettlement').on('click', function (event) {
				event.preventDefault();
				var selectedDate = $("#customSettlementDate").val();
				// $confirmation = confirm("Are you sure you want to create settlements?");

				if(!(selectedDate.length)) {
					$("#errorMessage").text("Date field shouldn't be empty").removeClass("hide");
					return false;
				}
				$("#errorMessage").addClass("hide");
				$(this).text("We are on it...Please Wait").attr("disabled","true");
				
				$.ajax({
					url: $(this).data('url'),
					type: 'POST',
					dataType: 'json',
					data:{
						'customSelectedDate' : selectedDate 
					},
					success: function (data) {
						if (data.success && (data.success === true)) {
							var $success = "New settlements have been created.";
							if (data.successMsg) {
								$success = data.successMsg;
							}
							window.showNotySuccess($success);
							setTimeout(function () {
								location.reload();
							}, 2000);
							$("#settlementDateModal").modal('hide');
						}
						else {
							var $error = 'Some error occurred while creating settlements';
							if (data.error) {
								$error = data.error;
							}
							window.showNotyError($error);
							$('#createSettlement').text("Create Settlement").attr("disabled","false");
						}
					},
					fail: function () {
						window.showNotyError();
					}
				});
			});

		});
	</script>
@endsection