<!-- Modal to show finance info -->
<div id="modalShowFinanceInfo" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Finance Info</h4>
			</div>
			<div class="pad-l-20 pad-r-20 pad-t-20">
				<div class="finance-info-text">
					<div>
						This finance console will give a clear overview of how the amount paid by the customer is divided and channelised, and also the functionalities to tackle cases like refunds, cancellations, partner adjustments etc., while settling a partner.
					</div>
					<div class="mar-t-10">
						Console is divided into 4 sub consoles:
						<ul>
							<li>Settlements</li>
							<li>Customer Refunds</li>
							<li>Partner Adjustments</li>
							<li>Downloads</li>
						</ul>
					</div>
					<hr>
					<div>
						<span class="text-bold">Settlements</span>
						<div class="finance-info-sub-text">
							<ul>
								<li>Settlements console consists of all the list of settlements that needs to be processed or all the settled settlements.</li>
								<li>On every Wednesday / Thursday 10:00 AM, a settlement will be automatically created, which consists of all the ticket booking for which, partner amount should be settled from previous week Monday to previous week Saturday.</li>
								<li>Account Team / concerned executive should tally the booking and adjustment included in a settlement, and then process them.</li>
								<li>
									<b>To process:</b> Kindly make the transfer to the partner and then enter the payment reference in the pop up modal which comes after pressing “Settle Up” button.
								</li>
								<li>By clicking on any settlement, you will be able to view all the included ticket bookings and partner adjustments in that settlement.</li>
							</ul>
						</div>
					</div>
					<div class="mar-t-15">
						<span class="text-bold">Customer Refunds</span>
						<div class="finance-info-sub-text">
							<ul>
								<li>This console gives us the ability to add, approve, reject and edit a refund.</li>
								<li>
									A refund can be initiated in 2 ways:
									<ul>
										<li>Whenever a ticket booking gets cancelled - either by us or by the customer.</li>
										<li>If any post party issues requires refund to a customer.</li>
									</ul>
								</li>
								<li>Creation of refund is independent on any process.</li>
								<li>
									Processing refund may trigger any of two processed:
									<ul>
										<li>If the respective partner bear in not included in any settlement, the data will be included in ticket booking itself.</li>
										<li>If a settlement is already created with the same ticket booking, the settlement cannot be changed, partner bear will be included in adjustment, which will be included in the next settlement.</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
					<div class="mar-t-15">
						<span class="text-bold">Partner Adjustments</span>
						<div class="finance-info-sub-text">
							<ul>
								<li>Partner Adjustment is a way to include any amount that needs to be settled to a partner or to be collected by us from a partner which deviates from normal settlement flow like last minute refunds, non-booking related partner transactions, partner cancellation charges, etc,.</li>
								<li>A partner adjustment can be created irrespective of any ticket booking, and the adjustment can also be ‘partner paying us’.</li>
								<li>An adjustment will also be include in a settlement on every Wednesday / Thursday 10:00 AM which will later be processed.</li>
								<li>Once included in a settlement, anything cannot be done to that adjustment.</li>
							</ul>
						</div>
					</div>
					<div class="mar-t-15">
						<span class="text-bold">Downloads</span>
						<div class="finance-info-sub-text">
							<ul>
								<li>Can be used to download all the settlements in a single sheet in desired time frames.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>