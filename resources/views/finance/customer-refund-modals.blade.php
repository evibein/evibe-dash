<!-- Modal to add customer refund -->
<div id="modalAddCustomerRefund" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Add Customer Refund</h4>
				<div class="finance-list-info mar-t-5">
					<ul>
						<li>Select the ticket booking for which you want to initiate a refund.</li>
						<li>Verify the amount bore by us and partner.</li>
					</ul>
				</div>
			</div>
			<div class="pad-l-20 pad-r-20 pad-t-20">
				<form class="form form-horizontal" role="form">
					<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
					<div class="form-group">
						<label class="col-sm-3">Ticket Booking</label>
						<div class="col-sm-9">
							<select class="cr-ticket-booking-id form-control" id="crTicketBookingId">
								<option value="0">-- Select Ticket Booking --</option>
								@if(isset($data['ticketBookings']) && count($data['ticketBookings']))
									@foreach($data['ticketBookings'] as $ticketBooking)
										<option data-data='{@if(isset($ticketBooking['booking_amount']) && $ticketBooking['booking_amount'])"bookingAmount": "{{ $ticketBooking['booking_amount'] }}", @endif
															@if(isset($ticketBooking['advance_amount']) && $ticketBooking['advance_amount'])"advanceAmount": "{{ $ticketBooking['advance_amount'] }}" @endif}'
												data-booking-amount="@if(isset($ticketBooking['booking_amount']) && $ticketBooking['booking_amount']) {{ $ticketBooking['booking_amount'] }} @endif"
												data-advance-amount="@if(isset($ticketBooking['advance_amount']) && $ticketBooking['advance_amount']) {{ $ticketBooking['advance_amount'] }} @endif"
												value="{{ $ticketBooking['id'] }}">
											@if(isset($ticketBooking['name'])) {{ $ticketBooking['name'] }} @endif -
											@if(isset($ticketBooking['party_date_time']) && ($ticketBooking['party_date_time'] > 0)) {{ date('d/m/Y h:i A', $ticketBooking['party_date_time']) }} @endif -
											@if(isset($ticketBooking['area_name'])) {{ $ticketBooking['area_name'] }} @endif -
											@if(isset($ticketBooking['booking_id'])) {{ $ticketBooking['booking_id'] }} @endif
										</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Booking Amount</label>
						<div class="col-sm-9">
							<span class="cr-booking-amount"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Advance Amount</label>
						<div class="col-sm-9">
							<span class="cr-advance-amount"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Refund Amount</label>
						<div class="col-sm-9">
							<input class="cr-refund-amount form-control" type="text"
									id="crRefundAmount" placeholder="refund amount given to customer">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Evibe Bear</label>
						<div class="col-sm-9">
							<input class="cr-evibe-bear form-control" type="text"
									id="crEvibeBear" placeholder="refund amount bore by us" value="0">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Partner Bear</label>
						<div class="col-sm-9">
							<input class="cr-partner-bear form-control" type="text"
									id="crPartnerBear" placeholder="refund amount bore by partner">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Reasons to Refund</label>
						<div class="col-sm-9">
							<textarea id="crComments" class="cr-comments form-control"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-cr-submit"
						data-url="{{ route("finance.customer-refund.create") }}">Submit
				</button>
			</div>
		</div>
	</div>
</div>


<!-- Modal to edit customer refund -->
<div id="modalEditCustomerRefund" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Customer Refund</h4>
			</div>
			<div class="pad-l-20 pad-r-20 pad-t-20">
				<form class="form form-horizontal" role="form">
					<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
					<div class="form-group">
						<label class="col-sm-3">Ticket Booking</label>
						<div class="col-sm-9">
							<div>
								<span class="cr-edit-customer-name"></span> -
								<span class="cr-edit-party-date"></span>
							</div>
							<div>
								<span class="cr-edit-party-location"></span> -
								<span class="cr-edit-booking-id"></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Booking Amount</label>
						<div class="col-sm-9">
							<span class="cr-edit-booking-amount"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Advance Amount</label>
						<div class="col-sm-9">
							<span class="cr-edit-advance-amount"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Refund Amount</label>
						<div class="col-sm-9">
							<input class="cr-edit-refund-amount form-control" type="text"
									placeholder="refund amount given to customer">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Evibe Bear</label>
						<div class="col-sm-9">
							<input class="cr-edit-evibe-bear form-control" type="text"
									placeholder="refund amount bore by us">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Partner Bear</label>
						<div class="col-sm-9">
							<input class="cr-edit-partner-bear form-control" type="text"
									placeholder="refund amount bore by partner">
						</div>
					</div>
					<div class="form-group hide">
						<label class="col-sm-3">Reasons to Refund</label>
						<div class="col-sm-9">
							<textarea class="cr-edit-comments form-control"></textarea>
						</div>
					</div>
					<input type="hidden" id="hidCrEditTicketBookingId" value=""/>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-cr-update"
						data-url="{{ route("finance.customer-refund.update") }}">Submit
				</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal to reject customer refund -->
<div id="modalRejectCustomerRefund" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Reject Customer Refund</h4>
				<div class="finance-list-info mar-t-5">
					<ul>
						<li>Kindly enter the reasons to reject / delete this refund.</li>
					</ul>
				</div>
			</div>
			<div class="pad-l-20 pad-r-20 pad-t-20">
				<form class="form form-horizontal" role="form">
					<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
					<div class="form-group">
						<label class="col-sm-3">Ticket Booking</label>
						<div class="col-sm-9">
							<div>
								<span class="cr-reject-customer-name"></span> -
								<span class="cr-reject-party-date"></span>
							</div>
							<div>
								<span class="cr-reject-party-location"></span> -
								<span class="cr-reject-booking-id"></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Reasons to Reject</label>
						<div class="col-sm-9">
							<textarea class="cr-reject-comments form-control"></textarea>
						</div>
					</div>
					<input type="hidden" id="hidCrRejectTicketBookingId" value=""/>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-cr-reject"
						data-url="{{ route("finance.customer-refund.reject") }}">Submit
				</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal to approve customer refund -->
<div id="modalApproveCustomerRefund" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Approve Customer Refund</h4>
				<div class="finance-list-info mar-t-5">
					<ul>
						<li>Kindly transfer the refund amount to the customer and then enter the payment reference to approve.</li>
						<li>After approval, the partner bear will either be included in ticket booking, or in any adjustment.</li>
					</ul>
				</div>
			</div>
			<div class="pad-l-20 pad-r-20 pad-t-20">
				<form class="form form-horizontal" role="form">
					<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
					<div class="form-group">
						<label class="col-sm-3">Ticket Booking</label>
						<div class="col-sm-9">
							<div>
								<span class="cr-approve-customer-name"></span> -
								<span class="cr-approve-party-date"></span>
							</div>
							<div>
								<span class="cr-approve-party-location"></span> -
								<span class="cr-approve-booking-id"></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Booking Amount</label>
						<div class="col-sm-9">
							<span class="cr-approve-booking-amount"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Advance Amount</label>
						<div class="col-sm-9">
							<span class="cr-approve-advance-amount"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Refund Amount</label>
						<div class="col-sm-9">
							<span class="cr-approve-refund-amount"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Evibe Bear</label>
						<div class="col-sm-9">
							<span class="cr-approve-evibe-bear"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Partner Bear</label>
						<div class="col-sm-9">
							<span class="cr-approve-partner-bear"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">Refund Reference</label>
						<div class="col-sm-9">
							<input type="text" class="cr-approve-refund-reference form-control"
									placeholder="reference for refund transfer">
						</div>
					</div>
					<div class="form-group hide">
						<label class="col-sm-3">Reasons to Refund</label>
						<div class="col-sm-9">
							<textarea class="cr-approve-comments form-control"></textarea>
						</div>
					</div>
					<input type="hidden" id="hidCrApproveTicketBookingId" value=""/>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-cr-approve"
						data-url="{{ route("finance.customer-refund.approve") }}">Submit
				</button>
			</div>
		</div>
	</div>
</div>