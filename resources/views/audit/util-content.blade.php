<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
	@if(isset($data['options']) && count($data['options']))
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 no-pad">
			<input type="hidden" id="optionAuditColumn" value="{{ $data['auditColumnName'] }}">
			<input type="hidden" id="optionAuditTable" value="{{ $data['auditTableName'] }}">
			<input type="hidden" id="optionTypeId" value="{{ $data['optionTypeId'] }}">
			@foreach($data['options'] as $option)
				<div class="option-audit-wrap" data-id="{{ $option['id'] }}">
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 no-pad-r">
						<div class="option-audit-img-wrap">
							<img class="option-audit-img" src="{{ $option['profilePicUrl'] }}">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<a href="{{ $option['url'] }}" target="_blank">
							<div class="option-audit-title">{{ $option['name'] }}</div>
						</a>
						<div class="option-audit-code mar-t-5">[{{ $option['code'] }}]</div>
						<div class="option-audit-price mar-t-5">
							<span class="rupee-font">&#8377;</span> {{ $option['price'] }}
						</div>
						@if(isset($option['rejectReason']) && $option['rejectReason'])
							<div class="mar-t-10 text-italic font-12">Reject Reason:</div>
							<div class="text-red text-italic font-12">{{ $option['rejectReason'] }}</div>
						@endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad">
						<label>{{ $data['auditColumnName'] }}</label>
						<textarea placeholder="Enter {{ $data['auditColumnName'] }}" class="editor-box form-control editor-html option-audit-column" rows="8" data-id="{{ $option['id'] }}">{{ $option['auditColumn'] }}</textarea>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center">
						@if(isset($data['referenceTab']) && $data['referenceTab'] == config('evibe.reference.audit.review'))
							<div class="btn btn-primary option-audit-cta mar-t-5" data-id="{{ $option['id'] }}" data-audit-id="{{ $option['auditId'] }}"
									data-url="{{ route('audit.review', 'accept') }}">Accept
							</div>
							<div class="btn btn-primary option-audit-cta mar-t-5" data-id="{{ $option['id'] }}" data-audit-id="{{ $option['auditId'] }}"
									data-url="{{ route('audit.review', 'reject') }}">Reject
							</div>
							<div class="">
								<textarea placeholder="Reject reason" class="full-width mar-t-10 option-audit-reject-reason" rows="5" data-audit-id="{{ $option['auditId'] }}"></textarea>
							</div>
						@else
							<div class="btn btn-primary option-audit-cta mar-t-5" data-id="{{ $option['id'] }}" data-audit-id="{{ $option['auditId'] }}"
									@if(isset($data['referenceTab']) && $data['referenceTab'] == config('evibe.reference.audit.edit-new'))
									data-url="{{ route('audit.edit-new') }}"
									@elseif(isset($data['referenceTab']) && $data['referenceTab'] == config('evibe.reference.audit.edit-pending'))
									data-url="{{ route('audit.edit', 'pending') }}"
									@elseif(isset($data['referenceTab']) && $data['referenceTab'] == config('evibe.reference.audit.edit-rejected'))
									data-url="{{ route('audit.edit', 'rejected') }}"
									@else
									disabled="disabled"
									@endif
							>Submit
							</div>
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
			@endforeach
			<div class="text-center">
				{{ $data['options']->appends($data['filters']['queryParams'])->links() }}
			</div>
		</div>
		<div class="clearfix"></div>
	@else
		<div class="text-center font-24">
			@if(isset($data['auditColumnName']) && $data['auditColumnName'] && isset($data['optionTypeId']) && $data['optionTypeId'])
				No options to audit under in this category column.
			@else
				Kindly select a product type and column to edit
			@endif
		</div>
	@endif
</div>
<div class="clearfix"></div>