@extends("layout.base")

@section("content")

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
			<a href="{{ route('audit.edit-new') }}"
					class="btn btn-default mar-l-20 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.audit.edit-new'))) btn-warning @endif">
				Edit New</a>
			<a href="{{ route('audit.edit', 'pending') }}"
					class="btn btn-default mar-l-20 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.audit.edit-pending'))) btn-warning @endif">
				Edit Pending</a>
			<a href="{{ route('audit.edit', 'rejected') }}"
					class="btn btn-default mar-l-20 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.audit.edit-rejected'))) btn-warning @endif">
				Edit Rejected</a>
			@php $user = auth()->user(); @endphp
			@if(in_array($user->role_id, [
			config('evibe.roles.super_admin'),
			config('evibe.roles.admin'),
			config('evibe.roles.ops_tl'),
			config('evibe.roles.bd_tl'),
			]))
				<a href="{{ route('audit.review-show') }}"
						class="btn btn-default mar-l-20 @if(isset($data['referenceTab']) && ($data['referenceTab'] == config('evibe.reference.audit.review'))) btn-warning @endif">
					Review Data</a>
			@endif
		</div>
		<div class="clearfix"></div>
		<hr>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
			<div class="col-sm-3">
				<label for="auditProductType">Product Type</label>
				<select name="auditProductType" id="auditProductType" class="form-control">
					<option value="-1">-- Select --</option>
					@foreach($data['productTypes'] as $productType)
						<option value="{{ $productType->id }}" @if(isset($data['optionTypeId']) && $data['optionTypeId'] == $productType->id) selected @endif>{{ $productType->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-sm-3">
				<label for="auditColumn">Column</label>
				<select name="auditColumn" id="auditColumn" class="form-control">
					@foreach($data['columnNames'] as $columnName)
						<option value="{{ $columnName }}" @if(isset($data['auditColumnName']) && $data['auditColumnName'] == $columnName) selected @endif>{{ $columnName }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-sm-3">
				<a href="#" id="showAuditOptionsBtn" class="btn btn-default mar-l-15 mar-t-25 pad-l-20 pad-r-20 btn-primary">Audit</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<hr>
		@yield("audit-content")
	</div>

@endsection

@section("stylesheets")
	@parent
	<style type="text/css" rel="stylesheet" media="all">

		.option-audit-wrap {
			border: 1px solid #EEEEEE;
			border-radius: 5px;
			padding: 5px;
			margin-bottom: 10px;
		}

		.option-audit-img {
			width: 100%;
		}

	</style>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			$('.editor-html').wysihtml5({
				toolbar: {
					"image": false,
					'html': false,
					'blockquote': false,
					'size': 'xs'
				}
			});

			$('#showAuditOptionsBtn').on('click', function (event) {
				event.preventDefault();

				if ($('#auditProductType').val() <= 0) {
					showNotyError('Kindly select a product type to audit');
					return false;
				}

				/* make request with params */
				var $currentUrl = window.location.href;
				$currentUrl += '?productType=' + $('#auditProductType').val() + '&column=' + $('#auditColumn').val();
				window.location.href = $currentUrl;
			});

			$('.option-audit-cta').on('click', function (event) {
				event.preventDefault();

				var $optionId = $(this).data('id');
				var $auditId = $(this).data('audit-id');
				var $rejectReason = "";

				if ($auditId) {
					$rejectReason = $('.option-audit-reject-reason[data-audit-id=' + $auditId + ']').val();
				}

				var data = {
					'auditId': $auditId,
					'optionId': $optionId,
					'optionTypeId': $('#optionTypeId').val(),
					'tableName': $('#optionAuditTable').val(),
					'columnName': $('#optionAuditColumn').val(),
					'columnData': $('.option-audit-column[data-id=' + $optionId + ']').val(),
					'rejectReason': $rejectReason
				};

				if (!confirm("Are you sure you want to submit?")) {
					return false;
				}

				$.ajax({
					url: $(this).data('url'),
					type: 'POST',
					dataType: 'json',
					data: data
				}).done(function (data, textStatus, jqXHR) {
					if (data.success) {
						showNotySuccess("Successfully submitted for review");
						window.location.reload();
					} else {
						var $errorMsg = "Some error occurred while submitting the data";
						if (data.errorMsg) {
							$errorMsg = data.errorMsg;
						}
						showNotyError($errorMsg);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					showNotyError("Some error occurred while submitting the data");
				});

			});

		});
	</script>
@endsection