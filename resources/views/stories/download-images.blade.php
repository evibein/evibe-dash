@extends('layout.base')

@section('content')
	<div class="container page-content cakes-list-warp">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="title-sec pad-t-10">
					<span class="text-info font-24 text-bold">@if($data)All images of {{$data['selectedCode']}}@else{{"No data found "}}@endif</span>
					<span class="pull-right">
						<a class="btn btn-info btn-sm" href="{{route('download.images.getCodes')}}"><i
									class="glyphicon glyphicon-search"></i> Search other codes</a>
					</span>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-12 col-md-12 col-lg-12 pad-t-30">
					@if($data)
						@foreach($data['imageDetails'] as $imageDetail)
							<li class="in-blk a-decor">
								@if($data['imageType']==config('evibe.ticket_type.decors'))
									<a href="{{$imageDetail->getLink()}}" target="_blank">
										<img alt="Decor Image" src="{{$imageDetail->getLink()}}">
									</a>
								@elseif($data['imageType']==config('evibe.ticket_type.cakes'))
									<a href="{{$imageDetail->getLink()}}" target="_blank">
										<img alt="Cake Image" src="{{$imageDetail->getLink()}}">
									</a>
								@elseif($data['imageType'] == config('evibe.ticket_type.packages'))
									<a href="{{AppUtil::getPackageGalleryUrl($imageDetail)}}"
											target="_blank"><img
												src="{{ AppUtil::getPackageGalleryUrl($imageDetail)}}"
												alt="Package Image">
									</a>
								@elseif($data['imageType'] == config('evibe.ticket_type.trends'))
									<a href="{{ config('evibe.gallery.host') ."/trends/" . $imageDetail['trending_id'] . "/images/" . $imageDetail['url'] }}"
											target="_blank"><img
												src="{{ config('evibe.gallery.host') ."/trends/" . $imageDetail['trending_id'] . "/images/" . $imageDetail['url'] }}"
												alt="Trend Image">
									</a>
								@elseif($data['imageType'] == config('evibe.ticket_type.services'))
									<a href="{{ config('evibe.gallery.host') . '/services/' . $imageDetail['type_service_id'] . '/images/' . $imageDetail['url'] }}"
											target="_blank"><img
												src="{{ config('evibe.gallery.host') . '/services/' . $imageDetail['type_service_id'] . '/images/' . $imageDetail['url'] }}"
												alt="Service Image">
									</a>
								@endif
							</li>
						@endforeach
					@else
						<div class="text-bold font-22 text-danger"><i
									class="glyphicon glyphicon-info-sign"></i> No images found for this product code, you can search other product codes.
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection