@extends('layout.base')

@section('content')
	<div class="container page-content cakes-list-warp">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="title-sec pad-t-10 text-center font-22 text-info  text-bold">
					Search by product code
				</div>
				<div class="col-sm-6 col-lg-6 col-md-6 col-xs-6 pad-t-30 col-sm-offset-2">
					@if($data['searchableProducts'])
						<select class="form-control" id="imageCode" name="imageCode">
							@foreach($data['searchableProducts'] as $key=>$value)
								<option value="{{$value['id']}}">{{$value['code']}}</option>
							@endforeach
						</select>
					@endif
				</div>
				<div class="col-sm-2 col-lg-2 col-md-2 col-xs-2 pad-t-30">
					<button type="submit" class="btn btn-sm btn-info btn-search-images">
						<i class="glyphicon glyphicon-search"></i> Search
					</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			(function initFunctions() {
				$('#imageCode').selectize();
			})();

			$('.btn-search-images').on('click', function () {
				var imageCode = parseInt($('#imageCode').val());

				window.location.replace(window.location.href + "/" + imageCode)
			});
		});
	</script>
@endsection