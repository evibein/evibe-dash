@extends('layout.base')
@section('content')
	@if($decors)
		<div class="heading">
			Decors
		</div>
		<div class="col-lg-12">
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th rowspan="2" class="text-center">S.No</th>
					<th rowspan="2" class="text-center">Option Id</th>
					<th rowspan="2" class="text-center">Option Name</th>
					<th rowspan="1" colspan="3" class="text-center">Old</th>
					<th rowspan="1" colspan="3" class="text-center">New</th>
				</tr>
				<tr>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
				</tr>
				</thead>
				<tbody>
				@php $i = 1; @endphp
				@foreach($decors as $decor)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $decor['optionId'] }}</td>
						<td>{{ ucwords($decor['optionName']) }}</td>
						<td class="text-center">{{ $decor['oldPrice'] }}</td>
						<td class="text-center">{{ $decor['oldPriceMax'] }}</td>
						<td class="text-center">{{ $decor['oldPriceWorth'] }}</td>
						<td class="text-center">
							<b>{{ $decor['newPrice'] }} ({{ $decor['newPrice'] -  $decor['oldPrice'] }})</b></td>
						<td class="text-center">{{ $decor['newPriceMax'] }}</td>
						<td class="text-center">{{ $decor['newPriceWorth'] }}</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		</div>
	@endif

	@if($services)
		<div style="padding-top: 50px;"></div>
		<div class="heading">
			Services
		</div>
		<div class="col-lg-12">
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th rowspan="2" class="text-center">S.No</th>
					<th rowspan="2" class="text-center">Option Id</th>
					<th rowspan="2" class="text-center">Option Name</th>
					<th rowspan="1" colspan="3" class="text-center">Old</th>
					<th rowspan="1" colspan="3" class="text-center">New</th>
				</tr>
				<tr>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
				</tr>
				</thead>
				<tbody>
				@php $i = 1; @endphp
				@foreach($services as $service)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $service['optionId'] }}</td>
						<td>{{ ucwords($service['optionName']) }}</td>
						<td class="text-center">{{ $service['oldPrice'] }}</td>
						<td class="text-center">{{ $service['oldPriceMax'] }}</td>
						<td class="text-center">{{ $service['oldPriceWorth'] }}</td>
						<td class="text-center">
							<b>{{ $service['newPrice'] }} ({{ $service['newPrice'] -  $service['oldPrice'] }})</b></td>
						<td class="text-center">{{ $service['newPriceMax'] }}</td>
						<td class="text-center">{{ $service['newPriceWorth'] }}</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		</div>
	@endif

	@if($packages)
		<div style="padding-top: 50px;"></div>
		<div class="heading">
			Packages
		</div>
		<div class="col-lg-12">
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th rowspan="2" class="text-center">S.No</th>
					<th rowspan="2" class="text-center">Option Id</th>
					<th rowspan="2" class="text-center">Option Name</th>
					<th rowspan="1" colspan="4" class="text-center">Old</th>
					<th rowspan="1" colspan="4" class="text-center">New</th>
				</tr>
				<tr>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
					<th rowspan="1" class="text-center">Price Per Guest</th>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
					<th rowspan="1" class="text-center">Price Per Guest</th>
				</tr>
				</thead>
				<tbody>
				@php $i = 1; @endphp
				@foreach($packages as $package)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $package['optionId'] }}</td>
						<td>{{ ucwords($package['optionName']) }}</td>
						<td class="text-center">{{ $package['oldPrice'] }}</td>
						<td class="text-center">{{ $package['oldPriceMax'] }}</td>
						<td class="text-center">{{ $package['oldPriceWorth'] }}</td>
						<td class="text-center">{{ $package['oldPricePerExtraGuest'] }}</td>
						<td class="text-center">
							<b>{{ $package['newPrice'] }} ({{ $package['newPrice'] -  $package['oldPrice'] }})</b></td>
						<td class="text-center">{{ $package['newPriceMax'] }}</td>
						<td class="text-center">{{ $package['newPriceWorth'] }}</td>
						<td class="text-center">{{ $package['newPricePerExtraGuest'] }}</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		</div>
	@endif

	@if($venueDeals)
		<div style="padding-top: 50px;"></div>
		<div class="heading">
			Venue Deals
		</div>
		<div class="col-lg-12">
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th rowspan="2" class="text-center">S.No</th>
					<th rowspan="2" class="text-center">Option Id</th>
					<th rowspan="2" class="text-center">Option Name</th>
					<th rowspan="1" colspan="4" class="text-center">Old</th>
					<th rowspan="1" colspan="4" class="text-center">New</th>
				</tr>
				<tr>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
					<th rowspan="1" class="text-center">Price Per Guest</th>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
					<th rowspan="1" class="text-center">Price Per Guest</th>
				</tr>
				</thead>
				<tbody>
				@php $i = 1; @endphp
				@foreach($venueDeals as $venueDeal)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $venueDeal['optionId'] }}</td>
						<td class="text-left">{{ ucwords($venueDeal['optionName']) }}</td>
						<td class="text-center">{{ $venueDeal['oldPrice'] }}</td>
						<td class="text-center">{{ $venueDeal['oldPriceMax'] }}</td>
						<td class="text-center">{{ $venueDeal['oldPriceWorth'] }}</td>
						<td class="text-center">{{ $venueDeal['oldPricePerExtraGuest'] }}</td>
						<td class="text-center">
							<b>{{ $venueDeal['newPrice'] }} ({{ $venueDeal['newPrice'] -  $venueDeal['oldPrice'] }})</b>
						</td>
						<td class="text-center">{{ $venueDeal['newPriceMax'] }}</td>
						<td class="text-center">{{ $venueDeal['newPriceWorth'] }}</td>
						<td class="text-center">{{ $venueDeal['newPricePerExtraGuest'] }}</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		</div>
	@endif

	@if($trends)
		<div style="padding-top: 50px;"></div>
		<div class="heading">
			Trends
		</div>
		<div class="col-lg-12">
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th rowspan="2" class="text-center">S.No</th>
					<th rowspan="2" class="text-center">Option Id</th>
					<th rowspan="2" class="text-center">Option Name</th>
					<th rowspan="1" colspan="3" class="text-center">Old</th>
					<th rowspan="1" colspan="3" class="text-center">New</th>
				</tr>
				<tr>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Max Price</th>
					<th rowspan="1" class="text-center">Price Worth</th>
				</tr>
				</thead>
				<tbody>
				@php $i = 1; @endphp
				@foreach($trends as $trend)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $trend['optionId'] }}</td>
						<td class="text-left">{{ ucwords($trend['optionName']) }}</td>
						<td class="text-center">{{ $trend['oldPrice'] }}</td>
						<td class="text-center">{{ $trend['oldPriceMax'] }}</td>
						<td class="text-center">{{ $trend['oldPriceWorth'] }}</td>
						<td class="text-center">
							<b>{{ $trend['newPrice'] }} ({{ $trend['newPrice'] -  $trend['oldPrice'] }})</b>
						</td>
						<td class="text-center">{{ $trend['newPriceMax'] }}</td>
						<td class="text-center">{{ $trend['newPriceWorth'] }}</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		</div>
	@endif

	@if($cakes)
		<div style="padding-top: 50px;"></div>
		<div class="heading">
			Cakes
		</div>
		<div class="col-lg-12">
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th rowspan="2" class="text-center">S.No</th>
					<th rowspan="2" class="text-center">Option Id</th>
					<th rowspan="2" class="text-center">Option Name</th>
					<th rowspan="1" colspan="3" class="text-center">Old</th>
					<th rowspan="1" colspan="3" class="text-center">New</th>
				</tr>
				<tr>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Price Per Kg</th>
					<th rowspan="1" class="text-center">Price Worth</th>
					<th rowspan="1" class="text-center">Price</th>
					<th rowspan="1" class="text-center">Price Per Kg</th>
					<th rowspan="1" class="text-center">Price Worth</th>
				</tr>
				</thead>
				<tbody>
				@php $i = 1; @endphp
				@foreach($cakes as $cake)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $cake['optionId'] }}</td>
						<td class="text-left">{{ ucwords($cake['optionName']) }}</td>
						<td class="text-center">{{ $cake['oldPrice'] }}</td>
						<td class="text-center">{{ $cake['oldPricePerKg'] }}</td>
						<td class="text-center">{{ $cake['oldPriceWorth'] }}</td>
						<td class="text-center">
							<b>{{ $cake['newPrice'] }} ({{ $cake['newPrice'] -  $cake['oldPrice'] }})</b>
						</td>
						<td class="text-center">{{ $cake['newPricePerKg'] }}</td>
						<td class="text-center">{{ $cake['newPriceWorth'] }}</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		</div>
	@endif

	@if($venues)
		<div style="padding-top: 50px;"></div>
		<div class="heading">
			Venues
		</div>
		<div class="col-lg-12">
			<table style="border: 1px solid black" class="table table-bordered">
				<thead>
				<tr>
					<th rowspan="2" class="text-center">S.No</th>
					<th rowspan="2" class="text-center">Option Id</th>
					<th rowspan="2" class="text-center">Option Name</th>
					<th rowspan="1" colspan="11" class="text-center">Old</th>
					<th rowspan="1" colspan="11" class="text-center">New</th>
				</tr>
				<tr>
					<th rowspan="1" class="text-center">Rent Worth</th>
					<th rowspan="1" class="text-center">Price Min Rent</th>
					<th rowspan="1" class="text-center">Price Max Rent</th>
					<th rowspan="1" class="text-center">Price Kid</th>
					<th rowspan="1" class="text-center">Price Adult</th>
					<th rowspan="1" class="text-center">Worth Kid</th>
					<th rowspan="1" class="text-center">Worth Adult</th>
					<th rowspan="1" class="text-center">Price Min Veg</th>
					<th rowspan="1" class="text-center">Min Veg Worth</th>
					<th rowspan="1" class="text-center">Price Min NonVeg</th>
					<th rowspan="1" class="text-center">Min NonVeg Worth</th>

					<th rowspan="1" class="text-center">Rent Worth</th>
					<th rowspan="1" class="text-center">Price Min Rent</th>
					<th rowspan="1" class="text-center">Price Max Rent</th>
					<th rowspan="1" class="text-center">Price Kid</th>
					<th rowspan="1" class="text-center">Price Adult</th>
					<th rowspan="1" class="text-center">Worth Kid</th>
					<th rowspan="1" class="text-center">Worth Adult</th>
					<th rowspan="1" class="text-center">Price Min Veg</th>
					<th rowspan="1" class="text-center">Min Veg Worth</th>
					<th rowspan="1" class="text-center">Price Min NonVeg</th>
					<th rowspan="1" class="text-center">Min NonVeg Worth</th>
				</tr>
				</thead>
				<tbody>
				@php $i = 1; @endphp
				@foreach($venues as $venue)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $venue['optionId'] }}</td>
						<td class="text-left">{{ ucwords($venue['optionName']) }}</td>
						<td class="text-center">{{ $venue['oldWorthRent'] }}</td>
						<td class="text-center">{{ $venue['oldPriceMinRent'] }}</td>
						<td class="text-center">{{ $venue['oldPriceMaxRent'] }}</td>
						<td class="text-center">{{ $venue['oldPriceKid'] }}</td>
						<td class="text-center">{{ $venue['oldPriceAdult'] }}</td>
						<td class="text-center">{{ $venue['oldWorthKid'] }}</td>
						<td class="text-center">{{ $venue['oldWorthAdult'] }}</td>
						<td class="text-center">{{ $venue['oldPriceMinVeg'] }}</td>
						<td class="text-center">{{ $venue['oldMinVegWorth'] }}</td>
						<td class="text-center">{{ $venue['oldPriceMinNonVeg'] }}</td>
						<td class="text-center">{{ $venue['oldMinNonVegWorth'] }}</td>
						<td class="text-center">{{ $venue['newWorthRent'] }}</td>
						<td class="text-center">{{ $venue['newPriceMinRent'] }}</td>
						<td class="text-center">{{ $venue['newPriceMaxRent'] }}</td>
						<td class="text-center">{{ $venue['newPriceKid'] }}</td>
						<td class="text-center">{{ $venue['newPriceAdult'] }}</td>
						<td class="text-center">{{ $venue['newWorthKid'] }}</td>
						<td class="text-center">{{ $venue['newWorthAdult'] }}</td>
						<td class="text-center">
							<b>{{ $venue['newPriceMinVeg'] }} ({{ $venue['newPriceMinVeg'] -  $venue['oldPriceMinVeg'] }})</b>
						</td>
						<td class="text-center">{{ $venue['newMinVegWorth'] }}</td>
						<td class="text-center">{{ $venue['newPriceMinNonVeg'] }}</td>
						<td class="text-center">{{ $venue['newMinNonVegWorth'] }}</td>
					</tr>
					@php $i++; @endphp
				@endforeach
				</tbody>
			</table>
		</div>
	@endif

@endsection