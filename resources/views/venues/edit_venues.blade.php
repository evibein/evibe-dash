@extends('layout.base')

@section('title')
	<title>Edit Venue - {{ $data['venue']->name }} </title>
@stop

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
			<div class="app-tabs">
				@include('app/tabs')
			</div>
			<ol class="breadcrumb">
				<li><a href="/venues/list">Show All Venues</a></li>
			</ol>
			<div class="page-content venue-wrap">
				<div class="venus-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="pull-left">
								<h2 class="panel-title">Edit Venue Details</h2>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							@if ($errors->count() > 0)
								<div class="errors-cnt alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;
									</button>
									<ul class="erros-list">
										@foreach ($errors->all() as $message)
											<li>{!! $message !!}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<form action="/venues/update/{{$data['venue']->id}}" method="POST" role="form">
								<div class="alert alert-warning">
									<h4 class="text-center">Contact Information</h4>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Venue Name</span>
												</label>
												<input id="venueName" name="venueName" type="text" class="form-control"
														placeholder="Enter company name"
														value="@if (Input::old('venueName')){{ Input::old('venueName') }}@else{{ $data['venue']->name }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Venue Type</span>
												</label>
												<select name="venueType" id="venueType" class="form-control">
													@foreach ($data['venueTypes'] as $type)
														<option value="{{ $type->id }}"
																@if((Input::old('venueType') && $type->id == Input::old('venueType')) || (Input::old('venueType') == '' && $type->id == $data['venue']->type_id)) selected @endif>
															{{ $type->name }}
														</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label><span>Contact Person</span></label>
												<input id="venuePerson" name="venuePerson" type="text"
														class="form-control"
														placeholder="Enter contact person name"
														value="@if (Input::old('venuePerson')){{ Input::old('venuePerson') }}@else{{ $data['venue']->person }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Phone Number</label>
												<input id="venuePhone" name="venuePhone" type="text"
														class="form-control"
														placeholder="Enter contact number"
														value="@if (Input::old('venuePhone')){{ Input::old('venuePhone') }}@else{{ $data['venue']->phone }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>City</span>
												</label>
												<select name="city" id="city" class="form-control"
														data-old="@if(old('city')){{ old('city') }}@elseif($data['venue']->city_id){{ $data['venue']->city_id }}@endif">
													@foreach ($data['cities'] as $city)
														<option value="{{ $city->id }}">{{ $city->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Location</span>
													<a href="#" class="btn-add-edit-location text-normal"
															data-redirectto="{{ route('venues.edit',$data['venue']->id) }}">(Add New Location)</a>
												</label>
												<select name="area" id="area" class="form-control"
														data-old="@if(old('area')){{ old('area') }}@elseif($data['venue']->area_id){{ $data['venue']->area_id }}@endif">
													@foreach ($data['areas'] as $area)
														<option value="{{ $area->id }}">
															{{ $area->name }}
														</option>
													@endforeach
												</select>
												<ul id="allAreaOptions" class="hide">
													@foreach ($data['areas'] as $area)
														<li data-id="{{ $area->id }}"
																data-city_id="{{ $area->city_id }}"
																data-name="{{ $area->name }}"></li>
													@endforeach
												</ul>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>

										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Email 1</label>
												<input type="text" name="altEmail1" class="form-control"
														placeholder="Enter alternative email"
														value="@if(old('altEmail1')){{ old('altEmail1') }}@else{{ $data['venue']->alt_email_1 }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Email 2</label>
												<input type="text" name="altEmail2" class="form-control"
														placeholder="Enter alternative email"
														value="@if(old('altEmail2')){{ old('altEmail2') }}@else{{ $data['venue']->alt_email_2 }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Phone</label>
												<input type="text" name="altPhone" class="form-control"
														placeholder="Enter alternative email"
														value="@if(old('altPhone')){{ old('altPhone') }}@else{{ $data['venue']->alt_phone }}@endif">
											</div>
										</div>
									</div>
									<div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="source">Source Raised Through</label>
												<select name="source" id="source" class="form-control">
													@foreach ($data['source'] as $source)
														<option value="{{ $source->id }}"
																@if($data['venue']->source_id == 	$source->id) selected @endif>{{ $source->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Source Specific</label>
												<input id="sourceSpecific" name="sourceSpecific" type="text"
														class="form-control"
														placeholder="Enter specific source"
														value="@if(old('sourceSpecific')){{ old('sourceSpecific') }}@else{{$data['venue']->source_specific}}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Facebook url</label>
												<input id="sourceFacebook" name="sourceFacebook" type="text"
														class="form-control"
														placeholder="Enter company facebook profile"
														value="@if(old('sourceFacebook')){{ old('sourceFacebook') }}@else{{$data['venue']->facebook_url}}@endif">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Google-plus url</label>
												<input id="sourceGooglePlus" name="sourceGooglePlus" type="text"
														class="form-control"
														placeholder="Enter company google plus profile"
														value="@if(old('sourceGooglePlus')){{ old('sourceGooglePlus') }}@else{{$data['venue']->google_plus_url}}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Instagram url</label>
												<input id="sourceInstagram" name="sourceInstagram" type="text"
														class="form-control"
														placeholder="Enter company instagram profile"
														value="@if(old('sourceInstagram')){{ old('sourceInstagram') }}@else{{$data['venue']->instagram_url}}@endif">
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="leadStatus">Lead Status</label>
												<select name="leadStatus" id="leadStatus" class="form-control">
													@foreach ($data['leadStatus'] as $status)
														<option value="{{ $status->id }}"
																@if($data['venue']->lead_status_id == $status->id) selected @endif>{{ $status->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label>Commission rate</label>
												<input id="commissionRate" name="commissionRate" type="text"
														class="form-control"
														placeholder="Enter commission rate"
														value="@if(old('commissionRate')){{ old('commissionRate') }}@else{{$data['venue']->commission_rate}}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Is Partner?</span>

												</label><br/>
												<label class="font-normal mar-r-10">
													<input type="radio" name="venueIsPartner" value="1"
															@if ($data['venue']->is_partner==1) checked="checked" @endif> Yes
												</label>
												<label class="font-normal">
													<input type="radio" name="venueIsPartner" value="0"
															@if ($data['venue']->is_partner==0) checked="checked" @endif> No
												</label>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Is Verified?</span>

												</label>
												<br/>
												<label class="font-normal mar-r-10">
													<input type="radio" name="venueIsVerified" value="1"
															@if ($data['venue']->is_verified==1) checked="checked" @endif> Yes
												</label>
												<label class="font-normal">
													<input type="radio" name="venueIsVerified" value="0"
															@if ($data['venue']->is_verified==0) checked="checked" @endif> No
												</label>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="vMappingAddress">Address</label>
												<textarea id="vMappingAddress" name="vMappingAddress" class="form-control vertical-resize"
														rows="4" placeholder="enter Address..."
														data-val="{{ $data['venue']->full_address }}">{!! $data['venue']->full_address !!}</textarea>
											</div>
										</div>
										<div class="pad-r-20 pad-l-10 col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="vMappingLandmark">Landmark</label>
												<textarea id="vMappingLandmark" name="vMappingLandmark" class="form-control vertical-resize"
														rows="4" placeholder="enter Landmark..."
														data-val="{{ $data['venue']->landmark }}">{{$data['venue']->landmark }}</textarea>
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="vMappingLongitude">Longitude</label>
												<input type="text"
														value="@if(Input::old('vMappingLongitude')){{Input::old('vMappingLongitude')}}@else{{ $data['venue']->map_long }}@endif"
														name="vMappingLongitude"
														class="form-control vMappingLongitude"
														placeholder="Enter the longitude">
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="mapCoordinates">Latitude</label>
												<input class="form-control vMappingLatitude" type="text"
														name="vMappingLatitude"
														value="@if(Input::old('vMappingLatitude')){{Input::old('vMappingLatitude')}}@else{{ $data['venue']->map_lat }}@endif"
														placeholder="Enter the latitude">
											</div>
										</div>
										<div class="pad-r-20 pad-l-10 col-sm-4 col-md-4 col-lg-4 col-xs-12">
											<div class="form-group">
												<label for="comments">Comments</label>
												<textarea id="comments" name="comments"
														class="form-control vertical-resize"
														rows="4" placeholder="enter comments..."
														data-val="{!! $data['venue']->comments !!}">{!! $data['venue']->comments !!}</textarea>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="alert alert-info">
									<h4 class="text-center">For Vendor Management System (VMS)</h4>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<label>
												<span>Username</span>
											</label>
											<input id="venueUsername" name="venueUsername" type="text"
													class="form-control" disabled
													placeholder="Enter valid email address"
													value="{{ $data['userInfo']->username }}">
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<input type="hidden" name="venueId" id="venueId"
												value="{{ $data['venue']->id }}"/>
										<div class="form-group">
											<input type="submit" value="Submit" class="btn btn-primary"/>
											<a class="pad-l-10" href="/venues/view/{{ $data['venue']->id }}">Cancel</a>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop


@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {

			// selectize area

			// add new location
			$('.btn-add-edit-location').click(function (event) {

				event.preventDefault();

				$('#addNewAreaModal').data('redirectto', $(this).data('redirectto'));
				$('#addNewAreaModal').modal();

			});

			// refresh option of city
			function initCityAreaSelect() {
				var areaValue,
					$selectArea,
					$area = $('#area'),
					$city = $('#city');

				$city.selectize({
					onChange: onSelectCityChange
				});

				$selectArea = $area.selectize({
					valueField: 'id',
					labelField: 'name',
					searchField: ['id', 'name']
				});
				areaValue = $selectArea[0].selectize;
				if (!$area.val()) {
					areaValue.disable();
				}

				function onSelectCityChange(value, isManual) {
					var results = [];
					areaValue.disable();
					areaValue.clearOptions();
					if (!value.length || value == -1) {
						return false;
					}

					$('#allAreaOptions').find('li').each(function (count) {
						if ($(this).data('city_id') && $(this).data('city_id') == value) {
							results.push({
								id: $(this).data('id'),
								name: $(this).data('name')
							});
						}
					});

					areaValue.enable();
					areaValue.addOption(results);

					var old = $.trim($area.data('old')) && $city.data('old') == $city.val() ? $.trim($area.data('old')) : results[0].id;
					areaValue.setValue(old);
				}

				if ($city.val()) {
					onSelectCityChange($city.val(), true)
				}

				// select default city

				if ($city.data('old')) {
					$city[0].selectize.setValue($.trim($city.data('old')))
				}
			}

			initCityAreaSelect();
		});

	</script>
@stop