@extends('layout.base')

@section('title')
<title>Edit External Venue Ratings | VMS - Evibe.in</title>
@stop

@section('content')
<div class="container">
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="inner">
				<div class="pull-left">
					<h3 class="panel-title">Venue Ext Ratings</h3>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="panel-body">
			<div class="pad-10">
				<div class="errors-cnt">
					<div class="errors-box">
						@if ($errors->any())
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								@if ($errors->any())
									<div>{{ $errors->first() }}</div>
								@endif
							</div>
						@endif
					</div>
				</div>
				<form action="{{ URL::route('venueExtRatingsUpdate',$data['venueId']) }}" method="POST" role="form" class="form">
					<table class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<th>Name</th>
								<th>Ext URL</th>
								<th>Rating Value</th>
								<th>Rating Count</th>
								<th>Rating Max</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($data['typeExtRatings'] as $typeExtRating)
								<?php $existingRating = AppUtil::filterVenueExtRatings($data['venueRatings'], $typeExtRating->id); ?>
								<tr>
									<td>{{ $typeExtRating->id }}</td>
									<td>
										<span>{{ $typeExtRating->name }}</span>
										<input type="hidden" name="extRow[{{ $typeExtRating->id }}][id]" value="{{ $typeExtRating->id }}" />
										
										
										<input type="hidden" name="extRow[{{ $typeExtRating->id }}][extId]" value="@if($existingRating){{$existingRating->id}}@endif" />
									</td>
									<td>
										<input name="extRow[{{ $typeExtRating->id }}][extUrl]" type="text" class="form-control"
											value="@if($existingRating){{$existingRating->url}}@endif" />
									</td>
									<td>
										<input name="extRow[{{ $typeExtRating->id }}][extRatingValue]" type="text" class="form-control"
											value="@if($existingRating){{$existingRating->rating_value}}@endif" />
									</td>
									<td>
										<input name="extRow[{{ $typeExtRating->id }}][extRatingCount]" type="text" class="form-control"
											value="@if($existingRating){{$existingRating->rating_count}}@endif" />
									</td>
									<td>
										<input name="extRow[{{ $typeExtRating->id }}][extRatingMax]" type="text" class="form-control"
											value="@if($existingRating){{$existingRating->rating_max}}@endif" />
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<div class="form-group pad-t-10 text-center">
						<a href="{{ URL::route('venueExtRatings',$data['venueId']) }}" class="btn btn-link">Back</a>
						<input type="submit" class="btn btn-warning" value="Submit Ratings" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop