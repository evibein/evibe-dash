@extends('layout.base')

@section('title')
<title>View External Venue Ratings | VMS - Evibe.in</title>
@stop

<?php 
?>
@section('content')
<div class="container">
	<div class="mar-b-10">
		<a href="{{ URL::route('venues.list') }}"><< Show all Venues</a>
	</div>
	<div class="panel panel-default">

		<div class="panel-heading">
			<div class="inner">
				<div class="pull-left">
					<h3 class="panel-title">Venue Ext Ratings</h3>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="panel-body">
			@if (count($data['venueRatings']) > 0)
				<div class="pad-10 ratings-list">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Name</th>
								<th>Ext Url</th>
								<th>Rating Value</th>
								<th>Rating Count</th>
								<th>Rating Max</th>
								<th>Operation</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['venueRatings'] as $venueRating)
								<tr>
									<td>{{ $venueRating->name }}</td>
									<td>
										<a target="_blank" href="{{ $venueRating->url }}">
											{{ $venueRating->url }}
										</a>
									</td>
									<td>{{ $venueRating->rating_value }}</td>
									<td>{{ $venueRating->rating_count}}</td>
									<td>{{ $venueRating->rating_max }}</td>
									<td>
										<form action="{{ URL::route('venueExtRatingsDelete') }}" method="POST">
											
											<input type="hidden" name="ratingVenueId" value="{{ $venueRating->venue_id }}">
											<input type="hidden" name="ratingRowId" value="{{ $venueRating->id }}">
											<button type="submit" name="submit" class="btn btn-xs btn-danger btn-delete-rating">
												<span><i class="glyphicon glyphicon-trash"></i> Delete</span>
											</button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<div class="pad-t-10 text-center">
						<a href="{{ URL::route('venueExtRatingsEdit',array($venueRating->venue_id) )}}" class="btn btn-info">Edit Ext Ratings</a>
					</div>
				</div>
			@else
				<div class="pad-10">
					<h4>You haven't added any external profiles. Clik on "Add Ext Ratings" to get started.</h4>
					<div class="pad-t-10">
						<a href="{{ URL::route('venueExtRatingsEdit',$data['venueId']) }}" class="btn btn-primary btn-lg">
							<span>Add Ext Ratings</span>
						</a>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>
@stop

@section('javascript')
<script type="text/javascript">

	$(document).ready(function() {

		$('.ratings-list form').on('click', '.btn-delete-rating', function(event) {

			if(!confirm('Are you sure ?')) event.preventDefault();

		});

	});

</script>
@stop