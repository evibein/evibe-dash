@extends('layout.base')
@section('content')
	<div class="container">
		@if ($errors->count() > 0)
			<div class="mar-t-20 ">
				<div class="col-sm-12">
					<div class="alert alert-danger pad-10">
						<ul class="erros-list ls-none">
							<li>{{ $errors->first() }}</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		@endif

		@if(session()->has('fileSizeError'))
			<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
				<a href="#" class="close" data-dismiss="alert"
						aria-label="close">&times;</a>
				<i class="glyphicon glyphicon-remove-sign"></i>
				{{ session('fileSizeError') }}
			</div>
		@endif

		@if(session()->has('successMessage'))
			<div class="alert alert-success mar-l-10 mar-r-10 mar-b-10 pad-10">
				<a href="#" class="close" data-dismiss="alert"
						aria-label="close">&times;</a>
				<i class="glyphicon glyphicon-remove-sign"></i>
				{{ session('successMessage') }}
			</div>
		@endif

		@if(session()->has('failureMessage'))
			<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
				<a href="#" class="close" data-dismiss="alert"
						aria-label="close">&times;</a>
				<i class="glyphicon glyphicon-remove-sign"></i>
				{{ session('failureMessage') }}
			</div>
		@endif

		<div>
			<h1 class="text-center">Upload Dcouments</h1>
			<div class="alert-info pad-10 mar-b-15">
				<span class="">Please upload only PDF or image files and file
					size should be less than 2MB.
				</span>
			</div>

			<form action="{{route('save.documents',$partnerId)}}" method="post" enctype="multipart/form-data">
				<div class="col-sm-4 col-sm-offset-4">

					<label>Select File Name</label>

					<select name="type_document_id" class="form-control mar-b-15" autofocus>
						<option value="0">Type Of File</option>
						@foreach($typeDocumentIds as $type_document)
							<option value="{{$type_document->id}}">{{$type_document->document_type_name}}</option>
						@endforeach
					</select>

					<label>Enter File name</label>
					<input type="text" class="form-control" name="title" size="40" minlength="2"
							placeholder="eg. Aaadher card,license card etc."><br/><br/>

					<input type="file" name="uploadedFileName" class="mar-b-15">

					<button type="submit" value="Submit" class="btn btn-primary mar-r-15">Submit</button>

					<input type="button" class="btn btn-warning mar-l-20" value="Go back!" onclick="history.back()">

				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
@stop