<?php $venue = $data['venue'][0]; ?>

@extends('layout.base')

@section('title')
	<title>Venue {{ $venue->name }} - Details | EvibeDash</title>
@stop

@section('content')
	<div class="container page-content packages-warp">
		<div class="mar-b-10">
			<a href="{{ route('venues.list') }}"><< Show all Venues</a>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="inner">
					<div class="pull-left">
						<h3 class="panel-title">@if($venue->name) {{ $venue->name }} @else -- @endif</h3>
					</div>
					@if (AppUtil::isTabVisible('delete_venues'))
						<div class="pull-right">
							<a href="/venues/delete/{{ $venue->id }}"
									onClick="return confirm('Do you really want to delete this venue?');"
									class="btn btn-sm btn-danger mar-r-10">
								<i class="glyphicon glyphicon-trash"></i> Delete</a>
						</div>
					@endif
					@if (AppUtil::isTabVisible('edit_venues'))
						<div class="pull-right" style="margin-right:1%">
							<a class="btn btn-sm btn-primary mar-r-10" href="/venues/edit/{{ $venue->id }}">
								<i class="glyphicon glyphicon-pencil"></i> Edit</a>

						</div>
					@endif
					<div class="pull-right">
						@if(AppUtil::isTabVisible('partner_bank_details') && $venue->user)
							<div class="pull-right" style="margin-right:1%">
								<a class="btn btn-sm btn-warning mar-r-10"
										href="{{ route('partner.bank_details.edit',$venue->user->id) }}">
									<i class="glyphicon glyphicon-credit-card"></i> Edit Bank Info</a>

							</div>
						@endif
					</div>
					<div class="pull-right" style="margin-right:1%">


						<a class="btn btn-info btn-sm mar-r-10"
								href="{{config('evibe.host').'partner-options/'.config("evibe.ticket_type.venues").'/'.$venue->id}}"
								target="_blank">
							<i class="glyphicon glyphicon-globe"></i><span> See Options</span>
						</a>

						<a class="btn btn-sm btn-sm btn-success mar-r-10"
								href="{{route('upload.documents',[$venue->id, config('evibe.ticket_type.venues')])}}">
							<i class="glyphicon glyphicon-upload"></i>Upload Documents
						</a>

						<a class="btn btn-sm btn-sm btn-info mar-r-10 iq"
								data-url="{{route('iq.add',[ 'mapId' => $venue->id,'mapTypeID' => config('evibe.ticket_type.venues')])}}"
								data-redirect="{{route('venues.view',$venue->id)}}">

							<i class="glyphicon glyphicon-plus"></i> Add IQ
						</a>
						<a class="btn btn-info btn-sm mar-r-10" href="{{ route('venueExtRatings',$venue->id) }}"><i
									class="glyphicon glyphicon-stats"></i> Ext Ratings</a>
						<a class="btn btn-sm btn-evibe mar-r-10" target="_blank"
								href="@if($venue->user){{ route('partner.profile.autoLogin',$venue->user->short_url) }}@endif"><i
									class="glyphicon glyphicon-eye-open"></i> <span
									class="text-lower">live profile</span></a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel-body">
				<div class="venue-profile-data">
					@if(session()->has('customError'))
						<div class="mar-t-10 mar-b-10 pad-6 alert-danger">
							{{ session('customError') }}
						</div>
					@endif
					@if(session()->has('successMsg'))
						<div class="mar-t-10 mar-b-10 pad-6 alert-success">
							{{ session('successMsg') }}
						</div>
					@endif
					<div class="text-center">
						<div class="alert @if ($venue->is_live) alert-success @else alert-danger @endif venue-status">
							@if (AppUtil::isTabVisible('activate_venue'))
								@if ($venue->is_live)
									<a href="/venues/inacive/{{ $venue->id }}"
											onClick="return confirm('Do you really want to deactivate this venue?');"
											class="btn btn-sm btn-danger">Make InActive </a>
								@else
									<a href="/venues/active/{{ $venue->id }}"
											onClick="return confirm('Do you really want to Active this venue?');"
											class="btn btn-info">Make Active</a>
								@endif
							@else
								@if ($venue->is_live)
									<span>InActive</span>
								@else
									<span>Active</span>
								@endif
							@endif
						</div>
					</div>
					<section class="data-sec pad-b-10">

						<!--Internal question start -->
						<div class="col-lg-13">
							<div class="iq-container">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="no-mar">Internal Questions</h4>
									</div>
									<div class="panel-body">
										@if ($data['iqs']->count())
											<ul class="iq-list no-mar no-pad">
												@foreach($data['iqs'] as $iq)
													<li class="@if ($iq->deleted_at) strike @endif">
														<div class="pull-left">
															<div class="iq-q">{!! $iq->question !!}</div>
															<div class="iq-ans">{!! $iq->answer !!}</div>
															<div class="iq-info">
																<i>{{ date('j F, g:i a',strtotime($iq->created_at)) }} by
																	<b>{{ $iq->user ? $iq->user->name:"--" }}</b>
																	@if ($iq->deleted_by)
																		Deleted by
																		<b>{{  $iq->deletedBy ? $iq->deletedBy->name :"--" }} </b>
																	@endif
																</i>
															</div>
														</div>
														<div class="pull-right">
															@if (!$iq->deleted_at)
																<button class=" btn btn-sm btn-primary in-blk iq-edit"
																		data-url="{{route('iq.edit',$iq->id)}}"
																		data-redirect="{{route('venues.view',$venue->id)}}"
																		data-question="{!! $iq->question !!}"
																		data-id="{{$iq->id}}">
																	<i class="glyphicon glyphicon-edit"></i>

																</button>
																<button class=" btn btn-sm btn-danger in-blk iq-delete"
																		data-url="{{route('iq.delete',$iq->id)}}"><i
																			class="glyphicon glyphicon-trash"></i>
																</button>
															@endif
														</div>
														<div class="clearfix"></div>
													</li>
												@endforeach
											</ul>
										@else
											<div class="text-danger">No internal questions were added yet. Click on
												<b>Add IQ</b> button to create first internal question.
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>

						<!--Internal questions end-->
					</section>
					<section class="data-sec pad-b-20">
						<h4 class="text-danger">Contact Information</h4>
						<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">

							<table class="table table-striped table-bordered">

								<tr>
									<td><label>Name</label></td>
									<td>@if($venue->name){{ $venue->name }}@else -- @endif</td>
								</tr>
								<tr>
									<td><label>Primary Person</label></td>
									<td>@if($venue->person) {{ $venue->person }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Primary Number</label></td>
									<td>@if($venue->phone) {{ $venue->phone }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Alt. Phone Number</label></td>
									<td>@if($venue->alt_phone) {{ $venue->alt_phone }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Primary Email Id</label></td>
									<td>@if($venue->email) {{ $venue->email }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Landline</label></td>
									<td>@if($venue->landline) {{ $venue->landline }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Website</label></td>
									<td>@if ($venue->website) {{ $venue->website }} @else --- @endif</td>
								</tr>
								<tr>
									<td><label>City</label></td>
									<td>@if ($venue->area->name) {{ $venue->area->name }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Evibe Code</label></td>
									<td>@if($venue->code) {{ $venue->code }} @else -- @endif</td>
								</tr>
								@if($venue->user)
									<tr>
										<td><label>Short Url</label></td>
										<td>
											{{ $venue->user->short_url ? config('evibe.live.host').'/p/'.$venue->user->short_url :"--" }}
										</td>
									</tr>
								@endif
								<tr>
									<td><label>Source</label></td>
									<td class="lbl-val">
										@if($venue->source && $venue->source->name)
											{{ $venue->source->name }}
										@else
											---
										@endif
									</td>
								</tr>
								<tr>
									<td><label>Lead Status</label></td>
									<td class="lbl-val">
										@if ($venue->leadStatus && $venue->leadStatus->name)
											{{ $venue->leadStatus->name }}
										@else
											---
										@endif
									</td>
								</tr>
								<tr>
									<td><label>Commission rate</label></td>
									<td class="lbl-val">
										@if($venue->commission_rate)
											{{ $venue->commission_rate }}
										@else
											---
										@endif
									</td>
								</tr>
							</table>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6">
							<table class="table table-striped table-bordered">
								<tr>
									<td><label>Alt Person</label></td>
									<td>@if($venue->alt_person) {{ $venue->alt_person }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Alt Number</label></td>
									<td>@if($venue->alt_phone) {{ $venue->alt_phone }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Alt Email Id</label></td>
									<td>@if($venue->alt_email) {{ $venue->alt_email }} @else -- @endif</td>
								</tr>
								@if($venue->alt_email_1 || $venue->alt_email_2 || $venue->alt_email_13)
									<tr>
										<td><label>Alt Emails</label></td>
										<td class="lbl-val">
											<p>@if($venue->alt_email_1){{ $venue->alt_email_1 }}@endif</p>
											<p>@if($venue->alt_email_2){{ $venue->alt_email_2 }}@endif</p>
											<p>@if($venue->alt_email_3){{ $venue->alt_email_3 }}@endif</p>
										</td>
									</tr>
								@endif
								<tr>
									<td><label>Address</label></td>
									<td>@if($venue->full_address) {{ $venue->full_address }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Landmark</label></td>
									<td>@if($venue->landmark) {{ $venue->landmark }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Zip</label></td>
									<td>@if($venue->zip) {{ $venue->zip }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Area</label></td>
									<td>@if($venue->area->name) {{ $venue->area->name }} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Map Co-ordinates</label></td>
									<td>@if($venue->map_lat) {{ $venue->map_lat }} @else -- @endif, @if($venue->map_long) {{ $venue->map_long }} @else -- @endif</td>
								</tr>
								@if($venue->source_specific)
									<tr>
										<td class="lbl"><label>Source Specific</label></td>
										<td class="lbl-val">{{ $venue->source_specific }}
										</td>
									</tr>
								@endif
								<tr>
									<td class="lbl"><label>Facebook url</label></td>
									<td class="lbl-val">
										@if($venue->facebook_url)
											{{ $venue->facebook_url }}
										@else
											---
										@endif
									</td>
								</tr>
								<tr>
									<td class="lbl"><label>Google plus url</label></td>
									<td class="lbl-val">
										@if($venue->google_plus_url)
											{{ $venue->google_plus_url }}
										@else
											---
										@endif
									</td>
								</tr>
								<tr>
									<td class="lbl"><label>Instagram url</label></td>
									<td class="lbl-val">
										@if($venue->instagram_url)
											{{ $venue->instagram_url }}
										@else
											---
										@endif
									</td>
								</tr>
								<tr>
									<td class="lbl"><label>Comments</label></td>
									<td class="lbl-val">
										@if($venue->comments)
											{!! $venue->comments !!}
										@else
											---
										@endif
									</td>
								</tr>
							</table>

						</div>
						<div class="clearfix"></div>
						<h4 class="text-danger mar-b-15">Documents</h4>

						@if(count($data['documents'])>0)
							@foreach($data['documents'] as $document)
								<div class="uploaded-images text-center">
									<form action="{{route('file.delete',$document->id)}}"
											method="post" enctype="multipart/form-data">
										<a href="{{ $document->getPath() }}"
												target="_blank">
											@if($document["type_file"] == config('evibe.type_file.pdf'))
												<embed src="{{ $document->getPath() }}"
														class="uploaded-image-size embed-responsive-item">
													@else
														<img src="{{ $document->getPath() }}"
																class="mar-b-15 uploaded-image-size img-responsive"
																alt="Uploaded File">
											@endif
										</a>
										<div class="text-center">
											@if ($document->title)
												<div class="mar-b-15 text-bold">{{$document->title }}</div>
											@endif
											<div>{{$document->getName()}}</div>
										</div>
										<input type="submit" value="delete" class="btn btn-xs btn-warning">
									</form>
								</div>
							@endforeach
						@else
							<div class="alert alert-danger">No documents found.</div>
						@endif

						<div class="clearfix"></div>
						@if($venue->user)
							@include('partner.profile.show_bank_details',['bankDetails'=> $venue->user->bankDetails])
						@endif
					</section>

					<section class="data-sec pad-b-20">
						<h4 class="text-danger">Food &amp; Beverage</h4>
						<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
							<table class="table table-striped table-bordered">
								<tr>
									<td><label>Cusines</label></td>
									<td>@if($data['VenueCuisine']){{$data['VenueCuisine']}} @else -- @endif</td>
								</tr>
								<tr>
									<td><label>Pure Veg</label></td>
									<td>@if ($venue->is_pure_veg)Yes @else No @endif</td>
								</tr>
								<tr>
									<td><label>Tax Percent</label></td>
									<td>@if($venue->tax_percent) {{ $venue->tax_percent }}% @else -- @endif</td>
								</tr>
							</table>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6">
							<table class="table table-striped table-bordered">
								<tr>
									<td><label>Veg Price</label></td>
									<td>
										<span class="rupee-font">&#8377;</span>
										@if($venue->price_min_veg)
											<del style="margin-right:5%">{{ $venue->min_veg_worth }} </del>{{ $venue->price_min_veg }} onwards @else -- @endif
									</td>
								</tr>
								<tr>
									<td><label>Non-Veg Price</label></td>
									<td>
										<span class="rupee-font">&#8377;</span>
										@if($venue->price_min_nonveg)
											<del style="margin-right:5%">{{ $venue->min_nonveg_worth }} </del>{{ $venue->price_min_nonveg }} onwards @else -- @endif
									</td>
								</tr>
								<tr class="@if ($venue->is_alcohol_served) success @else danger @endif">
									<td><label>Alcohol Served</label></td>
									<td>@if ($venue->is_alcohol_served)Yes @else No @endif</td>
								</tr>
								<tr class="@if ($venue->has_food_sampling) success @else danger @endif">
									<td><label>Has Food Sampling</label></td>
									<td>@if ($venue->has_food_sampling)Yes @else No @endif</td>
								</tr>
								<tr>
									<td><label>Food Sampling Comments</label></td>
									<td>@if($venue->food_sampling_comments) {!! $venue->food_sampling_comments !!} @else -- @endif</td>
								</tr>
							</table>
						</div>
						<div class="clearfix"></div>
					</section>

					<section class="data-sec pad-b-20">
						<h4 class="text-danger">Facilities &amp; Equipment</h4>
						<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
							<table class="table table-striped table-bordered">
								<tr>
									<td><label>Capacity Range</label></td>
									<td>@if($venue->cap_min){{ $venue->cap_min }} @else -- @endif - @if($venue->cap_max) {{ $venue->cap_max }} @else -- @endif (Float: @if($venue->cap_float) {{ $venue->cap_float }} @else -- @endif )</td>
								</tr>
								<tr class="@if ($venue->has_valet_parking) success @else danger @endif">
									<td><label>Valet Parking</label></td>
									<td>@if ($venue->has_valet_parking)Yes @else No @endif</td>
								</tr>
								<tr>
									<td><label>Car Park Count</label></td>
									<td>@if($venue->car_park_count) {{ $venue->car_park_count }} @else -- @endif</td>
								</tr>
								<tr class="@if ($venue->has_lift) success @else danger @endif">
									<td><label>Has Lift</label></td>
									<td>@if ($venue->has_lift)Yes @else No @endif</td>
								</tr>
							</table>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6">
							<table class="table table-striped table-bordered">
								<tr class="@if ($venue->has_sound_system) success @else danger @endif">
									<td><label>Sound System?</label></td>
									<td>
										@if ($venue->has_sound_system)
											Yes (
											@if ($venue->price_sound_system)
												+
												<span class="rupee-font">&#8377;</span> {{ $venue->price_sound_system }}
											@else free
											@endif
										)
										@else
											No
										@endif
									</td>
								</tr>
								<tr class="@if ($venue->has_mike) success @else danger @endif">
									<td><label>Mike?</label></td>
									<td>
										@if ($venue->has_mike)
											Yes (
											@if ($venue->price_mike)
												+ <span class="rupee-font">&#8377;</span> {{ $venue->price_mike }}
											@else free
											@endif
										)
										@else
											No
										@endif
									</td>
								</tr>
								<tr class="@if ($venue->has_projector) success @else danger @endif">
									<td><label>Projector?</label></td>
									<td>
										@if ($venue->has_projector)
											Yes (
											@if ($venue->price_projector)
												+ <span class="rupee-font">&#8377;</span> {{ $venue->price_projector }}
											@else free
											@endif
										)
										@else
											No
										@endif
									</td>
								</tr>
								<tr class="@if ($venue->is_outside_decors_allowed) success @else danger @endif">
									<td><label>Outside decors allowed?</label></td>
									<td>
										@if ($venue->is_outside_decors_allowed)
											Yes (
											@if ($venue->price_outside_decors_allowance)
												+
												<span class="rupee-font">&#8377;</span> {{ $venue->price_outside_decors_allowance }}
											@else no charges
											@endif
										)
										@else
											No
										@endif
									</td>
								</tr>
							</table>
						</div>
						<div class="clearfix"></div>
					</section>

					<!-- Menu list begin -->
					<section class="data-sec pad-b-20">
						<h4 class="text-danger">Menu</h4>
						<div class="pad-10">
							@if ($data['VenueMenu']->count())
								@foreach ($data['VenueMenu'] as $VenueMenu)
									<div class="col-md-4 col-lg-4 col-sm-4">
										<img style="height: 200px;max-width: 100%;" class="profile-img"
												src="{{$VenueMenu->getVenueMenuPic($venue->id,$VenueMenu->id)}}">
										<div class="pad-t-10">
											{{ $VenueMenu->name }}
											<div>Rs. @if($VenueMenu->worth)
													<strike>{{ $VenueMenu->worth }}</strike> @else -- @endif @if($VenueMenu->price) Rs.{{ $VenueMenu->price }} @else -- @endif
											</div>
											<div>@if($VenueMenu->min_guests){{ $VenueMenu->min_guests }} Guests @else -- @endif</div>
										</div>
									</div>
								@endforeach
								<div class="clearfix"></div>
							@else
								<div class="alert alert-danger">
									No results found.
								</div>
							@endif
						</div>
					</section>
					<!-- Menu list end -->

					<section class="data-sec pad-b-20">
						<h4 class="text-danger">Other</h4>
						<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
							<table class="table table-striped table-bordered">
								<tr>
									<td><label>Description</label></td>
									<td>@if($venue->info) {!! $venue->info !!} @else -- @endif</td>
								</tr>
							</table>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6">
							<table class="table table-striped table-bordered">
								<tr>
									<td><label>Terms</label></td>
									<td>@if($venue->terms) {!! $venue->terms !!} @else -- @endif</td>
								</tr>
							</table>
						</div>
						<div class="clearfix"></div>
					</section>

					<!-- Hall results begin -->
					<section class="data-sec pad-b-20">
						<h4 class="text-danger">Venue Halls</h4>
						<div class="col-sm-12 col-md-12 col-lg-12">
							@if(count($data['venueHall']) == 0)
								<div class="alert alert-danger">
									<span>Sorry, no Halls were found.</span>
								</div>
							@else
								@foreach ($data['venueHall'] as $venueHall)

									<div class="owl-item panel "
											style="width: 228px;float:left;border:solid 1.5px;margin-left: 10px;padding-left: 10px; border-color: @if($venueHall->venue->is_live==1) #468847; @else #d2322d @endif">
										<i class="glyphicon glyphicon-home" style="padding:5px"></i>
										<a href="{{ route('venues.hall', $venueHall->id) }}">
											@if ($venueHall->name == '')
												{{ $venueHall->venue->name }}
											@else
												{{ $venueHall->name }}
											@endif
										</a>
										<br>
										<span><b style="padding:3px">By:</b>{{ $venueHall->venue->name }}</span><br>
										<i class="glyphicon glyphicon-qrcode"
												style="padding:5px"></i>{{ $venueHall->code }}<br>
										<i class="glyphicon glyphicon-th-large"
												style="padding:5px"></i>@if($venueHall->type) {{ $venueHall->type->name }}@endif
										<br>
										<i class="glyphicon glyphicon-map-marker"
												style="padding:5px"></i>{{$venueHall->venue->area->name}}<br>
										<i class="glyphicon glyphicon-user"
												style="padding:5px"></i>{{ $venueHall->cap_min }}-{{ $venueHall->cap_max }}
										<br>
										<div>
											<b><span class="rupee-font" style="padding:5px">&#8377;</span> Veg: </b>
											<del style="margin-left:5%">{{ $venueHall->venue->min_veg_worth }}</del>
											<span style="margin-left:5%">{{ $venueHall->venue->price_min_veg }}</span><br>
										</div>
										<div>
											<b><span class="rupee-font" style="padding:3px">&#8377;</span> Non-Veg:</b>
											<del>{{ $venueHall->venue->min_nonveg_worth }}</del>
											<span style="margin-left:2px">{{ $venueHall->venue->price_min_nonveg }}</span>
										</div>
									</div>

								@endforeach
							@endif
						</div>
						<div class="clearfix"></div>
					</section>
					<!-- Hall results end -->

					<!-- Gallery images begin -->
					<section class="data-sec pad-b-20">
						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Gallery Images</h4>
							<div class="pad-10">
								@if (COUNT($data['imgUrl'])>0)
									@foreach($data['imgUrl'] as $imgDetails)
										<div class="col-md-4 col-lg-4 col-sm-4">
											<div>
												<img style="height:200px;max-width: 90%;margin-top:10%"
														class="profile-img"
														src="{{ $imgDetails['url'] }}">
												<br/>
												<a href="{{$imgDetails['url']}}" target='_blank'>
													@if ($imgDetails['type'] == 'hall')
														Hall - {{ $imgDetails['title'] }}
													@else
														Venue - {{ $imgDetails['title'] }}
													@endif
												</a>
											</div>
										</div>
									@endforeach
									<div class="clearfix"></div>
								@else
									<div class="alert alert-danger">No Images found.</div>
								@endif
							</div>
						</section>
						<!-- Gallery images end-->

				</div>
			</div>
		</div>
	</div>

@stop