@extends('layout.base')

@section('title')
	<title>Venue List</title>
@stop

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
			<div class="app-tabs">
				@include('app/tabs')
			</div>
			<div class="page-content vendors-wrap">
				<div class="mar-b-10">
					<div class="pull-left hide">
						<a href="{{ route('venues') }}"><< Show all Halls</a>
					</div>
					<div class="pull-right">
						<select name="city" id="city" class="form-control">
							@foreach($data['cities'] as $city)
								<option value="{{ $city->id }}"
										@if(request()->input('city')== $city->id) selected @endif>{{ $city->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="vendors-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="upper">
								<div class="pull-left">
									<h2 class="panel-title">Venue List</h2>
								</div>
								<div class="pull-right">
									<div class="status-cnt">
										<div class="vt v-t-total in-blk">
											<span>Total: </span>
											<span>{{ count($data['venues']) }}</span>
										</div>

									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="pad-t-10">
								<div class="pull-left">
									<div class="form-inline" role="form">
										<div class="form-group">
											<input name="query" id="query" type="text" class="form-control"
													value="{{ request()->input('query') }}"
													placeholder="Code, Name, person etc."/>
										</div>
										<div class="form-group">
											<button type="button" id="btnSearchSubmit" class="btn btn-info">Search
											</button>
										</div>
										<div class="form-group">
											<a href="{{ route('venues.list') }}" class="btn btn-link">Cancel</a>
										</div>
									</div>
								</div>

								<div class="pull-right hide">
									<a href="{{ route('venues.new') }}" class="btn btn-primary btn-add-new-vendor">
										<i class="glyphicon glyphicon-plus"></i> Add New Venue
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="panel-body">
							@if (count($data['venues']) == 0)
								<div class="alert alert-danger">
									No results found
								</div>
							@else
								<table class="table table-condensed no-mar-b">
									<thead>
									<tr>
										<td>Code</td>
										<td>Name</td>
										<td>Contact Person</td>
										<td>Phone Number</td>
										<td>Email Id</td>
										<td>Type</td>
										<td>Active?</td>
									</tr>
									</thead>
									<tbody>
									@foreach ($data['venues'] as $venue)

										<tr class="v-t-{{ $venue->id }}">
											<td>{{ $venue->code }}</td>
											<td>
												<a href="/venues/view/{{ $venue->id }}"
														class="venue-link">{{ $venue->name }}</a>
											</td>
											<td>{{ $venue->person }}</td>
											<td>{{ $venue->phone }}</td>
											<td>{{ $venue->email }}</td>
											<td>{{ $venue->type }}</td>
											<td>
												@if ($venue->is_live == 1)
													<span class="text-success"><i
																class="glyphicon glyphicon-ok-sign"></i> Yes</span>
												@else
													<span class="text-danger"><i
																class="glyphicon glyphicon-remove-sign"></i> No</span>
												@endif
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {

			if (window.location.search.indexOf('new=true') > -1) {
				if (!alert('A new vendor has been created')) {
					window.location = "/venues";
				}
			}

			if (window.location.search.indexOf('delete=true') > -1) {
				if (!alert('Vendor has been deleted')) {
					window.location = "/venues";
				}
			}

			$('#query').focus();

			$('#city').change(function (event) {
				event.preventDefault();
				location.href = '/venues/list?city=' + $(this).val();
			});

			$('#btnSearchSubmit').click(function (event) {
				event.preventDefault();

				var city = $('#city').val();
				location.href = "/venues/list?city=" + city + '&query=' + $('#query').val();

			});
		});

	</script>
@stop