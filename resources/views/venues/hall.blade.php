@extends('layout.base')

@section('title')
	<title>Hall {{ $venue->name }} - Details | Evibe Dash</title>
@stop

@section('content')
	<div class="container page-content packages-warp">
		<div class="mar-b-10">
			<a href="{{ route('venues') }}"><< Show all Halls</a>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<div class="panel-heading">
					<div class="inner">
						<div class="pull-left">
							<h3 class="panel-title">{{ $venue->name }}</h3>
						</div>
						<div class="pull-right">
							<a class="btn btn-sm btn-info mar-r-10 iq"
									data-url="{{route('iq.add',[ 'mapId' => $venue->id,'mapTypeID' => config('evibe.ticket_type.venue_halls')])}}"
									data-redirect="{{route('venues.hall',$venue->id)}}">

								<i class="glyphicon glyphicon-plus"></i> Add IQ
							</a>
							<a href="{{ $venue->getLiveLink() }}" target="_blank"
									class="btn btn-primary mar-r-10" @if (!$venue->venue->is_live) disabled @endif>
								<i class="glyphicon glyphicon-hand-up"></i> View Live
							</a>
							@if (AppUtil::isTabVisible('delete_venues'))
								<a href="{{ route('venues.hall.delete',$venue->id) }}"
										onClick="return confirm('Do you really want to delete this hall?');"
										class="btn btn-danger">
									<i class="glyphicon glyphicon-trash"></i> Delete</a>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="panel-body">
					<div class="venue-profile-data">
						<div class="text-center">
							<div class="alert @if ($venue->venue->is_live) alert-success @else alert-danger @endif venue-status">
								<label> </label>
								@if ($venue->venue->is_live) Active @else InActive @endif
								<span style="margin-left:1%">[Venue: <a
											href="{{ route('venues.view', $venue->venue->id) }}">{{ $venue->venue->name }}</a>]</span>
							</div>
						</div>
						<section class="data-sec pad-b-10">

							<!--Internal question start -->
							<div class="col-lg-13">
								<div class="iq-container">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="no-mar">Internal Questions</h4>
										</div>
										<div class="panel-body">
											@if ($iqs->count())
												<ul class="iq-list no-mar no-pad">
													@foreach($iqs as $iq)
														<li class="@if ($iq->deleted_at) strike @endif">
															<div class="pull-left">
																<div class="iq-q">{!! $iq->question !!}</div>
																<div class="iq-ans">{!! $iq->answer !!}</div>
																<div class="iq-info">
																	<i>{{ date('j F, g:i a',strtotime($iq->created_at)) }} by
																		<b>{{ $iq->user ? $iq->user->name:"--" }}</b>
																		@if ($iq->deleted_by)
																			Deleted by
																			<b>{{  $iq->deletedBy->username }}</b>
																		@endif
																	</i>
																</div>
															</div>
															<div class="pull-right">
																@if($iq->map_type_id == config('evibe.ticket_type.venues'))
																	<div class="label label-warning font-13">Venue question</div>
																@else
																	@if (!$iq->deleted_at)
																		<button class=" btn btn-sm btn-primary in-blk iq-edit"
																				data-url="{{route('iq.edit',$iq->id)}}"
																				data-redirect="{{route('venues.hall',$venue->id)}}"
																				data-question="{!! $iq->question !!}"
																				data-id="{{$iq->id}}">
																			<i class="glyphicon glyphicon-edit"></i>

																		</button>
																		<button class=" btn btn-sm btn-danger in-blk iq-delete"
																				data-url="{{route('iq.delete',$iq->id)}}">
																			<i
																					class="glyphicon glyphicon-trash"></i>
																		</button>
																	@endif
																@endif
															</div>
															<div class="clearfix"></div>
														</li>
													@endforeach
												</ul>
											@else
												<div class="text-danger">No internal questions were added yet. Click on
													<b>Add IQ</b> button to create first internal question.
												</div>
											@endif
										</div>
									</div>
								</div>
							</div>

							<!--Internal questions end-->
						</section>
						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Contact Information</h4>
							<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Name</label></td>
										<td>@if($venue->name) {{ $venue->name }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Primary Person</label></td>
										<td>@if($venue->venue->person) {{ $venue->venue->person }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Primary Number</label></td>
										<td>@if ($venue->venue->phone) {{ $venue->venue->phone }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Primary Email Id</label></td>
										<td>@if ($venue->venue->email) {{ $venue->venue->email }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Landline</label></td>
										<td>@if ($venue->venue->landline) {{ $venue->venue->landline }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Website</label></td>
										<td>@if ($venue->venue->website) {{ $venue->venue->website }} @else --- @endif</td>
									</tr>
									<tr>
										<td><label>City</label></td>
										<td>@if($venue->venue->area->name) {{ $venue->venue->area->name }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Evibe Code</label></td>
										<td>@if($venue->code) {{ $venue->code }} @else -- @endif</td>
									</tr>

								</table>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Alt Person</label></td>
										<td>@if($venue->venue->alt_person) {{ $venue->venue->alt_person }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Alt Number</label></td>
										<td>@if($venue->venue->alt_phone) {{ $venue->venue->alt_phone }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Alt Email Id</label></td>
										<td>@if($venue->venue->alt_email) {{ $venue->venue->alt_email }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Address</label></td>
										<td>@if($venue->venue->full_address) {{ $venue->venue->full_address }}@else -- @endif</td>
									</tr>
									<tr>
										<td><label>Landmark</label></td>
										<td>@if($venue->venue->landmark) {{ $venue->venue->landmark }}@else -- @endif</td>
									</tr>
									<tr>
										<td><label>Zip</label></td>
										<td>@if($venue->venue->zip) {{ $venue->venue->zip }}@else -- @endif</td>
									</tr>
									<tr>
										<td><label>Area</label></td>
										<td>@if($venue->venue->area->name) {{ $venue->venue->area->name }} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Map Co-ordinates</label></td>
										<td>@if($venue->venue->map_lat) {{ $venue->venue->map_lat }} @else -- @endif, @if($venue->venue->map_long) {{ $venue->venue->map_long }}@else -- @endif</td>
									</tr>
								</table>
							</div>
							<div class="clearfix"></div>
						</section>
						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Food &amp; Beverage</h4>
							<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Cusines</label></td>
										<td>@if($venueCuisine) {!! $venueCuisine !!} @else -- @endif</td>
									</tr>
									<tr>
										<td><label>Pure Veg</label></td>
										<td>@if ($venue->venue->is_pure_veg)Yes @else No @endif</td>
									</tr>
									<tr>
										<td><label>Tax Percent</label></td>
										<td>@if($venue->venue->tax_percent) {{ $venue->venue->tax_percent }}% @else -- @endif</td>
									</tr>
								</table>

								<div class="alert alert-success">
									<h4 class="text-danger">Event Supported*</h4>

									@if(count($eventSupported) > 0)
										@foreach($eventSupported as $event)
											<div class="form-control-static in-blk font-12 label-event">{{$event->event->name}}</div>
										@endforeach
									@else
										<div class="in-blk col-lg-11 no-pad-l">
											<i>&nbsp;Not Available</i>
										</div>
									@endif
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Veg Price</label></td>
										<td>
											<span class="rupee-font">&#8377;</span>

											@if($venue->venue->price_min_veg)
												<del style="margin-right:5%">{{ $venue->venue->min_veg_worth }} </del> {{ $venue->venue->price_min_veg }} onwards @else -- @endif
										</td>
									</tr>
									<tr>
										<td><label>Non-Veg Price</label></td>
										<td>
											<span class="rupee-font">&#8377;</span>

											@if($venue->venue->price_min_nonveg)
												<del style="margin-right:5%">{{ $venue->venue->min_nonveg_worth }} </del> {{ $venue->venue->price_min_nonveg }} onwards @else -- @endif
										</td>
									</tr>
									<tr class="@if ($venue->venue->is_alcohol_served) success @else danger @endif">
										<td><label>Alcohol Served</label></td>
										<td>@if ($venue->venue->is_alcohol_served)Yes @else No @endif</td>
									</tr>
									<tr class="@if ($venue->venue->has_food_sampling) success @else danger @endif">
										<td><label>Has Food Sampling</label></td>
										<td>@if ($venue->venue->has_food_sampling)Yes @else No @endif</td>
									</tr>
									<tr>
										<td><label>Food Sampling Comments</label></td>
										<td>@if($venue->venue->food_sampling_comments) {{ $venue->venue->food_sampling_comments }} @else -- @endif</td>
									</tr>
								</table>
							</div>
							<div class="clearfix"></div>
						</section>
						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Facilities &amp; Equipment</h4>
							<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Capacity Range</label></td>
										<td>@if($venue->cap_min){{ $venue->cap_min }} @else -- @endif - @if($venue->cap_max){{ $venue->cap_max }}@else -- @endif @if($venue->cap_float) (Float: {{ $venue->cap_float }}) @else -- @endif</td>
									</tr>
									<tr class="@if ($venue->venue->has_valet_parking) success @else danger @endif">
										<td><label>Valet Parking</label></td>
										<td>@if ($venue->venue->has_valet_parking)Yes @else No @endif</td>
									</tr>
									<tr>
										<td><label>Car Park Count</label></td>
										<td>@if($venue->venue->car_park_count){{ $venue->venue->car_park_count }} @else -- @endif</td>
									</tr>
									<tr class="@if ($venue->venue->has_lift) success @else danger @endif">
										<td><label>Has Lift</label></td>
										<td>@if ($venue->venue->has_lift)Yes @else No @endif</td>
									</tr>
								</table>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6">
								<table class="table table-striped table-bordered">
									<tr class="@if ($venue->venue->has_sound_system) success @else danger @endif">
										<td><label>Sound System?</label></td>
										<td>
											@if ($venue->venue->has_sound_system)
												Yes (
												@if ($venue->venue->price_sound_system)
													+
													<span class="rupee-font">&#8377;</span> {{ $venue->venue->price_sound_system }}
												@else free
												@endif
											)
											@else
												No
											@endif
										</td>
									</tr>
									<tr class="@if ($venue->venue->has_mike) success @else danger @endif">
										<td><label>Mike?</label></td>
										<td>
											@if ($venue->venue->has_mike)
												Yes (
												@if ($venue->venue->price_mike)
													+
													<span class="rupee-font">&#8377;</span> {{ $venue->venue->price_mike }}
												@else free
												@endif
											)
											@else
												No
											@endif
										</td>
									</tr>
									<tr class="@if ($venue->venue->has_projector) success @else danger @endif">
										<td><label>Projector?</label></td>
										<td>
											@if ($venue->venue->has_projector)
												Yes (
												@if ($venue->venue->price_projector)
													+
													<span class="rupee-font">&#8377;</span> {{ $venue->venue->price_projector }}
												@else free
												@endif
											)
											@else
												No
											@endif
										</td>
									</tr>
									<tr class="@if ($venue->venue->is_outside_decors_allowed) success @else danger @endif">
										<td><label>Outside decors allowed?</label></td>
										<td>
											@if ($venue->venue->is_outside_decors_allowed)
												Yes (
												@if ($venue->venue->price_outside_decors_allowance)
													+
													<span class="rupee-font">&#8377;</span> {{ $venue->venue->price_outside_decors_allowance }}
												@else no charges
												@endif
											)
											@else
												No
											@endif
										</td>
									</tr>
								</table>
							</div>
							<div class="clearfix"></div>
						</section>
						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Food Price</h4>
							<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Min Veg Price </label></td>
										<td>
											@if($venue->price_min_veg && $venue->price_min_veg >0 ) <span
													class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->price_min_veg)}}</span>
											@elseif($venue->venue->price_min_veg && $venue->venue->price_min_veg>0)
												<span class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->venue->price_min_veg)}}</span>
											@else -- @endif
										</td>
									</tr>
									<tr>
										<td><label>Max Veg Price </label></td>
										<td>
											@if($venue->price_max_veg && $venue->price_max_veg >0 ) <span
													class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->price_max_veg)}}</span>
											@else -- @endif
										</td>
									</tr>
									<tr>
										<td><label>Worth Veg Price </label></td>
										<td>
											@if($venue->price_worth_veg && $venue->price_worth_veg >0 ) <span
													class="rupee-font text-strike">&#8377; {{AppUtil::formatPrice($venue->price_worth_veg)}}</span>
											@elseif($venue->venue->min_veg_worth && $venue->venue->min_veg_worth >0)
												<span class="rupee-font text-strike">&#8377; {{AppUtil::formatPrice($venue->venue->min_veg_worth)}}</span>
											@else -- @endif
										</td>
									</tr>
								</table>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Min Non-Veg Price </label></td>
										<td>
											@if($venue->price_min_nonveg && $venue->price_min_nonveg >0 ) <span
													class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->price_min_nonveg)}}</span>
											@elseif($venue->venue->price_min_nonveg && $venue->venue->price_min_nonveg>0)
												<span class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->venue->price_min_nonveg)}}</span>
											@else -- @endif
										</td>
									</tr>
									<tr>
										<td><label>Max Non-Veg Price </label></td>
										<td>
											@if($venue->price_max_nonveg && $venue->price_max_nonveg >0 ) <span
													class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->price_max_nonveg)}}</span>
											@else -- @endif
										</td>
									</tr>
									<tr>
										<td><label>Worth Non-Veg Price </label></td>
										<td>
											@if($venue->price_worth_nonveg && $venue->price_worth_nonveg >0 ) <span
													class="rupee-font text-strike">&#8377; {{AppUtil::formatPrice($venue->price_worth_nonveg)}}</span>
											@elseif($venue->venue->min_nonveg_worth && $venue->venue->min_nonveg_worth >0)
												<span class="rupee-font text-strike">&#8377; {{AppUtil::formatPrice($venue->venue->min_nonveg_worth)}}</span>
											@else -- @endif
										</td>
									</tr>
								</table>
							</div>
							<div class="clearfix"></div>
						</section>
						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Hall Rent & Duration</h4>
							<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Min Rent </label></td>
										<td>
											@if($venue->rent_min && $venue->rent_min >0 ) <span
													class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->rent_min)}}</span>
											@elseif($venue->venue->price_min_rent && $venue->venue->price_min_rent>0)
												<span class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->venue->price_min_rent)}}</span>
											@else -- @endif
										</td>
									</tr>
									<tr>
										<td><label>Max Rent </label></td>
										<td>
											@if($venue->rent_max && $venue->rent_max >0 ) <span
													class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->rent_max)}}</span>
											@elseif($venue->venue->price_max_rent && $venue->venue->price_max_rent>0)
												<span class="rupee-font">&#8377; {{AppUtil::formatPrice($venue->venue->price_max_rent)}}</span>
											@else -- @endif
										</td>
									</tr>
									<tr>
										<td><label>Worth Rent </label></td>
										<td>
											@if($venue->rent_worth && $venue->rent_worth >0 ) <span
													class="rupee-font text-strike">&#8377; {{AppUtil::formatPrice($venue->rent_worth)}}</span>
											@elseif($venue->venue->worth_rent && $venue->venue->worth_rent >0)
												<span class="rupee-font text-strike">&#8377; {{AppUtil::formatPrice($venue->venue->worth_rent)}}</span>
											@else -- @endif
										</td>
									</tr>
									<tr>
										<td><label>Min Rent Duration (hrs) </label></td>
										<td>
											@if($venue->min_rent_duration && $venue->min_rent_duration >0 ) {{$venue->min_rent_duration}}
											@elseif($venue->venue->min_rent_duration && $venue->venue->min_rent_duration >0)
												{{$venue->venue->min_rent_duration}}
											@else -- @endif
										</td>
									</tr>
								</table>
							</div>
							<div class="clearfix"></div>
						</section>
						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Menu</h4>
							<!-- Menu list begin -->
							<div class="pad-10">
								@if ($venueMenuDetails->count())
									@foreach ($venueMenuDetails as $venueMenu)
										<div class="col-md-4 col-lg-4 col-sm-4">
											<img style="height: 200px;max-width: 100%;" class="profile-img"
													src="{{ $venueMenu->getVenueMenuPic($venue->venue_id, $venueMenu->id) }}">
											<div class="pad-t-10">
												{{ $venueMenu->name }}
												<div>Rs. @if($venueMenu->worth)
														<strike>{{ $venueMenu->worth }}</strike> @else -- @endif @if($venueMenu->price) Rs.{{ $venueMenu->price }} @else -- @endif
												</div>
												<div>@if($venueMenu->min_guests){{ $venueMenu->min_guests }} Guests @else -- @endif</div>
											</div>
										</div>
									@endforeach

								@else
									<div class="alert alert-danger">
										No results found.
									</div>
								@endif
							</div>
							<!-- Menu list end -->

							<div class="clearfix"></div>
						</section>

						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Other</h4>
							<div class="col-sm-6 col-md-6 col-lg-6 no-pad-l">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Description</label></td>
										<td>@if($venue->venue->info){!! $venue->venue->info !!}@else -- @endif</td>
									</tr>
								</table>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6">
								<table class="table table-striped table-bordered">
									<tr>
										<td><label>Terms</label></td>
										<td>@if($venue->venue->terms){!! $venue->venue->terms !!}@else -- @endif</td>
									</tr>
								</table>
							</div>
							<div class="clearfix"></div>
						</section>
						<section class="data-sec pad-b-20">
							<h4 class="text-danger">Gallery Images</h4>
							<!-- Gallery Images -->
							<div class="pad-10">
								@if ($venueHallGallery->count())

									<?php $venueHallGallery = $venueHallGallery[0]; ?>

									@foreach($venueHallGallery->getHallGalleryPic($venue->id, $venue->venue_id) as $imgDetails)

										<div class="col-md-3 col-lg-3 col-sm-3">
											<div>
												<img style="float:left;height: 200px;max-width:100%;margin-top:10%;"
														class="profile-img" src="{{$imgDetails['url']}}">
												<br>
												<a href="{{$imgDetails['url']}}"
														target='_blank'>@if($imgDetails['type']=='hall') Hall- {{$imgDetails['title']}} @else Venue-{{$imgDetails['title']}} @endif</a>
											</div>
										</div>
									@endforeach
								@else
									<div class="alert alert-danger">
										No Images found.
									</div>
								@endif
							</div>
							<!-- Gallery Images -->
							<div class="clearfix"></div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop
