@extends('layout.base')

@section('title')
	<title>List of Halls | EvibeDash</title>
@stop

@section('content')

	<div class="container page-content packages-warp">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="title-sec pad-t-10">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<div>
							<div class="pull-left">
								<h2 class="panel-title">Showing list of all Halls</h2>
							</div>
							<div class="pull-right">
								<select name="city" id="city" class="form-control">
									@foreach($data['cities'] as $city)
										<option value="{{ $city->id }}"
												@if(request()->input('city')== $city->id) selected @endif>{{ $city->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="pad-t-20">

							<!-- search from begin -->
							<div class="pull-left">
								<form id="searchForm" role="form" class="form-inline form form-horizontal">
									<div class="in-blk mar-r-10">
										<input id="searchQuery" type="text" name="query"
												class="form-control u-seach-box"
												placeholder="Search venue by code, name"
												value="{{ Input::get('query') }}"/>
									</div>

									<div class="in-blk">
										<input type="submit" class="btn btn-info" value="Search">
										@if (Input::has('query') && Input::get('query'))
											<a id="resetSearch" class="pad-l-3">Clear search</a>
										@endif
									</div>
								</form>

							</div>
							<!-- search form end -->

							<!-- Show venue end -->
							<div class="pull-right">

								<a href="{{ route('venues.list') }}" class="btn btn-info  in-blk">
									<i class="glyphicon glyphicon-stats "></i> Show All Venues
								</a>
							</div>
							<!-- show venue end -->

							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="pad-t-30">

					<!--  Filter options begin -->
					<div class="col-sm-3 col-md-3 col-lg-3">
						<div class="panel panel-default halls-filter-options">
							<div class="panel-heading">
								<h5 class="panel-title pull-left">Filter Options</h5>
								@if(Input::has('venueType') || Input::has('hallType') || Input::has('priceRange') || Input::has('capacityRange') || Input::has('location'))
									<div class="pull-right">
										<a class="btn btn-xs btn-warning" href="{{ route('venues') }}">Reset Filters</a>
									</div>
								@endif
								<div class="clearfix"></div>
							</div>
							<div class="panel-body">
								<form id="venueHallsFilterForm">
									<div class="form-group">
										<div>
											<div class="text-center">
												<a class="btn btn-danger btn-sm mar-l-20  btn-submit mar-b-10">SEARCH</a>
											</div>
											<label class="filter-title text-center">
												<i class="glyphicon glyphicon-map-marker"></i> Location
											</label>
										</div>
										<ul id="locTags">
											@foreach($data['prefilledLocs'] as $oldLoc)
												<li>{{ $oldLoc }}</li>
											@endforeach
										</ul>
									</div>
									<div class="form-group">
										<label class="filter-title">
											<i class="glyphicon glyphicon-user"></i> Capacity
										</label>
										<div>
											<div class="col-md-6 no-pad-l">
												<input type="text" name="capacityRangeMin" class="form-control width-40"
														placeholder="Min."
														value="@if(!is_null($data['capMinSelected'])){{ $data['capMinSelected'] }}@endif">
											</div>
											<div class="col-md-6 no-pad-r text-center">
												<input type="text" name="capacityRangeMax" class="form-control width-40"
														placeholder="Max."
														value="@if(!is_null($data['capMaxSelected'])){{ $data['capMaxSelected'] }}@endif">
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="filter-title">
											<span class="rupee-font">&#8377;</span> Price
										</label>
										<div>
											<div class="col-md-6 no-pad-l text-center">
												<input id="priceRangeMin" type="text" name="priceRangeMin"
														class="form-control" placeholder="Min."
														value="@if(!is_null($data['priceMinSelected'])){{ $data['priceMinSelected'] }}@endif">
											</div>
											<div class="col-md-6 no-pad-r text-center">
												<input id="priceRangeMax" type="text" name="priceRangeMax"
														class="form-control" placeholder="Max."
														value="@if(!is_null($data['priceMaxSelected'])){{ $data['priceMaxSelected'] }}@endif">
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="venueType" class="filter-title text-muted"></label>
										<i class="glyphicon glyphicon-th-large"></i> Venue Type
										<div class="price-options">
											@foreach ($data['venueType'] as $typeVenue)
												<div>
													<label class="filter-tag-value">
														<input type="checkbox" name="venueType"
																value="{{ $typeVenue->id }}"
																@if (in_array($typeVenue->id, $data['venueTypeSelected'])) checked="checked" @endif>
														<span>{{ $typeVenue->name }}</span>
													</label>
												</div>
											@endforeach
										</div>
									</div>
									<div class="form-group">
										<label for="venueType" class="filter-title">
											<i class="glyphicon glyphicon-th-large"></i>Hall Type
										</label>
										<div class="price-options">
											@foreach ($data['typeVenueHall'] as $typeVenueHall)
												<div>
													<label class="filter-tag-value">
														<input type="checkbox" name="hallType"
																value="{{ $typeVenueHall->id }}"
																@if (in_array($typeVenueHall->id, $data['hallTypeSelected'])) checked="checked" @endif>
														<span>{{ $typeVenueHall->name }}</span>
													</label>
												</div>
											@endforeach
										</div>
									</div>
									<div class="form-group">
										<a class="btn btn-danger btn-sm btn-block btn-submit">SEARCH</a>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- Filter options end -->

					<!-- Venue halls results begin -->
					<div class="col-sm-9 col-md-9 col-lg-9">

						@if(count($data['venueHall']) == 0)
							<div class="alert alert-danger">
								<span>Sorry, no Halls were found.</span>
							</div>
						@else
							<ul class="ls-none no-mar no-pad venue-hall-results">
								@foreach ($data['venueHall'] as $venueHall)
									<li class="a-venue-hall">
										<div class="pad-10">
											<div class="top">
												<div class="pull-left partner-text text-bold">
													@if ($venueHall->venue->is_partner)
														<span class="text-success">
														<i class="glyphicon glyphicon-thumbs-up"></i> Partner
													</span>
													@else
														<span class="text-danger">
														<i class="glyphicon glyphicon-thumbs-down"></i> Not Partner
													</span>
													@endif
												</div>
												<div class="pull-right">
													<a href="{{ $venueHall->getLiveLink() }}"
															class="btn btn-warning btn-xs"
															target="_blank"> View Live</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="pad-t-3">
												<a href="{{ route('venues.hall', $venueHall->id) }}"
														class="title">[{{ $venueHall->code }}] @if($venueHall->type){{ $venueHall->type->name }}@endif</a>
											</div>
											<div class="pad-t-3">
												<i class="glyphicon glyphicon-user"></i>
												<span> {{ $venueHall->cap_min }} - {{ $venueHall->cap_max}}</span>
												@if ($venueHall->cap_float && $venueHall->cap_max != $venueHall->cap_float)
													(float: {{ $venueHall->cap_float }})
												@endif
											</div>
											<div class="pad-t-3">
												<i class="glyphicon glyphicon-usd"></i> Veg:
												@if ($venueHall->venue->min_veg_worth)
													<del>
														<span class="rupee-font">&#8377;</span> {{ $venueHall->venue->min_veg_worth }}
													</del>
												@endif
												<span class="rupee-font pad-l-3">&#8377;</span> {{ $venueHall->venue->price_min_veg }} @if ($venueHall->venue->tax_percent) + taxes @endif
												@if ($venueHall->venue->is_pure_veg)
													<div class="pad-l-18">(pure veg)</div>
												@else
													<div class="pad-l-18">
														Non-Veg:
														@if ($venueHall->venue->min_nonveg_worth)
															<del>
																<span class="rupee-font">&#8377;</span> {{ $venueHall->venue->min_nonveg_worth }}
															</del>
														@endif
														<span class="rupee-font pad-l-3">&#8377;</span> {{ $venueHall->venue->price_min_nonveg }} @if ($venueHall->venue->tax_percent) + taxes @endif
													</div>
												@endif
											</div>
											<div class="pad-t-3">
												<i class="glyphicon glyphicon-map-marker"></i> {{ $venueHall->venue->area->name }}
											</div>

										</div>
										<div class="venue-name">
											<a href="{{ route('venues.view', $venueHall->venue_id) }}" target="_blank">
												<i class="glyphicon glyphicon-home"></i> {{ $venueHall->venue->name }}
											</a>
										</div>
									</li>
								@endforeach
							</ul>

							<!-- pagination begin -->
							<div class="text-center">
								{{ $data['venueHall']->appends(Input::except('page'))->links() }}
							</div>
							<!-- pagination end -->

						@endif
					</div>
					<!-- venue results end -->

					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

@stop
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			// show comments in a modal
			$('.a-venue-hall .comments').click(function (event) {
				event.preventDefault();
				window.showGenericModal("Our Comments", $(this).data('text'), false);
			});

			// search results
			$('#searchForm').submit(function (event) {
				event.preventDefault();
				var searchQuery = $('#searchQuery').val();
				window.location = formUrl('query', searchQuery);

			});

			var availableLocs = <?php echo json_encode($data['locationsList']); ?>;
			$("#locTags").tagit({
				autocomplete: {delay: 0, minLength: 0},
				readOnly: false,
				availableTags: availableLocs,
				showAutocompleteOnFocus: true,
				beforeTagAdded: function (event, ui) {
					if (availableLocs.indexOf(ui.tagLabel) == -1) {
						return false;
					}
					if (ui.tagLabel == "not found") {
						return false;
					}
				}
			});

			/**** filter code starts ****/

				// submit filter options
			$('#venueHallsFilterForm .btn-submit').click(function (event) {

				event.preventDefault();

				var query = {
					'priceRange': '',
					'location': '',
					'capacityRange': '',
					'hallType': '',
					'venueType': ''
				};

				var Price = Price;

				// venue Type
				$('input[name="venueType"]:checked').each(function () {
					query['venueType'] += $(this).val() + ',';
				});
				// hall Type
				$('input[name="hallType"]:checked').each(function () {
					query['hallType'] += $(this).val() + ',';
				});
				// price range
				$('input[name="priceRangeMin"]').each(function () {
					query['priceRange'] += $(this).val() + ',';
				});
				// price range
				$('input[name="priceRangeMax"]').each(function () {
					query['priceRange'] += $(this).val() + ',';
				});
				// capacity range
				$('input[name="capacityRangeMin"]').each(function () {
					query['capacityRange'] += $(this).val() + ',';
				});
				// capacity range
				$('input[name="capacityRangeMax"]').each(function () {
					query['capacityRange'] += $(this).val() + ',';
				});
				// location
				if ($("#locTags").tagit("assignedTags") != '') {
					query['location'] = $("#locTags").tagit("assignedTags") + ',';
				}

				// update URL
				var loc = window.location.href;
				var queryParams = "";

				if (loc.indexOf("?") == -1) {
					loc += "?";
				}
				else {
					// get existing query params sets
					var existingQueryParams = loc.substr(loc.indexOf("?") + 1).split("&");
					var eqpNew = "";
					var existingQuerySet = {};
					var isExistingChanged = 0;

					for (var eqpKey in existingQueryParams) {
						var eqpSet = existingQueryParams[eqpKey].split("=");
						var eqp = eqpSet[0];

						// take only sort, leave priceRange, tags, success and page
						if (eqp == "sort") {
							eqpNew += existingQueryParams[eqpKey] + "&";
						}
						else {
							// save all the existing query paramas
							// with values to compare with the new ones
							existingQuerySet[eqp] = eqpSet[1];
						}
					}

					loc = loc.substr(0, loc.indexOf("?") + 1) + eqpNew;
				}

				for (key in query) {
					if (query.hasOwnProperty(key) && query[key]) {
						// remove trailing ","
						query[key] = query[key].substr(0, query[key].length - 1);

						// check if existing query params value has changed
						if (!existingQuerySet ||
								( existingQuerySet && ( (existingQuerySet.hasOwnProperty(key) && existingQuerySet[key] != query[key]) || !existingQuerySet.hasOwnProperty(key) )
								)
						) {
							isExistingChanged = 1;
						}

						queryParams += key + "=" + query[key] + "&";
					}
				}

				// set page number only if the existing query
				// params are exactly the same as new ones
				if (existingQuerySet &&
						existingQuerySet.hasOwnProperty("page") && !isExistingChanged) {
					queryParams += "page=" + existingQuerySet["page"];
				}

				// remove trailing "&"
				var cityId = $('#city').val() ? $('#city').val() : 1;
				queryParams = queryParams.substr(0, queryParams.length - 1);
				if (queryParams) window.location = loc + encodeURI(queryParams + "&city=" + cityId);
			});

			/***** filter code ends */

				// reset search
			$('#resetSearch').click(function (event) {
				event.preventDefault();
				window.location = formUrl('query', null);

			});

			function formUrl(currentParam, currentParamValue) {
				var loc = window.location;
				var url = loc.origin + loc.pathname + '?';
				var queryParams = loc.search.substr(1).split("&");
				var count = 0;
				var trimmedQueryParams = '';

				for (key in queryParams) {
					var cParam = queryParams[key].split("=");
					var qKey = cParam[0];
					var qVal = cParam[1];

					// form array
					if (!(currentParam instanceof Array)) currentParam = [currentParam];

					if (qKey && $.inArray(qKey, currentParam) == -1) {
						count++;
						trimmedQueryParams += queryParams[key] + '&';
					}
				}

				if (currentParamValue) url += currentParam + '=' + currentParamValue + '&';
				if (count > 0)  url += trimmedQueryParams;

				return url.substr(0, url.length - 1);
			}

		});

		$('#listenSlider').change(function () {
			$('.output b').text(this.value);
		});

		if (window.location.search.indexOf('delete=true') > -1) {
			if (!alert('venue has been deleted')) {
				window.location = "/venues";
			}
		}
		else if (window.location.search.indexOf('delete=false') > -1) {
			if (!alert('Problem with deleting venue')) {
				window.location = "/venues";
			}
		}
		else if (window.location.search.indexOf('inactive=true') > -1) {
			if (!alert('venue has been deactivated')) {
				window.location = "/venues";
			}
		}
		else if (window.location.search.indexOf('inactive=false') > -1) {
			if (!alert('Problem with deactivating venue')) {
				window.location = "/venues";
			}
		}
		else if (window.location.search.indexOf('active=true') > -1) {
			if (!alert('venue has been activated')) {
				window.location = "/venues";
			}
		}
		else if (window.location.search.indexOf('active=false') > -1) {
			if (!alert('Problem with activating venue')) {
				window.location = "/venues";
			}
		}
		else {
			// do nothing
		}

		$('#query').focus();

		$('#city').click(function (event) {
			event.preventDefault();

			location.href = '/venues?city=' + $(this).val();

		});

	</script>
@stop