@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
			<div class="app-tabs">
				@include('app/tabs')
			</div>
			<ol class="breadcrumb">
				<li><a href="/venues/list">Show All Venues</a></li>
			</ol>
			<div class="page-content vendors-wrap">
				<div class="vendors-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="pull-left">
								<h2 class="panel-title"> Add New Venues</h2>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							@if ($errors->count() > 0 || session()->has('custom_error'))
								<div class="errors-cnt alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
									<ul class="erros-list">
										@foreach ($errors->all() as $message)
											<li>{!! $message !!}</li>
										@endforeach
										@if (session()->has('custom_error'))
											<li>{{ Session::get('custom_error') }}</li>
										@endif
									</ul>
								</div>
							@endif
							<form action="/venues/create-venues" method="POST" role="form">
								<div class="alert alert-warning">
									<h4 class="text-center">Venue Information</h4>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Name</span>
												</label>
												<input id="venueName" name="venueName" type="text" class="form-control"
														placeholder="Enter venue name"
														value="{{ old('venueName') }}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Type</span>
												</label>
												<select name="typeVenue" id="typeVenue" class="form-control">
													@foreach ($data['typeVenue'] as $typeVenue)
														<option value="{{ $typeVenue->id }}">{{ AppUtil::formatHandler($typeVenue->name) }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label><span>Contact Person</span></label>
												<input id="venuePerson" name="venuePerson" type="text"
														class="form-control"
														placeholder="Enter contact person name"
														value="{{ old('venuePerson') }}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Phone Number</label>
												<input id="venuePhone" name="venuePhone" type="text"
														class="form-control"
														placeholder="Enter contact number"
														value="{{ old('venuePhone') }}">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>City</span>
												</label>
												<select name="city" id="city" class="form-control"
														data-old="@if(old('city')){{ old('city') }}@endif">
													@foreach ($data['cities'] as $city)
														<option value="{{ $city->id }}">{{ $city->name }}</option>
													@endforeach
												</select>
											</div>
										</div>

										<div class="col-sm-4 col-sm-4 col-md-4 col-lg-4">
											<div class="form-group">
												<label for="area">
													<span class="mar-r-10">Location</span>
													<a href="#" class="btn-add-loc text-normal"
															data-redirectto="{{ URL::route('venues.new') }}">Add New Location</a>
												</label>
												<select name="area" id="area" class="form-control"
														data-old="@if(old('area')){{ old('area') }}@endif">
													@foreach ($data['areas'] as $area)
														<option value="{{ $area->id }}">{{ $area->name }}</option>
													@endforeach
												</select>
												<ul id="allAreaOptions" class="hide">
													@foreach ($data['areas'] as $area)
														<li data-id="{{ $area->id }}"
																data-city_id="{{ $area->city_id }}"
																data-name="{{ $area->name }}"></li>
													@endforeach
												</ul>
											</div>
										</div>

										<div class="clearfix"></div>
									</div>
									<div>


										<div class="clearfix"></div>
									</div>
									<div>

									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Email 1</label>
												<input type="text" name="altEmail1" class="form-control"
														placeholder="Enter alternative email"
														value="@if(old('altEmail1')){{ old('altEmail1') }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Email 2</label>
												<input type="text" name="altEmail2" class="form-control"
														placeholder="Enter alternative email"
														value="@if(old('altEmail2')){{ old('altEmail2') }}@endif">
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Alt Phone</label>
												<input type="text" name="altPhone" class="form-control"
														placeholder="Enter alternative phone"
														value="@if(old('altPhone')){{ old('altPhone') }}@endif">
											</div>
										</div>
									</div>
									<div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Is Partner?</span>

												</label><br/>
												<label class="font-normal mar-r-10">
													<input type="radio" name="venueIsPartner" value="1"
															@if (old('venueIsPartner')) checked="checked" @endif> Yes
												</label>
												<label class="font-normal">
													<input type="radio" name="venueIsPartner" value="0"
															@if (!old('venueIsPartner')) checked="checked" @endif> No
												</label>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>
													<span>Is Verified?</span>

												</label>
												<br/>
												<label class="font-normal mar-r-10">
													<input type="radio" name="venueIsVerified" value="1"
															@if (old('venueIsVerified')) checked="checked" @endif> Yes
												</label>
												<label class="font-normal">
													<input type="radio" name="venueIsVerified" value="0"
															@if (!old('venueIsVerified')) checked="checked" @endif> No
												</label>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="alert alert-info">
									<h4 class="text-center">For EvibePro</h4>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<label>
												<span>Username</span>
											</label>
											<input id="venueUsername" name="venueUsername" type="text"
													class="form-control"
													placeholder="Enter valid email address"
													value="{{ old('venueUsername') }}">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<label>
												<span>Password</span>
											</label>
											<input id="venuePassword" name="venuePassword" type="password"
													class="form-control"
													placeholder="Min 6 characters"
													value="{{ old('venuePassword') }}">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<label>
												<span>Confirm Password</span>
											</label>
											<input id="venuePassword_confirmation" name="venuePassword_confirmation"
													type="password" class="form-control" placeholder="Confirm password">
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div>
									<div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-center">
										<div class="form-group">
											<input type="submit" value="Submit" class="btn btn-primary"/>
											<a class="pad-l-10" href="/venues">Cancel</a>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {
			// add new location
			$('.btn-add-loc').click(function (event) {

				event.preventDefault();

				$('#addNewAreaModal').data('redirectto', $(this).data('redirectto'));
				$('#addNewAreaModal').modal();

			});
			// refresh option of city
			function initCityAreaSelect() {
				var areaValue,
						$selectArea,
						$area = $('#area'),
						$city = $('#city');

				$city.selectize({
					onChange: onSelectCityChange
				});

				$selectArea = $area.selectize({
					valueField: 'id',
					labelField: 'name',
					searchField: ['id', 'name']
				});
				areaValue = $selectArea[0].selectize;
				if (!$area.val()) {
					areaValue.disable();
				}

				function onSelectCityChange(value, isManual) {
					var results = [];
					areaValue.disable();
					areaValue.clearOptions();
					if (!value.length || value == -1) {
						return false;
					}

					$('#allAreaOptions').find('li').each(function (count) {
						if ($(this).data('city_id') && $(this).data('city_id') == value) {
							results.push({
								id: $(this).data('id'),
								name: $(this).data('name')
							});
						}
					});

					areaValue.enable();
					areaValue.addOption(results);

					var old = $.trim($area.data('old')) && $city.data('old') == $city.val() ? $.trim($area.data('old')) : results[0].id;
					areaValue.setValue(old);
				}

				if ($city.val()) {
					onSelectCityChange($city.val(), true)
				}

				// select default city

				if ($city.data('old')) {
					$city[0].selectize.setValue($.trim($city.data('old')))
				}
			}

			initCityAreaSelect();

		});

	</script>
@stop