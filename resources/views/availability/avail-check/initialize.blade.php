@extends('availability.avail-check.base')

@section("title")
	<title>Partner Availability Check</title>
@endsection

@section('sub-content')
	<div class="alert alert-danger">
		<span>Please select a partner to continue.</span>
	</div>
@endsection