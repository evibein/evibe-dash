@extends('availability.avail-check.base')

@section("title")
	<title>Partner Availability Check</title>
@endsection

@section('custom-head')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.css"/>
@endsection

@section('sub-content')
	<div class="text-center">
		@if((isset($data["availCheckData"]) && count($data["availCheckData"]) > 0) || (isset($data["ticketData"]) && count($data["ticketData"]) > 0))
			@php
				$availCheckData = isset($data["availCheckData"]) && count($data["availCheckData"]) > 0 ? $data["availCheckData"] : [];
				$ticketData = isset($data["ticketData"]) && count($data["ticketData"]) > 0 ? $data["ticketData"] : [];
				$allDates = array_unique(array_merge(array_keys($availCheckData), array_keys($ticketData)));
				$selectedPartnerCode = isset($data["selectedCode"]) ? $data["selectedCode"] : "";
				$availCheckStatus = config("evibe.avail_check.slots");
			@endphp
			<div class="mar-t-20">
				<table id="returnTicketsSummary" class="table table-bordered table-responsive table-hover text-left">
					<thead class="text-info">
					<tr>
						<th>Date</th>
						<th>Slots Booked (Partner Unavailable)</th>
					</tr>
					</thead>
					<tbody>
					@foreach($allDates as $date)
						<tr>
							<td>
								<div class="text-white font-1">[{{ date('y/m/d', strtotime($date)) }}]</div>
								{{ date("d M Y", strtotime($date)) }}
							</td>
							<td>
								@if(isset($availCheckData[$date]))
									@foreach ($availCheckData[$date] as $dateSlot)
										@if(isset($availCheckStatus[$dateSlot]))
											<div class="alert alert-info in-blk mar-l-5 pad-8 no-mar-b">
												{{ $availCheckStatus[$dateSlot] }}
												@if($dateSlot != config("evibe.avail_check.ids.all"))
													<a class="btn btn-warning pad-5 mar-l-10 edit-unavailable-slot" data-slot="{{ $dateSlot }}" data-date="{{ date("d-m-Y", strtotime($date)) }}">
														<i class="glyphicon glyphicon-pencil"></i> Edit
													</a>
												@endif
												<a class="btn btn-danger pad-5" href="{{ route("avail.check.partner.delete.slot", [$selectedPartnerCode, $date, $dateSlot, "productId" => request("productId")]) }}"
														onclick="return confirm('Are you sure to delete this slot?','YES','NO')"><i class="glyphicon glyphicon-trash"></i></a>
											</div>
										@endif
									@endforeach
								@endif
								@if(isset($availCheckData[$date]) && isset($ticketData[$date]))
									<hr>
								@endif
								@if(isset($ticketData[$date]))
									@foreach ($ticketData[$date] as $dateSlot)
										@if(isset($availCheckStatus[$dateSlot]))
											<div class="alert alert-success in-blk mar-l-5 pad-8 no-mar-b">
												{{ $availCheckStatus[$dateSlot] }}
											</div>
										@endif
									@endforeach
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div id="showCalendar" class="pad-10"></div>
			</div>
		@else
			<div class="alert alert-success in-blk mar-t-10">Partner available in all the dates.</div>
		@endif
	</div>
@endsection

@section('javascript')
	@parent
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			(function initPlugins() {
				// initiate dataTables
				$('#returnTicketsSummary').DataTable({
					"fixedHeader": true,
					"lengthMenu": [[100, 10, 25, 50, -1], [100, 10, 25, 50, "All"]]
				});
			})();

		});
	</script>
@stop