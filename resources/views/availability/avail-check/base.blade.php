@extends('layout.base')

@section('content')
	@php
		$selectedPartnerCode = isset($data["selectedCode"]) ? $data["selectedCode"] : "";
	@endphp
	<div class="analytics-higher-wrap">
		<div class="mar-l-10 mar-r-10">
			<div class="text-center">
				<div class="avail-check-success alert alert-success hide in-blk mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-check"></i>
					Partner available on selected slot & date.
				</div>
				<div class="avail-check-failure alert alert-danger hide in-blk mar-l-10 mar-r-10 mar-b-10 pad-10">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<i class="glyphicon glyphicon-remove-sign"></i>
					<span class="avail-check-failure-text">Partner not available on this slot & date.</span>
				</div>
			</div>
			<div class="col-sm-3 no-pad-l pad-r-5 text-left">
				<label for="selectPartner">Select Partner</label>
				<select class="form-control" name="selectPartner" id="selectPartner">
					<option></option>
					@foreach($data["partnerData"] as $key => $value)
						<option @if($key == $selectedPartnerCode) selected @endif value="{{ $key }}">{{ $value }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-sm-3 no-pad-l pad-r-5 show-venue-options hide">
				<label for="selectPartnerPackage">Select Partner Package</label>
				<select class="form-control" name="selectPartnerPackage" id="selectPartnerPackage">
				</select>
			</div>
			<div class="col-sm-4 no-pad-l">
				<div class="col-sm-3 no-pad-l pad-r-5">
					<label for="checkAvailSlotDate">Select Date</label>
					<input type="text" class="form-control pad-l-5 no-pad-r" value="{{ date('d-m-Y', time()) }}" id="checkAvailSlotDate">
				</div>
				<div class="col-sm-4 no-pad-l pad-r-5">
					<label for="checkAvailSlot">Select Slot</label>
					<select class="form-control" name="checkAvailSlot" id="checkAvailSlot">
						<option value="2">Breakfast</option>
						<option value="3">Lunch</option>
						<option value="4">Hi Tea</option>
						<option value="5">Dinner</option>
					</select>
				</div>
				<div class="col-sm-5 in-blk mar-t-20 no-pad-l">
					<a class="btn btn-success check-availability-btn">Check Availability</a>
				</div>
			</div>
			<div class="col-sm-2 no-pad-r pull-right">
				<a class="btn btn-primary mar-t-20" id="addUnavailableSlot"><i class="glyphicon glyphicon-plus"></i> Add Unavailable Slot</a>
			</div>
			<div class="clearfix"></div>
			@yield("sub-content")
		</div>
	</div>
	<div id="partnerSlotUpdates" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Partner Unavailable Slot</h4>
				</div>
				<div class="modal-body">
					<div>
						<label for="partnerSlotDate">Select Date</label>
						<input type="text" class="form-control partner-slot-date" value="{{ date('d-m-Y', time()) }}" id="partnerSlotDate">
					</div>
					<div class='mar-t-20'>
						<span><b>Select Slot</b></span><br>
						<label class="checkbox partner-unavailable-slots in-blk mar-r-10 pad-t-8 no-mar-t"><input type="checkbox" name="partnerSlot[]" id="partnerSlot" value="1">All Slots</label>
						<label class="checkbox partner-unavailable-slots in-blk mar-r-10 pad-t-8"><input type="checkbox" name="partnerSlot[]" id="partnerSlot" value="2">Breakfast</label>
						<label class="checkbox partner-unavailable-slots in-blk mar-r-10 pad-t-8"><input type="checkbox" name="partnerSlot[]" id="partnerSlot" value="3">Lunch</label>
						<label class="checkbox partner-unavailable-slots in-blk mar-r-10 pad-t-8"><input type="checkbox" name="partnerSlot[]" id="partnerSlot" value="4">Hi Tea</label>
						<label class="checkbox partner-unavailable-slots in-blk mar-r-10 pad-t-8"><input type="checkbox" name="partnerSlot[]" id="partnerSlot" value="5">Dinner</label>
					</div>
					<div class="text-center mar-t-30">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success btn-unavailable-slot-submit">SUBMIT</button>
						<button type="button" class="btn btn-success btn-unavailable-slot-update">UPDATE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="redirectUrl" value="{{ route("avail.check.initialize") }}">
	<input type="hidden" id="partnerURLCode" value="{{ isset($data["selectedCode"]) ? $data["selectedCode"] : "" }}">
	<input type="hidden" id="venuePackageId" value="{{ isset($data["venuePackageId"]) ? $data["venuePackageId"] : "" }}">
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			var selectedDate = "";
			var selectedSlot = "";
			var partnerURLCode = $("#partnerURLCode").val();
			var venuePackageId = $("#venuePackageId").val();

			(function initFunctions() {
				/*Selectize for partner selection*/
				$("#selectPartner").selectize({
					onChange: onSelectPartnerChange
				});

				value = partnerURLCode.split("_");
				var partnerTypeId = value["0"];
				var partnerCode = value["1"];
				if (partnerTypeId.length > 0 && partnerTypeId == 3 && partnerCode.length > 1) {
					showLoading();
					loadVenuePackages(partnerTypeId, partnerCode);
				}

				/*date time picker for filters*/
				$('.partner-slot-date, #checkAvailSlotDate').datetimepicker({
					timepicker: false,
					scrollInput: false,
					scrollMonth: false,
					closeOnDateSelect: true,
					format: "d-m-Y"
				});
			})();

			(function modalActions() {
				$("#addUnavailableSlot").on("click", function () {
					if (partnerURLCode.length > 0) {
						$('input:checkbox').removeAttr('checked');
						$(".btn-unavailable-slot-submit").removeClass('hide');
						$(".btn-unavailable-slot-update").addClass('hide');
						$("#partnerSlotUpdates").modal("show");
					} else {
						showNotyError("Please select a partner to add unavailable slot.")
					}
				});

				$(".edit-unavailable-slot").on("click", function () {
					if (partnerURLCode.length > 0) {
						$('input:checkbox').removeAttr('checked');

						selectedDate = $(this).data("date");
						selectedSlot = $(this).data("slot");
						$("#partnerSlotDate").val(selectedDate);
						$(".partner-unavailable-slots").each(function () {
							if ($(this).find(':input').val() == selectedSlot) {
								$(this).find(':input').prop('checked', true);
							}
						});

						$(".btn-unavailable-slot-submit").addClass('hide');
						$(".btn-unavailable-slot-update").removeClass('hide');
						$("#partnerSlotUpdates").modal("show");
					} else {
						showNotyError("Please select a partner to add unavailable slot.")
					}
				});

				$(".partner-unavailable-slots").on("click", function () {
					if ($(this).find(':input').val() == 1) {
						$('input:checkbox').removeAttr('checked');
						$(this).find(':input').prop('checked', true);
					} else {
						$(".partner-unavailable-slots").each(function () {
							if ($(this).find(':input').val() == 1) {
								$(this).find(':input').prop('checked', false);
							}
						});
					}
				});
			})();

			(function submitSlot() {
				$(".btn-unavailable-slot-submit").on("click", function () {
					var selectedSlots = [];

					if (partnerURLCode.length > 0) {
						showLoadingScreen();

						$("#partnerSlot:checked").each(function () {
							selectedSlots.push($(this).val());
						});

						$.ajax({
							url: '/avail-check/add/' + partnerURLCode,
							dataType: 'json',
							type: 'POST',
							data: {'selectedSlots': selectedSlots, 'date': $("#partnerSlotDate").val(), 'venuePackageId': venuePackageId},
							success: function (data) {
								if (data.success) {
									hideLoading();
									showNotySuccess("Slots added successfully");
									window.location.reload();
								} else {
									showNotyError(data.error);
									showModalScreen();
								}
							},
							fail: function () {
								showModalScreen();
								showNotyError('Error occurred while submitting data, Please try again reloading the page.')
							}
						});
					} else {
						showNotyError("Please select at least one slot to continue");
					}
				});
			})();

			(function editSlots() {
				$(".btn-unavailable-slot-update").on("click", function () {
					var selectedSlots = [];

					if (partnerURLCode.length > 0) {
						$("#partnerSlot:checked").each(function () {
							selectedSlots.push($(this).val());
						});

						if (selectedSlots.length > 1) {
							showNotyError("Can't select more than one slot while editing.");
						} else {
							showLoadingScreen();

							$.ajax({
								url: '/avail-check/edit/' + partnerURLCode,
								dataType: 'json',
								type: 'POST',
								data: {'selectedSlots': selectedSlots, 'date': $("#partnerSlotDate").val(), 'selectedDate': selectedDate, 'selectedSlot': selectedSlot, 'venuePackageId': venuePackageId},
								success: function (data) {
									if (data.success) {
										hideLoading();
										showNotySuccess("Slots edited successfully");
										window.location.reload();
									} else {
										showModalScreen();
										showNotyError(data.error);
									}
								},
								fail: function () {
									showModalScreen();
									showNotyError('Error occurred while submitting data, Please try again reloading the page.')
								}
							});
						}
					} else {
						showNotyError("Please select at least one slot to continue");
					}
				});
			})();

			(function availabilityCheck() {
				$(".check-availability-btn").on("click", function () {
					if (partnerURLCode.length > 0) {
						showLoading();
						$(".avail-check-success").addClass("hide");
						$(".avail-check-failure").addClass("hide");

						$.ajax({
							url: '/avail-check/check/' + partnerURLCode,
							dataType: 'json',
							type: 'POST',
							data: {'date': $("#checkAvailSlotDate").val(), 'slot': $("#checkAvailSlot").val(), 'venuePackageId': venuePackageId},
							success: function (data) {
								if (data.success) {
									hideLoading();
									$(".avail-check-success").removeClass("hide");
								} else {
									hideLoading();
									$(".avail-check-failure-text").empty().append(data.error);
									$(".avail-check-failure").removeClass("hide");
								}
							},
							fail: function () {
								hideLoading();
								showNotyError('Error occurred while fetching data, Please try again reloading the page.')
							}
						});
					}
				});
			})();

			function showLoadingScreen() {
				showLoading();
				$("#partnerSlotUpdates").modal("hide");
			}

			function showModalScreen() {
				hideLoading();
				$("#partnerSlotUpdates").modal("show");
			}

			function onSelectPartnerChange(value) {
				value = value.split("_");

				var partnerTypeId = value["0"];
				var partnerCode = value["1"];
				if (partnerTypeId.length > 0 && partnerCode.length > 1) {
					if (partnerTypeId == 2) {
						$(".show-venue-options").addClass("hide");
						window.location = $("#redirectUrl").val() + "/" + partnerTypeId + "/" + partnerCode;
					} else if (partnerTypeId == 3) {
						showLoading();
						loadVenuePackages(partnerTypeId, partnerCode);
					}
				}
			}

			function loadVenuePackages(partnerTypeId, partnerCode) {
				var vars = [], hash;
				var q = document.URL.split('?')[1];

				if (q != undefined) {
					if (q.indexOf('#') > -1) {
						q = q.substring(0, q.indexOf('#'));
					}

					q = q.split('&');
					for (var i = 0; i < q.length; i++) {
						hash = q[i].split('=');
						vars.push(hash[1]);
						vars[hash[0]] = hash[1];
					}
				}
				var productId = typeof vars["productId"] !== "undefined" ? vars["productId"] : "";

				$.ajax({
					url: '/avail-check/venue/' + partnerTypeId + "/" + partnerCode,
					dataType: 'json',
					type: 'POST',
					data: {},
					success: function (data) {
						if (data.success) {
							var template = '<option value=""></option>';

							$.each(data.packages, function ($key, $package) {
								template += '<option ';
								if (productId > 0 && productId == $package.id) {
									template += ' selected ';
								}
								template += 'value=' + $package.id + '>' + $package.name + ' - ' + $package.code + '</option>';
							});

							// Remove the current selectize
							$('#selectPartnerPackage').selectize()[0].selectize.destroy();

							// Apply selectize for the new options
							$('#selectPartnerPackage').empty().append(template).selectize({
								onChange: function (value) {
									if (value && value > 0) {
										window.location = $("#redirectUrl").val() + "/" + partnerTypeId + "/" + partnerCode + "?productId=" + value;
									}
								}
							});

							hideLoading();
							$(".show-venue-options").removeClass("hide");
						}
					},
					fail: function () {
						showNotyError('Error occurred while submitting data, Please try again reloading the page.')
					}
				});
			}
		});
	</script>
@stop