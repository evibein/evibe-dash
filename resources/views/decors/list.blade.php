@extends('layout.base')

@section('title')
	<title>Decor styles list | EvibeDash</title>
@stop

@section('content')
	<div class="container page-content cakes-list-warp">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="title-sec pad-t-10">
					<div class="col-sm-12 col-md-12 col-lg-12">

						@if (session()->has('customError'))
							<div class="alert alert-danger alert-dismissible mar-t-10"
									role="alert"> <!-- show custom errors begin -->
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span>{{ session()->get('customError') }}</span>
							</div>
						@endif
						@if (session()->has('customSuccessMessage'))
							<div class="alert alert-success alert-dismissible mar-t-10"
									role="alert"> <!-- show custom errors begin -->
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								<span>{{ session()->get('customSuccessMessage') }}</span>
							</div>
						@endif
						<div>
							<div class="pull-left"> <!-- search from begin -->
								<h2 class="panel-title">Showing all decor styles in {{ $data['selectedCity'] }}</h2> <!-- show custom errors end -->
							</div>
							<div class="pull-right">
								@if(AppUtil::isTabVisible('new-decor'))
									<div class="in-blk">
										<a href="{{ route('decor.new.show') }}" class="btn btn-primary in-blk">
											<i class="glyphicon glyphicon-plus"></i> Add Decor Style
										</a>
									</div>
									@include('app.copy-data')
									<input type="hidden" name="typeTicketId" id="typeTicketId"
											value="{{ config('evibe.ticket_type.decors') }}">
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="pad-t-30"> <!-- decor results begin -->

					<div class="col-sm-3"> <!--  Filter options begin -->
						<div>
							<h4 class="no-mar pull-left">Filter Options</h4>
							<div class="pull-right">
								@if($data['isShowReset'])
									<a class="btn btn-xs btn-warning mar-l-20"
											href={{ route('decors.list') }}>Reset Filter</a>
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="filter-group">
							<div class="pad-b-20 ">
								<div class="filter-title">By name, code</div>
								<div class="text-center">
									<input id="searchQuery" class="form-control" type="text" name="query"
											placeholder="Search by name, code"
											value="{{ request()->input('query') }}"/>
								</div>
							</div>


							<div class="filter-title">By Price
							</div>
							<ul class="filter-group-options ls-none no-mar no-pad">
								<div class="pad-b-20">
									<li class="in-blk text-center">
										<input type="text" class="form-control filter-price filter-min-price"
												value="{{ request()->input('price_min') }}" placeholder="Min">
										<div class="text-muted font-13">Price Min</div>
									</li>
									<li class="in-blk text-center">
										<input type="text" class="form-control filter-price filter-max-price"
												value="{{ request()->input('price_max') }}" placeholder="Max">
										<div class="text-muted font-13">Price Max</div>
									</li>
								</div>

								<div class="pad-b-20">
									<select name="city" id="city" class="form-control city-clas">
										<option value="all">All Cities</option>
										@foreach($data['cities'] as $city)
											<option value="{{ $city->id }}"
													@if(request()->input('city')== $city->id)selected @endif>{{ $city->name }}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group">

									<select name="selectProvider" id="selectProvider" class="form-control">
										<option value="all">All Providers</option>
										@foreach($data['providers'] as $provider)
											<option value="{{ $provider->id }}"
													@if(request()->input('provider')== $provider->id)selected @endif>{{ $provider->name }}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group">
									<select name="selectOccasion" id="selectOccasion" class="form-control">
										<option value="all">All Occasions</option>
										@foreach($data['occasions'] as $occasion)
											<option value="{{ $occasion->id }}"
													@if(request()->input('occasion')== $occasion->id)selected @endif >{{ $occasion->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<select name="selectSellingStatus" id="selectSellingStatus" class="form-control">
										<option value="0">All Selling Status</option>
										@foreach($data['typeSellingStatus'] as $sellingState)
											<option value="{{ $sellingState->id }}"
													@if(request()->input('sellingStatus')== $sellingState->id)selected @endif>{{ ucfirst(strtolower($sellingState->name)) }}</option>
										@endforeach
									</select>
								</div>

								<div class="pad-t-15">
									<li class="text-center">
										<button id="btnFilter" onClick="search_func()"
												class="btn btn-info btn-filter-by-price">Filter
										</button>
									</li>
								</div>
							</ul>
						</div>
					</div> <!-- Filter options end -->

					<div class="col-sm-9"> <!-- decors list begin -->
						@if ($data['decors']->count())
							<ul class="ls-none no-mar no-pad"> <!-- styles list begin -->
								@foreach ($data['decors'] as $decor)
									<li class="in-blk a-decor">
										<div class="top">
											<a href="{{ route('decor.info.view', $decor->id) }}">
												<img class="profile-img" src="{{ $decor->getProfilePic() }}">
											</a>
											@if (AppUtil::isTabVisible('decors::status'))
												<div class="status @if($decor->is_live) active @else inactive @endif">
													@if($decor->is_live)
														<i class="glyphicon glyphicon-ok-sign"></i> Active
													@else
														<i class="glyphicon glyphicon-remove-sign"></i> Inactive
													@endif
												</div>
											@endif
										</div>
										<div class="pad-10 text-center">
											<div class="title">
												<a href="{{ route('decor.info.view', $decor->id) }}"
														title="[{{ $decor->code }}] {{ $decor->name }}">
													[{{ $decor->code }}] {{ $decor->name }}
												</a>
											</div>
											<div class="pad-t-3 pricing">
												@if ($decor->worth)
													<del class="pad-r-6">
														<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($decor->worth) }}
													</del>
												@endif
												<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($decor->min_price) }}
												@if ($decor->max_price && $decor->max_price > $decor->min_price)
													-
													<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($decor->max_price) }}
													@if ($decor->range_info)
														<a class="range-info" data-toggle="tooltip"
																data-placement="left"
																title="{!! $decor->range_info !!}">
															<i class="glyphicon glyphicon-question-sign"></i>
														</a>
													@endif
												@endif
											</div>
										</div>
										<div class="provider">
											<a href="{{ $decor->provider->getLocalLink() }}" target="_blank">
												<i class="glyphicon glyphicon-user"></i> {{ $decor->provider->code }}
											</a>
										</div>
									</li>
								@endforeach
							</ul> <!-- styles list end -->

							<div class="text-center"> <!-- pagination begin -->
								{{ $data['decors']->appends(request()->toArray())->links() }}
							</div>
						@else
							<div class="alert alert-danger">
								No results found. Use 'Add Decor Style' button above to create first decor.
							</div>
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {

			function updateUrlByFilters(event) {
				event.preventDefault();

				var priceMin = $('.filter-min-price').val();
				var priceMax = $('.filter-max-price').val();
				var query = $('#searchQuery').val();
				var provider = $('#selectProvider').val();
				var city = $('#city').val();
				var sellingStatus = $('#selectSellingStatus').val();
				var occasion = $('#selectOccasion').val();

				var url = "decors?";
				var isUrlChanged = false;

				if (city) {
					isUrlChanged = true;
					url += '&city=' + city;
				}

				if (query) {
					isUrlChanged = true;
					url += '&query=' + query;
				}

				if (priceMin || priceMax) {
					isUrlChanged = true;
					url += '&price_min=' + priceMin + '&price_max=' + priceMax;
				}

				if (provider) {
					url += '&provider=' + provider;
				}

				if (occasion) {
					url += '&occasion=' + occasion;
				}

				if (sellingStatus) {
					isUrlChanged = true;
					url += '&sellingStatus=' + sellingStatus;
				}

				if (isUrlChanged) {
					location.href = url;
				}
			}

			$('#searchQuery').keyup(function (event) {
				if (event.which == 13 && $(this).val()) { // on enter submit search
					updateUrlByFilters(event);
				}
			});

			// filter options
			$('#city').change(updateUrlByFilters);
			$('#selectProvider').change(updateUrlByFilters);
			$('#btnFilter').click(updateUrlByFilters);
			$('#selectSellingStatus').change(updateUrlByFilters);
			$('#selectOccasion').change(updateUrlByFilters);
		});

	</script>
@stop