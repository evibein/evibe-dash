@extends('layout.base')

@section('title')
	<title>Add New Decor | EvibeDash</title>
@stop

@section('content')
	<div class="container page-content new-decor-warp">
		<div class="mar-b-10">
			<a href="{{ route('decors.list') }}" class="font-16"><< Show all decor styles</a>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">

				<div class="title-sec pad-t-10">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h3 class="no-mar">Create New Decor</h3>
					</div>
					<div class="clearfix"></div>
				</div>

				<!-- show errors begin -->
				@if ($errors->count() > 0)
					<div class="mar-t-20 ">
						<div class="col-sm-12">
							<div class="alert alert-danger pad-10">
								<ul class="erros-list ls-none">
									<li>{{ $errors->first() }}</li>
								</ul>
							</div>
						</div>
						<div class="clearfi"></div>
					</div>
				@endif
			<!-- show errors end -->

				<!-- show custom errors begin -->
				@if (Session::has('customError'))
					<div class="alert alert-danger alert-dismissible mar-t-10" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span>{{ Session::get('customError') }}</span>
					</div>
			@endif
			<!-- show custom errors end -->

				<!-- create new decor begin -->
				<form action="{{ route('decor.new.save') }}" method="POST" role="form" class="mar-t-20">
					<div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Provider</label>
								<select name="dprovider" id="dprovider" class="form-control"
										data-old="{{ old('dprovider') }}">
									@foreach($data['vendors'] as $vendor)
										<option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Name*</label>
								<input id="dname" name="dname" type="text" class="form-control"
										placeholder="type name.." value="{{ old('dname') }}">
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Price (Worth - Min - Max)*</label>
								<div>
									<div class="col-sm-4 no-pad-l">
										<input id="dworth" name="dworth" type="text" class="form-control"
												placeholder="Worth..." value="{{ old('dworth') }}">
									</div>
									<div class="col-sm-4 no-pad-l">
										<input id="dminprice" name="dminprice" type="text" class="form-control"
												placeholder="Min. price..."
												value="{{ old('dminprice') }}">
									</div>
									<div class="col-sm-4 no-pad">
										<input id="dmaxprice" name="dmaxprice" type="text" class="form-control"
												placeholder="Max. price..."
												value="{{ old('dmaxprice') }}">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Timing (Setup - Duration) (in Hrs)*</label>
								<div>
									<div class="col-sm-6 no-pad-l">
										<input id="dtimesetup" name="dtimesetup" type="text"
												class="form-control"
												placeholder="Setup time..."
												value="{{ old('dtimesetup') }}">
									</div>
									<div class="col-sm-6 no-pad-r">
										<input id="dtimeduration" name="dtimeduration" type="text"
												class="form-control"
												placeholder="Rent duration..."
												value="{{ old('dtimeduration') }}">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>KMs (Free - Max)*</label>
								<div>
									<div class="col-sm-6 no-pad-l">
										<input id="dkmsfree" name="dkmsfree" type="text" class="form-control"
												placeholder="Free kms..." value="{{ old('dkmsfree') }}">
									</div>
									<div class="col-sm-6 no-pad-r">
										<input id="dkmsmax" name="dkmsmax" type="text" class="form-control"
												placeholder="Max kms..." value="{{ old('dkmsmax') }}">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Transport Charges (Min - Max)*</label>
								<div>
									<div class="col-sm-6 no-pad-l">
										<input id="dtransmin" name="dtransmin" type="text" class="form-control"
												placeholder="Min. price..."
												value="{{ old('dtransmin') }}">
									</div>
									<div class="col-sm-6 no-pad-r">
										<input id="dtransmax" name="dtransmax" type="text" class="form-control"
												placeholder="Max. price..."
												value="{{ old('dtransmax') }}">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Inclusions*</label>
								<textarea id="dinfo" name="dinfo" type="text" class="form-control vertical-resize"
										placeholder="type inclusions..." data-old="{{ old('dinfo') }}"></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Pre Reqs</label>
								<span>
								 <span class="text-italic">Copy from: </span>
								<select name="copyPreReq" id="copyPreReq">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($data['preReq'] as $req)
										<option value="{{ $req->value }}">{{ $req->name }}</option>
									@endforeach
								</select>
							</span>
								<textarea id="dmoreinfo" name="dmoreinfo" type="text"
										class="form-control vertical-resize"
										placeholder="Type pre reqs (if any)..."
										data-old="{{ old('dmoreinfo') }}"></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label title="Price range info is required with Max price">Price Range Information**</label>
								<textarea id="drangeinfo" name="drangeinfo" type="text"
										class="form-control vertical-resize"
										placeholder="type decor information (if any)...">{{ old('drangeinfo') }}</textarea>
							</div>
							<div class="form-group">
								<select name="handler" id="handler" class="form-control
										@if(in_array(auth()->user()->role_id , [config('evibe.roles.bd'), config('evibe.roles.sr_bd')])) hide @endif">
									<option value="-1">----Select Handler----</option>
									@foreach($data['handlers'] as $handler)
										<option value="{{ $handler->id }}"
												@if((in_array(auth()->user()->role_id , [config('evibe.roles.bd'), config('evibe.roles.sr_bd')]) && $handler->id == auth()->user()->id) ||
													(in_array(auth()->user()->role_id , [config('evibe.roles.bd'), config('evibe.roles.sr_bd')]) && old('handler') == $handler->id) ) selected
												@endif> {{ $handler->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Facts</label>
								<span>
								 <span class="text-italic">Copy from: </span>
								<select name="copyFacts" id="copyFacts">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($data['facts'] as $fact)
										<option value="{{ $fact->value }}">{{ $fact->name }}</option>
									@endforeach
								</select>
							</span>
								<textarea id="dfacts" name="dfacts" type="text" class="form-control vertical-resize"
										placeholder="type facts (if any)..."
										data-old="{{ old('dfacts') }}"></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label>Booking Terms*</label>
								<span>
								 <span class="text-italic">Copy from: </span>
								<select name="copyTerms" id="copyTerms">
									<option value="-1" selected="selected">-- select ---</option>
									@foreach ($data['terms'] as $term)
										<option value="{{ $term->value }}">{{ $term->name }}</option>
									@endforeach
								</select>
							</span>
								<textarea id="dterms" name="dterms" type="text" class="form-control vertical-resize"
										placeholder="type decor terms..." data-old="{{ old('dterms') }}"></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							<div class="form-group">
								<label>Event Supported*</label>
								<select name="events[]" id="events" class="form-control no-pad-l"
										data-old="{{old('events')}}">
									<option value="">Select events supported...</option>
									@foreach($data['typeEvents'] as $event)
										<option value="{{ $event->id }}">{{ $event->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="pad-t-10">
						<div class="col-sm-12 col-md-12 col-lg-12 text-center">
							<input type="submit" value="Submit" class="btn btn-primary"/>
							<a href="{{ route('decors.list') }}" class="btn btn-link">Cancel</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			// copy terms on selection

			// wysiwyg editor
			$('#dinfo, #dmoreinfo, #dfacts, #dterms').wysihtml5({
				toolbar: {
					"image": false,
					'html': false,
					'blockquote': false,
					'size': 'xs'
				}
			});

			// add existing data for wysiwyg editor
			var oldDataKeys = [
				'dinfo',
				'dmoreinfo',
				'dfacts',
				'dterms'
			];

			for (key in oldDataKeys) {
				var oldDataKey = oldDataKeys[key];
				var oldValue = $('#' + oldDataKey).data('old');
				var editorObj = $('#' + oldDataKey).data('wysihtml5').editor;
				if (editorObj) editorObj.setValue(oldValue);
			}

			//copy existing content in editor from drop down list
			var copyItems = {
				'#copyTerms': '#dterms',
				'#copyPreReq': '#dmoreinfo',
				'#copyFacts': '#dfacts'

			};
			$.each(copyItems, function (key, value) {
				$(key).change(function (event) {
					event.preventDefault();
					var val = $(key + ' option:selected').val();
					var editorObj = $(value).data('wysihtml5').editor;
					val = (val == -1) ? '' : val;
					if (editorObj) editorObj.setValue(val);
				});
			});

			//Event tags

			var decorEventSelect = {};
			var items = [{key: 'events', el: '#events'}];
			item = items[0];
			decorEventSelect[item['key']] = $(item['el']).selectize({
				maxItems: 1000,
				create: false,
				hideSelected: true
			});
			//set the old event tags
			function setData() {
				var ids = '', selEl = '', k = 0;
				for (i = 0; i < items.length; i++) {
					if ($(items[i]['el']).data('old')) {
						ids = $(items[i]['el']).data('old').toString().split(",");
						selEl = decorEventSelect[items[i]['key']][0].selectize;
						for (k = 0; k < ids.length; k++) {
							selEl.addItem(ids[k], false);
						}

					}
				}
			}

			setData();

			// selectize
			var $provider = $('#dprovider').selectize();
			$provider[0].selectize.setValue($.trim($('#dprovider').data('old')));
		});

	</script>
@stop