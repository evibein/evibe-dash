@extends('layout.base')

@section('content')
	<div class="container page-content decor-page-content">
		<?php $decorEvents = $data['decorEventsId']; ?>
		<div class="mar-b-10">
			<a href="{{ route('decors.list') }}" class="mar-r-15"><< All decor styles</a>
			@if ($data['decor']->provider && $data['decor']->provider->city)
				<a href="{{ route('decors.list') }}?city={{ $data['decor']->provider->city->id }}"><< All decors in {{ ucfirst($data['decor']->provider->city->name) }}</a>
			@endif
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="inner">
					<div class="pull-left">
						<input type="hidden" id="decorId" value="{{ $data['decor']->id }}">
						<h3 class="panel-title">[{{ $data['decor']->code }}] {{ $data['decor']->name }}</h3>
					</div>
					<div class="pull-right">
						@php $sellingStatus = $data['sellingStatus'] ? $data['typeSellingStatus']->find($data['sellingStatus']->product_selling_status_id) : $data['typeSellingStatus']->find(config("evibe.selling.status.default")); @endphp
						<div class="font-18 top-header-bar form-group no-mar in-blk">
							<div class="tkt-lead-status-wrap ticket-header-info-status-{{ $sellingStatus ? strtolower($sellingStatus->name) : "medium"}}">
								<select name="ticketLeadStatus" id="optionSellingStatus" class="form-control"
										data-url="{{ route("selling.status.update", [config("evibe.ticket_type.decors"), $data['decor']->id]) }}">
									@foreach($data['typeSellingStatus'] as $typeStatus)
										<option value="{{ $typeStatus->id }}"
												data-color="{{ strtolower($typeStatus->name) }}"
												@if($sellingStatus && ($sellingStatus->id == $typeStatus->id)) selected @endif>{{ $typeStatus->name }}
										</option>
									@endforeach
								</select>
							</div>
						</div>
						<a class="btn btn-sm btn-info mar-r-10 iq"
								data-url="{{route('iq.add',[ 'mapId' => $data['decor']->id,'mapTypeID' => config('evibe.ticket_type.decors')])}}"
								data-redirect="{{route('decor.info.view',$data['decor']->id)}}">

							<i class="glyphicon glyphicon-plus"></i> Add IQ
						</a>
						@if($data['approvalButton'])
							<a href="{{ route('approval.accept',[$data['decor']->id,config('evibe.ticket_type.decors')]) }}"
									class="btn btn-success btn-sm mar-r-10"
									onclick="return confirm('Are you sure?','Yes','No')"><i
										class="glyphicon glyphicon-thumbs-up"></i> Accept Approval</a>
							<button id="approvalRejectBtn"
									data-url="{{ route('approval.reject',[$data['decor']->id,config('evibe.ticket_type.decors')]) }}"
									class="btn btn-danger btn-sm mar-r-10"><i
										class="glyphicon glyphicon-thumbs-down"></i> Reject Approval
							</button>
						@endif
						@if($data['askApprovalButton'])
							<a id="askApproval"
									data-url="{{ route('approval.ask',[$data['decor']->id,config('evibe.ticket_type.decors')]) }}"
									class="btn btn-warning btn-sm mar-r-10"
									data-askreason="@if($data['isApprovalReason']) 1 @else 0 @endif"><i
										class="glyphicon glyphicon-question-sign"></i> Ask Approval</a>
							@if (AppUtil::isTabVisible('decors::edit_info'))
								<a id="editDecorInfoBtn"
										class="btn btn-danger btn-sm mar-r-10">
									<i class="glyphicon glyphicon-edit"></i> Edit
								</a>
							@endif
						@elseif (AppUtil::isTabVisible('decors::edit_info'))
							<a id="editDecorInfoBtn"
									class="btn btn-danger btn-sm mar-r-10">
								<i class="glyphicon glyphicon-edit"></i> Edit
							</a>
						@endif
						@if ($data['decor']->is_live)
							<a href="{{ AppUtil::getLiveUrl(config('evibe.ticket_type.decors'), $data['decor'])}}"
									target="_blank"
									class="btn btn-sm btn-primary mar-r-10">
								<i class="glyphicon glyphicon-eye-open"></i> View Live
							</a>
						@endif
						@if (AppUtil::isTabVisible('decors::actions'))
							@if ($data['decor']->is_live)
								@if (AppUtil::isSuperAdmin())
									<a href="{{ route('decor.actions.deactivate', $data['decor']->id) }}"
											class="mar-r-10 btn btn-sm btn-warning"
											onclick="return confirm('Are you sure?');">
										<i class="glyphicon glyphicon-off"></i> Deactivate
									</a>
								@endif
							@endif
							@if(AppUtil::isSuperAdmin())
								<a href="{{ route('decor.actions.delete', $data['decor']->id) }}"
										class="btn btn-sm btn-danger"
										onclick="return confirm('Are you sure?');">
									<i class="glyphicon glyphicon-trash"></i>
								</a>
							@endif
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel-body">

				<!-- take action begin -->
				@if ((AppUtil::isTabVisible('decors::makeactive')  || AppUtil::isTabVisible('activate_options')) && !$data['decor']->is_live)
					<div class="mar-b-20">
						<div class="alert alert-danger text-center pad-5">
							<b>THIS DECOR STYLE IS NOT ACTIVE.</b>
							<a href="{{ route('decor.actions.activate', $data['decor']->id) }}"
									class="btn btn-sm btn-success mar-l-10"
									onclick="return confirm('Are you sure?');">Make Active</a>
						</div>
					</div>
			@endif
			<!-- take action end -->

				<!--Internal question start -->
				<div class="col-lg-12">
					<div class="text-center">
						@if(session()->has('customError'))
							<div class="pad-6 alert-danger in-blk">
								{{ session('customError') }}
							</div>
						@elseif(session()->has('customSuccess'))
							<div class="alert-success pad-6 in-blk">
								{{ session('customSuccess') }}
							</div>
						@endif
					</div>
					<div class="iq-container">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="no-mar">Internal Questions</h4>
							</div>
							<div class="panel-body">
								@if ($data['iqs']->count())
									<ul class="iq-list no-mar no-pad">
										@foreach($data['iqs'] as $iq)
											<li class="@if ($iq->deleted_at) strike @endif">
												<div class="pull-left">
													<div class="iq-q">{!! $iq->question !!}</div>
													<div class="iq-ans">{!! $iq->answer !!}</div>
													<div class="iq-info">
														<i>{{ date('j F, g:i a',strtotime($iq->created_at)) }} by
															<b>{{ $iq->user ? $iq->user->name:"--" }}</b>
															@if ($iq->deleted_by)
																Deleted by
																<b>{{ $iq->deletedBy ? $iq->deletedBy->name:"--" }}</b>
															@endif
														</i>
													</div>
												</div>
												<div class="pull-right">
													@if (!$iq->deleted_at)
														<button class=" btn btn-sm btn-primary in-blk iq-edit"
																data-url="{{route('iq.edit',$iq->id)}}"
																data-redirect="{{route('decor.info.view',$data['decor']->id)}}"
																data-question="{!! $iq->question !!}"
																data-id="{{$iq->id}}">
															<i class="glyphicon glyphicon-edit"></i>

														</button>
														<button class=" btn btn-sm btn-danger in-blk iq-delete"
																data-url="{{route('iq.delete',$iq->id)}}"><i
																	class="glyphicon glyphicon-trash"></i></button>
													@endif
												</div>
												<div class="clearfix"></div>
											</li>
										@endforeach
									</ul>
								@else
									<div class="text-danger">No internal questions were added yet. Click on
										<b>Add IQ</b> button to create first internal question.
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>

				<!--Internal questions end-->

				<!-- header begin -->
				<section class="data-sec pad-b-20">
					<div class="text-center">
						<div class="pricing in-blk">
							@if ($data['decor']->worth)
								<div class="worth in-blk">
									<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($data['decor']->worth) }}
								</div>
							@endif
							<div class="in-blk">
								<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($data['decor']->min_price) }}
								@if ($data['decor']->max_price && $data['decor']->max_price > $data['decor']->min_price)
									-
									<span class="rupee-font">&#8377;</span> {{ AppUtil::formatPrice($data['decor']->max_price) }}
								@endif
							</div>
						</div>
						<div class="location in-blk">
							<i class="glyphicon glyphicon-map-marker"></i>
							<b>{{ $data['decor']->provider->area->name }}</b>
						</div>
					</div>
					@if ($data['decor']->range_info)
						<div class="pad-t-5 alert alert-info alert-range-info">
							<h5 class="no-mar text-muted">
								<i class="glyphicon glyphicon-info-sign"></i> Price range information
							</h5>
							<div class="pad-t-10">{!! $data['decor']->range_info !!}</div>
						</div>
					@endif
				</section>
				<!-- header end -->
				<div class="mar-b-6">
					@if($data['askApprovalAlert'])
						{{ $data['askApprovalAlert'] }}
					@elseif($data['isActivateAlert'])
						{{ $data['isActivateAlert'] }}
					@endif
					@if($data['rejectionComment'])
						{!! $data['rejectionComment'] !!}
					@endif
				</div>
				<!-- gallery begin -->
				<section class="data-sec pad-b-20">
					<div class="sec-body pad-t-10">

						<!-- upload new images begin -->
						@if (AppUtil::isTabVisible('decors::edit_gallery'))
							@if (session()->has('imageError'))
								<div class="alert alert-danger alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="glyphicon glyphicon-remove-sign"></i> {{ session('imageError') }}
								</div>
							@endif
							@if (session()->has('successMsg'))
								<div class="alert alert-success alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="glyphicon glyphicon-ok-sign"></i> {{ session('successMsg') }}
								</div>
							@endif
							<div class="upload-img-wrap">
								<form action="{{ route('decor.gallery.upload', $data['decor']->id) }}"
										method="POST" role="form"
										enctype="multipart/form-data" accept-charset="UTF-8" class="form">
									<div class="mar-b-10">
										<label>
											<input type="radio" name="gal-type"
													value="{{ config('evibe.gallery.type.image') }}"
													@if(old('gal-type') == 0) checked
													@elseif(old('gal-type') !=1) checked @endif> Image
										</label>
										<label class="mar-l-10">
											<input type="radio" name="gal-type"
													value="{{ config('evibe.gallery.type.video') }}"
													@if(old('gal-type') == 1) checked
													@endif @if(session('type') == 'video')  checked @endif> Video
										</label>
									</div>
									<div class="alert alert-warning">
										<div class="gal-type-image">
											<h4>Upload New Images <span class="font-14">( Valid formats: JPG, jpeg, jpg and png. Maximum file size: <b><u>5 MB</u></b> )</span>
											</h4>
											<div class="form-group">
												<input type="file" name="decorImages[]" multiple="true"/>
											</div>
											<div class="form-group">
												<label for="ignoreWatermark" class="text-normal text-black">
													<input type="checkbox" id="ignoreWatermark"
															name="ignoreWatermark"> Ignore Watermark
												</label>
											</div>
										</div>
										<div class="gal-type-video hide">
											<div class="mar-b-15">
												<input type="text" name="video" class="form-control"
														placeholder="Enter youtube video link"
														value="{{ old('video') }}">
											</div>
										</div>
										<button type="submit" class="btn btn-sm btn-warning">
											<i class="glyphicon glyphicon-plus"></i> Upload
										</button>
									</div>
								</form>
							</div>
						@endif

						@if ($data['images']->count() || $data['videos']->count())
							<section class="data-sec pad-b-20">
								@if($data['images']->count())
									<div class="sec-head">
										<h4 class="text-danger in-blk">Images</h4>
									</div>
									<ul class="ls-none no-mar no-pad images-list">
										@foreach($data['images'] as $image)
											<li class="image-cnt">
												<img class="an-image" src="{{ $image->getLink() }}">
												<div class="text-center pad-t-3">
													<div class="img-title">
														<div class="view">
															<div class="image-link"
																	title="[{{ $image->code }}] {{ $image->title }}">
																<a href="{{ $image->getLink() }}"
																		target="_blank">[{{ $image->code }}] {{ $image->title }}</a>
															</div>
														</div>
														<div class="edit hide">
															<form action="{{ route('decor.gallery.title', [$data['decor']->id, $image->id]) }}"
																	method="POST">
																<input type="text" name="title"
																		placeholder="type image title..."
																		class="form-control"
																		value="{{ $image->title }}">
																<button class="btn btn-xs btn-success">
																	<i class="glyphicon glyphicon-check"></i> Save
																</button>
																<button class="btn btn-link btn-img-title-edit-cancel">Cancel</button>
															</form>
														</div>
													</div>
												</div>
												@if (AppUtil::isTabVisible('decors::edit_gallery'))
													<div class="img-actions">
														@if ($image->is_profile)
															<a href="{{ route('decor.gallery.unset', [$data['decor']->id, $image->id]) }}"
																	class="btn btn-xs btn-link">
																<i class="glyphicon glyphicon-remove"></i> Unset Profile
															</a>
														@else
															<a href="{{ route('decor.gallery.set', [$data['decor']->id, $image->id]) }}"
																	class="btn btn-xs btn-link">
																<i class="glyphicon glyphicon-ok"></i> Set Profile
															</a>
														@endif
														<a class="btn btn-xs btn-link btn-edit-img-title">
															<i class="glyphicon glyphicon-pencil"></i> Edit
														</a>
														<a href="{{ route('decor.gallery.delete', [$data['decor']->id, $image->id]) }}"
																class="btn btn-xs btn-link"
																onclick="return confirm('Are you sure?');">
															<i class="glyphicon glyphicon-trash"></i> Del
														</a>
													</div>
												@endif
												@if ($image->is_profile)
													<div class="profile-label label label-success">
														<i class="glyphicon glyphicon-star"></i> Profile
													</div>
												@endif
											</li>
										@endforeach
									</ul>
								@endif
								@if($data['videos']->count())
									<div class="sec-head">
										<h4 class="in-blk text-danger">Videos</h4>
									</div>
									<div>
										@foreach($data['videos'] as $video)
											<div class="col-sm-4">
												<div class="mar-b-20">
													<iframe height="315" width="100%" class="no-pad"
															src="https://www.youtube.com/embed/{{ $video->url }}">
													</iframe>
													<a href="{{ route('decor.gallery.delete', [$data['decor']->id, $video->id]) }}"
															class="btn btn-xs btn-danger btn-block pad-5"
															onclick="return confirm('Are you sure?');">
														<i class="glyphicon glyphicon-trash"></i> Delete
													</a>
												</div>
											</div>
										@endforeach
										<div class="clearfix"></div>
									</div>
								@endif
							</section>
						@else
							<div class="alert alert-danger">
								<i class="glyphicon glyphicon-warning-sign"></i> No images/videos were uploaded for this decor style.
							</div>
						@endif
					</div>
				</section>
				<!-- gallery end -->

				<!-- description begin -->
				<section class="data-sec pad-b-20">
					<div class="sec-head">
						<h4 class="in-blk mar-r-10 text-danger">Details</h4>
					</div>
					<div class="sec-body">
						<div class="col-sm-8 no-pad-l">
							<div class="sub-sec mar-b-20">
								<h5 class="no-mar text-muted">Inclusions</h5>
								<div class="pad-t-10">{!! $data['decor']->info !!}</div>
							</div>
							@if ($data['decor']->more_info)
								<div class="sub-sec mar-b-20">
									<h5 class="no-mar text-muted">Pre Reqs</h5>
									<div class="pad-t-10">{!! $data['decor']->more_info !!}</div>
								</div>
							@endif
							@if ($data['decor']->facts)
								<div class="sub-sec mar-b-20">
									<h5 class="no-mar text-muted">Facts</h5>
									<div class="pad-t-10">{!! $data['decor']->facts !!}</div>
								</div>
							@endif
							@if ($data['decor']->terms)
								<div class="sub-sec  mar-b-20">
									<h5 class="no-mar text-muted">Terms</h5>
									<div class="pad-t-10">{!! $data['decor']->terms !!}</div>
								</div>
							@endif
							@if ($data['decorEvents'])
								<div class="sub-sec  mar-b-20">
									<h5 class="no-mar text-muted">Events supported</h5>
									<div class="pad-t-10">
										@foreach($data['decorEvents'] as $event)
											<span class="label-event">{{ $event->event->name }}</span>
										@endforeach
									</div>
								</div>
							@endif
						</div>
						<div class="col-sm-4 no-pad-l">
							<table id="decor-info" class="table table-striped table-bordered">
								<tr>
									<td><label>Free KMs</label></td>
									@if ($data['decor']->kms_free)
										<td>{{ $data['decor']->kms_free }} KMs</td>
									@else
										<td> --</td>
									@endif
								</tr>
								<tr>
									<td><label>Max KMs</label></td>
									@if ($data['decor']->kms_max)
										<td>{{ $data['decor']->kms_max }} KMs</td>
									@else
										<td> --</td>
									@endif
								</tr>
								<tr>
									<td><label>Transport Charges</label></td>
									<td>
										@if (!$data['decor']->trans_min)
											Free
										@else
											<span class="rupee-font">&#8377;</span> {{ $data['decor']->trans_min }}
											@if ($data['decor']->trans_max && $data['decor']->trans_max > $data['decor']->trans_min)
												-
												<span class="rupee-font">&#8377;</span> {{ $data['decor']->trans_max }}
											@endif
										@endif
									</td>
								</tr>
								<tr>
									<td><label>Setup Time</label></td>
									@if ($data['decor']->time_setup)
										<td>{{ $data['decor']->time_setup }} Hrs</td>
									@else
										<td> --</td>
									@endif
								</tr>
								<tr>
									<td><label>Rent Duration</label></td>
									@if ($data['decor']->time_duration)
										<td>{{ $data['decor']->time_duration }} Hrs</td>
									@else
										<td> --</td>
									@endif
								</tr>
								<tr>
									<td><label>Name</label></td>
									<td>{{$data['decor']->name}}</td>
								</tr>
								<tr>
									<td><label>Code</label></td>
									<td>{{$data['decor']->code}}</td>
								</tr>
								<tr>
									<td><label>Provider</label></td>
									<td>
										@if (!AppUtil::isTabVisible('decors::view_provider'))
											{{ $data['decor']->provider->name }}
										@else
											<a href="{{ $data['decor']->provider->getLocalLink() }}"
													target="_blank">
												{{ $data['decor']->provider->name }}
											</a>
										@endif
									</td>
								</tr>
								<tr>
									<td><label>Handler</label></td>
									<td>
										{{ $data['decor']->handler ? $data['decor']->handler->name : "--" }}
									</td>
								</tr>
								@if($data['decor']->provider)
									<tr>
										<td><label>Location</label></td>
										<td>@if($data['decor']->provider->area){{ $data['decor']->provider->area->name }}@else -- @endif</td>
									</tr>
									<tr>
										<td><label>City</label></td>
										<td>@if($data['decor']->provider->area){{ $data['decor']->provider->city->name }}@else -- @endif</td>
									</tr>
								@endif
							</table>
						</div>
						<div class="clearfix"></div>
					</div>
				</section>
				<!-- description end -->

				<!-- tags section begin -->
				<section class="data-sec pad-b-20">
					<div class="sec-head">
						<h4 class="in-blk mar-r-10 text-danger">Tags</h4>
					</div>
					<div class="sec-body">

						<!-- add new tags begin -->
						@if (AppUtil::isTabVisible('decors::edit_tags'))
							<div class="alert alert-warning upload-img-wrap">
								<h4>Add More Tags</h4>
								<div>You can add a new tag by just selecting a parent tag (or) both parent tag and child tag.</div>
								<form action="{{ route('decor.tags.add', $data['decor']->id) }}" method="POST"
										role="form" enctype="multipart/form-data" accept-charset="UTF-8"
										class="form-inline pad-t-10">
									<div class="form-group">
										<select id="selectParentTag" name="selectParentTag" class="form-control">
											<option value="-1">--select parent tag--</option>
											@foreach ($data['tagsList'] as $key=>$tag)
												<option value="{{ $key }}">{{ $tag['name'] }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<select id="selectChildTag" name="selectChildTag" class="form-control">
											<option value="-1">--select child tag--</option>
										</select>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-sm btn-warning in-blk">
											<i class="glyphicon glyphicon-plus"></i> Add Tag
										</button>
									</div>
								</form>
							</div>
						@endif
					<!-- add new tags end -->

						@if (count($data['tags']))
							<ul class="ls-none no-mar no-pad tags-list">
								@foreach ($data['tags'] as $key => $tag)
									<li class="label-tag">{{ $tag }}
										<a href="{{ route('decor.tag.delete', [$data['decor']->id, $key]) }}"
												class="text-danger">
											<i class="glyphicon glyphicon-trash"></i>
										</a>
									</li>
								@endforeach
							</ul>
						@else
							<div class="alert alert-danger">
								<i class="glyphicon glyphicon-warning-sign"></i> No tags were assigned for this decor style.
							</div>
						@endif
					</div>
				</section>
				<!-- tags section end -->

			</div>
		</div>
		<div class="hide">
			<select id="duplicateChildTags">
				@foreach($data['tagsList'] as $key => $tag)
					@foreach ($tag['childs'] as $childKey => $childTag)
						<option value="{{ $childKey }}" data-parentid="{{ $key }}">{{ $childTag['name'] }}</option>
					@endforeach
				@endforeach
			</select>
		</div>
	@stop

	@section('app-modals')
		@parent

		<!-- edit decor info modal begin -->
			<div id="modalEditDecorInfo" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Edit Decor Style Details</h4>
						</div>
						<div class="modal-body">
							<div class="alert alert-danger mar-b-15 pad-10 hide"></div>
							<div class="pad-l-20 pad-r-20">
								<form class="form form-horizontal" role="form">
									<div class="form-group">
										<label class="col-sm-3">Name</label>
										<div class="col-sm-9">
											<input type="text" class="form-control decor-name"
													placeholder="enter decor style name"
													value="{{ $data['decor']->name }}"/>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">Provider</label>
										<div class="col-sm-9">
											<select class="form-control decor-vendor">
												@foreach($data['vendors'] as $vendor)
													<option value="{{ $vendor->id }}"
															@if($vendor->id == $data['decor']->provider_id) selected="selected" @endif>{{ $vendor->name }}</option>
												@endforeach
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">Handler</label>
										<div class="col-sm-9">
											<select name="handler" id="handler" class="form-control"
													@if(in_array(auth()->user()->role_id , [config('evibe.roles.bd'), config('evibe.roles.sr_bd')])) disabled=disabled @endif>
												<option value="-1">-- Select Handler --</option>
												@foreach($data['handlers'] as $handler)
													<option value="{{ $handler->id }}"
															@if($data['decor']->handler_id == $handler->id || (auth()->user()->role_id == config('evibe.roles.bd'))) selected
															@endif> {{ $handler->name }}</option>
												@endforeach
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">Pricing (Worth - Min - Max)</label>
										<div class="col-sm-9">
											<div class="col-sm-4 no-pad-l">
												<input type="text" class="form-control decor-worth"
														placeholder="Worth..."
														value="{{ $data['decor']->worth }}"/>
											</div>
											<div class="col-sm-4 no-pad-l">
												<input type="text" class="form-control decor-min-price"
														placeholder="Min. price..."
														value="{{ $data['decor']->min_price }}"/>
											</div>
											<div class="col-sm-4 no-pad">
												<input type="text" class="form-control decor-max-price"
														placeholder="Max. price..."
														value="@if($data['decor']->min_price == $data['decor']->max_price){{0}}@else{{$data['decor']->max_price}}@endif"/>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="col-sm-3"
												title="Price range info is required with max price">Price range info**</label>
										<div class="col-sm-9">
										<textarea class="form-control decor-range-info vertical-resize"
												placeholder="type why this price? (or) why this range in price?"
												data-val="{{ $data['decor']->range_info }}">{!! $data['decor']->range_info !!}</textarea>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">Timing (Setup - Duration) (in Hrs)*</label>
										<div class="col-sm-9">
											<div class="col-sm-6 no-pad-l">
												<input type="text" class="form-control decor-time-setup"
														placeholder="Setup time..."
														value="{{ $data['decor']->time_setup }}"/>
											</div>
											<div class="col-sm-6 no-pad">
												<input type="text" class="form-control decor-time-duration"
														placeholder="Rent Duration..."
														value="{{ $data['decor']->time_duration }}"/>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">KMs (Free - Max)*</label>
										<div class="col-sm-9">
											<div class="col-sm-6 no-pad-l">
												<input type="text" class="form-control decor-kms-free"
														placeholder="Free KMs..."
														value="{{ $data['decor']->kms_free }}"/>
											</div>
											<div class="col-sm-6 no-pad">
												<input type="text" class="form-control decor-kms-max"
														placeholder="Max. KMs..."
														value="{{ $data['decor']->kms_max }}"/>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">Transport Charges (Min - Max)*</label>
										<div class="col-sm-9">
											<div class="col-sm-6 no-pad-l">
												<input type="text" class="form-control decor-trans-min"
														placeholder="Min. transport charges"
														value="{{ $data['decor']->trans_min }}"/>
											</div>
											<div class="col-sm-6 no-pad">
												<input type="text" class="form-control decor-trans-max"
														placeholder="Max. transport charges"
														value="{{ $data['decor']->trans_max }}"/>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">Inclusions</label>
										<div class="clearfix"></div>
										<div class="col-sm-12 decor-edit-info-wrap">
										<textarea class="form-control decor-info vertical-resize"
												placeholder="type complete inclusions"
												data-val="{!! $data['decor']->info !!}"></textarea>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group mar-t-5">
										<label class="col-sm-3">Pre Reqs</label>
										<div class="col-sm-9">
											<div class="pad-b-10">Copy From:
												<select name="decorCopyPreReq" id="decorCopyPreReq"
														class="">
													<option value="-1">-- select --</option>
													@foreach ($data['preReq'] as $req)
														<option value="{{ $req->value }}">{{ $req->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-sm-12">
											<textarea class="form-control decor-more-info vertical-resize"
													placeholder="type pre requisites (if any)"
													data-val="{!! $data['decor']->more_info !!}"></textarea>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group mar-t-5">
										<label class="col-sm-3">Facts</label>
										<div class="col-sm-9">
											<div class="pad-b-10">Copy From:
												<select name="decorCopyFacts" id="decorCopyFacts" class="form-contorl">
													<option value="-1">-- select --</option>
													@foreach ($data['facts'] as $facts)
														<option value="{{ $facts->value }}">{{ $facts->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-sm-12">
											<textarea class="form-control decor-facts vertical-resize"
													placeholder="type decor facts (if any)"
													data-val="{!! $data['decor']->facts !!}"></textarea>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group mar-t-5">
										<label class="col-sm-3">Booking terms</label>
										<div class="col-sm-9">
											<div class="pad-b-10">Copy From:
												<select name="decorCopyTerms" id="decorCopyTerms" class="form-contorl">
													<option value="-1">-- select --</option>
													@foreach ($data['terms'] as $term)
														<option value="{{ $term->value }}">{{ $term->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-sm-12">
											<textarea class="form-control decor-terms vertical-resize"
													placeholder="type terms"
													data-val="{!! $data['decor']->terms !!}"></textarea>
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-group">
										<label class="col-sm-3">Event Supported*</label>
										<div class="col-sm-9">
											<select name="events[]" id="events" class="form-control no-pad-l"
													data-old='{{$decorEvents}}'>
												<option value="">Select events supported...</option>
												@foreach($data['typeEvents'] as $event)
													<option value="{{ $event->id }}">{{ $event->name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="clearfix"></div>
								</form>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-danger btn-edit-decor-submit">Submit</button>
						</div>
					</div>
				</div>
			</div>
	</div>

	@include('layout.approval_action') <!-- edit decor info modal end -->
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/decor.js");
		});
	</script>
@stop