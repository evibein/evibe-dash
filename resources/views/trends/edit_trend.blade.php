@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">

			<ol class="breadcrumb">
				<li><a href="/trends"><<< Show All Trends</a></li>
			</ol>
			<div class="page-content trends-wrap">
				<div class="add-trend-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="pull-left">
								<h2 class="panel-title">Edit Trend</h2>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							@if (session()->has('custom_error'))
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<span class="">{{ session('custom_error') }}</span>
								</div>
								<?php Session::forget('custom_error'); ?>
							@endif
							@if ($errors->count() > 0)
								<div class="errors-cnt alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
									<ul class="erros-list list-unstyled">
										<li>{{ $errors->first() }}</li>
									</ul>
								</div>
							@endif
							<form action="/trends/edit-trend" method="POST" role="form" class="form-horizontal"
									enctype="multipart/form-data" accept-charset="UTF-8">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="item-label">Name *</label>

										<input id="tname" name="tname" type="text" class="form-control"
												placeholder="Enter trend name"
												value="@if(old('tname')){{ old('tname') }}@else{{$data['trend']->name}}@endif">
									</div>
								</div>
								<div class="col-sm-3 mar-l-20">
									<label class="item-label">Provider *</label>
									<select name="tprovider" id="tprovider" class="form-control"
											data-old="@if(old('tprovider')){{old('tprovider')}}@else{{ $data['trend']->planner_id }}@endif">
										@foreach($data['vendors'] as $vendor)
											<option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-sm-2 mar-l-20">
									<label class=" item-label">City *</label>
									<select name="tcity" id="tcity" class="form-control"
											data-old="@if(old('tcity')){{old('tcity')}}@else{{$data['trend']->city_id}}@endif">
										@foreach($data['city'] as $city)
											<option value="{{ $city->id }}"
													@if($data['trend']->city_id == $city->id) selected @endif>{{ $city->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
								<div class="mar-t-10">
									<div class="col-sm-2 no-pad-l">
										<label class="item-label">Price *</label>
										<input id="tprice" name="tprice" type="text" class="form-control"
												placeholder="Enter price"
												value="@if(old('tprice')){{ old('tprice') }}@else{{$data['trend']->price}}@endif"/>
									</div>
									<div class="col-sm-2">
										<label class=" item-label">Worth Price </label>
										<input name="worthPrice" type="text" class="form-control"
												placeholder="Enter Worth price"
												value="@if(old('worthPrice')){{ old('worthPrice') }}@elseif($data['trend']->price_worth && $data['trend']->price_worth>0){{ $data['trend']->price_worth }}@endif">
									</div>
									<div class="col-sm-2">
										<label class=" item-label">Max Price </label>
										<input name="maxPrice" type="text" class="form-control"
												placeholder="Enter Worth price"
												value="@if(old('maxPrice')){{ old('maxPrice') }}@elseif($data['trend']->price_max && $data['trend']->price_max>0){{$data['trend']->price_max}}@endif">
									</div>
									<div class="col-sm-6">
										<label class=" item-label"
												title="Range info is required with max price">Price Range Info ** </label>
									<textarea name="rangeInfo" id="rangeInfo" class="form-control" rows="3"
											placeholder="Enter why this price?">@if(old('rangeInfo')){{old('rangeInfo')}}@else{!! $data['trend']->range_info !!}@endif</textarea>
									</div>
									<div class="col-lg-6 no-pad-l">
										<label class="item-label">Information *</label>
									<textarea id="tinfo" name="tinfo" type="text" class="form-control" rows="6"
											placeholder="Enter trend information">@if(old('tinfo')){{ old('tinfo') }}@else{!! $data['trend']->info !!}@endif</textarea>
									</div>
									<div class="col-lg-6">
										<label class="item-label">Prerequisites </label>
										<span class="mar-l-10">
										 <span class="text-italic">Copy from: </span>
											<select name="copyPreReq" id="copyPreReq">
												<option value="-1" selected="selected">-- select ---</option>
												@foreach ($data['preReq'] as $req)
													<option value="{{ $req->value }}">{{ $req->name }}</option>
												@endforeach
											</select>
									</span>
									<textarea id="prereq" name="prereq" type="text" class="form-control" rows="6"
											placeholder="Enter Prerequisites for this trend">@if(old('prereq')){{ old('prereq') }}@else{!! $data['trend']->prerequisites !!}@endif</textarea>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="mar-t-10">
									<div class="col-lg-6 no-pad-l">
										<label class="item-label"> Facts</label>
										<span class="mar-l-10">
											<span class="text-italic">Copy from: </span>
												<select name="copyFacts" id="copyFacts">
													<option value="-1" selected="selected">-- select ---</option>
													@foreach ($data['facts'] as $fact)
														<option value="{{ $fact->value }}">{{ $fact->name }}</option>
													@endforeach
												</select>
										</span>
									<textarea id="facts" name="facts" type="text" class="form-control" rows="6"
											placeholder="Enter facts ">@if(old('facts')){{ old('facts') }}@else{!! $data['trend']->facts !!}@endif</textarea>
									</div>
									<div class="col-lg-6 ">
										<label class="item-label">Terms </label>
										<span class="mar-l-10">
										 <span class="text-italic">Copy from: </span>
												<select name="copyTerms" id="copyTerms">
													<option value="-1" selected="selected">-- select ---</option>
													@foreach ($data['terms'] as $term)
														<option value="{{ $term->value }}">{{ $term->name }}</option>
													@endforeach
												</select>
										</span>
									<textarea id="terms" name="terms" type="text" class="form-control" rows="6"
											placeholder="Enter terms for this trend">@if(old('terms')){{ old('terms') }}@else{!! $data['trend']->terms !!}@endif</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-lg-8">
									<div class="form-group pad-10">
										<label>Event Supported*</label>
										<select name="events[]" id="events" class="form-control no-pad-l"
												data-old='@if(old('events')){{ old('events') }}@else{{$data['trendEventIds']}}@endif'>
											<option value="">Select events supported...</option>
											@foreach($data['typeEvents'] as $event)
												<option value="{{ $event->id }}">{{ $event->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="clearfix"></div>

								<div class="form-group hide">
									<label class="col-sm-2 item-label">Existing Images</label>
									<div class="col-sm-10">
										@if (count(AppUtil::filterGalleryItems($data['trend']->gallery, 0)) == 0)
											<div>No images found</div>
										@endif
										@foreach ($data['trend']->gallery as $galleryItem)
											@if ($galleryItem->type == 0)
												<div class="in-blk">
													<img class="gallery-item" alt="{{ $galleryItem->title }}"
															src="{{ AppUtil::getGalleryBaseUrl() . '/trends/' . $data['trend']->id . '/images/' . $galleryItem->url }}">
													<div class="pad-t-3 text-center">
														<a href="/trends/remove-image/{{ $data['trend']->id }}/{{ $galleryItem->id }}"
																class="btn btn-xs btn-danger"
																onclick="return confirm('Are you sure?', 'Yes', 'No');">
															<i class=""></i>
															<span>Remove</span>
														</a>
													</div>
												</div>
											@endif
										@endforeach
									</div>
								</div>
								<input type="hidden" id="trendId" name="trendId" value="{{ $data['trend']->id }}"/>
								<div class="form-group text-center">
									<a href="/trends/view/{{$data['trend']->id}}" class="btn btn-danger mar-r-10">
										<i class="glyphicon glyphicon-remove-circle"></i> Cancel</a>

									<button type="submit" class="btn btn-primary"><i
												class="glyphicon glyphicon-floppy-save"></i>
										Submit
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$('#facts ,  #tinfo , #range_info, #prereq , #terms').wysihtml5({
				toolbar: {
					"image": false,
					'html': false,
					'blockquote': false,
					'size': 'xs'
				}
			});

			var oldVal = $('#tprovider').data('old');
			var providerObj = $('#tprovider').selectize();
			providerObj[0].selectize.setValue($.trim(oldVal));

			//copy existing content in editor from drop down list
			var copyItems = {
				'#copyTerms': '#terms',
				'#copyPreReq': '#prereq',
				'#copyFacts': '#facts'

			};
			$.each(copyItems, function (key, value) {
				$(key).change(function (event) {
					event.preventDefault();
					var val = $(key + ' option:selected').val();
					var editorObj = $(value).data('wysihtml5').editor;
					val = (val == -1) ? '' : val;
					if (editorObj) editorObj.setValue(val);
				});
			});

			var trendsEventSelect = {};
			var items = [{key: 'events', el: '#events'}];

			item = items[0];
			trendsEventSelect[item['key']] = $(item['el']).selectize({
				maxItems: 1000,
				create: false,
				hideSelected: true
			});
			//set the old event tags
			function setData() {
				var ids = '', selEl = '', k = 0;
				for (i = 0; i < items.length; i++) {
					if ($(items[i]['el']).data('old')) {
						ids = $(items[i]['el']).data('old').toString().split(",");
						selEl = trendsEventSelect[items[i]['key']][0].selectize;
						for (k = 0; k < ids.length; k++) {
							selEl.addItem(ids[k], false);
						}

					}
				}
			}

			//set the data of combo box
			var ids = [, '#tcity'];
			$.each(ids, function (key, value) {
				var oldValue = $(value).data('old');
				$(value).val(oldValue);
			});

			setData();
		})
	</script>
@stop