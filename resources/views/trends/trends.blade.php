@extends('layout.base')

@section('title')
	<title>All Trends | EvibeDash</title>
@stop

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">
			<div class="page-content trends-wrap mar-t-25">
				<div class="trends-panel-cnt">
					<div class="panel panel-default">
						<div class="panel-body">
							<h4>Showing All Trends List</h4>
							<div class="mar-t-20">
								<div class="pull-left">
									<div id="searchForm" role="form" class="form form-horizontal">
										<div class="in-blk mar-r-10 valign-mid">
											<input id="searchQuery" type="text" name="query"
													class="form-control u-seach-box"
													placeholder="Search by name, price, code"
													value="{{ request()->input('query') }}"/>
										</div>
										<div class="in-blk valign-mid">
											<input type="button" id="btnSearchSubmit" class="btn btn-info"
													value="Search">
											@if (request()->has('query') && request()->input('query'))
												<a href="/trends"
														class="pad-l-3">Clear search</a>
											@endif
										</div>
									</div>
								</div>
								<div class="pull-right">
									<div class="in-blk">
										<a href="trends/new" class="btn btn-primary btn-add-new-vendor">
											<i class="glyphicon glyphicon-plus"></i> Add New Trend
										</a>
									</div>
									@include('app.copy-data')
									<input type="hidden" name="typeTicketId" id="typeTicketId"
											value="{{ config('evibe.ticket_type.trends') }}">
									<div class="mar-l-10 in-blk">
										<select name="city" id="city" class="form-control">
											@foreach($data['cities'] as $city)
												<option value="{{ $city->id }}"
														@if(request()->input('city')== $city->id) selected @endif>{{ $city->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							@if (session()->has('custom_success'))
								<div class="alert alert-success alert-dismissable mar-t-15">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<span class="">{{ session('custom_success') }}</span>
									<?php session()->forget('custom_success'); ?>
								</div>
							@endif
							<div class="trend-wrap mar-t-25">
								@if($data['trends']->count())
									@foreach($data['trends'] as $trending)
										<div class="col-sm-3">
											<div class="trend-list-wrap">
												<div class="inner">
													<a href="trends/view/{{ $trending->id }}">
														<img class="trend-profile"
																src="{{ $trending->getProfilePic() }}">
													</a>
													<div class="layer"></div>
													<div class="price">
														Price <span
																class="rupee-font mar-l-5">&#8377; {{AppUtil::formatPrice($trending->price)}}</span>
													</div>
													<div class="title text-center">
														<a href="trends/view/{{$trending->id}}">{{ $trending->name }} [ {{$trending->code}} ]</a>
													</div>
												</div>
												<div class="footer">
													<div class="top text-center">
														Provider : @if($trending->provider)
															<a href="{{route('vendor.view',$trending->provider->id)}}">{{$trending->provider->name}}</a>
														@else
															<span class="text-strike"> Deleted</span>
														@endif
													</div>
													@if($trending->is_live == 0)
														<div class=" text-center  label-not-live">
															<i class="glyphicon glyphicon-thumbs-down "></i>
															<span>Not Live</span>
														</div>
													@elseif($trending->is_live == 1)
														<a href="{{AppUtil::getLiveUrl(config('evibe.ticket_type.trends'),$trending)}}"
																target="_blank" class="btn  btn-live">
															<i class="glyphicon glyphicon-hand-up "></i>
															<span>View Live</span>
														</a>
													@endif
													<a href="trends/edit/{{ $trending->id }}"
															class="btn btn-edit">
														<i class="glyphicon glyphicon-pencil"></i>
														<span>Edit</span>
													</a>
												</div>
											</div>
										</div>
									@endforeach
									<div class="clearfix"></div>
								@else
									<div class="alert alert-danger">
										No Trends available. click <b>Add New Trend</b> Button to create one.
									</div>
								@endif
								<div class="text-center mar-t-30">
									{{$data['trends']->appends(request()->except('page'))->links()}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			$('#city').change(function (event) {
				event.preventDefault();
				location.href = '/trends?city=' + $(this).val();
			});

			$('#btnSearchSubmit').click(function (event) {
				event.preventDefault();

				var city = $('#city').val();
				location.href = "/trends?city=" + city + '&query=' + $('#searchQuery').val();

			});
		})
	</script>
@endsection