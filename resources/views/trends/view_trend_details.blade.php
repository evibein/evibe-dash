@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="content-inner col-xs-12
				col-md-10 col-md-offset-1
				col-sm-10 col-sm-offset-1
				col-lg-10 col-lg-offset-1">

			<ol class="breadcrumb">
				<li><a href="/trends"><<< Show All Trends</a></li>
			</ol>

			<div class="page-content">
				<div class="vendor-details-cnt">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="pull-left">
								<h4 class="panel-title">{{$data['trend']->name}} [ {{$data['trend']->code}} ]</h4>
							</div>
							<div class="pull-right">
								<a class="btn btn-sm btn-info mar-r-10 iq "
										data-url="{{route('iq.add',[ 'mapId' => $data['trend']->id,'mapTypeID' => config('evibe.ticket_type.trends')])}}"
										data-redirect="{{'/trends/view/'.$data['trend']->id}}">

									<i class="glyphicon glyphicon-plus"></i> Add IQ
								</a>

								@if(AppUtil::isAdmin() || AppUtil::isSrBd())
									@if($data['trend']->is_live == 0)
										<a href="/trends/activate/{{$data['trend']->id}}"
												class="btn btn-success btn-sm mar-r-10">
											<i class="glyphicon glyphicon-hand-up"></i> Activate
										</a>
									@elseif($data['trend']->is_live == 1)
										@if(AppUtil::isSuperAdmin())
											<a href="/trends/deactivate/{{$data['trend']->id}}"
													class="btn btn-sm btn-not-live mar-r-10 btn-deactivate">
												<i class="glyphicon glyphicon-thumbs-down"></i> Deactivate
											</a>
										@endif
									@endif
								@endif
								@if($data['trend']->provider && $data['trend']->is_live == 1)
									<a href="{{AppUtil::getLiveUrl(config('evibe.ticket_type.trends'),$data['trend'])}}"
											class="btn btn-warning btn-sm mar-r-10" target="_blank">
										<i class="glyphicon glyphicon-eye-open"></i> View Live
									</a>
								@endif
								@if(AppUtil::isTabVisible('trends::edit_info'))
									<a href="/trends/edit/{{ $data['trend']->id }}"
											class="btn btn-sm btn-primary mar-r-10">
										<i class="glyphicon glyphicon-pencil"></i>
										<span>Edit</span>
									</a>
								@endif
								@if(AppUtil::isTabVisible('trends::delete'))
									@if(AppUtil::isSuperAdmin())
										<a href="/trends/delete/{{ $data['trend']->id }}" class="btn btn-danger btn-sm"
												onclick="return confirm('Are you sure?','Yes','NO')">
											<i class="glyphicon glyphicon-trash"></i> Delete
										</a>
									@endif
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">

							<!--Internal question start -->
							<div class="iq-container">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="no-mar pull-left">Internal Questions</h4>
										<div class="clearfix"></div>
									</div>
									<div class="panel-body">
										@if ($data['iqs']->count())
											<ul class="iq-list no-mar no-pad">
												@foreach($data['iqs'] as $iq)
													<li class="@if ($iq->deleted_at) strike @endif">
														<div class="pull-left">
															<div class="iq-q">{!! $iq->question !!}</div>
															<div class="iq-ans">{!! $iq->answer !!}</div>
															<div class="iq-info">
																<i>{{ date('j F, g:i a',strtotime($iq->created_at)) }} by
																	<b>{{ $iq->user ? $iq->user->name:"--" }}</b>
																	@if ($iq->deleted_by)
																		Deleted by
																		<b>{{ $iq->deletedBy ? $iq->deletedBy->name:"--" }}</b>
																	@endif
																</i>
															</div>
														</div>
														<div class="pull-right">
															@if (!$iq->deleted_at)
																<button class=" btn btn-sm btn-primary in-blk iq-edit"
																		data-url="{{route('iq.edit',$iq->id)}}"
																		data-redirect="{{'/trends/view/'.$data['trend']->id}}"
																		data-question="{!! $iq->question !!}"
																		data-id="{{$iq->id}}">
																	<i class="glyphicon glyphicon-edit"></i>
																</button>
																<button class=" btn btn-sm btn-danger in-blk iq-delete"
																		data-url="{{route('iq.delete',$iq->id)}}"><i
																			class="glyphicon glyphicon-trash"></i>
																</button>

															@endif
														</div>
														<div class="clearfix"></div>
													</li>
												@endforeach
											</ul>
										@else
											<div class="text-danger">No internal questions were added yet. Click on
												<b>Add IQ</b> button to create first internal question.
											</div>
										@endif
									</div>
								</div>
							</div>
							<!--Internal question end -->

							<div class="trend-data-cnt">
								<div>
									<div class="col-lg-8">
										<div class="sub-sec mar-b-20">
											<h5 class="no-mar text-muted">INFO</h5>
											<div class="pad-t-10">
												{!! $data['trend']->info !!}
											</div>
										</div>
										<div class="mar-t-15">
											@if($data['trend']->events->count() >0)
												<h4 class="text-danger">Event supported</h4>
												@foreach($data['trend']->events as $event)
													<label class="label-event">@if($event->event){{$event->event->name}}@endif</label>
												@endforeach
											@endif
										</div>
										<div class="mar-t-30">
											@if($data['trend']->prerequisites)
												<div class="sub-sec mar-b-20">
													<h5 class="no-mar text-muted">PREREQUISITES</h5>
													<div class="pad-t-10">
														{!! $data['trend']->prerequisites !!}
													</div>
												</div>
											@endif
											@if($data['trend']->facts)
												<div class="sub-sec mar-b-20">
													<h5 class="no-mar text-muted">FACTS</h5>
													<div class="pad-t-10">
														{!! $data['trend']->facts !!}
													</div>
												</div>
											@endif

											@if($data['trend']->terms)
												<div class="sub-sec mar-b-20">
													<h5 class="no-mar text-muted">TERMS</h5>
													<div class="pad-t-10">
														{!! $data['trend']->terms !!}
													</div>
												</div>
											@endif
										</div>
									</div>
									<div class="col-lg-4">
										<table class="table table-bordered table-striped">
											<tr>
												<td><b>Name</b></td>
												<td>{{$data['trend']->name}}</td>
											</tr>
											<tr>
												<td><b>Price</b></td>
												<td>
													<span class="rupee-font">&#8377; </span> {{AppUtil::formatPrice($data['trend']->price)}}
													@if($data['trend']->price_max && $data['trend']->price_max>0)
														-
														<span class="rupee-font">&#8377; </span> {{AppUtil::formatPrice($data['trend']->price_max)}}
													@endif
													@if($data['trend']->price_worth && $data['trend']->price_worth>0)
														( <span class="rupee-font">&#8377; </span> <span
																class="text-strike">{{AppUtil::formatPrice($data['trend']->price_worth)}}</span> )
													@endif
												</td>
											</tr>
											<tr>
												<td><b>Provider</b></td>
												<td>
													@if($data['trend']->provider)
														<a href="/vendors/view/{{ $data['trend']->provider->id }}">
															{{ $data['trend']->provider->name }}
														</a>
													@else
														<span class="text-strike text-danger">
													Deleted Provider
												</span>
													@endif
												</td>
											</tr>
											<tr>
												<td><b>Live Url</b></td>
												<td>
													<a href="{{AppUtil::getLiveUrl(config('evibe.ticket_type.trends'),$data['trend'])}}">{{$data['trend']->url}}</a>
												</td>
											</tr>
											<tr>
												<td><b>Priority</b></td>
												<td>@if($data['trend']->priority) {{ $data['trend']->priority }}@else
														<i>Not Available</i> @endif</td>
											</tr>
											<tr>
												<td><b>City</b></td>
												<td>{{$data['trend']->city->name}}</td>
											</tr>
										</table>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="images-section">
									<div class="upload-form-section">
										<div class="col-lg-8">
											<div class="img-sec mar-t-15">
												<h4 class="text-danger">Gallery</h4>
												@if ($errors->count() > 0)
													<div class=" alert alert-danger alert-dismissable">
														<i class="glyphicon glyphicon-info-sign"></i>
														<button type="button" class="close" data-dismiss="alert"
																aria-hidden="true">&times;
														</button>
														{{ $errors->first() }}
													</div>

												@endif
												@if(session()->has('custom_error'))
													<div class="alert alert-danger alert-dismissable">
														<i class="glyphicon glyphicon-info-sign"></i>
														<button type="button" class="close" data-dismiss="alert"
																aria-hidden="true">&times;
														</button>
														{{session('custom_error')}}
													</div>
												@endif
												@if(session()->has('successMsg'))
													<div class="alert alert-success">
														<button type="button" class="close" data-dismiss="alert"
																aria-hidden="true">&times;
														</button>
														{{session('successMsg')}}
													</div>
												@endif
												<form action="/trends/upload-image/{{$data['trend']->id}}" method="POST"
														role="form" enctype="multipart/form-data">
													<div class="mar-b-10">
														<label>
															<input type="radio" name="gal-type"
																	value="{{ config('evibe.gallery.type.image') }}"
																	@if(old('gal-type') == 0) checked
																	@elseif(old('gal-type') !=1) checked @endif> Image
														</label>
														<label class="mar-l-10">
															<input type="radio" name="gal-type"
																	value="{{ config('evibe.gallery.type.video') }}"
																	@if(old('gal-type') == 1) checked
																	@endif @if(session('type') == 'video')  checked @endif> Video
														</label>
													</div>
													<div class="form-group alert alert-info pad-t-20 0">
														<div class="gal-type-image">
															<label class="item-label text-black text-normal in-blk">Upload Images
																<span class="text-danger">(Max Size - 5 MB)</span></label>
															<div class="in-blk mar-l-20">
																<input type="file" name="images[]" id="images[]"
																		multiple="true"/>
															</div>
														</div>
														<div class="gal-type-video hide">
															<div class="mar-b-15">
																<input type="text" name="video" class="form-control"
																		placeholder="Enter youtube video link"
																		value="{{ old('video') }}">
															</div>
														</div>
														<div class="in-blk">
															<button type="submit" class="btn btn-info btn-sm in-blk">
																<i class="glyphicon glyphicon-upload"></i> Upload
															</button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									@if (!$data['images']->count() && !$data['videos'])
										<div class="alert alert-danger">No images/videos uploaded for this trend</div>
									@else
										@if($data['images']->count())
											<div class="gallery-images">
												<h4 class="text-danger">Images</h4>
												@foreach ($data['images'] as $image)
													<div class="gallery-wrap col-sm-3">
														<div class="gal-ctn">
															<img class="trend-gallery"
																	src="{{ AppUtil::getGalleryBaseUrl() . '/trends/' . $data['trend']->id . '/images/results/' . $image->url }}">
															<div class="image-title text-center">
														<span class="title">
															@if($image->title) {{ $image->title }} @else -- @endif
														</span>
																<div class="title-form hide">
																	<form action="/trends/image-title/{{$image->id}}/{{$data['trend']->id}}"
																			method="post">
																		<div>
																			<input type="text"
																					placeholder="type title.."
																					class="pad-5" style="width:100%;"
																					name="img-title-input"
																					value="{{ $image->title }}">
																		</div>
																		<div class="text-center mar-t-10">
																			<button type="submit"
																					class="btn btn-xs btn-success">
																				<i class="glyphicon glyphicon-ok"></i>
																				Save
																			</button>
																			<button class="btn btn-title-cancel btn-xs btn-danger mar-l-5">
																				<i class="glyphicon glyphicon-remove"></i> Cancel
																			</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
														<div class="gallery-action mar-t-10">
															<a href="/trends/delete-pic/{{$image->id}}/{{$data['trend']->id}}"
																	class="btn btn-danger btn-xs mar-r-4"
																	onclick="return confirm('Are you sure?','Yes','NO')">
																<i class="glyphicon glyphicon-trash"></i> Delete
															</a>
															<button class="btn btn-xs btn-edit btn-trends-edit btn-primary mar-r-4">
																<i class="glyphicon glyphicon-edit"></i> Edit
															</button>
															@if($image->is_profile == 0)
																<a href="/trends/profile-pic/{{$image->id}}/{{$data['trend']->id}}"
																		class="btn btn-primary btn-xs mar-r-4">
																	<i class="glyphicon glyphicon-hand-up"></i> Set profile
																</a>
															@elseif($image->is_profile == 1)
																<span class="profile-set">
															<i class="glyphicon glyphicon-ok"></i> Profile Pic
														</span>
															@endif
														</div>
													</div>
												@endforeach
												<div class="clearfix"></div>
											</div>
										@endif
										@if($data['videos']->count())
											<div class="mar-t-20">
												<h4 class="text-danger">Videos</h4>
											</div>
											<div>
												@foreach($data['videos'] as $video)
													<div class="col-sm-4">
														<div class="mar-b-20">
															<div class="mar-b-20">
																<iframe height="315" width="100%" class="no-pad"
																		src="https://www.youtube.com/embed/{{ $video->url }}">
																</iframe>
																<a href="/trends/delete-pic/{{$video->id}}/{{$data['trend']->id}}"
																		class="btn btn-xs btn-danger btn-block pad-5"
																		onclick="return confirm('Are you sure?');">
																	<i class="glyphicon glyphicon-trash"></i> Delete
																</a>
															</div>
														</div>
													</div>
												@endforeach
												<div class="clearfix"></div>
											</div>
										@endif
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			// trends image title
			$('.btn-trends-edit').on('click', function () {
				$(this).parents('.gallery-wrap').find('.image-title .title').addClass('hide');
				$(this).parents('.gallery-wrap').find('.title-form').removeClass('hide')
			});

			$('.btn-title-cancel').click(function (event) {

				event.preventDefault();
				$(this).parents('.gallery-wrap').find('.image-title .title').removeClass('hide');
				$(this).parents('.gallery-wrap').find('.title-form').addClass('hide')
			});
		});
	</script>
@endsection