@extends('layout.base')

@section('content')
	<div class="container page-content new-cake-warp">
		<div>
			<div class="pull-left">
				<a href="{{ route('partner.add.package.list') }}" class="btn btn-sm btn-default"><i
							class="glyphicon glyphicon-step-backward"></i> Show all packages</a>
			</div>
			<div class="pull-right">
				@if(session()->has('customError'))
					<div class="alert-danger pad-5"><i
								class="glyphicon glyphicon-info-sign"></i> {{ session('customError') }}</div>
				@elseif(session()->has('customSuccess'))
					<div class="alert-success pad-5"><i
								class="glyphicon glyphicon-ok-sign"></i> {{ session('customSuccess') }}</div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="panel panel-default mar-t-5">
			<div class="panel-heading">
				<div class="col-sm-4 no-pad-l">
					<h4 class="sec-sub-title"><span
								class="text-black">{{ $data['package']->package_name }}</span>
					</h4>
				</div>
				@if(is_null($data['package']->accepted_at) && is_null($data['package']->rejected_at))
					<div class="pull-right">
						<a href="{{ route('partner.add.package.accept.approval',$data['package']->id) }}"
								class="btn btn-success btn-sm mar-r-10"
								onclick="return confirm('Are you sure? This package will be copied for activation','Yes','No')"><i
									class="glyphicon glyphicon-thumbs-up"></i> Accept Package</a>
						<a data-url="{{ route('partner.add.package.upgrade',$data['package']->id) }}"
								class="btn btn-warning btn-sm mar-r-10 btn-need-upgrade"><i
									class="glyphicon glyphicon-envelope"></i> Message Partner</a>
						<a data-url="{{ route('partner.add.package.reject.approval',$data['package']->id) }}"
								class="btn btn-danger btn-sm mar-r-10 btn-reject-reason"><i
									class="glyphicon glyphicon-thumbs-down"></i> Reject Package
						</a>
					</div>
					<div class="clearfix"></div>
				@elseif(!is_null($data['package']->accepted_at))
					<div class="col-md-3 pull-right alert alert-success no-mar-b">
						Accepted on {{ $data['package']->accepted_at }}
					</div>
				@elseif(!is_null($data['package']->rejected_at))
					<div class="col-md-3 pull-right alert alert-danger no-mar-b">
						Rejected on {{ $data['package']->rejected_at }}
					</div>
				@endif
				<div class="clearfix"></div>
			</div>
			<div class="panel panel-default mar-l-5 mar-r-4 no-mar-b mar-t-10">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
								aria-expanded="true" aria-controls="collapseOne">
							<i class="glyphicon glyphicon-plus"></i> Messages ({{ count($data['messages']) }})
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
						aria-labelledby="headingOne">
					<div class="panel-body partner-messages-wrap">
						@if($data['messages'])
							@foreach($data['messages'] as $message)
								<div class="table-bordered pad-10">
									@foreach($message['upgrades'] as $upgrade)
										@if(config("evibe.partner.discussion.".$upgrade))
											<div class="alert-info pad-5 in-blk mar-b-6">{{ config("evibe.partner.discussion.".$upgrade) }}</div>
										@endif
									@endforeach
									<div><b>Comments: </b>{!! $message['comment'] !!}</div>
									<div class="mar-b-10">
										<span class="pull-right"><i
													class="glyphicon glyphicon-time"></i> {{ $message['created_at'] }}</span>
									</div>
								</div>
								<div class="table-bordered partner-response-wrap col-lg-9 col-md-9 col-sm-9 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 mar-t-10">
									<div class="pad-b-10"><b>Partner Response: </b>
										@if($message['partnerComment'])
											{!! $message['partnerComment'] !!}
										@else
											--
										@endif
									</div>
									<div class="mar-b-10">
										<span class="pull-left">
											<i class="glyphicon glyphicon-time"></i> <b>Read at:</b>
											@if($message['read_at'])
												{{ $message['read_at'] }}
											@else
												--
											@endif
										</span>
										<span class="pull-right">
											<i class="glyphicon glyphicon-time"></i> <b>Submitted at:</b>
											@if($message['respond_at'])
												{{ $message['respond_at'] }}
											@else
												--
											@endif
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
								<hr>
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="panel-body">
				<h4>Info :</h4>
				<table class="table-bordered table table-hover no-pad-b no-mar-b">
					<tr>
						@php $partnerId = ""; @endphp
						@if($data['partnerData']['partner'])
							@php $partnerId = $data['partnerData']['partner']->id @endphp
						@endif
						<td>Partner</td>
						<td>
							<a href="@if($data['partnerData']['mapTypeId'] == config("evibe.ticket_type.planners")){{ route("vendor.view", $partnerId) }}@else{{ route("venues.view", $partnerId) }}@endif">
								@if(!is_null($data['partnerData']['partner'])){{ $data['partnerData']['partner']->person." - ".$data['partnerData']['partner']->name }}@endif
							</a>
						</td>
					</tr>
					<tr>
						<td>Name</td>
						<td>{{ $data['package']->package_name }}</td>
					</tr>
					<tr>
						<td>Price</td>
						<td>{{ $data['package']->price }}</td>
					</tr>
					<tr>
						<td>Worth</td>
						<td>{!! $data['package']->worth !!}</td>
					</tr>
					<tr>
						<td>Inclusions</td>
						<td>{!! $data['package']->inclusions !!}</td>
					</tr>
					@foreach($data['packageValues'] as $value)
						<tr>
							<td>{{ $value['name'] }}</td>
							<td>{{ $value['value'] }}</td>
						</tr>
					@endforeach
				</table>
			</div>
			<div class="mar-l-10 mar-r-10">
				<div class="pad-10">
					<h4>Gallery :</h4>
					@foreach($data['packageGallery'] as $gallery)
						<a href="{{ config("evibe.gallery.host")."/package/new/".$data['package']->id."/".$gallery->url }}"
								target="_blank"
								class="booking-gallery-link">
							<img src="{{ config("evibe.gallery.host")."/package/new/".$data['package']->id."/".$gallery->url}}"
									class="ticket-booking-image"
									alt="{{ $gallery->title }}">
						</a>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<div id="modalUpdatePackageInfo" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Message to partner regarding improvements</h4>
				</div>
				<div class="modal-body no-pad-b">
					<div class="alert alert-danger mar-b-15 pad-10 hide"></div>
					<div class="pad-l-20 pad-r-20">
						<form class="form form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-12"
										for="packageUpgradeReasons">Please select the reasons to upgrade the package</label>
								<div class="col-sm-12">
									<select name="packageUpgradeReasons[]" id="packageUpgradeReasons"
											class="form-control no-pad-l">
										<option value="">Select reasons...</option>
										<option value="1">Gallery images quality is not good</option>
										<option value="2">Gallery images are not matching with description provided</option>
										<option value="3">More Gallery images needed to understand the package</option>
										<option value="4">Price is too high</option>
										<option value="5">Similar package already exists</option>
										<option value="6">Inclusions are not upto the mark</option>
									</select>
								</div>
							</div>
							<div class="form-group mar-t-20">
								<label class="col-sm-12">Comments:</label>
								<div class="col-sm-12">
									<textarea class="form-control decor-facts vertical-resize"
											id="packageUpgradeComments" name="packageUpgradeComments"
											placeholder="Please enter comments (if any)"></textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-danger btn-upgrade-modal-submit"
							data-url="{{ route('partner.add.package.upgrade',$data['package']->id) }}">Submit
					</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modalPackageRejectReason" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Reason for rejecting the package</h4>
				</div>
				<div class="modal-body no-pad-b">
					<div class="pad-l-20 pad-r-20">
						<form class="form form-horizontal" role="form">
							<div class="form-group mar-t-20">
								<label class="col-sm-12">Please specify the reason for rejecting:</label>
								<div class="col-sm-12">
									<textarea class="form-control decor-facts vertical-resize"
											id="partnerPackageRejectReason" name="partnerPackageRejectReason"
											placeholder="Please enter comments"></textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-danger btn-partner-package-reject-comment"
							data-url="{{ route('partner.add.package.reject.approval',$data['package']->id) }}">Submit
					</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			var packageEventSelect = {};
			var item = '';
			var items = [{key: 'packageUpgradeReasons', el: '#packageUpgradeReasons'}];

			item = items[0];
			packageEventSelect[item['key']] = $(item['el']).selectize({
				maxItems: 1000,
				create: false,
				hideSelected: true
			});

			$('.btn-need-upgrade').on('click', function (e) {
				e.preventDefault();
				$('#modalUpdatePackageInfo').modal({
					show: false,
					keyboard: false,
					backdrop: 'static'
				});
				$('#modalUpdatePackageInfo').modal('show');
			});

			$('.btn-upgrade-modal-submit').on('click', function (e) {
				e.preventDefault();

				$(this).css('cursor', 'progress');
				$(this).attr('disabled', true);
				var data = {
					reasons: $('#packageUpgradeReasons').val(),
					comments: $('#packageUpgradeComments').val()
				};
				var url = $(this).data("url");
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data: data
				}).done(function (data, textStatus, jqXHR) {
					if (data.success) {
						location.reload();
					} else {
						$('.btn-upgrade-modal-submit').css('cursor', 'pointer');
						$('.btn-upgrade-modal-submit').attr('disabled', false);
						window.showNotyError(data.error);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					$(this).removeAttr('disabled');
					window.showNotyError();
				});
			});

			$('.btn-reject-reason').on('click', function (e) {
				e.preventDefault();
				$('#modalPackageRejectReason').modal({
					show: false,
					keyboard: false,
					backdrop: 'static'
				});
				$('#modalPackageRejectReason').modal('show');
			});

			$('.btn-partner-package-reject-comment').on('click', function (e) {
				e.preventDefault();

				$(this).css('cursor', 'progress');
				$(this).attr('disabled', true);
				var data = {
					comments: $('#partnerPackageRejectReason').val()
				};
				var url = $(this).data("url");
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data: data
				}).done(function (data, textStatus, jqXHR) {
					if (data.success) {
						location.reload();
					} else {
						$('.btn-partner-package-reject-comment').css('cursor', 'pointer');
						$('.btn-partner-package-reject-comment').attr('disabled', false);
						window.showNotyError(data.error);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					$(this).removeAttr('disabled');
					window.showNotyError();
				});
			})
		});
	</script>
@stop