@extends('layout.base')

@section('content')
	<div class="container page-content">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="title-sec pad-t-10">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h2 class="panel-title">Partner packages list from main</h2>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="pad-t-30">
					@if (count($data['newPackages']) > 0)
						@foreach ($data['newPackages'] as $package)
							<div class="in-blk col-sm-2 pad-b-10">
								<div class="new-package-list-info">
									<div class="top">
										<div class="ribbon-2">
											<span>
												@foreach($data['typeTicket'] as $type)
													@if($type->id == $package->package_type_id)
														{{ $type->name }}
													@endif
												@endforeach
											</span>
										</div>
										<a href="{{ route('partner.add.package.profile', $package->id) }}">
											@php $image = $data['packageGallery']->where('partner_option_id', $package->id)->first(); @endphp
											@if($image)
												<img class="profile-img"
														src="{{ config("evibe.gallery.host")."/package/new/".$package->id."/".$image->url }}"
														style="height: 100px; width: 100%">
											@else
												<img class="profile-img"
														src="http://local.evibe-gallery.in/img/app/default_package.jpg"
														style="height: 100px; width: 100%">
											@endif
										</a>
									</div>
									<div class="text-center">
										<div class="title">
											<a href="{{ route('partner.add.package.profile', $package->id) }}"
													title="{{ $package->package_name }}">
												{{ $package->package_name }}
											</a>
										</div>
										<div class="pad-t-3 pricing">
											@if ($package->worth && $package->worth > 0)
												<del class="pad-r-6">
													<span class="rupee-font">&#8377;</span> {{ $package->worth }}
												</del>
											@endif
											<span class="rupee-font">&#8377;</span> {{ $package->price }}
										</div>
									</div>
								</div>
							</div>
						@endforeach
						<div class="text-center">
							{{ $data['newPackages']->appends(request()->except('page'))->links() }}
						</div>
					@else
						<div class="alert alert-danger">
							No Packages are added yet from partner.
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection