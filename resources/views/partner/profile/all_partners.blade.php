@extends('layout.base')
@section('content')
	<div class="container page-content cakes-list-warp">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="title-sec pad-t-10 text-center font-22 text-info">
					Select the partner type and partner(search by name/company/email)
				</div>
				<form class="form-group pad-t-30">
					@if (session()->has('statusUpdateError') || $errors->count() > 0)
						<div class="alert alert-danger mar-l-20 mar-r-20" role="alert">
							<ul class="erros-list ls-none">
								@if (session()->has('statusUpdateError'))
									<li>{{ session('statusUpdateError') }}</li>
								@endif
								@if ($errors->count())
									<li>{{ $errors->first() }}</li>
								@endif
							</ul>
						</div>
					@endif
					<div class="col-sm-3 col-lg-3 col-md-3 col-xs-3">
						<select class="form-control" id="typePartner" name="typePartner">
							<option value="-1">Select the type of partner</option>
							<option value="2">planner</option>
							<option value="3">venue</option>
						</select>
					</div>
					<div class="col-sm-7 col-lg-7 col-md-7 col-xs-7">
						@if($data)
							<select class="form-control" id="partnerId" name="partnerId">
							</select>
						@endif
					</div>
					<div class="col-sm-2 col-lg-2 col-md-2 col-xs-2">
						<a class="btn btn-info btn-search-partner-options">
							<i class="glyphicon glyphicon-search"></i> Search
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="complete-partner-list hide">
		@foreach($data['allVendors'] as $vendor)
			<option name="vendor"
					value="{{$vendor['id']}}">{{ $vendor['name'] . " / " . $vendor['person'] ." / ".  $vendor['email']}}</option>
		@endforeach
		@foreach($data['allVenues'] as $venue)
			<option name="venue"
					value="{{$venue['id']}}">{{ $venue['name'] . " / " . $venue['person'] ." / ".  $venue['email']}}</option>
		@endforeach
	</div>
@endsection
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			(function initFunctions() {
				$('#typePartner').on('change', function () {
					var partnerType = parseInt($(this).val());
					if (partnerType <= 0) {
						// Remove the current selectize
						$('#partnerId').selectize()[0].selectize.destroy();
						$('#partnerId').empty();
					}
					else if (partnerType === 2) {
						var vendorOptions = $('.complete-partner-list option[name=vendor]').clone();
						// Remove the current selectize
						$('#partnerId').selectize()[0].selectize.destroy();

						// Apply selectize for the new options
						$('#partnerId').empty().append(vendorOptions).selectize();
					}
					else if (partnerType === 3) {
						var venueOptions = $('.complete-partner-list option[name=venue]').clone();
						// Remove the current selectize
						$('#partnerId').selectize()[0].selectize.destroy();

						// Apply selectize for the new options
						$('#partnerId').empty().append(venueOptions).selectize();
					}
				});

				$('.btn-search-partner-options').on('click', function () {
					var partnerType = parseInt($('#typePartner').val());
					var partnerId = parseInt($('#partnerId').val());

					if ((partnerType <= 0) || (partnerId <= 0)) {
						showNotyError("Please select a valid partner type & partner");
					}
					else {
						window.location.replace(window.location.href + "/" + partnerType + "/" + partnerId)
					}
				});
			})();
		});
	</script>
@endsection