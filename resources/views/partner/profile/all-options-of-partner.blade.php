@extends('layout.base')

@section('content')
	<div class="container page-content cakes-list-warp">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="title-sec pad-t-10">
					<span class="text-info font-24 text-bold">All options of @if(!is_null($data['nameOfThePartner'])){{$data['nameOfThePartner']}}@else{{"selected"}}@endif</span>
					<span class="pull-right">
						<a class="btn btn-info btn-sm" href="{{route('partner.options.home')}}"><i
									class="glyphicon glyphicon-search"></i> Search Other Partners</a>
					</span>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-12 col-md-12 col-lg-12 pad-t-30">
					@if($data['data'])
						@foreach($data['data'] as $product)
							<li class="in-blk a-decor">
								@if($product['mapTypeId'] == config('evibe.ticket_type.cakes'))
									<div class="top">
										<div class="ribbon-2">
											<span>
												{{$product['displayName']}}
											</span>
										</div>
										<div class="status @if($product['isLive']) active @else inactive @endif">
											@if($product['isLive'])
												<i class="glyphicon glyphicon-ok-sign"></i> Active
											@else
												<i class="glyphicon glyphicon-remove-sign"></i> Inactive
											@endif
										</div>
									</div>
									@if(!is_null($product['galleryUrl']))
										<a href="{{config('evibe.host').'cakes/'.$product['mapId'].'/info'}}"
												target="_blank">
											<img src="{{$product['galleryUrl']}}" alt="This is one image"
													class="img-responsive">
										</a>
									@else
										<a href="#">No data found about this option</a>
									@endif
									<div class="pad-10 text-center">
										<div class="title">
											[{{ $product['code']}}] {{ $product['name'] }}
										</div>
										<div class="pad-t-3 pricing">
											<span class="rupee-font">&#8377;</span>
											@if($product['maxPrice'])
												{{ $product['maxPrice']}} /kg
											@endif
										</div>
									</div>
								@elseif($product['mapTypeId'] == config('evibe.ticket_type.decors'))
									@if(!is_null($product['galleryUrl']))
										<div class="top">
											<div class="ribbon-2">
											<span>
												{{$product['displayName']}}
											</span>
											</div>
											<div class="status @if($product['isLive']) active @else inactive @endif">
												@if($product['isLive'])
													<i class="glyphicon glyphicon-ok-sign"></i> Active
												@else
													<i class="glyphicon glyphicon-remove-sign"></i> Inactive
												@endif
											</div>
										</div>
										<a href="{{config('evibe.host').'decors/'. $product['mapId'] .'/info'}}"
												target="_blank"><img
													src="{{$product['galleryUrl']}}"
													alt="This is one image" width="150" height="200"></a>
										<div class="pad-10 text-center">
											<div class="title">
												[{{ $product['code']}}] {{ $product['name'] }}
											</div>
											<div class="pad-t-3 pricing">
												@if ($product['worthPrice'])
													<del class="pad-r-6">
														<span class="rupee-font">&#8377;</span> {{AppUtil::formatPrice($product['worthPrice'])}}
													</del>
												@endif
												<span class="rupee-font">&#8377;</span>{{AppUtil::formatPrice($product['minPrice'])}}
												@if ($product['maxPrice'] && $product['maxPrice'] > $product['minPrice'])
													-
													<span class="rupee-font">&#8377;</span> {{AppUtil::formatPrice($product['maxPrice'])}}
												@endif
											</div>
										</div>
									@else
										<a href="#">No data found about this option</a>
									@endif
								@elseif($product['mapTypeId'] == config('evibe.ticket_type.packages'))
									@if(!is_null($product['galleryUrl']))
										<div class="top">
											<div class="ribbon-2">
											<span>
												{{$product['displayName']}}
											</span>
											</div>
											<div class="status @if($product['isLive']) active @else inactive @endif">
												@if($product['isLive'])
													<i class="glyphicon glyphicon-ok-sign"></i> Active
												@else
													<i class="glyphicon glyphicon-remove-sign"></i> Inactive
												@endif
											</div>
										</div>
										<a href="{{config('evibe.host').'packages/view/'.$product['mapId']}}"
												target="_blank"><img
													src="{{$product['galleryUrl']}}"
													alt="This is one image" width="150" height="200">
										</a>
										<div class="pad-10 text-center">
											<div class="title">
												[{{ $product['code']}}] {{ $product['name'] }}
											</div>
											<div class="pad-t-3 pricing">
												@if ($product['worthPrice'])
													<del class="pad-r-6">
														<span class="rupee-font">&#8377;</span> {{ ($product['worthPrice']) }}
													</del>
												@endif
												<span class="rupee-font">&#8377;</span> {{ ($product['minPrice']) }}
												@if ($product['maxPrice'] && $product['maxPrice'] > $product['minPrice'])
													-
													<span class="rupee-font">&#8377;</span> {{($product['maxPrice'])}}
												@endif

											</div>
										</div>
									@else
										<a href="#">No data found about this option</a>
									@endif
								@elseif($product['mapTypeId'] == config('evibe.ticket_type.venue_halls'))
									@if(!is_null($product['galleryUrl']))
										<div class="top">
											<div class="ribbon-2">
											<span>
												{{ $product['displayName']}}
											</span>
											</div>
											<div class="status @if($product['isLive']) active @else inactive @endif">
												@if($product['isLive'])
													<i class="glyphicon glyphicon-ok-sign"></i> Active
												@else
													<i class="glyphicon glyphicon-remove-sign"></i> Inactive
												@endif
											</div>
										</div>
										<a href="{{config('evibe.host').'venues/hall/'.$product['mapId']}}"
												target="_blank"><img
													src="{{$product['galleryUrl']}}"
													alt="This is one image" width="150" height="200">
										</a>
										<div class="pad-10 text-center">
											<div class="title">
												[{{ $product['code']}}] {{ $product['name'] }}
											</div>
											<div class="pad-t-3 pricing">
												{{'---'}}
											</div>
										</div>
									@else
										<a href="#">No data found about this option</a>
									@endif
								@elseif($product['mapTypeId'] == config('evibe.ticket_type.trends'))
									@if(!is_null($product['galleryUrl']))
										<div class="top">
											<div class="ribbon-2">
											<span>
												{{ $product['displayName'] }}
											</span>
											</div>
											<div class="status @if($product['isLive']) active @else inactive @endif">
												@if($product['isLive'])
													<i class="glyphicon glyphicon-ok-sign"></i> Active
												@else
													<i class="glyphicon glyphicon-remove-sign"></i> Inactive
												@endif
											</div>
										</div>
										<a href="{{config('evibe.host').'trends/view/'.$product['mapId']}}"
												target="_blank"><img
													src="{{$product['galleryUrl']}}"
													alt="This is one image" width="150" height="200">
										</a>
										<div class="pad-10 text-center">
											<div class="title">
												[{{ $product['code']}}] {{ $product['name'] }}
											</div>
											<div class="pad-t-3 pricing">
												<span class="rupee-font">&#8377; </span> {{($product['minPrice'])}}
												@if($product['maxPrice'] && $product['maxPrice']>0)
													-
													<span class="rupee-font">&#8377; </span> {{($product['maxPrice'])}}
												@endif
												@if($product['worthPrice'] && $product['worthPrice']>0)
													( <span class="rupee-font">&#8377; </span> <span
															class="text-strike">{{$product['worthPrice']}}</span> )
												@endif
											</div>
										</div>
									@else
										<a href="#">No data found about this option</a>
									@endif
								@endif
							</li>
						@endforeach
					@else
						<div class="text-bold font-22 text-danger"><i
									class="glyphicon glyphicon-info-sign"></i> No data found for this partner, you can search other partners.
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection