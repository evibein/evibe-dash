@extends("analytics.base")

@section('title')
	<title>Cumulative Analytics | EvibeDash by Evibe.in</title>
@stop

@section('analytics-content')
	<div>
		<div class="analytics-dates">
			<form action="{{ route('analytics.cumulative') }}" method="POST">
				<div class="in-blk mar-r-20">
					<label for="from">From</label>
					<input type="text" id="from" name="from"
							class="form-control"
							value="{{ \Carbon\Carbon::parse($dates['from'])->format('Y/m/d H:i') }}">
				</div>
				<div class="in-blk mar-r-20">
					<label for="to">To</label>
					<input type="text" id="to" name="to"
							class="form-control"
							value="{{ \Carbon\Carbon::parse($dates['to'])->format('Y/m/d H:i') }}">
				</div>
				<div class="in-blk">
					<button id="btnAnalyze" class="btn btn-md btn-primary text-uppercase"
							type="submit">Analyze
					</button>
				</div>
			</form>
		</div>
		<div class="analytics-data">
			<div class="col-sm-4">
				<table class="table table-bordered table-responsive table-striped">
					<tr>
						<td>Total Valid Tickets</td>
						<td><strong>{{ $tickets['count'] }}</strong></td>
					</tr>
					<tr>
						<td>Total Tickets</td>
						<td>
							<strong>{{ $tickets['allCount'] }}</strong>
							@if($tickets['allCount'])
								<div class="font-12">
									({{round(($tickets['allCount'] - $tickets['count'])/$tickets['allCount'] * 100, 2) }}% invlaid)
								</div>
								<table class="table table-bordered table-responsive table-striped mar-t-5 no-mar-b">
									<tr>
										<td>Duplicate</td>
										<td>{{ $tickets['duplicate'] }}</td>
									</tr>
									<tr>
										<td>Related</td>
										<td>{{ $tickets['related'] }}</td>
									</tr>
									<tr>
										<td>Irrelevant</td>
										<td>{{ $tickets['irrelevant'] }}</td>
									</tr>
								</table>
							@endif
						</td>
					</tr>
					<tr>
						<td>Total Parties</td>
						<td><strong>{{ $parties['count'] }}</strong></td>
					</tr>
					<tr>
						<td>Total Bookings</td>
						<td><strong>{{ $bookings['count'] }}</strong></td>
					</tr>
					<tr>
						<td>Total Revenue</td>
						<td><strong>@price($bookings['revenue'])</strong></td>
					</tr>
					<tr>
						<td>Total GMV</td>
						<td><strong>@price($bookings['gmv']['total'])</strong></td>
					</tr>
				</table>
				<div class="mar-t-20">
					<table class="table table-bordered table-responsive table-striped">
						<thead>
						<tr>
							<th colspan="2">By Parties</th>
						</tr>
						</thead>
						<tr>
							<td>Conversion %</td>
							<td><strong>{{ $parties['conversion'] }} %</strong></td>
						</tr>
						<tr>
							<td>Max. Booking</td>
							<td><strong>@price($parties['gmv']['max'])</strong></td>
						</tr>
						<tr>
							<td>Min. Booking</td>
							<td><strong>@price($parties['gmv']['min'])</strong></td>
						</tr>
						<tr>
							<td>Avg. Booking</td>
							<td><strong>@price($parties['gmv']['avg'])</strong></td>
						</tr>
					</table>
				</div>
				<div class="mar-t-20">
					<table class="table table-bordered table-responsive table-striped">
						<thead>
						<tr>
							<th colspan="2">By Orders</th>
						</tr>
						</thead>
						<tr>
							<td>Conversion % (by Orders)</td>
							<td><strong>{{ $bookings['conversion'] }} %</strong></td>
						</tr>
						<tr>
							<td>Max. Booking</td>
							<td><strong>@price($bookings['gmv']['max'])</strong></td>
						</tr>
						<tr>
							<td>Min. Booking</td>
							<td><strong>@price($bookings['gmv']['min'])</strong></td>
						</tr>
						<tr>
							<td>Avg. Booking</td>
							<td><strong>@price($bookings['gmv']['avg'])</strong></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-6 no-pad">
						<canvas id="ticketsBySource" width="100" height="100"></canvas>
					</div>
					<div class="col-sm-6 no-pad">
						<canvas id="bookingsBySource" width="100" height="100"></canvas>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="row mar-t-20">
					<div class="col-sm-6 no-pad">
						<canvas id="bookingsByOccasion" width="100" height="100"></canvas>
					</div>
					<div class="col-sm-6 no-pad">
						<canvas id="bookingsByCity" width="100" height="100"></canvas>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="row mar-t-20">
					<div class="col-sm-6 no-pad">
						<canvas id="conversionsBySource" width="100" height="100"></canvas>
					</div>
					<div class="col-sm-6 no-pad">
						<canvas id="whyChooseEvibe" width="100" height="100"></canvas>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@stop

@section('javascript')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#from,#to').datetimepicker({
				step: 30,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true
			});
			var dynamicColors = function () {
				var r = Math.floor(Math.random() * 255);
				var g = Math.floor(Math.random() * 255);
				var b = Math.floor(Math.random() * 255);
				return "rgb(" + r + "," + g + "," + b + ")";
			};

			var toolTipLabel = function (tooltipItem, data, isPercentage) {
				var allData = data.datasets[tooltipItem.datasetIndex].data;
				var tooltipLabel = data.labels[tooltipItem.index];
				var tooltipData = allData[tooltipItem.index];
				var total = 0;
				for (var i in allData) {
					total += allData[i];
				}
				var tooltipPercentage = Math.round((tooltipData / total) * 100);
				tooltipData = isPercentage ? tooltipData + '%' : tooltipData;

				return isPercentage ? tooltipData : tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
			};

			var legendStyle = {
				display: true,
				position: 'bottom',
				labels: {
					boxWidth: 20,
					fontSize: 10,
					padding: 4
				}
			};

			var sourcesData = '<?php echo json_encode($sources); ?>';
			sourcesData = JSON.parse(sourcesData);

			var sLabels = [];
			var sBg = [];
			var tsData = [];
			var bsData = [];
			var csData = [];

			for (var item in sourcesData) {
				if (sourcesData.hasOwnProperty(item)) {
					var ci = sourcesData[item];
					sLabels.push(ci['name']);
					tsData.push(ci['ticketsCount']);
					bsData.push(ci['bookingCount']);
					csData.push(ci['conversion']);
					sBg.push(dynamicColors());
				}
			}

			// bookings by city
			var bookingsCityData = [];
			var bookingsCityLabels = [];
			var bookingsCityBgs = [];
			var citiesData = '<?php echo json_encode($bookings["category"]["city"]) ?>';
			citiesData = JSON.parse(citiesData);

			for (item in citiesData) {
				if (citiesData.hasOwnProperty(item)) {
					var cityVal = citiesData[item];
					bookingsCityLabels.push(cityVal['name']);
					bookingsCityData.push(cityVal['count']);
					bookingsCityBgs.push(dynamicColors());
				}
			}

			// bookings by occasion
			var bookingsOccasionData = [];
			var bookingsOccasionLabels = [];
			var bookingsOccasionBgs = [];
			var occasionsData = '<?php echo json_encode($bookings["category"]["occasion"]) ?>';
			occasionsData = JSON.parse(occasionsData);

			for (item in occasionsData) {
				if (occasionsData.hasOwnProperty(item)) {
					var occasionVal = occasionsData[item];
					bookingsOccasionLabels.push(occasionVal['name']);
					bookingsOccasionData.push(occasionVal['count']);
					bookingsOccasionBgs.push(dynamicColors());
				}
			}

			// why choose evibe.in
			var whyChooseEvibeData = [];
			var whyChooseEvibeLabel = [];
			var whyChooseEvibeBag = [];
			var whyChooseEvibe = '<?php echo json_encode($whyChooseEvibe) ?>';
			whyChooseEvibe = JSON.parse(whyChooseEvibe);

			for (item in whyChooseEvibe) {
				if (whyChooseEvibe.hasOwnProperty(item)) {
					var chooseEvibeVal = whyChooseEvibe[item];
					whyChooseEvibeLabel.push(chooseEvibeVal['name']);
					whyChooseEvibeData.push(chooseEvibeVal['optionCount']);
					whyChooseEvibeBag.push(dynamicColors());
				}
			}

			// tickets by source
			new Chart($('#ticketsBySource'), {
				type: 'pie',
				data: {
					labels: sLabels,
					datasets: [{
						data: tsData,
						backgroundColor: sBg,
						borderColor: sBg,
						borderWidth: 1
					}]
				},
				options: {
					legend: legendStyle,
					title: {
						display: true,
						text: 'Tickets by Source'
					},
					tooltips: {
						callbacks: {
							label: function (tooltipItem, data) {
								return toolTipLabel(tooltipItem, data);
							}
						}
					}
				}
			});

			// bookings by source
			new Chart($('#bookingsBySource'), {
				type: 'pie',
				data: {
					labels: sLabels,
					datasets: [{
						data: bsData,
						backgroundColor: sBg,
						borderColor: sBg,
						borderWidth: 1
					}]
				},
				options: {
					legend: legendStyle,
					title: {
						display: true,
						text: 'Bookings by Source'
					},
					tooltips: {
						callbacks: {
							label: function (tooltipItem, data) {
								return toolTipLabel(tooltipItem, data);
							}
						}
					}
				}
			});

			// conversions by source
			new Chart($('#conversionsBySource'), {
				type: 'bar',
				data: {
					labels: sLabels,
					datasets: [{
						data: csData,
						backgroundColor: sBg,
						borderColor: sBg,
						borderWidth: 1
					}]
				},
				options: {
					scales: {
						xAxes: [{
							stacked: true
						}],
						yAxes: [{
							stacked: true
						}]
					},
					legend: {
						display: false
					},
					title: {
						display: true,
						text: 'Conversions % by Source'
					},
					tooltips: {
						callbacks: {
							label: function (tooltipItem, data) {
								return toolTipLabel(tooltipItem, data, true);
							}
						}
					}
				}
			});

			// bookings by city
			new Chart($('#bookingsByCity'), {
				type: 'pie',
				data: {
					labels: bookingsCityLabels,
					datasets: [{
						data: bookingsCityData,
						backgroundColor: bookingsCityBgs,
						borderColor: bookingsCityBgs,
						borderWidth: 1
					}]
				},
				options: {
					legend: legendStyle,
					title: {
						display: true,
						text: 'Bookings by City'
					},
					tooltips: {
						callbacks: {
							label: function (tooltipItem, data) {
								return toolTipLabel(tooltipItem, data, false);
							}
						}
					}
				}
			});

			// bookings by occasion
			new Chart($('#bookingsByOccasion'), {
				type: 'pie',
				data: {
					labels: bookingsOccasionLabels,
					datasets: [{
						data: bookingsOccasionData,
						backgroundColor: bookingsOccasionBgs,
						borderColor: bookingsOccasionBgs,
						borderWidth: 1
					}]
				},
				options: {
					legend: legendStyle,
					title: {
						display: true,
						text: 'Bookings by Occasion'
					},
					tooltips: {
						callbacks: {
							label: function (tooltipItem, data) {
								return toolTipLabel(tooltipItem, data, false);
							}
						}
					}
				}
			});

			// why choose evibe by option
			new Chart($('#whyChooseEvibe'), {
				type: 'pie',
				data: {
					labels: whyChooseEvibeLabel,
					datasets: [{
						data: whyChooseEvibeData,
						backgroundColor: whyChooseEvibeBag,
						borderColor: whyChooseEvibeBag,
						borderWidth: 1
					}]
				},
				options: {
					legend: legendStyle,
					title: {
						display: true,
						text: 'Why Choose Evibe.in'
					},
					tooltips: {
						callbacks: {
							label: function (tooltipItem, data) {
								return toolTipLabel(tooltipItem, data, false);
							}
						}
					}
				}
			});
		});
	</script>
@stop
