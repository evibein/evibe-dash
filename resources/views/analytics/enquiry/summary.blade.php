@extends("analytics.base")

@section("title")
	<title>Enquiries Summary Analytics</title>
@endsection

@section('custom-head')
	<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
@endsection

@section('analytics-content')
	<div class="text-center mar-b-20">
		@php
			$selectedOccasionValues = count(request()->input('occasion')) > 0 ? request()->input('occasion') : [];
			$selectedEnquirySourceValues = count(request()->input('enquirySource')) > 0 ? request()->input('enquirySource') : [];
			$selectedTicketSourceValues = count(request()->input('customerSource')) > 0 ? request()->input('customerSource') : [];
			$selectedCityValues = count(request()->input('city')) > 0 ? request()->input('city') : [];
			$selectedTicketStatusValues = count(request()->input('ticketStatus')) > 0 ? request()->input('ticketStatus') : [];
		@endphp
		<form action="{{ request()->url() }}" method="POST">
			<div>
				<div class="text-left">
					<div class="col-sm-3 no-pad-l">
						<label for="from">From</label>
						<input type="text" id="from" name="from" class="form-control" value="{{ \Carbon\Carbon::parse($data["dates"]['from'])->format('Y/m/d H:i') }}">
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="to">To</label>
						<input type="text" id="to" name="to" class="form-control" value="{{ \Carbon\Carbon::parse($data["dates"]['to'])->format('Y/m/d H:i') }}">
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="occasion">By Occasion</label>
						<select class="form-control chosen-select" multiple name="occasion[]" data-placeholder="Select Occasions" id="occasion">
							@foreach($data["filters"]['typeEvents'] as $key => $event)
								<option value="{{$key}}" @if(in_array($key, $selectedOccasionValues))selected @endif>{{$event["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="enquirySource">By Enquiry Source</label>
						<select class="form-control chosen-select" multiple name="enquirySource[]" data-placeholder="Select Enquiry Source" id="enquirySource">
							<option @if(in_array("scheduled", $selectedEnquirySourceValues))selected @endif value="scheduled">Schedule (Non-Phone)</option>
							@foreach($data["filters"]['enquirySources'] as $key => $enquirySource)
								<option value="{{$key}}" @if(in_array($key, $selectedEnquirySourceValues))selected @endif>{{$enquirySource["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="mar-t-10 text-left">
					<div class="col-sm-3 no-pad-l">
						<label for="customerSource">By Customer Source</label>
						<select class="form-control chosen-select" multiple name="customerSource[]" data-placeholder="Select Customer Source" id="customerSource">
							@foreach($data["filters"]['ticketSources'] as $key => $customerSource)
								<option value="{{$key}}" @if(in_array($key, $selectedTicketSourceValues))selected @endif>{{$customerSource["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="city">By City</label>
						<select class="form-control chosen-select" multiple name="city[]" data-placeholder="Select City" id="city">
							@foreach($data["filters"]['cities'] as $key => $city)
								<option value="{{$key}}" @if(in_array($key, $selectedCityValues))selected @endif>{{$city["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="ticketStatus">By Status</label>
						<select class="form-control chosen-select" multiple name="ticketStatus[]" data-placeholder="Select Status" id="ticketStatus">
							<option @if(in_array("valid", $selectedTicketStatusValues))selected @endif value="valid">Valid Status</option>
							<option @if(in_array("invalid", $selectedTicketStatusValues))selected @endif value="invalid">Invalid Status</option>
							@foreach($data["filters"]['ticketStatus'] as $key => $ticketState)
								<option value="{{$key}}" @if(in_array($key, $selectedTicketStatusValues))selected @endif>{{$ticketState["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="timeline">By Timeline</label>
						<select class="form-control" name="timeline" id="timeline">
							<option @if("all" == request("timeline"))selected @endif value="all">All</option>
							<option @if("new" == request("timeline"))selected @endif value="new">New</option>
							<option @if("return" == request("timeline"))selected @endif value="return">Return</option>
						</select>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="mar-t-10">
					<div class="col-sm-3 text-center">
						<button class="btn btn-primary mar-t-15 font-16" type="submit">Apply</button>
						@if($data["resetFilter"])
							<a class="btn btn-warning mar-t-15 font-16 mar-l-10" href="{{ route('analytics.detailed.enquiries') }}">Reset Filters</a>
						@endif
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
	<div>
		<div class="col-sm-6">
			@if(request("timeline") == "all" || is_null(request("timeline")))
				<canvas id="timelineGraph" height="100" width="100"></canvas>
			@endif
			<canvas id="ticketStatusGraph" height="100" width="100"></canvas>
			<canvas id="enquirySourceGraph" height="100" width="100"></canvas>
			<canvas id="customerCities" height="100" width="100"></canvas>
		</div>
		<div class="col-sm-6">
			<canvas id="occasionGraph" height="100" width="100"></canvas>
			<canvas id="ticketSource" height="100" width="100"></canvas>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section('javascript')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
	<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			var dynamicColors = function () {
				var r = Math.floor(Math.random() * 255);
				var g = Math.floor(Math.random() * 255);
				var b = Math.floor(Math.random() * 255);
				return "rgb(" + r + "," + g + "," + b + ")";
			};

			var legendStyle = {
				display: true,
				position: 'bottom',
				labels: {
					boxWidth: 20,
					fontSize: 10,
					padding: 4
				}
			};

			$('#from,#to').datetimepicker({
				step: 30,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true
			});

			(function showPrimaryOccasionsData() {
				$data = '<?php echo json_encode($data["count"]["occasions"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#occasionGraph"), "Occasions")
			})();

			(function showEnquirySources() {
				$data = '<?php echo json_encode($data["count"]["enquirySources"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#enquirySourceGraph"), "Enquiry Sources")
			})();

			(function showTicketSources() {
				$data = '<?php echo json_encode($data["count"]["sources"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#ticketSource"), "Customer Sources")
			})();

			(function showCities() {
				$data = '<?php echo json_encode($data["count"]["cities"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#customerCities"), "Cities")
			})();

			(function showStatus() {
				$data = '<?php echo json_encode($data["count"]["endStatus"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#ticketStatusGraph"), "Status")
			})();

			(function showTimeline() {
				if ($("#timelineGraph").length > 0) {
					$data = '<?php echo json_encode($data["count"]["timeline"]); ?>';
					$data = JSON.parse($data);

					showDefaultGraph($data, $("#timelineGraph"), "New/Return")
				}
			})();

			(function loadCssForMultipleSelect() {
				$(".chosen-select").chosen({
					no_results_text: "Oops, nothing found!",
					width: "100%"
				})
			})();

			function showDefaultGraph($data, $component, $title) {
				var sLabels = [];
				var sBg = [];
				var tsData = [];

				var sum = 0;
				$.each($data, function (index, value) {
					var capacity = parseInt(value, 10);
					sum += capacity;
				});

				for (var item in $data) {
					if ($data.hasOwnProperty(item)) {
						var ci = $data[item];
						sLabels.push(item);
						tsData.push(ci);
						sBg.push(dynamicColors());
					}
				}

				new Chart($component, {
					type: "doughnut",
					data: {
						labels: sLabels,
						datasets: [{
							data: tsData,
							backgroundColor: sBg,
							borderColor: sBg,
							borderWidth: 1
						}]
					},
					options: {
						legend: legendStyle,
						title: {
							display: true,
							text: $title + " (" + sum + ")",
							fontStyle: "bold",
							fontColor: "#666",
							fontSize: "17"
						},
						tooltips: {
							callbacks: {
								label: function (tooltipItem, data) {
									var allData = data.datasets[tooltipItem.datasetIndex].data;
									var tooltipLabel = data.labels[tooltipItem.index];
									var tooltipData = allData[tooltipItem.index];
									var total = 0;
									for (var i in allData) {
										total += allData[i];
									}
									var tooltipPercentage = Math.round((tooltipData / total) * 100);
									return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
								}
							}
						}
					}
				});
			}
		});
	</script>
@stop