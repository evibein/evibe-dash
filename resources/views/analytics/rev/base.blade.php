@extends("analytics.base")

@section("analytics-content")
	<div class="analytics-data analytics-revenue">
		<div>
			<div class="pull-right">
				<label for="revLevel" class="pull-left mar-r-10">Select Category: </label>
				<select name="level" id="revLevel" class="pull-right">
					<option value="summary" @if(!$level || $level=="summary") selected @endif>Summary</option>
					<option value="by_city" @if($level=="by_city") selected @endif>By City</option>
					<option value="by_occasion" @if($level=="by_occasion") selected @endif>By Occasion</option>
					<option value="by_category" @if($level=="by_category") selected @endif>By Category</option>
					<option value="by_book_method" @if($level=="by_book_method") selected @endif>By Booking Method
					</option>
					<option value="by_source" @if($level=="by_source") selected @endif>By Source</option>
				</select>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="mar-t-10">
			@yield("rev-data")
		</div>
	</div>
@endsection