@extends("analytics.rev.base")

@section("title")
	<title>Revenue Summary Analytics</title>
@endsection

@section("rev-data")
	<div class="text-center mar-b-20">
		<form action="{{ request()->url() }}" method="GET">
			<div class="in-blk mar-r-20">
				<label for="from">Start Date</label>
				<input type="date" id="from" name="from"
						class="form-control"
						value="{{ \Carbon\Carbon::parse($dates['from'])->format('Y-m-d') }}">
			</div>
			<div class="in-blk mar-r-20">
				<label for="to">End Date</label>
				<input type="date" id="to" name="to"
						class="form-control"
						value="{{ \Carbon\Carbon::parse($dates['to'])->format('Y-m-d') }}">
			</div>
			<div class="in-blk">
				<button id="btnAnalyze" class="btn btn-md btn-primary text-uppercase"
						type="submit">Analyze
				</button>
			</div>
		</form>
	</div>
	<div>
		<div class="col-sm-6">
			<div class="">
				<table class="table table-bordered  table-city">
					<tr class="success">
						<th colspan="4">By City</th>
					</tr>
					<tr>
						<th>City</th>
						<th>Count</th>
						<th>Revenue</th>
						<th>Rev. Share</th>
					</tr>
					@foreach($rData["by_city"]["list"] as $key => $cData)
						<tr>
							<td>{{ $cData["values"]["name"] }}</td>
							<td>{{ $cData["values"]["count"] }}</td>
							<td>
								<span class="rupee-font">&#8377;</span>
								<span>{{ round($cData["values"]["revenue"]/100000, 2) }}L</span>
							</td>
							<td>
								@if($rData["by_city"]["values"]["revenue"])
									{{ round($cData["values"]["revenue"] / $rData["by_city"]["values"]["revenue"], 4)*100 }}%
								@else
									0%
								@endif
							</td>
						</tr>
					@endforeach
				</table>
			</div>
			<div class=" mar-t-10">
				<table class="table table-bordered table-category">
					<tr class="success">
						<th colspan="4">By Category</th>
					</tr>
					<tr>
						<th>Category</th>
						<th>Count</th>
						<th>Revenue</th>
						<th>Rev. Share</th>
					</tr>
					@foreach($rData["by_category"]["list"] as $key => $catData)
						<tr>
							<td>{{ $catData["values"]["name"] }}</td>
							<td>{{ $catData["values"]["count"] }}</td>
							<td>
								<span class="rupee-font">&#8377;</span>
								<span>{{ round($catData["values"]["revenue"]/100000, 2) }}L</span>
							</td>
							<td>
								@if($rData["by_category"]["values"]["revenue"])
									{{ round($catData["values"]["revenue"] / $rData["by_category"]["values"]["revenue"], 4)*100 }}%
								@else
									0%
								@endif
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
		<div class="col-sm-6">
			<div>
				<table class="table table-bordered table-occasion">
					<tr class="success">
						<th colspan="4">By Occasion</th>
					</tr>
					<tr>
						<th>Occasion</th>
						<th>Count</th>
						<th>Revenue</th>
						<th>Rev. Share</th>
					</tr>
					@foreach($rData["by_occasion"]["list"] as $key => $oData)
						<tr>
							<td>{{ $oData["values"]["name"] }}</td>
							<td>{{ $oData["values"]["count"] }}</td>
							<td>
								<span class="rupee-font">&#8377;</span>
								<span>{{ round($oData["values"]["revenue"]/100000, 2) }}L</span>
							</td>
							<td>
								@if($rData["by_occasion"]["values"]["revenue"])
									{{ round($oData["values"]["revenue"] / $rData["by_occasion"]["values"]["revenue"], 4)*100 }}%
								@else
									0%
								@endif
							</td>
						</tr>
					@endforeach
				</table>
			</div>
			<div class="mar-t-10">
				<table class="table table-bordered table-book-method">
					<tr class="success">
						<th colspan="4">By Booking Method</th>
					</tr>
					<tr>
						<th>Method</th>
						<th>Count</th>
						<th>Revenue</th>
						<th>Rev. Share</th>
					</tr>
					@foreach($rData["by_book_method"]["list"] as $key => $bmData)
						<tr>
							<td>{{ $bmData["values"]["name"] }}</td>
							<td>{{ $bmData["values"]["count"] }}</td>
							<td>
								<span class="rupee-font">&#8377;</span>
								<span>{{ round($bmData["values"]["revenue"]/100000, 2) }}L</span>
							</td>
							<td>
								@if($rData["by_book_method"]["values"]["revenue"])
									{{ round($bmData["values"]["revenue"] / $rData["by_book_method"]["values"]["revenue"], 4)*100 }}%
								@else
									0%
								@endif
							</td>
						</tr>
					@endforeach
				</table>
			</div>
			<div>
				<table class="table table-bordered table-occasion">
					<tr class="success">
						<th colspan="4">By Source</th>
					</tr>
					<tr>
						<th>Source</th>
						<th>Count</th>
						<th>Revenue</th>
						<th>Rev. Share</th>
					</tr>
					@foreach($rData["by_source"]["list"] as $key => $oData)
						<tr>
							<td>{{ $oData["values"]["name"] }}</td>
							<td>{{ $oData["values"]["count"] }}</td>
							<td>
								<span class="rupee-font">&#8377;</span>
								<span>{{ round($oData["values"]["revenue"]/100000, 2) }}L</span>
							</td>
							<td>
								@if($rData["by_source"]["values"]["revenue"])
									{{ round($oData["values"]["revenue"] / $rData["by_source"]["values"]["revenue"], 4)*100 }}%
								@else
									0%
								@endif
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection