@extends("analytics.rev.base")

@section("title")
	<title>Revenue - {{ $rData[$level]["values"]["name"] }} Analysis</title>
@endsection

@section("rev-data")
	<div>
		@foreach($rData[$level]["list"] as $subList)
			<div class="col-sm-6">
				<table class="table table-bordered">
					<tr>
						<td class="success">
							<b>{{ $rData[$level]["values"]["name"] }}: {{ $subList["values"]["name"] }} [{{ $subList["values"]["count"] }}]</b>
						</td>
					</tr>
					@foreach($subList["levels"] as $levelData)
						<tr class="warning">
							<td>{{ $levelData["values"]["name"] }}</td>
						</tr>
						<tr>
							<td>
								<table class="table table-bordered table-condensed no-mar-b">
									<thead>
									<tr>
										<th>Name</th>
										<th>Count</th>
										<th>Revenue</th>
										<th>Rev. Share</th>
									</tr>
									</thead>
									<tbody>
									@foreach($levelData["list"] as $levelListData)
										<tr>
											<td>{{ $levelListData["values"]["name"] }}</td>
											<td>{{ $levelListData["values"]["count"] }}</td>
											<td>
												<span class="rupee-font">&#8377;</span>
												<span> {{ round($levelListData["values"]["revenue"] / 100000, 2) }}L</span>
											</td>
											<td>
												@if($levelData["values"]["revenue"])
													{{ round($levelListData["values"]["revenue"] / $levelData["values"]["revenue"], 4)*100 }}%
												@else
													0%
												@endif
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		@endforeach
		<div class="clearfix"></div>
	</div>
@endsection