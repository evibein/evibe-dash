@extends('layout.base')

@section('title')
	<title>Funnel Chart | EvibeDash by Evibe.in(Reffer and Earn)</title>
@stop
@section('content')
	<div class="container-wrap analytics-wrap">
		<div class="container">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="analytics-dates">
						<form action="{{ route('analytics.refer') }}" method="GET" class="dateFilter">
							<div class="in-blk mar-r-20">
								<label for="from">From</label>
								<input type="date" id="from" name="from"
										class="form-control"
										value="{{ \Carbon\Carbon::parse($dates['from'])->format('Y-m-d') }}">
							</div>
							<div class="in-blk mar-r-20">
								<label for="to">To</label>
								<input type="date" id="to" name="to"
										class="form-control"
										value="{{ \Carbon\Carbon::parse($dates['to'])->format('Y-m-d') }}">
							</div>
							<div class="in-blk">
								<button id="btnAnalyze" class="btn btn-md btn-primary text-uppercase"
										type="submit">Analyze
								</button>
							</div>
						</form>
					</div>
					<div class="analytics-data">
						<div class="col-sm-3">
							<div class="mar-t-20">
								<table class="table table-bordered table-hover text-center">
									<tbody>
									<tr>
										<td>Share<br><b class="text-info">{{$share}}</b></td>
									</tr>
									<tr>
										<td>Views<br><b class="text-info">{{$views}}</b></td>
									</tr>
									<tr>
										<td>Signups<br><b class="text-info">{{$signups}}</b></td>
									</tr>
									<tr>
										<td>Booked<br><b class="text-info">{{$booked}}</b></td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-sm-9 text-center">
							<div class="row" style=alignment:left>
								<div class="col-sm-12 no-pad">
									<div id="chart-container">FusionCharts will render here</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('javascript')
	<script src="https://static.fusioncharts.com/code/latest/fusioncharts.js?cacheBust=8232"></script>
	<script src="https://csm.fusioncharts.com/projects/themes/hulk-light.js?cacheBust=8232"></script>
	<script type="text/javascript">
		function drawChart() {
			var chartData = '<?php echo json_encode($funnelData); ?>';
			chartData = JSON.parse(chartData);

			FusionCharts.ready(function () {
				new FusionCharts({
					type: 'funnel',
					renderAt: 'chart-container',
					width: '500',
					height: '400',
					dataFormat: 'json',
					dataSource: {
						"chart": {
							"caption": "Refer & Earn Data Analysis",
							"subcaption": "Analysis of RE based on the data stored in DB",
							"paletteColors": "#1aaf5d,#8e0000,#f2c500,#4285F4",
							"bgColor": "#ffffff",
							"decimals": "1",
							"showBorder": "0",
							"is2D": "1",
							"labelDistance": "15",
							"isHollow": "0",
							showLegend: "1",
							"useSameSlantAngle": "1",
							"plotTooltext": "Count : $percentOfPrevValue",
							"theme": "hulk-light"
						},
						"data": chartData
					}
				}).render();
			});
		}

		drawChart();
	</script>
@stop