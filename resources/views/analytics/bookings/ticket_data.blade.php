@if($data["ticketData"]->count() > 0)
	<table id="example" class="display" style="width:100%">
		<thead>
		<tr>
			<th></th>
			<th>Name (Phone)</th>
			<th>Party Date</th>
			<th>Source (Customer / Enquiry)</th>
			<th>Handler (Current / Created)</th>
			<th>City</th>
			<th>Occasion</th>
		</tr>
		</thead>
		<tbody>
		@foreach($data["ticketData"]->unique("ticket_id") as $ticket)
			@php
				$typeEnquirySourceOfTicket = isset($data["typeEnquirySource"][$ticket["enquiry_source_id"]])? $data["typeEnquirySource"][$ticket["enquiry_source_id"]] : '';
				$typeTicketSourceOfTicket = isset($data["typeTicketSource"][$ticket["source_id"]])? $data["typeTicketSource"][$ticket["source_id"]] : '';
				$handlerDetailsOfTicket = isset($data["handlerDetails"][$ticket["handler_id"]])? $data["handlerDetails"][$ticket["handler_id"]] : '';
				$createdHandlerDetailsOfTicket = isset($data["handlerDetails"][$ticket["created_handler_id"]])? $data["handlerDetails"][$ticket["created_handler_id"]] : '';
				$cityDetailsOfTicket = isset($data["cityDetails"][$ticket["city_id"]])? $data["cityDetails"][$ticket["city_id"]] : '';
				$eventTypeDetailsOfTicket = isset($data["eventTypeDetails"][$ticket["event_id"]])? $data["eventTypeDetails"][$ticket["event_id"]] : '';
			@endphp
			<tr>
				<td data-value="{{ $ticket["ticket_id"] }}"></td>
				<td>{{ $ticket["name"] }}<br>{{ $ticket["phone"] }}</td>
				<td>
					@if($ticket["event_date"])
						<div class="text-white font-1">{{ date('y/m/d H:i:s', $ticket["event_date"])}}</div>
						{{ date("d M y, g:i a", $ticket["event_date"]) }}
					@else
						<div class="text-white font-1">9999</div>
						---
					@endif
				</td>
				<td>
					@if($typeEnquirySourceOfTicket) {{ ucfirst($typeEnquirySourceOfTicket) }} @else {{ "---" }} @endif
					/ @if($typeTicketSourceOfTicket) {{ ucfirst($typeTicketSourceOfTicket) }} @else {{ "---" }} @endif
				</td>
				<td>
					@if($handlerDetailsOfTicket) {{ucfirst($handlerDetailsOfTicket)}} @else {{ "---" }} @endif
					/ @if($createdHandlerDetailsOfTicket) {{ucfirst($createdHandlerDetailsOfTicket)}} @else {{'---'}} @endif
				</td>
				<td>
					@if($cityDetailsOfTicket) {{ucfirst($cityDetailsOfTicket)}} @else {{ "---" }} @endif
				</td>
				<td>
					@if($eventTypeDetailsOfTicket) {{ucfirst($eventTypeDetailsOfTicket)}} @else {{ "---" }} @endif
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	<table class="specific-ticket-bookings-data hide">
		@foreach($data["ticketData"] as $ticket)
			@php
				$productType = isset($data["typeTicketBookingSource"][$ticket["product_type_id"]])? $data["typeTicketBookingSource"][$ticket["product_type_id"]] : '';
			@endphp
			<tr class="specific-ticket-booking-data-{{ $ticket["ticket_id"] }}">
				<td>{{ $ticket["booking_id"] }}</td>
				<td>{!! $productType !!}</td>
				<td>{{ $ticket["booking_amount"] }}</td>
			</tr>
		@endforeach
	</table>
@endif