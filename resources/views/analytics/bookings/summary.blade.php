@extends("analytics.base")

@section("title")
	<title>Enquiries Summary Analytics</title>
@endsection

@section('custom-head')
	<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
@endsection

@section('analytics-content')
	<div class="text-center mar-b-20">
		@php
			$selectedOccasionValues = count(request()->input('occasion')) > 0 ? request()->input('occasion') : [];
			$selectedEnquirySourceValues = count(request()->input('enquirySource')) > 0 ? request()->input('enquirySource') : [];
			$selectedTicketSourceValues = count(request()->input('customerSource')) > 0 ? request()->input('customerSource') : [];
			$selectedCityValues = count(request()->input('city')) > 0 ? request()->input('city') : [];
		@endphp
		<form action="{{ request()->url() }}" method="POST">
			<div>
				<div class="text-left">
					<div class="col-sm-3 no-pad-l">
						<label for="from">From</label>
						<input type="text" id="from" name="from" class="form-control" value="{{ \Carbon\Carbon::parse($data["dates"]['from'])->format('Y/m/d H:i') }}">
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="to">To</label>
						<input type="text" id="to" name="to" class="form-control" value="{{ \Carbon\Carbon::parse($data["dates"]['to'])->format('Y/m/d H:i') }}">
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="occasion">By Occasion</label>
						<select class="form-control chosen-select" multiple name="occasion[]" data-placeholder="Select Occasions" id="occasion">
							@foreach($data["filters"]['typeEvents'] as $key => $event)
								<option value="{{$key}}" @if(in_array($key, $selectedOccasionValues))selected @endif>{{$event["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="enquirySource">By Enquiry Source</label>
						<select class="form-control chosen-select" multiple name="enquirySource[]" data-placeholder="Select Enquiry Source" id="enquirySource">
							<option @if(in_array("scheduled", $selectedEnquirySourceValues))selected @endif value="scheduled">Schedule (Non-Phone)</option>
							@foreach($data["filters"]['enquirySources'] as $key => $enquirySource)
								<option value="{{$key}}" @if(in_array($key, $selectedEnquirySourceValues))selected @endif>{{$enquirySource["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="mar-t-10 text-left">
					<div class="col-sm-3 no-pad-l">
						<label for="customerSource">By Customer Source</label>
						<select class="form-control chosen-select" multiple name="customerSource[]" data-placeholder="Select Customer Source" id="customerSource">
							@foreach($data["filters"]['ticketSources'] as $key => $customerSource)
								<option value="{{$key}}" @if(in_array($key, $selectedTicketSourceValues))selected @endif>{{$customerSource["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="city">By City</label>
						<select class="form-control chosen-select" multiple name="city[]" data-placeholder="Select City" id="city">
							@foreach($data["filters"]['cities'] as $key => $city)
								<option value="{{$key}}" @if(in_array($key, $selectedCityValues))selected @endif>{{$city["name"]}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="customerGender">By Gender</label>
						<select class="form-control" name="customerGender" id="customerGender">
							<option value="">All Genders</option>
							<option @if("1" == request("customerGender"))selected @endif value="1">Male</option>
							<option @if("2" == request("customerGender"))selected @endif value="2">Female</option>
						</select>
					</div>
					<div class="col-sm-3 no-pad-l">
						<label for="bookingProcess">By Booking Process</label>
						<select class="form-control" name="bookingProcess" id="bookingProcess">
							<option value="">All Types</option>
							<option @if("0" == request("bookingProcess"))selected @endif value="0">Normal Booking</option>
							<option @if("1" == request("bookingProcess"))selected @endif value="1">Auto Booking</option>
						</select>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="mar-t-10 text-left">
					<div class="col-sm-3 no-pad-l">
						<label for="timeline">By Timeline</label>
						<select class="form-control" name="timeline" id="timeline">
							<option @if("all" == request("timeline"))selected @endif value="all">All</option>
							<option @if("new" == request("timeline"))selected @endif value="new">New</option>
							<option @if("return" == request("timeline"))selected @endif value="return">Return</option>
						</select>
					</div>
					<div class="col-sm-3 text-center">
						<button class="btn btn-primary mar-t-15 font-16" type="submit">Apply</button>
						@if($data["resetFilter"])
							<a class="btn btn-warning mar-t-15 font-16 mar-l-10" href="{{ route('analytics.detailed.bookings') }}">Reset Filters</a>
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
	<div class="pull-right mar-b-10">
		<a class="analytics-graphs-btn btn-primary pad-5">Show Tickets Data</a>
		<a class="analytics-ticket-data-btn btn-primary pad-5 hide">Show Analytics Data</a>
	</div>
	<div class="clearfix"></div>
	<div class="analytics-graphs">
		<div>
			<canvas id="enquirySourceLineGraph" height="100"></canvas>
		</div>
		<div class="col-sm-4">
			@if(request("timeline") == "all" || is_null(request("timeline")))
				<canvas id="timelineGraph" height="100" width="100"></canvas>
			@endif
			<canvas id="occasionGraph" height="100" width="100"></canvas>
			<canvas id="secondaryOccasion" height="100" width="100"></canvas>
			<canvas id="customerCities" height="100" width="100"></canvas>
		</div>
		<div class="col-sm-4">
			<canvas id="enquirySourceGraph" height="100" width="100"></canvas>
			<canvas id="customerGenderGraph" height="100" width="100"></canvas>
			<canvas id="customerBookingTypesGraph" height="100" width="100"></canvas>
		</div>
		<div class="col-sm-4">
			<canvas id="ticketSource" height="100" width="100"></canvas>
			<canvas id="customerBookingAddOnsGraph" height="100" width="100"></canvas>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="analytics-ticket-data hide">
		<a class="alert-info alert pad-10 text-center">Loading Ticket Data, Please wait....</a>
	</div>
@endsection

@section('javascript')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
	<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			var dynamicColors = function () {
				var r = Math.floor(Math.random() * 255);
				var g = Math.floor(Math.random() * 255);
				var b = Math.floor(Math.random() * 255);
				return "rgb(" + r + "," + g + "," + b + ")";
			};

			var legendStyle = {
				display: true,
				position: 'bottom',
				labels: {
					boxWidth: 20,
					fontSize: 10,
					padding: 4
				}
			};

			$('#from,#to').datetimepicker({
				step: 30,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true
			});

			$(".analytics-graphs-btn").on("click", function () {
				$(".analytics-graphs").addClass("hide");
				$(".analytics-ticket-data").removeClass("hide");
				$(".analytics-ticket-data-btn").removeClass("hide");
				$(this).addClass("hide");
			});

			$(".analytics-ticket-data-btn").on("click", function () {
				$(".analytics-ticket-data").addClass("hide");
				$(".analytics-graphs").removeClass("hide");
				$(".analytics-graphs-btn").removeClass("hide");
				$(this).addClass("hide");
			});

			$(window).load(function () {
				$data = '<?php echo json_encode(request()->all()); ?>';

				$.ajax({
					url: "/analytics/detailed/bookings/additional",
					type: 'POST',
					dataType: 'JSON',
					data: JSON.parse($data),
					success: function (data) {
						if (data.success) {
							showDefaultGraph(data.data, $("#customerBookingAddOnsGraph"), "Add-Ons")
						} else {
							window.showNotyError("Some error occurred while fetching add-ons");
						}
					},
					fail: function () {
						window.showNotyError("Some error occurred while loading the extra data of tickets");
					}
				});

				$.ajax({
					url: "/analytics/detailed/bookings/ticket-data",
					type: 'POST',
					data: JSON.parse($data),
					success: function (data) {
						$(".analytics-ticket-data").empty().append(data);

						function format(d, ticketId) {
							var template = '';
							$.each($(".specific-ticket-booking-data-" + ticketId), function () {
								template += '<tr>' + $(this).html() + '</tr>';
							});

							return '<div class="slider">' +
								'<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
								'<thead><tr><th>Booking Id</th><th>Product Type</th><th>Booking Amount</th></tr></thead>' +
								'<tbody>' +
								template +
								'</tbody></table>' +
								'</div>';
						}

						var table = $('#example').DataTable({
							"columns": [
								{
									"class": 'details-control',
									"orderable": false,
									"data": null,
									"defaultContent": ''
								},
								{"data": "name"},
								{"data": "date"},
								{"data": "source"},
								{"data": "handler"},
								{"data": "city"},
								{"data": "occasion"}
							]
						});

						// Add event listener for opening and closing details
						$('#example tbody').on('click', 'td.details-control', function () {
							var tr = $(this).closest('tr');
							var row = table.row(tr);

							if (row.child.isShown()) {
								// This row is already open - close it
								$('div.slider', row.child()).slideUp(function () {
									row.child.hide();
									tr.removeClass('shown');
								});
							} else {
								// Open this row
								row.child(format(row.data(), $(this).data("value")), 'no-padding').show();
								tr.addClass('shown');

								$('div.slider', row.child()).slideDown();
							}
						});
					},
					fail: function () {
						window.showNotyError("Some error occurred while loading the extra data of tickets");
					}
				});

				$.ajax({
					url: "/analytics/detailed/line/bookings/status",
					type: 'POST',
					data: JSON.parse($data),
					success: function (data) {
						if (data.labels.length > 1) {
							var tsData = [];

							$.each(data.ticketStatus, function (index, value) {
								var color = dynamicColors();

								tsData.push({
									label: index,
									type: 'line',
									data: $.map(value, function (value1, index) {
										return [value1];
									}),
									fill: false,
									borderColor: color,
									backgroundColor: color,
									pointBorderColor: color,
									pointBackgroundColor: color,
									pointHoverBackgroundColor: color,
									pointHoverBorderColor: color
								});
							});

							tsData.push({
								type: 'bar',
								label: "Total",
								data: $.map(data.totalCount, function (value, index) {
									return [value];
								}),
								fill: false,
								backgroundColor: '#71B37C',
								borderColor: '#71B37C',
								hoverBackgroundColor: '#71B37C',
								hoverBorderColor: '#71B37C'
							});

							console.log(tsData);

							new Chart($("#enquirySourceLineGraph"), {
								type: 'bar',
								data: {
									labels: data.labels,
									datasets: tsData
								},
								options: {
									responsive: true,
									tooltips: {
										mode: 'label'
									},
									elements: {
										line: {
											fill: false
										}
									}
								}
							});
						} else {
							$("#enquirySourceLineGraph").remove();
						}
					},
					fail: function () {
						window.showNotyError("Some error occurred while loading the extra data of tickets");
					}
				});
			});

			(function showPrimaryOccasionsData() {
				$data = '<?php echo json_encode($data["count"]["occasions"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#occasionGraph"), "Occasions")
			})();

			(function showSecondaryOccasionsData() {
				$data = '<?php echo json_encode($data["count"]["subOccasions"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#secondaryOccasion"), "Detailed Occasion")
			})();

			(function showEnquirySources() {
				$data = '<?php echo json_encode($data["count"]["enquirySources"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#enquirySourceGraph"), "Enquiry Sources")
			})();

			(function showTicketSources() {
				$data = '<?php echo json_encode($data["count"]["sources"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#ticketSource"), "Customer Sources")
			})();

			(function showCities() {
				$data = '<?php echo json_encode($data["count"]["cities"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#customerCities"), "Cities")
			})();

			(function showCustomerGender() {
				$data = '<?php echo json_encode($data["count"]["gender"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#customerGenderGraph"), "Gender")
			})();

			(function showCustomerBookingTypes() {
				$data = '<?php echo json_encode($data["count"]["bookingTypes"]); ?>';
				$data = JSON.parse($data);

				showDefaultGraph($data, $("#customerBookingTypesGraph"), "Booking Types")
			})();

			(function showTimeline() {
				if ($("#timelineGraph").length > 0) {
					$data = '<?php echo json_encode($data["count"]["timeline"]); ?>';
					$data = JSON.parse($data);

					showDefaultGraph($data, $("#timelineGraph"), "New/Return")
				}
			})();

			(function loadCssForMultipleSelect() {
				$(".chosen-select").chosen({
					no_results_text: "Oops, nothing found!",
					width: "100%"
				})
			})();

			function showDefaultGraph($data, $component, $title) {
				var sLabels = [];
				var sBg = [];
				var tsData = [];

				var sum = 0;
				$.each($data, function (index, value) {
					var capacity = parseInt(value, 10);
					sum += capacity;
				});

				for (var item in $data) {
					if ($data.hasOwnProperty(item)) {
						var ci = $data[item];
						sLabels.push(item);
						tsData.push(ci);
						sBg.push(dynamicColors());
					}
				}

				new Chart($component, {
					type: "pie",
					data: {
						labels: sLabels,
						datasets: [{
							data: tsData,
							backgroundColor: sBg,
							borderColor: sBg,
							borderWidth: 1
						}]
					},
					options: {
						legend: legendStyle,
						title: {
							display: true,
							text: $title + " (" + sum + ")",
							fontStyle: "bold",
							fontColor: "#666",
							fontSize: "17"
						},
						tooltips: {
							callbacks: {
								label: function (tooltipItem, data) {
									var allData = data.datasets[tooltipItem.datasetIndex].data;
									var tooltipLabel = data.labels[tooltipItem.index];
									var tooltipData = allData[tooltipItem.index];
									var total = 0;
									for (var i in allData) {
										total += allData[i];
									}
									var tooltipPercentage = Math.round((tooltipData / total) * 100);
									return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
								}
							}
						}
					}
				});
			}
		});
	</script>
@stop