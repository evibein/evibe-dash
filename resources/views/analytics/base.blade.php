@extends("layout.base")

@section("content")
	<div class="analytics-higher-wrap">
		<div class="col-sm-2" style="border-right: 1px solid grey">
			<ul class="nav nav-pills nav-stacked">
				<li role="presentation" class="@if(request()->is("*cumulative*")) active @endif">
					<a href="{{ route('analytics.cumulative') }}">Cumulative</a>
				</li>
				<li role="presentation" class="@if(request()->is("*detailed/revenue*")) active @endif">
					<a href="{{ route("analytics.detailed.revenue") }}">Revenue</a>
				</li>
				<li role="presentation" class="@if(request()->is("*detailed/enquiries*")) active @endif">
					<a href="{{ route("analytics.detailed.enquiries") }}">Enquiries</a>
				</li>
				<li role="presentation" class="@if(request()->is("*detailed/bookings*")) active @endif">
					<a href="{{ route("analytics.detailed.bookings") }}">Bookings</a>
				</li>
			</ul>
		</div>
		<div class="col-sm-10">
			@yield("analytics-content")
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			$("#revLevel").change(function () {
				var origin = window.location.origin;
				var pathname = window.location.pathname;
				var params = window.location.search;
				var newParams = "?";

				if (params) {
					params = params.substring(1).split("&");
					for (var key in params) {
						if (params.hasOwnProperty(key)) {
							var subParam = params[key].split("=");
							console.log("subParam: ");
							console.log(subParam[0]);
							if (subParam[0] !== "level") {
								newParams += params[key] + "&";
							}
						}
					}
				}
				newParams += "level=" + $(this).val();

				window.location = origin + pathname + newParams;
			});
		});
	</script>
@endsection
