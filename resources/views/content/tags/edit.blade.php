@extends('content.content_base')
@section('page-heading')
	<div class="pull-left"><a class="tag-link" href="{{route('content.tags.list')}}"> <<< Show Tag List </a>
	</div>Edit Tags
@endsection
@section('action_button')
@endsection
@section('body')
	<div class="mar-l-20 mar-t-20">
		<form id="editTagForm" action="{{route('content.tags.edit.save', $tag->id)}}" method="post">
			<div class="col-lg-12">
				@if($errors->count())
					<div class="alert alert-danger" style="margin-bottom: 40px">
						<button class="close" data-dismiss="alert">&times;</button>
						{{$errors->first()}}
					</div>
				@endif
				<div class="col-lg-4 no-pad-l">
					<div class="form-group">
						<select name="occasion" id="occasion" class="form-control"
								data-old="@if($tag->event && $tag->event->id){{$tag->event->id}}@else -1 @endif">
							@if($tag->event && $tag->event->id)
								<option value="{{$tag->event->id}}">{{$tag->event->name}}</option>
								<option value="-1">-- Select Occasion --</option>
							@else
								<option value="-1">-- Select Occasion --</option>
							@endif
							@foreach($occasions as $occasion)
								<option value="{{$occasion->id}}">{{$occasion->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<select name="type" id="type" class="form-control"
								data-old="@if($tag->mapType->id){{$tag->mapType->id}}@else -1 @endif">
							<option value="{{$tag->mapType->id}}">{{$tag->mapType->name}}</option>
							@foreach($typePages as $page)
								<option value="{{$page->id}}">{{$page->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4 parent-wrap">
					<label class="parent-checkbox">
						<input type="checkbox" id="isParent"
								name="isParent" @if($tag->parent_id)  checked="checked" @endif > is Child ?
					</label>
					<div class="form-group">
						@if($tag->parent)
							<select name="parentTags" id="parentTags" class="form-control" style="display: none"
									data-old="@if($tag->parent->id){{$tag->parent->id}}@else -1 @endif">
								<option value="{{$tag->parent->id}}">{{$tag->parent->name}}</option>
								@foreach($parentTags as $pTag)
									<option value="{{$pTag->id}}">{{$pTag->name}}</option>
								@endforeach
							</select>

						@else
							<select name="parentTags" id="parentTags" class="form-control" style="display: none"
									data-old="@if(old('parentTags')){{old('parentTags')}}@else -1 @endif">
								<option value="-1">-- Select Parent Tags --</option>
								@foreach($parentTags as $pTag)
									<option value="{{$pTag->id}}">{{$pTag->name}}</option>
								@endforeach
							</select>
						@endif
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="mar-t-30">
				<div class="col-lg-4">
					<div class="form-group">
						<label for="tagName">Tag Name</label>
						<input type="text" name="tagName" class="form-control" value="{{$tag->name}}">
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label for="seoTitle">SEO Title</label>
						<input type="text" name="seoTitle" class="form-control" value="{{$tag->seo_title}}">
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label for="seoTitle">SEO Desc</label>
						<input type="text" name="seoDesc" class="form-control" value="{{$tag->seo_desc}}">
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="mar-t-10">
					<div class="col-sm-8">
						<label>Identifier</label>
						<input type="text" class="form-control" name="identifier" value="{{$tag->identifier}}">
					</div>
					<div class="col-sm-4">
						<label>
							<input type="checkbox" name="isFilter" @if($tag->is_filter == 1)  checked="checked" @endif>
							Is Filter
						</label>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="mar-t-10 pad-t-20">
					<div class="col-lg-4">
						<div class="form-group">
							<select name="typeBooking" id="typeBooking" class="form-control"
									data-old="@if($tag->typeBooking && $tag->typeBooking->id){{$tag->typeBooking->id}}@else -1 @endif">
								@if($tag->typeBooking && $tag->typeBooking->id)
									<option value="{{$tag->typeBooking->id}}">{{$tag->typeBooking->name}}</option>
									<option value="-1">-- Select Booking Type --</option>
								@else
									<option value="-1">-- Select Booking Type --</option>
								@endif
								@foreach($typeBookings as $typeBooking)
									<option value="{{$typeBooking->id}}">{{$typeBooking->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="text-center mar-t-20">
					<button type="submit" id="editTagSubmit" class="btn btn-success btn-lg ">
						<i class="glyphicon glyphicon-save"></i>
						Save Tag Changes
					</button>
				</div>
			</div>
		</form>
	</div>
@endsection
@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {

			//showing hiding the tag after selecting the option tag is child or parent
			$('#isParent').change(function (event) {
				event.preventDefault();
				if ($(this).prop('checked')) {
					$('#parentTags').show();
					$('.parent-wrap').css('margin-top', '-35px')
				}
				else {
					$('#parentTags').hide();
					$('.parent-wrap').css('margin-top', '0')
				}
			});

			$('#editTagSubmit').click(function (event) {
				event.preventDefault();
				if(!($('#occasion').val() > 0))
				{
					if(!confirm('Are you sure you want to edit tag without an occasion?'))
					{
						return false;
					}
				}
				$('#editTagForm').submit();
			});

			//setting the old values
			var options = ['#occasion', '#type', '#parentTags'];

			$.each(options, function (key, value) {
				var oldVal = $(value).data('old');
				$(value).val($.trim(oldVal));
			});

			if ($('#isParent').prop('checked')) {
				$('#parentTags').show();
				$('.parent-wrap').css('margin-top', '-35px')
			}
		});

	</script>
@endsection