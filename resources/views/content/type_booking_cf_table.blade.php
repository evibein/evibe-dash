@extends('layout.base')
@section('content')
	<div class="col-lg-12">
		<div class="col-lg-12">
			<div class="col-lg-2">
				<div class="ttb-back">
					<a href="{{route('content.type_ticket_booking.list')}}">
						<button class="btn btn-link">
							<i class="glyphicon glyphicon-arrow-left"></i> type ticket bookings
						</button>
					</a>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="ttb-title text-center">
					<h4>Showing all Type Ticket Bookings with Checkout Fields</h4>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="ttb-info">
					<div class="ab-ttb-count-wrap">
						<div class="ab-ttb-color ttb-color-wrap in-blk"></div>
						<div class="ab-ttb-text in-blk">
							Auto Book Type Ticket Booking ({{ $abTtbCount }})
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="normal-ttb-count">
						<div class="normal-ttb-color ttb-color-wrap in-blk"></div>
						<div class="normal-ttb-text in-blk">
							Normal Type Ticket Bookings ({{ $normalTtbCount }})
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-lg-12">
			<div class="ticket-bookings-table-wrap mar-t-30">
				@if($typeTicketBookings && count($typeTicketBookings) > 0)
					<table class="table table-bordered">
						<tr>
							<th rowspan="2" width="6%" class="text-center">Sl No.</th>
							<th rowspan="2" class="text-center">Id</th>
							<th rowspan="2" class="text-center">TypeTicketBooking</th>
							<th colspan="5" class="text-center">
								Checkout Fields
							</th>
						</tr>
						<tr>
							<th class="text-center">Id</th>
							<th class="text-center">Name</th>
							<th class="text-center">Occasion</th>
							<th class="text-center">Is Crm</th>
							<th class="text-center">Is AB</th>
						</tr>
						@php $i = 1; @endphp
						@foreach($typeTicketBookings as $typeTicketBooking)
							@php $cf =  $typeTicketBooking->checkoutFields->count() ? $typeTicketBooking->checkoutFields->count()+1 : 1; @endphp
							<tr>
								<td rowspan="{{ $cf }}">{{ $i }}</td>
								<td rowspan="{{ $cf }}" class="text-bold">{{ $typeTicketBooking->id }}</td>
								<td rowspan="{{ $cf }}"
										class="@if($typeTicketBooking->for_auto_booking == 1)ab-ttb-color @else normal-ttb-color @endif">
									{{ $typeTicketBooking->name }}</td>
							</tr>
							@php $checkoutFields = $typeTicketBooking->checkoutFields @endphp
							@if($checkoutFields->count() > 0)
								@foreach($checkoutFields as $checkoutField)
									<tr>
										<td>{{ $checkoutField->id }}</td>
										<td>{{ $checkoutField->identifier }}</td>
										<td>
											@if($checkoutField->event_id)
												{{ config('evibe.type-event-title.'.$checkoutField->event_id) }}
											@else
												N/A
											@endif
										</td>
										<td width="10%" class="text-center">@if($checkoutField->is_crm) <i
													class="glyphicon glyphicon-ok text-success"> </i>@else <i
													class="glyphicon glyphicon-remove text-danger"></i> @endif</td>
										<td width="10%" class="text-center">@if($checkoutField->is_auto_booking) <i
													class="glyphicon glyphicon-ok text-success"> </i>@else <i
													class="glyphicon glyphicon-remove text-danger"></i> @endif</td>
									</tr>
								@endforeach
							@endif
							@php $i++; @endphp
						@endforeach
					</table>
				@else
					<div class="pad-t-30">
					<h3 class="text-center pad-t-20 pad-b-20 border-red">No typeTicketBookings were found.</h3>
					</div>
				@endif
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection
@section('page-heading')
	Showing all Type Ticket Bookings with Checkout Fields
@endsection
@section('action_button')
	<div class="ab-ttb-count-wrap">
		<div class="ab-ttb-color in-blk"></div>
		<div class="ab-ttb-text in-blk">
			Auto Booking TTB ({{ $abTtbCount }})
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="normal-ttb-count">
		<div class="normal-ttb-color in-blk"></div>
		<div class="normal-ttb-text in-blk">
			Normal TTB ({{ $normalTtbCount }})
		</div>
		<div class="clearfix"></div>
	</div>
@endsection
@section('body')
	@if(isset($typeTicketBookings) && $typeTicketBookings->count() > 0)
		<table class="table table-bordered">
			<tr>
				<th rowspan="2">Sl No.</th>
				<th rowspan="2">Id</th>
				<th rowspan="2">TypeTicketBooking</th>
				<th>
					Checkout Fields
				</th>
			</tr>
		</table>
	@endif
	@if(isset($allTypeBookings))
		@if(count($allTypeBookings) > 0)
			<a class="type-booking-table-btn pull-right mar-b-10" href="{{route('content.type_ticket_booking.table')}}">
				Type Booking Checkout Fields
			</a>
			<table class="table table-bordered">
				<tr>
					<th>Sl No.</th>
					<th>Name</th>
					<th>For Auto Booking</th>
					@if(AppUtil::isAdmin())
						<th class="text-center">Action</th>
					@endif
				</tr>
				<?php $slNo = 1 ?>
				@foreach($allTypeBookings as $typeTicketBooking)
					<tr>
						<td>{{$slNo}}</td>
						<td>{{$typeTicketBooking->name}}</td>
						<td width="20%" class="text-center">@if($typeTicketBooking->for_auto_booking) <i
									class="glyphicon glyphicon-ok text-success"> </i>@else <i
									class="glyphicon glyphicon-remove text-danger"></i> @endif</td>
						@if(AppUtil::isAdmin())
							<td class="text-center">
								<a class="btn btn-danger btn-xs mar-l-20"
										href="{{route('content.type_ticket_booking.delete',$typeTicketBooking->id)}}"
										onclick="return confirm('Are you sure?','Yes','No')">
									<i class="glyphicon glyphicon-trash"></i> </a>
							</td>
						@endif
						<?php $slNo++?>
					</tr>
				@endforeach
			</table>
			<div class="text-center">
				{{$allTypeBookings->appends(Input::except('Page'))->links()}}
			</div>
		@else
			<div class="alert-danger pad-10">
				No Type ticket booking found, Click <b>Add Type Ticket Booking</b> Button above to add one.
			</div>
		@endif
	@endif
@endsection
@section('modal')
	<div class="modal fade" id="modalTypeTicketBooking" data-url="" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger hide"></div>
					<form class="form" id="name">
						<div class="form-group">
							<label>Ticket Booking Type</label>
							<input type="text" name="typeTicketBooking" id="typeTicketBooking" class="form-control"
									placeholder="Enter the ticket booking type">
						</div>
						<div class="form-group">
							<label>
								<input type="checkbox" id="forAutoBooking" name="forAutoBooking">
								For Auto Booking
							</label>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary content-type-ticket-booking-submit" id="">Save</button>
				</div>
			</div>
		</div>
	</div>
@endsection