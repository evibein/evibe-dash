@extends('content.content_base')
@section('page-heading')
	List of All Tracking Questions.
@endsection
@section('action_button')
	<a class="add-tracking-questions" data-url="{{route('content.tracking.questions.add')}}">
		<button class="" id="addButton">Add Question
		</button>
	</a>
@endsection
@section('body')
	@if($trackQuestions->count())
		<table class="table table-bordered">
			<tr>
				<th>Sl No.</th>
				<th>Name</th>
				<th>Is Partner</th>
				<th>Is Customer</th>
				@if(AppUtil::isAdmin())
					<th class="text-center">Action</th>
				@endif
			</tr>
			<?php $slNo = 1 ?>
			@foreach($trackQuestions as $trackQuestion)
				<tr>
					<td>{{$slNo}}</td>
					<td>{!! $trackQuestion->question !!}</td>
					<td>
						@if($trackQuestion->is_partner == 1)
							<div class="text-center">
								<div class="text-success btn-xs">
									<i class="glyphicon glyphicon-ok"></i> </div>
							</div>
						@else
							<div class="text-center">
								<div class="text-danger btn-xs">
									<i class="glyphicon glyphicon-remove"></i> </div>
							</div>
						@endif
					</td>
					<td>
						@if($trackQuestion->is_customer == 1)
							<div class="text-center">
								<div class="text-success btn-xs">
									<i class="glyphicon glyphicon-ok"></i> </div>
							</div>
						@else
							<div class="text-center">
								<div class="text-danger btn-xs">
									<i class="glyphicon glyphicon-remove"></i> </div>
							</div>
						@endif
					</td>
					@if(AppUtil::isAdmin())
						<td class="text-center">
							<a class="btn btn-danger btn-xs mar-l-20"
									href="{{route('content.tracking.questions.delete',$trackQuestion->id)}}"
									onclick="return confirm('Are you sure?','Yes','No')">
								<i class="glyphicon glyphicon-trash"></i> </a>
						</td>
					@endif
					<?php $slNo++?>
				</tr>
			@endforeach
		</table>
		<div class="text-center">
			{{$trackQuestions->appends(Input::except('Page'))->links()}}
		</div>
	@else
		<div class="alert alert-danger">
			No Tracking Questions added yet. Click <b>Add Question</b> to add one.
		</div>
	@endif
@endsection
@section('modal')
	<div class="modal fade" id="modalTrackQuestions" data-url="" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger hide"></div>
					<form class="form" id="name">
						<div class="form-group">
							<label>Tracking Question</label>
							<input type="text" name="trackQuestion" id="trackQuestion" class="form-control"
									placeholder="Enter the Question">
						</div>
						<div>
							<input type="radio" name="TrackQuestionRadio" id="TrackQuestionRadio" value="1" checked> <b>Is Partner Question</b>
							<input type="radio" name="TrackQuestionRadio" id="TrackQuestionRadio" value="2"> <b>Is Customer Question</b>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary content-track-question-submit" id="">Save</button>
				</div>
			</div>
		</div>
	</div>
@endsection