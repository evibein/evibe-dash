<div id="qbPaymentLinkModal" class="modal " tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Order Process Payment Link</h4>
			</div>
			<div class="modal-body">
				<form class="form form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4">Customer Name:</label>
						<div class="col-sm-8">
							<span id="qbCustomerName"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4">Customer Phone:</label>
						<div class="col-sm-8">
							<span id="qbCustomerPhone"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4">Customer Email:</label>
						<div class="col-sm-8">
							<span id="qbCustomerEmail"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4">Payment Link:</label>
						<div class="col-sm-8">
							<span id="qbPaymentLink" class="in-blk"></span>
							<button id="qbCopyLink" class="in-blk mar-l-5">
								<span class="mdl-tooltip mdl-tooltip--large">Copy to clipboard</span>
							</button>
						</div>
					</div>
					<div class="form-group hide">
						<label class="col-sm-4">Party Date:</label>
						<div class="col-sm-8">
							<span id="qbPartyDate"></span>
						</div>
					</div>
				</form>
				<div class="text-center">
					<div id="quickBookWhatsappPayment" class="btn btn-primary" data-id="{{$ticket->id}}">
						<img src="http://gallery.evibe.in/img/icons/whatsapp-logo.png" alt="whatsapp-icon" class="whatsapp-icon pad-r-5">Send Payment Link via Whatsapp
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="/quick-book" class="btn btn-default">Close</a>
			</div>
		</div>
	</div>
</div>