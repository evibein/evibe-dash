@extends('layout.base')

@section('content')
	<div class="quick-book-wrap">
		<div class="col-sm-10 col-md-10 col-lg-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
			<div class="quick-book-title font-20 text-bold">
				<i class="glyphicon glyphicon-flash"></i> Quickly create a booking and generate payment link
			</div>
			<div class="quick-book-body mar-t-20">
				<form id="quickBookForm">
					<div class="quick-book-ticket">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
									<div class="font-16 text-bold mar-t-5">
										Ticket Details
									</div>
								</div>
								<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
									<select id="ticket" name="ticket" class="form-control">
									</select>
									@if($data['ticket'])
										<ul id="allTicketOptions" class="hide">
											@foreach($data['ticket'] as $ticket)
												<li data-id="{{ $ticket->id }}"
														data-name="{{ $ticket->name }}"
														data-phone="{{ $ticket->phone }}"
														data-alt-phone="{{ $ticket->alt_phone }}"
														data-email="{{ $ticket->email }}"
														data-party-date="{{ date("Y/m/d H:i", $ticket->event_date) }}"
														data-city-id="{{ $ticket->city_id }}"
														data-event-id="{{ $ticket->event_id }}"
														data-area-id="{{ $ticket->area_id }}"
														data-zip-code="{{ $ticket->zip_code }}"
														data-ticket-source-id="{{ $ticket->source_id }}"
														data-source-specific="{{ $ticket->source_specific }}"
														data-venue-landmark="{{ $ticket->venue_landmark }}"
														data-type-venue-id="{{ $ticket->type_venue_id }}"></li>
											@endforeach
										</ul>
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-body">
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="name">Name *</label>
										<input type="text" class="form-control" data-validation="required"
												id="customerName"
												name="customerName"
												data-validation-error-msg="Please enter the customer name"
												placeholder="Enter customer name">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="occasion">Primary Event *</label>
										<select class="form-control" id="occasion" name="occasion">
											<option value="-1">Select an occasion</option>
											@foreach($data['event'] as $occasion)
												<option @if(old('occasion') == $occasion->id) selected
														@elseif(isset($data["parentEventId"]) && $data["parentEventId"] == $occasion->id) selected
														@endif value="{{ $occasion->id }}">{{ $occasion->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="eventDate">Party Date & Time *</label>
										<input type="text" class="form-control" id="eventDate" name="eventDate"
												value="@if(old('eventDate')){{ old('eventDate') }}@elseif(isset($data["response"]["eventDate"])){{ date('Y/m/d H:i',$data["response"]["eventDate"]) }}@endif"
												data-validation="required"
												data-validation-error-msg="Please enter party date"
												placeholder="Select party date"
												autocomplete="off">
									</div>
								</div>
								<div class="clearfix"></div>

								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="name">Phone number *</label>
										<input type="text" class="form-control" data-validation="required" id="phone"
												name="phone"
												data-validation-error-msg="Please enter the phone number"
												value="@if(old('phone')){{ old('phone') }}@elseif(isset($data["phone"])){{ $data["phone"] }}@endif"
												placeholder="Enter customer phone number">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="email">Email *</label>
										<input type="text" class="form-control" id="email" name="email"
												data-validation="required"
												data-validation-error-msg="Please enter the email"
												value="@if(old('email')){{ old('email') }}@elseif(isset($data["email"])){{ $data["email"] }}@endif"
												placeholder="Enter email of the customer">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="altPhone">Alt Phone number</label>
										<input type="text" class="form-control" id="altPhone" name="altPhone"
												value="@if(old('altPhone')){{ old('altPhone') }}@elseif(isset($data["altPhone"])){{ $data["altPhone"] }}@endif"
												placeholder="Enter alternate phone number"
												autocomplete="off">
									</div>
								</div>
								<div class="clearfix"></div>

								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="city">City *</label>
										<select name="city" id="city" class="form-control">
											<option value="-1" selected>-- select city --</option>
											@foreach($data['city'] as $city)
												<option value="{{ $city->id }}"
														@if(((isset($data['response']['cityId'])) && (($data['response']['cityId']) == $city->id))
														|| ((old('city') == $city->id))) selected
														@endif>{{ $city->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="area">
											<span class="mar-r-10">Location *</span>
											<a href="#" class="btn-add-loc text-normal"
													data-redirectto="{{ route('ticket.new') }}">Add New Location</a>
										</label>
										<select name="area" id="area" class="form-control" pincode=""
												data-default="@if(isset($data['response']['area_id'])){{$data['response']['area_id']}}@endif">
										</select>
										@if($data['area'])
											<ul id="allAreaOptions" class="hide">
												@foreach ($data['area'] as $area)
													<li data-id="{{ $area->id }}"
															data-city_id="{{ $area->city_id }}"
															data-name="{{ $area->name }}"
															data-pin="{{ $area->zip_code }}"></li>
												@endforeach
											</ul>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="zip">Zip Code *</label>
										<input type="text" class="form-control" id="zip" name="zip"
												placeholder="enter zip code"
												data-validation="number" data-validation-allowing="range[100000;999999]"
												value="@if(old('zip')){{ old('zip') }}@elseif(isset($data['response']['zip_code'])){{$data['response']['zip_code']}}@endif">
									</div>
								</div>
								<div class="clearfix"></div>

								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="landmark">Landmark *</label>
										<input type="text" class="form-control" name="landmark" id="landmark"
												placeholder="enter landmark"
												value="@if(old('landmark')){{ old('landmark') }}@elseif(isset($data["venueLandmark"])){{ $data["venueLandmark"] }}@endif">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="ticketSource">Ticket Source</label>
										<select name="ticketSource" id="ticketSource" class="form-control">
											<option value="-1">-- Please Select Ticket Source --</option>
											@foreach($data['ticketSource'] as $key=>$value)
												<option
														@if((old('ticketSource') == $value['id']) ||
														((isset($data['thisTicketSourceId']) && $data['thisTicketSourceId'] == $value['id']))) selected
														@endif
														value="{{$value['id']}}">{{$value['name']}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="sourceSpecific">Source Specific</label>
										<input type="text" class="form-control" id="sourceSpecific"
												name="sourceSpecific"
												value="@if(old('sourceSpecific')){{ old('sourceSpecific') }}@elseif(isset($data["sourceSpecific"])){{ $data["sourceSpecific"] }}@endif"
												placeholder="Enter Source specific details">
									</div>
								</div>
								<div class="clearfix"></div>

								<div class="col-xs-12 col-sm-4">
									<div class="form-group create-ticket-venue-type hide">
										<label for="typeVenue">Where*</label>
										<select class="form-control" id="typeVenue" name="typeVenue">
											@foreach($data['typeVenue'] as $typeVenue)
												<option @if(old('typeVenue') == $typeVenue->id) selected
														@elseif(isset($data["response"]["typeVenueId"]) && $data["response"]["typeVenueId"] == $typeVenue->id) selected
														@endif value="{{ $typeVenue->id }}">{{ $typeVenue->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="quick-book-product">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
									<div class="font-16 text-bold mar-t-5">
										Product & Partner Selection
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-body">
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<label for="productType">Product Type *</label>
									<select id="productType" name="productType" class="form-control">
									</select>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 no-pad">
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
										<label for="product">Product *</label>
										<select id="product" name="product" class="form-control"></select>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
										<div class="planner-partner">
											<label for="planner">Provider *</label>
											<select id="planner" name="planner" class="form-control">
												<option value="-1"> -- Select a provider --</option>
												@if($data['planner'])
													@foreach($data['planner'] as $planner)
														<option value="{{ $planner->id }}">{{ $planner->name }}</option>
													@endforeach
												@endif
											</select>
										</div>
										<div class="venue-partner pad-t-5 hide">
											<label>Venue *</label>
											<div class="venue-partner-name"></div>
										</div>
										@if($data['venue'])
											<ul id="allVenueDetails" class="hide">
												@foreach($data['venue'] as $venue)
													<li data-id="{{ $venue->id }}"
															data-name="{{ $venue->name }}"></li>
												@endforeach
											</ul>
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>

								<hr>

								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="productPrice">Total Amount *</label>
										<input type="text" class="form-control" id="productPrice"
												name="productPrice"
												value=""
												data-validation="number" data-validation-allowing="min1"
												placeholder="Enter total product price">
									</div>
								</div>
								<div class="col-xs-12 col-sm-8">
									<div class="text-bold">Discount</div>
									<div class="mar-t-5">
										<div class="in-blk mar-r-10">
											<div class="in-blk">
												<label>
													<input type="radio" value="-1" name="discountPercentage"
															checked> Custom:
												</label>
											</div>
											<div class="in-blk">
												<input type="text" class="form-control" id="customDiscount"
														name="productPrice"
														value="0"
														placeholder="Enter total product price">
											</div>
										</div>
										<div class="in-blk mar-r-10">
											<label>
												<input type="radio" value="2" name="discountPercentage"> 2%
											</label>
										</div>
										<div class="in-blk mar-r-10">
											<label>
												<input type="radio" value="3" name="discountPercentage"> 3%
											</label>
										</div>
										<div class="in-blk mar-r-10">
											<label>
												<input type="radio" value="5" name="discountPercentage"> 5%
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="clearfix"></div>

								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="discountAmount">Discount Amount *</label>
										<input type="text" class="form-control" id="discountAmount"
												name="discountAmount"
												value=""
												data-validation="number" data-validation-allowing="min1" disabled
												placeholder="Enter discount amount">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="bookingAmount">Booking Amount *</label>
										<input type="text" class="form-control" id="bookingAmount"
												name="bookingAmount"
												value=""
												data-validation="number" data-validation-allowing="min1"
												placeholder="Enter booking amount">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<div>
											<div class="in-blk">
												<label for="advanceAmount">Advance Amount *</label>
											</div>
											<div class="in-blk">
												<label>
													<input type="radio" value="30" name="advancePercentage" checked> 30%
												</label>
											</div>
											<div class="in-blk">
												<label>
													<input type="radio" value="50" name="advancePercentage"> 50%
												</label>
											</div>
											<div class="in-blk">
												<label>
													<input type="radio" value="100" name="advancePercentage"> 100%
												</label>
											</div>
										</div>
										<input type="text" class="form-control" id="advanceAmount"
												name="advanceAmount"
												value=""
												data-validation="number" data-validation-allowing="min1"
												placeholder="Enter advance amount">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="quick-book-booking">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
									<div class="font-16 text-bold mar-t-5">
										Booking Details
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-body">
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="typeTicketBooking">Ticket Booking Type *</label>
										<select class="form-control" id="typeTicketBooking" name="typeTicketBooking">
											<option value="-1"> -- Select ticket booking type --</option>
											@foreach($data['typeTicketBooking'] as $typeTicketBooking)
												<option value="{{ $typeTicketBooking->id }}">{{ $typeTicketBooking->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="bookingType">Booking Type Details *</label>
										<input type="text" id="bookingType" class="form-control"
												placeholder="Enter booking type details">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="typeBookingConcept">Ticket Concept Type *</label>
										<select class="form-control" id="typeBookingConcept" name="typeBookingConcept">
											<option value="-1"> -- Select concept type --</option>
											@foreach($data['typeBookingConcept'] as $typeBookingConcept)
												<option value="{{ $typeBookingConcept->id }}">{{ $typeBookingConcept->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="clearfix"></div>

								<div class="checkout-fields-wrap"></div>

								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="orderDetails">Order Details *</label>
										<textarea id="orderDetails" name="orderDetails"
												class="form-control vertical-resize editor-box editor-html"
												placeholder="Enter order details"></textarea>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="prerequisites">Prerequisites (optional)</label>
										<textarea id="prerequisites" name="prerequisites"
												class="form-control vertical-resize editor-box editor-html"
												placeholder="Enter prerequisites"></textarea>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="form-group">
										<label for="facts">Facts (optional)</label>
										<textarea id="facts" name="facts"
												class="form-control vertical-resize editor-box editor-html"
												placeholder="Enter facts"></textarea>
									</div>
								</div>
								<div class="clearfix"></div>

								<div class="product-images-wrap mar-t-5"></div>
							</div>
						</div>
					</div>
					<div class="quick-book-cta text-center mar-b-30">
						<button id="quickBookSubmit" class="btn btn-primary">Generate Payment Link</button>
					</div>

					<!-- hidden data -->
					<input type="hidden" id="hidQuickBookProductsUrl" value="{{ route('quick-book.products') }}">
					<input type="hidden" id="hidQuickBookCheckoutFieldsUrl" value="{{ route('quick-book.checkout-fields') }}">
					<input type="hidden" id="hidQuickBookProductImagesUrl" value="{{ route('quick-book.product-images') }}">
					<input type="hidden" id="hidPlannerTypeId" value="{{ config('evibe.ticket_type.planners') }}">
					<input type="hidden" id="hidVenueTypeId" value="{{ config('evibe.ticket_type.venues') }}">
					<input type="hidden" id="hidTypeFieldText" value="{{ config('evibe.type_field.text') }}">
					<input type="hidden" id="hidTypeFieldTextarea" value="{{ config('evibe.type_field.textarea') }}">
				</form>

			</div>

		</div>
		<div class="clearfix"></div>
	</div>

	@include("quick-book.payment-link-modal")
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			var ticketValue,
				eventValue,
				cityValue,
				areaValue,
				productTypeValue,
				productValue,
				plannerValue,
				typeTicketBookingValue,

				$selectTicket,
				$selectEvent,
				$selectCity,
				$selectArea,
				$selectProductType,
				$selectProduct,
				$selectPlanner,
				$selectTypeTicketBooking,

				$ticket = $('#ticket'),
				$event = $('#occasion'),
				$city = $('#city'),
				$area = $('#area'),
				$productType = $('#productType'),
				$product = $('#product'),
				$planner = $('#planner'),
				$typeTicketBooking = $('#typeTicketBooking'),

				ticketResults = [],
				areaResults = [],
				productTypeResults = [],
				productResults = [],
				plannerResults = [],
				venueResults = [],
				typeTicketBookingResults = [],

				$partnerId = 0,
				$partnerTypeId = 0,
				disAmount = 0,
				bookAmount = 0,
				productPrice = $('#productPrice').val();

			$('#qbPaymentLinkModal').modal({
				show: false,
				keyboard: false,
				backdrop: 'static'
			});

			$('.editor-html').wysihtml5({
				toolbar: {
					"image": false,
					'html': false,
					'blockquote': false,
					'size': 'xs'
				}
			});

			var today = new Date();
			var oneDay = new Date(today.getTime() + 24 * 60 * 60 * 1000);
			$('#eventDate').datetimepicker({
				minDate: 0,
				format: 'Y/m/d H:i',
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});

			//			$.validate({
			//				lang: 'es',
			//				modules: 'toggleDisabled',
			//				disabledFormFilter: 'form.new-ticket-create-form',
			//				showErrorDialogs: false
			//			});

			$selectTicket = $ticket.selectize({
				valueField: 'id',
				labelField: 'label',
				searchField: ['label', 'name', 'phone', 'email', 'id'],
				placeholder: "Select a ticket to use its details",
				render: {
					option: function (data, escape) {
						return "<div>" +
							"<div><b>" + escape(data.name) + "</b></div>" +
							"<div>" + escape(data.phone) + "</div>" +
							"<div>" + escape(data.email) + "</div>" +
							"</div>";
					}
				},
				onChange: function (val) {
					if (!val.length || val == -1) {
						return false;
					}

					var data = this.options[val];
					if ((typeof(data) !== "undefined")) {

						// update required fields
						$('#customerName').val(data.name);
						//						$('#occasion').val(data.eventId);
						$('#eventDate').val(data.partyDate);
						$('#phone').val(data.phone);
						$('#email').val(data.email);
						$('#altPhone').val(data.altPhone);
						eventValue.setValue(data.eventId);
						cityValue.setValue(data.cityId);
						areaValue.setValue(data.areaId);
						areaValue.blur();
						$('#landmark').val(data.landmark);
						$('#ticketSource').val(data.ticketSource);
						$('#sourceSpecific').val(data.sourceSpecific);

						var pinCode;
						var areaPin = findObjectByKey(areaResults, 'id', data.areaId);
						if (data.zipCode) {
							pinCode = data.zipCode;
						}
						else if (areaPin && areaPin['pin']) {
							pinCode = areaPin['pin'];
						}
						$('#zip').val(pinCode);
					}
				}
			});

			$selectEvent = $event.selectize({
				onChange: function (val) {
					productTypeValue.disable();
					productTypeValue.clearOptions();
					productTypeResults = [];

					if (!val.length || val == -1) {
						return false;
					}

					// ajax request to fetch applicable product types
					$.ajax({
						url: '/quick-book/product-types',
						type: 'GET',
						dataType: 'json',
						data: {
							eventId: val
						},
						success: function (data) {
							if (data.success) {
								if (data.productTypes) {
									$.each(data.productTypes, function (index, value) {
										productTypeResults.push({
											id: value.id,
											name: value.name
										});
									});

									productTypeValue.enable();
									productTypeValue.addOption(productTypeResults);
									productTypeValue.refreshOptions();
								}

								if (data.productTypes.length === 0) {
									productTypeValue.disable();
									productTypeValue.clearOptions();
								}
							}
							else {
								var error = 'Some error occurred while fetching occasion specific product types';
								if (data.error) {
									error = data.error;
								}
								window.showNotyError(error);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							showNotyError('Error occurred while fetching product types, Please reload the page and try again.')
						}
					});
				}
			});

			// @see: do not select any thing by default
			$selectCity = $city.selectize({
				onChange: function (val) {
					areaValue.disable();
					areaValue.clearOptions();
					if (!val.length || val == -1) {
						return false;
					}

					$('#allAreaOptions').find('li').each(function (count) {
						if ($(this).data('city_id') && $(this).data('city_id') == val) {
							areaResults.push({
								id: $(this).data('id'),
								name: $(this).data('name'),
								pin: $(this).data('pin')
							});
						}
					});

					areaValue.enable();
					areaValue.addOption(areaResults);
					areaValue.refreshOptions();
				}
			});

			$selectArea = $area.selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: ['id', 'name'],
				render: {
					option: function (data, escape) {
						return "<div data-pin-code='" + data.pin + "'>" + data.name + "</div>"
					}
				},
				onChange: function (val) {
					var data = this.options[val];
					if ((typeof(data) !== "undefined")) {
						if ((typeof(data.pin) !== "undefined") && data.pin) {
							$('#zip').val(data.pin);
						}
					}
				}
			});

			$selectProductType = $productType.selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: ['id', 'name'],
				placeholder: 'Select product type',
				render: {
					option: function (data, escape) {
						return '<div data-id="' + data.id + '" data-name="' + data.name + '">' + data.name + '</div>'
					}
				},
				onChange: function (val) {
					productValue.disable();
					productValue.clearOptions();
					productResults = [];
					if (!val.length || val == -1) {
						return false;
					}

					if (($('#city').val()) && ($('#occasion').val()) && ($('#city').val() > 0) && ($('#occasion').val() > 0)) {
						$.ajax({
							url: $('#hidQuickBookProductsUrl').val(),
							type: 'GET',
							dataType: 'JSON',
							data: {
								productTypeId: $('#productType').val(),
								cityId: $('#city').val(),
								eventId: $('#occasion').val()
							},
							success: function (data) {
								if (data.success) {
									if (data.products) {
										$.each(data.products, function (index, value) {
											productResults.push({
												id: index,
												name: value.name,
												code: value.code,
												price: value.price,
												partnerId: value.partnerId,
												partnerTypeId: value.partnerTypeId,
												info: value.info,
												label: value.code + ' - ' + value.name + ' - ' + value.price
											});
										});

										productValue.enable();
										productValue.addOption(productResults);
										productValue.refreshOptions();
									}

									if (data.products.length == 0) {
										productValue.disable();
										productValue.clearOptions();
									}
								}
								else {
									var error = 'Error occurred while fetching products data, Please reload the page and try again.';
									if (data.error) {
										error = data.error;
									}
									showNotyError(error);
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								showNotyError('Error occurred while fetching products data, Please reload the page and try again.')
							}
						});
					}
					else {
						window.showNotyError("Kindly select city and occasion to select a product type");
						return false;
					}
				}
			});

			$selectProduct = $product.selectize({
				valueField: 'id',
				labelField: 'label',
				searchField: ['label', 'name', 'code'],
				placeholder: "Select a product",
				render: {
					option: function (data, escape) {
						return "<div>" +
							"<div><b>" + escape(data.name) + "</b></div>" +
							"<div>[" + escape(data.code) + "]</div>" +
							"<div>" + escape(data.price) + "</div>" +
							"</div>";
					}
				},
				onChange: function (val) {
					var data = this.options[val];
					$('.product-images-wrap').empty();
					if ((typeof(data) !== "undefined")) {
						if ((typeof(data.partnerTypeId) !== "undefined") && data.partnerTypeId) {
							if (data.partnerTypeId == $('#hidPlannerTypeId').val()) {

								plannerValue.enable();
								plannerValue.setValue(data.partnerId);

								$('.planner-partner').removeClass('hide');
								$('.venue-partner').addClass('hide');
							}
							else if (data.partnerTypeId == $('#hidVenueTypeId').val()) {
								$('.planner-partner').addClass('hide');
								$('.venue-partner').removeClass('hide');

								var vName = findObjectByKey(venueResults, 'id', data.partnerId);
								if (vName && vName['name']) {
									$('.venue-partner-name').text(vName['name']);
								}
								else {
									$('.venue-partner-name').text('');
								}
							}

							$partnerId = data.partnerId;
							$partnerTypeId = data.partnerTypeId;
						}
						else {
							$partnerId = 0;
							$partnerTypeId = 0;

							plannerValue.setValue(0);
							$('.venue-partner-name').text('');
						}

						// todo: booking details
						// todo: calculator function
						// todo: info, terms, facts, etc,..

						if ((typeof(data.price) !== "undefined") && data.price) {
							$('#productPrice').val(data.price);
							calcAndUpdateAmounts();
						}

						if ((typeof(data.info) !== "undefined") && data.info) {
							$('#orderDetails').data('wysihtml5').editor ? $('#orderDetails').data('wysihtml5').editor.setValue(data.info) : '';
						}

						if ((typeof(data.id) !== "undefined") && data.id) {

							$.ajax({
								url: $('#hidQuickBookProductImagesUrl').val(),
								type: 'GET',
								dataType: 'JSON',
								data: {
									productId: data.id,
									productTypeId: $('#productType').val()
								},
								success: function (data) {
									if (data.success) {
										if (data.productImages) {
											var $element = '';
											var count = 0;

											$element += '<div class="col-xs-12 col-sm-12">';
											$element += '<div class="form-group">';
											$element += '<label>Product Images</label>';
											$element += '<div class="booking-details">';

											$.each(data.productImages, function (index, value) {
												count++;

												$element += '<div class="img-wrap qb-product-image in-blk" data-link="' + value + '">';
												$element += '<a href="' + value + '" target="_blank" class="booking-gallery-link">';
												$element += '<img src="' + value + '" class="ticket-booking-image">';
												$element += '</a>';
												$element += '<a href="" class="remove-booking-image">';
												$element += '<div class="delete-image in-blk">';
												$element += '<i class="glyphicon glyphicon-remove-circle">';
												$element += '</i>';
												$element += '</div>';
												$element += '</a>';
												$element += '</div>';

											});

											$element += '<div class="clearfix"></div>';
											$element += '</div>';
											$element += '</div>';
											$element += '</div>';
											$element += '<div class="clearfix"></div>';

											$('.product-images-wrap').append($element);
										}
									}
								},
								error: function (jqXHR, textStatus, errorThrown) {
									showNotyError('Error occurred while fetching products images, Please reload the page and try again.')
								}
							});
						}
					}
				}
			});

			$selectPlanner = $planner.selectize({});

			$selectTypeTicketBooking = $typeTicketBooking.selectize({
				onChange: function (val) {
					var data = this.options[val];
					$('.checkout-fields-wrap').empty();
					// using val, make ajax request;

					if ($('#occasion').val() && $('#occasion').val() > 0) {
						$.ajax({
							url: $('#hidQuickBookCheckoutFieldsUrl').val(),
							type: 'GET',
							dataType: 'JSON',
							data: {
								typeTicketBookingId: $typeTicketBooking.val(),
								eventId: $('#occasion').val()
							},
							success: function (data) {
								if (data.success) {
									if (data.checkoutFields) {
										var $element = '';
										var count = 0;
										$.each(data.checkoutFields, function (index, value) {
											count++;
											if (value.typeFieldId == $('#hidTypeFieldText').val()) {
												$element += '<div class="col-xs-12 col-sm-4">';
												$element += '<div class="form-group">';
												$element += '<label for="' + value.name + '">' + value.identifier + '</label>';
												$element += '<input type="text" id="' + value.name + '" name="' + value.name + '" data-id="' + value.id + '" data-name="' + value.name + '" class="form-control qb-checkout-field ' + value.name + '">';
												$element += '</div>';
												$element += '</div>';
											}
											else if (value.typeFieldId == $('#hidTypeFieldTextarea').val()) {
												$element += '<div class="col-xs-12 col-sm-4">';
												$element += '<div class="form-group">';
												$element += '<label for="' + value.name + '">' + value.identifier + '</label>';
												$element += '<textarea id="' + value.name + '" name="' + value.name + '" data-id="' + value.id + '" data-name="' + value.name + '" class="form-control qb-checkout-field ' + value.name + '"></textarea>';
												$element += '</div>';
												$element += '</div>';
											}

											if (count % 3 == 0) {
												$element += '<div class="clearfix"></div>';
											}
										});

										if (count % 3 !== 0) {
											$element += '<div class="clearfix"></div>';
										}

										$('.checkout-fields-wrap').append($element);
									}
								}
								else {
									var error = 'Error occurred while fetching checkout fields, Please reload the page and try again.';
									if (data.error) {
										error = data.error;
									}
									showNotyError(error);
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								showNotyError('Error occurred while fetching products data, Please reload the page and try again.')
							}
						});
					}
					else {
						window.showNotyError("Kindly select occasion to select booking type");
						return false;
					}
				}
			});

			ticketValue = $selectTicket[0].selectize;
			eventValue = $selectEvent[0].selectize;
			cityValue = $selectCity[0].selectize;
			areaValue = $selectArea[0].selectize;
			productTypeValue = $selectProductType[0].selectize;
			productValue = $selectProduct[0].selectize;
			plannerValue = $selectPlanner[0].selectize;
			typeTicketBookingValue = $selectTypeTicketBooking[0].selectize;

			if (!$area.val()) {
				areaValue.disable();
			}

			// form ticket data
			(function formTicketOptions() {
				$('#allTicketOptions').find('li').each(function (count) {

					ticketResults.push({
						id: $(this).data('id'),
						name: $(this).data('name'),
						phone: $(this).data('phone'),
						email: $(this).data('email'),
						label: $(this).data('name') + ' - ' + $(this).data('phone') + ' - ' + $(this).data('email'),
						altPhone: $(this).data('alt-phone'),
						partyDate: $(this).data('party-date'),
						cityId: $(this).data('city-id'),
						eventId: $(this).data('event-id'),
						areaId: $(this).data('area-id'),
						zipCode: $(this).data('zip-code'),
						landmark: $(this).data('venue-landmark'),
						ticketSource: $(this).data('ticket-source-id'),
						sourceSpecific: $(this).data('source-specific'),
						typeVenueId: $(this).data('type-venue-id')
					});

				});

				ticketValue.enable();
				ticketValue.addOption(ticketResults);
				ticketValue.refreshOptions();
				ticketValue.blur();
			})();

			$('#occasion').on('change', function () {
				productTypeValue.setValue(-1);
			});

			$city.on('change', function () {
				productTypeValue.setValue(-1);
			});

			$('#productPrice').on('change', function () {
				calcAndUpdateAmounts();
			});

			$('#allVenueDetails').find('li').each(function (count) {
				venueResults.push({
					id: $(this).data('id'),
					name: $(this).data('name')
				});
			});

			$('input[type=radio][name=discountPercentage]').on('change', function () {
				if ($('input[type=radio][name=discountPercentage]:checked').val() == -1) {
					$("#customDiscount").attr('disabled', false);
				}
				else {
					$("#customDiscount").attr('disabled', true);
					calcAndUpdateAmounts();
				}
			});

			$('#customDiscount').on('change', function () {
				calcAndUpdateAmounts();
			});

			$('input[type=radio][name=advancePercentage]').on('change', function () {
				calcAndUpdateAmounts();
			});

			$('#qbCopyLink').on('click', function (event) {
				event.preventDefault();

				copyToClipboard($('#qbPaymentLink').text());
				window.showNotySuccess("Payment link copied to clipboard");
			});

			$('#quickBookSubmit').on('click', function (event) {
				event.preventDefault();
				window.showLoading();
				var quickBookForm = $('#quickBookForm');
				var productImages = [];

				if ($partnerTypeId && $partnerTypeId == $('#hidPlannerTypeId').val()) {
					$partnerId = $('#planner').val();
				}

				var inputData = {
					ticketId: $('#ticket').val(),
					name: $('#customerName').val(),
					phone: $('#phone').val(),
					email: $('#email').val(),
					altPhone: $('#altPhone').val(),
					eventId: $('#occasion').val(),
					cityId: $('#city').val(),
					partyDateTime: $('#eventDate').val(),
					areaId: $('#area').val(),
					zipCode: $('#zip').val(),
					venueLandmark: $('#landmark').val(),
					ticketSource: $('#ticketSource').val(),
					sourceSpecific: $('#sourceSpecific').val(),
					productId: $('#product').val(),
					productTypeId: $('#productType').val(),
					partnerId: $partnerId,
					partnerTypeId: $partnerTypeId,
					productPrice: $('#productPrice').val(),
					discountAmount: $('#discountAmount').val(),
					bookingAmount: $('#bookingAmount').val(),
					advanceAmount: $('#advanceAmount').val(),
					typeTicketBooking: $('#typeTicketBooking').val(),
					bookingTypeDetails: $('#bookingType').val(),
					typeBookingConcept: $('#typeBookingConcept').val(),
					orderDetails: $('#orderDetails').val(),
					prerequisites: $('#prerequisites').val(),
					facts: $('#facts').val()
				};

				// adding value dynamically for checkout field
				$('.qb-checkout-field').each(function (key, value) {
					var fieldValue = '', fieldName = '';
					fieldName = $(this).data('name');
					fieldValue = $(this).val();
					inputData[fieldName] = fieldValue;
				});

				// adding value dynamically for product images
				var count = 0;
				$('.qb-product-image').each(function (key, value) {
					productImages[count] = $(this).data('link');
					count++;
				});

				inputData['productImages'] = productImages;

				$.ajax({
					url: '/quick-book',
					type: 'POST',
					dataType: 'json',
					data: inputData,
					success: function (data) {
						if (data.success) {
							$('#qbPaymentLinkModal #qbCustomerName').text(data.customerName);
							$('#qbPaymentLinkModal #qbCustomerPhone').text(data.customerPhone);
							$('#qbPaymentLinkModal #qbCustomerEmail').text(data.customerEmail);
							$('#qbPaymentLinkModal #qbPaymentLink').text(data.paymentLink);
							window.hideLoading();
							$('#qbPaymentLinkModal').modal('show');
						}
						else {
							var error = 'Some error occurred while creating quick booking. Please try again.';
							if (data.error) {
								error = data.error;
							}

							window.hideLoading();
							window.showNotyError(error);
						}
					},
					fail: function () {
						window.hideLoading();
						showNotyError('Error occurred while fetching products data, Please reload the page and try again.')
					}
				});

			});
			$('#quickBookWhatsappPayment').on('click', function () {
				var id = $(this).data('id');
				var data = {id: id};
				var url = "/ticket/whatsapp/order-process";
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data: data,
					success: function (data) {
						if (data.success) {
							window.open(data.text, '_blank');
						}
						else {
							showNotyError("Please Reload The Page");
						}
					},
					error: function () {
						showNotyError("Please Refresh The Page")
					}
				});
			});
			$('.product-images-wrap').on('click', '.remove-booking-image', function (event) {
				event.preventDefault();

				if (!confirm("Are you sure to deleted this image?")) {
					return false;
				}
				$(this).parent().remove();
			});

			function copyToClipboard(text) {
				var textarea = document.createElement('textarea');
				textarea.textContent = text;
				document.body.appendChild(textarea);

				var selection = document.getSelection();
				var range = document.createRange();

				range.selectNode(textarea);
				selection.removeAllRanges();
				selection.addRange(range);

				selection.removeAllRanges();

				document.body.removeChild(textarea);
			}

			function calcAndUpdateAmounts() {
				var disPercent = $('input[type=radio][name=discountPercentage]:checked').val();
				productPrice = $('#productPrice').val();
				if (disPercent == -1) {
					disAmount = $('#customDiscount').val();
				}
				else {
					disAmount = parseInt(productPrice * disPercent / 100, 10);
				}

				bookAmount = parseInt((productPrice - disAmount), 10);

				if (bookAmount < 0) {
					alert('Discount Amount cannot be greater that Total Amount');
					disAmount = 0;
					$('#customDiscount').val(disAmount);
					calcAndUpdateAmounts();
				}
				else {
					$('#discountAmount').val(disAmount);
					$('#bookingAmount').val(bookAmount);
					$('#advanceAmount').val(calculateRoundedAdvance(bookAmount));
				}
			}

			function calculateRoundedAdvance($bookingAmount) {
				$advancePercentage = $('input[type=radio][name=advancePercentage]:checked').val();
				var $advanceAmount = $bookingAmount * $advancePercentage / 100;
				var $balAmount = $bookingAmount - $advanceAmount;
				if (($balAmount % 100) != 0 && $balAmount > 100) {
					var $round = parseInt($balAmount % 100);
					var $roundedAdvanceAmount = parseInt($bookingAmount) - parseInt($balAmount) + $round;
					return $roundedAdvanceAmount;
				}

				return parseInt($advanceAmount);
			}

			function findObjectByKey(array, key, value) {
				for (var i = 0; i < array.length; i++) {
					if (array[i][key] === value) {
						return array[i];
					}
				}
				return null;
			}

			// select default area
			if ($area.data('default')) {
				areaValue.setValue($area.data('default'));
			}

			productTypeValue.disable();
			productValue.disable();
			plannerValue.disable();

		});
	</script>
@endsection