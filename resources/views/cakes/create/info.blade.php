@extends('cakes.base.create')

@section('heading')
	Add New Cake
	@endsection

			<!-- new cake form begin -->
@section('body')
	@if ($errors->count() > 0)
		<div class="mar-t-20 ">
			<div class="col-sm-12">
				<div class="alert alert-danger pad-10">
					<ul class="erros-list ls-none">
						<li>{{ $errors->first() }}</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	@endif
	<form action="{{ route('cakes.new.create') }}" method="POST" role="form" class="mar-t-20">
		<div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="form-group">
					<label for="title">Title <span class="text-mandatory">*</span></label>
					<input type="text" class="form-control" id="title" name="title"
							value="{{ old('title') }}" placeholder="Enter cake title">
				</div>
			</div>
			<div class="col-sm-2">
				<label>Min. Order (In KG) <span class="text-mandatory">*</span></label>
				<div class="form-group">
					<input type="text" class="form-control" id="min-weight" name="min-weight"
							value="{{ old('min-weight') }}" placeholder="Enter min weight">
				</div>
			</div>
			<div class="col-sm-2 ">
				<label>Price <span class="text-mandatory">*</span></label>
				<div class="form-group">
					<input type="text" class="form-control" id="price" name="price"
							value="{{ old('price') }}" placeholder="Enter Price">
				</div>
			</div>
			<div class="col-sm-2 ">
				<label>Price Worth</label>
				<div class="form-group">
					<input type="text" class="form-control" id="price-worth" name="price-worth"
							value="{{ old('price-worth') }}" placeholder="Enter price worth">
				</div>
			</div>
			<div class="col-sm-2 ">
				<label>Price for 1 KG <span class="text-mandatory">*</span></label>
				<div class="form-group">
					<input type="text" class="form-control" id="price-per-kg" name="price-per-kg"
							value="{{ old('price-per-kg') }}" placeholder="Enter price / kg">
				</div>
			</div>

			<div class="clearfix"></div>
		</div>
		<div>
			<div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
				<div class="form-group">
					<label for="info">Description <span class="text-mandatory">*</span></label>
					<textarea name="info" id="info" class="form-control"
							placeholder="Enter cake description">{{ old('info')}}</textarea>
				</div>
			</div>
			<div class="col-sm-3 col-md-3 col-lg-3">
				<div class="form-group">
					<label for="provider">Provider <span class="text-mandatory">*</span></label>
					<select name="provider" id="provider" class="form-control">
						@foreach ($data['providers'] as $provider)
							<option value="{{ $provider->id }}">{{ $provider->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-sm-3 col-md-3 col-lg-3">
				<div class="form-group">
					<label for="flavour">Recommended Flavours</label>
					<select name="flavour" id="flavour" class="form-control">
						<option value="-1">None</option>
						@foreach ($data['flavours'] as $flavour)
							<option value="{{ $flavour->id }}">{{ $flavour->identifier }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="pad-t-10">
			<div class="col-sm-12 col-md-12 col-lg-12 text-center">
				<div class="form-group">
					<a href="{{ route('cakes.list') }}" class="btn btn-default mar-r-10"><i
								class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
					<button type="submit" class="btn btn-danger"><i
								class="glyphicon glyphicon-plus-sign"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</form>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			// selectize provider
			$('#provider').selectize();
		});
	</script>
@stop