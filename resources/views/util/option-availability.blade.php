@extends('layout.base')

@section('content')
	<div class="">
		<div>
			<div class="col-sm-12">
				<div class="font-28 mar-t-20 mar-b-10 text-center">Option Availability - VDay Campaign - 14 Feb 2020</div>
				<hr>
				<div class="text-center">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="in-blk">Select City</div>
						<div class="in-blk mar-l-10">
							<select id="oaCity" name="oaCity" class="form-control">
								<option value="1" @if(isset($data['cityId']) && ($data['cityId'] == 1)) selected @endif>Bangalore</option>
								<option value="2" @if(isset($data['cityId']) && ($data['cityId'] == 2)) selected @endif>Hyderabad</option>
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="mar-t-20 pad-b-30">
			<div class="col-sm-12">
				@if(isset($data['packagesData']) && count($data['packagesData']))
					<table class="table table-bordered">
						<tr>
							<th rowspan="2" class="text-center">Option</th>
							<th rowspan="2" class="text-center">Partner</th>
							<th colspan="5" class="text-center">Slots</th>
						</tr>
						<tr>
							<th class="text-center">Start Time</th>
							<th class="text-center">End Time</th>
							<th class="text-center">Total</th>
							<th class="text-center">Booked</th>
							<th class="text-center">Available</th>
						</tr>
						@foreach($data['packagesData'] as $packageData)
							<tr>
								<td @if(isset($packageData['slots']) && count($packageData['slots'])) rowspan="{{ count($packageData['slots']) }}" @else rowspan="1" @endif>
									@if(isset($packageData['link']) && $packageData['link'])
										<div>
											<a href="{{ $packageData['link'] }}" target="_blank">{{ $packageData['name'] }}</a>
										</div>
									@else
										{{ $packageData['name'] }}
									@endif
									<div>[{{ $packageData['code'] }}] @if(isset($packageData['area']) && $packageData['area']) - {{ $packageData['area'] }} @endif</div>
								</td>
								<td @if(isset($packageData['slots']) && count($packageData['slots'])) rowspan="{{ count($packageData['slots']) }}" @else rowspan="1" @endif>
									@if(isset($packageData['partnerLink']) && $packageData['partnerLink'])
										<a href="{{ $packageData['partnerLink'] }}" target="_blank">{{ $packageData['partnerName'] }}</a>
									@else
										{{ $packageData['partnerName'] }}
									@endif
								</td>
							@if(isset($packageData['slots']) && count($packageData['slots']))
								@foreach($packageData['slots'] as $key => $slot)
									@if($key > 0)
										<tr> @endif
											<td>{{ $slot['slotStartTime'] }}</td>
											<td>{{ $slot['slotEndTime'] }}</td>
											<td>{{ $slot['total'] }}</td>
											<td>{{ $slot['booked'] }}</td>
											<td class="@if($slot['available'] < 1) text-red @else text-green @endif">{{ $slot['available'] }}</td>
											@if($key > 0) </tr> @endif
								@endforeach
							@else
								<td colspan="5" class="text-center text-danger">Slots are not provided</td>
								@endif
								</tr>
								@endforeach
					</table>
				@else
					<div class="text-center text-danger font-18">There are no option slots provided!</div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {

			$('#oaCity').on('change', function (event) {
				event.preventDefault();

				window.location.href = '/option-availability?city=' + $(this).val();
			});

		});
	</script>
@endsection