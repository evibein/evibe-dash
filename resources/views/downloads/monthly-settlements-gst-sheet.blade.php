<html>
<body>
<table>
	<tr>
		<th>Id</th>
		<th>Company name</th>
		<th>Partner Name</th>
		<th>City</th>
		<th>Booking Amount</th>
		<th>Advance Amount</th>
		<th>Evibe Service Fee</th>
		<th>Booking Amount - 18%</th>
		<th>Evibe Service Fee - 18%</th>
		<th>CGST - 18%</th>
		<th>SGST - 18%</th>
		<th>IGST - 18%</th>
		<th>GST - 18%</th>
		<th>Booking Amount - 5%</th>
		<th>Evibe Service Fee - 5%</th>
		<th>CGST - 5%</th>
		<th>SGST - 5%</th>
		<th>IGST - 5%</th>
		<th>GST - 5%</th>
		<th>Refund Amount</th>
		<th>Settlement Amount</th>
		<th>Settlement Done At</th>
		<th>GSTIN</th>
	</tr>
	@if(isset($settlementArray) && count($settlementArray))
		@foreach($settlementArray as $settlementDatum)
			<tr>
				<td>
					@if(isset($settlementDatum['settlementId']) && $settlementDatum['settlementId'])
						{{ $settlementDatum['settlementId'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['partnerData']['company']) && $settlementDatum['partnerData']['company'])
						{{ $settlementDatum['partnerData']['company'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['partnerData']['person']) && $settlementDatum['partnerData']['person'])
						{{ $settlementDatum['partnerData']['person'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['partnerData']['city']) && $settlementDatum['partnerData']['city'])
						{{ $settlementDatum['partnerData']['city'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['settlementBookingAmount']) && $settlementDatum['settlementBookingAmount'])
						{{ $settlementDatum['settlementBookingAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['settlementAdvanceAmount']) && $settlementDatum['settlementAdvanceAmount'])
						{{ $settlementDatum['settlementAdvanceAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['settlementEvibeFee']) && $settlementDatum['settlementEvibeFee'])
						{{ $settlementDatum['settlementEvibeFee'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['normalData']['bookingAmount']) && $settlementDatum['normalData']['bookingAmount'])
						{{ $settlementDatum['normalData']['bookingAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['normalData']['evibeServiceFee']) && $settlementDatum['normalData']['evibeServiceFee'])
						{{ $settlementDatum['normalData']['evibeServiceFee'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['normalData']['cgst']) && $settlementDatum['normalData']['cgst'])
						{{ $settlementDatum['normalData']['cgst'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['normalData']['sgst']) && $settlementDatum['normalData']['sgst'])
						{{ $settlementDatum['normalData']['sgst'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['normalData']['igst']) && $settlementDatum['normalData']['igst'])
						{{ $settlementDatum['normalData']['igst'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['normalData']['gst']) && $settlementDatum['normalData']['gst'])
						{{ $settlementDatum['normalData']['gst'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['starData']['bookingAmount']) && $settlementDatum['starData']['bookingAmount'])
						{{ $settlementDatum['starData']['bookingAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['starData']['evibeServiceFee']) && $settlementDatum['starData']['evibeServiceFee'])
						{{ $settlementDatum['starData']['evibeServiceFee'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['starData']['cgst']) && $settlementDatum['starData']['cgst'])
						{{ $settlementDatum['starData']['cgst'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['starData']['sgst']) && $settlementDatum['starData']['sgst'])
						{{ $settlementDatum['starData']['sgst'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['starData']['igst']) && $settlementDatum['starData']['igst'])
						{{ $settlementDatum['starData']['igst'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['starData']['gst']) && $settlementDatum['starData']['gst'])
						{{ $settlementDatum['starData']['gst'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['settlementPartnerBoreRefund']) && $settlementDatum['settlementPartnerBoreRefund'])
						{{ $settlementDatum['settlementPartnerBoreRefund'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['settlementPartnerAmount']) && $settlementDatum['settlementPartnerAmount'])
						{{ $settlementDatum['settlementPartnerAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['settlementDoneAt']) && $settlementDatum['settlementDoneAt'])
						{{ $settlementDatum['settlementDoneAt'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlementDatum['partnerData']['gstin']) && $settlementDatum['partnerData']['gstin'])
						{{ $settlementDatum['partnerData']['gstin'] }}
					@else
						--
					@endif
				</td>
			</tr>
		@endforeach
	@endif
</table>
</body>
</html>