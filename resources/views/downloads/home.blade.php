@extends('layout.base')

@section('content')

	<div class="col-md-12">
		@if (count($errors))
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				@foreach ($errors->all() as $error)
					<div>{!! $error !!}</div>
				@endforeach
			</div>
		@endif
		{{-- <a href="{{ route('downloads.star-orders') }}" class="in-blk btn btn-primary mar-r-15 mar-b-15">Star Order Settlements</a>  --}}
		<button   data-toggle="modal" data-target="#settlementDateModal" class="in-blk btn btn-primary mar-r-15 mar-b-15">Star Order Settlements</button>
		<a href="{{ route('downloads.planners') }}" class="in-blk btn btn-primary mar-r-15 mar-b-15">Planners</a>
		<a href="{{ route('downloads.venues') }}" class="in-blk btn btn-primary mar-r-15 mar-b-15">Venues</a>
		<a href="#" id="gstCalculateBtn" class="in-blk btn btn-primary mar-r-15 mar-b-15">GST Settlements</a>
	</div>
	<div class="clearfix"></div>

	@include('downloads.modals')
	@include("finance.settlementModal",['buttonText'=>'Download']);
	{{-- Hidden Data --}}
	<input type="hidden" id='custom-settlement-url' value="/downloads/star-orders">
@endsection

@section('javascript')
	<script type="text/javascript">

		$(document).ready(function () {
			$('.filter-date').datetimepicker({
				timepicker: false,
				scrollInput: false,
				scrollMonth: false,
				closeOnDateSelect: true,
				format: "Y-m-d",
				disabledWeekDays: [1,2,4,5,6,0]
			});

			$('.settlement-month').datepicker({
				changeMonth: true,
				changeYear: true,
				showButtonPanel: true,
				dateFormat: 'MM yy',
				onClose: function (dateText, inst) {
					$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
					if ($(this).val() !== '') {
						$('.btn-down-settlement').prop('disabled', false);
					}
				}
			});

			$('#gstCalculateBtn').on('click', function (e) {
				e.preventDefault();

				$('#modalSettlementsMonth').modal('show');
			});
			$('#createSettlement').on('click', function (event) {
				event.preventDefault();
				var selectedDate = $("#customSettlementDate").val();
				var reqUrl = $("#custom-settlement-url").val();

				if(!(selectedDate.length)) {
					$("#errorMessage").text("Date field shouldn't be empty").removeClass("hide");
					return false;
				}
				$("#errorMessage").addClass("hide");
				$(this).text("We are on it...Please Wait").attr("disabled","true");
				window.location.href = reqUrl+"/"+selectedDate;
				setTimeout(function () {
								$('#createSettlement').text("Download").attr("disabled","true");
								$("#settlementDateModal").modal('hide');

				}, 1000);

				// $.ajax({
				// 	url: reqUrl	,
				// 	type: 'GET',
				// 	dataType: 'json',
				// 	data:{
				// 		'customSelectedDate' : selectedDate 
				// 	},
				// 	success: function (data) {
				// 			var $success = "Your Download Will Start Soon...";
							
				// 			window.showNotySuccess($success);
				// 			$("#createSettlement").text("Download").attr("disabled","false");
				// 			$("#settlementDateModal").modal('hide');
						
						
				// 	},
				// 	fail: function () {
				// 		window.showNotyError();
				// 		$("#createSettlement").text("Download").attr("disabled","false");

				// 	}
				// });
			});
		});

	</script>
@endsection