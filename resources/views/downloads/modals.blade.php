<!-- Modal to select settled settlements date -->
<div id="modalSettlementsMonth" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Download GST Settlement</h4>
			</div>
			<div class="pad-l-20 pad-r-20 pad-t-20">
				<form class="form form-horizontal" role="form" id="downSettlementForm"
						action="{{ route('downloads.gst-settlements') }}" method="POST">
					<div class="alert alert-danger error-msg mar-b-15 pad-8 hide"></div>
					<div class="form-group">
						<label class="col-sm-3">Settlement Month</label>
						<div class="col-sm-9">
							<input type="text" id="settlementMonth" name="settlementMonth" class="settlement-month form-control">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-danger btn-down-settlement"
						form="downSettlementForm" disabled>Submit
				</button>
			</div>
		</div>
		<div class="finance-loader-wrap hide">
			<div class="loader-animation-wrap">
				<img class="loader-animation" src="{{ config('evibe.gallery.host').'/img/app/load_spinner.gif' }}"
						alt="">
			</div>
		</div>
	</div>
</div>