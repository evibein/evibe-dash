<html>
<body>
<table>
	<tr>
		<th>name</th>
		<th>option_id</th>
		<th>option_type_id</th>
		<th>booking_id</th>
		<th>partner</th>
		<th>vendor_amount</th>
		<th>partner_id</th>
		<th>partner_type_id</th>
		<th>party_date</th>
		<th>booking_amount</th>
		<th>transport_charges</th>
		<th>net_booking_amount</th>
		<th>advance_amount</th>
		<th>settlement_amount</th>
		<th>is_star_order</th>
	</tr>
	@if(isset($starOrdersData) && count($starOrdersData))
		@foreach($starOrdersData as $starOrderData)
			<tr>
				<td>
					@if(isset($starOrderData['name']) && $starOrderData['name'])
						{{ $starOrderData['name'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['option_id']) && $starOrderData['option_id'])
						{{ $starOrderData['option_id'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['option_type_id']) && $starOrderData['option_type_id'])
						{{ $starOrderData['option_type_id'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['booking_id']) && $starOrderData['booking_id'])
						{{ $starOrderData['booking_id'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['partner']) && $starOrderData['partner'])
						{{ $starOrderData['partner'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['vendor_amount']) && $starOrderData['vendor_amount'])
						{{ $starOrderData['vendor_amount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['partner_id']) && $starOrderData['partner_id'])
						{{ $starOrderData['partner_id'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['partner_type_id']) && $starOrderData['partner_type_id'])
						{{ $starOrderData['partner_type_id'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['party_date']) && $starOrderData['party_date'])
						{{ $starOrderData['party_date'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['booking_amount']))
						{{ $starOrderData['booking_amount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['transport_charges']))
						{{ $starOrderData['transport_charges'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['net_booking_amount']))
						{{ $starOrderData['net_booking_amount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['advance_amount']))
						{{ $starOrderData['advance_amount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['settlement_amount']))
						{{ $starOrderData['settlement_amount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($starOrderData['is_star_order']))
						{{ $starOrderData['is_star_order'] }}
					@else
						--
					@endif
				</td>




				<td>
					@if(isset($settlement['partnerName']) && $settlement['partnerName'])
						{{ $settlement['partnerName'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['partnerCity']) && $settlement['partnerCity'])
						{{ $settlement['partnerCity'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['bookingAmount']) && $settlement['bookingAmount'])
						{{ $settlement['bookingAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['advanceAmount']) && $settlement['advanceAmount'])
						{{ $settlement['advanceAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['evibeServiceFee']) && $settlement['evibeServiceFee'])
						{{ $settlement['evibeServiceFee'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['cgstAmount']) && $settlement['cgstAmount'])
						{{ $settlement['cgstAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['sgstAmount']) && $settlement['sgstAmount'])
						{{ $settlement['sgstAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['igstAmount']) && $settlement['igstAmount'])
						{{ $settlement['igstAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['gstAmount']) && $settlement['gstAmount'])
						{{ $settlement['gstAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['partnerBoreRefund']) && $settlement['partnerBoreRefund'])
						{{ $settlement['partnerBoreRefund'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['settlementAmount']) && $settlement['settlementAmount'])
						{{ $settlement['settlementAmount'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['settlementDoneAt']) && $settlement['settlementDoneAt'])
						{{ $settlement['settlementDoneAt'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['accountNumber']) && $settlement['accountNumber'])
						{{ $settlement['accountNumber'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['bankName']) && $settlement['bankName'])
						{{ $settlement['bankName'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['ifscCode']) && $settlement['ifscCode'])
						{{ $settlement['ifscCode'] }}
					@else
						--
					@endif
				</td>
				<td>
					@if(isset($settlement['gstin']) && $settlement['gstin'])
						{{ $settlement['gstin'] }}
					@else
						--
					@endif
				</td>
			</tr>
		@endforeach
	@endif
</table>
</body>
</html>