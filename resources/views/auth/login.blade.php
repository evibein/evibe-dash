@extends('layout.base')

@section('content')
	<div class="container-fluid">
		<div class="form-cnt col-md-4 col-sm-8 col-md-offset-4 col-sm-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Signin</h3>
				</div>
				<div class="panel-body">
					<div class="errors-box">
						@if ($errors->any() || Session::has('auth_error'))
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<ul>
									@if (Session::has('auth_error'))
										<li>{{ Session::get('auth_error') }}</li>
									@endif
									@if ($errors->any())
										@foreach($errors->all() as $message)
											<li>{!! $message !!}</li>
										@endforeach
									@endif
								</ul>
							</div>
						@endif
					</div>
					<div class="login-form">
						{{ Form::open(array(
                            'url' => 'login',
                            'autocomplete' => 'on',
                            'class' => 'form-horizontal'
                        )) }}
						<div class="form-group">
							{{ Form::label('username', 'Email') }}
							<input type="text" name="username" placeholder="user@example.com" class="form-control" value="{{old('username')}}">
						</div>
						<div class="form-group">
							{{ Form::label('password', 'Password') }}
							{{ Form::password('password', array(
								'placeholder' => '●●●●●●●●●●',
								'class' => 'form-control'
							)) }}
						</div>
						<div class="form-group">
							{{ Form::input('checkbox', 'rememberme', null, array('id' => 'rememberme')) }}
							{{ Form::label('rememberme', 'Remember me', array('class' => 'text-normal')) }}
							{{ Form::input('submit', 'login', 'Sign in', array('class' => "btn btn-danger pull-right")) }}
						</div>
						{{ Form::token() }}
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop