@extends('layout.base')

@section('content')
<div class="container page-content image-search-warp">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="title-sec pad-t-10">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<h2 class="panel-title">Search all images by IDs</h2>
					<div class="pad-t-20">

						<!-- search from begin -->
						<div class="pull-left">
							<form id="searchForm" role="form" class="form form-horizontal">
								<div class="in-blk mar-r-10 valign-mid">
									<input id="searchQuery" type="text" name="q" class="form-control u-seach-box" placeholder="Enter image ID" value="{{ Input::get('q') }}" />
								</div>
								<div class="in-blk">
									<input type="submit" class="btn btn-info" value="Search">
									@if (Input::has('q') && Input::get('q'))
										<a href="{{ URL::route('images.search') }}" class="pad-l-3">Clear search</a>
									@endif
								</div>
							</form>
						</div>
						<!-- search form end -->

						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>

			<!-- image result being -->
			<div class="pad-t-30">
				
				<div class="col-sm-12 col-md-12 col-lg-12">
					
					@if (count($data['images']))
						@foreach ($data['images'] as $key => $image)
							<div class="pad-b-20">
								<div class="pull-left pad-r-20">
									<img src="{{ $image['path'] }}" class="img img-rounded">
								</div>
								<div class="pull-left valign-middle">
									<div class="form-group">
										<label>Code: </label>
										<span>{{ $image['code'] }}</span>
									</div>
									<div class="form-group">
										<label>{{ $image['mapper']['type'] }}:</label>
										<a href="{{ $image['mapper']['url'] }}" target="_blank">{{ $image['mapper']['code'] }}</a>
									</div>
									<div class="form-group">
										<label>Price: </label>
										<span>Rs. {{ AppUtil::formatPrice($image['mapper']['price']) }}</span>
									</div>
									<div class="form-group">
										<label>Provider: </label>
										<a href="{{$image['provider']['link']}}" target="_blank">{{ $image['provider']['code'] }}</a>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach

					@elseif (Input::has('q') && Input::get('q') && !count($data['images']))

						<div class="alert alert-danger">
							<i class="glyphicon glyphicon-warning-sign"></i>
							<span> No image found for you search.</span>
						</div>

					@else

						<div class="alert alert-danger">
							<i class="glyphicon glyphicon-warning-sign"></i>
							<span> You have not searched for any image ID. Please enter an image ID in the search box above and click on "Search" button.</span>
						</div>

					@endif
				</div>

				<div class="clearfix"></div>
			</div>
			<!-- image result end-->
		</div>
	</div>
</div>
@stop