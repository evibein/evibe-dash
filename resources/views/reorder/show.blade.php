@extends('layout.base')

@section('content')
	<div class="col-sm-8 col-sm-offset-2">
		<div class="panel panel-default reorder-panel">
			<div class="panel-heading">
				<h4 class="no-mar">Reorder {{ucfirst($data['eventType']->name)}} {{ ucfirst($data['itemType']->name) }}@if($data['city']), {{ ucfirst($data['city']->name) }}@endif</h4>
			</div>
			<input type="hidden" value="{{ $data['cityId'] }}" id="cityId">
			<input type="hidden" value="{{ $data['pageType'] }}" id="pageType">
			<input type="hidden" value="{{ $data['occasion'] }}" id="occasion">
			<input type="hidden" value="{{ $data['tagTypeId'] }}" id="tagTypeId">
			<div class="panel-body">
				<div class="heading-wrap">
					<div class="pull-left">
						<div class="form-group in-blk">
							<label for="pageCount">Items Count</label>
							<select class="form-control" id="pageCount" name="pageCount">
								<option value="20"
										@if($data['itemCount'] == 20 ) selected @endif> 20
								</option>
								<option value="40"
										@if($data['itemCount'] == 40) selected @endif> 40
								</option>
								<option value="60"
										@if($data['itemCount'] == 60) selected @endif> 60
								</option>
								<option value="80"
										@if( $data['itemCount'] == 80) selected @endif> 80
								</option>
								<option value="100"
										@if($data['itemCount'] == 100) selected @endif> 100
								</option>
								<option value="200"
										@if( $data['itemCount'] == 200) selected @endif> 200
								</option>
								<option value="500"
										@if( $data['itemCount'] == 500) selected @endif> 500
								</option>
								<option value="1000"
										@if( $data['itemCount'] == 1000) selected @endif> 1000
								</option>
							</select>
						</div>
					</div>
					<div class="pull-right">
						<div class="in-blk mar-r-10 valign-mid">
							<label for="searchItems">Search & Add</label>
							<select id="searchItems" name="searchItems" class="form-control">
								<option value="-1">-- Select Item --</option>
							</select>
						</div>
						<div class="in-blk valign-mid">
							<button id="addItemsBtn" class="btn btn-success btn-sm mar-r-10 in-blk">
								<i class="glyphicon glyphicon-plus-sign"></i> Add Items
							</button>
							<button type="reset" class="btn btn-danger btn-sm btn-reset-reorder in-blk">
								<i class="glyphicon glyphicon-refresh"></i> Reset
							</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="alert alert-danger font-16 mar-t-15 alert-message hide">
					No Item Available For Reorder, To select different filter
					<a href="{{route('reorder.filter')}}" class="btn-link"> click here </a>.
				</div>
				<div class="page-container">
					<div class="mar-b-10 text-center mar-t-10">
						<div class="pull-left">
							<a href="{{route('reorder.filter')}}" class=" no-mar-b btn-sm btn-back">
								<i class="glyphicon glyphicon-backward"></i> Go Back
							</a>
						</div>
						<button id="btnSaveReorder" class="btn btn-group-lg btn-warning"
								data-url="{{ route('reorder.save') }}">
							<i class="glyphicon glyphicon-floppy-save"></i> Save Reorder
						</button>
						<div class="clearfix"></div>
					</div>
					<div class="table-header">
						<div class="col-sm-2 no-mar">New Priority</div>
						<div class="col-sm-6 no-mar">Items</div>
						<div class="col-sm-2 no-mar">Old Priority</div>
						<div class="col-sm-2 no-mar">Action</div>
						<div class="clearfix"></div>
					</div>
					<div class="items-body">
						<div class="new-priority">
							<ul class="new-priorities fixed-heights">
								@for($i = 1; $i <= count($data['priorityItems']); $i++)
									<li>{{ $i }}</li>
								@endfor
							</ul>
						</div>
						<div class="item-container">
							<ul class="item-wrap fixed-heights" id="sortable">
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="hide">
					<ul id="allItems" class="hide">
						@foreach($data['allItems'] as $item)

							@if($data['pageType'] == config('evibe.ticket_type.packages') ||
							$data['pageType'] == config('evibe.ticket_type.resort') ||
							$data['pageType'] == config('evibe.ticket_type.villa')||
							$data['pageType'] == config('evibe.ticket_type.lounge') ||
							$data['pageType'] == config('evibe.ticket_type.food')||
							$data['pageType'] == config('evibe.ticket_type.venue-deals') ||
							$data['pageType'] == config('evibe.ticket_type.couple-experiences') ||
							$data['pageType'] == config('evibe.ticket_type.tents'))
								<li data-id="{{ $item->id }}"
										data-code="{{ $item->code }}"
										data-name="{{ substr($item->name ,0,40).'..'}}"
										data-place4="{{ $item->price }}"
										data-priority="@if($item->packagePriority) {{ $item->packagePriority }} @else -- @endif">
								</li>
							@endif
							@if($data['pageType'] == config('evibe.ticket_type.entertainments'))
								<li data-id="{{ $item->id }}"
										data-code="{{ $item->code }}"
										data-name="{{ substr($item->name ,0,40).'..'}}"
										data-place4="{{ $item->min_price }} @if($item->max_price) - {{$item->max_price}}@endif"
										data-priority="@if($item->servicePriority) {{ $item->servicePriority }} @else -- @endif">
								</li>
							@endif
							@if($data['pageType'] == config('evibe.ticket_type.trends'))
								<li data-id="{{ $item->id }}"
										data-code="{{ $item->code }}"
										data-name="{{ substr($item->name ,0,40).'..'}}"
										data-place4="{{ $item->price }} @if($item->price_max) - {{$item->price_max}}@endif"
										data-priority="@if($item->trendPriority) {{ $item->trendPriority }} @else -- @endif">
								</li>
							@endif
							@if($data['pageType'] == config('evibe.ticket_type.cakes') )
								<li data-id="{{ $item->id }}"
										data-code="{{ $item->code }}"
										data-name="{{ $item->title }}"
										data-place4="{{ $item->price }}"
										data-priority="@if($item->cakePriority) {{ $item->cakePriority }} @else -- @endif">
								</li>
							@endif
							@if($data['pageType'] == config('evibe.ticket_type.decors'))
								<li data-id="{{ $item->id }}"
										data-code="{{ $item->code }}"
										data-name="{{ $item->name }}"
										data-place4="{{$item->price }}"
										data-priority="@if($item->decorPriority) {{ $item->decorPriority }} @else -- @endif">
								</li>
							@endif
							@if($data['pageType'] == config('evibe.ticket_type.venue_halls'))
								<li data-id="{{ $item->id }}"
										data-code="{{ $item->hall_code }}"
										data-name="{{ $item->venue_name }}"
										data-place3="{{ $item->hall_name }}"
										data-place4="{{ $item->location }}"
										data-priority="{{ $item->hallPriority}}">
								</li>
							@endif

						@endforeach
					</ul>
					<ul class="item-render">
						<li class="list" id="item" data-id="">
							<div class="left">
								<div>
									<span class="code"></span>
									<span class="name"></span>
								</div>
								<div>
									<span class="venue-name"></span>
									<span class="location"></span>
								</div>
							</div>
							<div class="old-priority center"></div>
							<div class="right">
								<a href="#" data-id="" class="btn btn-danger btn-sm btn-remove-item">
									<i class="glyphicon-th-large glyphicon glyphicon-remove-circle"></i>
								</a>
							</div>
							<div class="clearfix"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<textarea id="priorityItems" class="hide">{{ json_encode($data['priorityItems']) }}</textarea>
	<input id="baseUrl" type="hidden"
			data-url="{{ route('reorder.show', [$data['cityId'], $data['occasion'], $data['pageType'], $data['tagTypeId']]) }}">
	<input id="currentItemType" type="hidden" value="{{ $data['pageType'] }}">
	<input type="hidden" id="itemType" value="{{ $data['pageType'] }}">
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/reorder.js");
		});
	</script>
@endsection