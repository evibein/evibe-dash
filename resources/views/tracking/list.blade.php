@extends('layout.base')

@section('title')
	<title>Order Tracking | Evibe.in Dash</title>
@stop

@section('content')
	<div class="panel panel-default mar-r-10 mar-l-10">
		<div class="pad-t-10 text-center">
			<ul class="list-inline package-heading">
				<li class="@if(request()->is('tracking/party/*')) active @endif">
					<a href="{{route('tracking.list')}}">Order Tracking </a>
				</li>
				<li class="@if(request()->is('tracking/pending/*')) active @endif">
					<a href="{{route('tracking.pending.feedback')}}">Feedback </a>
				</li>
			</ul>
		</div>
		<div class="pad-t-20">
			<div class="col-sm-2 col-md-2 col-lg-2">
				<div>
					<h5 class="no-mar pull-left">Filter Options</h5>
					<div class="pull-right">
						@if($data['isShowReset'])
							<a class="btn btn-xs btn-warning mar-l-20"
									href="@if($data['allParties'] == 1) {{route('tracking.party.all',["past"])}} @else {{route('tracking.list')}} @endif">Reset</a>
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="filter-group">
					<div class="pad-b-20 ">
						<label for="searchQuery" class="filter-title">Search</label>
						<div class="text-center">
							<input id="searchQuery" class="form-control" type="text" name="query"
									placeholder="Name, Order Id"
									value="{{ request()->input('query') }}"/>
						</div>
					</div>
					<div class="ls-none no-mar no-pad">
						<label class="filter-title">By Party Date</label>
						<div class="pad-b-20">
							<div class="text-center">
								<input type="text" class="form-control filter-pd-start"
										value="{{ request()->input('start_date') }}" placeholder="Start Date">
							</div>
							<div class="in-blk text-center mar-t-10">
								<input type="text" class="form-control filter-pd-end"
										value="{{ request()->input('end_date') }}" placeholder="End Date">
							</div>
						</div>
						@php $explodePartnerId = explode('-', request()->input('p_id')) @endphp
						<div class="form-group order-tracking-provider-filter">
							<label for="selectProvider" class="filter-title">By Provider</label>
							<select name="selectProvider" id="selectProvider" class="form-control">
								<option value="all" data-type="all">All Providers</option>
								@foreach($data['providers'] as $provider)
									<option value="{{ $provider['id']."-".$provider['type'] }}"
											@if(isset($explodePartnerId[0]) && isset($explodePartnerId[1])
											&& ($explodePartnerId[0] == $provider['id'])
											&& ($explodePartnerId[1] == $provider['type']))selected @endif>{{ $provider['name'] . " - " . $provider['person'] }}</option>
								@endforeach
							</select>
						</div>
						<div class="pad-b-20">
							<label for="city" class="filter-title">By City</label>
							<select name="city" id="city" class="form-control city-clas">
								<option value="all">All Cities</option>
								@foreach($data['cities'] as $city)
									<option value="{{ $city->id }}"
											@if(request()->input('city')== $city->id)selected @endif>{{ $city->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="pad-t-15 text-center">
							<button id="btnFilter" class="btn btn-info btn-filter-by-price">FILTER NOW</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Filter options end -->

			<!-- booking results begin -->
			<div class="col-sm-10 col-md-10 col-lg-10">
				<div class="text-center font-16 pad-b-10">
					@if($data['allParties'] == 1)
						<b>Completed Orders [{{ $data['count'] }}]</b>
						<a href="{{route('tracking.list')}}" class="pull-right">To Track</a>
					@else
						<b>To Track [{{ $data['count'] }}]</b>
						<a href="{{route('tracking.party.all',["past"])}}"
								class="pull-right">Completed Orders</a>
					@endif
					@if(session()->has('successMsg'))
						<div class="alert alert-success font-14 pad-5 no-mar-b mar-t-5">
							<a class="close" data-dismiss="alert">&times;</a>
							{{session('successMsg')}}
						</div>
					@elseif(session()->has('errorMsg'))
						<div class="alert alert-danger font-14 pad-5 no-mar-b mar-t-5">
							<a class="close" data-dismiss="alert">&times;</a>
							{{session('errorMsg')}}
						</div>
					@endif
				</div>
				@if (isset($data['bookings']) && count($data['bookings']) > 0)
					@foreach ($data['bookings'] as $booking)
						<div class="ticket-wrap" data-id="{{ $booking->id }}">
							<div class="top-sec tracking-header">
								<div class="col-sm-6 col-md-6 col-lg-6">
									<a class="sub-sec-title"><i
												class="glyphicon glyphicon-screenshot"></i> Order Status :
									</a>
									@foreach($data['orderStatus'] as $orderState)
										@if($booking->order_status_id == $orderState->id)
											<div class="order-status-wrap {{ $orderState->color }}">
												<span><b>{{ $orderState->status }}</b></span>
											</div>
										@endif
									@endforeach
									@if($booking->order_status_id == null)
										<div class="order-status-wrap red">
											<span class="font-14"><b>Not Tracked</b></span>
										</div>
									@endif
								</div>
								<div class="col-sm-6 text-center col-md-6 col-lg-6 no-pad-l">
									<a class="sub-sec-title"><i
												class="glyphicon glyphicon-time"></i> Party Date & Time :
									</a>
									@if ($booking->party_date_time)
										<b>{{ date("d M Y \(D\), H:i:s", $booking->party_date_time) }}</b>
									@else
										---
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="no-mar tracking-body">
								<div class="pad-l-10 pad-r-10">
									<div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
										<table class="table table-condensed table-bordered table-striped text-center mar-b-10">
											<thead>
											<tr>
												<td colspan="2">
													<label>Customer</label>
													@if ($booking->party_date_time > \Carbon\Carbon::now()->timestamp &&
															$booking->order_status_id != config("evibe.order_status.tracked") &&
															is_null($booking->order_tracking_customer_handler_id))
														<a class="show-customer-track pull-right pad-r-6"
																data-id="{{ $booking->id }}">Mark Done</a>
													@elseif (!is_null($booking->order_tracking_customer_handler_id))
														<a class="show-customer-track pull-right"
																data-id="{{ $booking->id }}">See customer response</a>
													@endif
												</td>
											</tr>
											</thead>
											<tr>
												<td class="lbl-val">
													@if(isset($booking->ticket) && $booking->ticket)
														<b>{{ $booking->ticket->name }}</b>
													@else
														---
													@endif
												</td>
												<td class="lbl-val">
													@if(isset($booking->ticket) && $booking->ticket && $booking->ticket->phone)
														<b>{{ $booking->ticket->phone}} /</b> @else --- @endif
													@if(isset($booking->ticket) && $booking->ticket && $booking->ticket->alt_phone)
														<b>{{$booking->ticket->alt_phone }}</b> @else --- @endif
												</td>
											</tr>
										</table>
										<div class="table table-bordered text-center no-pad-b">
											@if($booking->order_tracking_customer_handler_id)
												Customer order tracked by
												@foreach($data['handlers'] as $handler)
													@if($booking->order_tracking_customer_handler_id == $handler->id)
														<b>{{ $handler->name }}</b>
													@endif
												@endforeach
												at
												@foreach($data['timings'] as $key => $timing)
													@if($key == $booking->id && $timing['customer'])
														<b>{{ $timing['customer']->updated_at }}</b>
													@endif
												@endforeach
											@else
												Customer order tracking is not started.
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
										<table class="table table-condensed table-bordered table-striped text-center mar-b-10">
											<thead>
											<tr>
												<td colspan="2">
													<label>Partner</label>
													@if ($booking->party_date_time > \Carbon\Carbon::now()->timestamp &&
															$booking->order_status_id != config("evibe.order_status.tracked") &&
															is_null($booking->order_tracking_partner_handler_id))
														<a class="pull-right order-tracking-unavailable-button"
																data-id="{{ $booking->id }}"> Partner unavailable</a>
														<a class="show-partner-track pull-right pad-r-10"
																data-id="{{ $booking->id }}">Mark Done</a>
													@elseif (!is_null($booking->order_tracking_partner_handler_id))
														<a class="show-partner-track pull-right"
																data-id="{{ $booking->id }}">See partner response</a>
													@endif
												</td>
											</tr>
											</thead>
											<tr>
												<td class="lbl-val">
													@if(isset($booking["provider"]) && $booking["provider"])
														<b>{{ $booking["provider"]["person"] }}</b> @else --- @endif
												</td>
												<td class="lbl-val">
													@if(isset($booking["provider"]) && $booking["provider"])
														<b>{{ $booking["provider"]["phone"] }} / </b> @else --- @endif
													@if(isset($booking["provider"]) && $booking["provider"])
														<b>{{ $booking["provider"]["alt_phone"] }}</b> @else --- @endif
												</td>
											</tr>
										</table>
										<div class="text-center table table-bordered no-pad-b">
											@if($booking->order_tracking_partner_handler_id)
												Partner order tracked by
												@foreach($data['handlers'] as $handler)
													@if($booking->order_tracking_partner_handler_id == $handler->id)
														<b>{{ $handler->name }}</b> @endif
												@endforeach
												at
												@foreach($data['timings'] as $key => $timing)
													@if($key == $booking->id && $timing['partner'])
														<b>{{ $timing['partner']->updated_at }}</b>
													@endif
												@endforeach
											@else
												Partner order tracking is not started.
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>

						<div id="modalOrderTracking{{ $booking->id }}" class="modal fade" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-tracking-wrap">
								<div class="modal-content">
									<div class="modal-header">
										<div class="modal-title text-center">
											<h4 class="font-16 no-mar-b no-mar-t order-track-modal-header">TRACK ORDER</h4>
										</div>
									</div>
									<form class="form form-horizontal order-track-form" action="{{ route('tracking.save') }}" method="post" role="form">
										<div class="modal-footer no-mar-t">
											<div class="ticket-booking-wrap col-md-9 col-xs-12 pad-t-3 font-12" id="{{ $booking->id }}">
												<div class="col-lg-12 no-pad-l">
													@if($booking->booking_type_details) ({!! $booking->booking_type_details !!})@endif
													<div class="clearfix"></div>
												</div>
												<div class="col-lg-12 col-md-12 col-sm-12 booking-details">
													<div class="col-lg-4 col-sm-4 col-md-4">
														<label>Booking Id: </label>
														<div> {{ $booking->booking_id }}</div>
													</div>
													<div class="col-lg-6 col-sm-6 col-md-6">
														<label>Party Location </label>
														<div>
															@if ($booking->ticket && $booking->ticket->area && $booking->ticket->area->name)
																{{ $booking->ticket->area->name }}, @else --- @endif
															@if ($booking->ticket && $booking->ticket->city && $booking->ticket->city->name)
																{{ $booking->ticket->city->name }} @else --- @endif
														</div>
													</div>
													<div class="col-lg-2 col-sm-2 col-md-2">
														<label>Ticket Id</label>
														<div>
															{{ $booking->ticket_id }}
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-lg-12 col-md-12 col-sm-12 mar-t-10 no-pad-l no-pad-r">
													<div class="col-lg-4 col-md-4 col-sm-4 no-pad-l">
														<div class="info-box">
															<label>Order Details
																@if(isset($booking["provider"]) && $booking["provider"] && is_null($booking["provider"]["deleted_at"]))
																	<a href="{{ config("evibe.host")."tickets/" . $booking['ticket_id']."/bookings" }}"
																			target="_blank">[{{ $booking['provider']['code'] }}]</a>
																@elseif(isset($booking["provider"]) && $booking['provider'])
																	<strike><span>{{ $booking['provider']['code'] }}</span></strike>
																@endif
															</label>
															<div>
																@if($booking->booking_info)
																	{{$booking->booking_info }}
																@else
																	Not Available
																@endif
															</div>
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-4 no-pad-l">
														<div class="info-box">
															<label>Prerequisites</label>
															<div>
																@if($booking->prerequisites)
																	{{$booking->prerequisites }}
																@else
																	<i>Not Available</i>
																@endif
															</div>
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-4 no-pad-l no-pad-r">
														<div class="info-box">
															<label>Facts</label>
															<div>
																@if($booking->facts)
																	{{$booking->facts }}
																@else
																	<i>Not Available</i>
																@endif
															</div>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-lg-12 col-md-12 col-sm-12 booking-details mar-t-10">
													<div class="col-lg-3 col-sm-3 col-md-3">
														<label>Booking Amount </label>
														<div>
															<span class="rupee-font">&#8377;</span>
															<span>{{ AppUtil::formatPrice($booking->booking_amount) }}</span>
														</div>
													</div>
													<div class="col-lg-3 col-sm-3 col-md-3">
														@if($booking->is_advance_paid)
															<div class="adv-paid">
																<label>Advance Paid</label>
																<div>
																	<span class="rupee-font">&#8377;</span>
																	<span>{{ AppUtil::formatPrice($booking->advance_amount) }}</span>
																</div>
															</div>
														@else
															<div class="adv-unpaid">
																<label>Advance to be Paid</label>
																<div>
																	<span class="rupee-font">&#8377;</span>
																	<span>{{ AppUtil::formatPrice($booking->advance_amount) }}</span>
																</div>
															</div>
														@endif
													</div>
													<div class="col-lg-3 col-md-3 col-sm-3">
														<label>Balance Amount</label>
														<div>
															<span class="rupee-font">&#8377;</span>
															<span>
												@if ($booking->is_advance_paid){{ AppUtil::formatPrice($booking->booking_amount - $booking->advance_amount) }}
																@else{{ AppUtil::formatPrice($booking->booking_amount) }}
																@endif
											</span>
														</div>
													</div>
													<div class="col-lg-3 col-md-3 col-sm-3">
														<label>Payment Reference </label>
														<div>
															@if ($booking->payment_reference)
																{{ $booking->payment_reference }}
															@else
																---
															@endif
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												@if(isset($data["checkoutFields"][$booking->id]))
													<?php $counter = 0 ?>
													<div class="col-lg-12 col-md-12 col-sm-12 mar-t-10 no-pad-l">
														@foreach($data["checkoutFields"][$booking->id] as $checkoutField)
															<div class="col-lg-4 col-sm-4 col-md-4 no-pad-l class text-black">
																<div class="info-box pad-l-3 mar-b-10">
																	<label> {{ ucwords($checkoutField["identifier"]) }} </label>
																	<div>{{ $checkoutField["value"] }}</div>
																</div>
															</div>
															<?php $counter++ ?>
															@if($counter !=0 && $counter %3 == 0)
																<div class="clearfix"></div>
															@endif
														@endforeach
													</div>
												@endif
												<div class="clearfix"></div>
												@if($booking->updated_message)
													<div class="col-lg-8 col-md-8 info-box col-sm-8">
														<label>Last updated for </label>
														<div>
															{!! $booking->updated_message !!}
														</div>
													</div>
													<div class="clearfix"></div>
												@endif
												<div class="clearfix"></div>
											</div>
											<div class="pad-l-20 pad-r-20 col-md-3 col-xs-12 text-center">
												<div class="order-track-input">
													<div class="customer-tracking-questions mar-b-20">
														@foreach($data['trackingQuestions'] as $question)
															@if($question->is_customer == 1)
																<div class="mar-b-10">
																	<b>{!! $question->question !!}<br></b>
																	<input type="radio" name="{{ $question->id }}" id="{{ $question->id.$booking->id }}" value="1" class="add_required_customer"
																			@if(!is_null($booking->order_tracking_customer_handler_id)) disabled @endif
																	@foreach($data["orderTracking"] as $tracking)
																	@if($booking->id == $tracking->booking_id && $tracking->tracking_question_id == $question->id && $tracking->value == 1) checked @endif
																			@endforeach>
																	<label for="{{ $question->id.$booking->id }}" class="pad-l-3 pad-r-10"> Yes</label>
																	<input type="radio" name="{{ $question->id }}" id="{{ $question->id.$booking->id."1" }}" value="2"
																			@if(!is_null($booking->order_tracking_customer_handler_id)) disabled @endif
																	@foreach($data["orderTracking"] as $tracking)
																	@if($booking->id == $tracking->booking_id && $tracking->tracking_question_id == $question->id && $tracking->value == 2)checked @endif
																			@endforeach>
																	<label for="{{ $question->id.$booking->id."1" }}" class="pad-l-3"> No</label>
																	<input type="hidden" name="isCustomer" class="isCustomer" value="1">
																</div>
															@endif
														@endforeach
														<label for="customerOrderUpdate">Customer Updates(optional)</label>
														<textarea name="customerOrderUpdate" id="customerOrderUpdate" class="form-control" placeholder="Please enter customer updates."
																@if($booking->party_date_time < \Carbon\Carbon::now()->timestamp) disabled @endif>
															@if($booking->customer_order_update){!! $booking->customer_order_update !!} @endif
														</textarea>
													</div>
													<div class="partner-tracking-questions mar-b-20">
														@php $isVenue = $booking->is_venue_booking == 1 ? 2 : 1; @endphp
														@foreach($data['trackingQuestions'] as $question)
															@if($question->is_partner == 1 && (is_null($question->is_venue) || ($question->is_venue == $isVenue)))
																<div class="mar-b-10">
																	<b>{!! $question->question !!}</b><br>
																	<input type="radio" name="{{ $question->id }}" id="{{ $question->id.$booking->id }}" class="add_required_partner" value="1"
																			@if(!is_null($booking->order_tracking_partner_handler_id)) disabled @endif
																	@foreach($data["orderTracking"] as $tracking)
																	@if($booking->id == $tracking->booking_id && $tracking->tracking_question_id == $question->id && $tracking->value == 1) checked @endif
																			@endforeach>
																	<label for="{{ $question->id.$booking->id }}" class="pad-l-3 pad-r-10"> Yes</label>
																	<input type="radio" name="{{ $question->id }}" id="{{ $question->id.$booking->id."1" }}" value="2"
																			@if(!is_null($booking->order_tracking_partner_handler_id)) disabled @endif
																	@foreach($data["orderTracking"] as $tracking)
																	@if($booking->id == $tracking->booking_id && $tracking->tracking_question_id == $question->id && $tracking->value == 2) checked @endif
																			@endforeach>
																	<label for="{{ $question->id.$booking->id."1" }}" class="pad-l-3"> No</label>
																</div>
															@endif
														@endforeach
														<label for="partnerOrderUpdate">Partner Update(optional)</label>
														<textarea name="partnerOrderUpdate" id="partnerOrderUpdate" class="form-control"
																@if($booking->party_date_time < \Carbon\Carbon::now()->timestamp) disabled @endif placeholder="Please enter partner update">
																		@if($booking->partner_order_update){{ $booking->partner_order_update }} @endif
																	</textarea>
														@if($isVenue == 2)
															<label for="partnerDeliveryPersonDetails" class="mar-t-15">Who is the Point of Contact from the hotel for this party?</label>
														@else
															<label for="partnerDeliveryPersonDetails" class="mar-t-15">Who is the Point of Contact from your team for this order?</label>
														@endif
														<textarea name="partnerDeliveryPersonDetails" class="partner_delivery_person_details form-control" id="partnerDeliveryPersonDetails"
																@if(!is_null($booking->order_tracking_partner_handler_id)) disabled @endif
														placeholder="Self (or) Others - Name & phone required">@if($booking->partner_delivery_person_details){{ trim($booking->partner_delivery_person_details) }}@else{{ "" }}@endif</textarea>
														@if($isVenue == 2)
															<label for="partnerDeliveryTimeDetails" class="mar-t-15">What is the setup finishing time?</label>
														@else
															<label for="partnerDeliveryTimeDetails" class="mar-t-15">What is the arrival time of you/your team?</label>
														@endif
														<textarea name="partnerDeliveryTimeDetails" class="partner_delivery_time_details form-control" id="partnerDeliveryTimeDetails"
																@if(!is_null($booking->order_tracking_partner_handler_id)) disabled @endif
														placeholder="Ex: 09:00 AM">@if($booking->partner_delivery_time_details){{ trim($booking->partner_delivery_time_details) }}@else{{ "" }}@endif</textarea>
													</div>
													<button type="button" class="btn btn-default in-blk" data-dismiss="modal">Cancel</button>
													<button type="submit" class="btn btn-danger in-blk btn-order-track-save"
															@if($booking->party_date_time < \Carbon\Carbon::now()->timestamp) disabled @endif>
														Submit
													</button>
												</div>
											</div>
										</div>
										<input type="hidden" name="orderBookingId" id="orderBookingId"
												value="{{ $booking->id }}">
									</form>
								</div>
							</div>
						</div>
						<div id="partnerUnavailable{{ $booking->id }}" class="modal fade" tabindex="-1"
								role="dialog">
							<div class="modal-dialog modal-partner-unavailable">
								<div class="modal-content">
									<div class="modal-header">
										<div class="modal-title text-center">
											<label class="font-16 order-track-modal-header">Escalate Partner Tracking Issue</label>
										</div>
									</div>
									<form class="form form-horizontal order-track-form"
											action="{{ route('tracking.partner.unavailable') }}"
											method="post"
											role="form">
													<textarea name="partnerUnavailableInput"
															id="partnerUnavailableInput"
															class="form-control mar-l-10 mar-r-10 mar-t-15 mar-b-15"
															placeholder="Please enter comments (optional)">@if($booking->partner_order_update){{ $booking->partner_order_update }} @endif</textarea>
										<div class="text-center mar-b-15">
											<button type="button" class="btn btn-default in-blk"
													data-dismiss="modal">Cancel
											</button>
											<button type="submit" onclick="return confirm('Are you sure?')"
													class="btn btn-danger in-blk btn-order-track-save">Submit
											</button>
										</div>
										<input id="orderBookingId" type="hidden" name="orderBookingId"
												value="{{ $booking->id }}">
									</form>
								</div>
							</div>
						</div>
					@endforeach
					<div class="pad-t-10 text-center">
						<div>{{ $data['bookings']->appends(Input::except('page'))->links() }}</div>
					</div>
				@else
					<div class="alert alert-danger">
						No bookings found.
					</div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/tracking/tracking.js");
		});
	</script>
@stop