@extends('layout.base')

@section('content')
	<div class="container page-content tickets-warp">
		<div class="panel panel-default">
			<div class="panel-body">

				<div class="title-sec pad-t-10">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h2 class="panel-title">Showing list of all Bookings</h2>
						<div class="pad-t-20">
							<!-- search from begin -->
							<div class="pull-left">
								<div class="pad-b-20 ">
									<div class="text-center">
										<div class="in-blk">
											<input id="searchQuery" class="form-control" type="text" name="query"
													placeholder="Search by name, Id"
													value="{{ request()->input('query') }}"/>
										</div>
										<div class="in-blk">
											<button id="btnFilter" onClick="search_func()" class="btn btn-info">Search
											</button>
										</div>
									</div>
								</div>
							</div>
							<!-- search form end -->
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="pad-t-30">
					<!--  Filter options begin -->
					<div class="col-sm-3 col-md-3 col-lg-3">
						<div class="pull-right">
							@if($data['isShowReset'])
								<a class="btn btn-xs btn-warning mar-l-20"
										href={{ route('booking.concept') }}>Reset Filter</a>
							@endif
						</div>
						<h4 class="no-mar text-center">Filter options</h4>
						<div class="filter-group">
							<div class="filter-title">By City</div>
							<select name="city" id="city" class="form-control">
								<option value="all">All Cities</option>
								@foreach($data['cities'] as $city)
									<option value="{{ $city->id }}"
											@if(request()->input('city')== $city->id)selected @endif>{{ $city->name }}</option>
								@endforeach
							</select>

							<div class="filter-title mar-t-10">By Source</div>
							<select name="filterSource" id="filterSource" class="form-control">
								<option value="all">All Sources</option>
								@foreach($data['sources'] as $source)
									<option value="{{ $source->id }}"
											@if(request()->input('source')== $source->id)selected @endif>{{ $source->name }}</option>
								@endforeach
							</select>

							<div class="filter-title mar-t-10">By Concept Type</div>
							<select name="filterConceptType" id="filterConceptType" class="form-control">
								<option value="all">All Concept Types</option>
								@foreach($data['ConceptTypes'] as $ConceptType)
									<option value="{{ $ConceptType->id }}"
											@if(request()->input('conceptType')== $ConceptType->id)selected @endif>{{ $ConceptType->name }}</option>
								@endforeach
							</select>

							<div class="filter-title mar-t-10">By Occasion</div>
							<select name="filterEvent" id="filterEvent" class="form-control">
								<option value="all">All Occasions</option>
								@foreach($data['events'] as $event)
									<option value="{{ $event->id }}"
											@if(request()->input('eventType')== $event->id)selected @endif>{{ $event->name }}</option>
								@endforeach
							</select>

							<div class="filter-title mar-t-10">By Date</div>
							From
							<input type="date" class="form-control" id="filterFromDate"
									value="{{ request()->input('fromDate') }}">
							To
							<input type="date" class="form-control" id="filterToDate"
									value="{{ request()->input('toDate') }}">

							<div class="mar-t-20 text-center">
								<button id="btnFilterSubmit" onClick="search_func()"
										class="btn btn-info">Filter
								</button>
							</div>
						</div>
					</div>
					<!-- Filter options end -->

					<!-- Ticket results begin -->
					<div class="col-sm-9 col-md-9 col-lg-9">
						<div class="tickets-list-wrap">
							@if (count($data['bookings']) == 0)
								<div class="alert alert-danger">
									<span>Sorry, no Bookings were found.</span>
								</div>
							@else
								<ul class="tickets-list ls-none">
									@foreach ($data['bookings'] as $booking)
										<li class="ticket-wrap" data-id="{{ $booking->id }}">
											<div class="top-sec">
												<a href="{{ route('booking.concept.info',$booking->id) }}">
													<div class="col-sm-4 col-md-4 col-lg-4">
														<div class="sub-sec-title">Name</div>
														<div class="sub-sec-value">
															<i class="glyphicon glyphicon-user"></i>
															@if ($booking->ticket)
																{{ $booking->ticket->name }} <br>
																({{ $booking->booking_id }})
															@else
																---
															@endif
														</div>
													</div>
													<div class="col-sm-4 col-md-4 col-lg-4 no-pad-l">
														<div class="sub-sec-title">Party Date</div>
														<div class="sub-sec-value">
															<i class="glyphicon glyphicon-time"></i>
															@if ($booking->party_date_time)
																{{ date('d M Y (D)',$booking->party_date_time) }}
															@else
																---
															@endif
														</div>
													</div>
													<div class="col-sm-4 col-md-4 col-lg-4 no-pad">
														<div class="sub-sec-title">Area</div>
														<div class="sub-sec-value">
															<i class="glyphicon glyphicon-map-marker"></i>
															@if ($booking->ticket && $booking->ticket->area && $booking->ticket->city)
																{{ $booking->ticket->area->name }}, {{ $booking->ticket->city->name }}
															@else
																---
															@endif
														</div>
													</div>
													<div class="clearfix"></div>
												</a>
											</div>
											<div class="mid-sec no-mar pad-b-10">
												<div class="col-sm-4 col-md-4 col-lg-4">
													<div class="sub-sec-title">Booking Source</div>
													<div class="sub-sec-value">
														@if($booking->ticket && $booking->ticket->source)
															{{ $booking->ticket->source->name }}
															@if($booking->ticket->source_specific)
																({{ $booking->ticket->source_specific }})
															@else
																(---)
															@endif
														@endif
													</div>
												</div>
												<div class="col-sm-4 col-md-4 col-lg-4">
													<div class="sub-sec-title">Occasion</div>
													<div class="sub-sec-value">
														@if($booking->ticket && $booking->ticket->event)
															{{ $booking->ticket->event->name }}
														@else
															---
														@endif
													</div>
												</div>
												<div class="col-sm-4 col-md-4 col-lg-4">
													<div class="sub-sec-title">Booking Concept</div>
													<div class="sub-sec-value">
														@if ($booking->bookingConcept)
															<b>{{ $booking->bookingConcept->name }}</b>
														@else
															---
														@endif
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</li>
									@endforeach
								</ul>

								<!-- pagination begin -->
								<div class="pad-t-10 text-center">
									<div>{{ $data['bookings']->appends(Input::except('page'))->links() }}</div>
								</div>
								<!--  pagination end -->
							@endif
						</div>
					</div>
					<!-- Ticket results end -->

					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			//for browser which doesn't support type date
			if ($('[type="date"]').prop('type') !== 'date') {
				$('[type="date"]').datepicker();
			}

			$('#btnFilter').click(function (event) {
				updateUrlByFilters(event);
			});

			$('#city').change(updateUrlByFilters);
			$('#filterSource').change(updateUrlByFilters);
			$('#filterConceptType').change(updateUrlByFilters);
			$('#filterEvent').change(updateUrlByFilters);
			$('#btnFilterSubmit').click(updateUrlByFilters);
			$('#downloadData').click(updateUrlByFilters);

			function updateUrlByFilters(event) {
				event.preventDefault();

				var query = $('#searchQuery').val();
				var city = $('#city').val();
				var source = $('#filterSource').val();
				var conceptType = $('#filterConceptType').val();
				var filterEvent = $('#filterEvent').val();
				var fromDate = $('#filterFromDate').val();
				var toDate = $('#filterToDate').val();

				url = 'booking-concept?city=' + city +
					'&source=' + source +
					'&conceptType=' + conceptType +
					'&eventType=' + filterEvent;

				if (fromDate) {
					url += '&fromDate=' + query;
				}

				if (toDate) {
					url += '&toDate=' + query;
				}

				if (query) {
					url += '&query=' + query;
				}

				location.href = url;
			}
		});
	</script>
@endsection