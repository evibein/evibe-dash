@extends('layout.base')

@section('content')
	<a href="{{ route('booking.concept') }}" class="mar-l-20 mar-b-10"> << Show All bookings</a>
	<div class="col-sm-10 col-md-10 col-lg-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 pad-t-10">
		<div class="section-info mar-l-20 mar-r-20">
			<div class="mar-l-5 mar-b-10 pad-5 alert-info text-center"><i
						class="glyphicon glyphicon-info-sign"></i> Ticket booking information
			</div>
			<?php $mappedValues = $booking->mapping->getMappedValues(); ?>
			<div class="ticket-booking-wrap pad-t-3">
				<div>
					<div class="col-lg-12 col-sm-12 col-md-12 no-pad-l">
						{{strtoupper($mappedValues['name'])}} @if($booking->booking_type_details) ({!! $booking->booking_type_details !!})@endif
						@if($booking->bookingConcept && ($bookingConceptName = $booking->bookingConcept->name))
							<div class="pull-right mar-l-10 alert-warning">
								Booking concept type : <b>{!! $bookingConceptName !!}</b>
							</div>
						@endif
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 booking-details">
						<div class="col-lg-3 col-sm-3 col-md-3">
							<label>Booking Id: </label>
							<div> {{ $booking->booking_id }}</div>
						</div>
						<div class="col-lg-3 col-sm-3 col-md-3">
							<label>Party Date </label>
							<div> {{date("d M Y",$booking->party_date_time) }}</div>
						</div>
						<div class="col-lg-3 col-sm-3 col-md-3">
							<label>Start Time</label>
							<div> {{date("h:i A",$booking->party_date_time) }}</div>
						</div>
						<div class="col-lg-3 col-sm-3 col-md-3">
							<label>End Time</label>
							<div>
								@if($booking->party_end_time)
									{{date("d M, h:i A",$booking->party_end_time) }}
								@else
									--
								@endif
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 mar-t-10 no-pad-l no-pad-r">
						<div class="col-lg-6 col-md-6 col-sm-6 no-pad-l">
							<div class="info-box">
								<label>Provider Details
									@if(!$mappedValues['deleted'])
										{{ ucfirst($booking->mapping->mapType->name) }} - {{ $mappedValues['code'] }}
									@else
										<strike><span>{{ $booking->mapping->mapType->name }} - </span></strike>
										<strike><span>{{ $mappedValues['code'] }}</span></strike>
									@endif
								</label>
								<div>
									Name:
									@if($mappedValues['self'] && $mappedValues['self']->provider && $mappedValues['self']->provider->name)
										{{ $mappedValues['self']->provider->name }}
									@else
										---
									@endif
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 no-pad-l">
							<div class="info-box">
								<label>Feedback</label>
								<div>
									@foreach($reviews as $review)
										@if($review->ticket_booking_id == $booking->id)
											Customer Rating: <b>{{ $review['rating'] }}</b><br>
											Customer Review: <b>{{ $review['review'] }}</b>
											@foreach($reviewAnswer as $item)
												@if($item->review_id == $review->id && $item->question && $item->rating)
													<br>{{ ucfirst($item->question->question) }}:
													<b>{{ $item->rating }}</b>
												@endif
											@endforeach
										@endif
									@endforeach
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="booking-details mar-t-10">
					<div class="pad-l-10">
						<h4 class="mar-t-5 no-mar-b">Gallery</h4>
						@if($booking->gallery->count() > 0)
							@foreach($booking->gallery as $gallery)
								<div class="img-wrap in-blk">
									<a href="{{ $gallery->getOriginalImagePath() }}" target="_blank"
											class="booking-gallery-link">
										<img src="{{ $gallery->getResultsImagePath() }}"
												class="ticket-booking-image" alt="{{ $gallery->title }}">
									</a>
								</div>
							@endforeach
						@else
							<div class="alert alert-danger pad-8 no-mar-b">
								<i class="glyphicon glyphicon-warning-sign"></i>
								No Image found for this ticket booking
							</div>
						@endif
					</div>
				</div>

				<div class="booking-details mar-t-10">
					<div class="pad-l-10">
						<h4 class="mar-t-5 no-mar-b">Delivery Images</h4>
						@if(count($booking->deliveryImages) > 0)
							@foreach($booking->deliveryImages as $delivery)
								<?php $link = config('evibe.gallery.host') . '/ticket/' . $booking->ticket->id . '/' . $booking['id'] . '/partner/' . $delivery->url; ?>
								<a href="{{ $link }}"><img src="{{ $link }}"></a>
							@endforeach
						@else
							<div class="alert alert-danger pad-8 no-mar-b">
								<i class="glyphicon glyphicon-warning-sign"></i>
								No Delivery Image uploaded for this ticket booking
							</div>
						@endif
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection