$(document).ready(function () {
	$('#bookingsCalendar').fullCalendar({
		header: {
			left: 'title',
			center: '',
			right: 'prev,next month,agendaWeek,agendaDay'
		},
		firstDay: 1,
		titleFormat: {
			month: "MMM yyyy",
			week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d, yyyy}",
			day: 'dd, MMM d, yyyy'
		},
		eventColor: '#3AAD3F',
		editable: false,
		eventStartEditable: false,
		eventDurationEditable: false,
		eventClick: function (calEvent, jsEvent, view) {

			var eventHtml = '<div class="event-click-info">' +
				'<div><b>Customer Name:</b> ' + calEvent.customerName + '</div>' +
				'<div><b>Vendors Mapped:</b> ' + calEvent.vendors + '</div>' +
				'<div><a href="/tickets/view?type=1&id=' + calEvent.ticketId + '">View ticket details<div>' +
				'</div>';

			$('#eventDescription').html(eventHtml);
		},
		eventSources: [
			{
				url: '/dashboard/calendar-bookings',
				type: 'POST',
				startParam: $('#bookingsCalendar').fullCalendar('getView').start,
				success: function (data, textStatus, jqXHR) {
					$('#totalBookings').text(data.length);
				},
				error: function (data, textStatus, jqXHR) {
				}
			}
		]
	});
});

(function initPlugins() {
	$('.filter-pd-start').datetimepicker({
		format: 'Y/m/d',
		onShow: function (ct) {
			this.setOptions({
				maxDate: $('.filter-pd-end').val() ? $('.filter-pd-end').val() : false
			})
		},
		timepicker: false,
		scrollInput: false,
		scrollMonth: false,
		closeOnDateSelect: true
	});

	$('.filter-pd-end').datetimepicker({
		format: 'Y/m/d',
		onShow: function (ct) {
			this.setOptions({
				minDate: $('.filter-pd-start').val() ? $('.filter-pd-start').val() : false
			})
		},
		timepicker: false,
		scrollInput: false,
		scrollMonth: false,
		closeOnDateSelect: true
	});
})();

(function dashBoardFilters() {
	function updateUrlByFilters(event) {
		event.preventDefault();

		var startDate = $('.filter-pd-start').val();
		var endDate = $('.filter-pd-end').val();
		var url = "dash-board?";
		var isUrlChanged = false;

		if (startDate && endDate) {
			isUrlChanged = true;
			url += 'start_date=' + startDate + '&end_date=' + endDate;
		}
		else {
			alert("please enter both the inputs")
		}

		if (isUrlChanged) {
			location.href = url;
		}
	}

	// filter options
	$('#btnFilter').click(updateUrlByFilters);
})();