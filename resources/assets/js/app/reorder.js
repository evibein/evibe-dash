$(function () {

	var isItemsListModified = false;
	var baseUrl = $('#baseUrl').data('url');

	function updateLocation(newLocation) {
		if (isItemsListModified) {
			window.onbeforeunload = function () {
				return "Data will be lost, are you sure?";
			};

		}
		location.replace(newLocation);
	}

	function setRenderValues(markup, data) {
		if (data.id) {
			$(markup).attr('id', 'item' + data.id);
			$(markup).attr('data-id', data.id);
			$(markup).find('.code').text(data.code ? data.code : '');
			$(markup).find('.name').text(data.name ? data.name : '');
			$(markup).find('.venue-name').text(data.place3 ? data.place3 + ', ' : '');
			$(markup).find('.location').text(data.place4 ? data.place4 : '');
			$(markup).find('.old-priority').text(data.priority ? data.priority : '--');
			$(markup).find('.btn-remove-item').attr('data-id', data.id);
			return markup;
		}
	}

	function setRenderValuesForHalls(markup, data) {
		if (data.id) {
			$(markup).attr('id', 'item' + data.id);
			$(markup).attr('data-id', data.id);
			$(markup).find('.code').text(data.hall_code);
			$(markup).find('.name').text('- ' + data.hall_name);
			$(markup).find('.venue-name').text(data.venue_name + ', ');
			$(markup).find('.location').text(data.location);
			$(markup).find('.old-priority').text(data.hallPriority ? data.hallPriority : '--');
			$(markup).find('.btn-remove-item').attr('data-id', data.id);

			return markup;
		}
	}

	function setRenderValuesForPackage(markup, data) {
		if (data.id) {
			$(markup).attr('id', 'item' + data.id);
			$(markup).attr('data-id', data.id);
			$(markup).find('.name').text(data.name);
			$(markup).find('.venue-name').text(data.code);
			$(markup).find('.location').text('- Rs. ' + data.price);
			$(markup).find('.old-priority').text(data.packagePriority ? data.packagePriority : '--');
			$(markup).find('.btn-remove-item').attr('data-id', data.id);

			return markup;
		}
	}

	function setRenderValuesForDecors(markup, data) {
		if (data.id) {
			$(markup).attr('id', 'item' + data.id);
			$(markup).attr('data-id', data.id);
			$(markup).find('.name').text(data.name);
			$(markup).find('.venue-name').text(data.code);
			$(markup).find('.location').text('- Rs. ' + data.price);
			$(markup).find('.old-priority').text(data.decorPriority ? data.decorPriority : '--');
			$(markup).find('.btn-remove-item').attr('data-id', data.id);

			return markup;
		}

	}

	function setRenderValuesForCake(markup, data) {
		if (data.id) {
			$(markup).attr('id', 'item' + data.id);
			$(markup).attr('data-id', data.id);
			$(markup).find('.code').text(data.title);
			$(markup).find('.venue-name').text(data.code + ' - ');
			$(markup).find('.location').text('Rs. ' + data.price);
			$(markup).find('.old-priority').text(data.cakePriority ? data.cakePriority : '--');
			$(markup).find('.btn-remove-item').attr('data-id', data.id);

			return markup;
		}
	}

	function setRenderValuesForTrends(markup, data) {
		if (data.id) {
			$(markup).attr('id', 'item' + data.id);
			$(markup).attr('data-id', data.id);
			$(markup).find('.name').text(data.name);
			$(markup).find('.venue-name').text(data.code);
			$(markup).find('.location').text('- Rs. ' + data.price);
			$(markup).find('.old-priority').text(data.trendPriority ? data.trendPriority : '--');
			$(markup).find('.btn-remove-item').attr('data-id', data.id);

			return markup;
		}

	}

	function setRenderValuesForEntertainment(markup, data) {
		if (data.id) {
			$(markup).attr('id', 'item' + data.id);
			$(markup).attr('data-id', data.id);
			$(markup).find('.name').text(data.name);
			$(markup).find('.venue-name').text(data.code);
			$(markup).find('.location').text('- Rs. ' + data.min_price);
			$(markup).find('.old-priority').text(data.servicePriority ? data.servicePriority : '--');
			$(markup).find('.btn-remove-item').attr('data-id', data.id);
			return markup;
		}

	}

	function renderExistingItems() {
		var itemsData = JSON.parse($('#priorityItems').val());
		var itemType = $('#itemType').val();
		var itemMarkup = '';
		var currentItem = '';
		var packagesTypes = [1, 17, 18, 19, 20, 21, 22, 24];
		var pkgType = packagesTypes.find(isPkgType);
		if (itemsData.length) {
			for (i = 0; i < itemsData.length; i++) {
				currentItem = itemsData[i];
				itemMarkup = $('.item-render li').clone();
				if (pkgType !== undefined && pkgType == itemType)
					itemMarkup = setRenderValuesForPackage(itemMarkup, currentItem);
				if (itemType == 14)
					itemMarkup = setRenderValuesForEntertainment(itemMarkup, currentItem);
				if (itemType == 5)
					itemMarkup = setRenderValuesForTrends(itemMarkup, currentItem);
				if (itemType == 6)
					itemMarkup = setRenderValuesForCake(itemMarkup, currentItem);
				if (itemType == 13)
					itemMarkup = setRenderValuesForDecors(itemMarkup, currentItem);
				if (itemType == 15)
					itemMarkup = setRenderValuesForHalls(itemMarkup, currentItem);
				$('#sortable').append(itemMarkup);
			}
		}
		else {
			$('.alert-message').removeClass('hide');
			$('.page-container').addClass('hide');
		}
		// function for checking in array
		function isPkgType(type) {
			return type == itemType;
		}

	}

	// Check if a given item is already in the re-ordering list
	function isItemInList(itemId) {
		if (itemId && $('#item' + itemId).length) {
			return true;
		}
		return false;
	}

	function addNewItemToList(itemId) {
		// render new li to existing items list
		var itemData = $('#allItems').find('li[data-id=' + itemId + ']');
		var markup = $('.item-render li').clone();
		var currentItemType = $('#currentItemType').val();
		var data = {
			'id': itemData.data('id'),
			'code': itemData.data('name'),
			'name': '',
			'place4': itemData.data('code') + ' - Rs. ' + itemData.data('place4'),
			'priority': itemData.data('priority')
		};

		if (currentItemType == 15) {
			data.code = itemData.data('code');
			data.name = itemData.data('name');
			data.place3 = itemData.data('place3');
			data.place4 = itemData.data('place4');
		}

		markup = setRenderValues(markup, data);
		$('#sortable').prepend(markup);

		// check for valid item selection and add new li element to new priorities
		if (markup) {
			$('.new-priorities').append('<li>' + $('#sortable li').length + '</li>');
		}
		else {
			alert('Please select a valid items!');
		}

		// set isItemsListModified
		isItemsListModified = true;
	}

	function initPage() {
		renderExistingItems();
	}

	initPage();

	// Check for any list modifications - reordered, item add, item remove
	$("#sortable").sortable({
		change: function (event, ui) {
			isItemsListModified = true;
		}
	}).disableSelection();

	$('#sortable').on('click', '.btn-remove-item', function (event) {
		event.preventDefault();
		var itemId = $(this).data('id');
		var $item = $('#item' + itemId);
		if ($item.length) {
			if ($item.remove()) {
				$('.new-priorities li:last').remove();
				isItemsListModified = true;
			}
		}
	});

	$('#btnSaveReorder').click(function () {
		var url = $(this).data('url');
		var serializeArray = $("#sortable").sortable('toArray', {
			attribute: 'data-id'
		});
		if (isItemsListModified == false && serializeArray.length != 1) {
			//in that case only one element may present without any priority
			alert('There are no changes to save.');
		}
		else {
			if (confirm("Are you sure to change the order?")) {
				window.showLoading();
				$.post(url, {
					'cityId': $('#cityId').val(),
					'occasionId': $('#occasion').val(),
					'pageId': $('#currentItemType').val(),
					'tagId': $('#tagTypeId').val(),
					'order': serializeArray
				}).done(function (data, textStatus, jqXHR) {
					window.hideLoading();
					if (data.success) {
						window.showGenericModal("Success", "New priorities have been saved successfully.", true);
					}
					else {
						window.showNotyError("FAIL! Could not save new priorities #DataFail");
					}
				}).fail(function (data, textStatus, jqXHR) {
					window.hideLoading();
					window.showNotyError("FAIL! Could not save new priorities #PostFail");
				});
			}
		}
	});

	// Selectize
	var selectSearchValue, $selectSearchValue;
	var results = [];
	var target;

	$('#allItems li').each(function (key, value) {
		results.push({
			id: $(value).attr('data-id'),
			name: $(value).attr('data-name'),
			code: $(value).attr('data-code'),
			place3: $(value).attr('data-place3'),
			place4: $(value).attr('data-place4')
		});
	});

	$selectSearchValue = $('#searchItems').selectize({
		valueField: 'id',
		labelField: 'name',
		searchField: ['name', 'code', 'place4', 'location'],
		render: {
			option: function (data, escape) {
				target = '';
				if (data.code) {
					target = '<div class="item-wrap">' +
						'<span class="name">' + (data.code ? '[' + escape(data.code) + ']' + '</span>&nbsp;' : '' ) +
						'<span class="code">' + (data.place3 ? escape(data.place3) + '</span>' + '<div class="clearfix"></div>' : '') +
						'<span class="venue-name">' + (data.name ? escape(data.name) + ', </span>' : '') +
						'<span class="location">' + (data.place4 ? escape(data.place4) + '</span>' : '') +
						'</div>';
				}
				return target;
			}
		},
		openOnFocus: true
	});

	selectSearchValue = $selectSearchValue[0].selectize;
	selectSearchValue.addOption(results);
	selectSearchValue.refreshOptions(false);

	// Add selected item to list
	$('#addItemsBtn').click(function (event) {
		// get item selected
		var selectedItemId = selectSearchValue.getValue();

		// check if item is correct
		if (!selectedItemId) return;

		// check if item has already been added
		// add item at top
		if (!isItemInList(selectedItemId)) {
			addNewItemToList(selectedItemId);
		}
		else {
			alert('Item already exists');
		}
	});

	$('.btn-reset-reorder').click(function (event) {
		//reset all changes on the current page
		var isConfirm = confirm('Are you sure? your all changes on the current page will be reset');
		if (isConfirm) {
			location.reload(true);
		}
	});

	//select number of items per page
	$('#pageCount').change(function () {
		var value = $(this).val();
		updateLocation(baseUrl + '?per_page=' + value);
	});

});
