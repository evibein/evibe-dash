$(document).ready(function () {

	// For add, edit pages - begin
	function setRoleId() {
		$('#roleId').val($('#vendorType option:selected').eq(0).data('role'));
	}

	$('#vendorType').change(setRoleId); // role id

	setRoleId();
	// For add, edit pages - end

	function initPlugins() {
		$("#updateDate, .edit-update-date, #editEventDate").datetimepicker({timepicker: false});
		$("#vendorUpdateDate, #editVendorUpdateDate").datetimepicker({
			scrollInput: false,
			scrollMonth: false,
			scrollTime: false
		});

	}

	function initModals() {
		var allModals = [
			'modalVendorConfirm'
		];

		for (var key = 0; key < allModals.length; key++) {
			$('#' + allModals[key]).modal({
				show: false,
				keyboard: false,
				backdrop: 'static'
			});
		}
	}

	function openFollowup() {
		var followup = window.location.href;
		var openFollowup = followup.substring(followup.indexOf("?") + 1);
		if (openFollowup == "vf=open") {
			$('#vendorFollowupAction').click();
		}
	}

	// Add vendor conv update
	$('#btn_add_vendor_update').click(function (e) {
		$(this).toggleClass('hide');
		$('.no-vendor-udpate-message').toggleClass('hide');
		$('.add-vendor-update-cnt').toggleClass('hide');
	});

	$('#add_vendor_update_cancel_btn').click(function (e) {
		$('#btn_add_vendor_update').toggleClass('hide');
		$('.no-vendor-udpate-message').toggleClass('hide');
		$('.add-vendor-update-cnt').toggleClass('hide');
	});

	$('#add_vendor_update_submit_btn').click(function (e) {
		e.preventDefault();
		$('.add-vendor-update-errors-cnt .errors-list').empty(); // clear errors cnt
		var url = $('#addConvUpdateForm').attr('action');
		var statusId = parseInt($('#vendorUpdateStatus').val(), 10);
		var updateDate = $('#vendorUpdateDate').val();
		var vendorId = $('#vendorUpdateId').val();

		var inputData = {
			'status': statusId,
			'updateDate': updateDate,
			'vendorId': vendorId
		};

		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: inputData,
			success: function (data, textStatus, jqXHR) {
				if (data.success) location.reload();
				else {
					for (var key in data.errors) { // show errors
						$('.add-vendor-update-errors-cnt .errors-list').append('<li>' + data.errors[key] + '</li>');
					}
					$('.add-vendor-update-errors-cnt').removeClass('hide');
				}
			},
			error: function (data, textStatus, jqXHR) {
				window.showNotyError();
			}
		});
	});

	// Edit vendor conversation update
	$('.btn-edit-vendor-update, .btn-edit-vendor-update-cancel').click(function (e) {
		e.preventDefault();
		var vendorUpdateId = parseInt($(this).data('vui'), 10);
		$('.vui-' + vendorUpdateId + ' .view-mode').toggleClass('hide');
		$('.vui-' + vendorUpdateId + ' .edit-mode').toggleClass('hide');
	});

	$('.btn-edit-vendor-update-submit').click(function (e) {
		e.preventDefault();
		var vendorUpdateId = parseInt($(this).data('vui'), 10);
		var parentElm = '.vui-' + vendorUpdateId;
		var url = $('#editConvUpdateForm').attr('action');
		var updateDate = $(parentElm + ' .edit-vendor-update-date').val();
		var statusId = parseInt($(parentElm + ' .edit-vendor-update-status').val(), 10);
		$('.edit-vendor-update-errors-cnt .errors-list').empty(); // clear errors cnt
		var inputData = {
			'vendorUpdateId': vendorUpdateId,
			'status': statusId,
			'updateDate': updateDate
		};

		$.ajax({
			url: url,
			type: 'PUT',
			dataType: 'json',
			data: inputData,
			success: function (data, textStatus, jqXHR) {
				if (data.success) location.reload();
				else {
					for (var key in data.errors) { // show errors
						$('.edit-vendor-update-errors-cnt .errors-list').append('<li>' + data.errors[key] + '</li>');
					}
					$('.edit-vendor-update-errors-cnt').removeClass('hide');
				}
			},
			error: function (data, textStatus, jqXHR) {
				window.showNotyError();
			}
		});
	});

	/**
	 * Enhancing the vendor in dash with vendor-tracking
	 * @author Vikash<vikash@evibe.in>
	 * @Since 27th April 2016
	 */

	// Delete vendor conv update
	$('.btn-delete-vendor-update').click(function (e) {
		e.preventDefault();
		if (!confirm('Are you sure to delete this update?')) return false;

		$.ajax({
			url: $(this).data('url'),
			type: 'DELETE',
			dataType: 'json',
			data: {},
			success: function (data, textStatus, jqXHR) {
				if (data.success) location.reload();
			},
			error: function (data, textStatus, jqXHR) {
				window.showNotyError();
			}
		});
	});

	//confirming the account of new vendors
	$('.btn-confirm-vendor').on('click', function (event) {
		event.preventDefault();
		var $modalVendorConfirm = $('#modalVendorConfirm');

		//clear the value of all input field;
		$modalVendorConfirm.find('.error-message').addClass('hide');
		$('.vendor-pro-password').val('');
		$('.vendor-pro-password_confirmation').val('');
		$('.vendor-commission-rate').val('');

		$modalVendorConfirm.data('url', $(this).data('url'));
		$('.vendor-pro-email').val($(this).data('email'));
		$modalVendorConfirm.modal('show');

	});

	$('.btn-vendor-confirm-submit').click(function (event) {
		event.preventDefault();
		var $modalSelectorObj = $('#modalVendorConfirm');
		var url = $modalSelectorObj.data('url');
		$modalSelectorObj.modal('hide');
		window.showLoading();
		var data = {
			email: $('.vendor-pro-email').val(),
			password: $('.vendor-pro-password').val(),
			password_confirmation: $('.vendor-pro-password_confirmation').val(),
			commission: $('.vendor-commission-rate').val(),
			type: $('#type').val()
		};
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (data) {
				if (data.success) {
					window.hideLoading();
					window.showGenericModal("Success", "Vendor Profile has been successfully confirmed with evibe")
				}
				else if (data.error) {
					window.hideLoading();
					$modalSelectorObj.modal('show');
					$('#modalVendorConfirm').find('.error-message').removeClass('hide').html(data.error);
				}
			},
			fail: function () {
				window.hideLoading();
				$modalSelectorObj.modal('hide');
				window.showNotyError("Some error occurred while saving the vendor");
			}
		})
	});

	/**
	 * @author Vikash<vikash@evibe.in>
	 * @since 30 April 2016
	 * Vendor followup section : done, edit, delete
	 */

	// followup done
	$('.btn-followup-done').on('click', function () {
		var $modalMarkDone = $('#modalFollowupDone');
		var url = $(this).data('url');
		$modalMarkDone.data('url', url);
		$modalMarkDone.modal('show');
	});

	$('.btn-done-followup-submit').click(function () {
		var url = $('#modalFollowupDone').data('url');
		if (url) {
			$.ajax({
				url: url,
				type: 'POST',
				data: {doneComment: $('.followup-done-comments').val()},
				success: function (data) {
					if (data.success) {
						location.reload();
					}
					else {
						window.showNotyError(data.error);
					}
				},
				fail: function () {
					window.showNotyError();
				}

			})
		}
		else {
			alert('Error:invalid error');
		}
	});

	//followup edit
	$('.btn-followup-edit').on('click', function () {
		var $modalFollowup = $('#modalEditFollowup');
		var comments = $(this).data('comment');
		var dateFollowup = $(this).data('followup-date');
		var url = $(this).data('url');

		dateFollowup ? $('.edit-followup-date').val(dateFollowup) : false;
		comments ? $('.followup-edit-comments').val(comments) : false;
		$modalFollowup.data('url', url);
		$modalFollowup.modal('show');
	});

	$('.btn-edit-followup-submit').click(function () {
		var url = $('#modalEditFollowup').data('url');
		var date = $('.edit-followup-date').val();
		var comments = $('.followup-edit-comments').val();

		if (url) {
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: {
					date: date,
					comment: comments
				},
				success: function (data) {
					if (data.success) {
						location.reload();
					}
					else {
						$('#modalEditFollowup').find('.alert').text(data.error).removeClass('hide');
					}
				},
				fail: function () {
					window.showNotyError();
				}
			})
		}
		else {
			alert('Error: url is not valid');
		}
	});
	//delete
	$('.btn-followup-delete').on('click', function () {
		var url = $(this).data('url');
		var archive = $(this).data('archive');
		if (url) {
			if (confirm("Are you sure ?")) {
				$.ajax({
					url: url,
					type: 'DELETE',
					dataType: 'json',
					success: function (data) {
						if (data.success) {
							location.reload();
						}
						else {
							window.showNotyError(data.error)
						}
					},
					fail: function () {
						window.showNotyError();
					}
				})
			}
		}
	});

	// refresh option of city
	function initCityAreaSelect() {
		var areaValue,
			$selectArea,
			$area = $('#area'),
			$city = $('#city');

		$city.selectize({
			onChange: onSelectCityChange
		});

		$selectArea = $area.selectize({
			valueField: 'id',
			labelField: 'name',
			searchField: ['id', 'name']
		});
		areaValue = $selectArea[0].selectize;
		if (!$area.val()) {
			areaValue.disable();
		}

		function onSelectCityChange(value, isManual) {
			var results = [];
			areaValue.disable();
			areaValue.clearOptions();
			if (!value.length || value == -1) {
				return false;
			}

			$('#allAreaOptions').find('li').each(function (count) {
				if ($(this).data('city_id') && $(this).data('city_id') == value) {
					results.push({
						id: $(this).data('id'),
						name: $(this).data('name')
					});
				}
			});

			areaValue.enable();
			areaValue.addOption(results);

			var old = $.trim($area.data('old')) && $city.data('old') == $city.val() ? $.trim($area.data('old')) : results[0].id;
			areaValue.setValue(old);
		}

		if ($city.val()) {
			onSelectCityChange($city.val(), true)
		}

		// select default city

		if ($city.data('old')) {
			$city[0].selectize.setValue($.trim($city.data('old')))
		}
	}

	// change lead status
	$('#vendorLeadStatus').change(function (event) {
		event.preventDefault();

		var data = {leadStatus: $('#vendorLeadStatus').val()};
		var vendorId = parseInt($("#vendorId").val(), 10);
		var url = '/vendors/' + vendorId + '/lead-status';

		window.performEditLeadStatus({
			'el': $(this),
			'url': url,
			'data': data,
			'wrapClass': ".vendor-lead-status-wrap",
			'allClasses': "vendor-header-info-status-hot vendor-header-info-status-medium vendor-header-info-status-cold",
			'classPrefix': "vendor-header-info-status-"
		});
	});

	initCityAreaSelect();

	initPlugins();
	initModals();
	openFollowup();
});