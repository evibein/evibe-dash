/**
 * Decors page javascript
 *
 * @author Anji
 * @since 16 Sep 2015
 */

$(document).ready(function () {

	initDecorModals();
	initItemPlugins();

	function initDecorModals() {
		var allModals = [
			'modalEditDecorInfo',
		];

		for (key in allModals) {
			$('#' + allModals[key]).modal({
				show: false,
				keyboard: false,
				backdrop: 'static'
			});
		}
	}

	function initItemPlugins() {
		// html editor
		$('.decor-terms, .decor-info, .decor-more-info, .decor-facts').wysihtml5({
			toolbar: {
				'image': false,
				'html': false,
				'blockquote': false,
				'size': 'xs'
			}
		});

		$('.decor-vendor').selectize();
	}

	// show edit decor detials modal
	$('#editDecorInfoBtn').click(function (event) {
		event.preventDefault();
		var wysihtmlInputs = [
			'.decor-info',
			'.decor-more-info',
			'.decor-terms',
			'.decor-facts'
		];
		//add Event tags

		var decorEventSelect = {};
		var item = '';
		var items = [{key: 'events', el: '#events'}];

		item = items[0];
		decorEventSelect[item['key']] = $(item['el']).selectize({
			maxItems: 1000,
			create: false,
			hideSelected: true
		});
		//set the old event tags
		function setData() {
			var ids = '', selEl = '', j = 0;
			for (i = 0; i < items.length; i++) {
				if ($(items[i]['el']).data('old')) {
					ids = $(items[i]['el']).data('old').toString().split(",");
					selEl = decorEventSelect[items[i]['key']][0].selectize;
					for (j = 0; j < ids.length; j++) selEl.addItem(ids[j], false);
				}
			}
		}

		setData();

		// set wysihtml editor values
		for (var i in wysihtmlInputs) {
			var editor = $('#modalEditDecorInfo ' + wysihtmlInputs[i]).data('wysihtml5').editor;
			if (editor) editor.setValue($(wysihtmlInputs[i]).data('val'));
		}

		$('#modalEditDecorInfo .btn-edit-decor-submit').removeAttr('disabled'); // hide existing errors
		$('#modalEditDecorInfo .alert').addClass('hide'); // hide existing errors
		$('#modalEditDecorInfo').modal('show'); // show modal
	});

	// copy terms on selection

	var copyItems = {
		'#decorCopyTerms': '.decor-terms',
		'#decorCopyFacts': '.decor-facts',
		'#decorCopyPreReq': '.decor-more-info'

	};
	$.each(copyItems, function (key, value) {
		$(key).change(function (event) {
			event.preventDefault();
			$('#modalEditDecorInfo ' + key).change(function (event) {
				event.preventDefault();
				var val = $(this).val();
				var editor = $('#modalEditDecorInfo ' + value).data('wysihtml5').editor;
				val = (val == -1) ? '' : val;
				if (editor) editor.setValue(val);
			});
		});
	});

	// submit edit decor information
	$('.btn-edit-decor-submit').click(function (event) {
		event.preventDefault();
		var that = $(this);
		var decorId = parseInt($("#decorId").val(), 10);
		var url = '/decors/' + decorId + '/update';
		var data = {
			name: $('.decor-name').val(),
			info: $('.decor-info').val(),
			more_info: $('.decor-more-info').val(),
			facts: $('.decor-facts').val(),
			min_price: $('.decor-min-price').val(),
			worth: $('.decor-worth').val(),
			max_price: $('.decor-max-price').val(),
			range_info: $('.decor-range-info').val(),
			terms: $('.decor-terms').val(),
			provider_id: $('.decor-vendor').val(),
			trans_min: $('.decor-trans-min').val(),
			trans_max: $('.decor-trans-max').val(),
			kms_free: $('.decor-kms-free').val(),
			kms_max: $('.decor-kms-max').val(),
			time_setup: $('.decor-time-setup').val(),
			time_duration: $('.decor-time-duration').val(),
			events: $('#events').val(),
			handler: $('#handler').val()
		};

		$(this).attr('disabled', 'disabled'); // disable submit button
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data,
		}).done(function (data, textStatus, jqXHR) {
			if (data.success) {
				location.reload();
			} else {
				that.removeAttr('disabled');
				$('#modalEditDecorInfo .alert').text(data.error).removeClass('hide'); // show errors
			}
		}).fail(function (jqXHR, textStatus, errorThrown) {
			$(this).removeAttr('disabled');
			window.showNotyError();
		});
	});

	// edit decor image title
	$('.btn-edit-img-title').click(function (event) {
		event.preventDefault();
		$(this).parents('.image-cnt').find('.img-title .view').addClass('hide');
		$(this).parents('.image-cnt').find('.img-title .edit').removeClass('hide');
	});

	$('.btn-img-title-edit-cancel').click(function (event) {
		event.preventDefault();
		$(this).parents('.image-cnt').find('.img-title .view').removeClass('hide');
		$(this).parents('.image-cnt').find('.img-title .edit').addClass('hide');
	});

	// add tags
	$('#selectParentTag').on('change', function (event) {
		var options = $('<option value="-1" data-parentid="-1">--select child tag--</option>');
		var childTags = '';

		if ($(this).val() != -1) {
			var val = $(this).val(); // add child tags
			childTags = $('#duplicateChildTags option[data-parentid="' + val + '"]').clone();
		}

		options = $.merge(options, childTags);
		$('#selectChildTag').html(options);
	});

});