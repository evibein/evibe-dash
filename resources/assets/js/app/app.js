$(document).ready(function () {

	$('#modalCopyPackage').modal({
		show: false,
		keyboard: false,
		backdrop: 'static'
	});

	var $changeProviderButton = $('.btn-copy-package');
	// change provider button
	$changeProviderButton.click(function (event) {
		event.preventDefault();
		$("body").css("cursor", "wait");
		$('.btn-copy-package').attr('disabled', true);

		$.ajax({
			url: $(this).data('url'),
			type: 'get',
			dataType: 'json',
			data: {typeTicketId: $('#typeTicketId').val()},
			success: function (data) {
				$(".type-mapping-name").text(data.name);
				if (data.countPackages > 0) {
					$('.copy-package-ids').empty();
					$.each(data.packages, function (count, copyPackage) {
						var $name = copyPackage.name;
						if (typeof copyPackage.name === 'undefined') {
							$name = copyPackage.title;
						}
						var optionHtml = "<option class='copyPackageId' value='" + copyPackage.id + "'>" + $name + " (" + copyPackage.code + ")</option>";
						$(optionHtml).appendTo('.copy-package-ids');
					});
				}
				if (data.countOccasions > 0) {
					$('.copy-occasion-type').empty();
					$.each(data.occasions, function (count, occasion) {
						var optionHtml = "<option class='copyOccasionId' value='" + occasion.id + "'>" + occasion.name + "</option>";
						$(optionHtml).appendTo('.copy-occasion-type');
					});
				}
				if (data.countProviders > 0) {
					$('.copy-planner').removeClass('hide');
					$('.copy-package-planner').empty();
					$.each(data.providers, function (count, provider) {
						var optionHtml = "<option class='copyProviderId' value='" + provider.id + "'>" + provider.name + "</option>";
						$(optionHtml).appendTo('.copy-package-planner');
					});
				}
				if (data.countPageTypes > 0) {
					$('.type-ticket').removeClass('hide');
					$('.copy-page-type').empty();
					$.each(data.pageTypes, function (count, pageType) {
						var optionHtml = "<option class='copyPageTypeId' value='" + pageType.id + "'>" + pageType.name + "</option>";
						$(optionHtml).appendTo('.copy-page-type');
					});
				}
				$('#modalCopyPackage').modal('show');
				$("body").css("cursor", "default");
				$('.btn-copy-package').attr('disabled', false);

				var $planner = $('#copyPackagePlannerId').selectize();
				var $package = $('#copyPackageId').selectize();
				$planner[0].selectize.setValue($.trim($('#copyPackagePlannerId').data('old')));
				$package[0].selectize.setValue($.trim($('#copyPackageId').data('old')));
			},
			fail: function () {
				$("body").css("cursor", "default");
				window.showNotyError('Some error occurred while fetching data, please try again after sometime.')
			}
		});
	});

	$(".btn-copy-save").click(function (event) {
		event.preventDefault();

		var url = $('.copy-data-form').data('url');
		var id = $('#copyPackageId').val();
		var mapTypeId = $('#typeTicketId').val();
		var providerId = $('#copyPackagePlannerId').val();
		var occasionId = $('#copyOccasionTypeId').val();
		var copyPageTypeId = $('#copyPageTypeId').val();
		$(".btn-copy-save").attr('disabled', true);
		$("body").css("cursor", "wait");

		if (id) {
			$.post(url, {
				mapId: id,
				mapTypeId: mapTypeId,
				providerId: providerId,
				occasionId: occasionId,
				pageTypeId: copyPageTypeId
			}).done(function (data) {
				if (data.success) {
					window.location = data.redirect
				}
			}).fail(function () {
				$(".btn-copy-save").attr('disabled', false);
				$("body").css("cursor", "default");
				var errorMsg = 'An unknown error has occurred, please try again later.';
				if (!alert(errorMsg)) window.location = redirect;
			})
		} else {
			alert('Please select all the fields');
			$(".btn-copy-save").attr('disabled', false);
			$("body").css("cursor", "default");
		}
	});

	(function changeSellingStatus() {
		$('#optionSellingStatus').on('change', function (e) {
			e.preventDefault();
			var color = $('option:selected', this).attr('data-color');

			$.ajax({
				url: $(this).data("url") + "/" + $(this).val(),
				type: 'post',
				dataType: 'json',
				success: function (data) {
					if (data.success) {
						$('.tkt-lead-status-wrap').attr('class', 'tkt-lead-status-wrap').addClass('ticket-header-info-status-' + color);
						showNotySuccess("Status changed successfully");
					} else if (data.error) {
						showNotyError(data.error);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					showNotyError('Error occurred while deleting tag, Please try again.')
				}
			});
		})
	})();

	function initModals() {
		$('#modalLoading').modal({
			keyboard: false,
			backdrop: 'static',
			show: false
		});

		$("#modalGeneric").modal({
			show: false,
			keyboard: true,
			backdrop: true
		});

		$('#addNewAreaModal').modal({
			show: false
		});
	}

	(function initPlugins() {
		$('[data-toggle="tooltip"]').tooltip(); // tooltips
	})();

	window.showLoading = function () {
		$('#modalLoading').modal('show');
	};

	window.hideLoading = function () {
		$('#modalLoading').modal('hide');
	};

	window.showNotyError = function (errorText) {
		if (!errorText) errorText = 'An error occurred while submitting your request. Please try again later!';
		noty({
			text: errorText,
			layout: 'top',
			type: 'error',
			closeWith: ['click', 'hover'],
			timeout: 7500
		});
	};

	window.showNotySuccess = function (message, refresh) {
		var isReload = refresh || false;
		var timeout = 6000;
		showNotySuccess(message, timeout - 1000);

		if (isReload) {
			setTimeout(function () {
				location.reload();
			}, timeout);
		}

	};

	window.showGenericModal = function (title, body, refresh, url) {
		if (!title) title = "Message";
		if (!body) return;
		refresh = (typeof refresh !== 'undefined') ? refresh : true;

		var $genModal = $('#modalGeneric');
		$genModal.find('.modal-title').text(title);
		$genModal.find('.modal-body').html(body);
		$genModal.modal('show');

		if (refresh) {
			$genModal.on('hidden.bs.modal', function (event) {
				if (url) {
					window.location.href = url;
				} else {
					window.location.reload();
				}
			});
		}
	};

	window.indexOf = function (needle) {
		if (typeof Array.prototype.indexOf === 'function') {
			indexOf = Array.prototype.indexOf;
		} else {
			indexOf = function (needle) {
				var i = -1, index = -1;
				for (i = 0; i < this.length; i++) {
					if (this[i] === needle) {
						index = i;
						break;
					}
				}
				return index;
			};
		}
		return indexOf.call(this, needle);
	};

	//function for refreshing the url with the current parameter
	window.refreshUrlWithExistingParam = function (inputName, inputValue) {
		var redirectUrl = '';
		var currentUrl = $(location).attr('href');
		if (currentUrl.indexOf("?") == -1) {
			redirectUrl = currentUrl + '?' + inputName + '=' + inputValue;
		} else if (currentUrl.indexOf(inputName + '=') == -1) {
			redirectUrl = currentUrl + '&' + inputName + '=' + inputValue;
		} else {
			var res = currentUrl.split(inputName + '=')[1].split('&')[0];
			redirectUrl = currentUrl.replace(inputName + '=' + res, inputName + '=' + inputValue);
		}
		window.location = redirectUrl;
	};

	//since 18th April 2016
	//@param: element : array;
	window.setOptionValue = function (element) {
		$.each(element, function (key, value) {
			var oldVal = $(value).data('old');
			$(value).val($.trim(oldVal));
		});
	};

	function showNotySuccess(message, timeout) {
		if (!message) message = 'The task was successful';
		noty({
			text: message,
			layout: 'top',
			type: 'success',
			closeWith: ['click', 'hover'],
			timeout: timeout
		});
	}

	// generic classes for toggling video and image
	function toggleGalleryType(type) {
		// image
		if (type == 0) {
			$('.gal-type-image').removeClass('hide');
			$('.gal-type-video').addClass('hide');
			$('#categoryType').prop('disabled', false);
		}
		// video
		else if (type == 1) {
			$('.gal-type-video').removeClass('hide');
			$('.gal-type-image').addClass('hide');
			$('#categoryType').prop('disabled', true);
		}
	}

	function initGallery() {
		var value = $('input[type=radio][name=gal-type]:checked').val();
		toggleGalleryType(value);
	}

	$('input[type=radio][name=gal-type]').change(function () {
		var type = $(this).val();
		toggleGalleryType(type);
	});

	initGallery();

	// Add new location - show modal
	$('.btn-add-new-location').click(function (e) {
		e.preventDefault();
		$('#addNewAreaModal').data('redirectto', '/vendors/new').modal('show');
	});

	// Add new location - save
	$('.btn-submit-new-area').click(function (e) {
		e.preventDefault();
		var areaName = $('#addAreaForm #areaName').val();
		var cityId = $('#addAreaForm #addAreaCity').val();
		var locationDetails = $('#locationDetails').val();
		var pinCode = $('#zipCode').val();
		if (areaName) {
			$.ajax({
				url: '/add-area',
				type: 'POST',
				dataType: 'json',
				data: {
					cityId: cityId,
					areaName: areaName,
					locationDetails: locationDetails,
					pinCode: pinCode
				},
				success: function (data, textStatus, jqXHR) {
					if (data.success) {
						location.reload();
						$('#addNewAreaModal').modal('hide');
					} else {
						var errorMsg = data.msg;
						alert(errorMsg);
					}
				},
				error: function (data, textStatus, jqXHR) {
					var errorMsg = 'An unknown error has occured, please try again later.';
					if (!alert(errorMsg)) document.location.reload();
				}
			});
		} else {
			alert('Please enter a valid area name');
		}
	});

	// show internal questions modal
	$('.iq').click(function (e) {
		e.preventDefault();
		$('.iqModal').data('redirectTo', 'iq/new');
		$('.iqModal').modal('show');
	});

	// Add internal question Model
	$(".iqSubmit").click(function (event) {
		event.preventDefault();

		var url = $('.iq').data('url');
		var redirectUrl = $('.iq').data('redirect');
		var question = $('#question').val();
		var answer = $('#answer').val();

		if (question) {
			$.post(url, {
				question: question,
				answer: answer
			}).done(function (data, textStatus, jqXHR) {
				if (data.success) {
					$('.iqModal').modal('hide');
					window.location = redirectUrl
				}
			}).fail(function (data, textStatus, jqXHR) {
				var errorMsg = 'An unknown error has occurred, please try again later.';
				if (!alert(errorMsg)) window.location = redirectUrl;
			})
		} else {
			alert('Question field is mandatory');
		}
	});

	// show Edit form for internal questions modal
	$('.iq-edit').on('click', function (e) {
		e.preventDefault();
		var question = $(this).data('question');
		$('.edit-iqModal').data('redirectTo', 'iq/answer/edit');
		$('#editQuestion').val(question);
		$('.edit-iqModal').modal('show');
		var questionId = $(this).data('id');

		//Save Edit internal question Model

		$(".edit-iqSubmit").click(function (event) {
			event.preventDefault();
			var redirectUrl = $('.iq-edit').data('redirect');
			var url = $('.iq-edit').data('url');
			var editAnswer = $('#editAnswer').val();
			var editQuestion = $('#editQuestion').val();

			if (editAnswer) {
				$.post(url, {
					editQuestion: editQuestion,
					editAnswer: editAnswer,
					id: questionId
				}).done(function (data, textStatus, jqXHR) {
					if (data.success) {
						$('.edit-iqModal').modal('hide');
						window.location = redirectUrl
					}
				}).fail(function (data, textStatus, jqXHR) {
					var errorMsg = 'An unknown error has occurred at Edit, please try again later.';
					if (!alert(errorMsg)) window.location = redirectUrl;
				})
			} else {
				alert('please enter the answer');
			}
		});

	});

	// Confirm dialogue for delete Iq
	$('.iq-delete').on('click', function (e) {
		e.preventDefault();
		var href = $(this).data('url');
		if (confirm("Are you sure")) {
			window.location = href;
		}
	});

	// embedding Wysiwyg html Editor for IQ Answers
	$('#answer, #editAnswer').wysihtml5({
		toolbar: {
			"image": false,
			'html': false,
			'blockquote': false,
			'size': 'xs'
		}
	});

	// generic class for enable date time picker
	$('.date-enable-both').datetimepicker({
		step: 30,
		scrollInput: false,
		scrollMonth: false,
		scrollTime: false,
		closeOnDateSelect: true
	});
	$('.date-enable-future').datetimepicker({
		step: 30,
		minDate: 0,
		scrollInput: false,
		scrollMonth: false,
		scrollTime: false,
		closeOnDateSelect: true
	});
	$('.date-enable-past').datetimepicker({
		step: 30,
		maxDate: 0,
		scrollInput: false,
		scrollMonth: false,
		scrollTime: false,
		closeOnDateSelect: true
	});

	// Integrating google places autoComplete
	var input = document.getElementById('areaName');
	if (typeof google !== 'undefined') {
		google.maps.event.addDomListener(window, 'load', initialize);
		google.maps.event.addDomListener(input, 'keydown', preventFormSubmit);
	}

	function initialize() {
		var options = {
			types: ['(regions)'],
			componentRestrictions: {country: 'in'}
		};
		new google.maps.places.Autocomplete(input, options);

		var autoComplete = new google.maps.places.Autocomplete(input, options);
		autoComplete.addListener('place_changed', function () {
			var place = autoComplete.getPlace();
			$('#locationDetails').val(JSON.stringify(place.address_components));
			var places = place.address_components;
			$('#zipCode').val(getPinCodeFromPlaceData(places));
		});
	}

	// function doMyFun(key) {
	// 	alert(key);
	// }

	function getPinCodeFromPlaceData(places) {
		var pincode = null;
		for (key in places) {
			var place = places[key];
			var types = place.types;

			// if types has "postal_code
			if (types && types.indexOf("postal_code") != -1) {
				pincode = place.long_name;
				break;
			}
		}

		return pincode;
	}

	// code for disabling the input text field which takes pin code
	var disableField = function () {
		var state = pincode.value.length > 0;
		document.getElementById("zipCode").disabled = state;
	};

	//

	function preventFormSubmit(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
		}
	}

	$('.btn-add-loc').click(function () { // add new location
		$('#addNewAreaModal').data('redirectto', $(this).data('redirectto')).modal();
	});

	(function whatsappModal() {
		var id;
		var sendWhatsappMsg = $('.send-whatsapp-msg');

		$('#followupDate,#nspFollowupDate').datetimepicker({
			step: 30,
			minDate: 0,
			timepicker: true,
			scrollInput: false,
			scrollMonth: false,
			closeOnDateSelect: false,
			format: "Y/m/d H:i"
		});

		$('.application').on('click', '.whatsapp-button', function (event) {
			event.preventDefault();
			id = $(this).data('id');
			$('#whatsappModal').modal('show');
		});

		sendWhatsappMsg.on('click', function () {

			var msgType = $(this).attr('data-msg-type');

			if (msgType == "other") {
				sendMessage(msgType, "none");
			} else {

				var ticketUpdateDateAndTime = $('#updateDate').val();
				var ticketUpdateType = "whatsapp";
				var ticketUpdateStatusId = $(this).attr('data-update-type');
				var ticketUpdateComments = $(this).attr('data-msg-followup-comments');
				var ticketUpdateRelatedTicketId = "";
				var isAjaxRequest = 1;
				var followUpDate = $(this).attr('data-msg-followup-date');
				var followUpType = $(this).attr('data-msg-followup-id');
				var followType = $(this).attr('data-msg-followup-text').trim();
				var closureDate = followUpDate;

				var data = {

					tuDate: ticketUpdateDateAndTime,
					tuType: ticketUpdateType,
					tuStatus: ticketUpdateStatusId,
					tuComments: ticketUpdateComments,
					relatedTicket: ticketUpdateRelatedTicketId,
					isAjaxRequest: isAjaxRequest,
					followUpDate: followUpDate,
					followUpType: followType,
					closureDate: closureDate

				};

				var newUrl = $("#route").val();
				var ur = 'tickets/' + id + '/status-update';
				var url = newUrl.replace("Id-req", id);

				sendWhatsappMsg.addClass("hide");
				$(".send-whatsapp-msg-loading").removeClass("hide");
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data: data,
					success: function (data) {
						if (data.success) {
							$('#whatsappModal').modal('hide');
							window.showNotySuccess("Ticket status updated successfully, Sending Whatsapp message");
							//location.reload();
							//window.open(data.text, '_blank');
							sendMessage(msgType, followUpType);
							$(".send-whatsapp-msg-loading").addClass("hide");
							sendWhatsappMsg.removeClass("hide");
						} else {
							showNotyError("ERROR is " + data.error);
							$(".send-whatsapp-msg-loading").addClass("hide");
							sendWhatsappMsg.removeClass("hide");
						}
					},
					error: function () {
						showNotyError("Some error occurred while processing the request. Kindly refresh the data and try again.")
					}
				});

			}
		});

		function sendMessage(msgType, followupId) {
			var msgType = msgType;
			var followupId = followupId;

			var data = {
				id: id,
				msgType: msgType,
				followupId: followupId
			};

			var url = "/ticket/whatsapp/normal";
			sendWhatsappMsg.addClass("hide");
			$(".send-whatsapp-msg-loading").removeClass("hide");

			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (data) {
					if (data.success) {
						$('#whatsappModal').modal('hide');
						if (location.href.indexOf("sales/dashboard") === -1) {
							location.reload();
						}
						window.open(data.text, '_blank');
						$(".send-whatsapp-msg-loading").addClass("hide");
						sendWhatsappMsg.removeClass("hide");
					} else {
						showNotyError("Error while sending the message " + data.text);
						$(".send-whatsapp-msg-loading").addClass("hide");
						sendWhatsappMsg.removeClass("hide");
					}
				},
				error: function () {
					showNotyError("Some error occurred while processing the request. Kindly refresh the data and try again.")
				}
			});
		}

		$('input[name=typeMessage]').on('change', function () {
			var typeMsgValue = $('input[name=typeMessage]:checked').val();

			if (typeMsgValue == "followUp") {
				if ($('#followupType').val() == 0 || $('#followupDate').val() == 0 || $('#followupComments').val() == 0) {
					sendWhatsappMsg.addClass('disabled');
				} else {
					sendWhatsappMsg.attr('data-msg-type', 'followup');
				}

				$('.type-options').removeClass('hide');
				$('.type-followup').removeClass('hide');

				$('#followupType,#followupDate,#followupComments').on('change', function () {
					var value = $('#followupType').val();
					var value1 = $('#followupDate').val();
					var value2 = $('#followupComments').val();
					if (value == 0 || value1 == 0 || value2 == 0) {
						sendWhatsappMsg.attr('data-msg-type', 'followup');
						sendWhatsappMsg.addClass('disabled');
					} else {
						sendWhatsappMsg.removeClass('disabled');
						sendWhatsappMsg.attr('data-msg-type', 'followup');
						sendWhatsappMsg.attr('data-update-type', $('#followupStatus').val());
						sendWhatsappMsg.attr('data-msg-followup-id', value);
						sendWhatsappMsg.attr('data-msg-followup-date', value1);
						sendWhatsappMsg.attr('data-msg-followup-comments', value2);
						sendWhatsappMsg.attr('data-msg-followup-text', $("#followupType :selected").text());
					}
				});

			} else if (typeMsgValue == "noResponse") {

				if ($('#followupDate').val() == 0 || $('#followupComments').val() == 0) {
					sendWhatsappMsg.addClass('disabled');
				} else {
					sendWhatsappMsg.attr('data-msg-type', 'followup');
				}
				$('.type-followup').addClass('hide');
				$('.type-options').removeClass('hide');
				$('#followupDate,#followupComments').on('change', function () {

					var value1 = $('#followupDate').val();
					var value2 = $('#followupComments').val();
					if (value1 == 0 || value2 == 0) {
						sendWhatsappMsg.attr('data-msg-type', 'noResponse');
						sendWhatsappMsg.addClass('disabled');
					} else {
						sendWhatsappMsg.removeClass('disabled');
						sendWhatsappMsg.attr('data-msg-type', 'noResponse');
						sendWhatsappMsg.attr('data-update-type', $('#noResponseStatus').val());
						sendWhatsappMsg.attr('data-msg-followup-date', value1);
						sendWhatsappMsg.attr('data-msg-followup-comments', value2);
						sendWhatsappMsg.attr('data-msg-followup-text', 'NoResponse');
					}
				});

			} else {

				$('.type-options').addClass('hide');
				sendWhatsappMsg.attr('data-msg-type', 'other');
				sendWhatsappMsg.removeClass('disabled');
			}
		});
	})();

	$('#whatsappPaymentLink').on('click', function () {
		var id = $("#hidTicketId").val();
		var data = {id: id};
		var url = "/ticket/whatsapp/order-process";
		$(this).addClass("hide");
		$("#whatsappPaymentLinkLoading").removeClass("hide");
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (data) {
				if (data.success) {
					window.open(data.text, '_blank');
					location.reload();
				} else {
					showNotyError("Please Reload The Page");
				}
			},
			error: function () {
				showNotyError("Please Refresh The Page")
			}
		});
	});

	/**
	 * Edit lead status: tickets / vendors page
	 * being used in vendors.js & tickets/base.js
	 *
	 * @author Jeevan Reddy
	 * @since 10 April 2017
	 */
	window.performEditLeadStatus = function (params) {
		var el = params.el ? params.el : "";
		var data = params.data ? params.data : {};
		var url = params.url ? params.url : false;
		var infoText = params.infoText ? params.infoText : $("#leadStatusInfoText");
		var classPrefix = params.classPrefix ? params.classPrefix : "";
		var wrapClass = params.wrapClass ? params.wrapClass : "";
		var allClasses = params.allClasses ? params.allClasses : "";

		var that = el;
		that.attr('disabled', 'disabled'); // disable select

		if (!url) {
			window.showNotyError("Request could not be send. Wrong URL given");
			return false;
		}

		$.ajax({
			url: url,
			type: 'PUT',
			dataType: 'json',
			data: data
		}).done(function (data, textStatus, jqXHR) {
			that.removeAttr('disabled');
			if (data.success) {

				// change text in header
				var className = classPrefix + data.value.toString().toLowerCase();
				$(wrapClass).removeClass(allClasses).addClass(className);

				// change text in info section
				if (infoText.length) {
					infoText.text(data.value);
				}
			} else {
				window.showNotyError("Could not update the lead status. Please try again later");
			}
		}).fail(function (jqXHR, textStatus, errorThrown) {
			that.removeAttr('disabled');
			window.showNotyError("Error occurred while updating lead status");
		});
	};
});