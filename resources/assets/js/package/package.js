$(document).ready(function () {

	var $type = $('#type');

	function typeToggle(type) {
		$('.venue-details').toggleClass(type);
		$('.vendor-details').toggleClass(type);
	}

	$('#area').selectize();

	// showing planner or venue providers based on selection
	$type.on('change', function (event) {
		event.preventDefault();
		typeToggle('hide');
	});

	(function selectizeAddTags() {
		// relation tags
		var relationTagSelect = {};
		var relations = [{key: 'selectRelationShipTag', el: '#selectRelationShipTag'}];

		relation = relations[0];
		relationTagSelect[relation['key']] = $(relation['el']).selectize({
			maxItems: 1000,
			create: false,
			hideSelected: true
		});

		// occasion tags
		var occasionTagSelect = {};
		var occasions = [{key: 'selectOccasionTag', el: '#selectOccasionTag'}];

		occasion = occasions[0];
		occasionTagSelect[occasion['key']] = $(occasion['el']).selectize({
			maxItems: 1000,
			create: false,
			hideSelected: true
		});

		// age group tags
		var ageGroupTagSelect = {};
		var ageGroups = [{key: 'selectAgeGroupTag', el: '#selectAgeGroupTag'}];

		ageGroup = ageGroups[0];
		ageGroupTagSelect[ageGroup['key']] = $(ageGroup['el']).selectize({
			maxItems: 1000,
			create: false,
			hideSelected: true
		});
	})();

	(function validateSurpriseTags() {
		$('.btn-surprise-add-tag').on('click', function (e) {
			e.preventDefault();
			if (!$('#selectRelationShipTag').val() || !$('#selectOccasionTag').val()) {
				alert("Please enter relation & occasion tags");
			}
			else {
				$('#addSurpriseTagsForm').submit();
			}
		});
	})();

	(function loadValidTagsModal() {
		$('#surpriseValidTags').on('click', function (e) {
			e.preventDefault();
			$('#modalViewSurpriseTagsModal').modal('show');
		});
	})();

	(function surprisesPackageTypeSelect() {
		$('#occasion').on('change', function (e) {
			if ($('#occasion').val() == $('#surpriseEventId').val()) {
				$('#pageType').val($('#surpriseEventTypeTicketId').val()).attr('disabled', true);
			}
			else {
				$('#pageType').attr('disabled', false);
			}
		});
	})();

	// showing details after the vendor and occasion selection.
	$('.button-add-package').click(function (event) {
		event.preventDefault();
		var occasion = $('#occasion').val();
		var pageType = $('#pageType').val();
		var vendorId = $('#vendor').val();
		var venueId = $('#venue').val();
		var redirect_url = $('.newPackageFirstForm').data('url');

		//set the value for type,venue_type
		$arrayIds = ['#venue', '#type', '#vendor'];

		if (occasion < 1) {
			window.showNotyError("Please select a valid occasion type");
			return false;
		}
		else {
			if (pageType < 1) {
				window.showNotyError('Please select a valid page type');
				return false;
			}
			location.href = redirect_url + '?type=' + $type.val() + '&vendorId=' + vendorId + '&venueId=' + venueId + '&occasion=' + occasion + '&pageType=' + pageType;
		}
	});

	function triggerJSUpdateTypeTicketModal(packageId) {
		$('#btnSaveTypeTicketForPackage').click(function (event) {
			event.preventDefault();
			var typeTicket = $('#typeTicket').val();
			$.ajax({
				url: '/packages/type-ticket/' + packageId + '/update',
				type: 'POST',
				dataType: 'json',
				data: {
					typeTicketId: typeTicket
				},

				success: function (data, textStatus, jqXHR) {
					if (data.success) {
						window.showNotySuccess(data.message);
						location.reload();
					}
				},
				error: function (data, textStatus, jqXHR) {
					window.showNotyError("Some Error occurred while getting the data, Please try again.");
				}
			});
		});
	}

	// Facilitating the BD team to change the type ticket of the package
	$('.change-type-ticket').click(function (event) {
		event.preventDefault();
		var packageId = $('#packageId').val();
		$.ajax({
			url: '/packages/type-ticket',
			type: 'GET',
			dataType: 'json',
			success: function (data, textStatus, jqXHR) {
				var optionHtml = '';
				if (data.typeTicket) {
					$.each(data.typeTicket, function (key, typeTicketOptions) {
						optionHtml = optionHtml + "<option value='" + typeTicketOptions.id + "'>" + typeTicketOptions.name + "</option>>";
					});
					$(optionHtml).appendTo('.type-ticket-details');
				}
				$('#modalAskChangeTypeTicket').modal('show');

				triggerJSUpdateTypeTicketModal(packageId);
			},
			error: function (data, textStatus, jqXHR) {
				window.showNotyError("Some Error occurred while getting the data, Please try again.");
			}
		});
	});

	$("#myTags").tagit({
		readOnly: true
	});

	$('.inner-header a').click(function () {
		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top
		}, 700);
		return false;
	});

	// edit image title
	$('.btn-edit-img-title').click(function (event) {
		event.preventDefault();
		$(this).parents('.image-cnt').find('.img-title .view').addClass('hide');
		$(this).parents('.image-cnt').find('.img-title .edit').removeClass('hide');
	});

	// close button image title
	$('.btn-img-title-edit-cancel').click(function (event) {
		event.preventDefault();
		$(this).parents('.image-cnt').find('.img-title .view').removeClass('hide');
		$(this).parents('.image-cnt').find('.img-title .edit').addClass('hide');
	});

	//delete the packages
	$('.btn-pkg-delete').click(function (e) {
		e.preventDefault();
		var url = $(this).data('url');
		var redirect = $(this).data('redirect');
		if (confirm('Are you sure ? ')) {
			$.get(url, {redirectUrl: redirect}).done(function (data) {
				window.location = redirect;
				$('.alert-success').html(data.successMsg);
			});
		}
	});

	// add tags
	$('#selectParentTag').on('change', function (event) {
		var options = $('<option value="-1" data-parentid="-1">--select child tag--</option>');
		var childTags = '';

		if ($(this).val() !== -1) {
			var val = $(this).val(); // add child tags
			childTags = $('#duplicateChildTags option[data-parentid="' + val + '"]').clone();
		}

		options = $.merge(options, childTags);
		$('#selectChildTag').html(options);
	});

	// wysiwyg editor for textarea
	$('.editor-html').wysihtml5({
		toolbar: {
			"image": false,
			'html': false,
			'blockquote': false,
			'size': 'xs'
		}
	});

	// check in & check out date popup
	$('#checkIn,#checkOut').datetimepicker({
		datepicker: false,
		formatTime: 'g:i A',
		format: 'g:i A',
		step: 30
	});

	// copy terms on selection
	var copyItems = {
		'#copyTerms': '#pkgTerms',
		'#copyPreReq': '#pkgPrereq',
		'#copyFacts': '#pkgFacts'
	};

	$.each(copyItems, function (key, value) {
		$(key).change(function (event) {
			event.preventDefault();
			var val = $(key + ' option:selected').val();
			var editorObj = $(value).data('wysihtml5').editor;
			val = (val === -1) ? '' : val;
			if (editorObj) editorObj.setValue(val);
		});
	});

	var $changeProviderButton = $('.update-package-provider');

	// change provider button
	$changeProviderButton.click(function (event) {
		event.preventDefault();
		$("body").css("cursor", "progress");
		$('.update-package-provider').attr('disabled', true);
		$.ajax({
			url: $(this).data('url'),
			type: 'get',
			dataType: 'json',
			data: {packageId: $('#random').val()},
			success: function (data) {
				if (data.count > 0) {
					$("body").css("cursor", "default");
					$('.update-package-provider').attr('disabled', false);
					$.each(data.providers, function (count, provider) {
						var optionHtml = "<option class='packageProvider' value='" + provider.id + "'>" + provider.name + "</option>";
						$(optionHtml).appendTo('.package-planner');
					});
					$('#modalChangePackageProvider').modal('show');
				}
				else {
					alert('Some error occurred, please reload the page.')
				}
			},
			fail: function () {
				alert('Some error occurred while fetching providers, please try again after sometime.')
			}
		});
	});
});

// enabling submit button only if they accepts it
$('#btnProviderChangeSubmit').attr('disabled', true);
$('#termsCheckBox').change(function (event) {
	event.preventDefault();
	$('#btnProviderChangeSubmit').attr('disabled', true);
	if ($(this).is(':checked') === true) {
		$('#btnProviderChangeSubmit').attr('disabled', false);
	}
});