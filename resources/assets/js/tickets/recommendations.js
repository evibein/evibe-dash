function getQueryParams(a) {
	if (a == "") return {};
	var b = {};
	for (var i = 0; i < a.length; ++i) {
		var p = a[i].split('=');
		if (p.length != 2) continue;
		b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	}
	return b;
}

function formUrl(cUrl, key, value, isIgnore, ignores, customUrl) {
	cUrl = cUrl ? cUrl.substr(cUrl.indexOf('?') + 1).split('&') : window.location.search.substr(1).split('&');
	var cParams = getQueryParams(cUrl), queryString = "", loc = window.location;
	var url = loc.protocol + '//' + loc.host + loc.pathname;
	if (customUrl) {
		url = customUrl;
	}
	if (isIgnore) { // remove from current object
		if (cParams.hasOwnProperty(key)) {
			try {
				delete cParams[key];
			} catch (e) {
			}
		}
	} else cParams[key] = value;
	for (key in cParams) { // form string
		if (ignores && ignores.constructor === Array && window.indexOf.call(ignores, key) >= 0) continue;
		queryString += key + "=" + cParams[key] + "&";
	}
	if (queryString) {
		url += '?' + queryString;
		url = url.slice(0, -1); // remove trailing '&'
	}
	return url;
}

function addDeleteOptions() {
	$('.option-recommendation-btn').on("click", function () {
		$("body").css("cursor", "wait");
		var url = $(this).data("url");
		var serviceId = $(this).data("service-id");
		var editValue = $(this).data("value");
		var id = $(this).data("id");
		$.ajax({
			url: url,
			type: 'post',
			dataType: 'html',
			data: {id: id, serviceId: serviceId, editValue: editValue},
			success: function (data) {
				$("body").css("cursor", "default");
				if (editValue == 1) {
					$('.recommend-this-btn-' + id).addClass('hide');
					$('.recommended-btn-' + id).removeClass('hide');
					$('.status-corner-tag-' + id).removeClass('hide');
				} else {
					$('.recommend-this-btn-' + id).removeClass('hide');
					$('.recommended-btn-' + id).addClass('hide');
					$('.status-corner-tag-' + id).addClass('hide');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				showNotyError('Error occurred while fetching data, Please reload the page.')
			}
		});
	});
}

function removePreviewOption() {
	$('.option-remove-btn').on("click", function () {
		$("body").css("cursor", "wait");
		var url = $(this).data("url");
		var serviceId = $(this).data("service-id");
		var editValue = $(this).data("value");
		var id = $(this).data("id");
		if (confirm("Are you sure to remove this option ?")) {
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'html',
				data: {id: id, serviceId: serviceId, editValue: editValue},
				success: function (data) {
					$("body").css("cursor", "default");
					$('.remove-btn-' + id).addClass('hide');
				},
				error: function (jqXHR, textStatus, errorThrown) {
					showNotyError('Error occurred while fetching data, Please reload the page.')
				}
			});
		}
	});
}

function returnSortedFilters() {
	showLoading();

	var mainUrl = $('#mainQueryUrl').val();

	// getting the search query
	var query = $('#searchInput').val();
	var url = formUrl("", 'query', query, false, ['page'], mainUrl);

	// getting the price filter
	var minPrice = $('#priceMin').val();
	var maxPrice = $('#priceMax').val();
	url = formUrl(url, 'priceMin', minPrice, false, ['page'], mainUrl);
	url = formUrl(url, 'priceMax', maxPrice, false, ['page'], mainUrl);

	// getting the price sort selection
	var priceSort = $('.sort-option').attr('data-price');
	url = formUrl(url, 'priceSort', priceSort, false, ['page'], mainUrl);

	//getting area filter
	var areaFilters = "";
	$('.areas-filter :checkbox:checked').each(function () {
		areaFilters=areaFilters+$(this).val()+"_";
	});
	url = formUrl(url, 'area', areaFilters, false, ['page'], mainUrl);

	// getting category filters
	var categoryFilters = "";

	$('.categories-filter :checkbox:checked').each(function () {
		if ($(this).val().length != 0) {
			categoryFilters = categoryFilters + $(this).val() + ",";
		}
	});
	url = formUrl(url, 'category', categoryFilters, false, ['page'], mainUrl);
	return url;
}

function recoOptionsFilter() {
	$('.btn-reco-options-filter').on('click', function (event) {
		event.preventDefault();
		var url = returnSortedFilters();
		serviceOptionAjax(url);
	});
}

function sortByPrice() {
	$('.sort-option').on('click', function (event) {
		event.preventDefault();

		var url = returnSortedFilters();
		serviceOptionAjax(url);
	});
}

function sendRecommendations() {
	$('.btn-send-reco').on("click", function (e) {
		e.preventDefault();

		if (confirm("Are you sure to send the recommendation mail ?")) {
			showLoading();
			$.ajax({
				url: $(this).data("url"),
				type: 'post',
				dataType: 'json',
				success: function (data) {
					hideLoading();
					showNotySuccess("Recommendation mail send");
					$('.edit-followup-date-reco').val(data.followUp);
					var galleryUrl = $('.gallery-url').val();
					var whatsappText = "Hi " + data.name + ", here are the best recommendations for your party: " + data.recoUrl + " based on your preferences. Please check and submit your shortlist.";
					successMsg = "<div class='text-center'>" +
						"<a href='https://web.whatsapp.com/send?phone=" + data.callingCode + data.phone + "&text= " + whatsappText + "' target='_blank' class='btn btn-primary'>" +
						"<img src=" + galleryUrl + '/img/icons/whatsapp-logo.png' + " alt='whatsapp-icon' class='whatsapp-icon pad-r-5'>Send Recommendation via Whatsapp</a></div>";

					$(".selected-service-modal-whatsapp-section").append(successMsg);

					$(".edit-followup-date-reco").blur(function () {
						$(".edit-closure-date-reco").val($(this).val());
					});

					$("#modalAskRecoFollowupTime").modal("show");
				},
				error: function (jqXHR, textStatus, errorThrown) {
					showNotyError('Error occurred while fetching data, Please reload the page.')
				}
			});
		}
	})
}

function resetFilters() {
	$('.reset-filters').on("click", function (event) {
		event.preventDefault();

		var mainUrl = $('#mainQueryUrl').val();

		serviceOptionAjax(mainUrl);
	});
}

function serviceOptionAjax(url) {
	console.log(url);
	$.ajax({
		url: url,
		type: 'post',
		dataType: 'html',
		success: function (data) {
			hideLoading();
			$('.recommendations-wrap').empty();
			$('.recommendations-wrap').append(data);
			$('.recommendations-wrap').removeClass('hide');
			addDeleteOptions();
			recoOptionsFilter();
			sortByPrice();
			resetFilters();
			sendRecommendations();
			removePreviewOption();
		},
		error: function (jqXHR, textStatus, errorThrown) {
			showNotyError('Error occurred while fetching data, Please reload the page.')
		}
	});
}

(function initPlugins() {
	$('.edit-followup-date, .edit-closure-date').datetimepicker({
		step: 30,
		minDate: 0,
		scrollInput: false,
		scrollMonth: false,
		scrollTime: false,
		closeOnDateSelect: true
	});

	$('#modalAskRecoFollowupTime, #modalLoading').modal({
		show: false,
		keyboard: false,
		backdrop: 'static'
	});
})();

(function progressBarNavigation() {
	$('.selected-screen-progress-tags').on("click", function (e) {
		e.preventDefault();
		$('.selected-screen-progress-steps').removeClass('pink-btn');
		$('.selected-screen-progress-steps').removeClass('text-bold');
		$(this).addClass("text-bold pink-btn");
		$('.recommendations-wrap').addClass('hide');
		$('.recommendations-loading').addClass('hide');
		$('.selected-service-tag-wrap').removeClass('hide');
	});

	$('.selected-screen-progress').on("click", function (e) {
		e.preventDefault();
		$('.selected-screen-progress-steps').removeClass('pink-btn');
		$('.selected-screen-progress-steps').removeClass('text-bold');
		$(this).addClass("text-bold pink-btn");
		$('.selected-service-tag-wrap').addClass('hide');
		showLoading();
		$('.recommendations-wrap').addClass('hide');

		var url = $(this).data('url');
		serviceOptionAjax(url);
	})
})();

(function addFollowUpAfterReco() {
	$('.btn-save-reco-followup').on("click", function () {
		var ticketId = $('.btn-save-reco-followup').data("ticket-id");
		$("#modalAskRecoFollowupTime").modal("hide");
		showLoading();
		var url = '/tickets/followup/save/followup/' + ticketId;
		var date = $('.edit-followup-date').val();
		var comments = $('.followup-edit-comments').val();
		var followupType = $('#followupComment').val();
		var redirectUrl = $(this).data("redirect-url");

		if (url) {
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: {
					date: date,
					comment: comments,
					followupType: followupType,
					recommendationClosureDate: $(".edit-closure-date").val()
				},
				success: function (data) {
					if (data.success) {
						window.location.replace(redirectUrl)
					} else {
						$("#modalAskRecoFollowupTime").modal("show");
						hideLoading();
						showNotyError(data.error);
					}
				},
				fail: function () {
					$("#modalAskRecoFollowupTime").modal("show");
					hideLoading();
					window.showNotyError();
				}
			})
		} else {
			alert('Some error while adding the followup, Please try again after sometime');
		}
	});
})();