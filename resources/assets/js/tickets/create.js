function initCityAreaSelect() {
	var areaValue,
		$selectArea,
		$area = $('#area'),
		$city = $('#city');

	$city.selectize({
		onChange: onSelectCityChange
	});

	$selectArea = $area.selectize({
		valueField: 'id',
		labelField: 'name',
		searchField: ['id', 'name']
	});
	areaValue = $selectArea[0].selectize;
	if (!$area.val()) {
		areaValue.disable();
	}

	function onSelectCityChange(value, isManual) {
		var results = [];
		areaValue.disable();
		areaValue.clearOptions();
		if (!value.length || value == -1) {
			return false;
		}

		$('#allAreaOptions').find('li').each(function (count) {
			if ($(this).data('city_id') && $(this).data('city_id') == value) {
				results.push({
					id: $(this).data('id'),
					name: $(this).data('name')
				});
			}
		});

		areaValue.enable();
		areaValue.addOption(results);

		if (isManual !== true) {
			areaValue.refreshOptions();
		}
	}

	if ($city.val()) {
		onSelectCityChange($city.val(), true)
	}

	// select default area
	if ($area.data('default')) {
		areaValue.setValue($area.data('default'));
	}
}

function initTicketCreate() {
	var $comments = $('#comments');
	$('#eventDate').datetimepicker({
		step: 30,
		minDate: 0
	});

	$comments.wysihtml5({
		toolbar: {
			'image': false,
			'html': false,
			'blockquote': false,
			'size': 'xs'
		}
	});

	var editorObj = $comments.data('wysihtml5').editor; // set data
	if (editorObj && $comments.data('val')) editorObj.setValue($comments.data('val'));
}

$('.btn-add-loc').on('click', function () { // add new location
	$('#addNewAreaModal').data('redirectto', $(this).data('redirectto')).modal();
});

initCityAreaSelect();
initTicketCreate();