$(window).scroll(function () {
	if ($(this).scrollTop() > 150) {
		$('#topScroll').fadeIn();
	} else {
		$('#topScroll').fadeOut();
	}
});

// scroll body to 0px on click
$('#topScroll').click(function () {
	$('body,html').animate({
		scrollTop: 0
	}, 800);
	return false;
});

function initModals() {
	var allModals = [
		'modalViewTicketMapBookingInfo',
		'modalLoading',
		'modalMoreInfo',
		'modalEditTicket',
		'modalMarkAdvRcd',
		'modalCustomQuote1', // send custom quote - select
		'modalCustomQuote2', // send custom quote - edit
		'modalAskVendor', // send notification to a vendor
		'modalAskVenueAvl',//Ask venue avl list modal,
		'modalAskEnquiry', // get details of non venue availability
		'modalAskVenueAvlNext',//fill venue avl information modal
		'modalShowAvlInfo',
		'modalEditFollowup',
		'modalTicketFeedbackInfo', //modal for ticket review.
		'modalAddTicketFeedback', //modal add ticket feedback,
		'modalVendorFeedbackInfo', //modal for showing vendor review
		'modalAddVendorFeedback', // modal for adding or editing the vendor feedback,
		'modalSendBookingReceipt', // modal fo select partner to send receipt,
		'modalUploadTicketBookingImages', // modal for image upload,
		'modalEnquiryComment', // modal for saving the reply of availability check
		'modalReplyQuote', // reply text for custom quote from app.
		'modalAddCollectionOptions', // add collection option
		'modalAskCancellationReason', // cancellation reason,
		'modalSelectMappingMultipleQuote', // select multiple quote mapping
		'modalAskMultipleQuote' // sending multiple quotation
	];

	for (var key = 0; key < allModals.length; key++) {
		$('#' + allModals[key]).modal({
			show: false,
			keyboard: false,
			backdrop: 'static'
		});
	}
}

function initPlugins() {

	$('.tkt-date, .modal-tmap .tmap-party-date-time, .tmap-party-end-time,.edit-followup-date,#recommendationClosureDate,#updateClosureDate').datetimepicker({ // party date
		step: 30,
		minDate: 0,
		scrollInput: false,
		scrollMonth: false,
		scrollTime: false,
		closeOnDateSelect: true
	});

	$('.vCheckNext-party-date-1, .vCheckNext-party-date-2, .vCheckNext-party-date-3').datetimepicker({ // party date
		step: 30,
		minDate: 0,
		scrollInput: false,
		scrollMonth: false,
		scrollTime: false,
		closeOnDateSelect: true
	});

	$('.tu-date').datetimepicker({ // without min date
		step: 30,
		minDate: 0
	});

	var htmlEditors = [
		'.inp-spl-notes',
		'.editor-plugin',
		'.tmap-additional-info',
		'.tmap-facts',
		'.tmap-prereq',
		'.tmap-inclusions',
		'.tkt-comments',
		'#emailBody',
		'#emailSig',
		'#orderDetails',
		'#prerequisites',
		'#facts'
	]; // wysiwyg editor

	var len = htmlEditors.length;
	for (var i = 0; i < len; i++) {
		var el = htmlEditors[i];
		if ($(el).length) {
			$(el).wysihtml5({
				toolbar: {
					'image': false,
					'html': false,
					'blockquote': false,
					'size': 'xs'
				}
			});
			var editorObj = $(el).data('wysihtml5');
			if (editorObj && editorObj.editor) {
				editorObj = editorObj.editor; // set data
				if (editorObj && $(el).data('val')) {
					editorObj.setValue($(el).data('val'));
				}
			}
		}
	}
}

function checkForInvalidEmailAddress() {
	if ($('#hidTicketStatus').val() == $('#hidTicketStatus').data('invalid-email-status')) {
		var elementsToDisable = [
			'#showSendEmail',
			'#sendNoResponseAlert',
			'#sendCancellationMail',
			'.btn-send-reco',
			'#sendRecommendationEmailBtn',
			'#sendAutoBookingConfirm',
			'#sendAutoBookingCancel',
			'#resendAutoBookingConfirm',
			'#sendServiceBookedEmail',
			'#sendConfirmationEmail',
			'#sendUpdatedReceipt',
			'#sendReceipt'
		];

		$.each(elementsToDisable, function (key, value) {
			$(value).addClass("disabled");
			$(value).prop('disabled', true);
		});
	}
}

function checkForPartyOverTicket() {
	var $el = $('#isBookedTicket');
	if ($el.data('show')) {
		$(function () {
			jQuery.fn.extend({
				disable: function (state) {
					return this.each(function () {
						this.disabled = state;
					});
				}
			});
		});
		$('input[type="submit"], input[type="button"], button').disable(true);
		$(".btn").addClass("disabled");
		$('.auto-action-class').addClass('hide');
		$('#sendReceipt').removeClass('disabled');

		var nonDisableElements = [
			'#editTicketFeedbackInfo',
			'#showTicketFeedbackInfo',
			'#ticketFeedbackAccept',
			'.ticketFeedbackClose',
			'#addTicketFeedbackInfo',
			'#btnSaveTicketFeedback',
			'#modalGeneric .btn-success',
			'.show-vendor-feedback-info',
			'.vendorFeedbackClose',
			'#vendorReviewAcceptButton',
			'.add-vendor-feedback-info',
			'.edit-vendor-feedback-info',
			'#btnSaveVendorFeedback',
			'#topScroll',
			'.show-refund-amount',
			'.refund-modal-submit-btn',
			'.refund-modal-cancel-btn',
			'.btn-cancel-tkt-bkng',
			'.btn-resend-bkng-cancel-receipts',
			'.btn-booking-cancel-type',
			'.btn-booking-cancel-close',
			'.btn-cancel-booking',
			'.btn-cancel-booking-close',
			'.whatsapp-button'
		];

		$.each(nonDisableElements, function (key, value) {
			$(value).removeClass("disabled").disable(false);
		});

		$('#ticketUpdateBody button').removeClass("disabled").disable(false);
		$('#modalShowAvlInfo .btn-default').removeClass("disabled").disable(false);
	}
}

function loadPartyTime() {
	var $basePartyTime = $('#basePartyTime');

	$.ajax({
		url: '/tickets/' + $basePartyTime.data('id') + '/partyTimings',
		type: 'POST',
		dataType: 'json',
		data: {
			ticketId: $basePartyTime.data('id')
		}
	}).done(function (data, textStatus, jqXHR) {
		if (data.success === true && data.partyTime) {
			$basePartyTime.text(data.partyTime);
		}
	}).fail(function (jqXHR, textStatus, errorThrown) {
		window.showNotyError('Some error occurred in loading party date and time of ticket. Kindly refresh the page.');
	});
}

function makeSimplePostRequest(selector, successMsg, errorMsg) {
	// send confirmation email
	$(selector).click(function (event) {
		event.preventDefault();
		var confirmMessage = "Are you sure?";
		if (!confirm(confirmMessage)) {
			return false;
		}

		var url = $(this).data('url');
		window.showLoading(); // show loading modal

		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json'
		}).done(function (data, textStatus, jqXHR) {
			window.hideLoading();
			if (data.success) {
				window.showGenericModal("Success", successMsg);
			}
			else {
				var errorMes = '';
				if (data.error) {
					errorMes = data.error;
				}
				else {
					errorMes = errorMsg;
				}

				window.showNotyError(errorMes);
			}
		}).fail(function (jqXHR, textStatus, errorThrown) {
			window.hideLoading();
			window.showNotyError();
		});

		return false;
	});
}

function checkForCancelTicket() {
	var $el = $('#idCancelledMask');
	if ($el.data('show')) {
		$(function () {
			jQuery.fn.extend({
				disable: function (state) {
					return this.each(function () {
						this.disabled = state;
					});
				}
			});
		});

		$('input[type="submit"], input[type="button"], button').disable(true);
		$(".btn").addClass("disabled");
		$(":checkbox").bootstrapSwitch('toggleDisabled', true, true);
		$('#reOpenTicketBtn').removeClass("disabled");
		$('#modalShowAvlInfo .btn-default').removeClass("disabled").disable(false);
		$('#ticketUpdateBody button').removeClass("disabled").disable(false);
		if ($el.data('auto-cancel')) {
			$('#sendAutoBookingCancel').removeClass("disabled");
			$('#modalGeneric').find('button').removeClass("disabled").disable(false);
		}
		$('#topScroll').removeClass('disabled');
		$('.sub-heading li>a').removeClass('disabled');
	}
}

//@todo :: Should be removed
// Open update form
function openUpdate() {
	var update = window.location.href;
	var fromEmail = update.substring(update.indexOf("?") + 1);
	if (fromEmail == "ref=email") {
		$('#ticketUpdateAction').click();
	}
}

function openFollowup() {
	var followup = window.location.href;
	var openFollowup = followup.substring(followup.indexOf("?") + 1);
	if (openFollowup == "tf=open") {
		$('#ticketFollowupAction').click();
	}
}

// function for showing the checkout field based on ticket booking type id
function showCheckoutField(typeId) {
	$('.checkout-field-crm').each(function (key, value) {
		var $type = $(this).data('type');
		if ($type == typeId) {
			$(this).removeClass('hide');
		}
		else {
			$(this).addClass('hide');
		}
	});
}

// find Error
function openModalImageUpload() {
	var str = window.location.hash.substr(0);
	var fromEmail = str.substring(str.indexOf("?") + 1);
	var id = str.substring(str.lastIndexOf("#") + 1, str.lastIndexOf("?"));

	if (fromEmail == "error=true") {
		uploadImageAfterError(1, id);
		uploadImageAfterError(2, id);
	}
}

// change booking type details on change of type ticket booking
$('.type-ticket-booking').on('change', function (event) {
	event.preventDefault();
	var val = $(this).val();
	var bookingTypeDetails = $('.type-ticket-booking > option[value=' + val + ']').data('val');
	$('.booking-type-details').val(bookingTypeDetails);

	// showing the crm checkout field
	showCheckoutField(val)
});

//change lead status
$('#ticketLeadStatus').change(function (event) {
	event.preventDefault();

	var data = {leadStatus: $('#ticketLeadStatus').val()};
	var ticketId = parseInt($("#ticketId").val(), 10);
	var url = '/tickets/' + ticketId + '/lead-status';

	window.performEditLeadStatus({
		'el': $(this),
		'url': url,
		'data': data,
		'wrapClass': ".tkt-lead-status-wrap",
		'allClasses': "ticket-header-info-status-hot ticket-header-info-status-medium ticket-header-info-status-cold",
		'classPrefix': "ticket-header-info-status-"
	});
});

function getEmailTrackData() {
	var ticketId = $('#ticketId').val();
	if ($('#noResponseEmailStatus').length || $('#receiptEmailData').length || $('#sendOrderProcessEmail').length) {
		$.ajax({
			url: "/track-email",
			type: 'GET',
			dataType: 'json',
			data: {ticketId: ticketId},
			success: function (data) {
				if (data.success) {
					$.getScript();
					var noResponse = "";
					var receipt = "";
					var orderProcess = "";
					$.each(data.emailSentData, function (i, item) {
						var emailStatus = item.opens > 0 ? "read" : "did not read";
						var className = item.opens > 0 ? "text-success" : "text-danger";
						var image = item.opens > 0 ? "whatsapp_read.png" : "whatsapp_delivered.png";
						if (item.subject.includes("your party order for")) {
							receipt += '<div class="mar-t-10"><img src="https://gallery.evibe.in/img/icons/' + image + '" width="30">Email has been successfully sent to the customer. Customer <a class="' + className + '">' + emailStatus + '</a> the email</div><br>';
						}

						if (item.subject.includes("regarding your enquiry for party")) {
							noResponse += '<div class="mar-t-10"><img src="https://gallery.evibe.in/img/icons/' + image + '" width="30">No Response Email has been successfully sent to the customer. Customer <a class="' + className + '">' + emailStatus + '</a> the email</div><br>';
						}

						if (item.subject.includes("advance payment link & order details")) {
							orderProcess += '<div class="mar-t-10"><img src="https://gallery.evibe.in/img/icons/' + image + '" width="30">Order Process Email has been successfully sent to the customer. Customer <span class="' + className + '">' + emailStatus + '</span> the email</div><br>';
						}
					});
					$('#noResponseEmailStatus').append(noResponse);
					$('#receiptEmailData').append(receipt);
					$('#sendOrderProcessEmail').append(orderProcess);
				}
				else {
					showNotyError("Some Error occurred, please refresh the page");
				}
			},
			error: function () {
				showNotyError("Some Error occurred, please refresh the page");
			}
		});
	}
}

// initialize
initPlugins();
initModals();
loadPartyTime();
checkForCancelTicket();
checkForPartyOverTicket();
getEmailTrackData()

openUpdate();
openFollowup();
openModalImageUpload();
checkForInvalidEmailAddress();