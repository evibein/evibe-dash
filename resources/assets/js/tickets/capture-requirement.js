(function initFunctions() {
	$('#flexEventDate').datetimepicker({
		step: 30,
		minDate: 0
	});

	$('#eventDate').datetimepicker({
		step: 30,
		minDate: 0,
		timepicker: false,
		scrollInput: false,
		scrollMonth: false,
		closeOnDateSelect: true,
		format: 'm/d/Y'
	});

	$('#eventSlot').datetimepicker({
		datepicker:false,
		timepicker: true,
		step: 30,
		closeOnTimeSelect: true,
		format: 'H:i'
	});

	$('.event-date').datetimepicker({
		step: 30,
		minDate: 0,
		timepicker: true,
		scrollInput: false,
		scrollMonth: false,
		closeOnDateSelect: false,
		format: 'd/m/Y H:i'
	});

	$.validate({
		lang: 'es',
		modules: 'toggleDisabled',
		disabledFormFilter: 'form.new-ticket-create-form',
		showErrorDialogs: false
	});

	showSpecificVenues();
	showAdditionalInformation();

	$('form#newTicketCreateForm').find('#occasion').on('change', function () {
		$('.create-ticket-venue-type').addClass('hide');
		$('.additional-fields-input').addClass('hide');

		showSpecificVenues();
		showAdditionalInformation();
	});

	$('.email-typo-error').on('blur', function () {
		$(this).mailcheck({
			suggested: function (element, suggestion) {
				var errorMsg = "<span class='email-typo-error-msg'>Did you mean " + suggestion.address + "@" + suggestion.domain + "</span>? (Click here to update)";
				new noty({
					text: errorMsg,
					layout: "top",
					type: "warning",
					closeWith: ["click"],
					timeout: 8000
				});

				$(".email-typo-error-msg").closest(".noty_text").closest(".noty_message").on("click", function () {
					$('.email-typo-error').val(suggestion.address + "@" + suggestion.domain);
				});
			}
		});
	});

	$("#nextFollowUp").blur(function () {
		$("#closureDate").val($(this).val());
	});

	function showSpecificVenues() {
		// show type venue only for kids birthday & surprises
		if ($('form#newTicketCreateForm').find('#occasion :selected').val() == 1) {
			$(".create-ticket-venue-type-option").each(function () {
				$(this).removeClass("hide");
				if ($(this).val() == 32) {
					$(this).addClass("hide");
				}
			});

			$('.create-ticket-venue-type').removeClass('hide');
		} else if ($('form#newTicketCreateForm').find('#occasion :selected').val() == 16) {
			$(".create-ticket-venue-type-option").each(function () {
				$(this).addClass("hide");
				if ($(this).val() == 32 || $(this).val() == 23) {
					$(this).removeClass("hide");
				}
			});

			$('.create-ticket-venue-type').removeClass('hide');
		}
	}

	function showAdditionalInformation() {
		if ($('form#newTicketCreateForm').find('#occasion :selected').val() == 1) {
			$('.additional-fields-input').removeClass('hide');
		}

		$('#childEvents').on('change',function ()
		{
			let eventValue = $('#childEvents').val();
			let condition = (eventValue != $('#hidBabyShowerEventId').val() && eventValue != $('#hidSareeCeremonyEventId').val() && eventValue != $('#hidDhotiCeremonyEventId').val());
			condition && $('.additional-fields-input-gender').removeClass('hide');
			!condition && $('.additional-fields-input-gender').addClass('hide');
		});
	}

})();

(function newFormCreate() {
	$('.btn-new-ticket-create').on('click', function (e) {
		e.preventDefault();
		// lang to change language of errors, conf will push all the validations to errors
		var errors = [], lang = '', conf = {
			onElementValidate: function (valid, $el, $form, errorMess) {
				if (!valid) {
					// gather up the failed validations
					errors.push({el: $el, error: errorMess});
				}
			}
		};

		// If we change the third argument to false errors will not be shown
		if (!$('#newTicketCreateForm').isValid(lang, conf, true)) {
		} else {
			if ($(this).data("is-close") == 1) {
				$('input[name=isClose]').val('1');
			} else {
				$('input[name=isClose]').val('0');
			}
			$('form#newTicketCreateForm').submit();
		}
	});
})();

(function submitAdditionalFields() {
	$('.btn-submit-additional-fields').on('click', function (e) {
		e.preventDefault();
		// lang to change language of errors, conf will push all the validations to errors
		var errors = [], lang = '', conf = {
			onElementValidate: function (valid, $el, $form, errorMess) {
				if (!valid) {
					// gather up the failed validations
					errors.push({el: $el, error: errorMess});
				}
			}
		};

		// If we change the third argument to false errors will not be shown
		if (!$('#submitAdditionalFieldsForm').isValid(lang, conf, true)) {
		} else {
			if ($(this).data("is-close") == 1) {
				$('input[name=isClose]').val('1');
			} else {
				$('input[name=isClose]').val('0');
			}

			var url = $('#submitAdditionalFieldsForm').attr("action");

			$.post(url, {
				childEventId: $('#childEvents :selected').val(),
				kidsBirthdayGender: $('#kidsBirthdayGender :selected').val(),
				teamNotes: $('#teamNotes').val(),
				isClose: $('#isClose').val()
			}).done(function (data) {
				window.location.href = data.redirectUrl;
			}).fail(function (data) {
				if (data.error) {
					alert(data.error);
				} else {
					alert("An unknown error has occurred, please try again later.");
				}
			});

		}
	});
})();

(function changeTypeTicketBooking() {
	$('#typeTicketBookingId').on('change', function (e) {
		e.preventDefault();
		var url = $(this).data("url");
		var typeTicketBookingId = $(this).val();
		var ticketEventId = $('#ticketEventId').val();
		if (typeTicketBookingId == 0) {
			showNotyError("Please select a valid type ticket booking");
			$('.ticket-booking-checkout-fields').empty();
		} else {
			$.ajax({
				url: url + "?typeTicketBooking=" + typeTicketBookingId + "&ticketEventId=" + ticketEventId,
				dataType: 'html',
				type: 'POST',
				success: function (data) {
					$('.ticket-booking-checkout-fields').empty();
					$('.ticket-booking-checkout-fields').append(data);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					showNotyError('Error occurred while fetching data, Please reload the page.')
				}
			});
		}
	})
})();

(function selectedServicesContinue() {
	$('.btn-selected-service-continue').on('click', function () {
		var i = 0;
		var arr = [];

		$('.ticket-selected-categories:checked').each(function () {
			arr[i] = $(this).val();
			i++;
		});

		var minBudget = $('#minBudget').val();
		var maxBudget = $('#maxBudget').val();

		if (arr.length == 0) {
			showNotyError("Please select an option to continue.");
		} else if (parseInt(minBudget) >= parseInt(maxBudget)) {
			showNotyError("Min Budget should be less than Max budget");
		} else {
			$('.ticket-selected-category').find('h4').each(function () {
				if ($.inArray($(this).text(), arr) == -1) {
					$(this).next().remove();
					$(this).remove();
				}
			});

			/*
			$('.ticket-workflow-show-selected-categories').addClass('hide');
			$('.create-package-content-div').removeClass('hide');
			*/
			$('.btn-add-services').click();
		}
	})
})();

(function submitServices() {
	$('.btn-add-services').on('click', function (e) {
		e.preventDefault();
		// lang to change language of errors, conf will push all the validations to errors
		var errors = [], lang = '', conf = {
			onElementValidate: function (valid, $el, $form, errorMess) {
				if (!valid) {
					// gather up the failed validations
					errors.push({el: $el, error: errorMess});
				}
			}
		};
		var typeTicketBookingId = $('#typeTicketBookingId').val();
		var serviceTags = [];
		var checkoutInputs = [];
		var comments = $("#comments").val();
		var teamNotes = $("#teamNotes").val();
		var redirectUrl = $('#redirectUrl').val();
		var minBudget = $('#minBudget').val();
		var maxBudget = $('#maxBudget').val();

		$('.sub-tags-select:checked').each(function () {
			serviceTags.push($(this).attr("name"));
		});

		$('.ticket-booking-checkout-fields:input').each(function () {
			checkoutInputs.push({
				id: $(this).attr("id"),
				value: $(this).val()
			});
		});

		if (typeTicketBookingId == 0) {
			showNotyError("Please select a valid categories");
		} else if (serviceTags.length == 0) {
			showNotyError("Please select at least one category");
		} else {
			var isClose = 0;
			if ($(this).data("is-close") == 1) {
				isClose = 1;
			}
			url = $(this).data("url");
			$.post(url, {
				serviceTags: serviceTags,
				checkoutInputs: checkoutInputs,
				comments: comments,
				teamNotes: teamNotes,
				isClose: isClose,
				redirectUrl: redirectUrl,
				maxBudget: maxBudget,
				minBudget: minBudget

			}).done(function (data) {
				if (data.success) {
					window.location.href = data.redirectUrl;
				}
				if (data.error) {
					var errorMsg = data.error;
					showNotyError(errorMsg);
				}
			}).fail(function (data) {
				if (data.error) {
					var errorMsg = data.error;
				} else {
					errorMsg = 'An unknown error has occurred, please try again later.';
				}
				if (!showNotyError(errorMsg)) window.location = redirect;
			});
		}

		// If we change the third argument to false errors will not be shown
		// if (!$('#newTicketCreateForm').isValid(lang, conf, true)) {
		// } else {
		// 	$('form#newTicketCreateForm').submit();
		// }
	});
})();

(function submitContactInfo() {
	$('.btn-submit-contact-info').on('click', function (e) {
		e.preventDefault();
		// lang to change language of errors, conf will push all the validations to errors
		var errors = [], lang = '', conf = {
			onElementValidate: function (valid, $el, $form, errorMess) {
				if (!valid) {
					// gather up the failed validations
					errors.push({el: $el, error: errorMess});
				}
			}
		};

		// If we change the third argument to false errors will not be shown
		if (!$('#saveContactDetailsForm').isValid(lang, conf, true)) {
		} else {
			if ($(this).data("is-close") == 1) {
				$('input[name=isClose]').val('1');
			} else {
				$('input[name=isClose]').val('0');
			}
			$('form#saveContactDetailsForm').submit();
		}
	});
})();

(function submitStatusInfo() {
	var ticketSource = $('#ticketSource');
	ticketSource.selectize();
	var isValid = 1;
	$('.btn-submit-status-info').on('click', function (e) {
		e.preventDefault();
		if (!ticketSource.val()) {
			showNotyError("Please Select Ticket Source");
			var isValid = 0;
		}
		var errors = [], lang = '', conf = {
			onElementValidate: function (valid, $el, $form, errorMess) {
				if (!valid) {
					// gather up the failed validations
					errors.push({el: $el, error: errorMess});
				}
			}
		};

		// If we change the third argument to false errors will not be shown
		if (!$('#saveStatusDetailsForm').isValid(lang, conf, true) || isValid == 0) {
		} else {
			if ($(this).data("is-close") == 1) {
				$('input[name=isClose]').val('1');
			} else {
				$('input[name=isClose]').val('0');
			}
			$('form#saveStatusDetailsForm').submit();
		}

	});
	$('#decisionMaker').on('change', function () {
		if ($('#decisionMaker').val() > 1) {
			$('.decision-maker-contact').removeClass('hide');
		} else {
			$('.decision-maker-contact').addClass('hide');
		}
	});
	if ($('#decisionMaker').val() > 1) {
		$('.decision-maker-contact').removeClass('hide');
	} else {
		$('.decision-maker-contact').addClass('hide');
	}
})();

(function changeCity() {
	var areaValue,
		$selectArea,
		$area = $('#area'),
		$city = $('#city');

	$city.selectize({
		onChange: onSelectCityChange
	});

	$selectArea = $area.selectize({
		valueField: 'id',
		labelField: 'name',
		searchField: ['id', 'name'],
		render: {
			option: function (data, escape) {
				return "<div data-pin-code='" + data.pin + "'>" + data.name + "</div>"
			}
		},
		onChange: function (val) {
			var data = this.options[val];
			if ((typeof (data) !== "undefined")) {
				if ((typeof (data.pin) !== "undefined") && data.pin) {
					$('#zip').val(data.pin);
				}
			}
		}
	});

	areaValue = $selectArea[0].selectize;

	if (!$area.val()) {
		areaValue.disable();
	}

	if ($city.val()) {
		onSelectCityChange($city.val(), true)
	}

	function onSelectCityChange(value, isManual) {
		var results = [];
		areaValue.disable();
		areaValue.clearOptions();
		if (!value.length || value == -1) {
			return false;
		}

		$('#allAreaOptions').find('li').each(function (count) {
			if ($(this).data('city_id') && $(this).data('city_id') == value) {
				results.push({
					id: $(this).data('id'),
					name: $(this).data('name'),
					pin: $(this).data('pin')
				});
			}
		});

		areaValue.enable();
		areaValue.addOption(results);

		if (isManual !== true) {
			areaValue.refreshOptions();
		}
	}

	// select default area
	if ($area.data('default')) {
		areaValue.setValue($area.data('default'));
	}
})();
