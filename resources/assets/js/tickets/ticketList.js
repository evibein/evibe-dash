$(document).ready(function () {

	(function gettingData() {

		$('.ticket-list-details').css('height', $(window).height() - 126);

		var urlHot = $('.load-hot-ticket-data').data("url") + "?" + window.location.href.slice(window.location.href.indexOf('?') + 1);
		$.ajax({
			url: urlHot,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				$('.load-hot-ticket-data-body-wrap').addClass('hide');
				$('.load-hot-ticket-data-body').append(data);
				var $id = $('.status_id_hot').val();
				loadExtraData("#ticketInfo" + $id, ".tickets-wrap-" + $id);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				showNotyError('Error occurred while fetching data, Please reload the page.')
			}
		});

		var urlMedium = $('.load-medium-ticket-data').data("url") + "?" + window.location.href.slice(window.location.href.indexOf('?') + 1);
		$.ajax({
			url: urlMedium,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				$('.load-medium-ticket-data-body-wrap').addClass('hide');
				$('.load-medium-ticket-data-body').append(data);
				var $id = $('.status_id_medium').val();
				loadExtraData("#ticketInfo" + $id, ".tickets-wrap-" + $id);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				showNotyError('Error occurred while fetching data, Please reload the page.')
			}
		});

		var urlCold = $('.load-cold-ticket-data').data("url") + "?" + window.location.href.slice(window.location.href.indexOf('?') + 1);
		$.ajax({
			url: urlCold,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				$('.load-cold-ticket-data-body-wrap').addClass('hide');
				$('.load-cold-ticket-data-body').append(data);
				var $id = $('.status_id_cold').val();
				loadExtraData("#ticketInfo" + $id, ".tickets-wrap-" + $id);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				showNotyError('Error occurred while fetching data, Please reload the page.')
			}
		});

		var urlMapped = $('.load-unmapped-ticket-data').data("url") + "?" + window.location.href.slice(window.location.href.indexOf('?') + 1);
		$.ajax({
			url: urlMapped,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				$('.load-unmapped-ticket-data-body-wrap').addClass('hide');
				$('.load-unmapped-ticket-data-body').append(data);
				var $id = $('.status_id_unmapped').val();
				loadExtraData("#ticketInfo" + $id, ".tickets-wrap-" + $id);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				showNotyError('Error occurred while fetching data, Please reload the page.')
			}
		});

		$('.tickets-list-new').css('height', $(window).height() - 191);
	})();

	function loadExtraData($ticketInfoId, $ticketWrap) {
		var ticketInfo = $($ticketInfoId);
		var ticketIds = ticketInfo.data('ids');
		var url = ticketInfo.data('url');

		if (ticketIds.length && url) {
			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'JSON',
				data: {ticketIds: ticketIds},
				success: function (data) {
					if (data.success) {
						$($ticketWrap).each(function (key, val) {
							var tId = $(this).data('id');
							var ticketData = data.extraData[tId];

							if (ticketData.mappings) {
								$("." + "ticket-bottom-wrap-" + "").addClass('hide');
								var markup = "";
								$.each(ticketData.mappings, function (key, value) {
									markup = markup + "<li class='in-blk mar-r-4 font-10'><a href='" + value.link + "'>" + value.code + "</a></li>";
									$(this).find('.mappings-list')
								});

								$(this).find('.mappings-list').append(markup);
							}

							if (ticketData.status) {
								var firstUpdateMarkup = "Status updated to <b> " + ticketData.statusName + "</b> by <u>" + ticketData.handler + "</u> on <b>" + ticketData.statusAt +
									"</b><span>, happened through <b>" + ticketData.source + "</b></span>";
								if (ticketData.comment) {
									firstUpdateMarkup += "with <b>comments:</b> " + ticketData.comment;
								}

								$('.loading-text').addClass('hide');
								$(this).find('.ticket-bottom-wrap').append(firstUpdateMarkup);
							}
							else {
								$(this).find('.ticket-bottom-wrap').addClass('hide');
							}
						});
						$($ticketWrap).on('click', '.ticket-update-mappings-info', function () {
							window.showLoading();
							$('#modalShowTicketUpdate').modal('hide');
							var url = $(this).data('url');
							var handler = $(this).data('handler');

							$.ajax({
								url: url,
								type: 'POST',
								dataType: 'json',
								success: function (data) {
									window.hideLoading();
									if (data.success) {
										var $modal = $('#modalShowTicketUpdate');
										$modal.find('.ticket-update-body').empty();
										$modal.find('.handler').text(handler);

										$.each(data.ticketUpdates, function (key, value) {
											var markup = '<div class="status-updates-wrap pad-t-8">[' + value['date'] + '] ==> Status updated to <b>' + value['status'] + '</b> by ' +
												'<u>' + value['handler'] + '</u>. Update happened through ' + value['source'] + ' source ';
											if (value['comments']) {
												markup += 'with <b>comments: </b>' + value['comments'];
											}
											markup += '</div>';

											$modal.find('.ticket-update-body').append(markup);
										});

										$modal.modal('show');
									}
								},
								fail: function () {
									window.showNotyError();
								}
							})
						});
					}
					else {
						window.showNotyError("Some error occurred while loading the extra data of tickets");
					}
				},
				fail: function () {
					window.showNotyError("Some error occurred while loading the extra data of tickets");
				}
			});
		}
	}

	function initPlugins() {
		$('.filter-pd-start, .filter-pd-end, .filter-cd-start, .filter-cd-end, .filter-ud-start , .filter-ud-end').datetimepicker({ // party date
			timepicker: false,
			scrollInput: false,
			scrollMonth: false,
			closeOnDateSelect: true,
			format: "d-m-Y"
		});
		$('label:has(input[disabled])').addClass('text-strike'); // strike through disabled filters
	}

	function getQueryParams(a) {
		if (a == "") return {};
		var b = {};
		for (var i = 0; i < a.length; ++i) {
			var p = a[i].split('=');
			if (p.length != 2) continue;
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	}

	function formUrl(cUrl, key, value, isIgnore, ignores) {
		cUrl = cUrl ? cUrl.substr(cUrl.indexOf('?') + 1).split('&') : window.location.search.substr(1).split('&');
		var cParams = getQueryParams(cUrl), queryString = "", loc = window.location;
		var url = loc.protocol + '//' + loc.host + loc.pathname;
		if (isIgnore) { // remove from current object
			if (cParams.hasOwnProperty(key)) {
				try {
					delete cParams[key];
				}
				catch (e) {
				}
			}
		} else cParams[key] = value;
		for (key in cParams) { // form string
			if (ignores && ignores.constructor === Array && window.indexOf.call(ignores, key) >= 0) continue;
			queryString += key + "=" + cParams[key] + "&";
		}
		if (queryString) {
			url += '?' + queryString;
			url = url.slice(0, -1); // remove trailing '&'
		}
		return url;
	}

	function refreshUrl(url) {
		window.location.href = url;
	}

	// show notification, if ticket added successfully
	if (window.location.search.indexOf('success=1') > -1) {
		noty({
			text: 'New ticket has been created',
			layout: 'top',
			type: 'success',
			closeWith: ['click', 'hover'],
			timeout: 5000,
			callback: {
				onClose: function () {
					var loc = window.location.href;
					var qIndex = loc.indexOf('?');
					loc = loc.substring(0, qIndex);
					window.location = loc;
				}
			}
		});
		$('#query').focus();
	}

	// Delete a ticket
	$('.btn-ticket-del').click(function (event) {
		event.preventDefault();
		var isConfirm = confirm('Are you sure to delete this ticket?');
		if (!isConfirm) return false;
		var ticketId = parseInt($(this).data('id'), 10);
		$.ajax({
			url: 'tickets/delete/' + ticketId,
			type: 'POST',
			dataType: 'json',
			success: function (data, textStatus, jqXHR) {
				if (data.success) location.reload();
			},
			error: function (data, textStatus, jqXHR) {
				window.showNotyError();
			}
		});
	});

	$('#searchForm').submit(function (event) { // search results
		event.preventDefault();
		var searchQuery = $('#searchQuery').val();
		refreshUrl(formUrl('', 'query', searchQuery, false, ['page']));
	});

	$('#resetSearch').click(function (event) { // reset search
		event.preventDefault();
		refreshUrl(formUrl('', 'query', '', true, ['page']));
	});

	$('#sortOptions').change(function (event) { // sort results
		var sortVal = $('#sortOptions').val();
		refreshUrl(formUrl('', 'sort', sortVal, false, ['page']));
	});

	$('#perPage').change(function (event) { // results per page
		var perPage = $('#perPage').val();
		refreshUrl(formUrl('', 'count', perPage, false, ['page']));
	});

	$('.filter-option input').change(function (event) { // apply filters
		var type = $(this).prop('name');
		var filterVal = $('input[name="' + type + '"]:checked').val();
		refreshUrl(formUrl('', type, filterVal, false, ['page']));
	});

	$('.filter-option input:checked').click(function () { // uncheck any checked filter
		$(this).removeAttr('checked');
		var type = $(this).prop('name');
		var filter = [type, 'page'];
		refreshUrl(formUrl('', 'status', '', true));
	});

	$('.btn-filter-by-pd').click(function (event) { // filter by party date
		event.preventDefault();
		var startDate = $('.filter-pd-start').val();
		var endDate = $('.filter-pd-end').val();
		if (!startDate && !endDate) return false;
		var url = '';
		if (startDate) url = formUrl(url, 'pds', startDate, false, ['page']);
		if (endDate) url = formUrl(url, 'pde', endDate, false, ['page']);
		refreshUrl(url);
	});

	$('.btn-filter-by-cd').click(function (event) { // filter by party date
		event.preventDefault();
		var startDate = $('.filter-cd-start').val();
		var endDate = $('.filter-cd-end').val();
		if (!startDate && !endDate) return false;
		var url = '';
		if (startDate) url = formUrl(url, 'cds', startDate, false, ['page']);
		if (endDate) url = formUrl(url, 'cde', endDate, false, ['page']);
		refreshUrl(url);
	});

	$('.btn-filter-by-ud').click(function (event) { // filter by party date
		event.preventDefault();
		var startDate = $('.filter-ud-start').val();
		var endDate = $('.filter-ud-end').val();
		if (!startDate && !endDate) return false;
		var url = '';
		if (startDate) url = formUrl(url, 'uds', startDate, false, ['page']);
		if (endDate) url = formUrl(url, 'ude', endDate, false, ['page']);
		refreshUrl(url);
	});

	(function loadFilters() {
		//show ticket update info modal
		$('#modalShowTicketUpdate').modal({
			show: false,
			keyboard: false,
			backdrop: 'static'
		});

		// filters - sort
		$('.ticket-list-status-filter').on("change", function (event) {
			var statusFilter = $('.ticket-list-status-filter').val();
			refreshUrl(formUrl('', 'status', statusFilter, false, ['page']));
		});

		// reset all filters
		$('#resetAllFilters').on("click", function (event) {
			event.preventDefault();
			refreshUrl(formUrl('', '', '', true, ['status', 'leadStatus', 'page', 'pds', 'pde', 'cds', 'cde', 'uds', 'ude', 'query']));
		});
	})();

	// code for getting the extra data
	// $(window).load(function () {
	// 	$.each("", function (count, url) {
	// 		$(this).loadExtraData($ticketInfoId);
	// 	});
	// });

	initPlugins();
});
