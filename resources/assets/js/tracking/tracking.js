$('.btn-order-track').click(function (event) {
	event.preventDefault();
	$("body").css("cursor", "wait");
	$('.btn-order-track').attr('disabled', true);
	var data = {};
	$('.order-track-input').html('');

	$.ajax({
		url: $(this).data('url'),
		type: 'get',
		dataType: 'json',
		data: data,
		success: function (data) {
			if (data.success) {
				$(".order-track-modal-header").text(data.title);
				$("#orderBookingId").val(data.bookingId);
				if (data.countQuestions > 0) {
					$.each(data.questions, function (count, question) {
						var optionHtml = "<div class='font-16 pad-t-10 pad-b-4'><b>" + question.question + "" +
							"</b><br><input type='radio' name='" + question.id + "' id='" + question.id + "' value='1'><label for='" + question.id + "' class='pad-l-3 pad-r-10'> Yes</label> " +
							"<input type='radio' name='" + question.id + "' id='" + question.id + ".2' value='2' checked><label for='" + question.id + ".2' class='pad-l-3'> No</label></div>";
						$(optionHtml).appendTo('.order-track-input');
					});
				}
				$('#modalOrderTracking').modal('show');
				$("body").css("cursor", "default");
				$('.btn-order-track').attr('disabled', false);
			}
			else {
				$("body").css("cursor", "default");
				$('.btn-order-track').attr('disabled', false);
				window.showNotyError('Some error occurred(booking Id not found), please try again.')
			}
		},
		fail: function () {
			$("body").css("cursor", "default");
			window.showNotyError('Some error occurred while fetching data, please try again after sometime.')
		}
	});
});

(function showModelOnClick() {
	$('.show-partner-track').click(function (event) {
		event.preventDefault();
		$('.partner-tracking-questions').removeClass('hide');
		$('.customer-tracking-questions').addClass('hide');
		$('.add_required_customer').prop('required', false);
		$('.add_required_partner').prop('required', true);
		$('.partner_delivery_person_details').prop('required', true);
		$('.partner_delivery_time_details').prop('required', true);
		$('.isCustomer').val(2);
		var formName = "#modalOrderTracking" + $(this).data("id");
		$(formName).modal('show');
	});

	$('.show-customer-track').click(function (event) {
		event.preventDefault();
		$('.partner-tracking-questions').addClass('hide');
		$('.customer-tracking-questions').removeClass('hide');
		$('.add_required_customer').prop('required', true);
		$('.add_required_partner').prop('required', false);
		$('.partnerDeliveryPersonDetails').prop('required', false);
		$('.isCustomer').val(1);
		var formName = "#modalOrderTracking" + $(this).data("id");
		$(formName).modal('show');
	})
})();

(function initPlugins() {
	$('.filter-pd-start').datetimepicker({
		format: 'Y/m/d',
		onShow: function (ct) {
			this.setOptions({
				maxDate: $('.filter-pd-end').val() ? $('.filter-pd-end').val() : false
			})
		},
		timepicker: false,
		scrollInput: false,
		scrollMonth: false,
		closeOnDateSelect: true
	});

	$('.filter-pd-end').datetimepicker({
		format: 'Y/m/d',
		onShow: function (ct) {
			this.setOptions({
				minDate: $('.filter-pd-start').val() ? $('.filter-pd-start').val() : false
			})
		},
		timepicker: false,
		scrollInput: false,
		scrollMonth: false,
		closeOnDateSelect: true
	});

	$('label:has(input[disabled])').addClass('text-strike'); // strike through disabled filters

	$('#selectProvider').selectize();
})();

(function orderTrackFilters() {
	function updateUrlByFilters(event) {
		event.preventDefault();

		var startDate = $('.filter-pd-start').val();
		var endDate = $('.filter-pd-end').val();
		var query = $('#searchQuery').val();
		var provider = $('#selectProvider').val();
		var city = $('#city').val();
		var url = "current?";
		var isUrlChanged = false;

		if (city) {
			isUrlChanged = true;
			url += '&city=' + city;
		}

		if (query) {
			isUrlChanged = true;
			url += '&query=' + query;
		}

		if (startDate || endDate) {
			isUrlChanged = true;
			url += '&start_date=' + startDate + '&end_date=' + endDate;
		}

		if (provider) {
			url += '&p_id=' + provider;
		}

		if (isUrlChanged) {
			location.href = url;
		}
	}

	$('#searchQuery').keyup(function (event) {
		if (event.which == 13 && $(this).val()) { // on enter submit search
			updateUrlByFilters(event);
		}
	});

	// filter options
	$('#city').change(updateUrlByFilters);
	$('#btnFilter').click(updateUrlByFilters);
})();

(function escalateButton() {
	$('.order-tracking-unavailable-button').click(function (event) {
		event.preventDefault();
		var formName = "#partnerUnavailable" + $(this).data("id");
		$(formName).modal('show');
	});
})();