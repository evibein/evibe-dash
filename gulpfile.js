var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

	mix.styles([

		/* External files */
		'bootstrap/bootstrap.min.css',
		'datetimepicker/jquery.datetimepicker.css',
		'tagit/jquery.tagit.min.css',
		'jquery-ui/jquery-ui.min.css',
		'fullcalendar/fullcalendar.css',
		'bootstrap/bootstrap-switch.min.css',
		'fullcalendar/fullcalendar.css',
		'selectize/selectize.bootstrap3.css',
		'wysiwyg/bootstrap3-wysihtml5.min.css',

		/* Applications files */
		'app/util.css',
		'app/header.css',
		'app/footer.css',
		'app/app.css',
		'app/page_util.css',
		'app/iq.css',
		'login/login.css',
		'tickets/capture-requirement.css',
		'tickets/recommendations.css',
		'tickets/details.css',
		'tickets/tickets.css',
		'venues/venues.css',
		'vendors/vendors.css',
		'packages/packages.css',
		'package/package.css',
		'cakes/cakes.css',
		'decors/decors.css',
		'add-ons/add-ons.css',
		'reviews/reviews.css',
		'reorder/reorder.css',
		'content/content.css',
		'services/services.css',
		'availability/availability.css',
		'trends/trends.css',
		'stories/stories.css',
		'analytics/analytics.css',
		'tracking/tracking.css',
		'delivery-image/delivery-image.css'
	]);

	mix.scripts([

		/* External files */
		'jquery/jquery.min.js',
		'jquery/jquery.form-validator.min.js',
		'jquery-ui/jquery-ui.min.js',
		'scroll/jquery.scrollTo-1.4.3.1-min.js',
		'bootstrap/bootstrap.min.js',
		'bootstrap/bootstrap-switch.min.js',
		'bootstrap/bootstrap-progressbar.min.js',
		'modernizr/modernizr.custom.54542.js',
		'fullcalendar/fullcalendar.min.js',
		'fullcalendar/gcal.js',
		'datetimepicker/jquery.datetimepicker.js',
		'tagit/tag-it.min.js',
		'noty/jquery.noty.min.js',
		'noty/themes/default.min.js',
		'noty/layouts/top.min.js',
		'selectize/selectize.min.js',
		'wysiwyg/bootstrap3-wysihtml5.min.js',
		'availability/availability',

		/* Application files */
		'app/app.js',

		//Mail-Checker Plugin
		"email/mail-checker.js"
	]);

	mix.copy('resources/assets/js/tickets/create.js', 'public/js/tickets/create.js');
	mix.copy('resources/assets/js/tickets/capture-requirement.js', 'public/js/tickets/capture-requirement.js');
	mix.copy('resources/assets/js/tickets/recommendations.js', 'public/js/tickets/recommendations.js');
	mix.copy('resources/assets/js/tickets/base.js', 'public/js/tickets/base.js');
	mix.copy('resources/assets/js/tickets/booking.js', 'public/js/tickets/booking.js');
	mix.copy('resources/assets/js/tickets/mapping.js', 'public/js/tickets/mapping.js');
	mix.copy('resources/assets/js/tickets/update.js', 'public/js/tickets/update.js');
	mix.copy('resources/assets/js/tickets/basicInfo.js', 'public/js/tickets/basicInfo.js');
	mix.copy('resources/assets/js/tickets/ticketList.js', 'public/js/tickets/ticketList.js');

	mix.copy('resources/assets/js/app/decors.js', 'public/js/decor.js');
	mix.copy('resources/assets/js/app/vendors.js', 'public/js/vendors.js');
	mix.copy('resources/assets/js/app/reorder.js', 'public/js/reorder.js');
	mix.copy('resources/assets/js/app/content.js', 'public/js/content.js');
	mix.copy('resources/assets/js/app/field.js', 'public/js/field.js');
	mix.copy('resources/assets/js/app/stories.js', 'public/js/stories.js');
	mix.copy('resources/assets/js/app/dashboard.js', 'public/js/dashboard.js');
	mix.copy('resources/assets/js/package/package.js', 'public/js/package/package.js');
	mix.copy('resources/assets/js/tracking/tracking.js', 'public/js/tracking/tracking.js');
	mix.copy('resources/assets/js/lazyload-images/lazyLoadImages.min.js', 'public/js/util/lazyLoadImages.js');

	mix.version(['css/all.css', 'js/all.js']);
});