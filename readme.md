## Evibe.in Dash

Evibe.in Dash is developed to manage customers, partners and integrate all the teams at Evibe.in. Some additional features are provided as CMS to manage the website, track errors, manage users & others.

## Features

### Tickets
Manage complete life cycle of ticket: 
- create new tickets
- add mappings
- send recommendations
- order process
- send receipts
- view complete ticket time line