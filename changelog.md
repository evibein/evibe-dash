## Changelog

### v2.6.17 (*07 Jan 2022*) `@NGK`
##### Changed
- SMS templates as per DLT guidelines 

### v2.6.16 (*04 Jun 2020*) `@UM`
##### Added
- Modal to select schedule settlement date 

### v2.6.15 (*28 Apr 2020*) `@NGK`
##### Added
- Virtual Party console 

### v2.6.14 (*23 Apr 2020*) `@NGK`
##### Added
- Option Availability console 

### v2.6.13 (*14 Mar 2020*) `@NGK`
##### Changed
- evib.es to bit.ly

### v2.6.12 (*13 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 2

### v2.6.11 (*9 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 1 part 2

### v2.6.10 (*7 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 1 part 1

### v2.6.9 (*6 Mar 2020*) `@NGK`
##### Added
- Detailed cancellation email to partners (along with currently active bookings for that ticket)

### v2.6.8 (*21 Feb 2020*) `@NGK`
##### Updated
- Special enquiry source visibility for lead generation experiment tickets

### v2.6.7 (*07 Feb 2020*) `@NGK`
##### Added
- Option availability check page for vday

### v2.6.6 (*27 Jan 2020*) `@NGK`
##### Added
- Partner amount in star order settlement sheet

### v2.6.6 (*27 Jan 2020*) `@NGK`
##### Added
- Partner amount in star order settlement sheet

### v2.6.5 (*27 Jan 2020*) `@NGK`
##### Fixed
- Correct format for AppUtil price format functionality

### v2.6.4 (*21 Jan 2020*) `@NGK`
##### Changed
- Only super admins can deactivate the options

### v2.6.3 (*16 Jan 2020*) `@NGK`
##### Added
- Audit console to fix option data (info, terms, etc,.)

### v2.6.2 (*11 Jan 2020*) `@KJ`
##### Added
- Location filter in reco surprise options

### v2.6.1 (*09 Jan 2020*) `@JR`
##### Removed
- Removed one-signal to test the loading speed

### v2.6.0 (*09 Jan 2020*) `@JR`
##### Upgrade
- Laravel project upgrade

### v2.5.70 (*07 Jan 2020*) `@NGK`
##### Added
- Monthly GST reports download

### v2.5.69 (*03 Jan 2020*) `@KJ`
##### Added
- "Drop Off" status

### v2.5.68 (*19 Nov 2019*) `@NGK`
##### Added
- Option name in partner change cancellation email to old partner

### v2.5.67 (*19 Nov 2019*) `@NGK`
##### Added
- Tickets access to bd agent

### v2.5.66 (*11 Nov 2019*) `@NGK`
##### Added
- Planners and venues downloadable sheets with bank details

### v2.5.65 (*11 Nov 2019*) `@NGK`
##### Added
- Venue-halls creation and edit functionality in DASH

### v2.5.64 (*28 Oct 2019*) `@NGK`
##### Added
- Downloadable star orders sheet

### v2.5.63 (*18 Oct 2019*) `@NGK`
##### Added
- Feedback access to marketing

### v2.5.62 (*15 Oct 2019*) `@NGK`
##### Added
- Access to delivery images for marketing role
- Revenue and invalid tickets split in cumulative analytics

### v2.5.61 (*10 Oct 2019*) `@NGK`
##### Added
- Option specific add-ons console

### v2.5.60 (*10 Oct 2019*) `@NGK`
##### Added
- Accounts role and necessary restrictions

### v2.5.59 (*05 Oct 2019*) `@NGK`
##### Added
- Multiple selection for processing settlements

### v2.5.58 (*05 Oct 2019*) `@NGK`
##### Added
- GSTIN in settlement downloadable sheets

### v2.5.57 (*04 Oct 2019*) `@NGK`
##### Changed
- Increased image upload limit for dash options to 5 MB from 1 MB

### v2.5.56 (*27 Sep 2019*) `@NGK`
##### Added
- Package creation and edit support for different venue location packages under same partner
##### Changed
- Party venue address will be taken from package rather than venue partner

### v2.5.55 (*16 Sep 2019*) `@JR`
##### Added
- Added followup & in-progress in new dashboard

### v2.5.54 (*14 Sep 2019*) `@NGK`
##### Added
- Default selection of source and comments for duplicate ticket update

### v2.5.53 (*11 Sep 2019*) `@UM`
##### Modified
- New dashboard upgrades

### v2.5.52 (*11 Sep 2019*) `@NGK`
##### Added
- Button to mark booking as star orders

### v2.5.51 (*10 Sep 2019*) `@NGK`
##### Added
- Support for partner quick settlements

### v2.5.50 (*04 Sep 2019*) `@NGK`
##### Added
- Edit and delete functionality for processed adjustments

### v2.5.49 (*29 Aug 2019*) `@NGK`
##### Fixed
- Bank details validation for partner profiles

### v2.5.48 (*13 Aug 2019*) `@NGK`
##### Added
- Other roles in ticket list page handlers dropdown

### v2.5.47 (*05 Aug 2019*) `@NGK`
##### Added
- Avail check uploaded images

### v2.5.46 (*26 Jul 2019*) `@NGK`
##### Added
- Payment done at filter in tickets list page

### v2.5.45 (*04 Jul 2019*) `@NGK`
##### Added
- Star Order highlight in ticket bookings, OPS order confirmations and avail-checks

### v2.5.44 (*03 Jul 2019*) `@GS` `@KJ`
##### Added
- Console to map partner reviews and delivery images to respective options

### v2.5.43 (*02 Jul 2019*) `@NGK`
##### Added
- Notification info in avail-checks list

### v2.5.42 (*02 Jul 2019*) `@NGK`
##### Fixed
- Category selection for custom ticket

### v2.5.41 (*02 Jul 2019*) `@NGK`
##### Added
- Added settlement deviations btn in pending settlement tab

### v2.5.40 (*28 Jun 2019*) `@GS` `@KJ`
##### Added
- New 2 step avail check model

### v2.5.39 (*27 Jun 2019*) `@NGK`
##### Changed
- Skipping the category tag selection step in DASH workflow

### v2.5.38 (*26 Jun 2019*) `@NGK`
##### Changed
- Special notes will be shown for respective booking

### v2.5.37 (*22 Jun 2019*) `@NGK`
##### Added
- Added support for option type dynamic checkout fields

### v2.5.36 (*22 Jun 2019*) `@JR`
##### Added
- CRM dashboard in progress tab

### v2.5.35 (*21 Jun 2019*) `@GS`
##### Changed
- Changed AB and enquiry communication from business to ops

### v2.5.34 (*21 Jun 2019*) `@KJ`
##### Changed
- Restriction wrong pin code and duplicate area creations

### v2.5.33 (*20 Jun 2019*) `@GS` `@KJ`
##### Removed
- Landmark and followup creation in ticket workflow

### v2.5.32 (*20 Jun 2019*) `@GS`
##### Removed
- Removed unnecessary fields in finalising mapping and editing booking

### v2.5.31 (*17 Jun 2019*) `@KJ`
##### Added
- Partner cancelled (Auto Cancel) booking in OPS order confirmations

### v2.5.30 (*7 Jun 2019*) `@NGK`
##### Fixed
- Data table sorting for PIAB product bookings

### v2.5.29 (*7 Jun 2019*) `@NGK`
##### Fixed
- Delivery date in customer communication for delivered and cancelled status.

### v2.5.28 (*5 Jun 2019*) `@NGK`
##### Added
- Partner change option to bookings at any stage

### v2.5.27 (*31 May 2019*) `@GS` `@KJ`
##### Added
- Order Update functionality for PIAB dashboard

### v2.5.26 (*29 May 2019*) `@GS` `@KJ`
##### Fixed
- Avail check image error notification

### v2.5.25 (*29 May 2019*) `@NGK`
##### Changed
- Booking receipt status messaged in tickets booking tab

### v2.5.24 (*28 May 2019*) `@NGK`
##### Added
- Alt phone in contact section in tickets
##### Changed
- Email input to first screen in tickets
##### Fixed
- Order process on services will work from now

### v2.5.24 (*07 Jun 2019*) `@JR`
##### Modified
- Optimized code for partner delivery page, can be optimized more if further requested by the team

### v2.5.23 (*25 May 2019*) `@NGK`
##### Added
- Settlement related models

### v2.5.22 (*23 May 2019*) `@GS` `@KJ`
##### Added
- PIAB dashboard

### v2.5.21 (*22 May 2019*) `@GS`
##### Added
- Ticket update and followup for whatsapp communication

### v2.5.20 (*18 May 2019*) `@KJ`
##### Added
- Customer Reviews in OPS dashboard

### v2.5.19 (*18 May 2019*) `@KJ`
##### Added
- Individual ticket details edit functionality

### v2.5.18 (*18 May 2019*) `@NGK`
##### Changed
- Customer name is not mandatory to send no-response alert

### v2.5.17 (*16 May 2019*) `@NGK`
##### Added
- Added separate redis DB for sessions

### v2.5.16 (*16 May 2019*) `@KJ` `@GS`
##### Added
- OPS order confirmations list

### v2.5.15 (*13 May 2019*) `@JR`
##### Modified
- Modified subject lines for same email thread communication

### v2.5.14 (*9 May 2019*) `@NGK`
##### Added
- Additional status to remove / mark active followups

### v2.5.13 (*8 May 2019*) `@NGK`
##### Added
- Avail Check console

### v2.5.12 (*17 Apr 2019*) `@JR`
##### Added
- Added date filter for payment monitor console

### v2.5.11 (*10 Apr 2019*) `@JR`
##### Added
- Added pagination for payment monitoring

### v2.5.10 (*3 Apr 2019*) `@NGK`
##### Added
- Functionality to manage customer upload proofs in OPS dashboard

### v2.5.9 (*22 Mar 2019*) `@NGK`
##### Changed
- SMS sender id to EVIBES from EEVIBE

### v2.5.8 (*22 Mar 2019*) `@JR`
##### Modified
- Shifting from goo.gl to evib.es

### v2.5.7 (*20 Mar 2019*) `@NGK`
##### Fixed
- Post booking delete, ticket updates for auto booked tickets

### v2.5.6 (*19 Mar 2019*) `@JR`
##### Added
- Deleting HTML cache after deleting/deactiving the package

### v2.5.5 (*7 Mar 2019*) `@NGK`
##### Added
- Increased access for operations role

### v2.5.4 (*7 Mar 2019*) `@NGK`
##### Fixed
- Url in customer updates
##### Added
- Status in list table

### v2.5.3 (*7 Mar 2019*) `@NGK`
##### Added
- Bachelor occasion in edit ticket

### v2.5.2 (*6 Mar 2019*) `@NGK`
##### Added
- OPS Dashboard

### v2.5.1 (*5 Mar 2019*) `@JR`
##### Added
- Sales dashboard, added party date & sort by status

### v2.5.0 (*4 Mar 2019*) `@JR`
##### Added
- Sales dashboard

### v2.4.33 (*2 Mar 2019*) `@NGK`
##### Added
- Bachelors occasion in new ticket creation
##### Changed
- Showing party pin code in ticket creation screen

### v2.4.32 (*27 Feb 2019*) `@NGK`
##### Changed
- Order process SMS to customer with payment link

### v2.4.31 (*22 Feb 2019*) `@JR`
##### Modified
- Not adding timestamp in url for packages while adding & copying packages

### v2.4.30 (*20 Feb 2019*) `@NGK`
##### Added
- Hotjar tracking code

### v2.4.29 (*19 Feb 2019*) `@JR`
##### Modified
- Not adding timestamp for new packages

### v2.4.28 (*18 Feb 2019*) `@NGK`
##### Added
- Country calling code

### v2.4.27 (*14 Feb 2019*) `@NGK`
##### Added
- Reminder modal to send updated receipt after partner change

### v2.4.26 (*14 Feb 2019*) `@NGK`
##### Fixed
- Updated partner will receive new receipt instead of updated

### v2.4.25 (*13 Feb 2019*) `@NGK`
##### Fixed
- Closure date cannot be after party date

### v2.4.24 (*12 Feb 2019*) `@JR`
##### Added
- Showing line graph for bookings

### v2.4.23 (*09 Feb 2019*) `@JR`
##### Added
- Showing bookings info in analytics enquiry tab

### v2.4.22 (*07 Feb 2019*) `@JR`
##### Added
- Added new/return graph in analytics screen
- Syrow handled tickets Changing to In progress status

### v2.4.21 (*07 Feb 2019*) `@JR`
##### Added
- Added new/return tickets classification in analytics screen

### v2.4.20 (*06 Feb 2019*) `@JR`
##### Modified
- Removing followup for end state tickets
##### Added
- Add ons count in bookings console

### v2.4.19 (*01 Feb 2019*) `@JR`
##### Modified
- CRM dashboard rows updating, rather than refreshing the whole page

### v2.4.18 (*31 Jan 2019*) `@JR`
##### Fixed
- Error message for venue package null value in avail check console
- Add created at in syrow dashboard

### v2.4.17 (*30 Jan 2019*) `@NGK`
##### Fixed
- Update of booking amount on modal open by changing the product price on change position

### v2.4.16 (*30 Jan 2019*) `@NGK`
##### Fixed
- Removed incorrect mapping condition to show partner change option

### v2.4.15 (*29 Jan 2019*) `@JR`
##### Added
- Avail Check Console

### v2.4.14 (*29 Jan 2019*) `@JR`
##### Modified
- Not valid enquiry creating new ticket even if initiated ticket exists

### v2.4.13 (*29 Jan 2019*) `@JR`
##### Modified
- Minor updates in syrow dashboard

### v2.4.12 (*24 Jan 2019*) `@JR`
##### Added
- Showing multiple ticket for phone search in syrow dashboard

### v2.4.11 (*24 Jan 2019*) `@JR`
##### Added
- Modifications in syrow dashboard

### v2.4.10 (*24 Jan 2019*) `@NGK`
##### Added
- Info text for partner change modal
- Restriction for advance amount which has been paid

### v2.4.9 (*24 Jan 2019*) `@NGK`
##### Added
- Partner and Product update for bookings
##### Removed
- Old partner change modules (only front-end)

### v2.4.8 (*22 Jan 2019*) @JR
##### Added
- Added bookings console for analysis

### v2.4.7 (*21 Jan 2019*) @JR
##### Added
- Added valid, Invalid filters for enquiry data console

### v2.4.6 (*21 Jan 2019*) @JR
##### Added
- Added filters for enquiry data console

### v2.4.5 (*19 Jan 2019*) @JR
##### Added
- Added console for getting enquiry data

### v2.4.4 (*10 Jan 2019*) @JR
##### Added
- Added update button in CRM dashboard

### v2.4.3 (*10 Jan 2019*) @JR
##### Fixed
- Corner cases for fetching results in syrow dashboard

### v2.4.2 (*08 Jan 2019*) @JR
##### Added
- Console for payments tracking
##### Modified
- Changed shorten algorithm from bit.ly to goo.gl

### v2.4.0 (*02 Jan 2019*) @JR
##### Added
- Dashboard for SYROW users

### v2.3.95 (*02 Jan 2019*) @JR
##### Fixed
- Multiple tickets creation for copy ticket

### v2.3.94 (*24 Dec 2018*) @JR
##### Modified
- Dash workflow v3

### v2.3.93 (*14 Dec 2018*) @NGK
##### Added
- Support for checkbox checkout fields

### v2.3.92 (*13 Dec 2018*) `@JR`
##### Added
- Comments for followup updates 

### v2.3.91 (*11 Dec 2018*) `@JR`
##### Added
- Unable to accept service auto booking with add-on
- Unable to update partner if we re-open the ticket after cancellation
- Followup type & closure date integration in all the places

### v2.3.90 (*5 Dec 2018*) `@JR`
##### Changed
- Updated order process email subject

### v2.3.89 (*4 Dec 2018*) `@AR`
##### Changed
- Updated company number

### v2.3.88 (*4 Dec 2018*) `@NGK`
##### Added
- Event name in customer and partner communication

### v2.3.87 (*3 Dec 2018*) `@NGK`
##### Changed
- Cancellation policy

### v2.3.86 (*10 Nov 2018*) `@NGK`
##### Fixed
- Temporary time switch to avoid bitly error

### v2.3.85 (*19 Oct 2018*) `@Manav`
##### Added
- Asking for expected closure date after successful reco email, no process email and if any followup is updated
- Changing the date picker in cumulative analytics to date time picker and default time is yesterday 8pm to today 8pm

### v2.3.84 (*05 Nov 2018*) `@JR`
##### Modified
- Modified content for No-response whatsapp message.

### v2.3.83 (*02 Nov 2018*) `@Manav`
##### Added
- Asking for avail check in finalize model.

### v2.3.82 (*01 Nov 2018*) `@Manav`
##### Changed
- Number being sent to customer for no-response alert

### v2.3.81 (*29 Oct 2018*) `@Manav`
##### Added
- Now allow user to change the partner and select a default evibe partner.
- Can be changes before enquiring and finalizing

### v2.3.80 (*29 Oct 2018*) `@Manav`
##### Added
- Price Filter for delivery images.

### v2.3.79 (*19 Oct 2018*) `@Manav`
##### Added,fixed
- Quick book, now sending mails and setting payment deadline to 3 hours as default for quickbook tickets
- Added model to whatsapp button and setting messages according to the option selected
- Added whatsapp button after order process is sent.

### v2.3.78 (*15 Oct 2018*) `@Manav`
##### Added
- Adding created at filter for all the tabs.

### v2.3.77 (*28 Sep 2018*) `@Manav`
##### Added
- Order process sending Email format change.

### v2.3.76 (*16 Oct 2018*) `@Manav`
##### Added
- Added History tab in crm dashboard

### v2.3.75 (*15 Oct 2018*) `@Manav`
##### Added
- Asking for gender of customer.
- Asking for time slot of party.
- Team Notes on every page.
- No default for any select box
- Making ticket source selectize.
- Asking min and max budget of customer.
- Added new select box asking the Best time to contact.
- Added new select box asking the decision maker.
- Added new box asking the alt email and alt phone in 5th screen.


### v2.3.74 (*12 Oct 2018*) `@Manav`
##### Added
- Removing comment box where followups were there. Adding the dropdown for followup messages.
- Adding the filter for the followup type in new,return and missed tabs in crm dashboard.
- Adding closure date filter and closure date column in new,followup and missed tabs.
- Adding prospects tab in crm dashboard.

### v2.3.73 (*10 Oct 2018*) `@Manav`
##### Fixes,Added
- Fixed the redirection based on the filters. Previously it was redirecting to selected images.
- Added Default button to add image to default product.
- Added message for image rotation
- Added reset filter button

### v2.3.72 (*26 Sep 2018*) `@Manav`
##### Added
- Delivery Image Console to Dash

### v2.3.71 (*8 oct 2018 ~ 9 Oct 2018*) `@Manav`
##### Added
- Added filter for customer source
- Added button to send no response msg va whatsapp

##### Fixed
- Analytics cummulative screen bug fix

### v2.3.70 (*6 Oct 2018*) `@MC`
##### Added
- Added filter for party date and followup date

### v2.3.69 (*6 Oct 2018*) `@Manav`
##### Added
- Added missed in CRM dashboard

### v2.3.68 (*5 Oct 2018*) `@JR`
##### Modified
- Minor UI changes for CRM dashboard

### v2.3.67 (*5 Oct 2018*) `@AR`
##### Added
- Extra line indicating customers to revert if there are any modifications or corrections required

### v2.3.66 (*3 Oct 2018*) `@AR`
##### Fixed
- Fixed issue with array functions on empty array for add-on analytics

### v2.3.65 (*1 Oct 2018*) `@NGK`
##### Changed
- Using invites group for invitations related communication

### v2.3.64 (*29 Sep 2018*) `@AR`
##### Fixed
- Not considering add-ons with same partner for count calculations in cumulative analytics screen

### v2.3.63 (*28 Sep 2018*) `@NGK`
##### Changed
- Partner change can happen irrespective of partner type

### v2.3.62 (*28 Sep 2018*) `@Manav`
##### Added
- Email Tracker for no response and receipt emails.

### v2.3.61 (*25 Sep 2018*) `@NGK`
##### Added
- TYC in benefits

### v2.3.60 (*21 Sep 2018*) `@NGK`
##### Added
- SMS template to notify invite card to customers

### v2.3.59 (*19 Sep 2018*) `@Manav`
##### Fixed
- Date Sorting in CRM Dashboard.

### v2.3.58 (*17 Sep 2018*) `@NGK`
##### Fixed
- RSVP link generation issue 

### v2.3.57 (*12 Sep 2018*) `@JR`
##### Fixed
- Numbers mismatch due to filter value 0

### v2.3.56 (*11 Sep 2018*) `@NGK`
##### Added
- Invites/TYC tab to send cards to opted customers

### v2.3.55 (*07 Aug 2018*) `@Manav`
##### Modified
- Changes in Crm dashboard. 
-Showing booking likeliness
-Changes from checkbox to dropdown

### v2.3.54 (*07 Sep 2018*) `@NGK`
##### Changed
- Default database number for redis

### v2.3.53 (*04 Sep 2018*) `@Manav`
##### Added 
- Added Special notes in bookings tab.

### v2.3.52 (*29 Aug 2018*) `@JR`
##### Fixed 
- Packages not showing in mappings page
- Sort by created issue in dashboard

### v2.3.51 (*29 Aug 2018*) 
##### Added `@Manav`
- Whatsapp button to ticket list, profile and recommendations model

### v2.3.50 (*25 Aug 2018*) 
##### Added `@Manav`
- Email typo error while creating the new ticket
##### Fixed `@JR`
- Calender data not showing for other months

### v2.3.49 (*25 Aug 2018*) `@NGK`
##### Fixed
- Included sms error details and will be sent to sysad@evibe.in

### v2.3.48 (*25 Aug 2018*) `@NGK`
##### Added
- Support to cancel multiple auto-bookings

### v2.3.47 (*24 Aug 2018*) `@Manav`
##### Fixed
- Info page not loading due to mapping issue

### v2.3.46 (*17 Aug 2018*) `@Manav`
##### Fixed
- Integration of benefits in emails.

### v2.3.45 (*16 Aug 2018 ~ 18 Aug 2018*) `@Manav`
##### Added
- Cache in main when some item is edited

### v2.3.44 (*10 Aug 2018*) `@Manav`
##### Fixed
- Custom payment deadline selection

### v2.3.43 (*06 Aug 2018 ~ 10 Aug 2018*) `@Manav`
##### Added
- Sort reco options through price.

### v2.3.42 (*02 Aug 2018*) `@Manav`
##### Modified
- Watermark for DASH  

### v2.3.41 (*04 Aug 2018*) `@Manav`
##### Fixed
- Recon page error in dash for no events 

### v2.3.40 (*25 July 2018*)
##### Added
- RE metrics console

### v2.3.39 (*19 July 2018*) `@NGK`
##### Fixed
- Disabling reco send email button to avoid sending email to invalid email address.

### v2.3.38 (*21 Mar 2018 ~ 18 July 2018*)
##### Added
- Added extra columns for auto-popup `@JR`

### v2.3.37 (*17 July 2018 ~ 18 July 2018*) `@NGK`
##### Added
- Populating option related images for team to send in receipts.
- Additional alert for refund creation modal.
- Post party cancellations.
##### Changed
- Delete btn enabled for tickets where payment is not done.
##### Fixed
- Venue address for quick booked tickets

### v2.3.36 (*06 July 2018*)
##### Added
- Payment deadline Options `@Manav`

### v2.3.35 (*30 June 2018 ~ 02 Jul 2018*)
##### added
- Implemented quick book feature `@NGK`

### v2.3.34 (*27 June 2018*)
##### Added
- Recommendation link in mappings and all pages of reco tab `@NGK`

### v2.3.33 (*18 June 2018 ~ 26 June 2018*)
##### Changes
- Updates happened through 'auto' will have different background color `@NGK`

### v2.3.32 (*23 June 2018*)
##### Added
- Ability to add and remove events, categorised tags `@AR`

### v2.3.31 (*08 June 2018*)
##### Added
- Code for calculating the resolution of images  `@Ramu`

### v2.3.30 (*31 May 2018*)
##### Fixed
- No response ticket follow up issues resolved `@Ramu`

### v2.3.29 (*23 May 2018*)
##### Fixed
- Fixed view live button issues for all options `@Ramu`

### v2.3.28 (*21 May 2018*)
##### Changed
- Search images by product code console added `@Ramu`

### v2.3.27 (*18 May 2018*)
##### Changed
- Create settlements button will be enabled all the time `@NGK`

### v2.3.26 (*09 May 2018*)
##### Changed
- Booking cancellation options test in cancellation module `@NGK`
##### Updated   
- Partner Delivery upgrades `@RAMU` `@JR`

### v2.3.25 (*08 May 2018*)
##### Added
- Access token for crm dashboard `@JR`

### v2.3.24 (*02 May 2018 ~ 09 May 2018*)
##### Modified `@RAMU`
- Rupee font changed in some files
- partner options button added partner page

### v2.3.23` (*01 May 2018 ~ 09 May 2018*)
##### Added `@RAMU`
- CS workflow console source specific added 
- Added modal for asking followup after no response
- Added modal for asking change the type ticket in packages

### v2.3.22 (*02 May 2018*)
##### Updated
- Updated CRM metrics numbers `@JR`

### v2.3.21 (*01 May 2018*)
##### Removed
- Triggers to update lookup table `@NGK`

### v2.3.20 (*30 Apr 2018*)
##### Added
- Complete dashboard V2

### v2.3.19 (*29 Apr 2018*)
##### Fixed
- Route error for CRM dashboard link `@JR`

### v2.3.18 (*28 Apr 2018*)
##### Added
- Refund reference in customer refunds console `@NGK`
##### Updated
- CRM dashboard `@JR`

### v2.3.16 (*24 Apr 2018*)
##### Removed
- 'Delete' option for ticket and booking `@NGK`
##### Added
- Confirmation message for settlement creation button `@NGK`
- Old partner link if partner updated `@NGK`
- Pending settlement tab `@NGK`
##### Changed
- Create settlements button will be enabled only on Wednesday `@NGK`

### v2.3.15 (*23 Apr 2018*)
##### Added
- Coupon Consoles `@JR`

### v2.3.14 (*19 Apr 2018*)
##### Changed
- On partner change, updated receipts will be sent automatically `@NGK`

### v2.3.13 (*18 Apr 2018*)
##### added
- Implemented bitly url shortener as goo.gl is no longer functional `@NGK`
- Partner city in settlements sheet and console `@NGK`

### v2.3.12 (*09 Apr 2018*)
##### Removed
- Change in settlement system text in settlement emails `@NGK`
##### Added
- Negative sign in Indian price format `@NGK`

### v2.3.11 (*07 Apr 2018 ~ 08 Apr 2018*)
##### Added
- Will send cancellation receipts and button to resend receipts `@NGK`
##### Fixed
- Cancel btn will not be shown for AB tickets as they can cancel at ticket level `@NGK`

### v2.3.10 (*07 Apr 2018 ~ 08 Apr 2018*)
##### Added
- CRM dashboard history tab `@Ramu`
##### Fixed
- Added live profile of the partner for BD team `@Ramu`

### v2.3.9 (*05 Apr 2018 ~ 08 Apr 2018*)
##### Added
- Send cancellation email to changed partner `@NGK`

### v2.3.8 (*04 Apr 2018*)
##### Fixed
- Temporary fix for order details not showing while clicking on edit in bookings tab `@JR`

### v2.3.7 (*29 Mar 2018 ~ 04 Apr 2018*)
##### Added
- ENS for Settlements `@NGK`
- Change partner for booked tickets v1 `@NGK`

### v2.3.6 (*26 Mar 2018 ~ 31 Mar 2018*)
##### Fixed
- Added live profile of the partner for BD team `@Ramu`

### v2.3.5 (*04  2018*)
##### Fixed
- Ticket update character count is decreased to 6 `@Ramu`

### v2.3.4 (*22 Mar 2018 ~ 27 Mar 2018*)
##### Fixed
- Login Issue for the partner acceptance, if email already exists in the user table `@JR`

### v2.3.3 (*27 Mar 2018*)
##### Added
- Introduced new status 'No Response' `@NGK`

### v2.3.2 (*24 Mar 2018*)
##### Added
- CRM DASH Board Upgrades  `@Ramu`

### v2.3.1 (*24 Mar 2018*)
##### Added
- Ticket list page upgrades added and some modifications
##### Fixed
- Not showing checkout fields in the bookings tab `@JR`

### v2.3.0 (*14 Mar 2018*)
##### Added
- Auto Settlements v1 - Finance console and ticket finance tab `@NGK`

### v2.2.64 (*20 Mar 2018*)
##### Changed
- Updated mobile number sent in no-response SMS alert and body changes for email alerts based on ticket status `@NGK`

### v2.2.63 (*20 Mar 2018*)
##### Fixed
-Fixed ticket edit error `@Ramu`

### v2.2.62 (*16 Mar 2018 ~ 19 Mar 2019*)
##### Added
- Added map co-ordinates and service active / inactive options `@Ramu`

### v2.2.61 (*16 Mar 2018 ~ 19 Mar 2019*)
##### Added
- Edit tags for house-warming decor & surprises
- Order tracking partner selection 
- Minor fixes regarding partner name validation `@JR`

### v2.2.58 (*16 Mar 2018*)
##### Fixed
- DataTable added for tables in CRM Dashboard `@ANJI`

### v2.2.57 (*16 Mar 2018*)
##### Fixed
- Order process & multiple enquiry fixes `@JR`

### v2.2.56 (*14 Mar 2018*)
##### Fixed
- Fixed some of the issues of CRM DASH Board `@Ramu`

### v2.2.55 (*14 Mar 2018*)
##### Fixed
- Issues with change in parent event ID `@JR`

### v2.2.54 (*13 Mar 2018*)
##### Fixed
- Fixed some of the issues in CRM DASH Board `@Ramu`

### v2.2.53 (*12 Mar 2018*)
##### Added
- CS1 v2 - screen 3 modifications `@JR`
- CS1 v2 -  screen 2 and 5 modifications `@RAMU`

### v2.2.51 (*07 Mar 2018*)
##### Fixed
- Sorting results by partydate instead of booking date `@JR`

### v2.2.50 (*28 Feb 2018*)
##### Added
- Added old crm dashboard to current CRM analytics page `@Ramu`

### v2.2.49 (*01 Mar 2018*)
##### Added
- Site visit details `@JR`

### v2.2.48 (*02 Mar 2018*)
##### Changed
- Upgraded showing pending feedback for reviews page `@JR`

### v2.2.47 (*24 Feb 2018*)
##### Added
- Added ADD/DELETE functionality for settlement bookings `@JR`

### v2.2.46 (*22 Feb 2018*)
##### Added
- Added Company name for corporates `@JR`

### v2.2.45 (*21 Feb 2018*)
##### Added
- Added Filter options in CRM DASH page

### v2.2.44 (*21 Feb 2018*)
##### Added
- Automate customer feedback,Review accept/reject module for BD
##### Fixed
- Delete mapping for autobooked tickets.
- Senior CRM login page redirecting issue `@JR`

### v2.2.41 (*14 Feb 2018*)
##### Added
- Added few more details to the CRM analytics page

### v2.2.41 (*14 Feb 2018*)
##### Added
- Implementation for created handler id for tickets

### v2.2.40 (*14 Feb 2018*)
##### Added
- Added CRM analytics page

### v2.2.39 (*08 Feb 2018*)
##### Added
- Ticket customer source filter
##### Fixed
- Optimize queries in ticket list page `@JR`

### v2.2.37 (*08 Feb 2018*)
##### Added
- Package inclusions not showing
- Decor filter for occasions
- Reload page after adding mappings
- Order tracking completed orders page not working `@Ramu`

### v2.2.33 (*01 Feb 2018*)
##### Fixed
- Fixing filters in cake and decors `@Ramu`

### v2.2.32 (*31 Jan 2018*)
##### Fixed
- Showing relevant tags in cakes ,decors and packages `@Ramu`

### v2.2.31 (*31 Jan 2018*)
##### Updated
- Date and timings filter updated in tickets page `@Ramu`

### v2.2.30 (*30 Jan 2018*)
##### Fixed
- Pincode problem in some pages in DASH `@Ramu`

### v2.2.29 (*30 Jan 2018*)
##### Added
- Cake filter added in cake page `@Ramu`

### v2.2.28 (*27 Jan 2018*)
##### Fixed
- Party date time in tickets list page `@NGK`

### v2.2.27 (*27 Jan 2018*)
#### Update
- updated tickets page for pin code issue
 
### v2.2.26 (*27 Jan 2018*)
#### Added
- Added reason for rejection in bd approval

### v2.2.25 (*17 Jan 2018*)
##### Added
- updated not interested filter in DASH `@Ramu`

### v2.2.24 (*17 Jan 2018*)
##### Added
- Senior CRM employees permissions `@Ramu`

### v2.2.23 (*16 Jan 2018*)
##### Added
- Partner documentation for partners to upload files `@Ramu`

#### v2.2.22 (*13 Jan 2018*)
##### Changed 
- Test followup cases console included in automation `@NGK`
##### Fixed
- Auto followup reminders console `@NGK`

#### v2.2.21 (*12 Jan 2018*)
##### Added 
- New ticket status 'Not Interested' `@NGK`
- New API support `@NGK`
- Merged ticket updates with log ticket reminders `@NGK`

#### v2.2.20 (*12 Jan 2018*)
##### Fixes 
- Booking order details css bug fix
- Adding new role sr_bd & give activation access to that
- Cake description adding html editor & small fix in fetching data for feedback approvals `@JR`

#### v2.2.17 (*10 Jan 2018*)
##### Added 
- Add/delete service categories for services `@JR`

#### v2.2.16 (*08 Jan 2018*)
##### Added 
- Included 'automation' under tech access `@NGK`

#### v2.2.15 (*05 Jan 2018*)
##### Added 
- Console to check automation reminders for a ticket `@RAMU`


#### v2.2.14 (*29 Dec 2017*)
##### Added 
- Added recommendation filter for ticket list page `@RAMU`

#### v2.2.13 (*26 Dec 2017*)
##### Added 
- Provided support for followup automation and provided option for CRM to view recommendations without altering the flow `@NGK`

#### v2.2.12 (*23 Dec 2017*)
##### Added 
- Added event id for reviews `@JR`

#### v2.2.11 (*18 Dec 2017*)
##### Added 
- Selling status for packages, decors, cakes & services `@JR`

#### v2.2.10 (*11 Dec 2017*)
##### Fixed 
- Close button for recommendations, minor UI / UX changes for CS2A
##### Added 
- Console for add/delete tags for kids Bday decorations `@JR`

#### v2.2.8 (*11 Dec 2017*)
##### Fixed 
- Copy feature for packages `@JR`

#### v2.2.7 (*09 Dec 2017*)
##### Fixed 
- Partner signup form message showing in partner profile
- Order tracking partner mapping mismatch errors `@JR`

#### v2.2.5 (*08 Dec 2017*)
##### Fixed 
- Mappings revamp to reduce the queries and load faster`@JR`

#### v2.2.4 (*05 Dec 2017*)
##### Added 
- CS2A (Recommendation Stage) of new ticket enquiry flow supporting add/delete options based on tags `@JR`

#### v2.2.3 (*02 Dec 2017*)
##### Added 
- Selectize for the location selection & change pincode on change of the location
- Cancel button to go to list / info page accordingly
##### Fixed
- Decors link visibility to CRM & Edit button to access to SR-CRM & Admin `@JR`

#### v2.2.0 (*01 Dec 2017*)
##### Added 
- CS1 (Enquiry Stage) of new ticket enquiry flow supporting addition and update of ticket enquiry `@JR`

#### v2.1.3 (*01 Dec 2017*)
##### Fixed
- Showing dynamic venue fields for non-venue packages
- Not showing inbound packages link for BD team `@JR`

#### v2.1.1 (*01 Dec 2017*)
##### Changed 
- Email template for partner settlement emails `@ANJI`

#### v2.1.0 (*18 Nov 2017*)
##### Added 
- Partner dashboard related features: Approve / Message / Reject partner package `@JR`

#### v2.0.3 (*18 Nov 2017*)
##### Added 
- Cumulative analytics by parties (need to optimise the code) `@ANJI`

#### v2.0.2 (*13 Nov 2017*)
##### Fixed
- Instead of restricting CRM team from creating a booking without rounding off the balance amount, alert them

#### v2.0.1 (*9 Nov 2017*)
##### Fixed
- Server timeout error in packages list page