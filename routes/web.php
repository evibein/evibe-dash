<?php

use Illuminate\Support\Facades\Route;

/**
 * Testing Purposes
 */
Route::group(['prefix' => 'test'], function () {
	Route::get('future-bookings', ['uses' => 'TestController@fetchFutureBookings']);

	Route::get('updated-prices', ['uses' => 'TestController@showUpdatedPrices']);

	Route::get('show-add-on-check-fields', ['uses' => 'TestController@showAddOnsCheckOutFields']);

	Route::post('create-add-on-check-fields', ['uses' => 'TestController@createAddOnsCheckOutFields']);
});

/**
 * UPGRADES
 */
Route::group(['prefix' => 'upgrade', 'namespace' => 'Util'], function () {
});

/**
 * Display metrics
 */
Route::group(['prefix' => 'metrics/data', 'namespace' => 'Util'], function () {
	Route::get('/', [
		'uses' => 'MetricsDisplayController@showMetricsData',
		'as'   => 'metrics.data'
	]);

	Route::post('/new-tickets/', [
		'uses' => 'MetricsDisplayController@showMetricsNewTicketsData',
		'as'   => 'metrics.data.new.tickets'
	]);

	Route::post('/followups/', [
		'uses' => 'MetricsDisplayController@showMetricsFollowupsData',
		'as'   => 'metrics.data.followups'
	]);

	Route::post('/in-progress/', [
		'uses' => 'MetricsDisplayController@showMetricsInProgressData',
		'as'   => 'metrics.data.inprogress'
	]);
});

/**
 * Temp Route
 */
Route::get('audit/download', ['uses' => 'Settlement\PaymentSettlementController@downloadReport']);

/**
 * WATERMARK
 */
Route::get('images/watermark', ['uses' => 'App\AppController@watermarkImage']);

/**
 * AUTHENTICATION
 */
Route::group(['namespace' => 'App'], function () {
	Route::post('login', 'LoginController@postLogin');

	Route::get('/', [
		'middleware' => 'auth',
		'uses'       => 'AppController@authenticate'
	]);

	Route::get('login', [
		'as'         => 'login',
		'middleware' => 'guest.custom',
		'uses'       => 'AppController@showLogin'
	]);

	Route::any('logout', [
		'as'         => 'logout',
		'middleware' => 'auth',
		'uses'       => 'AppController@doLogout'
	]);
});

/**
 * RESTRICTED ACCESS
 */
Route::group(['middleware' => 'auth', 'prefix' => '/'], function () {
	/**
	 * Change Password
	 */
	Route::group(['prefix' => 'change-password', 'namespace' => 'App'], function () {
		Route::get('/', ['uses' => 'ChangePasswordController@showChangeScreen']);
		Route::post('/', ['uses' => 'ChangePasswordController@updatePassword']);
	});

	/**
	 * Syrow access
	 */
	Route::group(['prefix'    => 's',
	              'namespace' => 'Util',
	              'as'        => "syrow."], function () {
		Route::get('/tickets/list', [
			'as'   => 'tickets.list',
			'uses' => 'SyrowDashBoardController@showTicketList'
		]);

		Route::post('/tickets/list/data', [
			'as'   => 'tickets.list.data',
			'uses' => 'SyrowDashBoardController@getTicketData'
		]);

		Route::post('/ticket/save/invalid', [
			'as'   => 'ticket.save.invalid',
			'uses' => 'SyrowDashBoardController@saveInvalidTicket'
		]);

		Route::post('/ticket/save/comments/{ticketId}', [
			'as'   => 'ticket.save.comments',
			'uses' => 'SyrowDashBoardController@updateTicketComments'
		]);

		Route::post('/phone/save/comments/{phone}', [
			'as'   => 'phone.save.comments',
			'uses' => 'SyrowDashBoardController@updateTicketNotFoundComments'
		]);

		Route::post('/ticket/request/save/{ticketId}', [
			'as'   => 'ticket.request.save',
			'uses' => 'SyrowDashBoardController@saveRequest'
		]);
	});

	/**
	 * Pinterest Api's
	 */
	Route::group(['prefix' => 'pinterest', 'namespace' => 'Pinterest', 'as' => "pinterest."], function () {
		Route::get('/getCode', [
			'as'   => 'code',
			'uses' => 'PinterestController@getPinterestCode'
		]);

		Route::get('/getAccessToken', [
			'as'   => 'token',
			'uses' => 'PinterestController@getPinterestAccessToken'
		]);

		Route::get('/connect', [
			'as'   => 'connect',
			'uses' => 'PinterestController@pinterestOAuth'
		]);

		Route::get('/callback', [
			'as'   => 'connect',
			'uses' => 'PinterestController@callbackFromPinterest'
		]);

		Route::get('/profile', [
			'as'   => 'profile.info',
			'uses' => 'PinterestController@getProfileInfo'
		]);

		Route::post('/tickets/list/data', [
			'as'   => 'tickets.list.data',
			'uses' => 'SyrowDashBoardController@getTicketData'
		]);
	});

	/**
	 * Sales dashboard
	 */
	Route::group(['prefix'    => 'sales',
	              'namespace' => 'Util',
	              'as'        => "sales."], function () {
		Route::get('/dashboard/{ticketRange?}', [
			'as'   => 'tickets.list',
			'uses' => 'SalesDashBoardController@showTicketList'
		]);

		Route::post('/tickets/list/data', [
			'as'   => 'tickets.list.data',
			'uses' => 'SalesDashBoardController@getTicketData'
		]);

		Route::post('/tickets/search/data', [
			'as'   => 'tickets.search.data',
			'uses' => 'SalesDashBoardController@showTicketList'
		]);

		Route::post('/tickets/admin/data', [
			'as'   => 'tickets.search.data',
			'uses' => 'SalesDashBoardController@showAdminMetricsData'
		]);

		Route::post('/ticket/save/invalid', [
			'as'   => 'ticket.save.invalid',
			'uses' => 'SalesDashBoardController@saveInvalidTicket'
		]);

		Route::post('/ticket/save/comments/{ticketId}', [
			'as'   => 'ticket.save.comments',
			'uses' => 'SalesDashBoardController@updateTicketComments'
		]);

		Route::post('/phone/save/comments/{phone}', [
			'as'   => 'phone.save.comments',
			'uses' => 'SalesDashBoardController@updateTicketNotFoundComments'
		]);

		Route::post('/ticket/request/save/{ticketId}', [
			'as'   => 'ticket.request.save',
			'uses' => 'SalesDashBoardController@saveRequest'
		]);
	});

	/**
	 * Add new location
	 */
	Route::post('add-area', ['uses' => 'App\AppController@addNewArea']);

	/**
	 * Update selling status
	 */
	Route::group(['prefix'    => 'update-selling-status/{productTypeId}/{productId}/{statusId?}',
	              'namespace' => 'Util',
	              'as'        => "selling.status."], function () {
		Route::post('/', [
			'as'   => 'update',
			'uses' => 'UtilitiesController@updateSellingStatus'
		]);
	});

	/**
	 * Copy feature
	 */
	Route::group(['prefix' => 'copy', 'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'show.copy.model',
			'uses' => 'CopyOptionController@showCopyModel'
		]);

		Route::post('/', [
			'as'   => 'copy.model.save',
			'uses' => 'CopyOptionController@saveCopyModel'
		]);
	});

	/*
	 * Tracking the order, call to customer, call to partner, customer feedback after party
	 */
	Route::group(['middleware' => 'authorize.tracking',
	              'prefix'     => 'tracking',
	              'as'         => 'tracking.'], function () {
		Route::group(['namespace' => 'Util'], function () {

			Route::get('pending/feedback', [
				'as'   => 'pending.feedback',
				'uses' => 'CustomerFeedbackController@showPendingFeedback'
			]);

			Route::get('pending/feedback/approval', [
				'as'   => 'pending.feedback.approval',
				'uses' => 'CustomerFeedbackController@showPendingApproval'
			]);
		});

		Route::group(['namespace' => 'Tracking'], function () {

			Route::get('/party/current', [
				'as'   => 'list',
				'uses' => 'TrackingController@showList'
			]);

			Route::get('/party/all/{done?}', [
				'as'   => 'party.all',
				'uses' => 'TrackingController@showList'
			]);

			Route::get('{bookingId}/{tracker_id}', [
				'as'   => 'show.model',
				'uses' => 'TrackingController@showModel'
			]);

			Route::post('details/save', [
				'as'   => 'save',
				'uses' => 'TrackingController@saveModel'
			]);

			Route::post('patner/unavailable', [
				'as'   => 'partner.unavailable',
				'uses' => 'TrackingController@sendPartnerUnavailableMail'
			]);
		});
	});

	/**
	 * Dash board based on each team.
	 */
	Route::group(['prefix'    => 'dash-board',
	              'as'        => 'dashBoard.',
	              'namespace' => 'DashBoard'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'DashBoardController@showList'
		]);
	});

	/**
	 * CRM Dash board based on ticket data.
	 */
	Route::group(['prefix'     => 'crm-analytics',
	              'as'         => 'crmDashBoard.',
	              'middleware' => 'authorize.old-dashboard',
	              'namespace'  => 'DashBoard'], function () {
		Route::get('/new-tickets', [
			'as'   => 'new.tickets.list',
			'uses' => 'CrmDashBoardController@showNewTickets'
		]);

		Route::get('/progress-tickets', [
			'as'   => 'progress.tickets.list',
			'uses' => 'CrmDashBoardController@showInProgressTickets'
		]);

		Route::get('/return-tickets', [
			'as'   => 'return.tickets.list',
			'uses' => 'CrmDashBoardController@showReturnTickets'
		]);

		Route::get('/missed-tickets', [
			'as'   => 'missed.tickets.list',
			'uses' => 'CrmDashBoardController@showMissedTickets'
		]);

		Route::get('/prospect-tickets', [
			'as'   => 'prospect.tickets.list',
			'uses' => 'CrmDashBoardController@showProspectTickets'
		]);

		Route::post('/return-tickets/{ticketId}', [
			'as'   => 'return.tickets.updates',
			'uses' => 'CrmDashBoardController@showReturnTicketUpdates'
		]);

		Route::post('/history/{ticketId}', [
			'as'   => 'history.tickets.updates',
			'uses' => 'CrmDashBoardController@showHistoryUpdates'
		]);

		Route::get('/avail-checks', [
			'as'   => 'avail-check.list',
			'uses' => 'CrmDashBoardController@showAvailChecks'
		]);

		Route::get('/metrics', [
			'as'   => 'metrics',
			'uses' => 'CrmDashBoardController@showMetrics'
		]);

		Route::get('/history', [
			'as'   => 'history',
			'uses' => 'CrmDashBoardController@showHistory'
		]);
	});
	/**
	 * ops-dashbaord based on ticket data
	 */
	Route::group(['prefix'    => 'ops-dashboard',
	              'as'        => 'ops-dashboard.',
	              'namespace' => 'DashBoard'], function () {
		Route::get('/', [
			'as'   => 'home',
			'uses' => 'OpsDashboardController@showOrderConfirmationTickets'
		]);

		Route::get('tabs-count', [
			'as'   => 'tabs-count',
			'uses' => 'OpsDashboardController@fetchTabsCount'
		]);

		Route::group(['prefix' => 'order-confirmation-tickets',
		              'as'     => 'order-confirmation-tickets.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'OpsDashboardController@showOrderConfirmationTickets'
			]);

			Route::post('/confirm-partner/{id}', [
				'uses' => 'OpsDashboardController@confirmPartner'
			]);

			Route::get('search', [
				'as'   => 'search',
				'uses' => 'OpsDashboardController@searchOrderConfirmationsData'
			]);
		});

		Route::get('customer-updates', [
			'as'   => 'customer-updates.list',
			'uses' => 'OpsDashboardController@showCustomerUpdates'
		]);

		Route::get('order-tracking', [
			'as'   => 'order-tracking.list',
			'uses' => 'OpsDashboardController@showOrderTracking'
		]);

		Route::group(['prefix' => 'avail-checks',
		              'as'     => 'avail-checks.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'OpsDashboardController@showAvailabilityChecks'
			]);

			Route::post('/get-details', [
				'as'   => 'details',
				'uses' => 'OpsDashboardController@fetchAvailabilityData'
			]);

			Route::post('/confirm-avail-check', [
				'as'   => 'sendCheck',
				'uses' => 'OpsDashboardController@confirmAvailCheck'
			]);
		});

		Route::group(['prefix' => 'customer-proofs',
		              'as'     => 'customer-proofs.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'OpsDashboardController@showCustomerProofs'
			]);

			Route::get('search', [
				'as'   => 'search',
				'uses' => 'OpsDashboardController@searchCustomerProofsData'
			]);

			Route::post('validate', [
				'as'   => 'validate',
				'uses' => 'OpsDashboardController@validateCustomerProofs'
			]);
		});
		/**
		 * Customer Reviews in ops dashboard.
		 */

		Route::get('customer-reviews', [
			'as'   => 'customer-reviews.list',
			'uses' => 'OpsDashboardController@showCustomerReviews'
		]);
		Route::get('customer-reviews/resolve', [

			'uses' => 'OpsDashboardController@resolveCustomerReview'

		]);

		Route::group(['prefix' => 'option-availability',
		              'as'     => 'option-availability.'], function () {
			Route::get('/', [
				'as'   => 'info',
				'uses' => 'OpsDashboardController@showOptionAvailability'
			]);

			Route::get('option-slots', [
				'as'   => 'option-slots',
				'uses' => 'OpsDashboardController@fetchOptionSlots'
			]);

			Route::post('add-unavailability', [
				'as'   => 'add-unavailability',
				'uses' => 'OpsDashboardController@addUnavailability'
			]);
		});
	});

	/**
	 * PIAB-dashboard
	 *
	 */
	Route::group(['prefix' => 'piab-dashboard', 'as' => 'piab-dashboard.', 'namespace' => 'DashBoard'], function () {
		Route::group(['prefix' => 'product-bookings', 'as' => 'product-bookings.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'PiabDashboardController@showProductBookings'
			]);
			Route::get('{id}/info', [
				'as'   => 'info',
				'uses' => 'PiabDashboardController@showProductBookingInfo'
			]);

			Route::post('update-status', [
				'uses' => 'PiabDashboardController@updateProductBookingStatus'
			]);

		});
	});

	/**
	 * Coupon consoles.
	 */
	Route::group(['prefix'    => 'coupon',
	              'as'        => 'coupon.',
	              'namespace' => 'Coupon'], function () {
		Route::get('/redeemed', [
			'as'   => 'redeemed',
			'uses' => 'CouponController@showRedeemed'
		]);

		Route::get('/available', [
			'as'   => 'available',
			'uses' => 'CouponController@showAvailableCoupons'
		]);

		Route::get('/trails', [
			'as'   => 'trails',
			'uses' => 'CouponController@showTrails'
		]);
	});

	Route::group(['prefix' => 'virtual-party', 'as' => 'virtual-party.', 'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'home',
			'uses' => 'VirtualPartyController@getVirtualPartiesList'
		]);

		Route::get('{id}', [
			'as'   => 'details',
			'uses' => 'VirtualPartyController@getVirtualPartyDetails'
		]);

		Route::post('/', [
			'as'   => 'save',
			'uses' => 'VirtualPartyController@createVirtualParty'
		]);

		Route::post('{id}/update', [
			'as'   => 'update',
			'uses' => 'VirtualPartyController@updateVirtualParty'
		]);
	});

	/**
	 * Update tags - ADD/DELETE tags.
	 */
	Route::group(['prefix'    => 'revamp-tags',
	              'as'        => 'revamp.tags.',
	              'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'TagUpgradeController@showList'
		]);

		Route::post('/delete/{tagId?}/{decorId?}', [
			'as'   => 'delete.tag',
			'uses' => 'TagUpgradeController@deleteTag'
		]);

		Route::post('/add/{decorId}', [
			'as'   => 'add.tag',
			'uses' => 'TagUpgradeController@addTag'
		]);

		Route::post('/add/event/{decorId}', [
			'as'   => 'add.event',
			'uses' => 'TagUpgradeController@addEvent'
		]);

		Route::post('/delete/event/{eventId?}/{decorId?}', [
			'as'   => 'delete.event',
			'uses' => 'TagUpgradeController@deleteEvent'
		]);
	});

	/**
	 * New packages added in main will be displayed here.
	 */
	Route::group(['prefix'    => 'partner/add/new-package',
	              'as'        => 'partner.add.package.',
	              'namespace' => 'Partner'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'PartnerPackageController@showList'
		]);

		Route::get('/{packageId}', [
			'as'   => 'profile',
			'uses' => 'PartnerPackageController@showDetails'
		]);

		Route::get('/{packageId}/accept', [
			'as'   => 'accept.approval',
			'uses' => 'PartnerPackageController@acceptPackageApproval'
		]);

		Route::post('/{packageId}/reject', [
			'as'   => 'reject.approval',
			'uses' => 'PartnerPackageController@rejectPackageApproval'
		]);

		Route::post('/{packageId}/upgrade', [
			'as'   => 'upgrade',
			'uses' => 'PartnerPackageController@packageUpgrade'
		]);
	});

	/**
	 * Quick Book
	 *
	 * Shortcut method to quickly create booking and payment link for a ticket
	 */
	Route::group(['middleware' => 'authorize.quick-book',
	              'prefix'     => 'quick-book',
	              'as'         => 'quick-book.',
	              'namespace'  => 'Ticket'
	             ], function () {
		Route::get('/', [
			'as'   => 'data',
			'uses' => 'QuickBookController@fetchQuickBookRequiredData'
		]);

		Route::post('/', [
			'as'   => 'save',
			'uses' => 'QuickBookController@createQuickBooking'
		]);

		Route::get('products', [
			'as'   => 'products',
			'uses' => 'QuickBookController@getQuickBookProducts'
		]);

		Route::get('product-images', [
			'as'   => 'product-images',
			'uses' => 'QuickBookController@getQuickBookProductImages'
		]);

		Route::get('checkout-fields', [
			'as'   => 'checkout-fields',
			'uses' => 'QuickBookController@getQuickBookCheckoutFields'
		]);

		Route::get('product-types', [
			'as'   => 'product-types',
			'uses' => 'QuickBookController@getQuickBookProductTypes'
		]);
	});

	/**
	 * Tickets
	 *
	 * Able to add / edit / view ticket details, mappings, send recommendations
	 * booking confirmations, venue availability check
	 */
	Route::group(['middleware' => 'authorize.tickets',
	              'prefix'     => 'tickets',
	              'as'         => 'ticket.',
	              'namespace'  => 'Ticket'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'TicketListController@showTicketsList'
		]);

		Route::get('new/list', [
			'as'   => 'new.list',
			'uses' => 'TicketListController@showNewTicketsList'
		]);

		Route::get('/status-update/details', [
			'as'   => 'ajaxUpdate',
			'uses' => 'TicketDetailsController@getTicketUpdateDetails'
		]);
		Route::get('update', [
			'as'   => 'update',
			'uses' => 'TicketDetailsController@updateCustomerInfo'
		]);

		Route::post('/setFollowUpForNoResponseAfterMail/{ticketId}', [
			'as'   => 'ajaxFollowUpDate',
			'uses' => 'TicketDetailsController@setFollowUpForNoResponseAfterMail'
		]);

		Route::group(['prefix' => 'create',
		              'as'     => 'create.'], function () {

			Route::group(['prefix' => 'initialise'], function () {

				Route::get('/', [
					'as'   => 'initialise.details',
					'uses' => 'CaptureRequirementsController@initializeTicket'
				]);

				Route::post('/', [
					'as'   => 'save.initialise.details',
					'uses' => 'CaptureRequirementsController@saveInitializedTicket'
				]);
			});

			Route::group(['prefix' => '{ticketId}'], function () {

				Route::group(['prefix' => 'party-details'], function () {

					Route::get('/', [
						'as'   => 'edit.party.details',
						'uses' => 'CaptureRequirementsController@editPartyDetails'
					]);

					Route::post('/', [
						'as'   => 'save.party.details',
						'uses' => 'CaptureRequirementsController@savePartyDetails'
					]);
				});

				Route::group(['prefix' => 'additional-party-details'], function () {

					Route::get('/', [
						'as'   => 'edit.additional.party.details',
						'uses' => 'CaptureRequirementsController@editAdditionalPartyDetails'
					]);

					Route::post('/', [
						'as'   => 'save.additional.party.details',
						'uses' => 'CaptureRequirementsController@saveAdditionalPartyDetails'
					]);
				});

				Route::group(['prefix' => 'services'], function () {

					Route::get('/', [
						'as'   => 'edit.services',
						'uses' => 'CaptureRequirementsController@editServices'
					]);

					Route::post('/', [
						'as'   => 'save.services',
						'uses' => 'CaptureRequirementsController@saveServices'
					]);
				});

				Route::post('/get-checkout-fields', [
					'as'   => 'get.checkout.fields',
					'uses' => 'CaptureRequirementsController@getTicketBookingCheckoutFields'
				]);

				Route::group(['prefix' => 'contact-info'], function () {

					Route::get('/', [
						'as'   => 'edit.contact.info',
						'uses' => 'CaptureRequirementsController@editContactInfo'
					]);

					Route::post('/', [
						'as'   => 'save.contact.info',
						'uses' => 'CaptureRequirementsController@saveContactInfo'
					]);
				});

				Route::group(['prefix' => 'status-info'], function () {

					Route::get('/', [
						'as'   => 'edit.status.info',
						'uses' => 'CaptureRequirementsController@editPartyStatusInfo'
					]);

					Route::post('/', [
						'as'   => 'save.status.info',
						'uses' => 'CaptureRequirementsController@savePartyStatusInfo'

					]);

				});
			});
		});

		Route::group(['prefix' => 'edit', 'as' => 'modify.'], function () {

			Route::group(['prefix' => '{ticketId}'], function () {

				Route::group(['prefix' => 'party-details/{edit?}'], function () {

					Route::get('/', [
						'as'   => 'edit.party.details',
						'uses' => 'CaptureRequirementsController@editPartyDetails'
					]);

					Route::post('/', [
						'as'   => 'save.party.details',
						'uses' => 'CaptureRequirementsController@savePartyDetails'
					]);
				});

				Route::group(['prefix' => 'additional-party-details/{edit?}'], function () {

					Route::get('/', [
						'as'   => 'edit.additional.party.details',
						'uses' => 'CaptureRequirementsController@editAdditionalPartyDetails'
					]);

					Route::post('/', [
						'as'   => 'save.additional.party.details',
						'uses' => 'CaptureRequirementsController@saveAdditionalPartyDetails'
					]);
				});

				Route::group(['prefix' => 'services/{edit?}'], function () {
					Route::get('/', [
						'as'   => 'edit.services',
						'uses' => 'CaptureRequirementsController@editServices'
					]);

					Route::post('/', [
						'as'   => 'save.services',
						'uses' => 'CaptureRequirementsController@saveServices'
					]);
				});

				Route::post('/get-checkouordert-fields', [
					'as'   => 'get.checkout.fields',
					'uses' => 'CaptureRequirementsController@getTicketBookingCheckoutFields'
				]);

				Route::group(['prefix' => 'contact-info/{edit?}'], function () {

					Route::get('/', [
						'as'   => 'edit.contact.info',
						'uses' => 'CaptureRequirementsController@editContactInfo'
					]);

					Route::post('/', [
						'as'   => 'save.contact.info',
						'uses' => 'CaptureRequirementsController@saveContactInfo'
					]);
				});

				Route::group(['prefix' => 'status-info/{edit?}'], function () {

					Route::get('/', [
						'as'   => 'edit.status.info',
						'uses' => 'CaptureRequirementsController@editPartyStatusInfo'
					]);

					Route::post('/', [
						'as'   => 'save.status.info',
						'uses' => 'CaptureRequirementsController@savePartyStatusInfo'
					]);
				});
			});
		});

		Route::group(['prefix' => '{ticketId}/recommendations', 'as' => 'reco.'], function () {

			Route::get('/', [
				'as'   => 'selected.services',
				'uses' => 'RecommendationsController@getSelectedServices'
			]);

			Route::post('/{serviceId}/service-options', [
				'as'   => 'service.options',
				'uses' => 'RecommendationsController@getSelectedServiceOptions'
			]);

			Route::post('/option/edit', [
				'as'   => 'edit.option',
				'uses' => 'RecommendationsController@editServiceOption'
			]);

			Route::post('/selected/options', [
				'as'   => 'selected.options',
				'uses' => 'RecommendationsController@getSelectedOptions'
			]);

			Route::post('/send', [
				'as'   => 'send',
				'uses' => 'RecommendationsController@sendRecommendations'
			]);
		});

		Route::get('new', [
			'as'   => 'new',
			'uses' => 'TicketListController@showCreateTicket'
		]);

		Route::post('new', [
			'as'   => 'save',
			'uses' => 'TicketListController@saveTicket'
		]);

		Route::get('{id}', [
			'as'   => 'details',
			'uses' => 'TicketListController@showTicketDetails'
		]);

		Route::post('copy/{id}', [
			'as'   => 'copy.save',
			'uses' => 'TicketListController@saveCopyTicket'
		]);

		Route::get('copy/{id}', [
			'as'   => 'copy',
			'uses' => 'TicketListController@CopyTicket'
		]);

		Route::group(['prefix' => 'status/list/data'], function () {
			Route::post('{Id}', [
				'as'   => 'status.list',
				'uses' => 'TicketListController@LoadTicketListAjaxData'
			]);
		});

		/**
		 * Ticket Booking Details tab
		 *
		 * @author :: Vikash <vikash@evibe.in>
		 * @since  :: 7 Feb 2017
		 */
		Route::group(['prefix' => '{id}/', 'as' => 'details.'], function () {
			Route::get('benefit-services', [
				'as'   => 'benefit-services',
				'uses' => 'TicketDetailsController@showTicketBenefitServiceDetails'
			]);

			Route::get('finance', [
				'as'   => 'finance',
				'uses' => 'TicketDetailsController@showTicketFinanceDetails'
			]);

			Route::get('bookings', [
				'as'   => 'bookings',
				'uses' => 'TicketDetailsController@showTicketBookingDetails'
			]);

			Route::get('mappings', [
				'as'   => 'mappings',
				'uses' => 'TicketDetailsController@showTicketMappingDetails'
			]);

			// @todo: avail-checks-list
			Route::get('avail-checks', [
				'as'   => 'avail-checks',
				'uses' => 'TicketDetailsController@showAvailChecksDetails'
			]);

			Route::get('updates', [
				'as'   => 'updates',
				'uses' => 'TicketDetailsController@showTicketUpdateDetails'
			]);

			Route::get('information', [
				'as'   => 'info',
				'uses' => 'TicketDetailsController@showTicketInformation'
			]);

			Route::post('partyTimings', [
				'as'   => 'partyTimings',
				'uses' => 'TicketDetailsController@fetchTicketPartyTimings'
			]);
		});

		Route::put('{id}', [
			'as'   => 'update',
			'uses' => 'TicketListController@editTicket'
		]);

		Route::put('{id}/lead-status', [
			'as'   => 'update',
			'uses' => 'TicketDetailsController@editLeadStatus'
		]);

		Route::post('loadExtraData', [
			'as'   => 'loadExtraData',
			'uses' => 'TicketListController@loadExtraData'
		]);

		Route::put('{id}/cancel', [
			'as'   => 'cancel',
			'uses' => 'TicketListController@cancelTicket'
		]);

		Route::put('{id}/re-open', [
			'as'   => 'reopen',
			'uses' => 'TicketListController@reOpenTicket'
		]);

		Route::post('delete/{id}', [
			'as'   => 'delete',
			'uses' => 'TicketListController@deleteTicket'
		]);

		// ticket update section
		Route::post('{id}/status-update', [
			'as'   => 'status.update',
			'uses' => 'TicketUpdateController@saveStatusUpdate'
		]);

		Route::post('update/info/{id}', [
			'as'   => 'update.load',
			'uses' => 'TicketUpdateController@loadTicketUpdate'
		]);

		// ticket followup section
		Route::group(['prefix' => 'followup', 'as' => 'followup.'], function () {
			Route::post('save/{ticketId}', [
				'as'   => 'save',
				'uses' => 'TicketFollowupController@saveFollowup'
			]);
			Route::post('done/{id}', [
				'as'   => 'done',
				'uses' => 'TicketFollowupController@completeFollowup'
			]);
			Route::post('update/{id}', [
				'as'   => 'update',
				'uses' => 'TicketFollowupController@updateFollowup'
			]);
			Route::delete('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TicketFollowupController@deleteFollowup'
			]);

			Route::post('save/followup/{ticketId}', [
				'as'   => 'save.popup',
				'uses' => 'TicketFollowupController@savePopUpFollowUp'
			]);
		});

		// ticket booking section
		Route::group(['prefix' => 'bookings', 'as' => 'booking.'], function () {

			Route::get('{id}', [
				'as'   => 'show',
				'uses' => 'TicketBookingController@showEditTicketBooking'
			]);

			Route::put('{id}', [
				'as'   => 'update',
				'uses' => 'TicketBookingController@updateTicketBookingDetails'
			]);

			Route::delete('{id}', [
				'as'   => 'delete',
				'uses' => 'TicketBookingController@deleteTicketBooking'
			]);

			Route::put('{id}/partner-change', [
				'as'   => 'change-partner',
				'uses' => 'TicketBookingController@changeTicketBookingPartner'
			]);

			Route::get('cancel/{id}', [
				'as'   => 'cancellation-refund',
				'uses' => 'TicketBookingController@fetchCancellationCharges'
			]);

			Route::put('cancel/{id}', [
				'as'   => 'cancel',
				'uses' => 'TicketBookingController@cancelTicketBooking'
			]);

			Route::get('btime/{id}', [
				'as'   => 'btime.show',
				'uses' => 'TicketBookingController@showEditTicketBookingBeforeTime'
			]);

			Route::post('btime/{id}', [
				'as'   => 'btime.update',
				'uses' => 'TicketBookingController@editTicketBookingBeforeTime'
			]);

			Route::get('resend-cancel-receipt/{id}', [
				'as'   => 'resend-cancel-receipt',
				'uses' => 'TicketBookingController@resendBookingCancellationReceipts'
			]);

			Route::post('update-partner/{id}', [
				'as'   => 'update-partner',
				'uses' => 'TicketBookingController@updateBookingPartner'
			]);

			Route::post('confirm-partner/{id}', [
				'as'   => 'confirm-partner',
				'uses' => 'TicketBookingController@confirmPartner'
			]);

			Route::post('mark-star-order/{id}', [
				'as'   => 'mark-star-order',
				'uses' => 'TicketBookingController@markStarOrder'
			]);

			// ticket booking gallery routes (upload, delete)
			Route::group(['prefix' => 'image', 'as' => 'image.'], function () {
				Route::post('{bookingId}/upload', [
					'as'   => 'upload',
					'uses' => 'TicketBookingController@uploadImages'
				]);

				Route::get('delete/{id}', [
					'as'   => 'delete',
					'uses' => 'TicketBookingController@deleteImage'
				]);
			});

			Route::post('refund/{id}', [
				'as'   => 'refund.save',
				'uses' => 'TicketBookingController@saveRefundAmount'
			]);

			Route::post('update-planner/{id}', [
				'as'   => 'update-planner',
				'uses' => 'TicketBookingController@updatePlanner'
			]);
		});

		// ticket order related button (buttons above booking section)
		Route::group(['prefix' => 'order', 'as' => 'orders.'], function () {
			Route::post('{id}/confirm', [
				'as'   => 'confirm',
				'uses' => 'TicketOrderController@sendOrderProcessAlerts'
			]);

			Route::post('{id}/receipt', [
				'as'   => 'receipt',
				'uses' => 'TicketOrderController@sendReceipt'
			]);

			Route::post('{id}/advance', [
				'as'   => 'received/advance',
				'uses' => 'TicketOrderController@newAdvancePaid'
			]);

			Route::post('{id}/update-receipt', [
				'as'   => 'updatedReceipt',
				'uses' => 'TicketOrderController@sendUpdatedReceipt'
			]);

			Route::post('{id}/service-booked', [
				'as'   => 'serviceBooked',
				'uses' => 'TicketOrderController@sendServiceBookedMail'
			]);
		});

		// auto booking action
		Route::group(['prefix' => 'auto', 'as' => 'auto.booking.'], function () {
			Route::post('{id}/confirm', [
				'as'   => 'confirm',
				'uses' => 'TicketOrderController@confirmAutoBooking'
			]);

			Route::post('{id}/cancel', [
				'as'   => 'cancel',
				'uses' => 'TicketOrderController@cancelAutoBooking'
			]);

			Route::post('{id}/venue-deal/cancel', [
				'as'   => 'vd.cancel',
				'uses' => 'TicketOrderController@cancelVenueDealAutoBooking'
			]);

			Route::post('{id}/cake/cancel', [
				'as'   => 'cake.cancel',
				'uses' => 'TicketOrderController@cancelCakeAutoBooking'
			]);

			Route::post('{id}/resend', [
				'as'   => 'resend',
				'uses' => 'TicketOrderController@resendAutoBookingReceipt'
			]);

			Route::post('{id}/service-confirm', [
				'as'   => 'service.confirm',
				'uses' => 'TicketOrderController@confirmServiceAutoBooking'
			]);
		});

		// ticket mapping section
		Route::group(['prefix' => 'mapping', 'as' => 'mapping.'], function () {

			Route::post('{id}', [
				'as'   => 'new',
				'uses' => 'TicketMappingController@newTicketMapping'
			]);

			Route::get('{id}/finalize', [
				'as'   => 'finalize.show',
				'uses' => 'TicketMappingController@showTicketMapBooking'
			]);

			Route::post('{id}/finalize', [
				'as'   => 'finalize',
				'uses' => 'TicketMappingController@newTicketMapBooking'
			]);

			Route::post('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TicketMappingController@deleteTicketMapping'
			]);

			Route::post('{ticketId}/type-ticket-options', [
				'as'   => 'get.options',
				'uses' => 'TicketMappingController@getOptionsByTypeTicket'
			]);
		});

		//avail-checks section
		Route::group(['prefix' => '{id}/avail-checks', 'as' => 'avail-checks.'], function () {
			Route::post('get-options', [
				'as'   => 'get-options',
				'uses' => 'TicketAvailCheckController@getOptionsByTypeTicket'
			]);
			Route::post('fetch-info', [
				'as'   => 'fetch-info',
				'uses' => 'TicketAvailCheckController@fetchAvailCheckOptionInfo'
			]);
			Route::post('create-check', [
				'as'   => 'create-check',
				'uses' => 'TicketAvailCheckController@createAvailCheck'
			]);
		});

		// other action buttons of ticket
		Route::group(['prefix' => 'action', 'as' => 'action.'], function () {

			//custom quote button
			Route::post('{id}/quote/details', [
				'as'   => 'quote.details',
				'uses' => 'TicketActionButtonController@getDetailsForQuote'
			]);

			Route::post('{id}/quote/send', [
				'as'   => 'quote.send',
				'uses' => 'TicketActionButtonController@sendCustomQuote'
			]);

			//recommendation email
			Route::post('{id}/email-reco', [
				'as'   => 'reco-email',
				'uses' => 'TicketActionButtonController@sendRecommendationEmail'
			]);

			//exchange contact
			Route::post('{id}/exchange-contacts', [
				'as'   => 'exchange_contact',
				'uses' => 'TicketActionButtonController@exchangeContacts'
			]);
			//venue availability check
			Route::post('ask-availability/{ticketId}', [
				'as'   => 'venue-availability',
				'uses' => 'TicketActionButtonController@askVenueAvailability'
			]);
			//auto cancellation email
			Route::post('{id}/toggleAutoCancellationMail', [
				'as'   => 'autoCancellationEmail',
				'uses' => 'TicketActionButtonController@toggleAutoCancellationMail'
			]);
			// Any email
			Route::post('{id}/any-email/send', [
				'as'   => 'any.email.send',
				'uses' => 'TicketActionButtonController@sendAnyCustomEmail'
			]);
			// No response alert
			Route::post('{id}/no-response/send', [
				'as'   => 'ticket.noresponse.send',
				'uses' => 'TicketActionButtonController@sendNoResponseAlert'
			]);
			//cancellation email
			Route::post('{id}/sendCancellationMailToCustomer', [
				'as'   => 'cancellationEmail',
				'uses' => 'TicketActionButtonController@sendCancellationMailToCustomer'
			]);

			// get attachments for notifications (including reply)
			Route::post('showattachments', [
				'as'   => 'attachement.show',
				'uses' => 'TicketActionButtonController@getAttachments'
			]);

			//update more info
			Route::post('{id}/more-info', [
				'uses' => 'TicketActionButtonController@setTicketMoreInfo'
			]);
			Route::put('{id}/more-info', [
				'uses' => 'TicketActionButtonController@setTicketMoreInfo'
			]);

			// resolve post booking customer queries
			Route::post('resolve-query/{queryId}', [
				'as'   => 'resolve-query',
				'uses' => 'TicketActionButtonController@resolveCustomerQuery'
			]);

		});

		// ticket special benefits section
		Route::group(['prefix' => 'benefit-services', 'as' => 'benefit-services.'], function () {

			Route::group(['prefix' => 'e-invite', 'as' => 'e-invite.'], function () {

				Route::get('generate-rsvp/{ticketId}', [
					'as'   => 'generate-rsvp',
					'uses' => 'TicketBenefitServicesController@generateRSVPLink'
				]);

				Route::post('send/{ticketId}', [
					'as'   => 'send',
					'uses' => 'TicketBenefitServicesController@sendEInvite'
				]);

				Route::post('resend/{ticketId}', [
					'as'   => 'resend',
					'uses' => 'TicketBenefitServicesController@resendEInvite'
				]);
			});
		});

	});

	Route::group(['middleware' => 'authorize.vendors',
	              'prefix'     => 'vendors',
	              'namespace'  => 'Vendor',
	              'as'         => 'vendor.',
	             ], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'VendorsController@showList'
		]);

		Route::group(['prefix' => 'view/{id}',], function () {
			Route::get('/', [
				'as'   => 'view',
				'uses' => 'VendorsController@showDetails'
			]);
		});

		Route::post('document/delete/{documentId}', [
			'as'   => 'file.delete',
			'uses' => 'VendorsController@deleteDocument'
		]);

		Route::get('edit/{id}', [
			'as'   => 'edit.show',
			'uses' => 'VendorsController@showEditForm'
		]);

		Route::post('edit/{id}', [
			'as'   => 'edit.save',
			'uses' => 'VendorsController@saveVendorEdit'
		]);
		Route::get('delete/{id}', [
			'as'   => 'delete',
			'uses' => 'VendorsController@deleteVendor'
		]);

		Route::post('update/{id}', [
			'as'   => 'update.save',
			'uses' => 'VendorsController@saveExitingVendorUpdate'
		]);

		Route::post('followup/{id}', [
			'as'   => 'followup.save',
			'uses' => 'VendorsController@saveExistingVendorFollowup'
		]);

		Route::post('followup/complete/{id}', [
			'as'   => 'followup.complete',
			'uses' => 'VendorsController@FollowupMarkComplete'
		]);

		Route::post('followup/update/{id}', [
			'as'   => 'followup.update',
			'uses' => 'VendorsController@updateFollowup'
		]);

		Route::delete('followup/delete/{id}', [
			'as'   => 'followup.delete',
			'uses' => 'VendorsController@deleteFollowup'
		]);

		Route::get('{id}/action/activate', [
			'as'   => 'actions.activate',
			'uses' => 'VendorsController@activateVendor'
		]);

		Route::get('{id}/action/deactivate', [
			'as'   => 'actions.deactivate',
			'uses' => 'VendorsController@deActivateVendor'
		]);

		Route::put('{id}/lead-status', [
			'as'   => 'update',
			'uses' => 'VendorSignupController@editLeadStatus'
		]);

		Route::group(['prefix' => 'new', 'as' => 'new.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'VendorSignupController@showNewVendorList'
			]);

			Route::get('add', [
				'as'   => 'add',
				'uses' => 'VendorSignupController@showAddNewForm'
			]);

			Route::post('save', [
				'as'   => 'save',
				'uses' => 'VendorSignupController@saveNewVendor'
			]);
			Route::get('view/{id}', [
				'as'   => 'view',
				'uses' => 'VendorSignupController@showNewVendorDetails'
			]);

			Route::get('edit/{id}', [
				'as'   => 'edit',
				'uses' => 'VendorSignupController@showEditNewVendor'
			]);

			Route::post('edit/save/{id}', [
				'as'   => 'update',
				'uses' => 'VendorSignupController@updateNewVendor'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'VendorSignupController@deleteNewVendor'
			]);

			Route::post('confirm/{id}', [
				'as'   => 'confirm',
				'uses' => 'VendorSignupController@confirmVendor'
			]);

			Route::post('update/{id}', [
				'as'   => 'update.save',
				'uses' => 'VendorSignupController@saveNewVendorUpdate'
			]);
			Route::post('followup/{id}', [
				'as'   => 'followup.save',
				'uses' => 'VendorSignupController@saveNewVendorFollowup'
			]);

			Route::post('followup/complete/{id}', [
				'as'   => 'followup.complete',
				'uses' => 'VendorSignupController@markCompleteNewFollowup'
			]);

			Route::post('followup/update/{id}', [
				'as'   => 'followup.update',
				'uses' => 'VendorSignupController@updateFollowup'
			]);

			Route::delete('followup/delete/{id}', [
				'as'   => 'followup.delete',
				'uses' => 'VendorSignupController@deleteFollowup'
			]);
		});

	});

	/**
	 * Venue Halls Begin [Venue halls belong to venue. Each venue may have multiple halls]
	 */
	Route::group(['namespace' => 'VenueHall', 'middleware' => 'authorize.venue-halls', 'prefix' => 'venue-halls', 'as' => 'venue-halls.'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'VenueHallController@showList'
		]);

		Route::group(['prefix' => 'new', 'as' => 'new.'], function () {
			Route::get('view/{venueId}', [
				'as'   => 'view',
				'uses' => 'VenueHallController@showAddNewHall'
			]);

			Route::post('save/{venueId}', [
				'as'   => 'save',
				'uses' => 'VenueHallController@saveNewHall'
			]);
		});

		Route::group(['prefix' => '{id}', 'as' => 'profile.'], function () {
			Route::get('view', [
				'as'   => 'view',
				'uses' => 'VenueHallController@showDetails'
			]);

			Route::get('delete', [
				'as'   => 'delete',
				'uses' => 'VenueHallController@deleteVenueHall'
			]);

			Route::group(['prefix' => 'edit', 'as' => 'edit.'], function () {
				Route::get('show', [
					'as'   => 'show',
					'uses' => 'VenueHallController@showVenueHallEdit'
				]);

				Route::post('save', [
					'as'   => 'save',
					'uses' => 'VenueHallController@saveVenueHallEdit'
				]);
			});

			Route::group(['prefix' => 'gallery', 'as' => 'gallery.'], function () {
				Route::post('/', [
					'as'   => 'upload',
					'uses' => 'VenueHallController@uploadImages',
				]);

				Route::group(['prefix' => '{galleryId}'], function () {
					Route::get('delete', [
						'as'   => 'delete',
						'uses' => 'VenueHallController@deleteImage',
					]);

					Route::post('edit', [
						'as'   => 'edit',
						'uses' => 'VenueHallController@saveImageEdit',
					]);

					Route::get('set', [
						'as'   => 'set',
						'uses' => 'VenueHallController@setAsProfilePic'
					]);

					Route::get('unset', [
						'as'   => 'unset',
						'uses' => 'VenueHallController@unsetAsProfilePic'
					]);
				});

			});
		});

	});

	/**
	 * Venues Begin
	 */
	Route::group(['namespace' => 'Venue'], function () {
		Route::group(['middleware' => 'authorize.venues', 'prefix' => 'venues'], function () {
			Route::get('/', [
				'as'   => 'venues',
				'uses' => 'VenuesController@getIndex'
			]);

			Route::get('hall/{id}', [
				'as'   => 'venues.hall',
				'uses' => 'VenuesController@showVenueHall'
			]);

			Route::group(['prefix' => 'view/{id}', 'as' => 'venues.',], function () {
				Route::get('/', [
					'as'   => 'view',
					'uses' => 'VenuesController@showVenueDetails'
				]);
			});

			Route::post('document/delete/{partnerId}', [
				'as'   => 'file.delete',
				'uses' => 'VenuesController@deleteDocument'
			]);

			Route::get('list/', [
				'as'   => 'venues.list',
				'uses' => 'VenuesController@showVenueList'
			]);

			Route::get('new', [
				'as'   => 'venues.new',
				'uses' => 'VenuesController@getNew'
			]);

			Route::post('create-venues', [
				'as'   => 'venues.create-venues',
				'uses' => 'VenuesController@postCreateVenues'
			]);

			Route::get('delete/{id}', [
				'as'   => 'venues.delete',
				'uses' => 'VenuesController@deleteVenue'
			]);

			Route::get('edit/{id}', [
				'as'   => 'venues.edit',
				'uses' => 'VenuesController@getEdit'
			]);

			Route::post('update/{id}', [
				'as'   => 'venues.update',
				'uses' => 'VenuesController@postUpdateInfo'
			]);

			Route::get('hall/delete/{id}', [
				'as'   => 'venues.hall.delete',
				'uses' => 'VenuesController@deleteHall'
			]);

			Route::get('active/{id}', [
				'uses' => 'VenuesController@setVenueActive'
			]);

			Route::get('inacive/{id}', [
				'uses' => 'VenuesController@setVenueInActive'
			]);
		});

		// Ext Reviews
		Route::group(['prefix' => 'ext-ratings'], function () {
			Route::get('/{id}', [
				'as'   => 'venueExtRatings',
				'uses' => 'VenueExtRatingsController@viewRatings'
			]);

			Route::get('edit/{venue_id}', [
				'as'   => 'venueExtRatingsEdit',
				'uses' => 'VenueExtRatingsController@editRatings'
			]);

			Route::post('update/{venue_id}', [
				'as'   => 'venueExtRatingsUpdate',
				'uses' => 'VenueExtRatingsController@updateRatings'
			]);

			Route::post('delete', [
				'as'   => 'venueExtRatingsDelete',
				'uses' => 'VenueExtRatingsController@deleteRating'
			]);

		});
	});

	/*
	 * Common route for both vendors and venues for uploading documents
	 */

	Route::group(['prefix' => 'partner/{partnerId}/{partnerTypeId}/upload-vendor-documents', 'namespace' => 'Partner'], function () {

		Route::get('/', [
			'as'   => 'upload.documents',
			'uses' => 'PartnerProfileController@uploadPartnerDocuments'
		]);

		Route::post('/', [
			'as'   => 'save.documents',
			'uses' => 'PartnerProfileController@saveUploadedDocuments'
		]);
	});

	/**
	 *
	 * Avail Check
	 */
	Route::group(['prefix' => 'avail-check', 'namespace' => 'AvailCheck', 'as' => "avail.check."], function () {
		Route::get('', [
			'as'   => 'initialize',
			'uses' => 'AvailCheckController@showInitialPage'
		]);

		Route::post('/venue/{partnerTypeId}/{partnerCode}', [
			'as'   => 'partner.venue.data',
			'uses' => 'AvailCheckController@showPartnerVenueData'
		]);

		Route::post('add/{partnerCode}', [
			'as'   => 'partner.add.slot',
			'uses' => 'AvailCheckController@savePartnerUnavailableSlot'
		]);

		Route::post('edit/{partnerCode}', [
			'as'   => 'partner.edit.slot',
			'uses' => 'AvailCheckController@editPartnerUnavailableSlot'
		]);

		Route::post('check/{partnerCode}', [
			'as'   => 'partner.check.slot',
			'uses' => 'AvailCheckController@checkPartnerUnavailableSlot'
		]);

		Route::get('delete/{partnerCode}/{date}/{slot}', [
			'as'   => 'partner.delete.slot',
			'uses' => 'AvailCheckController@deletePartnerUnavailableSlot'
		]);

		Route::get('{partnerTypeId}/{partnerCode}', [
			'as'   => 'partner.data',
			'uses' => 'AvailCheckController@showPartnerData'
		]);
	});

	/**
	 *
	 * Packages
	 */
	Route::group(['middleware' => 'authorize.packages', 'prefix' => 'packages', 'namespace' => 'Package'], function () {
		Route::get('list', [
			'as'   => 'packages.all.list',
			'uses' => 'PackageController@showAllPackagesList'
		]);

		Route::get('vendor', [
			'as'   => 'packages.vendor.list',
			'uses' => 'PackageController@showVendorPackagesList'
		]);

		Route::get('venue', [
			'as'   => 'packages.venue.list',
			'uses' => 'PackageController@showVenuePackagesList'
		]);

		Route::get('artist', [
			'as'   => 'packages.artist.list',
			'uses' => 'PackageController@showArtistPackagesList'
		]);

		Route::get('vendor/view/{id}', [
			'as'   => 'package.vendor.view',
			'uses' => 'PackageController@viewVendorPackageDetails'
		]);

		Route::get('venue/view/{id}', [
			'as'   => 'package.venue.view',
			'uses' => 'PackageController@viewVenuePackageDetails'
		]);

		Route::get('artist/view/{id}', [
			'as'   => 'package.artist.view',
			'uses' => 'PackageController@viewArtistPackageDetails'
		]);

		Route::get('view/{id}', [
			'as'   => 'package.view',
			'uses' => 'PackageController@viewPackageDetails'
		]);

		Route::get('delete/{id}', [
			'as'   => 'package.delete',
			'uses' => 'PackageController@getDelete'
		]);
		Route::get('activate/{id}', [
			'as'   => 'package.activate',
			'uses' => 'PackageController@activate'
		]);
		Route::get('dectivate/{id}', [
			'as'   => 'package.deactivate',
			'uses' => 'PackageController@deActivate'
		]);

		Route::get('type-ticket', [
			'as'   => 'packages.typeTicket',
			'uses' => 'PackageController@getTypeTickets'
		]);

		Route::post('type-ticket/{id}/update', [
			'as'   => 'packages.typeTicketUpdate',
			'uses' => 'PackageController@updatePackageTypeTicket'
		]);

		// new package
		Route::group(['prefix' => 'new'], function () {
			Route::get('first', [
				'as'   => 'package.new.show',
				'uses' => 'CreatePackageController@showCreateNewPackagePage'
			]);

			Route::get('second', [
				'as'   => 'package.new.show.details',
				'uses' => 'CreatePackageController@showFieldsBasedOnProvider'
			]);
		});

		Route::post('save', [
			'as'   => 'package.new.save',
			'uses' => 'CreatePackageController@savePackageData'
		]);

		Route::get('edit/{packageId}', [
			'as'   => 'package.edit',
			'uses' => 'CreatePackageController@editPackage'
		]);

		Route::get('/provider/edit/{packageId}', [
			'as'   => 'package.provider.edit',
			'uses' => 'PackageController@editPackageProvider'
		]);

		Route::post('/provider/edit/{packageId}', [
			'as'   => 'package.provider.edit',
			'uses' => 'PackageController@saveEditPackageProvider'
		]);

		// Tags
		Route::group(['prefix' => 'tags'], function () {
			Route::post('{pkgId}/add', [
				'as'   => 'package.tags.add',
				'uses' => 'PackageController@addPackageTag'
			]);

			Route::get('{pkgId}/{tagId}/delete', [
				'as'   => 'package.tag.delete',
				'uses' => 'PackageController@deletePackageTag'
			]);

			Route::post('{pkgId}/surprise/add', [
				'as'   => 'package.tags.surprises.add',
				'uses' => 'PackageController@addSurprisePackageTag'
			]);
		});

		// Gallery
		Route::group(['prefix' => 'gallery/{pkgId}'], function () {
			Route::post('upload', [
				'as'   => 'package.gallery.upload',
				'uses' => 'PackageController@uploadPackageGallery'
			]);

			Route::get('{galleryId}/set', [
				'as'   => 'package.gallery.set',
				'uses' => 'PackageController@setAsProfile'
			]);

			Route::get('{galleryId}/unset', [
				'as'   => 'package.gallery.unset',
				'uses' => 'PackageController@unsetAsProfile'
			]);

			Route::get('{galleryId}/delete', [
				'as'   => 'package.gallery.delete',
				'uses' => 'PackageController@deletePackageGallery'
			]);

			Route::post('{galleryId}/title', [
				'as'   => 'package.gallery.title',
				'uses' => 'PackageController@saveImgTitle'
			]);
		});

		///**
		// * Edit Packages in pro
		// * By Vikash <vikash@evibe.in> at 9 Jun 2016.
		// */
		//
		//Route::get('autoLogin/{id}', [
		//	'as'   => 'package.edit',
		//	'uses' => 'PackageController@editPackage'
		//]);
	});

	/**
	 * Trends
	 */
	Route::group(['middleware' => 'authorize.trends', 'namespace' => 'Trend'], function () {
		//Route::controller('trends', 'TrendsController');
	});

	Route::group(['middleware' => 'authorize.trends', 'namespace' => 'Trend', 'prefix' => 'trends'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'TrendsController@getIndex'
		]);
		Route::get('view/{id}', [
			'as'   => 'view',
			'uses' => 'TrendsController@getView'
		]);
		Route::get('new', [
			'as'   => 'new',
			'uses' => 'TrendsController@getNew'
		]);
		Route::post('create-trend', [
			'as'   => 'create-trend',
			'uses' => 'TrendsController@postCreateTrend'
		]);
		Route::get('edit/{id}', [
			'as'   => 'edit',
			'uses' => 'TrendsController@getEdit'
		]);
		Route::post('edit-trend', [
			'as'   => 'edit',
			'uses' => 'TrendsController@postEditTrend'
		]);
		Route::get('deactivate/{id}', [
			'as'   => 'deactivate',
			'uses' => 'TrendsController@getDeactivate'
		]);
		Route::get('activate/{id}', [
			'as'   => 'deactivate',
			'uses' => 'TrendsController@getActivate'
		]);
		Route::get('delete/{id}', [
			'as'   => 'deactivate',
			'uses' => 'TrendsController@getDelete'
		]);
		Route::post('upload-image/{id}', [
			'as'   => 'upload-image',
			'uses' => 'TrendsController@postUploadImage'
		]);
		Route::get('profile-pic/{imageId}/{id}', [
			'as'   => 'profile-pic',
			'uses' => 'TrendsController@getProfilePic'
		]);
		Route::post('image-title/{imageId}/{id}', [
			'as'   => 'image-title',
			'uses' => 'TrendsController@postImageTitle'
		]);
		Route::get('delete-pic/{imageId}/{id}', [
			'as'   => 'delete-pic',
			'uses' => 'TrendsController@getDeletePic'
		]);

	});

	/**
	 * Image Search
	 */
	Route::group(['middleware' => 'authorize.images', 'prefix' => 'images-search', 'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'images.search',
			'uses' => 'ImageSearchController@showIndex'
		]);
	});

	/**
	 * Booking concept console for marketing and digital marketing team
	 */
	Route::group(['middleware' => 'authorize.booking_concept', 'prefix' => 'booking-concept', 'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'booking.concept',
			'uses' => 'BookingConceptController@showBookings'
		]);

		Route::get('info/{id}', [
			'as'   => 'booking.concept.info',
			'uses' => 'BookingConceptController@showBookingInfo'
		]);
	});

	Route::group(['middleware' => 'authorize.delivery_images', 'as' => 'delivery.image', 'prefix' => 'delivery-images', 'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'show',
			'uses' => 'DeliveryImageController@showDeliveryImages'
		]);
		Route::post('/reject-image', [
			'as'   => 'reject',
			'uses' => 'DeliveryImageController@rejectImage'
		]);
		Route::post('/accept-image', [
			'as'   => 'accept',
			'uses' => 'DeliveryImageController@acceptImage'
		]);
		Route::post('/set-profile-image', [
			'as'   => 'profile.image',
			'uses' => 'DeliveryImageController@setAsProfile'
		]);
		Route::post('/populate-map-id', [
			'as'   => 'populate.map.id',
			'uses' => 'DeliveryImageController@populateMapId'
		]);
		Route::post('/reject-select-image', [
			'as'   => 'reject.selected.image',
			'uses' => 'DeliveryImageController@rejectSelectedImage'
		]);
	});

	/**
	 * Reviews
	 */
	Route::group(['middleware' => 'authorize.reviews',
	              'prefix'     => 'reviews',
	              'as'         => 'reviews.',
	              'namespace'  => 'Review'], function () {

		Route::get('new', [
			'as'   => 'new',
			'uses' => 'ReviewsController@addNewReview'
		]);

		Route::post('new/save', [
			'as'   => 'new.save',
			'uses' => 'ReviewsController@saveNewReview'
		]);

		Route::post('edit', [
			'as'   => 'edit',
			'uses' => 'ReviewsController@editReview'
		]);

		Route::get('delete/{id}', [
			'as'   => 'review.delete',
			'uses' => 'ReviewsController@deleteReview'
		]);

		Route::post('add/partner-reply', [
			'as'   => 'add.partner.review',
			'uses' => 'ReviewsController@addPartnerReply'
		]);

		Route::post('delete/partner-reply', [
			'as'   => 'delete.partner.review',
			'uses' => 'ReviewsController@deletePartnerReply'
		]);

		Route::post('accept/review/{id}', [
			'as'   => 'accept.review',
			'uses' => 'ReviewsController@acceptReview'
		]);

		Route::post('reject/review/{id}', [
			'as'   => 'reject.review',
			'uses' => 'ReviewsController@rejectReview'
		]);

		Route::get('{status}', [
			'as'   => 'list',
			'uses' => 'ReviewsController@getReviewsList'
		]);
	});

	/**
	 * Website Errors
	 */
	Route::group(['middleware' => 'authorize.errors', 'namespace' => 'App'], function () {
		Route::get('/errors', 'SiteErrorsController@showErrors');

		// Disabling delete all errors button
		//Route::get('errors/delete',
		//           [
		//	           'as'   => 'error.delete',
		//	           'uses' => 'SiteErrorsController@deleteErrors'
		//           ]);

		Route::get('delete/{id}', [
			'as'   => 'error.individual.delete',
			'uses' => 'SiteErrorsController@individualDelete'
		]);
	});

	/**
	 * Website Monitoring
	 */
	Route::group(['prefix' => 'monitor', 'middleware' => 'authorize.monitor', 'namespace' => 'App', 'as' => 'monitor.'], function () {

		Route::get('payments', [
			'as'   => 'payments',
			'uses' => 'MonitoringController@showPayments'
		]);
	});

	/**
	 * Nearby areas
	 */
	Route::group(['middleware' => 'authorize.nearby-areas', 'prefix' => 'nearby-areas', 'namespace' => 'Util'], function () {
		Route::get('/', ['uses' => 'UtilitiesController@showNearbyAreas']);

		Route::get('{cityId}/{areaId}', ['uses' => 'UtilitiesController@getNearbyAreas']);

		Route::get('delete', ['uses' => 'UtilitiesController@deleteNearbyArea']);

		Route::post('{areaId}/save', ['uses' => 'UtilitiesController@saveNearbyAreas']);
	});

	/**
	 * Cakes
	 *
	 * @author Anji <anji@evibe.in>
	 * @since  1 June 2015
	 */
	Route::group(['middleware' => 'authorize.cakes', 'prefix' => 'cakes', 'namespace' => 'Cake', 'as' => 'cakes.'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'CakeController@showCakesList'
		]);

		Route::get('new', [
			'as'   => 'new',
			'uses' => 'CakeController@showCreateNewCakePage'
		]);

		// create new cake
		Route::group(['as' => 'new.'], function () {
			Route::get('info/create', [
				'as'   => 'info',
				'uses' => 'CakeController@showCreateNewCakePage'
			]);

			Route::post('new/create', [
				'as'   => 'create',
				'uses' => 'CakeController@createNewCake'
			]);

			Route::get('{id}/gallery/view', [
				'as'   => 'gallery',
				'uses' => 'CakeController@showCreateGallery'
			]);

			Route::get('{id}/category/{catId}/create', [
				'as'   => 'category.create',
				'uses' => 'CakeController@showCakeCategoryCreate'
			]);

			Route::post('{id}/category/{catId}/create', [
				'as'   => 'category.save',
				'uses' => 'CakeController@saveCakeCategoryValues'
			]);
		});

		Route::get('{id}/category/{catId}/view', [
			'as'   => 'category.view',
			'uses' => 'CakeController@showCategoryValues'
		]);

		Route::get('edit/{cakeId}', [
			'as'   => 'edit',
			'uses' => 'CakeController@showEditCake'
		]);

		Route::post('update/{cakeId}', [
			'as'   => 'update',
			'uses' => 'CakeController@updateCakeDetails'
		]);

		Route::get('activate/{cakeId}', [
			'as'   => 'activate',
			'uses' => 'CakeController@activateCake'
		]);

		Route::get('deactivate/{cakeId}', [
			'as'   => 'deactivate',
			'uses' => 'CakeController@deactivateCake'
		]);

		Route::get('delete/{cakeId}', [
			'as'   => 'delete',
			'uses' => 'CakeController@deleteCake'
		]);

		Route::group(['prefix' => '{cakeId}'], function () {
			Route::get('info', [
				'as'   => 'view.info',
				'uses' => 'CakeController@showCakeDetailsInfo'
			]);

			Route::group(['prefix' => 'gallery'], function () {
				Route::post('upload', [
					'as'   => 'gallery.upload',
					'uses' => 'CakeController@uploadImages'
				]);

				Route::get('{imageId}/unset', [
					'as'   => 'gallery.unset',
					'uses' => 'CakeController@unsetImageAsProfilePic'
				]);

				Route::get('{imageId}/set', [
					'as'   => 'gallery.set',
					'uses' => 'CakeController@setImageAsProfilePic'
				]);

				Route::post('{imageId}/title', [
					'as'   => 'gallery.title',
					'uses' => 'CakeController@editCakeTitle'
				]);

				Route::get('{imageId}/delete', [
					'as'   => 'gallery.delete',
					'uses' => 'CakeController@deleteImage'
				]);
			});

			Route::group(['prefix' => 'tags'], function () {
				Route::post('new', [
					'as'   => 'tags.new',
					'uses' => 'CakeController@addNewTag'
				]);

				Route::get('{cakeTagId}/delete', [
					'as'   => 'tags.delete',
					'uses' => 'CakeController@deleteCakeTag'
				]);
			});

			Route::group(['prefix' => 'events', 'as' => 'events.'], function () {
				Route::post('new', [
					'as'   => 'new',
					'uses' => 'CakeController@addNewEventSupported'
				]);

				Route::get('{cakeEventId}/delete', [
					'as'   => 'delete',
					'uses' => 'CakeController@deleteCakeEvent'
				]);
			});
		});
	});

	/**
	 * Decor
	 */
	Route::group(['prefix' => 'decors', 'namespace' => 'Decor'], function () {
		Route::get('/', [
			'as'   => 'decors.list',
			'uses' => 'DecorController@showDecorStylesList'
		]);

		// new decor
		Route::group(['prefix' => 'new'], function () {
			Route::get('/', [
				'as'   => 'decor.new.show',
				'uses' => 'DecorController@showCreateNewDecorPage'
			]);

			Route::post('save', [
				'as'   => 'decor.new.save',
				'uses' => 'DecorController@saveNewDecor'
			]);
		});

		Route::group(['prefix' => '{id}'], function () {
			// info
			Route::group([], function () {
				Route::get('info', [
					'as'   => 'decor.info.view',
					'uses' => 'DecorController@showDecorDetails'
				]);

				Route::get('edit', [
					'as'   => 'decor.info.edit',
					'uses' => 'DecorController@showDecorEditInfo'
				]);

				Route::post('update', [
					'as'   => 'decor.info.update',
					'uses' => 'DecorController@updateDecorInfoDetails'
				]);
			});

			// gallery
			Route::group(['prefix' => 'gallery'], function () {
				Route::post('upload', [
					'as'   => 'decor.gallery.upload',
					'uses' => 'DecorController@uploadDecorGallery'
				]);

				Route::get('{galleryId}/set', [
					'as'   => 'decor.gallery.set',
					'uses' => 'DecorController@setAsProfile'
				]);

				Route::get('{galleryId}/unset', [
					'as'   => 'decor.gallery.unset',
					'uses' => 'DecorController@unsetAsProfile'
				]);

				Route::get('{galleryId}/delete', [
					'as'   => 'decor.gallery.delete',
					'uses' => 'DecorController@deleteDecorGallery'
				]);

				Route::post('{galleryId}/title', [
					'as'   => 'decor.gallery.title',
					'uses' => 'DecorController@saveImgTitle'
				]);
			});

			// tags
			Route::group(['prefix' => 'tags'], function () {
				Route::post('add', [
					'as'   => 'decor.tags.add',
					'uses' => 'DecorController@addDecorTag'
				]);

				Route::get('{tagId}/delete', [
					'as'   => 'decor.tag.delete',
					'uses' => 'DecorController@deleteDecorTag'
				]);
			});

			// actions
			Route::group(['prefix' => 'action'], function () {
				Route::get('delete', [
					'as'   => 'decor.actions.delete',
					'uses' => 'DecorController@deleteDecor'
				]);

				Route::get('deactivate', [
					'as'   => 'decor.actions.deactivate',
					'uses' => 'DecorController@deactivateDecor'
				]);

				Route::get('activate', [
					'as'   => 'decor.actions.activate',
					'uses' => 'DecorController@activateDecor'
				]);
			});

		});
	});

	/*
	 * Reordering listings
	 *
	 * @author Vikash <vikash@evibe.in>
	 * @since  13 January 2016
	 */
	Route::group(['prefix' => 'reorder', 'as' => 'reorder.', 'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'filter',
			'uses' => 'ReorderingController@showFilter'
		]);

		Route::post('next', [
			'as'   => 'next',
			'uses' => 'ReorderingController@nextRequest'
		]);

		Route::get('list/{cityId}/{occasionId}/{pageId}/{tagId?}', [
			'as'   => 'show',
			'uses' => 'ReorderingController@showItemsList'
		]);

		Route::post('save', [
			'as'   => 'save',
			'uses' => 'ReorderingController@postOrder'
		]);

		Route::post('get-tags/{occasionId}/{pageId}', [
			'as'   => 'tags.get',
			'uses' => 'ReorderingController@getTags'
		]);

	});

	/*
	 * @author Vikash
	 * @since 30 January 2016
	 *
	 * save,delete and Edit the internal questions
	 */
	Route::group(['namespace' => 'App'], function () {
		Route::post('iq/{mapId}/{mapTypeId}', [
			'as'   => 'iq.add',
			'uses' => 'AppController@saveIq'
		]);

		Route::get('iq/delete/{id}', [
			'as'   => 'iq.delete',
			'uses' => 'AppController@deleteIq'
		]);

		Route::post('edit/iq/{id}', [
			'as'   => 'iq.edit',
			'uses' => 'AppController@editIq'
		]);
	});

	// Common facts, prereq, terms routes
	Route::group(['prefix' => 'content', 'as' => 'content.', 'namespace' => 'Content'], function () {
		Route::get('/', [
			'as'   => 'show',
			'uses' => 'PrereqFactsTermsController@showContent']);

		Route::get('terms', [
			'as'   => 'terms',
			'uses' => 'PrereqFactsTermsController@showTerms'
		]);

		Route::get('prereq', [
			'as'   => 'prereq',
			'uses' => 'PrereqFactsTermsController@showPrereq'
		]);
		Route::get('prereq/add', [
			'as'   => 'prereq.add',
			'uses' => 'PrereqFactsTermsController@showPrereq'
		]);
		Route::get('facts', [
			'as'   => 'facts',
			'uses' => 'PrereqFactsTermsController@showFacts'
		]);

		Route::post('add-content/{mapType}', [
			'as'   => 'add',
			'uses' => 'PrereqFactsTermsController@addContent'
		]);

		Route::get('delete/{id}/{mapType}', [
			'as'   => 'delete',
			'uses' => 'PrereqFactsTermsController@deleteContent'
		]);

		Route::post('edit/{id}/{mapType}', [
			'as'   => 'edit',
			'uses' => 'PrereqFactsTermsController@editContent'
		]);

		//add, edit, delete tag
		Route::group(['prefix' => 'tags', 'as' => 'tags.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeTagsController@showTagsList'
			]);

			Route::get('/add', [
				'as'   => 'create',
				'uses' => 'TypeTagsController@createTags'
			]);

			Route::post('save', [
				'as'   => 'save',
				'uses' => 'TypeTagsController@saveTags'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeTagsController@deleteTags'
			]);

			Route::get('edit/{id}', [
				'as'   => 'edit',
				'uses' => 'TypeTagsController@editTags'
			]);

			Route::post('edit/{id}', [
				'as'   => 'edit.save',
				'uses' => 'TypeTagsController@saveEditedTags'
			]);
		});

		//add delete type hall
		Route::group(['prefix' => 'halls', 'as' => 'halls.'], function () {
			Route::get('/', [
				'as'   => 'show',
				'uses' => 'TypeHallController@showTypeHalls'
			]);

			Route::post('/', [
				'as'   => 'create',
				'uses' => 'TypeHallController@addTypeHall'
			]);

			Route::get('{id}', [
				'as'   => 'delete',
				'uses' => 'TypeHallController@deleteTypeHall'
			]);

		});

		//add delete type venue
		Route::group(['prefix' => 'venue', 'as' => 'venue.'], function () {

			Route::get('/', [
				'as'   => 'show',
				'uses' => 'TypeVenueController@showTypeVenues'
			]);

			Route::post('/', [
				'as'   => 'create',
				'uses' => 'TypeVenueController@addTypeVenue'
			]);

			Route::get('{id}', [
				'as'   => 'delete',
				'uses' => 'TypeVenueController@deleteTypeVenue'
			]);

		});

		//add, delete type vendor
		Route::group(['prefix' => 'vendor', 'as' => 'vendor.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeVendorController@showTypeVendor'
			]);

			Route::post('create', [
				'as'   => 'create',
				'uses' => 'TypeVendorController@createTypeVendor'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeVendorController@deleteTypeVendor'
			]);
		});

		//add, delete type ticket source
		Route::group(['prefix' => 'source', 'as' => 'source.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeTicketSourceController@showTicketSourceList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeTicketSourceController@addTypeSource'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeTicketSourceController@deleteTypeSource'
			]);
		});

		//add, delete type event
		Route::group(['prefix' => 'event', 'as' => 'event.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeEventController@showTypeEventList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeEventController@addTypeEvent'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeEventController@deleteTypeEvent'
			]);
		});

		//add, delete type slot
		Route::group(['prefix' => 'slot', 'as' => 'slot.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeSlotController@showTypeSlotList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeSlotController@addSlotEvent'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeSlotController@deleteSlotEvent'
			]);
		});

		//add, delete type cuisine
		Route::group(['prefix' => 'cuisine', 'as' => 'cuisine.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeCuisineController@showTypeCuisineList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeCuisineController@addTypeCuisine'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeCuisineController@deleteTypeCuisine'
			]);
		});

		//add, delete venue_gallery
		Route::group(['prefix' => 'gallery', 'as' => 'gallery.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeVenueGalleryController@showTypeVenueGalleryList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeVenueGalleryController@addTypeVenueGallery'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeVenueGalleryController@deleteTypeVenueGallery'
			]);
		});

		//add delete venue menu
		Route::group(['prefix' => 'menu', 'as' => 'menu.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeVenueMenuController@showTypeVenueMenuList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeVenueMenuController@addTypeVenueMenu'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeVenueMenuController@deleteTypeVenueMenu'
			]);
		});

		//add, delete package gallery
		Route::group(['prefix' => 'package/gallery', 'as' => 'package.gallery.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypePackageGalleryController@showTypePackageGalleryList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypePackageGalleryController@addTypePackageGallery'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypePackageGalleryController@deleteTypePackageGallery'
			]);
		});

		//add, delete type package field category for dynamic package field, (where to shown on main website)
		Route::group(['prefix' => 'package/fieldCat', 'as' => 'package.field.category.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypePackageFieldCategoryController@showList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypePackageFieldCategoryController@addCategoryField'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypePackageFieldCategoryController@deleteCategoryField'
			]);
		});

		//add, delete type ticket booking
		Route::group(['prefix' => 'typeTicketBooking', 'as' => 'type_ticket_booking.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeTicketBookingController@showList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeTicketBookingController@addTypeTicketBookingField'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeTicketBookingController@deleteTypeTicketBookingField'
			]);

			Route::get('table', [
				'as'   => 'table',
				'uses' => 'TypeTicketBookingController@getTypeTicketBookingsTable'
			]);
		});

		// add, delete cake flavour
		Route::group(['prefix' => 'cake/flavour', 'as' => 'cake.flavour.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeCakeFlavourController@showList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeCakeFlavourController@addCakeFlavour'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeCakeFlavourController@deleteCakeFlavour'
			]);
		});

		Route::group(['prefix' => 'track/questions', 'as' => 'tracking.questions.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'TypeTrackingQuestionsController@showList'
			]);

			Route::post('/', [
				'as'   => 'add',
				'uses' => 'TypeTrackingQuestionsController@addTrackingQuestion'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'TypeTrackingQuestionsController@deleteTrackingQuestion'
			]);
		});

	});

	Route::group(['prefix' => 'audit', 'as' => 'audit.', 'namespace' => 'Util'], function () {
		//Route::get('/', function () {
		//	return redirect('audit/edit/new');
		//});

		Route::get('edit-new', [
			'as'   => 'edit-new',
			'uses' => 'AuditController@getAuditEditNewData'
		]);

		Route::post('edit-new', [
			'as'   => 'edit-new',
			'uses' => 'AuditController@saveAuditEditNewData'
		]);

		Route::get('edit/{query}', [
			'as'   => 'edit',
			'uses' => 'AuditController@getAuditEditData'
		]);

		Route::post('edit/{query}', [
			'as'   => 'edit',
			'uses' => 'AuditController@saveAuditEditData'
		]);

		Route::get('review', [
			'as'   => 'review-show',
			'uses' => 'AuditController@getAuditReviewData'
		]);

		Route::post('review/{query}', [
			'as'   => 'review',
			'uses' => 'AuditController@saveAuditReviewData'
		]);
	});

	/**
	 * Services Tab
	 *
	 * @author Vikash <vikash@evibe.in>
	 * @Since  26 Feb 2016
	 */
	Route::group(['prefix' => 'services', 'as' => 'services.', 'namespace' => 'Services'], function () {
		Route::get('/', [
			'as'   => 'show',
			'uses' => 'ServiceController@showServices'
		]);

		Route::get('add', [
			'as'   => 'new',
			'uses' => 'ServiceController@showAddServices'
		]);

		Route::post('add', [
			'as'   => 'add',
			'uses' => 'ServiceController@saveServices'
		]);

		Route::get('edit/{id}', [
			'as'   => 'edit',
			'uses' => 'ServiceController@showEditServices'
		]);

		Route::post('edit/{id}', [
			'as'   => 'update',
			'uses' => 'ServiceController@updateServices'
		]);

		Route::get('details/{id}', [
			'as'   => 'details',
			'uses' => 'ServiceController@showServiceDetails'
		]);

		Route::get('delete/{id}', [
			'as'   => 'delete',
			'uses' => 'ServiceController@deleteServices'
		]);

		Route::post('upload/image/{id}', [
			'as'   => 'gallery.upload',
			'uses' => 'ServiceController@uploadGallery'
		]);

		Route::get('delete/{id}/{imageId}', [
			'as'   => 'gallery.delete',
			'uses' => 'ServiceController@deleteGallery'
		]);

		Route::post('title/{id}/{imageId}', [
			'as'   => 'gallery.title',
			'uses' => 'ServiceController@saveEditedTitle'
		]);

		Route::get('gallery/setProfile/{serviceId}/{imageId}', [
			'as'   => 'gallery.setProfile',
			'uses' => 'ServiceController@setProfilePic'
		]);

		Route::get('live/{id}', [
			'as'   => 'live',
			'uses' => 'ServiceController@makeLive'
		]);

		Route::get('non-live/{id}', [
			'as'   => 'nonlive',
			'uses' => 'ServiceController@makeNonLive'
		]);

		Route::get('{id}/{categoryId}/delete', [
			'as'   => 'category.delete',
			'uses' => 'ServiceController@deleteServiceCategory'
		]);

		Route::post('{id}/category/add', [
			'as'   => 'categories.add',
			'uses' => 'ServiceController@addServiceCategory'
		]);
	});

	/**
	 * User Managements System
	 *
	 * @author Viaksh <vikash@evibe.in>
	 * @Since  14 March 2016
	 */
	Route::group(['prefix'     => 'user',
	              'as'         => 'user.',
	              'namespace'  => 'User',
	              'middleware' => 'authorize.user_management'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'UserController@showList',
		]);
		Route::get('add', [
			'as'   => 'add.show',
			'uses' => 'UserController@showAddUser',
		]);

		Route::get('add/{userId}/details', [
			'as'   => 'add.details.show',
			'uses' => 'UserController@showAddUserDetails'
		]);

		Route::post('add/{userId}/save', [
			'as'   => 'save.details',
			'uses' => 'UserController@saveUserDetails'
		]);

		Route::get('change-password/{userId}', [
			'as'   => 'change-password.show',
			'uses' => 'UserController@showChangePassword'
		]);

		Route::post('change-password/{userId}', [
			'as'   => 'change-password.save',
			'uses' => 'UserController@saveChangePassword'
		]);

		Route::post('save', [
			'as'   => 'save',
			'uses' => 'UserController@addUser',
		]);
		Route::post('edit/{id}', [
			'as'   => 'edit',
			'uses' => 'UserController@updateUser',
		]);
		Route::get('delete/{id}', [
			'as'   => 'delete',
			'uses' => 'UserController@deleteUser',
		]);

		//edit, delete, update user's role
		Route::group(['prefix' => 'role', 'as' => 'role.'], function () {
			Route::get('list', [
				'as'   => 'list',
				'uses' => 'UserController@showRoleList'
			]);
			Route::get('add', [
				'as'   => 'show',
				'uses' => 'UserController@showAddRole'
			]);
			Route::post('save', [
				'as'   => 'save',
				'uses' => 'UserController@saveRole'
			]);
			Route::post('update/{id}', [
				'as'   => 'update',
				'uses' => 'UserController@updateRole'
			]);
			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'UserController@deleteRole',
			]);
		});
	});

	/**
	 * check availability of venue
	 *
	 * @author Vikash <vikash@evibe.in>
	 * @Since  21 March 2016
	 */
	Route::group(['prefix' => 'availability', 'as' => 'availability.', 'namespace' => 'Availability'], function () {
		Route::group(['prefix' => 'venue', 'as' => 'venue.'], function () {
			Route::get('live/list', [
				'as'   => 'live',
				'uses' => 'VenueAvailabilityController@availabilityLiveCheckList'
			]);
			Route::get('archive/list', [
				'as'   => 'archive',
				'uses' => 'VenueAvailabilityController@availabilityArchiveList'
			]);
			Route::get('details/live/{enquiryId}', [
				'as'   => 'live.details',
				'uses' => 'VenueAvailabilityController@liveAvailabilityDetails'
			]);
			Route::get('details/archive/{enquiryId}', [
				'as'   => 'archive.details',
				'uses' => 'VenueAvailabilityController@ArchiveAvailabilityDetails'
			]);
			Route::get('delete/{ticketId}', [
				'as'   => 'delete',
				'uses' => 'VenueAvailabilityController@deleteAvailabilityCheck'
			]);
			Route::post('ask-availability/confirm/load/{ticketId}', [
				'as'   => 'confirm.load',
				'uses' => 'VenueAvailabilityController@loadVenueConfirmAvailability'
			]);
			Route::post('ask-availability/confirm/{ticketId}', [
				'as'   => 'confirm',
				'uses' => 'VenueAvailabilityController@confirmVenueAvailability'
			]);
			Route::post('ask-availability/deny/load/{ticketId}', [
				'as'   => 'deny.load',
				'uses' => 'VenueAvailabilityController@loadVenueDenyAvailability'
			]);
			Route::post('ask-availability/deny/{ticketId}', [
				'as'   => 'deny',
				'uses' => 'VenueAvailabilityController@denyVenueAvailability'
			]);
		});

		Route::group(['prefix' => 'check/{ticketId}', 'as' => 'check.'], function () {
			Route::post('fetch', [
				'as'   => 'fetch',
				'uses' => 'AvailabilityCheckController@fetchAvailabilityData'
			]);

			Route::post('post', [
				'as'   => 'post',
				'uses' => 'AvailabilityCheckController@postAvailabilityChecks'
			]);
		});
	});

	/*
	 * Partner ask enquiry(availability + customQuote) routes (for Evibe.in partner App).
	 */

	Route::group(['prefix' => 'enquiry', 'as' => 'enquiry.', 'namespace' => 'Enquiry'], function () {
		Route::group(['prefix' => 'partner', 'as' => 'partner.'], function () {
			Route::get('{id}', [
				'as'   => 'ask.details',
				'uses' => 'PartnerEnquiryController@showAskDetails'
			]);

			Route::post('{id}', [
				'as'   => 'save.details',
				'uses' => 'PartnerEnquiryController@saveEnquiryDetails'
			]);

			Route::get('edit/{id}', [
				'as'   => 'edit.details',
				'uses' => 'PartnerEnquiryController@showEnquiryDetails'
			]);

			Route::post('edit/{id}', [
				'as'   => 'update.details',
				'uses' => 'PartnerEnquiryController@updateEnquiryDetails'
			]);

			Route::post('ask/multiple-quote', [
				'as'   => 'multiple_quote.ask',
				'uses' => 'PartnerEnquiryController@askMultipleCustomQuote'
			]);

			Route::get('ask/{id}', [
				'as'   => 'ask',
				'uses' => 'PartnerEnquiryController@askEnquiry'
			]);

			Route::post('accept/{id}', [
				'as'   => 'accept',
				'uses' => 'PartnerEnquiryController@acceptEnquiry'
			]);

			Route::post('reject/{id}', [
				'as'   => 'reject',
				'uses' => 'PartnerEnquiryController@rejectEnquiry'
			]);

			Route::post('reply/{id}', [
				'as'   => 'reply',
				'uses' => 'PartnerEnquiryController@replyCustomQuote'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'PartnerEnquiryController@deleteEnquiry'
			]);

			Route::get('gallery/delete/{imgId}', [
				'as'   => 'gallery.delete',
				'uses' => 'PartnerEnquiryController@deleteEnquiryGallery'
			]);
		});
	});

	Route::group([
		             'prefix'     => 'partner',
		             'as'         => 'partner.',
		             'namespace'  => 'Partner',
		             'middleware' => 'authorize.partner_profile'
	             ], function () {
		Route::get('bank-details/{userId}', [
			'as'   => 'bank_details.edit',
			'uses' => 'PartnerProfileController@showEditBankDetails'
		]);

		Route::post('bank-details/{userId}', [
			'as'   => 'bank_details.update',
			'uses' => 'PartnerProfileController@saveBankDetails'
		]);

		Route::get('activation', [
			'as'   => 'activation.accepted',
			'uses' => 'PartnerActivationController@showAcceptedActivationList'
		]);

		Route::get('activation/rejected', [
			'as'   => 'activation.rejected',
			'uses' => 'PartnerActivationController@showRejectedActivationList'
		]);

		/**
		 * Partner Delivery Console
		 *
		 * @author: Vikash <vikash@evibe.in>
		 * @since : 3 Feb 2017
		 */
	});

	Route::group(['prefix' => 'partner/delivery', 'as' => 'partner.delivery.', 'namespace' => 'Partner'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'PartnerDeliveryController@showDeliveries'
		]);

		//Route::post('show/{bookingId}', [
		//	'as'   => 'show.upload',
		//	'uses' => 'PartnerDeliveryController@showBookingDelivery'
		//]);

		Route::get('details/{ticketId}', [
			'as'   => 'details',
			'uses' => 'PartnerDeliveryController@showDeliveryDetails'
		]);
	});

	/**
	 * @details:Settlement excel , downloading excel with all the booking amount details
	 * @author Vikash <vikash@evibe.in>
	 * @Since  25th April 2016
	 */
	Route::group(['prefix' => 'settlement', 'as' => 'settlement.', 'namespace' => 'Settlement'], function () {
		Route::get('/', [
			'as'   => 'requirement',
			'uses' => 'PaymentSettlementController@getRequirement'
		]);

		Route::post('get', [
			'as'   => 'get',
			'uses' => 'PaymentSettlementController@getPaymentSettlement'
		]);

		Route::post('partner', [
			'as'   => 'partner',
			'uses' => 'PaymentSettlementController@showPartnerSettlement'
		]);

		Route::get('partner/analyse/{mapId}/{mapTypeId}', [
			'as'   => 'partner.analyse',
			'uses' => 'PaymentSettlementController@showAnalysePartner'
		]);

		Route::post('partner/download/{mapId}/{mapTypeId}', [
			'as'   => 'partner.sendEmail',
			'uses' => 'PaymentSettlementController@sendEmailToPartner'
		]);
	});

	Route::group(['prefix' => 'finance', 'as' => 'finance.', 'namespace' => 'Finance'], function () {
		Route::group(['prefix' => 'settlements', 'as' => 'settlement.'], function () {
			Route::get('/', [
				'as'   => 'get',
				'uses' => 'SettlementController@getSettlementsList'
			]);

			Route::get('{query}', [
				'as'   => 'list',
				'uses' => 'SettlementController@getSettlementsList'
			]);

			Route::get('{settlementId}/info', [
				'as'   => 'info',
				'uses' => 'SettlementController@getSettlementDetails'
			]);

			Route::post('create', [
				'as'   => 'create',
				'uses' => 'SettlementController@createSettlements'
			]);

			Route::post('multi-settle-up', [
				'as'   => 'multi-settle-up',
				'uses' => 'SettlementController@processMultipleSettlements'
			]);

			Route::put('{settlementId}/process', [
				'as'   => 'process',
				'uses' => 'SettlementController@processSettlement'
			]);

			Route::post('download/{query}', [
				'as'   => 'download',
				'uses' => 'SettlementController@downloadSettlementSheet'
			]);

			Route::post('commission', [
				'as'   => 'commission',
				'uses' => 'SettlementController@downloadSettlementsCommission'
			]);
		});

		Route::group(['prefix' => 'quick-settlements', 'as' => 'quick-settlements.'], function () {
			Route::get('/', [
				'as'   => 'get',
				'uses' => 'SettlementController@getQuickSettlementsData'
			]);

			Route::put('process', [
				'as'   => 'process',
				'uses' => 'SettlementController@processQuickSettlement'
			]);
		});

		Route::group(['prefix' => 'partner-adjustments', 'as' => 'partner-adjustment.'], function () {
			Route::get('/', [
				'as'   => 'get',
				'uses' => 'PartnerAdjustmentController@getPartnerAdjustmentsList'
			]);

			Route::get('{query}', [
				'as'   => 'list',
				'uses' => 'PartnerAdjustmentController@getPartnerAdjustmentsList'
			]);

			Route::post('save', [
				'as'   => 'create',
				'uses' => 'PartnerAdjustmentController@createPartnerAdjustment'
			]);

			Route::put('update', [
				'as'   => 'update',
				'uses' => 'PartnerAdjustmentController@updatePartnerAdjustment'
			]);

			Route::put('delete', [
				'as'   => 'delete',
				'uses' => 'PartnerAdjustmentController@deletePartnerAdjustment'
			]);
		});

		Route::group(['prefix' => 'customer-refunds', 'as' => 'customer-refund.'], function () {

			Route::get('/', [
				'as'   => 'get',
				'uses' => 'RefundController@getCustomerRefundsList'
			]);

			Route::get('{query}', [
				'as'   => 'list',
				'uses' => 'RefundController@getCustomerRefundsList'
			]);

			Route::post('save', [
				'as'   => 'create',
				'uses' => 'RefundController@createCustomerRefund'
			]);

			Route::put('update', [
				'as'   => 'update',
				'uses' => 'RefundController@updateCustomerRefund'
			]);

			Route::put('reject', [
				'as'   => 'reject',
				'uses' => 'RefundController@rejectCustomerRefund'
			]);

			Route::put('approve', [
				'as'   => 'approve',
				'uses' => 'RefundController@approveCustomerRefund'
			]);
		});
	});

	/**
	 * @Since 17th Many 2016
	 * @detail: Stories module for customer & Vendor : Create, Edit, Delete
	 * @added reordering, ui improved :: modified by Vikash <vikash@evibe.in> since 16 Feb 2017
	 */
	Route::group(['prefix' => 'stories', 'as' => 'stories.', 'namespace' => 'Stories'], function () {
		Route::group(['prefix' => 'customer', 'as' => 'customer.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'CustomerStoriesController@viewCustomerStories'
			]);

			Route::get('add', [
				'as'   => 'add',
				'uses' => 'CustomerStoriesController@addCustomerStory'
			]);

			Route::post('add', [
				'as'   => 'save',
				'uses' => 'CustomerStoriesController@saveCustomerStory'
			]);

			Route::get('edit/{id}', [
				'as'   => 'edit',
				'uses' => 'CustomerStoriesController@showEditCustomerStory'
			]);

			Route::post('update/{id}', [
				'as'   => 'update',
				'uses' => 'CustomerStoriesController@updateCustomerStory'
			]);

			Route::get('remove/{id}', [
				'as'   => 'image.remove',
				'uses' => 'CustomerStoriesController@removeCustomerStoryImage'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'CustomerStoriesController@deleteCustomerStory'
			]);

			Route::post('reorder', [
				'as'   => 'reorder',
				'uses' => 'CustomerStoriesController@saveStoryPriority'
			]);

		});

		Route::group(['prefix' => 'vendor', 'as' => 'vendor.'], function () {

			Route::get('/', [
				'as'   => 'list',
				'uses' => 'VendorStoriesController@viewVendorStories'
			]);

			Route::get('add', [
				'as'   => 'add',
				'uses' => 'VendorStoriesController@addVendorStory'
			]);

			Route::post('add', [
				'as'   => 'save',
				'uses' => 'VendorStoriesController@saveVendorStory'
			]);

			Route::get('edit/{id}', [
				'as'   => 'edit',
				'uses' => 'VendorStoriesController@showEditVendorStory'
			]);

			Route::post('update/{id}', [
				'as'   => 'update',
				'uses' => 'VendorStoriesController@updateVendorStory'
			]);

			Route::get('remove/{id}', [
				'as'   => 'image.remove',
				'uses' => 'VendorStoriesController@removeVendorStoryImage'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'VendorStoriesController@deleteVendorStory'
			]);

			Route::post('reorder', [
				'as'   => 'reorder',
				'uses' => 'VendorStoriesController@saveStoryPriority'
			]);
		});
	});

	/**
	 * Package dynamic field console: Add, Edit; checkout field Add, Delete;
	 *
	 * @Since 26 May 2016 by Vikash <Vikash@evibe.in>
	 */
	Route::group(['prefix' => 'field', 'as' => 'field.', 'namespace' => 'Field'], function () {
		Route::group(['prefix' => 'package', 'as' => 'package.'], function () {
			Route::get('view', [
				'as'   => 'list',
				'uses' => 'PackageFieldController@showFieldList'
			]);

			Route::get('add', [
				'as'   => 'add',
				'uses' => 'PackageFieldController@showAddField'
			]);

			Route::post('save', [
				'as'   => 'save',
				'uses' => 'PackageFieldController@saveFieldDetails'
			]);

			Route::get('edit/{id}', [
				'as'   => 'edit',
				'uses' => 'PackageFieldController@showEditField'
			]);

			Route::post('update/{id}', [
				'as'   => 'update',
				'uses' => 'PackageFieldController@updateFieldDetails'
			]);

			Route::get('reorder', [
				'as'   => 'reorder',
				'uses' => 'PackageFieldController@showReorderPackageField'
			]);

			Route::post('reorder/save', [
				'as'   => 'reorder.save',
				'uses' => 'PackageFieldController@reorderPackageField'
			]);

			Route::get('child/delete/{childId}', [
				'as'   => 'child.delete',
				'uses' => 'PackageFieldController@deleteChild'
			]);

			Route::get('active/{id}', [
				'as'   => 'active',
				'uses' => 'PackageFieldController@activateField'
			]);

			Route::get('inactivate/{id}', [
				'as'   => 'inactive',
				'uses' => 'PackageFieldController@inactivateField'
			]);

		});

		// checkout field console

		Route::group(['prefix' => 'checkout', 'as' => 'checkout.'], function () {
			Route::get('list', [
				'as'   => 'list',
				'uses' => 'CheckoutFieldController@showList'
			]);

			Route::get('add', [
				'as'   => 'add',
				'uses' => 'CheckoutFieldController@showAddField'
			]);

			Route::post('save', [
				'as'   => 'save',
				'uses' => 'CheckoutFieldController@saveField'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'CheckoutFieldController@deleteField'
			]);
		});
	});

	/**
	 * Manage which element should have auto-book option
	 *
	 * @Since  : 1 June 2016 By Vikash <vikash@evibe.in>
	 */
	Route::group(['prefix' => 'auto/booking', 'namespace' => 'AutoBooking', 'as' => 'auto.booking.'], function () {
		Route::get('list', [
			'as'   => 'list',
			'uses' => 'AutoBookingController@showItemList'
		]);

		Route::post('action', [
			'as'   => 'save',
			'uses' => 'AutoBookingController@saveAutoBookings'
		]);
	});

	Route::group(['prefix' => 'feedback', 'as' => 'feedback.', 'namespace' => 'Util'], function () {
		Route::post('customer/accept/{id}', [
			'as'   => 'customer.accept',
			'uses' => 'CustomerFeedbackController@confirmCustomerFeedback'
		]);

		Route::post('customer/add-review/{id}', [
			'as'   => 'customer.add',
			'uses' => 'CustomerFeedbackController@addCustomerFeedback'
		]);

		Route::post('customer/edit-review/{id}/{feedBackId}', [
			'as'   => 'customer.edit',
			'uses' => 'CustomerFeedbackController@editCustomerFeedback'
		]);

		Route::get('pending', [
			'as'   => 'pending',
			'uses' => 'CustomerFeedbackController@showPendingFeedback'
		]);

		Route::get('pending/approval', [
			'as'   => 'pending.approval',
			'uses' => 'CustomerFeedbackController@showPendingApproval'
		]);

		Route::get('vendor/show/{vendorFeedbackId}', [
			'as'   => 'vendor.show',
			'uses' => 'VendorFeedbackController@showVendorFeedbackInfo'
		]);

		Route::post('vendor/confirm/{id}', [
			'as'   => 'vendor.confirm',
			'uses' => 'VendorFeedbackController@confirmVendorFeedback'
		]);

		Route::get('vendor/load/{id}/questions', [
			'as'   => 'vendor.load.questions',
			'uses' => 'VendorFeedbackController@loadVendorFeedbackQuestion'
		]);

		Route::get('vendor/load/{feedbackId}/edit', [
			'as'   => 'vendor.load.edit',
			'uses' => 'VendorFeedbackController@loadEditVendorFeedback'
		]);

		Route::post('vendor/add-review/{id}', [
			'as'   => 'vendor.add',
			'uses' => 'VendorFeedbackController@addVendorFeedback'
		]);

		Route::post('vendor/edit-review/{id}/{feedBackId}', [
			'as'   => 'vendor.edit',
			'uses' => 'VendorFeedbackController@editVendorFeedback'
		]);
	});

	/**
	 * Analytics Dashboard for Admins
	 *
	 * @author Anji <anji@evibe.in>
	 * @since  03 Sep 2016
	 */
	Route::group(['middleware' => 'authorize.analytics',
	              'namespace'  => 'Analytics',
	              'prefix'     => 'analytics',
	              'as'         => 'analytics.'
	             ], function () {
		Route::any('cumulative', [
			'as'   => 'cumulative',
			'uses' => 'AnalyticsController@showCumulativeData'
		]);

		Route::group(["prefix" => "detailed", "as" => "detailed."], function () {
			Route::get("revenue", [
				'as'   => "revenue",
				'uses' => "AnalyticsController@showDetailedRevenueData"
			]);

			Route::any("enquiries", [
				'as'   => "enquiries",
				'uses' => "AnalyticsController@showDetailedEnquiryData"
			]);

			Route::any("bookings", [
				'as'   => "bookings",
				'uses' => "AnalyticsController@showDetailedBookingsData"
			]);

			Route::post("bookings/additional", [
				'as'   => "bookings.additional",
				'uses' => "AnalyticsController@fetchADDOns"
			]);

			Route::post("bookings/ticket-data", [
				'as'   => "bookings.ticket.data",
				'uses' => "AnalyticsController@getBookingsTicketData"
			]);

			Route::post("/line/bookings/status", [
				'as'   => "bookings.ticket.data",
				'uses' => "AnalyticsController@getBookingsTicketStatusData"
			]);
		});

		Route::any('funnel', [
			'as'   => 'funnel',
			'uses' => 'AnalyticsController@funnelChart'
		]);

		Route::get('refer', [
			'as'   => 'refer',
			'uses' => 'AnalyticsController@referAndEarnStatistics'
		]);
	});

	/**
	 * Approval Dashboard after editing or adding items
	 *
	 * @author Vikash <vikash@evibe.in>
	 * @since  01 Dec 2016
	 */

	Route::group(['prefix' => 'approval', 'namespace' => 'Approval', 'as' => 'approval.'], function () {
		Route::get('ask/{mapId}/{mapTypeId}', [
			'as'   => 'ask',
			'uses' => 'ApprovalController@askApproval'
		]);

		Route::get('accept/{mapId}/{mapTypeId}', [
			'as'   => 'accept',
			'uses' => 'ApprovalController@acceptApproval'
		]);

		Route::get('reject/{mapId}/{mapTypeId}', [
			'as'   => 'reject',
			'uses' => 'ApprovalController@rejectApproval'
		]);
	});

	/**
	 * Booking calendar
	 *
	 * @author Vikash <vikash@evibe.in>
	 * @since  21 Dec 2016
	 */

	Route::group(['prefix' => 'calendar', 'as' => 'calendar.', 'namespace' => 'Calendar'], function () {
		Route::group(['prefix' => 'booking', 'as' => 'booking.'], function () {
			Route::get('/', [
				'as'   => 'show',
				'uses' => 'BookingCalendarController@showCalendar'
			]);

			Route::post('info', [
				'as'   => 'getData',
				'uses' => 'BookingCalendarController@getBookingData'
			]);
		});
	});

	/* Collection system, creation and tagging options
	*
	* @author Vikash <vikash@evibe.in>
	* @since  22 Dec 2016
	*/

	Route::group(['prefix' => 'collection', 'namespace' => 'Collection', 'as' => 'collection.'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'CollectionController@showCollections'
		]);

		Route::get('create', [
			'as'   => 'create',
			'uses' => 'CollectionController@showCreateCollection'
		]);

		Route::post('save', [
			'as'   => 'save',
			'uses' => 'CollectionController@saveCollection'
		]);

		Route::get('edit/{id}', [
			'as'   => 'edit',
			'uses' => 'CollectionController@showEditCollection'
		]);

		Route::post('update/{id}', [
			'as'   => 'update',
			'uses' => 'CollectionController@updateCollection'
		]);

		Route::get('{id}/details', [
			'as'   => 'details',
			'uses' => 'CollectionController@showCollectionDetails'
		]);

		Route::get('live/{id}', [
			'as'   => 'live',
			'uses' => 'CollectionController@makeLive'
		]);

		Route::get('non-live/{id}', [
			'as'   => 'non_live',
			'uses' => 'CollectionController@makeNonLive'
		]);

		Route::get('delete/{id}', [
			'as'   => 'delete',
			'uses' => 'CollectionController@deleteCollection'
		]);

		Route::get('reorder/show', [
			'as'   => 'reorder.show',
			'uses' => 'CollectionController@showCollectionPriority'
		]);

		Route::post('reorder/save', [
			'as'   => 'reorder.save',
			'uses' => 'CollectionController@saveCollectionPriority'
		]);

		Route::group(['prefix' => 'gallery', 'as' => 'gallery.'], function () {
			Route::post('upload/{id}', [
				'as'   => 'upload',
				'uses' => 'CollectionController@uploadGallery'
			]);

			Route::get('delete/{id}', [
				'as'   => 'delete',
				'uses' => 'CollectionController@deleteGallery'
			]);
		});

		Route::group(['prefix' => '{id}/options', 'as' => 'options.'], function () {
			Route::get('/', [
				'as'   => 'show',
				'uses' => 'CollectionOptionController@showOptions'
			]);

			Route::post('/items', [
				'as'   => 'items.fetch',
				'uses' => 'CollectionOptionController@fetchItems'
			]);

			Route::post('/save', [
				'as'   => 'save',
				'uses' => 'CollectionOptionController@saveOption'
			]);

			Route::get('/delete/{OptId}', [
				'as'   => 'delete',
				'uses' => 'CollectionOptionController@removeOption'
			]);

			Route::get('/reorder', [
				'as'   => 'reorder.show',
				'uses' => 'CollectionOptionController@showReorder'
			]);

			Route::post('/reorder/save', [
				'as'   => 'reorder.save',
				'uses' => 'CollectionOptionController@saveOptionPriority'
			]);
		});
	});

	Route::group(['prefix' => 'add-ons', 'namespace' => 'AddOn', 'as' => 'add-ons.'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'AddOnController@showAddOns'
		]);

		Route::get('{id}/info', [
			'as'   => 'info',
			'uses' => 'AddOnController@showAddOnInfo'
		]);

		Route::group(['prefix' => 'option-mappings', 'as' => 'option-mappings.'], function () {
			Route::get('/', [
				'as'   => 'map',
				'uses' => 'AddOnController@showOptionMappings'
			]);

			Route::get('options', [
				'as'   => 'options',
				'uses' => 'AddOnController@fetchOptions'
			]);

			Route::post('toggle', [
				'as'   => 'toggle',
				'uses' => 'AddOnController@toggleAddOnAvailability'
			]);
		});
	});

	Route::get('partner/autoLogin/{shortUrl}', [
		'as'   => 'partner.profile.autoLogin',
		'uses' => 'Base\BaseController@redirectPartnerProfile'
	]);

	/**
	 * Team profile Console. manage team from console
	 *
	 * @author:: Vikash <vikash@evibe.in>
	 * @since :: 18 Feb 2017
	 */

	Route::group(['prefix' => 'team', 'as' => 'team.', 'namespace' => 'Team'], function () {
		Route::get('/', [
			'as'   => 'list',
			'uses' => 'TeamController@showList'
		]);

		Route::get('/new', [
			'as'   => 'member.new',
			'uses' => 'TeamController@showCreateMember'
		]);

		Route::post('/create', [
			'as'   => 'member.create',
			'uses' => 'TeamController@createTeamMember'
		]);

		Route::get('/edit/{id}', [
			'as'   => 'member.edit',
			'uses' => 'TeamController@showEditTeamMember'
		]);

		Route::post('/update/{id}', [
			'as'   => 'member.update',
			'uses' => 'TeamController@updateTeamMember'
		]);

		Route::get('/details/{id}', [
			'as'   => 'member.details',
			'uses' => 'TeamController@showTeamMemberDetails'
		]);

		Route::get('/delete/{id}', [
			'as'   => 'member.delete',
			'uses' => 'TeamController@deleteTeamMember'
		]);
	});

	Route::group(['prefix'    => 'gallery.in/planner',
	              'as'        => 'gallery-planners.',
	              'namespace' => 'Vendor'],
		function () {
			Route::get('/', [
				'as'   => 'will.use',
				'uses' => 'VendorsController@showFile'
			]);
			Route::get('/{imageShow}', [
				'as'   => 'image.show',
				'uses' => 'VendorsController@showFile'
			]);
		});

	Route::group(['prefix' => 'automation', 'as' => 'automation.', 'namespace' => 'Automation'], function () {
		Route::get('/', [
			'as'   => 'home',
			'uses' => 'AutomationController@getAutoFollowupReminders'
		]);

		Route::get('reminders', [
			'as'   => 'reminders',
			'uses' => 'AutomationController@getAutoFollowupReminders'
		]);

		Route::get('test-followups', [
			'as'   => 'test-followups',
			'uses' => 'AutomationController@fetchFollowupReminders'
		]);
	});

	Route::group(['prefix' => 'downloads', 'as' => 'downloads.', 'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'home',
			'uses' => 'DownloadSheetsController@showDownloadableOptions'
		]);

		Route::get('star-orders/{date}', [
			'as'   => 'star-orders',
			'uses' => 'DownloadSheetsController@starOrdersSettlementData'
		]);
	

		Route::get('planners', [
			'as'   => 'planners',
			'uses' => 'DownloadSheetsController@plannersData'
		]);

		Route::get('venues', [
			'as'   => 'venues',
			'uses' => 'DownloadSheetsController@venuesData'
		]);

		Route::post('gst-settlements', [
			'as'   => 'gst-settlements',
			'uses' => 'DownloadSheetsController@gstSettlementsData'
		]);
	});

	Route::group(['prefix' => 'partner-options', 'as' => 'partner.options.', 'namespace' => 'Partner'], function () {
		Route::get('/', [
			'as'   => 'home',
			'uses' => 'PartnerProfileController@getAllPartners',
		]);

		Route::get('/{pTypeId}/{pId}', [
			'as'   => 'packages',
			'uses' => 'PartnerProfileController@showPackages'
		]);
	});

	Route::group(['prefix' => 'resolution', 'as' => 'resolution.note', 'namespace' => 'Partner'], function () {

		Route::get('/{typeOfOption}', [
			'as'   => 'testing',
			'uses' => 'ResolutionController@getImageResolutions'
		]);
	});

	Route::group(['prefix' => 'download-images', 'as' => 'download.images.', 'namespace' => 'Stories'], function () {
		Route::get('/', [
			'as'   => 'getCodes',
			'uses' => 'ImagesDownloadController@getAllImageCodes',
		]);

		Route::get('/{productCode}', [
			'as'   => 'getAllImages',
			'uses' => 'ImagesDownloadController@showAllImages',
		]);
	});
	Route::get('/track-email', [
		'as'   => 'trackEmail',
		'uses' => 'Ticket\TicketDetailsController@getEmailTrackerData'
	]);

	Route::group(['prefix' => 'ticket', 'as' => 'ticket.whatsapp.msg.'], function () {
		Route::post('whatsapp/normal', [
			'as'   => 'normal',
			'uses' => 'Base\BaseTicketController@sendWhatsAppMsg'
		]);

		Route::post('whatsapp/order-process', [
			'as'   => 'order-process',
			'uses' => 'Base\BaseTicketController@sendWhatsAppMsg'
		]);
	});

	Route::group(['prefix' => 'track-user', 'as' => 'track-user'], function () {
		Route::post('info', [
			'as'   => 'info',
			'uses' => 'Base\BaseController@trackUserInfo'
		]);
	});

	Route::group(['prefix' => 'reviews-mapping', 'as' => 'reviews-mapping.', 'namespace' => 'Review'], function () {
		Route::get('/', [
			'as'   => 'home',
			'uses' => 'ReviewsController@getPendingList'
		]);

		Route::get('mapped', [
			'as'   => 'mapped',
			'uses' => 'ReviewsController@getMappedList'
		]);

		Route::get('pending', [
			'as'   => 'pending',
			'uses' => 'ReviewsController@getPendingList'
		]);

		Route::post('options', [
			'as'   => 'getOptions',
			'uses' => 'ReviewsController@getOptionsDataByType'
		]);

		Route::post('map', [
			'as'   => 'save-option-mapping',
			'uses' => 'ReviewsController@saveOptionMapping'
		]);

	});

	Route::get('option-availability', [
		'as'   => 'option-availability',
		'uses' => 'Util\OptionAvailabilityController@getAvailabilitySlots'
	]);

});